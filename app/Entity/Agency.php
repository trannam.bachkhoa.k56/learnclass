<?php

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Agency extends Model
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $dates = ['deleted_at'];

    protected $table = 'agency';

    protected $primaryKey = 'agency_id';

    protected $fillable = [
        'agency_id',
        'name',
        'address',
        'phone',
        'email',
        'percent',
        'code',
        'bank_information',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
