<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class AppraiseClass extends Model
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $dates = ['deleted_at'];

    protected $table = 'appraise_class';

    protected $primaryKey = 'appraise_class_id';

    protected $fillable = [
        'appraise_class_id',
        'user_id',
        'classroom_id',
        'history',
        'point',
        'description',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public static function countAppraise($classroomId) {
        $appraiseClasses = static::where('classroom_id', $classroomId)->get();

        $totalPoint = 0;
        foreach ($appraiseClasses as $appraiseClass) {
            $totalPoint += $appraiseClass->point;
        }

        $countAppraise = static::where('classroom_id', $classroomId)->count();
        if ($countAppraise == 0) {
            return 0;
        }

        return (float) $totalPoint/$countAppraise;
    }

    public static function showAppraise ($classroomId) {
        $appraiseClasses = static::join('users', 'users.id', 'appraise_class.user_id')
        ->select('users.name', 'appraise_class.point', 'appraise_class.description', 'appraise_class.created_at', 'appraise_class_id')
        ->where('classroom_id', $classroomId)
        ->orderBy('appraise_class_id', 'desc')
        ->get();

        return $appraiseClasses;
    }

}
