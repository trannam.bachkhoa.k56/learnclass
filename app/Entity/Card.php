<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 10/17/2018
 * Time: 9:18 AM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Card extends Model
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $dates = ['deleted_at'];

    protected $table = 'cards';

    protected $primaryKey = 'card_id';

    protected $fillable = [
        'card_id',
        'code',
        'is_use',
        'user_id',
        'coin',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
}