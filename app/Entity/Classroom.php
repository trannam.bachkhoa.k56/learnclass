<?php

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Classroom extends Model
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $dates = ['deleted_at'];

    protected $table = 'classroom';

    protected $primaryKey = 'classroom_id';

    protected $fillable = [
        'classroom_id',
        'teacher_id',
        'product_id',
        'category_product',
        'name',
        'student_list',
        'image',
        'cost',
        'discount',
        'started_date',
        'ended_date',
        'is_approve',
        'is_opening',
        'level_learn',
        'objects',
        'recruitment',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public static function getClassrooms ($teacherId) {
        try {
            $classrooms = static::leftJoin('teacher', 'teacher.teacher_id', '=', 'classroom.teacher_id')
                ->leftJoin('users', 'users.id', 'teacher.user_id')
                ->leftJoin('products', 'products.product_id','=', 'classroom.product_id')
                ->leftJoin('posts', 'posts.post_id', '=', 'products.post_id')
                ->select(
                    'classroom.*',
                    'users.name as teacher_name',
                    'products.price',
                    'posts.slug',
                    'posts.image',
                    'posts.title'
                )
                ->where('classroom.teacher_id', $teacherId)
				->orderBy('posts.post_id', 'desc')
                ->get();

            return $classrooms;
        } catch (\Exception $e) {
            return array();
        }
    }

    public static function getClassroomsOfUser($userID) {
        try {
            $classrooms = static::leftJoin('teacher', 'teacher.teacher_id', '=', 'classroom.teacher_id')
                ->leftJoin('users', 'users.id', 'teacher.user_id')
                ->leftJoin('products', 'products.product_id','=', 'classroom.product_id')
                ->leftJoin('posts', 'posts.post_id', '=', 'products.post_id')
                ->leftJoin('classroom_student', 'classroom_student.classroom_id', 'classroom.classroom_id')
                ->select(
				'products.price',
				'products.image_list',
				'products.discount',
				'products.price_deal',
				'products.code',
				'products.product_id',
				'products.properties',
				'products.buy_together',
				'products.buy_after',
				'products.discount_start',
				'products.discount_end',
				'posts.*',
				'teacher.teacher_id',
				'users.name as user_name',
				'users.image as user_image',
				'classroom.started_time',
				'classroom.started_date',
				'classroom.number_lesson',
				'classroom.classroom_id',
				'classroom.min_student',
				'classroom.is_opening'
				)
                ->where('classroom_student.user_id', $userID)
                ->distinct()
                ->get();

            return $classrooms;
        } catch (\Exception $e) {
            return array();
        }
    }
	
	public static function getDetailClassroom($classroomID) {
		$product = static::join('products', 'products.product_id', 'classroom.product_id')
			->leftJoin('posts', 'products.post_id', 'posts.post_id')
			->leftJoin('teacher', 'teacher.teacher_id', '=', 'classroom.teacher_id')
			->leftJoin('users', 'users.id', '=', 'teacher.user_id')
			->select(
				'products.price',
				'products.image_list',
				'products.discount',
				'products.price_deal',
				'products.code',
				'products.product_id',
				'products.properties',
				'products.buy_together',
				'products.buy_after',
				'products.discount_start',
				'products.discount_end',
				'posts.*',
				'teacher.teacher_id',
				'users.name as user_name',
				'users.image as user_image',
				'classroom.started_time',
				'classroom.started_date',
				'classroom.number_lesson',
				'classroom.classroom_id',
				'classroom.is_opening'
			)
			->where('classroom.classroom_id', $classroomID)->first();
			
		return $product;
	}

	public static function startTeacherClassroomNow ($teacherId) {
        $classrooms = static::where('teacher_id', $teacherId)->get();
		$lessons = array();
		
        foreach ($classrooms as $classroom) {
            $lesson = Lesson::whereDate('date_at', Carbon::today())
                ->where('classroom_id', $classroom->classroom_id)
                ->first();


            if (!empty($lesson) ) {
                $dateEnd = $lesson->date_at.' '.$lesson->time_end;
                $timeEnd = strtotime($dateEnd);
				
				$dateStart = $lesson->date_at.' '.$lesson->time_start;
				$timeStart = strtotime($dateStart);

                 if (  ($timeStart - time()) / 60 <= 10 and ($timeStart - time()) / 60 >= 0 ) {
                    $lessons[] =  array(
                        'title' => $classroom->name,
                        'classroom_id' => $classroom->classroom_id,
                        'lesson_id' => $lesson->lesson_id,
						'message' => 'sắp bắt đầu'
                    );
                }
				
				if ( $timeStart < time() && $timeEnd > time() ) {
					$lessons[] =  array(
                        'title' => $classroom->name,
                        'classroom_id' => $classroom->classroom_id,
                        'lesson_id' => $lesson->lesson_id,
						'message' => 'đang diễn ra'
                    );
				}
            }
        }

        return $lessons;
    }

    public static function startStudentClassroomNow ($userId) {
        $classrooms = static::join('classroom_student', 'classroom_student.classroom_id', 'classroom.classroom_id')
            ->select('classroom.*')
            ->where('classroom_student.user_id', $userId)->get();
		$lessons = array();
		
        foreach ($classrooms as $classroom) {
            $lesson = Lesson::whereDate('date_at', Carbon::today())
                ->where('classroom_id', $classroom->classroom_id)
                ->first();


            if (!empty($lesson) ) {
                $dateEnd = $lesson->date_at.' '.$lesson->time_end;
                $timeEnd = strtotime($dateEnd);
				
				$dateStart = $lesson->date_at.' '.$lesson->time_start;
				$timeStart = strtotime($dateStart);

                if (  ($timeStart - time()) / 60 <= 10 and ($timeStart - time()) / 60 >= 0 ) {
                    $lessons[] = array(
                        'title' => $classroom->name,
                        'classroom_id' => $classroom->classroom_id,
                        'lesson_id' => $lesson->lesson_id,
						'message' => 'sắp bắt đầu'
                    );
                }
				
				if ( $timeStart < time() && $timeEnd > time() ) {
					$lessons[] = array(
                        'title' => $classroom->name,
                        'classroom_id' => $classroom->classroom_id,
                        'lesson_id' => $lesson->lesson_id,
						'message' => 'đang diễn ra'
                    );
				}
            }
        }

        return $lessons;
    }

    public static function startClassroom() {
        $lessons = Lesson::leftjoin('classroom', 'classroom.classroom_id', 'lesson.classroom_id')
            ->leftjoin('teacher', 'teacher.teacher_id', 'classroom.teacher_id')
            ->leftjoin('users', 'users.id', 'teacher.user_id')
            ->select(
                'classroom.name as classroom_name',
                'users.name as teacher_name',
                'lesson.title as lesson_title',
                'lesson.time_start'
            )
            ->whereDate('lesson.date_at' , Carbon::today())
            ->get();

        return $lessons;
    }

    public static function appraiseClassroom ($userId) {
        $classrooms = static::join('classroom_student', 'classroom_student.classroom_id', 'classroom.classroom_id')
        ->select('classroom.*')
        ->where('classroom_student.user_id', $userId)
        ->whereDate('ended_date', '<=', date('Y-m-d'))
        ->get();

        foreach ($classrooms as $classroom) {
            // đánh giá lớp học
            $appraiseClassroom = AppraiseClass::where('user_id', $userId)
                ->where('classroom_id', $classroom->classroom_id)
                ->first();
            
            if (empty($appraiseClassroom) ) {
                return array(
                    'title' => $classroom->name,
                    'classroom_id' => $classroom->classroom_id,
                    'teacher_id' => $classroom->teacher_id
                );

            }

        }

        return null;
    }
}
