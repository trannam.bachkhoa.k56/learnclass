<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 6/27/2018
 * Time: 2:23 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassroomStudent extends Model
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $dates = ['deleted_at'];

    protected $table = 'classroom_student';

    protected $primaryKey = 'classroom_student_id';

    protected $fillable = [
        'classroom_student_id',
        'user_id',
        'classroom_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
	
	public static function countStudent($classroomId) {
		return static::where('classroom_id', $classroomId)->count();
	}
}