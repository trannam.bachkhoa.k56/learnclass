<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailSpam extends Model
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $dates = ['deleted_at'];

    protected $table = 'email_spam';

    protected $primaryKey = 'email_spam_id';

    protected $fillable = [
        'email_spam_id',
        'email',
        'name',
        'use_dated',
        'email_days',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
}
