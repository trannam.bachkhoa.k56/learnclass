<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lesson extends Model
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $dates = ['deleted_at'];

    protected $table = 'lesson';

    protected $primaryKey = 'lesson_id';

    protected $fillable = [
        'lesson_id',
        'classroom_id',
        'title',
        'time_start',
        'time_end',
        'date_at',
        'status',
        'number_lesson',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public static function getLesson ($classroomId) {
        $lessons = static::where('classroom_id', $classroomId)
            ->orderBy('lesson_id')
            ->get();

        return $lessons;
    }
}
