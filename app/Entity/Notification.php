<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 11/29/2017
 * Time: 10:09 AM
 */

namespace App\Entity;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Notification extends Model
{

    use SoftDeletes;

    protected $softDelete = true;

    protected $dates = ['deleted_at'];

    protected $table = 'notification';

    protected $primaryKey = 'notify_id';

    protected $fillable = [
        'notify_id',
        'teacher_id',
        'classroom_id',
        'content',
        'detail',
        'url',
        'title',
        'status',
        'kind',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    public static function countReport(){
        try {
            $userId = Auth::user()->id;

            $countNotification = Teacher::join('notification', 'notification.teacher_id', 'teacher.teacher_id')
                ->join('users', 'users.id', 'teacher.user_id')
                ->where('id', $userId)
                ->where('status', 0)
                ->count();

            return $countNotification;
        } catch (\Exception $e) {
            Log::error('Entity->Notification->countReport: Lỗi lấy tổng số thông báo');

            return 0;
        }

    }

    public static function showNotify(){
        $userId = Auth::user()->id;

        $notifications = Teacher::join('notification', 'notification.teacher_id', 'teacher.teacher_id')
            ->join('users', 'users.id', 'teacher.user_id')
            ->where('id', $userId)
            ->select('notification.*')
            ->orderBy('notify_id', 'desc')
            ->get();

        return $notifications;
    }

    //thông báo
    public static function showAllNotify(){
        try {
            $notificationAlls = Teacher::join('notification','notification.teacher_id', 'teacher.teacher_id')
                ->select('notification.*')
                ->orderBy('notify_id', 'desc')
                ->get();

            $notificationModel = new Notification();
            $notificationModel
                ->where('status', 0)
                ->update([
                    'status' => 1
                ]);

        } catch (\Exception $e) {
            Log::error('http->site->notificationController->index: hiển thị tất cả thông báo.');
        } finally {
            return $notificationAlls;
        }
    }

    public static function showNotifyClass($kind, $userType){
        $userId = Auth::user()->id;

        $notifications = Teacher::join('notification', 'notification.teacher_id', 'teacher.teacher_id')
            ->join('users', 'users.id', 'teacher.user_id')
            ->where('id', $userId)
            ->where('users.user_type', $userType)
            ->where('notification.kind', $kind)
            ->select('notification.*')
            ->orderBy('created_at', 'desc')
            ->get();

        return $notifications;
    }

    public static function countNotifyDetail($kind){
        try {
            $userId = Auth::user()->id;

            $countNotification = Teacher::join('notification', 'notification.teacher_id', 'teacher.teacher_id')
                ->join('users', 'users.id', 'teacher.user_id')
                ->where('id', $userId)
                ->where('notification.kind', $kind)
                ->where('status', 0)
                ->count();

            return $countNotification;
        } catch (\Exception $e) {
            Log::error('Entity->Notification->countReport: Lỗi lấy tổng số thông báo');

            return 0;
        }

    }
}
