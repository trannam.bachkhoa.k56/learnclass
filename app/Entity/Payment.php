<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 8/14/2018
 * Time: 8:58 AM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payment';

    protected $primaryKey = 'payment_id';

    protected $fillable = [
        'payment_id',
        'user_id',
        'name',
        'email',
        'phone',
        'address',
        'created_at',
        'updated_at',
    ];

    public static function getPaymentOfUser ($userId) {
        try {

            return static::where('user_id', $userId)
                ->orderBy('payment_id', 'desc')->get();

        } catch (\Exception $e) {
            return array();
        }
    }

    public static function checkEmptyOfUser ( $userId ) {
        try {

            return static::where('user_id', $userId)
                ->orderBy('payment_id', 'desc')->count();

        } catch (\Exception $e) {
            return 0;
        }
    }
}