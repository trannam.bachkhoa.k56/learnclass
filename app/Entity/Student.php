<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Student extends Model
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $dates = ['deleted_at'];

    protected $table = 'student';

    protected $primaryKey = 'student_id';

    protected $fillable = [
        'student_id',
        'school',
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public static function insertStudent($request) {
        try {
            DB::beginTransaction();
            // insert user
            $userModel = new User();
            $userID = $userModel->insertGetId([
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'phone' => $request->input('phone'),
                'image' => $request->input('image'),
                'name' => $request->input('name'),
                'birthday' => new \Datetime($request->input('birthday')),
                'description' => $request->input('description'),
                'address' => $request->input('address'),
                'role' => 1,
                'user_type' => 'hoc_sinh',
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
            // insert teacher
            $student = new Student();
            $student->insert([
                'user_id' => $userID,
                'school' => $request->input('school'),
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Entity->Teacher->insertTeacher: lỗi thêm mới giáo viên');
        }
    }
}
