<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class StudentAppraiseTeacher extends Model
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $dates = ['deleted_at'];

    protected $table = 'student_appraise_teacher';

    protected $primaryKey = 'student_appraise_teacher_id';

    protected $fillable = [
        'student_appraise_teacher_id',
        'user_id',
        'teacher_id',
        'point',
        'history',
        'description',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public static function countAppraise($teacherId) {
        $appraiseTeachers = static::where('teacher_id', $teacherId)->get();

        $totalPoint = 0;
        foreach ($appraiseTeachers as $appraiseTeacher) {
            $totalPoint += $appraiseTeacher->point;
        }

        $countAppraise = static::where('teacher_id', $teacherId)->count();
        if ($countAppraise == 0) {
            return 0;
        }

        return (float) $totalPoint/$countAppraise;
    }

    public static function totalAppraise($teacherId) { 
        $countAppraise = static::where('teacher_id', $teacherId)->count();

        return $countAppraise;
    }

    public static function showAppraise ($teacherId) {
        $appraiseClasses = static::join('users', 'users.id', 'student_appraise_teacher.user_id')
        ->select('users.name', 'student_appraise_teacher.point', 'student_appraise_teacher.description', 'student_appraise_teacher.created_at', 'student_appraise_teacher_id')
        ->where('teacher_id', $teacherId)
        ->orderBy('student_appraise_teacher_id', 'desc')
        ->get();

        return $appraiseClasses;
    }
}
