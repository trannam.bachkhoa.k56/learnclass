<?php

namespace App\Entity;

use Faker\Provider\DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Teacher extends Model
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $dates = ['deleted_at'];

    protected $table = 'teacher';

    protected $primaryKey = 'teacher_id';

    protected $fillable = [
        'teacher_id',
        'user_id',
        'where_working',
        'academic_standard',
        'current_job',
        'teaching_field',
        'form_of_teaching',
        'teaching_subject',
        'files',
        'is_approve',
        'teaching_methods',
        'communication_skill',
        'experience',
        'numeral',
        'certification',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public static function insertTeacher($request) {
        try {
            DB::beginTransaction();
            // insert user
            $userModel = new User();
            $userID = $userModel->insertGetId([
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'phone' => $request->input('phone'),
                'image' => $request->input('image'),
                'name' => $request->input('name'),
                'birthday' => new \Datetime($request->input('birthday')),
                'description' => $request->input('description'),
                'address' => $request->input('address'),
                'role' => 1,
                'user_type' => 'giao_vien',
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
            // insert teacher
            $teacherModel = new Teacher();
            $teacherModel->insert([
                'user_id' => $userID,
                'where_working' => $request->input('where_working'),
                'academic_standard' => $request->input('academic_standard'),
                'current_job' => $request->input('current_job'),
                'teaching_field' => $request->input('teaching_field'),
                'form_of_teaching' => $request->input('form_of_teaching'),
                'teaching_subject' => $request->input('teaching_subject'),
                'is_approve' => !empty($request->input('is_approve')) ? $request->input('is_approve') : 0,
                'certification' => $request->input('certification'),
                'files' => !empty($request->input('filename')) ? implode(',', $request->input('filename')) : null,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
// insert notification
            $teacherId = Teacher::where('teacher.user_id', $userID)->value('teacher_id');
            $notificationModel = new Notification();
                $notificationModel->insert([
                    'teacher_id' => $teacherId,
                    'title' => 'Đăng ký',
                    'content' => 'Đăng ký giáo viên thành công',
                    'detail' => 'Đăng ký trở thành giáo viên thành công . Vui lòng chờ Admin phê duyệt để bạn có thể bắt đầu giảng dạy . Xin cảm ơn!',
                    'status' => '0',
                    'kind' => '3',
//                    'url' => '/trang/chi-tiet-thong-bao',
                    'created_at' => new \DateTime(),
                    'updated_at' => new \DateTime()
                ]);

            DB::commit();

            return $userID;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Entity->Teacher->insertTeacher: lỗi thêm mới giáo viên');
        }
    }

    public static function updateTeacher($request) {
        try {
            DB::beginTransaction();
            $userModel = new User();
            $userID = $userModel->insertGetId([
                'email' => $request->input('email'),
//                'password' => bcrypt($request->input('password')),
                'phone' => $request->input('phone'),
                'image' => $request->input('image'),
                'name' => $request->input('name'),
                'birthday' => new \Datetime($request->input('birthday')),
                'description' => $request->input('description'),
                'address' => $request->input('address'),
                'role' => 1,
                'post_type' => 'giao_vien',
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
            $teacherModel = new Teacher();
            $teacherModel->insert([
                'user_id' => $userID,
                'where_working' => $request->input('where_working'),
                'academic_standard' => $request->input('academic_standard'),
                'current_job' => $request->input('current_job'),
                'teaching_field' => $request->input('teaching_field'),
                'form_of_teaching' => $request->input('form_of_teaching'),
                'teaching_subject' => $request->input('teaching_subject'),
                'files' => !empty($request->input('filename')) ? implode(',', $request->input('filename')) : null,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Entity->Teacher->insertTeacher: lỗi thêm mới giáo viên');
        }
    }

    public static function detailTeacher($userId) {
        try {
            $teacher = static::where('user_id', $userId)->first();

            return $teacher;
        } catch (\Exception $e) {
            return null;
        }
    }

    public static function getTeacherIndex () {
        try {
            $teachers = static::join('users', 'users.id', '=', 'teacher.user_id')
                ->select(
                    'users.*',
                    'teacher.current_job',
                    'teacher.teacher_id',
                    'teacher.academic_standard'
                )
                ->inRandomOrder()
                ->offset(0)
                ->limit(1000)
                ->get();

            return $teachers;
        } catch (\Exception $exception) {
            return array();
        }
    }
}
