<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeacherAppraiseStudent extends Model
{
    use SoftDeletes;

    protected $softDelete = true;

    protected $dates = ['deleted_at'];

    protected $table = 'teacher_appraise_student';

    protected $primaryKey = 'teacher_appraise_student_id';

    protected $fillable = [
        'teacher_appraise_student_id',
        'teacher_id',
        'student_id',
        'point',
        'description',
        'history',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
