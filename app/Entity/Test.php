<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 8/16/2018
 * Time: 3:10 PM
 */

namespace App\Entity;


use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = 'test';

    protected $primaryKey = 'test_id';

    protected $fillable = [
        'test_id',
        'name',
        'address',
        'phone',
        'created_at',
        'updated_at',
    ];
}