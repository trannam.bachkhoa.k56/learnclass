<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Agency;
use App\Entity\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;
use Validator;

class AgencyController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.agency.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.agency.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'email|required',
            'code' => 'required | unique:agency',
            'bank_information' => 'required'
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect('admin/agency/create')
                ->withErrors($validation)
                ->withInput();
        }

        $agencyModel = new Agency();
        $agencyId = $agencyModel->insertGetId([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'address' => $request->input('address'),
            'email' => $request->input('email'),
            'percent' => $request->input('percent'),
            'bank_information' => $request->input('bank_information')
        ]);

        // insert slug
        $agencyExist = $agencyModel->where('code', $request->input('code'))->first();
        if (empty($agencyExist)) {
            $agencyModel->where('agency_id', '=', $agencyId)
                ->update([
                    'code' => $request->input('code')
                ]);
        } else {
            $agencyModel->where('agency_id', '=', $agencyId)
                ->update([
                    'code' => $request->input('code').'-'.$agencyId
                ]);
        }

        return redirect(route('agency.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function show(Agency $agency)
    {
        return redirect(route('agency.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function edit(Agency $agency)
    {
        return view('admin.agency.edit', compact('agency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agency $agency)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'email|required',
            'code' => Rule::unique('agency')->ignore($agency->code, 'code'),
            'bank_information' => 'required'
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect(route('agency.edit', compact('$agency')))
                ->withErrors($validation)
                ->withInput();
        }

        $agency->update([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'address' => $request->input('address'),
            'email' => $request->input('email'),
            'percent' => $request->input('percent'),
            'bank_information' => $request->input('bank_information')
        ]);

        // insert slug
        $agencyModel = new Agency();
        $agencyExist = $agencyModel->where('agency_id', '!=' , $agency->agency_id)
            ->where('code', $request->input('code'))->first();
        if (empty($agencyExist)) {
            $agency->update([
                    'code' => $request->input('code')
                ]);
        } else {
            $agency->update([
                    'code' => $request->input('code').'-'.$agency->agency_id
                ]);
        }

        return redirect(route('agency.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agency $agency)
    {
        $agency->delete();
        return redirect(route('agency.index'));
    }

    public function anyDatatables() {
        $agencies = Agency::select(
            'agency_id',
            'name',
            'address',
            'phone',
            'email',
            'percent',
            'code',
            'bank_information'
        );

        return Datatables::of($agencies)
            ->addColumn('action', function($agency) {
                $string =  '<a href="'.route('agency.edit', ['agency_id' => $agency->agency_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('agency.destroy', ['agency_id' => $agency->agency_id]).'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->make(true);
    }
}
