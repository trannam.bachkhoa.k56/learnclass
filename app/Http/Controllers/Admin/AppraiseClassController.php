<?php

namespace App\Http\Controllers\Admin;

use App\Entity\AppraiseClass;
use App\Entity\Classroom;
use App\Entity\Student;
use Illuminate\Http\Request;
use App\Entity\User;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Validation\Rule;
use Validator;

class AppraiseClassController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.appraise_class.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = User::get();
        $classrooms = Classroom::get();

        return view ('admin.appraise_class.add', compact('students', 'classrooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $appraiseClassroom = new AppraiseClass();
        $appraiseClassroom->insert([
            'user_id' => $request->input('student_id'),
            'classroom_id' => $request->input('classroom_id'),
            'point' => $request->input('point'),
            'created_at' => new \DateTime($request->input('created_at')),
            'description' => $request->input('description'),
        ]);

        return redirect(route('appraise-class.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\AppraiseClass  $appraiseClass
     * @return \Illuminate\Http\Response
     */
    public function show(AppraiseClass $appraiseClass)
    {
        return redirect(route('appraise-class.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\AppraiseClass  $appraiseClass
     * @return \Illuminate\Http\Response
     */
    public function edit(AppraiseClass $appraiseClass)
    {
        $students = User::get();
        $classrooms = Classroom::get();

        return view ('admin.appraise_class.edit', compact('students', 'classrooms', 'appraiseClass'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\AppraiseClass  $appraiseClass
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppraiseClass $appraiseClass)
    {
        $appraiseClass->update([
            'user_id' => $request->input('student_id'),
            'classroom_id' => $request->input('classroom_id'),
            'point' => $request->input('point'),
            'created_at' => new \DateTime($request->input('created_at')),
            'description' => $request->input('description'),
        ]);

        return redirect(route('appraise-class.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\AppraiseClass  $appraiseClass
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppraiseClass $appraiseClass)
    {
        $appraiseClass->delete();

        return redirect(route('appraise-class.index'));
    }

    public function anyDatatables() {
        $appraiseClassrooms = AppraiseClass::join('users as user_student', 'appraise_class.user_id', 'user_student.id')
            ->join('classroom', 'appraise_class.classroom_id', 'classroom.classroom_id')
            ->select(
                'appraise_class.*',
                'classroom.name as classroom_name',
                'user_student.name as student_name'
            );

        return Datatables::of($appraiseClassrooms)
            ->addColumn('action', function($appraiseClassroom) {
                $string =  '<a href="'.route('appraise-class.edit', ['appraise_class_id' => $appraiseClassroom->appraise_class_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('appraise-class.destroy', ['appraise_class_id' => $appraiseClassroom->appraise_class_id]).'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->orderColumn('appraise_class.appraise_class_id', 'appraise_class.appraise_class_id desc')
            ->make(true);
    }
}
