<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Card;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class CardController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.card.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // random card
        $randomString = $this->generateRandomString(5);
        $randomCode = bcrypt(time().$randomString);

        return view('admin.card.add', compact('randomCode'));
    }

    private function  generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Card::insert([
            'code' => $request->input('code'),
            'coin' =>  !empty($request->input('coin')) ? str_replace(".", "", $request->input('coin')) : 0,
            'is_use' => $request->input('is_use'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return redirect(route('cards.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $card = Card::where('card_id', $id)->first();

        return view ('admin.card.edit', compact('card'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Card::where('card_id', $id)->update([
            'code' => $request->input('code'),
            'is_use' => $request->input('is_use'),
            'coin' => !empty($request->input('coin')) ? str_replace(".", "", $request->input('coin')) : 0,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return redirect(route('cards.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Card::where('card_id', $id)->delete();

        return redirect(route('cards.index'));
    }

    public function anyDatatables(Request $request) {
        $cards = Card::select(
            'cards.card_id',
            'cards.code',
            'cards.coin',
            'cards.user_id',
            'cards.is_use'
        );

        return Datatables::of($cards)
            ->addColumn('action', function($card) {
                $string =  '<a href="'.route('cards.edit', ['card_id' => $card->card_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('cards.destroy', ['card_id' => $card->card_id]).'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->orderColumn('cards.card_id', 'cards.card_id desc')
            ->make(true);
    }
}
