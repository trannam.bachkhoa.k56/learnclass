<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Category;
use App\Entity\CategoryPost;
use App\Entity\Classroom;
use App\Entity\ClassroomStudent;
use App\Entity\Lesson;
use App\Entity\Notification;
use App\Entity\Post;
use App\Entity\Product;
use App\Entity\Student;
use App\Entity\StudentAppraiseTeacher;
use App\Entity\Teacher;
use App\Ultility\NotificationMobile;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use App\Entity\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use kcfinder\session;
use Yajra\Datatables\Datatables;
use Illuminate\Validation\Rule;
use Validator;

class ClassroomController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view ('admin.classroom.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teachers = Teacher::join('users', 'users.id', '=', 'teacher.user_id')
            ->select('teacher.teacher_id', 'users.name')
            ->get();

        $users = user::get();

        $category = new Category();
        $categories = $category->getCategory('product');

        return view ('admin.classroom.add', compact('teachers', 'users', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect('admin/classroom/create')
                ->withErrors($validation)
                ->withInput();
        }

        $slug = $request->input('slug');
        if (empty($slug)) {
            $slug = Ultility::createSlug($request->input('name'));
        }

        if (!empty($request->input('parents'))) {
            $categoriParents = Category::whereIn('category_id', $request->input('parents'))->get();
            $categories = array();
            foreach ($categoriParents as $cate) {
                $categories[] =  $cate->title;
            }
        }

        $postModel = new Post();
        $postID = $postModel->insertGetId([
            'description' => $request->input('description'),
            'content' => $request->input('content'),
            'image' => $request->input('image'),
            'title' => $request->input('name'),
            'category_string' => !empty($categories) ? implode(',', $categories) : '',
            'post_type' => 'product'
        ]);

        $productModel = new Product();
        $productID = $productModel->insertGetId([
            'post_id' => $postID,
            'code' => $request->input('code'),
            'price' => !empty($request->input('price')) ? str_replace(".", "", $request->input('price')) : 0,
            'discount' => !empty($request->input('discount')) ? str_replace(".", "", $request->input('discount')) : 0,
        ]);

        $classroomModel = new Classroom();
        $classroomId = $classroomModel->insertGetId([
            'product_id' => $productID,
            'teacher_id' => $request->input('teacher_id'),
            'name' => $request->input('name'),
            'student_list' => !empty($request->input('student_list')) ? implode(',', $request->input('student_list')) : '',
            'files' => !empty($request->input('filename')) ? implode(',', $request->input('filename')) : null,
            'started_date' => new \DateTime($request->input('date_start')),
            'ended_date' => new \DateTime($request->input('date_end')),
            'is_approve' => $request->input('is_approve'),
        ]);

        // insert danh mục cha
        $categoryPost = new CategoryPost();
        if (!empty($request->input('parents'))) {
            foreach ($request->input('parents') as $parent) {
                $categoryPost->insert([
                    'category_id' => $parent,
                    'post_id' => $postID,
                ]);
            }
        }


        $teacherId = $request->input('teacher_id');
        $is_approve = $request->input('is_approve');
        $notificationModel = new Notification();
        if ($is_approve == 1){
            $notificationModel->insert([
                'classroom_id' => $classroomId,
                'teacher_id' => $teacherId,
                'title' => 'Phê duyệt',
                'content' => 'Lớp học của bạn đã được phê duyệt, bạn có thể đăng ký buổi học ngay bây giờ',
                'detail' => $request->input('approve'),
                'status' => '0',
                'kind' => '3',
//                'url' => route('notificationClassroom'),
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
        }
        else{
            $notificationModel->insert([
                'classroom_id' => $classroomId,
                'teacher_id' => $teacherId,
                'title' => 'Phê duyệt',
                'content' => 'Lớp học của bạn không đủ điều kiện để tham gia làm giáo viên, bấm xem chi tiết để bổ sung các thông tin cần thiết nhé!',
                'status' => '0',
                'detail' => $request->input('not_approve'),
                'kind' => '3',
//                'url' => route('notificationClassroom'),
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
        }

        // insert lớp học và học sinh
        $studentLists = $request->input('student_list');
        $classroomStudentModel = new ClassroomStudent();
        if (!empty($studentLists)) {
            foreach ($studentLists as $studentId) {
                $classroomStudentModel->insert([
                    'user_id' => $studentId,
                    'classroom_id' => $classroomId
                ]);
            }
        }

        // insert slug
        $postWithSlug = $postModel->where('slug', $slug)->first();
        if (empty($postWithSlug)) {
            $postModel->where('post_id', '=', $postID)
                ->update([
                    'slug' => $slug
                ]);
        } else {
            $postModel->where('post_id', '=', $postID)
                ->update([
                    'slug' => $slug.'-'.$postID
                ]);
        }


        return redirect(route('classroom.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Classroom  $classroom
     * @return \Illuminate\Http\Response
     */
    public function show(Classroom $classroom)
    {


        return redirect(route('classroom.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Classroom  $classroom
     * @return \Illuminate\Http\Response
     */
    public function edit(Classroom $classroom)
    {
        $teachers = Teacher::join('users', 'users.id', '=', 'teacher.user_id')
            ->select('teacher.teacher_id', 'users.name')
            ->get();

        $users = User::join('classroom_student', 'classroom_student.user_id', 'users.id')
            ->where('classroom_student.classroom_id', $classroom->classroom_id)
            ->where('classroom_student.deleted_at', null)
            ->get();

        $product = Product::leftJoin('posts', 'posts.post_id', '=', 'products.post_id')
            ->where('products.product_id', $classroom->product_id)
            ->select('products.*', 'posts.*')
            ->first();

        $allUsers = User::get();

        $lessons = Lesson::leftJoin('classroom','classroom.classroom_id', '=', 'lesson.classroom_id')
            ->leftJoin('teacher', 'teacher.teacher_id', '=', 'classroom.teacher_id')
            ->leftJoin('users', 'users.id', '=', 'teacher.user_id')
            ->select('classroom.name as classroom_name',
                'lesson.*',
                'users.name as teacher_name')
            ->where('lesson.classroom_id', '=', $classroom->classroom_id)
            ->get();

        $categoryPosts = CategoryPost::where('post_id', $product->post_id)->get();
        $categoryPost = array();

        foreach($categoryPosts as $cate ) {
            $categoryPost[] = $cate->category_id;
        }

        $category = new Category();
        $categories = $category->getCategory('product');

        return view ('admin.classroom.edit', compact('classroom', 'users', 'teachers', 'product', 'allUsers', 'lessons', 'categories', 'categoryPost'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Classroom  $classroom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Classroom $classroom)
    {
        $slug = $request->input('slug');
        if (empty($slug)) {
            $slug = Ultility::createSlug($request->input('name'));
        }

        if (!empty($request->input('parents'))) {
            $categoriParents = Category::whereIn('category_id', $request->input('parents'))->get();
            $categories = array();
            foreach ($categoriParents as $cate) {
                $categories[] =  $cate->title;
            }
        }

        $product = Product::where('product_id', $classroom->product_id)->first();
        $postModel = new Post();
        $post = Post::where('post_id','=', $product->post_id)->first();
        $post->update([
            'description' => $request->input('description'),
            'content' => $request->input('content'),
            'image' => $request->input('image'),
            'title' => $request->input('name'),
            'category_string' => !empty($categories) ? implode(',', $categories) : '',
        ]);

        // insert slug
        // insert slug
        $postWithSlug = Post::where('slug', $slug)
            ->where('post_id', '!=', $post->post_id)
            ->first();
        if (empty($postWithSlug)) {
            $postModel->where('post_id', '=', $post->post_id)
                ->update([
                    'slug' => $slug
                ]);
        } else {
            $postModel->where('post_id', '=',  $post->post_id)
                ->update([
                    'slug' => $slug.'-'.$post->post_id
                ]);
        }

        // insert danh mục cha
        $categoryPost = new CategoryPost();
        if (!empty($request->input('parents'))) {
            foreach ($request->input('parents') as $parent) {
                $categoryPost->insert([
                    'category_id' => $parent,
                    'post_id' => $post->post_id,
                ]);
            }
        }

        $productId = Product::where('product_id', $classroom->product_id)->first();
        $productId->update([
            'code' => $request->input('code'),
            'price' => $request->input('price'),
            'discount' => $request->input('discount'),
        ]);

        $classroom->update([
            'teacher_id' => $request->input('teacher_id'),
            'name' => $request->input('name'),
            'files' => !empty($request->input('filename')) ? implode(',', $request->input('filename')) : null,
            'started_date' => new \DateTime($request->input('started_date')),
            'ended_date' => new \DateTime($request->input('ended_date')),
            'is_approve' => $request->input('is_approve'),
            'category_product' => !empty($request->input('cate_product')) ? implode(',', $request->input('cate_product')) : '',
        ]);

        $teacherId = $request->input('teacher_id');
        $is_approve = $request->input('is_approve');
        $userMobile = User::join('teacher', 'teacher.user_id', 'users.id')
            ->select('users.token_mobile')
            ->where('teacher.teacher_id', $teacherId)
            ->first();

        $notificationModel = new Notification();
        if ($is_approve == 1){
            $tokenMobile = $userMobile->token_mobile;
            NotificationMobile::pushNotification($tokenMobile, 'Phê duyệt lớp học', 'Lớp học của bạn đã được phê duyệt, bạn có thể đăng ký buổi học ngay bây giờ');

            $notificationModel->insert([
                'teacher_id' => $teacherId,
                'classroom_id' => $classroom->classroom_id,
                'title' => 'Phê duyệt',
                'content' => 'Lớp học của bạn đã được phê duyệt, bạn có thể đăng ký buổi học ngay bây giờ',
                'detail' => $request->input('approve'),
                'status' => '0',
                'kind' => '3',
//                'url' => route('notificationClassroom'),
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
        }
        else{
            $tokenMobile = $userMobile->token_mobile;
            NotificationMobile::pushNotification($tokenMobile, 'Không phê duyệt lớp học', 'Lớp học của bạn không đủ điều kiện để tham gia làm giáo viên, bấm xem chi tiết để bổ sung các thông tin cần thiết nhé!');

            $notificationModel->insert([
                'teacher_id' => $teacherId,
                'classroom_id' => $classroom->classroom_id,
                'title' => 'Phê duyệt',
                'content' => 'Lớp học của bạn không đủ điều kiện để tham gia làm giáo viên, bấm xem chi tiết để bổ sung các thông tin cần thiết nhé!',
                'detail' => $request->input('not_approve'),
                'status' => '0',
                'kind' => '3',
//                'url' => route('notificationClassroom'),
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
        }

        return redirect(route('classroom.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Classroom  $classroom
     * @return \Illuminate\Http\Response
     */
    public function destroy(Classroom $classroom)
    {
        $product = Product::where('product_id', $classroom->product_id)->first();

        Post::where('post_id', $product->post_id)->delete();

        $product->delete();
        $classroom->delete();
        return redirect(route('classroom.index'));
    }

    public function anyDatatables() {
        $classrooms = Classroom::leftJoin('teacher', 'teacher.teacher_id', '=', 'classroom.teacher_id')
            ->leftJoin('users', 'users.id', 'teacher.user_id')
            ->leftJoin('products', 'products.product_id','=', 'classroom.product_id')
            ->leftJoin('posts', 'posts.post_id', 'products.post_id')
            ->select(
                'classroom.*',
                'users.name as teacher_name',
                'products.price',
                'posts.category_string'
            )
        ->where('classroom.deleted_at', null)
        ->where('products.deleted_at', null)
        ->where('products.deleted_at', null);

        return Datatables::of($classrooms)
            ->addColumn('action', function($classroom) {
                $string =  '<a href="'.route('classroom.edit', ['classroom_id' => $classroom->classroom_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('classroom.destroy', ['classroom_id' => $classroom->classroom_id]).'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->orderColumn('classroom.classroom_id', 'classroom.classroom_id desc')
            ->make(true);
    }

    public function addStudentClassroom(Request $request) {
        $studentLists = $request->input('student_list');
        $classroomStudentModel = new ClassroomStudent();
        if (!empty($studentLists)) {
            foreach ($studentLists as $studentId) {
                $classroomStudentModel->insert([
                    'user_id' => $studentId,
                    'classroom_id' =>  $request->input('classroom_id')
                ]);
            }
        }

        return back()->withInput();
    }

    public function removeStudentClassroom($classroomId, $userId) {
        $classroomStudentModel = new ClassroomStudent();
        $classroomStudentModel->where('classroom_id', $classroomId)
            ->where('user_id', $userId)->delete();

        return back()->withInput();
    }

    public function addLessonClassroom(Request $request) {
        $classroomLessonModel = new Lesson();
		
		$classroomLessonModel->insert([
			'classroom_id' =>  $request->input('classroom_id'),
			'title' =>  $request->input('title'),
			'time_start' =>  new \Datetime($request->input('time_start')),
			'time_end' =>  new \Datetime($request->input('time_end')),
			'date_at' =>  new \Datetime($request->input('date_at')),
		]);

        return back()->withInput();
    }

    public function removeLessonClassroom($classroomId) {
        $classroomStudentModel = new Lesson();
        $classroomStudentModel->where('classroom_id', $classroomId)->delete();

        return back()->withInput();
    }
}
