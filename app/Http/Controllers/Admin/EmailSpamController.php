<?php

namespace App\Http\Controllers\Admin;

use App\Entity\EmailSpam;
use App\Entity\MailConfig;
use App\Entity\User;
use App\Mail\Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class EmailSpamController extends AdminController
{
    protected $role;
    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            return $next($request);
        });

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emailSpams = EmailSpam::orderBy('email_spam_id', 'desc')
            ->paginate(15);

        return view('admin.email_spam.list', compact('emailSpams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.email_spam.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // if slug null slug create as title
            // insert to database
            $emailSpam = new EmailSpam();
            $emailSpam->insert([
                'email' => $request->input('email'),
                'password' => $request->input('password'),
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
            ]);

        } catch (\Exception $e) {
            Log::error('http->admin->EmailSpamController->store: Lỗi thêm mới email spam');
        } finally {
            return redirect('admin/email-spam');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\EmailSpam  $emailSpam
     * @return \Illuminate\Http\Response
     */
    public function show(EmailSpam $emailSpam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\EmailSpam  $emailSpam
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailSpam $emailSpam)
    {
        return view('admin.email_spam.edit', compact('emailSpam'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\EmailSpam  $emailSpam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailSpam $emailSpam)
    {
        try {
            // insert to database
            $emailSpam->update([
                'email' => $request->input('email'),
                'password' => $request->input('password'),
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
            ]);
        } catch (\Exception $e) {
            Log::error('http->admin->EmailSpamController->update: Lỗi chỉnh sửa  email spam');
        } finally {
            return redirect('admin/email-spam');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\EmailSpam  $emailSpam
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailSpam $emailSpam)
    {
        try {
            $emailSpam->delete();
        } catch (\Exception $e) {
            Log::error('http->admin->EmailSpamController->destroy: Lỗi xóa template');
        } finally {
            return redirect('admin/email-spam');
        }
    }

    public function testEmail(Request $request) {
        $email = $request->input('email');
        $password = $request->input('password');

        $result = $this->sendMail($email, $password);

        if ($result == true) {
            return response([
                'status' => 200,
            ])->header('Content-Type', 'text/plain');
        }

        return response([
            'status' => 500,
        ])->header('Content-Type', 'text/plain');
    }

    private function sendMail($email, $password) {
        if (!empty($this->domainUser)) {
            $subject = 'Website '.$this->domainUser->name.' kiểm tra email';
        } else {
            $subject = 'Website vn3c kiểm tra email';
        }

        $content = 'Kiểm tra cài dặt thông tin email thành công. Bạn có thể sử dụng để gửi đơn hàng, hay chăm sóc khách hàng.';

        return MailConfig::sendMail('', $subject, $content, $email, $password);

    }
}
