<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Classroom;
use App\Entity\ClassroomStudent;
use App\Entity\Lesson;
use App\Entity\Teacher;
use App\Entity\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class LessonController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lessons = Lesson::leftJoin('classroom','classroom.classroom_id', '=', 'lesson.classroom_id')
            ->leftJoin('teacher', 'teacher.teacher_id', '=', 'classroom.teacher_id')
            ->leftJoin('users', 'users.id', '=', 'teacher.user_id')
            ->select('classroom.name as classroom_name', 'lesson.*', 'users.name as teacher_name')
            ->get();

        return view('admin.classroom.edit', compact('lessons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.lesson.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $learnStartEnd = $request->input('learn_start_end');
        $learnTime = explode('-', $learnStartEnd);
        $learnStart = $learnTime[0];
        $learnEnd = $learnTime[1];

        $lessonModel = new Lesson();
        $lessonModel->insert([
            'classroom_id' => $request->input('classroom_id'),
            'time_start' => new \Datetime($learnStart),
            'time_end' => new \Datetime($learnEnd),
            'title' => $request->input('title'),
        ]);

        return redirect(route('classroom.edit'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function edit(Lesson $lesson)
    {
        $classrooms = Classroom::all();

        return view('admin.lesson.edit', compact('classrooms','lesson'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lesson $lesson)
    {
        $learnStartEnd = $request->input('learn_start_end');
        $learnTime = explode('-', $learnStartEnd);
        $learnStart = $learnTime[0];
        $learnEnd = $learnTime[1];

        $lesson->update([
            'classroom_id' => $request->input('classroom'),
            'time_start' => new \Datetime($learnStart),
            'time_end' => new \Datetime($learnEnd),
            'title' => $request->input('title'),
        ]);

        return redirect(route('classroom.edit'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lesson $lesson)
    {
        $lesson->delete();

        return redirect(route('classroom.edit'));
    }
}
