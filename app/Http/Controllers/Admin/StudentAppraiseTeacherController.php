<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Student;
use App\Entity\StudentAppraiseTeacher;
use App\Entity\Teacher;
use Illuminate\Http\Request;
use App\Entity\User;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Validation\Rule;
use Validator;

class StudentAppraiseTeacherController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.student_appraise_teacher.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = User::get();

        $teachers = Teacher::join('users', 'users.id', '=', 'teacher.user_id')
            ->select('teacher.teacher_id', 'users.name')
            ->get();

        return view('admin.student_appraise_teacher.add', compact('students', 'teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $studentAppraiseTeacher = new StudentAppraiseTeacher();
        $studentAppraiseTeacher->insert([
            'user_id' => $request->input('student_id'),
            'teacher_id' => $request->input('teacher_id'),
            'point' => $request->input('point'),
            'created_at' => new \DateTime($request->input('created_at')),
            'description' => $request->input('description'),
        ]);

        return redirect(route('student-appraise-teacher.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\StudentAppraiseTeacher  $studentAppraiseTeacher
     * @return \Illuminate\Http\Response
     */
    public function show(StudentAppraiseTeacher $studentAppraiseTeacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\StudentAppraiseTeacher  $studentAppraiseTeacher
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentAppraiseTeacher $studentAppraiseTeacher)
    {
        $students = User::get();
        $teachers = Teacher::join('users', 'users.id', '=', 'teacher.user_id')
            ->select('teacher.teacher_id', 'users.name')
            ->get();

        return view('admin.student_appraise_teacher.edit', compact('students', 'teachers', 'studentAppraiseTeacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\StudentAppraiseTeacher  $studentAppraiseTeacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentAppraiseTeacher $studentAppraiseTeacher)
    {
        $studentAppraiseTeacher->update([
            'user_id' => $request->input('student_id'),
            'teacher_id' => $request->input('teacher_id'),
            'point' => $request->input('point'),
            'created_at' => new \DateTime($request->input('created_at')),
            'description' => $request->input('description'),
        ]);

        return redirect(route('student-appraise-teacher.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\StudentAppraiseTeacher  $studentAppraiseTeacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentAppraiseTeacher $studentAppraiseTeacher)
    {
        $studentAppraiseTeacher->delete();

        return redirect(route('student-appraise-teacher.index'));
    }

    public function anyDatatables() {
        $studentAppraiseTeachers = StudentAppraiseTeacher::join('teacher', 'teacher.teacher_id', '=', 'student_appraise_teacher.teacher_id')
            ->join('users', 'users.id', 'student_appraise_teacher.user_id')
            ->join('users as user_teacher', 'teacher.user_id', 'user_teacher.id')
            ->select(
                'student_appraise_teacher.*',
                'user_teacher.name as teacher_name',
                'users.name as student_name',
                'student_appraise_teacher.student_appraise_teacher_id'
            );

        return Datatables::of($studentAppraiseTeachers)
            ->addColumn('action', function($studentAppraiseTeacher) {
                $string =  '<a href="'.route('student-appraise-teacher.edit', ['student_appraise_teacher_id' => $studentAppraiseTeacher->student_appraise_teacher_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('student-appraise-teacher.destroy', ['student_appraise_teacher_id' => $studentAppraiseTeacher->student_appraise_teacher_id]).'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->orderColumn('student_appraise_teacher.student_appraise_teacher_id', 'student_appraise_teacher.student_appraise_teacher_id desc')
            ->make(true);
    }
}
