<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Student;
use Illuminate\Http\Request;
use App\Entity\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\Datatables\Datatables;

class StudentController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        return view ('admin.student.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view ('admin.student.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'required | unique:users',
            'name' => 'required',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect('admin/student/create')
                ->withErrors($validation)
                ->withInput();
        }
        Student::insertStudent($request);

        return redirect(route('student.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return redirect(route('student.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        $userModel = new User();
        $userId = $student->user_id;
        $user = $userModel->where('id', $userId)->first();

        return view ('admin.student.edit', compact('user', 'student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $validation = Validator::make($request->all(), [
            'email' =>  Rule::unique('users')->ignore($student->user_id, 'id'),
            'name' => 'required',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect(route('$student.edit', [ 'student_id' => $student->student_id ]))
                ->withErrors($validation)
                ->withInput();
        }

        $userModel = new User();
        $userModel->where('id', $student->user_id)->update([
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'image' => $request->input('image'),
            'name' => $request->input('name'),
            'birthday' => new \Datetime($request->input('birthday')),
            'description' => $request->input('description'),
            'address' => $request->input('address'),
            'role' => 1,
            'user_type' => 'hoc_sinh',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        $isChangePassword = $request->input('is_change_password');
        if ($isChangePassword == 1) {
            $userModel->where('id', $student->user_id)->update([
                'password' =>  bcrypt($request->input('password'))
            ]);
        }

        $studentModel = new Student();
        $studentModel->where('user_id', $student->user_id)->update([
            'school' => $request->input('school'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return redirect(route('student.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $userModel = new User();
        $userModel->where('id', $student->user_id)->delete();

        $studentModel = new Student();
        $studentModel->where('user_id', $student->user_id)->delete();

        return redirect(route('student.index'));
    }

    public function anyDatatables(Request $request) {
        $userModel = new User();

        $students = $userModel->where('user_type', '!=', 'giao_vien')
            ->orWhereNull('user_type');

        return Datatables::of($students)
            ->orderColumn('id', 'id desc')
            ->make(true);
    }
}
