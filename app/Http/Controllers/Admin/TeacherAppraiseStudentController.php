<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Student;
use App\Entity\Teacher;
use App\Entity\TeacherAppraiseStudent;
use Illuminate\Http\Request;
use App\Entity\User;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Validation\Rule;
use Validator;

class TeacherAppraiseStudentController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.teacher_appraise_student.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::join('users', 'users.id', '=', 'student.user_id')
            ->select('student.student_id', 'users.name')
            ->get();
        $teachers = Teacher::join('users', 'users.id', '=', 'teacher.user_id')
            ->select('teacher.teacher_id', 'users.name')
            ->get();

        return view('admin.teacher_appraise_student.add', compact('students', 'teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teacherAppraiseStudent = new TeacherAppraiseStudent();
        $teacherAppraiseStudent->insert([
            'student_id' => $request->input('student_id'),
            'teacher_id' => $request->input('teacher_id'),
            'point' => $request->input('point'),
            'history' => new \DateTime($request->input('history')),
        ]);

        return redirect(route('teacher-appraise-student.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\TeacherAppraiseStudent  $teacherAppraiseStudent
     * @return \Illuminate\Http\Response
     */
    public function show(TeacherAppraiseStudent $teacherAppraiseStudent)
    {
        return redirect(route('teacher-appraise-student.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\TeacherAppraiseStudent  $teacherAppraiseStudent
     * @return \Illuminate\Http\Response
     */
    public function edit(TeacherAppraiseStudent $teacherAppraiseStudent)
    {
        $students = Student::join('users', 'users.id', '=', 'student.user_id')
            ->select('student.student_id', 'users.name')
            ->get();
        $teachers = Teacher::join('users', 'users.id', '=', 'teacher.user_id')
            ->select('teacher.teacher_id', 'users.name')
            ->get();

        return view('admin.teacher_appraise_student.edit', compact('students', 'teachers', 'teacherAppraiseStudent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\TeacherAppraiseStudent  $teacherAppraiseStudent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TeacherAppraiseStudent $teacherAppraiseStudent)
    {
        $teacherAppraiseStudent->update([
            'student_id' => $request->input('student_id'),
            'teacher_id' => $request->input('teacher_id'),
            'point' => $request->input('point'),
            'history' => new \DateTime($request->input('history')),
        ]);

        return redirect(route('teacher-appraise-student.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\TeacherAppraiseStudent  $teacherAppraiseStudent
     * @return \Illuminate\Http\Response
     */
    public function destroy(TeacherAppraiseStudent $teacherAppraiseStudent)
    {
        $teacherAppraiseStudent->delete();

        return redirect(route('teacher-appraise-student.index'));
    }

    public function anyDatatables() {
        $teacherAppraiseStudents = TeacherAppraiseStudent::join('teacher', 'teacher.teacher_id', '=', 'teacher_appraise_student.teacher_id')
            ->join('student', 'student.student_id', 'teacher_appraise_student.student_id')
            ->join('users as user_student', 'student.user_id', 'user_student.id')
            ->join('users as user_teacher', 'teacher.user_id', 'user_teacher.id')
            ->select(
                'teacher_appraise_student.*',
                'user_teacher.name as teacher_name',
                'user_student.name as student_name'
            );

        return Datatables::of($teacherAppraiseStudents)
            ->addColumn('action', function($teacherAppraiseStudent) {
                $string =  '<a href="'.route('teacher-appraise-student.edit', ['teacher_appraise_student_id' => $teacherAppraiseStudent->teacher_appraise_student_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('teacher-appraise-student.destroy', ['teacher_appraise_student_id' => $teacherAppraiseStudent->teacher_appraise_student_id]).'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->make(true);
    }
}
