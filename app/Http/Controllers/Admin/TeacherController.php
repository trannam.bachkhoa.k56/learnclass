<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Notification;
use App\Entity\Teacher;
use App\Ultility\NotificationMobile;
use Illuminate\Http\Request;
use App\Entity\User;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Validation\Rule;
use Validator;

class TeacherController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.teacher.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.teacher.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'required | unique:users',
            'name' => 'required',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect('admin/teacher/create')
                ->withErrors($validation)
                ->withInput();
        }

        Teacher::insertTeacher($request);

        return redirect(route('teacher.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        return view('admin.teacher.edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        $userModel = new User();
        $userId = $teacher->user_id;
        $user = $userModel->where('id', $userId)->first();

        return view('admin.teacher.edit', compact('teacher', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teacher $teacher)
    {
        $validation = Validator::make($request->all(), [
            'email' =>  Rule::unique('users')->ignore($teacher->user_id, 'id'),
            'name' => 'required',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect(route('teacher.edit', [ 'teacher_id' => $teacher->teacher_id ]))
                ->withErrors($validation)
                ->withInput();
        }

        $userModel = new User();
        $userModel->where('id', $teacher->user_id)->update([
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'image' => $request->input('image'),
            'name' => $request->input('name'),
            'birthday' => new \Datetime($request->input('birthday')),
            'description' => $request->input('description'),
            'address' => $request->input('address'),
            'role' => 1,
            'user_type' => 'giao_vien',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        $isChangePassword = $request->input('is_change_password');
        if ($isChangePassword == 1) {
            $userModel->where('id', $teacher->user_id)->update([
                'password' =>  bcrypt($request->input('password'))
            ]);
        }

        $teacherModel = new Teacher();
        $teacherModel->where('user_id', $teacher->user_id)->update([
            'where_working' => $request->input('where_working'),
            'academic_standard' => $request->input('academic_standard'),
            'current_job' => $request->input('current_job'),
            'teaching_field' => $request->input('teaching_field'),
            'form_of_teaching' => $request->input('form_of_teaching'),
            'teaching_subject' => $request->input('teaching_subject'),
            'files' => $request->input('files'),
            'is_approve' => $request->input('is_approve'),
            'certification' => $request->input('certification'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        // insert notification
        $userId = User::where('id', $teacher->user_id)->value('id');
        $teacherId = Teacher::where('teacher.user_id', $userId)->value('teacher_id');
        $is_approve = $request->input('is_approve');
        $userMobile = User::join('teacher', 'teacher.user_id', 'users.id')
            ->select('users.token_mobile')
            ->where('teacher.teacher_id', $teacherId)
            ->first();

        $notificationModel = new Notification();
        if ($is_approve == 1){
            $tokenMobile = $userMobile->token_mobile;
            NotificationMobile::pushNotification($tokenMobile, 'Phê duyệt giáo viên', 'Tài khoản của bạn đã được phê duyệt giáo viên, bạn có thể tham gia giảng dạy ngay bây giờ');

            $notificationModel->insert([
                'teacher_id' => $teacherId,
                'title' => 'Phê duyệt',
                'content' => 'Tài khoản của bạn đã được phê duyệt giáo viên, bạn có thể tham gia giảng dạy ngay bây giờ',
                'detail' => $request->input('approve'),
                'status' => '0',
                'kind' => '3',
//                'url' => route('notificationClassroom'),
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
        }
        else{
            $tokenMobile = $userMobile->token_mobile;
            NotificationMobile::pushNotification($tokenMobile, 'Không phê duyệt giáo viên', 'Tài khoản của bạn không đủ điều kiện để tham gia làm giáo viên, bấm xem chi tiết để bổ sung các thông tin cần thiết nhé!');

            $notificationModel->insert([
                'teacher_id' => $teacherId,
                'title' => 'Phê duyệt',
                'content' => 'Tài khoản của bạn không đủ điều kiện để tham gia làm giáo viên, bấm xem chi tiết để bổ sung các thông tin cần thiết nhé!',
                'status' => '0',
                'detail' => $request->input('not_approve'),
                'kind' => '3',
//                'url' => route('notificationClassroom'),
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
        }

        return redirect(route('teacher.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        $userModel = new User();
        $userModel->where('id', $teacher->user_id)->delete();

        $teacherModel = new Teacher();
        $teacherModel->where('user_id', $teacher->user_id)->delete();

        return redirect(route('teacher.index'));
    }

    public function anyDatatables(Request $request) {
        $teachers = Teacher::join('users', 'users.id', '=', 'teacher.user_id')
            ->select(
                'teacher.*',
                'users.*'
            );

        return Datatables::of($teachers)
            ->addColumn('action', function($teacher) {
                $string =  '<a href="'.route('teacher.edit', ['teacher_id' => $teacher->teacher_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('teacher.destroy', ['teacher_id' => $teacher->teacher_id]).'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->orderColumn('teacher.teacher_id', 'teacher.teacher_id desc')
            ->make(true);
    }
}
