<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 8/27/2018
 * Time: 2:17 PM
 */

namespace App\Http\Controllers\Api;


use App\Entity\Category;
use App\Entity\Classroom;
use App\Entity\Input;
use App\Entity\Lesson;
use App\Entity\Post;
use App\Entity\Product;
use App\Http\Controllers\Controller;
use App\Ultility\Ultility;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class ClassroomController  extends Controller
{
    public function index (Request $request) {
        $user = JWTAuth::toUser($request->input('access_token'));

        $products = Classroom::getClassroomsOfUser($user->id);
        $countProduct = $products->count() ;

        foreach ($products as $id => $product) {
            $startedDate = date_create($product->started_date);

            $products[$id]->image = asset($product->image);
            $products[$id]->user_image = asset($product->user_image);
            $products[$id]->started_date = date_format($startedDate,"d-m-Y");
        }

        return response()->json([
            'status' => '200',
            'products' => $products,
            'countProduct' => $countProduct
        ]);

    }

    public function detail($classroomID, Request $request) {
        $user = JWTAuth::toUser($request->input('access_token'));

        $classroom = Classroom::leftJoin('teacher', 'teacher.teacher_id', '=', 'classroom.teacher_id')
            ->leftJoin('users', 'users.id', 'teacher.user_id')
            ->leftJoin('products', 'products.product_id','=', 'classroom.product_id')
            ->leftJoin('posts', 'posts.post_id', '=', 'products.post_id')
            ->leftJoin('classroom_student', 'classroom_student.classroom_id', 'classroom.classroom_id')
            ->select(
                'products.price',
                'products.image_list',
                'products.discount',
                'products.price_deal',
                'products.code',
                'products.product_id',
                'products.properties',
                'products.buy_together',
                'products.buy_after',
                'products.discount_start',
                'products.discount_end',
                'posts.*',
                'teacher.teacher_id',
                'users.name as user_name',
                'users.image as user_image',
                'classroom.started_time',
                'classroom.started_date',
                'classroom.number_lesson',
                'classroom.classroom_id',
                'classroom.min_student',
                'classroom.is_opening'
            )
            ->where('classroom.classroom_id', $classroomID)
            ->where('classroom_student.user_id', $user->id)
            ->first();

        $startedDate = date_create($classroom->started_date);
        $classroom->image = asset($classroom->image);
        $classroom->user_image = asset($classroom->user_image);
        $classroom->started_date = date_format($startedDate,"d-m-Y");;

        $lessons = Lesson::where('classroom_id', $classroomID)->get();
        foreach ($lessons as $id => $lesson) {
            $dateStart = $lesson->date_at.' '.$lesson->time_start;
            $timeStart = strtotime($dateStart);
            $dateEnd = $lesson->date_at.' '.$lesson->time_end;
            $timeEnd = strtotime($dateEnd);

            if ($timeEnd < time()) {
                $lessons[$id]->statusString = 'Đã kết thúc';
                $lessons[$id]->status = 2;
            } else if ($timeEnd >= time() && $timeStart <= time()) {
                $lessons[$id]->statusString = 'Đang diễn ra';
                $lessons[$id]->status = 1;
            } else  {
                $lessons[$id]->statusString = 'Sắp bắt đầu';
                $lessons[$id]->status = 0;
            }
        }

        if (empty($classroom)) {
            return response()->json([
                'status' => '404',
                'message' => 'Lớp học không tồn tại.'
            ]);
        }

        return response()->json([
            'status' => '200',
            'classroom' => $classroom,
            'lessons' => $lessons
        ]);
    }

    public function getAllClassroom(Request $request){
        $classroom = Classroom::leftJoin('teacher','teacher.teacher_id','=','classroom.teacher_id')
        ->leftJoin('users','users.id','=','teacher.user_id')
        ->select(
            'classroom.*',
            'users.name as user_name',
            'users.image as user_image'
        )
        ->orderBy('classroom.started_date','asc')
        ->paginate(10);
        if(empty($classroom)){
            return response()->json([
                'status' => '404',
                'message' => 'Không có lớp học nào .'
            ]);
        }
        return response()->json([
            'status' => '200',
            'classroom' => $classroom
        ]);
    }
}