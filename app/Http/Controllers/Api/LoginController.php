<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 5/7/2018
 * Time: 9:58 AM
 */

namespace App\Http\Controllers\Api;

use App\Entity\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Mail\Mail;
use App\Mail\MailResetPassword;
use App\Entity\InformationGeneral;
use Validator;

class LoginController extends Controller
{
    public function index (Request $request) {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }

    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }

    public function checkAuthUser(Request $request) {
        try {
            $user = JWTAuth::toUser($request->access_token);

            return response()->json([
                'status' => '200',
                'message' => 'Token vẫn có thể sử dụng.'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '400',
                'message' => 'token đã hết hạn sử dụng.'
            ]);
        }
    }

    public function register (Request $request) {

        $validation = Validator::make($request->all(), [
            'email' => 'required | email | unique:users',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return response()->json([
                'status' => '500',
                'message' => 'Email đăng ký không đúng hoặc email đã tồn tại.'
            ]);
        }


        if (!$request->has('name')) {
            return response()->json([
                'status' => '404',
                'message' => 'Vui lòng nhập vào tên.'
            ]);
        }

        if (!$request->has('phone')) {
            return response()->json([
                'status' => '404',
                'message' => 'Vui lòng nhập vào Số điện thoại.'
            ]);
        }

        if (!$request->has('email')) {
            return response()->json([
                'status' => '404',
                'message' => 'Vui lòng nhập vào Email.'
            ]);
        }

        if (!$request->has('password')) {
            return response()->json([
                'status' => '404',
                'message' => 'Vui lòng nhập vào mật khẩu.'
            ]);
        }

        $userModel = new User();

        $userModel->insert([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'created_at' => new \Datetime()
        ]);

        return response()->json([
            'status' => '200',
            'message' => 'Chúc mừng đã thêm mới thành công!'
        ]);
    }

    public function forgetPassword(Request $request) {
        if (!$request->has('email')) {
            return response()->json([
                'status' => '404',
                'message' => 'Vui lòng nhập email'
            ]);
        }

        $email = $request->input('email');
        $newPassword = str_random(10);

        $user = User::where('email',$email)->first();
        if (empty($user)) {
            return response()->json([
                'status' => '404',
                'message' => 'Email không phải email đăng ký tài sản.'
            ]);
        }

        $user->update([
            'password' => bcrypt($newPassword)
        ]);

        $this->sendMailForget($email, $user, $newPassword);

        return response()->json([
            'status' => '202',
            'message' => 'Mật khẩu mới của bạn đã được gửi vào mail, vui lòng truy cập mail để lấy password.'
        ]);
    }

    private function sendMailForget($email, $user, $newPassword) {
        $informationGeneralModel = new InformationGeneral();
        $informationGenerals = $informationGeneralModel->get();

        //lay theo element de show ra
        $informationElement = array();
        foreach($informationGenerals as $informationGeneral){
            $informationElement[$informationGeneral->slug] = $informationGeneral->content;
        }
                        
        $to =  $email;
        $from = 'vn3ctran@gmail.com';
        $subject ='Golkids Phòng Học Trực Tuyến Quên Mật Khẩu';

        $mail = new MailResetPassword(
            $user->name,
            $user->address,
            $user->email,
            $newPassword,
            $informationElement
        );

        \Mail::to($to)->send($mail->from($from)->subject($subject));
    }
}
