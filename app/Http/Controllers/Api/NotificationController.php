<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 9/13/2018
 * Time: 9:17 AM
 */

namespace App\Http\Controllers\Api;

use App\Entity\Classroom;
use App\Entity\Lesson;
use App\Entity\Notification;
use App\Entity\Teacher;
use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Ultility\NotificationMobile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class NotificationController extends Controller
{
    public function index(Request $request) {
        try {
            $user = JWTAuth::toUser($request->input('access_token'));

            $notifications = Teacher::join('notification', 'notification.teacher_id', 'teacher.teacher_id')
                ->join('users', 'users.id', 'teacher.user_id')
                ->where('id', $user->id)
                ->select('notification.*')
                ->orderBy('notify_id', 'desc')
                ->get();

            $countNotification = Teacher::join('notification', 'notification.teacher_id', 'teacher.teacher_id')
                ->join('users', 'users.id', 'teacher.user_id')
                ->where('id',  $user->id)
                ->where('status', 0)
                ->count();

            return response()->json([
                'status' => '200',
                'notifications' => $notifications,
                'countNotification' => $countNotification
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '500',
                'message' => 'Không thể nhận được thông báo.',
            ]);
        }

    }

    public function detail($notificationId, Request $request) {
        try {
            $user = JWTAuth::toUser($request->input('access_token'));

            $classroom = Classroom::leftJoin('notification', 'notification.classroom_id', 'classroom.classroom_id')
                ->select('classroom.name', 'notification.detail')
                ->where('notify_id', $notificationId)
                ->first();

            $teacher = Teacher::leftJoin('notification', 'notification.teacher_id', 'teacher.teacher_id')
                ->select('notification.detail')
                ->where('notify_id', $notificationId)
                ->first();

            Notification::where('notify_id', $notificationId)
                ->update([
                    'status' => 2
                ]);
            return response()->json([
                'status' => '200',
                'classroom' => $classroom,
                'teacher' => $teacher,
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status' => '500',
                'message' => 'Không thể nhận được thông báo.',
            ]);
        }
    }

    public function pushNotification(Request $request) {
        try {
            $tokenMobile = $request->input('token_mobile');
            $accessToken = $request->input('access_token');

            $user = JWTAuth::toUser($accessToken);

            User::where('id', $user->id)->update([
                'token_mobile' => $tokenMobile
            ]);

            return response()->json([
                'status' => '200',
                'message' => 'Đăng ký thông báo thành công. ',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '500',
                'message' => 'Lỗi, không thể đăng ký được thông báo.',
            ]);
        }
    }

    public function joinClassroom() {
        $lessons = Lesson::whereDate('date_at', Carbon::today())
            ->get();

        foreach ($lessons as $lesson) {
            $dateStart = $lesson->date_at.' '.$lesson->time_start;
            $timeStart = strtotime($dateStart);

            if (  ($timeStart - time()) / 60 <= 10 and ($timeStart - time()) / 60 >= 0 ) {
                $userMobile = User::join('teacher', 'teacher.user_id', 'users.id')
                    ->join('classroom', 'classroom.teacher_id', 'teacher.teacher_id')
                    ->select('users.token_mobile')
                    ->where('classroom.classroom_id', $lesson->classroom_id)
                    ->first();

                $tokenMobile = $userMobile->token_mobile;
                NotificationMobile::pushNotification($tokenMobile, 'Lớp học sắp bắt đầu', 'Mau vào máy tính để vào lớp học.');

                $studentMobiles = User::join('classroom_student', 'classroom_student.user_id', 'users.id')
                    ->select('users.token_mobile')
                    ->where('classroom_student.classroom_id', $lesson->classroom_id)
                    ->get();

                foreach ($studentMobiles as $studentMobile) {
                    $tokenMobile = $studentMobile->token_mobile;
                    if (!empty($tokenMobile)) {
                        NotificationMobile::pushNotification($tokenMobile, 'Lớp học sắp bắt đầu', 'Mau vào máy tính để vào lớp học.');
                    }
                }
            }
        }

        return response()->json([
            'status' => '200',
            'message' => 'Gửi thông báo thành công.',
        ]);
    }
}