<?php

namespace App\Http\Controllers\Api;

use App\Entity\Classroom;
use App\Entity\ClassroomStudent;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;


class RegisterCourseController extends Controller
{
    public function registerCourse($classroomID, Request $request){
       try{
            $user = JWTAuth::toUser($request->input('token'));
            $classroom = Classroom::join('products','classroom.product_id','=','products.product_id')
                ->where('classroom_id',$classroomID)
                ->first();
            if(empty($classroom)){
                return response()->json([
                   'status' => '404',
                   'content' => 'Không có khóa học mà bạn đăng ký'
                ]);
            }
            // Nếu không đủ tiền thì thông báo không đủ tiền
            if($user->coint < $classroom->price){
                return response()->json([
                    'status' => '404',
                    'content' => 'Tài khoản của bạn không đủ để đăng ký khóa học này. Vui lòng '.
                                 'chọn khóa học khác hoặc nạp tiền để tiếp tục đăng ký.'
                ]);
            }

            // Check xem người dùng đã đăng ký hay chưa

            $classroomStudentExist = ClassroomStudent::where('classroom_id', $classroom->classroom_id)
                ->where('user_id', $user->id)->exists();

            // Nếu đủ tiền chưa thanh toán thì tiến hành đăng ký và trừ tài khoản
            if (!$classroomStudentExist) {
                User::where('id', $user->id)->update([
                    'coint' => $user->coint - $classroom->price
                ]);
                ClassroomStudent::insert([
                    'classroom_id' => $classroom->classroom_id,
                    'user_id' => $user->id
                ]);
                return response()->json([
                    'status' => '200',
                    'content' => 'Chúc mừng bạn đã đăng ký thành công khóa học.'
                ]);
            }
            return response()->json([
                'status' => '200',
                'content' => 'Bạn đã đăng ký khóa này trước đó rồi.'
            ]);
        }catch (\Exception $exception){
            return response()->json([
                'status' => '404',
                'content' => 'Lỗi đường truyền'
            ]);
        }
    }
}
