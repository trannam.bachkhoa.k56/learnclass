<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 8/16/2018
 * Time: 2:58 PM
 */

namespace App\Http\Controllers\Api;

use App\Entity\Test;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function getUsers () {
        $testModel = new Test();

        $tests = $testModel->orderBy('test_id', 'desc')->get();

        return response()->json([
            'tests' => $tests,
        ]);
    }

    public function store (Request $request) {

        if (!$request->has('name')) {
            return response()->json([
                'status' => '404',
                'message' => 'Vui lòng nhập vào tên.'
            ]);
        }

        if (!$request->has('phone')) {
            return response()->json([
                'status' => '404',
                'message' => 'Vui lòng nhập vào Số điện thoại.'
            ]);
        }

        if (!$request->has('email')) {
            return response()->json([
                'status' => '404',
                'message' => 'Vui lòng nhập vào Email.'
            ]);
        }

        if (!$request->has('password')) {
            return response()->json([
                'status' => '404',
                'message' => 'Vui lòng nhập vào mật khẩu.'
            ]);
        }

        $userModel = new User();

        $userModel->insert([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'password' => $request->input('email'),
            'created_at' => new \Datetime()
        ]);

        return response()->json([
            'status' => '200',
            'message' => 'Chúc mừng đã thêm mới thành công!'
        ]);
    }

    public function update (Request $request) {

        if (!$request->has('test_id')) {
            return response()->json([
                'status' => '404',
                'message' => 'Yêu cầu nhập id người dùng'
            ]);
        }

        $testModel = new Test();

        $test = $testModel->where('test_id', $request->input('test_id'))->first();

        if (empty($test)) {
            return response()->json([
                'status' => '404',
                'message' => 'Không tồn tại người dùng này'
            ]);
        }

        $test->update([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'address' => $request->input('address'),
            'updated_at' => new \Datetime()
        ]);

        return response()->json([
            'status' => '200',
            'message' => 'Chúc mừng cập nhật trường thành công!'
        ]);
    }

    public function delete(Request $request) {
        if (!$request->has('test_id')) {
            return response()->json([
                'status' => '404',
                'message' => 'Yêu cầu nhập id người dùng'
            ]);
        }

        $testModel = new Test();

        $test = $testModel
            ->where('test_id', $request->input('test_id'))
            ->first();

        if (empty($test)) {
            return response()->json([
                'status' => '404',
                'message' => 'Không tồn tại người dùng này'
            ]);
        }

        $test->delete();

        return response()->json([
            'status' => '200',
            'message' => 'xóa thành công!'
        ]);

    }

}
