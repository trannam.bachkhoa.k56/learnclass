<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 10/17/2018
 * Time: 4:20 PM
 */

namespace App\Http\Controllers\Site;

use App\Entity\Card;
use App\Entity\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CardController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }

    public function index (Request $request) {
        if (!Auth::check()) {
            $message = 'Bạn chưa đăng nhập để có thể sử dụng được chức năng này!';

            return redirect()->back()
                ->withErrors($message);
        }

        $card = Card::where('code', $request->input('code_card'))
            ->where('is_use', 0)
            ->first();

        if (!empty($card)) {
            Card::where('code', $request->input('code_card'))
                ->update([
                    'is_use' => 1,
                    'user_id' => Auth::user()->id
                ]);

            User::where('id', Auth::user()->id)->update([
                'coint' => $card->coin
            ]);

            $message = 'Bạn đã nạp thẻ thành công!';

            return view ('site.default.add_card', compact('message'));
        }

        $message = 'Mã thẻ của bạn không chính xác hoặc không tồn tại!';

        return redirect()->back()
            ->withErrors($message);
    }
}