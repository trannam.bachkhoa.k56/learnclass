<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 7/6/2018
 * Time: 10:23 AM
 */

namespace App\Http\Controllers\Site;

use App\Entity\Category;
use App\Entity\CategoryPost;
use App\Entity\Classroom;
use App\Entity\ClassroomStudent;
use App\Entity\Notification;
use App\Entity\Post;
use App\Entity\Product;
use App\Entity\Teacher;
use App\Entity\User;
use App\Ultility\Error;
use App\Ultility\Ultility;
use App\Entity\MailConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ClassroomController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }

    public function register (Request $request) {
        $user = Auth::user();

        $files = array();
        if (!empty($request->file('filename')) ) {
            foreach ($request->file('filename') as $file) {
                if (!empty($file)) {
                    $file->move('upload/files',$file->getClientOriginalName());
                    $files[] =  'upload/files/'.$file->getClientOriginalName();
                }
            }
        }

        $fileUrls = null;
        if (!empty($files)) {
            $fileUrls = implode(',', $files);
        }

        $fileName = null;
        $image = $request->file('image');
        if (!empty($image)) {
            $image->move('upload',$image->getClientOriginalName());
            $fileName =  'upload/'.$image->getClientOriginalName();
        }

        $teacher = Teacher::where('user_id', $user->id)->first();

        if (empty($slug)) {
            $slug = Ultility::createSlug($request->input('name'));
        }

        $postModel = new Post();
        $postID = $postModel->insertGetId([
            'description' => $request->input('description'),
            'content' => $request->input('content'),
            'image' => empty($fileName) ? $request->input('avatar') : $fileName,
            'title' => $request->input('name'),
            'post_type' => 'product'
        ]);

        $productModel = new Product();
        $productID = $productModel->insertGetId([
            'post_id' => $postID,
            'price' => !empty($request->input('price')) ? str_replace(".", "", $request->input('price')) : 0,
            'discount' => !empty($request->input('discount')) ? str_replace(".", "", $request->input('discount')) : 0,
            'filter' => !empty($request->input('filter')) ?  implode(',', $request->input('filter')) : null
        ]);

        $classroomModel = new Classroom();
        $classroomId = $classroomModel->insertGetId([
            'product_id' => $productID,
            'teacher_id' => $teacher->teacher_id,
            'name' => $request->input('name'),
            'number_lesson' => $request->input('number_lesson'),
            'min_student' => $request->input('min_student'),
            'is_approve' => 0,
            'files' => $fileUrls,
            'recruitment' => $request->input('recruitment'),
            'started_date' => new \DateTime($request->input('started_date')),
            'started_time' => new \DateTime($request->input('started_time'))
        ]);

        $notificationModel = new Notification();
        $notificationModel->insert([
            'classroom_id' => $classroomId,
            'teacher_id' => $teacher->teacher_id,
            'title' => 'Đăng ký',
            'content' => 'Đăng ký lớp học thành công',
            'detail' => 'Đăng ký lớp học thành công . Vui lòng chờ Admin phê duyệt để bạn có thể bắt đầu giảng dạy. Xin cảm ơn!',
            'status' => '0',
            'kind' => '3',
//            'url' => route('notificationClassroom'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        // gửi email khi đăng ký lớp
        $this->sendMail();


        // insert slug
        $postWithSlug = $postModel->where('slug', $slug)->first();
        if (empty($postWithSlug)) {
            $postModel->where('post_id', '=', $postID)
                ->update([
                    'slug' => $slug
                ]);

            return redirect(route('lesson', ['classroomSlug' => $slug, 'classroomId' => $classroomId ]) );
        } else {
            $postModel->where('post_id', '=', $postID)
                ->update([
                    'slug' => $slug.'-'.$postID
                ]);

            return redirect(route('lesson', ['classroomSlug' => $slug.'-'.$postID, 'classroomId' => $classroomId ]) );
        }


    }

    public function update (Request $request, $classroomId) {
        $classroom = Classroom::join('products', 'products.product_id', 'classroom.product_id')
            ->join('posts', 'posts.post_id', 'products.post_id')
            ->select('posts.slug', 'classroom.product_id', 'posts.post_id', 'classroom.classroom_id')
            ->where('classroom.classroom_id', $classroomId)
            ->first();

        $user = Auth::user();

        $fileNameOld = null;
        if (!empty($request->input('filenameOld'))) {
            $fileNameOld = implode(',', $request->input('filenameOld'));
        }

        $files = array();
        if (!empty($request->file('filename')) ) {
            foreach ($request->file('filename') as $file) {
                if (!empty($file)) {
                    $file->move('upload/files',$file->getClientOriginalName());
                    $files[] =  'upload/files/'.$file->getClientOriginalName();
                }
            }
        }

        $fileUrls = null;
        if (!empty($files)) {
            $fileUrls = implode(',', $files);
            if (!empty($fileNameOld)) {
                $fileUrls .= ','.$fileNameOld;
            }
        } else if (!empty($fileNameOld)) {
            $fileUrls = $fileNameOld;
        }

        $fileName = null;
        $image = $request->file('image');
        if (!empty($image)) {
            $image->move('upload',$image->getClientOriginalName());
            $fileName =  'upload/'.$image->getClientOriginalName();
        }

        $teacher = Teacher::where('user_id', $user->id)->first();

        if (empty($slug)) {
            $slug = Ultility::createSlug($request->input('name'));
        }

        $postModel = new Post();
        $postID = $postModel->where('post_id', $classroom->post_id)->update([
            'description' => $request->input('description'),
            'content' => $request->input('content'),
            'image' => empty($fileName) ? $request->input('avatar') : $fileName,
            'title' => $request->input('name'),
            'post_type' => 'product'
        ]);

        $productModel = new Product();
         $productModel->where('product_id', $classroom->product_id)->update([
            'price' => !empty($request->input('price')) ? str_replace(".", "", $request->input('price')) : 0,
            'discount' => !empty($request->input('discount')) ? str_replace(".", "", $request->input('discount')) : 0,
             'filter' => !empty($request->input('filter')) ?  implode(',', $request->input('filter')) : null
        ]);

        $classroomModel = new Classroom();
        $classroomModel->where('classroom_id', $classroom->classroom_id)->update([
            'teacher_id' => $teacher->teacher_id,
            'name' => $request->input('name'),
            'number_lesson' => $request->input('number_lesson'),
            'min_student' => $request->input('min_student'),
            'is_approve' => 0,
            'files' => $fileUrls,
            'recruitment' => $request->input('recruitment'),
            'started_date' => new \DateTime($request->input('started_date')),
            'started_time' => new \DateTime($request->input('started_time'))
        ]);

        // insert slug
        $postWithSlug = $postModel->where('slug', $slug)
            ->where('post_id', '!=', $classroom->post_id)
            ->first();
        if (empty($postWithSlug)) {
            $postModel->where('post_id', '=', $postID)
                ->update([
                    'slug' => $slug
                ]);

            return redirect(route('lesson', ['classroomSlug' => $slug, 'classroomId' => $classroom->classroom_id ]) );
        } else {
            $postModel->where('post_id', '=', $postID)
                ->update([
                    'slug' => $slug.'-'.$postID
                ]);

            return redirect(route('lesson', ['classroomSlug' => $slug.'-'.$postID, 'classroomId' => $classroom->classroom_id ]) );
        }
    }

    public function takePartInClassroom ($classroomId) {
        if (!Auth::check()) {
            return redirect('trang/dang-nhap');
        }

        $user = Auth::user();
        $classroom = Product::join('classroom', 'classroom.product_id', '=', 'products.product_id')
        ->where('classroom.classroom_id', $classroomId)->first();

        // Nếu user không có đủ tiền để thanh toán
        if ($user->coint < $classroom->price) {
            return redirect('/trang/tai-khoan-khong-du-tien-thanh-toan');
        }

        // Nếu đủ tiền thanh toán và chưa từng thanh toán
        $classroomStudentExist = ClassroomStudent::where('classroom_id', $classroom->classroom_id)
            ->where('user_id', $user->id)->exists();

        if (!$classroomStudentExist) {
            User::where('id', $user->id)->update([
                'coint' => $user->coint - $classroom->price
            ]);

            $classroomUserModel = new ClassroomStudent();
            $classroomUserModel->insert([
                'classroom_id' => $classroom->classroom_id,
                'user_id' => $user->id
            ]);
        }

        return redirect('/trang/chuc-mung-danh-ky-khoa-hoc-thanh-cong');
    }

    private function sendMail() {
        try {
            $subject =  'Đăng ký lớp học';

            $content = 'Anh (chị)'. Auth::user()->name.' Vừa đăng ký lớp học tại phòng học trực tuyến.';
            $content .= '<br> Thông tin liên hệ:';
            $content .= '<br> Số điện thoại: '. Auth::user()->phone;
            $content .= '<br> Email: '.Auth::user()->email;

            MailConfig::sendMail('', $subject, $content);

        } catch (\Exception $e) {
            return null;
        }
    }
}