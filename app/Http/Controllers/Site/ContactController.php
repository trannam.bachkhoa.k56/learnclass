<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/28/2017
 * Time: 10:07 PM
 */

namespace App\Http\Controllers\Site;

use App\Entity\Contact;
use App\Entity\Input;
use App\Entity\MailConfig;
use App\Entity\Payment;
use App\Entity\Post;
use App\Mail\Mail;
use App\Ultility\CallApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Validator;

class ContactController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }

    public function index() {

         return view($this->themeCode.'.default.contact');
    }

    public function submit(Request $request) {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect(route('sub_contact'))
                ->withErrors($validation)
                ->withInput();
        }

        //success
        $contact = new Contact();
        $contact->insert([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'address' => $request->input('address'),
            'message' => $request->input('message'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        // truyền lên getfly
        $callApi = new CallApi();
        $requestApi = (object) [
            'account' => (object) [
                "account_name" => $request->input('name'),
                "phone_office" => $request->input('phone'),
                "email" =>$request->input('email'),
                "billing_address_street" =>  $request->input('address').' '.$request->input('message'),
            ]
        ];
        $callApi->postCustomer($requestApi);

        $success = 1;
        $this->sendMainContact($request->input('name'));

        if ($request->has('is_ajax')) {
            return response([
                'status' => 200,
                'message' => 'Cảm ơn bạn đã liên hệ cho chúng tôi, chúng tôi sẽ sớm phản hồi sớm nhất.',
            ])->header('Content-Type', 'text/plain');
        }

        return view('site.default.contact', compact('activeMenu', 'success'));

    }


    private function sendMainContact($name)  {
		try {
			$subject =  'Có liên hệ mới từ website';
			$content = 'Anh '. $name.' Vừa liên hệ với bạn từ website';

			MailConfig::sendMail('', $subject, $content);
		} catch (\Exception $e) {
			return null; 
		}
        
    }

    public function payment(Request $request) {
        $contact = new Contact();
        // Chọn thanh toán cũ
        if ($request->has('payment_old')) {
            $payment = Payment::where('payment_id', $request->input('payment_old'))->first();

            //success
            $contact->insert([
                'name' => $payment->name,
                'phone' => $payment->phone,
                'email' => $payment->email,
                'address' => $payment->address,
                'message' => $request->input('message'),
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);

            $this->sendMainContact($payment->name);
            $this->getPostOrder($request, $payment->name, $payment->phone, $payment->email,$payment->address);

            return redirect('trang/thanh-toan-thanh-cong');
        }

        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect()->back()
                ->withErrors($validation)
                ->withInput();
        }

        if (Auth::check()) {
            Payment::insert([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'address' => $request->input('address'),
                'user_id' => Auth::id(),
                'created_at' => new \DateTime()
            ]);
        }

        //success
        $contact->insert([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'address' => $request->input('address'),
            'message' => $request->input('message'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        $this->sendMainContact($request->input('name'));
        $this->getPostOrder($request, $request->input('name'), $request->input('phone'), $request->input('email'), $request->input('message'));

        return redirect('trang/thanh-toan-thanh-cong');
    }

    public function getPostOrder ($request, $name, $address, $email, $phone) {

        $now = new \DateTime();
        $orderInfor = [
            'account_name' => $name,
            'account_address' => $address,
            'account_email' => $email,
            'account_phone' => $phone,
            'order_date' => date_format($now,"d/m/Y"),
            'discount' => 0,
            'discount_amount' => 0,
            'vat' => 0,
            'vat_amount' => 0,
            'transport_amount' => 0,
            'installation' => 0,
            'installation_amount' => 0,
            "amount" => 0
        ];

        $products = array();
        $products[] = (object) [
            'product_code' => 'phonghoctructuyen',
            'product_name' => 'nap tiền vào phòng học trực tuyến',
            'quantity' => '1',
            'price' =>  str_replace(".","",$request->input('message')),
            'product_sale_off' => '',
        ];

        $data = (object) [
            'order_info' => (object) $orderInfor,
            'products' => $products,
            'terms' => ['Đơn hàng được gửi tại website của phonghoctructuyen.com']
        ];

        $callApi = new CallApi();
        $return = $callApi->postOrder($data);

    }
}
