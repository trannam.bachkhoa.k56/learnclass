<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Yangqi\Htmldom\Htmldom;

class HomeController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!empty($this->domainUser)) {
            if ( strtotime($this->domainUser->end_at) < time() && ($this->emailUser != 'vn3ctran@gmail.com')) {
                return redirect(route('admin_dateline'));
            }
        }
        return view('site.default.index');
    }

    public function callback(Request $request){
        $hashSecretWord = 'MTBiZjMwNDQtMDdiMy00MTc0LTk3YjAtNzkyMDEyZmY3ODVj';
        $hashSid = '901406259';
        $hashTotal = '48.00';
        $hashOrder = $request->input('order_number');
        $StringToHash = strtoupper(md5($hashSecretWord . $hashSid . $hashOrder . $hashTotal));

        if($StringToHash != $request->input('key')){
            $result = 'Đã có lỗi xảy ra . Quá trình chuyển đổi không thành công';
            return;
        }
        $result = 'Thành công';

        echo $result;
    }
}
