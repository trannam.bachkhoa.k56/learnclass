<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 6/27/2018
 * Time: 2:06 PM
 */

namespace App\Http\Controllers\Site;

use App\Entity\Classroom;
use App\Entity\ClassroomStudent;
use App\Entity\Lesson;
use App\Entity\Student;
use App\Entity\Teacher;
use App\Entity\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LearnClassController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }

    public function index($classroomID, $lessonId) {
         try {
            $classroom = Classroom::getDetailClassroom($classroomID);
            // Kiem tra giao vien va lop hoc

            // Lấy ra giáo viên đang đăng nhập
            $teacher = Teacher::join('users', 'users.id', 'teacher.user_id')
                ->where('user_id', Auth::user()->id)->first();

			$userCounts = ClassroomStudent::where('classroom_id', $classroomID)->count();
			
			$students = ClassroomStudent::join('users', 'users.id', '=', 'classroom_student.user_id')
			->select('users.*')
			->where('classroom_id', $classroomID)->get();
			
			$lesson = Lesson::where('lesson_id', $lessonId)
                ->where('classroom_id', $classroomID)->first();

            $checkClassroom = $this->checkClassroomTeacher($classroom, $teacher, $lesson);
            if (!$checkClassroom) {
                return view('errors.permission');
            }

            // Gán giá trị là không phải giáo viên
            $isTeacher = 1;
            $isLogined = $this->loginedUser();
            if (!$isLogined) {
                return redirect('/trang/dang-nhap')
                    ->withErrors('Tài khoản của bạn đang được đăng nhập tại một nơi khác, hoặc vừa thoát ra. Nếu vừa thoát ra bạn phải chờ 5 phút để đăng nhập lại.');
            }

            return view('site.learnClass.index', compact(
			'teacher', 
			'classroom', 
			'isTeacher',
			'userCounts',
			'students',
            'lesson'
			));
			
         } catch (\Exception $e) {
             return view('errors.404');
         }
     }

	 public function screen($classroomID, $lessonId) {
		try {
            $classroom = Classroom::getDetailClassroom($classroomID);
            // Kiem tra giao vien va lop hoc

            // Lấy ra giáo viên đang đăng nhập
            $teacher = Teacher::join('users', 'users.id', 'teacher.user_id')
                ->where('user_id', Auth::user()->id)->first();

				
			$userCounts = ClassroomStudent::where('classroom_id', $classroomID)->count();

			$lesson = Lesson::where('lesson_id', $lessonId)
                ->where('classroom_id', $classroomID)->first();

            $checkClassroom = $this->checkClassroomTeacher($classroom, $teacher, $lesson);
            if (!$checkClassroom) {
                return view('errors.permission');
            }

            // Gán giá trị là không phải giáo viên
            $isTeacher = 1;

            $isLogined = $this->loginedUser();
            if (!$isLogined) {
                return redirect('/trang/dang-nhap')
                    ->withErrors('Tài khoản của bạn đang được đăng nhập tại một nơi khác, hoặc vừa thoát ra. Nếu vừa thoát ra bạn phải chờ 5 phút để đăng nhập lại.');
            }

            return view('site.learnClass.screen_teacher', compact(
			'teacher', 
			'classroom', 
			'isTeacher',
			'userCounts',
            'lesson'
			));
			
         } catch (\Exception $e) {
             return view('errors.404');
         }
	 }
    public function studentClick($classroomID, $lessonId) {
          try {
             $classroom = Classroom::getDetailClassroom($classroomID);
             // Lấy ra học sinh vừa đăng nhập
             $student = User::where('id', Auth::user()->id)->first();
             // Lấy ra giáo viên đang đăng nhập
             $teacher = Teacher::join('users', 'users.id', 'teacher.user_id')
                 ->where('teacher_id', $classroom->teacher_id)->first();

            $lesson = Lesson::where('lesson_id', $lessonId)->first();
			$userCounts = ClassroomStudent::where('classroom_id', $classroomID)->count();
			
            $checkClassroom = $this->checkClassroomStudent($classroom, $teacher, $student, $lesson);
			  
			$students = ClassroomStudent::join('users', 'users.id', '=', 'classroom_student.user_id')
			->select('users.*')
			->where('classroom_id', $classroomID)->get();

             if (!$checkClassroom) {
                 return view('errors.permission');
             }
             // Gán giá trị là không phải giáo viên
             $isTeacher = 0;

              $isLogined = $this->loginedUser();
              if (!$isLogined) {
				 
                  return redirect('/trang/dang-nhap')
                      ->withErrors('Tài khoản của bạn đang được đăng nhập tại một nơi khác, hoặc vừa thoát ra. Nếu vừa thoát ra bạn phải chờ 5 phút để đăng nhập lại.');
              }
			
              return view('site.learnClass.index', compact(
                  'isTeacher',
                  'classroom',
                  'teacher',
                  'student',
                  'students',
                  'userCounts',
                  'lesson'
                ));
          } catch (\Exception $e) {
              return view('errors.404');
          }
    }
	
	public function studentDemoClick($classroomID, $lessonId) {
          try {
             $classroom = Classroom::getDetailClassroom($classroomID);
             // Lấy ra học sinh vừa đăng nhập
             $student = User::where('id', Auth::user()->id)->first();
             // Lấy ra giáo viên đang đăng nhập
             $teacher = Teacher::join('users', 'users.id', 'teacher.user_id')
                 ->where('teacher_id', $classroom->teacher_id)->first();

            $lesson = Lesson::where('lesson_id', $lessonId)->first();

			$userCounts = ClassroomStudent::where('classroom_id', $classroomID)->count();
			
             $checkClassroom = $this->checkClassroomStudent($classroom, $teacher, $student, $lesson);
			 
			$students = ClassroomStudent::join('users', 'users.id', '=', 'classroom_student.user_id')
			->select('users.*')
			->where('classroom_id', $classroomID)->get();

             if (!$checkClassroom) {
                 return view('errors.permission');
             }
             // Gán giá trị là không phải giáo viên
             $isTeacher = 0;

            return view('site.learnClass.index_demo', compact(
			'isTeacher',
			'classroom',
			'teacher', 
			'student',
			'students',
			'userCounts',
            'lesson'
			));
          } catch (\Exception $e) {
              return view('errors.404');
          }
    }

    public function teacherOpenClass($classroomID, $lessonId) {
        $classroom = Classroom::where('classroom_id', $classroomID)->first();
        // Lấy ra giáo viên đang đăng nhập
		$teacher = Teacher::join('users', 'users.id', 'teacher.user_id')
			->where('user_id', Auth::user()->id)->first();

		$lesson = Lesson::where('lesson_id', $lessonId)->first();
		
		$checkClassroom = $this->checkClassroomTeacher($classroom, $teacher, $lesson);

        if (!$checkClassroom) {
            return response([
                'status' => 500,
                'comment_id' => 'Lớp học hoặc giáo viên không tồn tại',
            ])->header('Content-Type', 'text/plain');
        }

        $classroom->where('classroom_id', $classroomID)
		->update([
            'is_opening' => 1,
        ]);

        return response([
            'status' => 200,
            'comment_id' => 'Mở lớp thành công',
        ])->header('Content-Type', 'text/plain');
    }
    
    private function checkClassroomTeacher($classroom, $teacher, $lesson) {
        if (!Auth::check()) {
            return false;
        }
        
       // Nếu không tồn tại lớp học
        if (empty($classroom)) {
            return false;
        }

        // buổi học không tồn tại
        if (empty($lesson)) {
            return false;
        }

        // nếu không tồn tại giáo viên
        if (empty($teacher)) {
            return false;
        }


        // Nếu có nick đăng nhập là giáo viên nhưng ko phụ trách lớp học
        if ($teacher->teacher_id != $classroom->teacher_id ) {
            return false;
        }
        
        return true;
    }

    private function checkClassroomStudent($classroom, $teacher, $student, $lesson) {
        if (!Auth::check()) {
            return false;
        }
        
        if (empty($classroom)) {
			
            return false;
        }

        // buổi học không tồn tại
        if (empty($lesson)) {
		
            return false;
        }

        // nếu không tồn tại giáo viên
        if (empty($teacher)) {
			
            return false;
        }

        // nếu lớp chưa dạy thì sẽ báo lỗi
        // if ($classroom->is_opening != 1) {
			
           // return false;
        // }

        // Nếu không tồn tại học sinh
        if (empty($student)) {
		
            return false;
        }

        // kiểm tra xem học sinh có thuộc lớp học đó không
        $classroomStudentExist = ClassroomStudent::where('user_id', $student->id)
            ->where('classroom_id', $classroom->classroom_id)->Exists();

        if (!$classroomStudentExist) {
			
            return false;
        }

        return true;
    }

    private function loginedUser() {
        $user = Auth::user();
        session_start();

        if(!empty($user->last_session) 
			&& ($_SERVER['HTTP_USER_AGENT'] != $user->last_session) 
			&& (  ( time()  - strtotime($user->logined_at) ) / 60 <= 5) 
			)
		{
			exit;
            Auth::logout();

            return false;
        }

        $user->logined_at = new \DateTime();

        $user->last_session = $_SERVER['HTTP_USER_AGENT'];

        User::where('id', $user->id)
            ->update([
                'logined_at' => $user->logined_at,
                'last_session' => $user->last_session
            ]);
        return true;
    }

}
