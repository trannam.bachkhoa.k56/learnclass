<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 7/9/2018
 * Time: 8:17 AM
 */

namespace App\Http\Controllers\Site;


use App\Entity\Classroom;
use App\Entity\ClassroomStudent;
use App\Entity\Lesson;
use Illuminate\Http\Request;

class LessonController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }

    public function index($classroomSlug, $classroomId) {
        $classroom = $this->getDetailClassroom($classroomId);
        $lessons = Lesson::where('classroom_id', $classroomId)->get();

        $users = ClassroomStudent::join('users', 'users.id', '=', 'classroom_student.user_id')
            ->select('users.*')
            ->where('classroom_id', $classroomId)
			->get();

        return view('site.learnClass.lesson', compact('classroom', 'lessons', 'users'));
    }

    public function register ($classroomSlug, $classroomId) {
        $classroom = $this->getDetailClassroom($classroomId);

        return view('site.learnClass.register_lesson', compact('classroom'));
    }

    public function registerSubmit (Request $request) {
       $classroomId = $request->input('classroom_id');
       $classroom = $this->getDetailClassroom($classroomId);

       $lessonModel = new Lesson();
       $lessonModel->insert([
           'classroom_id' => $classroomId,
           'title' => $request->input('title'),
           'time_start' => new \DateTime($request->input('time_start')),
           'time_end' => new \DateTime($request->input('time_end')),
           'date_at' => new \DateTime($request->input('date_at')),
           'created_at' => new \DateTime($request->input('created_at')),
           'updated_at' => new \DateTime($request->input('updated_at')),
       ]);

       return redirect(route('lesson', ['classroomSlug' => $classroom->slug, 'classroomId' => $classroom->classroom_id ]) );
    }

    public function editLesson ($classroomId, $lessonID) {
        $classroom = $this->getDetailClassroom($classroomId);

        $lessonModel = new Lesson();
        $lesson = $lessonModel->where('classroom_id', $classroomId)
            ->where('lesson_id', $lessonID)
            ->first();

        return view('site.learnClass.edit_lesson', compact('classroom', 'lesson'));
    }

    public function update (Request $request) {
        $classroomId = $request->input('classroom_id');
        $lessonId = $request->input('lesson_id');

        $classroom = $this->getDetailClassroom($classroomId);

        $lessonModel = new Lesson();
        $lesson = $lessonModel->where('classroom_id', $classroomId)
            ->where('lesson_id', $lessonId)
            ->update([
                'title' => $request->input('title'),
                'time_start' => new \DateTime($request->input('time_start')),
                'time_end' => new \DateTime($request->input('time_end')),
                'date_at' => new \DateTime($request->input('date_at')),
                'updated_at' => new \DateTime($request->input('updated_at')),
            ]);

        return redirect(route('lesson', ['classroomSlug' => $classroom->slug, 'classroomId' => $classroom->classroom_id ]) );
    }

    public function destroy ($classroomId, $lessonID) {
        $classroom = $this->getDetailClassroom($classroomId);

        $lessonModel = new Lesson();
        $lessonModel->where('classroom_id', $classroomId)
            ->where('lesson_id', $lessonID)
            ->delete();

        return redirect(route('lesson', ['classroomSlug' => $classroom->slug, 'classroomId' => $classroom->classroom_id ]) );
    }

    private function getDetailClassroom ($classroomId) {
        $classroom = Classroom::leftJoin('products', 'products.product_id','=', 'classroom.product_id')
            ->leftJoin('posts', 'posts.post_id', '=', 'products.post_id')
            ->select(
                'classroom.*',
                'posts.title',
                'posts.description',
                'posts.content',
                'posts.image',
                'posts.slug',
                'products.price',
                'products.filter'
            )
            ->where('classroom.classroom_id', $classroomId)->first();

        return $classroom;
    }

}