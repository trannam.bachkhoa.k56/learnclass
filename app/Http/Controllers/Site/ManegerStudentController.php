<?php

namespace App\Http\Controllers\Site;

use App\Entity\Notification;
use App\Entity\Student;
use App\Ultility\NotificationMobile;
use Illuminate\Http\Request;
use App\Entity\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Validation\Rule;
use Validator;


class ManegerStudentController extends SiteController
{
//    public function __construct(){
//        parent::__construct();
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userModel = new User();

        $students = $userModel->where('user_type', '!=', 'giao_vien')
            ->orWhereNull('user_type')
        ->select(
        'users.*')
            ->orderBy('users.id','desc')
            ->paginate(15);
//        echo '<pre>';
//        print_r($students);
//        echo '</pre>';
//        exit();
        return view('site.Maneger.quan-ly-hoc-sinh',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('site.Maneger.them-moi-hoc-sinh',compact('user'));
    }
//
//    /**
//     * Store a newly created resource in storage.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @return \Illuminate\Http\Response
//     */
    public function store(Request $request)
    {

        $fileName = null;
        $image = $request->file('image');
        if (!empty($image)) {
            $image->move('upload',$image->getClientOriginalName());
            $fileName =  'upload/'.$image->getClientOriginalName();
        }


        $validation = Validator::make($request->all(), [
            'email' => 'required | unique:users',
            'name' => 'required',
        ]);
        // if validation fail return error
        if ($validation->fails())
        {
            return redirect(route('manegerstudent.create'))
                ->withErrors($validation)
                ->withInput();
        }
        try {
            DB::beginTransaction();
            // insert user
            $userModel = new User();
            $userID = $userModel->insertGetId([
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'phone' => $request->input('phone'),
                'image' => empty($fileName) ? $request->input('avatar') : $fileName,
                'name' => $request->input('name'),
                'birthday' => new \Datetime($request->input('birthday')),
                'description' => $request->input('description'),
                'address' => $request->input('address'),
                'role' => 1,
                'user_type' => 'hoc_sinh',
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
            // insert teacher
            $student = new Student();
            $student->insert([
                'user_id' => $userID,
                'school' => $request->input('school'),
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Entity->Teacher->insertTeacher: lỗi thêm mới giáo viên');
        }
        return redirect(route('manegerstudent.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
//    public function show($teacher_id)
//    {
//        $teacher = Teacher::where('teacher_id',$teacher_id)->first();
//        $userModel = new User();
//        $userModel->where('id', $teacher->user_id)->delete();
//        $teacherModel = new Teacher();
//        $teacherModel->where('user_id', $teacher->user_id)->delete();
//
//        return redirect()->back();
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit($student_id)
    {
        $student = Student::where('user_id',$student_id)->first();
        $userModel = new User();
        $userId = $student->user_id;
        $user = $userModel->where('id', $userId)->first();
        return view('site.Maneger.sua-hoc-sinh', compact('student', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $student_id)
    {
        $fileName = null;
        $image = $request->file('image');
        if (!empty($image)) {
            $image->move('upload',$image->getClientOriginalName());
            $fileName =  'upload/'.$image->getClientOriginalName();
        }
        try {
            DB::beginTransaction();
            // insert user
            $userModel = new User();
            User::where('id',$student_id)
                ->update([
                    'email' => $request->input('email'),
                    'phone' => $request->input('phone'),
                    'image' => empty($fileName) ? $request->input('avatar') : $fileName,
                    'name' => $request->input('name'),
                    'birthday' => new \Datetime($request->input('birthday')),
                    'description' => $request->input('description'),
                    'address' => $request->input('address'),
                    'role' => 1,
                    'user_type' => 'hoc_sinh',
                    'created_at' => new \DateTime(),
                    'updated_at' => new \DateTime()
                ]);
            $isChangePassword = $request->input('is_change_password');
            if ($isChangePassword == 1) {
                User::where('id',$student_id )->update([
                    'password' =>  bcrypt($request->input('password'))
                ]);
            }
            Student::where('user_id',$student_id)
                ->update([
                    'school' => $request->input('school'),
                    'created_at' => new \DateTime(),
                    'updated_at' => new \DateTime()
                ]);
            DB::commit();
            return redirect(route('manegerstudent.index'));
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Entity->Teacher->insertTeacher: lỗi thêm mới giáo viên');
        }
        return redirect(route('manegerstudent.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy($student_id)
    {

        User::where('id',$student_id)->delete();
        Student::where('user_id',$student_id)->delete();
        return redirect()->back();
    }

    public function anyDatatables(Request $request) {
        $teachers = Teacher::join('users', 'users.id', '=', 'teacher.user_id')
            ->select(
                'teacher.*',
                'users.*'
            );
        return Datatables::of($teachers)
//            ->addColumn('action', function($teacher) {
//                $string =  '<a href="'.route('ManergerTeacherController.edit', ['teacher_id' => $teacher->teacher_id]).'">
//                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
//                       </a>';
//                $string .= '<a  href="'.route('ManergerTeacherController.destroy', ['teacher_id' => $teacher->teacher_id]).'" class="btn btn-danger btnDelete"
//                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
//                               <i class="fa fa-trash-o" aria-hidden="true"></i>
//                            </a>';
//                return $string;
//            })
//            ->orderColumn('teacher.teacher_id', 'teacher.teacher_id desc')
            ->make(true);
    }
}
