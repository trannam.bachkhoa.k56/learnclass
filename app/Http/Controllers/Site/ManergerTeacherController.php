<?php

namespace App\Http\Controllers\Site;

use App\Entity\Notification;
use App\Entity\Teacher;
use App\Ultility\NotificationMobile;
use Illuminate\Http\Request;
use App\Entity\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Validation\Rule;
use Validator;


class ManergerTeacherController extends SiteController
{
//    public function __construct(){
//        parent::__construct();
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $manerger_teachers = Teacher::join('users', 'users.id', '=', 'teacher.user_id')
            ->select(
                'teacher.*',
                'users.*'
            )
            ->orderBy('users.id','desc')
            ->paginate(15);
//        echo '<pre>';
////        print_r($manerger_teachers);
////        echo '</pre>';
////        exit();
        return view('site.Maneger.quan-ly-giao-vien',compact('manerger_teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('site.Maneger.them-moi-giao-vien',compact('user'));
    }
//
//    /**
//     * Store a newly created resource in storage.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @return \Illuminate\Http\Response
//     */
    public function store(Request $request)
    {

        $fileName = null;
        $image = $request->file('image');
        if (!empty($image)) {
            $image->move('upload',$image->getClientOriginalName());
            $fileName =  'upload/'.$image->getClientOriginalName();
        }
//        echo $image;
//        print_r( $fileName);exit();

        $validation = Validator::make($request->all(), [
            'email' => 'required | unique:users',
            'name' => 'required',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect(route('manegerteacher.create'))
                ->withErrors($validation)
                ->withInput();
        }


        try {
            DB::beginTransaction();
            // insert user
            $userModel = new User();
            $userID = $userModel->insertGetId([
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'phone' => $request->input('phone'),
                'image' => empty($fileName) ? $request->input('avatar') : $fileName,
                'name' => $request->input('name'),
                'birthday' => new \Datetime($request->input('birthday')),
                'description' => $request->input('description'),
                'address' => $request->input('address'),
                'role' => 1,
                'user_type' => 'giao_vien',
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
            // insert teacher
            $teacherModel = new Teacher();
            $teacherModel->insert([
                'user_id' => $userID,
                'where_working' => $request->input('where_working'),
                'academic_standard' => $request->input('academic_standard'),
                'current_job' => $request->input('current_job'),
                'teaching_field' => $request->input('teaching_field'),
                'form_of_teaching' => $request->input('form_of_teaching'),
                'teaching_subject' => $request->input('teaching_subject'),
                'is_approve' => !empty($request->input('is_approve')) ? $request->input('is_approve') : 0,
                'certification' => $request->input('certification'),
                'files' => !empty($request->input('filename')) ? implode(',', $request->input('filename')) : null,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
// insert notification
            $teacherId = Teacher::where('teacher.user_id', $userID)->value('teacher_id');
            $notificationModel = new Notification();
            $notificationModel->insert([
                'teacher_id' => $teacherId,
                'title' => 'Đăng ký',
                'content' => 'Đăng ký giáo viên thành công',
                'detail' => 'Đăng ký trở thành giáo viên thành công . Vui lòng chờ Admin phê duyệt để bạn có thể bắt đầu giảng dạy . Xin cảm ơn!',
                'status' => '0',
                'kind' => '3',
//                    'url' => '/trang/chi-tiet-thong-bao',
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);

            DB::commit();

            return redirect(route('manegerteacher.index'));
        } catch (\Exception $e) {

            DB::rollback();
            Log::error('Entity->Teacher->insertTeacher: lỗi thêm mới giáo viên');
        }
        return redirect(route('manegerteacher.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show($teacher_id)
    {
        $teacher = Teacher::where('teacher_id',$teacher_id)->first();
        $userModel = new User();
        $userModel->where('id', $teacher->user_id)->delete();
        $teacherModel = new Teacher();
        $teacherModel->where('user_id', $teacher->user_id)->delete();

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit($teacher_id)
    {
        $teacher = Teacher::where('teacher_id',$teacher_id)->first();
        $userModel = new User();
        $userId = $teacher->user_id;
        $user = $userModel->where('id', $userId)->first();

        return view('site.Maneger.sua-giao-vien', compact('teacher', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $teacher_id)
    {
        $teacher = Teacher::where('teacher_id',$teacher_id)->first();
//        print_r($teacher);exit();
//        $validation = Validator::make($request->all(), [
//            'email' =>  Rule::unique('users')->ignore($teacher->user_id, 'id'),
//            'name' => 'required',
//        ]);
//        // if validation fail return error
//        if ($validation->fails()) {
//            return redirect(route('teacher.edit', [ 'teacher_id' => $teacher->teacher_id ]))
//                ->withErrors($validation)
//                ->withInput();
//        }
        $fileName = null;
        $image = $request->file('image');
        if (!empty($image)) {
            $image->move('upload',$image->getClientOriginalName());
            $fileName =  'upload/'.$image->getClientOriginalName();
        }

        $userModel = new User();
        $userModel->where('id', $teacher->user_id)->update([
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'image' => empty($fileName) ? $request->input('avatar') : $fileName,
            'name' => $request->input('name'),
            'birthday' => new \Datetime($request->input('birthday')),
            'description' => $request->input('description'),
            'address' => $request->input('address'),
            'role' => 1,
            'user_type' => 'giao_vien',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        $isChangePassword = $request->input('is_change_password');
        if ($isChangePassword == 1) {
            $userModel->where('id', $teacher->user_id)->update([
                'password' =>  bcrypt($request->input('password'))
            ]);
        }
        $teacherModel = new Teacher();
        $teacherModel->where('user_id', $teacher->user_id)->update([
            'where_working' => $request->input('where_working'),
            'academic_standard' => $request->input('academic_standard'),
            'current_job' => $request->input('current_job'),
            'teaching_field' => $request->input('teaching_field'),
            'form_of_teaching' => $request->input('form_of_teaching'),
            'teaching_subject' => $request->input('teaching_subject'),
            'files' => $request->input('files'),
            'is_approve' => $request->input('is_approve'),
            'certification' => $request->input('certification'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        // insert notification
        return redirect(route('manegerteacher.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy($teacher_id)
    {

        $teacher = Teacher::where('teacher_id',$teacher_id)->first();
        $userModel = new User();
        $userModel->where('id', $teacher->user_id)->delete();
        $teacherModel = new Teacher();
        $teacherModel->where('user_id', $teacher->user_id)->delete();

        return redirect()->back();
    }

    public function anyDatatables(Request $request) {
        $teachers = Teacher::join('users', 'users.id', '=', 'teacher.user_id')
            ->select(
                'teacher.*',
                'users.*'
            );
        return Datatables::of($teachers)
//            ->addColumn('action', function($teacher) {
//                $string =  '<a href="'.route('ManergerTeacherController.edit', ['teacher_id' => $teacher->teacher_id]).'">
//                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
//                       </a>';
//                $string .= '<a  href="'.route('ManergerTeacherController.destroy', ['teacher_id' => $teacher->teacher_id]).'" class="btn btn-danger btnDelete"
//                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
//                               <i class="fa fa-trash-o" aria-hidden="true"></i>
//                            </a>';
//                return $string;
//            })
//            ->orderColumn('teacher.teacher_id', 'teacher.teacher_id desc')
            ->make(true);
    }
}
