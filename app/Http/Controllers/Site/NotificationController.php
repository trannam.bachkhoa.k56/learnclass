<?php

namespace App\Http\Controllers\Site;

use App\Entity\Classroom;
use App\Entity\Notification;
use App\Entity\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends SiteController
{
    public function notificationClassroom($notifyId){
        $classroom = Classroom::leftJoin('notification', 'notification.classroom_id', 'classroom.classroom_id')
            ->select('classroom.name', 'notification.detail')
            ->where('notify_id', $notifyId)
            ->first();

        $teacher = Teacher::leftJoin('notification', 'notification.teacher_id', 'teacher.teacher_id')
            ->select('notification.detail')
            ->where('notify_id', $notifyId)
            ->first();

        return view('site.notification.notify_class', compact('classroom', 'teacher'));
    }
}
