<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/28/2017
 * Time: 9:31 AM
 */

namespace App\Http\Controllers\Site;


use App\Entity\Agency;
use App\Entity\Input;
use App\Entity\InformationGeneral;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Post;
use App\Entity\Teacher;
use App\Entity\User;
use App\Mail\Mail;
use App\Mail\MailResetPassword;
use App\Mail\Resetpassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class PersonController extends SiteController
{
     public function index() {
         if (!Auth::check()) {
             return redirect('/');
         }

         $user = Auth::user();

         return view('site.default.user', compact('user'));
     }

     public function store(Request $request) {


         
         $user = Auth::user();
         User::where('id', $user->id)->update([
             'name' => $request->input('name'),
             'phone' => $request->input('phone'),
             'address' => $request->input('address'),
             'age' => $request->input('age'),
             'email' => $request->input('email'),
             'sex' => $request->input('sex'),
             'facebook' => $request->input('facebook'),
             'description' => $request->input('description'),
             'youtube' => $request->input('youtube'),
             'other_social' => $request->input('other_social'),
             'image' => empty($fileName) ? $request->input('avatar') : $fileName
         ]);

         if ($user->user_type == 'giao_vien') {
            Teacher::where('user_id', $user->id)->update([
                'user_id' => $user->id,
                'where_working' => $request->input('where_working'),
                'academic_standard' => $request->input('academic_standard'),
                'current_job' => $request->input('current_job'),
                'teaching_field' => $request->input('teaching_field'),
                'form_of_teaching' => $request->input('form_of_teaching'),
                'teaching_subject' => $request->input('teaching_subject'),
            ]);
         }

         return redirect('/thong-tin-ca-nhan');
     }
     protected function sendMail($name, $phone, $address, $age, $email) {
         $mailConfig = Post::join('sub_post', 'sub_post.post_id', '=', 'posts.post_id')
             ->select('posts.*')
             ->where('type_sub_post_slug', 'cau-hinh-email')
             ->where('posts.slug', 'mail-thay-doi-tai-khoan')
             ->first();
          // config content
         $mailConfig->content = str_replace('[your-name]', $name, $mailConfig->content);
         $mailConfig->content = str_replace('[your-phone]', $phone, $mailConfig->content);
         $mailConfig->content = str_replace('[your-age]', $age, $mailConfig->content);
         $mailConfig->content = str_replace('[your-address]', $address, $mailConfig->content);
         $mailConfig->content = str_replace('[your-email]', $email, $mailConfig->content);
         $mailConfig->content = str_replace('[]', $email, $mailConfig->content);
         
         $inputs = Input::where('post_id', $mailConfig->post_id)->get();
         foreach ($inputs as $input) {
             $mailConfig[$input->type_input_slug] = $input->content;
             // config to, from, subject
             $mailConfig[$input->type_input_slug] = str_replace('[your-name]', $name, $mailConfig[$input->type_input_slug]);
             $mailConfig[$input->type_input_slug] = str_replace('[your-phone]', $phone,  $mailConfig[$input->type_input_slug]);
             $mailConfig[$input->type_input_slug] = str_replace('[your-age]', $age, $mailConfig[$input->type_input_slug]);
             $mailConfig[$input->type_input_slug] = str_replace('[your-address]', $address, $mailConfig[$input->type_input_slug]);
             $mailConfig[$input->type_input_slug] = str_replace('[your-email]', $email, $mailConfig[$input->type_input_slug]);
         }

         $to =  $mailConfig['to'];
         $from = $mailConfig['from'];
         $subject = $mailConfig['chu-de-(subject)'];
         $content = $mailConfig->content;
         $mail = new Mail(
            $content
         );

         \Mail::to($to)->send($mail->from($from)->subject($subject));
     }
     public function resetPassword() {
         $user = Auth::user();

         return view('site.default.reset_password', compact('user'));
     }

     public function storeResetPassword(Request $request) {

         $user = Auth::user();
         
         if ( !Hash::check($request->input('password_old'), $user->password)) {
             $faidOldPassword = "Mật khẩu cũ của bạn điền không đúng";

             return redirect('/doi-mat-khau')
                 ->with('faidOldPassword', $faidOldPassword)
                 ->withInput();
         }

         $validation = Validator::make($request->all(), [
             'password' => 'required|string|min:6|confirmed',
         ]);
         
         // if validation fail return error
         if ($validation->fails()) {
             return redirect('/doi-mat-khau')
                 ->withErrors($validation)
                 ->withInput();
         }

         User::where('id', $user->id)->update([
             'password' => bcrypt($request->input('password'))
         ]);

         return redirect('/doi-mat-khau')
             ->with('success', 'Bạn đã thay đổi mật khẩu thành công')
             ->withInput();
     }

     public function orderPerson(Request $request) {
         $phone = $request->input('phone');

         $orders = Order::orderBy('created_at')
             ->where('status', '>', 0);

         if (!empty($phone)) {
             $orders = $orders->where('shipping_phone', $phone);
         }

         if (Auth::check() && empty($phone)) {
             $user = Auth::user();
             $orders = $orders->where('user_id', $user->id);
         }

         $orders = $orders->paginate(3);
         $orders->appends(['phone' => $phone]);
         
         foreach($orders as $id => $order) {
             $orders[$id]->orderItems = OrderItem::join('products','products.product_id','=', 'order_items.product_id')
                 ->join('posts', 'products.post_id','=','posts.post_id')
                 ->select(
                     'posts.*',
                     'products.price',
                     'products.discount',
                     'order_items.*'
                 )
                 ->where('order_id', $order->order_id)
                 ->get();
         }

         return view('site.order.order_person', compact('user', 'orders'));
     }

    public function forgetPassword(Request $request) {
        $email = $request->input('email');
        $newPassword = str_random(10);

        $user = User::where('email',$email)->first();
        $isSuccess = 0;
        if (empty($user)) {
            return view('site.Auth.forget_password', compact('isSuccess'));
        }

        $user->update([
            'password' => bcrypt($newPassword)
        ]);

        $this->sendMailForget($email, $user, $newPassword);
        $isSuccess = 1;
        
        return view('site.Auth.forget_password', compact('isSuccess'));
    }

    private function sendMailForget($email, $user, $newPassword) {
		$informationGeneralModel = new InformationGeneral();
		$informationGenerals = $informationGeneralModel->get();

		//lay theo element de show ra
		$informationElement = array();
		foreach($informationGenerals as $informationGeneral){
			$informationElement[$informationGeneral->slug] = $informationGeneral->content;
		}

        $to =  $email;
        $from = 'vn3ctran@gmail.com';
        $subject ='Golkids Phòng Học Trực Tuyến Quên Mật Khẩu';

        $mail = new MailResetPassword(
            $user->name,
            $user->address,
            $user->email,
            $newPassword,
            $informationElement
        );

        \Mail::to($to)->send($mail->from($from)->subject($subject));
    }

    public function inputCode(Request $request) {
        try {
            if (!Auth::check()) {
                $message = 'Bạn chưa đăng nhập tài khoản.';

                return view('site.personal.input_code', compact('message'));
            }

            if (!empty(Auth::user()->code)) {
                $message = 'Bạn đã sử dụng một mã giới thiệu khác rồi, nên không được sử dụng nữa.';

                return view('site.personal.input_code', compact('message'));
            }

            $agency = Agency::where('code', $request->input('code'))->first();

            if (empty($agency)) {
                $message = 'Mã giới thiệu này không tồn tại, vui lòng quay lại để nhập lại mã giới thiệu khác';

                return view('site.personal.input_code', compact('message'));
            }

            User::where('id', Auth::user()->id)->update([
                'code' =>  $request->input('code'),
                'used_code' =>  0
            ]);
            $message = 'Chúc mừng bạn!, Bạn sẽ được giảm '. $agency->percent. '% cho lần thanh toán đầu tiên.';

            return view('site.personal.input_code', compact('message'));

        } catch (\Exception $e) {
            $message = "Lỗi xảy ra trong quá trình nhập mã giới thiệu";

            return view('site.personal.input_code', compact('message'));
        }
    }
}
