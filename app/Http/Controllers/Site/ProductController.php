<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/19/2017
 * Time: 10:23 AM
 */

namespace App\Http\Controllers\Site;


use App\Entity\Category;
use App\Entity\Classroom;
use App\Entity\ClassroomStudent;
use App\Entity\Input;
use App\Entity\Lesson;
use App\Entity\Post;
use App\Entity\Product;
use App\Entity\SettingOrder;
use App\Entity\Notification;
use App\Entity\Student;
use App\Entity\Teacher;
use App\Entity\User;
use App\Entity\AppraiseClass;
use App\Entity\StudentAppraiseTeacher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }

    public function index($slug_post, Request $request) {
        try {
            $product = $this->getProduct($slug_post);
            $averageRating = $product->avgRating;
            $sumRating = $product->countPositive;

            $lessons = Lesson::where('classroom_id', $product->classroom_id)
                ->orderBy('lesson_id')
                ->get();

            $numberStudent = ClassroomStudent::join('users', 'users.id', 'classroom_student.user_id')
            ->where('classroom_student.classroom_id', $product->classroom_id)->count();

            // Thông tin giáo viên đối với lớp học
            $teacher = Teacher::join('users', 'users.id', 'teacher.user_id')
                ->where('teacher.teacher_id', $product->teacher_id)->first();

            // Tổng số lớp học của giáo viên
            $totalClassroom = Classroom::where('teacher_id', $product->teacher_id)->count();

            // Tổng học sinh trong tất cả lớp học của giáo viên đó
            $totalStudentOfClassroomTeacher = ClassroomStudent::join('classroom', 'classroom.classroom_id', 'classroom_student.classroom_id')
                ->select('user_id')
                ->where('classroom.teacher_id', $product->teacher_id)
                ->get();

            // Học sinh của lớp học
            $studentOfClassrooms = ClassroomStudent::select('user_id')
                ->where('classroom_id', $product->classroom_id)
                ->get();

            // Tổng số học sinh mà giáo viên đó đã dạy
            $totalStudentOfTeacher = User::whereIn('id', $totalStudentOfClassroomTeacher)->count();

            $categories = $this->getCategories($product);

            // product seen
            $productSeen = Product::saveProductSeen($request, $product);
            //point
            $inforPoint = $this->getPoint($product);
            $point_price = $inforPoint['pointPrice'];
            $point_deal = $inforPoint['point_detal'];

            if ($product->template == 'default' || empty($product->template)) {
                return view('site.default.product', compact(
                    'product',
                    'categories',
                    'productSeen',
                    'averageRating',
                    'sumRating',
                    'point_price',
                    'point_deal',
                    'lessons',
                    'numberStudent',
                    'teacher',
                    'totalClassroom',
                    'totalStudentOfTeacher',
                    'studentOfClassrooms'
                ));
            } else {
                return view('site.template.'.$product->template, compact(
                    'product',
                    'categories',
                    'productSeen',
                    'averageRating',
                    'sumRating',
                    'point_price',
                    'point_deal',
                    'lessons',
                    'numberStudent',
                    'teacher',
                    'totalClassroom',
                    'totalStudentOfTeacher',
                    'studentOfClassrooms'
                ));
            }
        } catch (\Exception $e) {
            Log::error('http->site->ProductController->index: loi lay san pham');
            return redirect('/');
        }
    }

    private function getProduct ($slug_post) {
        try {
            $product = Post::join('products', 'products.post_id','=', 'posts.post_id')
                ->leftJoin('classroom', 'classroom.product_id', '=', 'products.product_id')
                ->leftJoin('teacher', 'teacher.teacher_id', '=', 'classroom.teacher_id')
                ->leftJoin('users', 'users.id', '=', 'teacher.user_id')
                ->select(
                    'products.price',
                    'products.image_list',
                    'products.discount',
                    'products.price_deal',
                    'products.code',
                    'products.product_id',
                    'products.properties',
                    'products.buy_together',
                    'products.buy_after',
                    'products.discount_start',
                    'products.discount_end',
                    'posts.*',
                    'teacher.teacher_id',
                    'users.name as user_name',
                    'users.image as user_image',
                    'classroom.started_time',
                    'classroom.started_date',
                    'classroom.ended_date',
                    'classroom.min_student',
                    'classroom.number_lesson',
                    'classroom.classroom_id'
                )
                ->where('post_type', 'product')
                ->where('visiable', 0)
                ->where('posts.slug', $slug_post)->first();

            $inputs = Input::where('post_id', $product->post_id)->get();
            foreach ($inputs as $input) {
                $product[$input->type_input_slug] = $input->content;
            }

            $product->update([
                'views' => ($product->views + 1)
            ]);

            return $product;
        } catch (\Exception $e) {
            Log::error('http->site->ProductController->getProduct: lỗi lấy dữ liệu sản phẩm');

            return redirect('/');
        }
    }

    private function getCategories ($product) {
        try {
            $categories = Category::join('category_post', 'categories.category_id', '=', 'category_post.category_id')
                ->select('categories.*')
                ->where('category_post.post_id', $product->post_id)->get();

            return $categories;
        } catch(\Exception $e) {
            Log::error('http->site->ProductController->getCategories: lỗi lấy dữ liệu danh mục');
            return null;
        }
    }

    private function getPoint($product) {
        try {
            $price = $product->price;
            $price_deal = $product->price_deal;

            $settingOrder = SettingOrder::first();
            if (!empty($settingOrder)) {
                $point_price = $price/$settingOrder->currency_give_point;
                $point_deal = $price_deal/$settingOrder->currency_give_point;
            } else {
                $point_price = 0;
                $point_deal = 0;
            }

            return [
                'pointPrice' => $point_price,
                'point_detal' => $point_deal
            ];

        } catch (\Exception $e) {
            Log::error('http->site->ProductController ->getPoint');

            return [
                'pointPrice' => 0,
                'point_detal' => 0
            ];
        }
    }
    public function Rating(Request $request){
        $classroomId = $request->input('classroom_id');
        $appraise = $request->input('appraise');
        $teacherId = $request->input('teacher_id');
        $appraiseTeacher = $request->input('appraise_teacher');

        $title = "Đánh giá lớp học";
        if (!Auth::check()) {
            $content = "Bạn chưa đăng nhập để có thể tham gia đánh giá lớp học";

            return view('site.partials.message', compact('title', 'content'));
        }

        $classroom = Classroom::join('classroom_student', 'classroom_student.classroom_id', 'classroom.classroom_id')
        ->where('classroom_student.user_id', Auth::user()->id)
        ->where('classroom.classroom_id', $classroomId)
        ->where('classroom.teacher_id', $teacherId)
        ->whereDate('ended_date', '<=', date('Y-m-d'))
        ->first();
        
        if (empty($classroom) ) {
            $content = "Bạn không tham gia lớp học này để có thể đánh giá";

            return view('site.partials.message', compact('title', 'content'));
        }

        $appraiseClassroom = AppraiseClass::where('user_id', Auth::user()->id)
        ->where('classroom_id', $classroomId)
        ->first();

        if (!empty($appraiseClassroom) ) {
            $content = "Bạn đã đánh giá cho lớp học này rồi, nên không thể tham gia đánh giá.";

            return view('site.partials.message', compact('title', 'content'));
        }

        $notificationModel = new Notification();
        AppraiseClass::insert([
            'user_id' => Auth::user()->id,
            'classroom_id' => $classroomId,
            'point' => $appraise,
            'description' => $request->input('description'),
            'created_at' => new \Datetime()
        ]);
        $notificationModel->insert([
            'teacher_id' => $teacherId,
            'classroom_id' => $classroom->classroom_id,
            'title' => 'Đánh giá về lớp học',
            'content' => 'Bạn '.Auth::user()->name.' đánh giá '.$appraise.' sao về lớp học '.$classroom->name.' của bạn',
            'detail' => 'Bạn '.Auth::user()->name.' đánh giá '.$appraiseTeacher.'về bạn. Nội dung: '.$request->input('description_teacher'),
            'status' => '0',
            'kind' => '3',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        StudentAppraiseTeacher::insert([
            'user_id' => Auth::user()->id,
            'teacher_id' => $teacherId,
            'point' => $appraiseTeacher,
            'description' => $request->input('description_teacher'),
            'created_at' => new \Datetime()
        ]);

        $notificationModel->insert([
            'teacher_id' => $teacherId,
            'classroom_id' => $classroom->classroom_id,
            'title' => 'Đánh giá về lớp học',
            'content' => 'Bạn '.Auth::user()->name.' đánh giá '.$appraiseTeacher.' sao về bạn',
            'detail' => 'Bạn '.Auth::user()->name.' đánh giá '.$appraiseTeacher.'về bạn. Nội dung: '.$request->input('description_teacher'),
            'status' => '0',
            'kind' => '3',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        $content = "Cảm ơn bạn đã tham gia đánh giá khóa học cho chúng tôi. ";

        return view('site.partials.message', compact('title', 'content'));
    }

    public function demoProduct($slug_post, Request $request) {
        try {

            $product = $this->getProduct($slug_post);
            $averageRating = $product->avgRating;
            $sumRating = $product->countPositive;

            $categories = $this->getCategories($product);

            // product seen
            $productSeen = Product::saveProductSeen($request, $product);
            //point
            $inforPoint = $this->getPoint($product);
            $point_price = $inforPoint['pointPrice'];
            $point_deal = $inforPoint['point_detal'];

            return view('site.template.show-product', compact('product', 'categories', 'productSeen', 'averageRating', 'sumRating', 'point_price','point_deal'));
        } catch (\Exception $e) {
            Log::error('http->site->ProductController->index: loi lay san pham');
            return redirect('/');
        }
    }
}
