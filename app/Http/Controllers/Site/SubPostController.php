<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/19/2017
 * Time: 10:25 AM
 */

namespace App\Http\Controllers\Site;


use App\Entity\Input;
use App\Entity\Post;
use App\Entity\TypeSubPost;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SubPostController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }

    public function index(Request $request, $sub_post_slug) {

        $posts = $this->getPosts($sub_post_slug, $request);

        $typeSubPost = $this->getTypeSubPost($sub_post_slug);

        if ($typeSubPost->template == 'default') {
            return view('site.default.sub_post', compact('posts'));
        } else {
            return view('site.template.'.$typeSubPost->template, compact('posts'));
        }
    }

    private function getPosts($sub_post_slug, $request) {
        try {
            $posts = Post::join('sub_post', 'sub_post.post_id', '=', 'posts.post_id')
                ->select('posts.*')
                ->where('type_sub_post_slug', $sub_post_slug);

            if (!empty($request->input('word'))) {
                $posts = $posts->where('slug', 'like', '%'.Ultility::createSlug($request->input('word')).'%');
            }
            $posts = $posts->paginate(2);

            foreach($posts as $id => $post) {
                $inputs = Input::where('post_id', $post->post_id)
                    ->get();
                foreach ($inputs as $input) {
                    $posts[$id][$input->type_input_slug] = $input->content;
                }
            }

            return $posts;
        } catch (\Exception $e) {
            Log::error('http->site->SubPostController->getPosts: lỗi lấy sản phẩm.');

            return null;
        }
    }

    private function getTypeSubPost($sub_post_slug) {
        try {
            $typeSubPost = TypeSubPost::where('slug', $sub_post_slug)->first();

            return $typeSubPost;
        } catch (\Exception $e) {
            Log::error('http->site->SubPostController->getTypeSubPost: lỗi lấy dạng bài viết.');

            return redirect('/');
        }
    }
}
