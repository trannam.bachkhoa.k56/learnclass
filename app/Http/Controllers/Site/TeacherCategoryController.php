<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 7/4/2018
 * Time: 2:04 PM
 */

namespace App\Http\Controllers\Site;


use App\Entity\Teacher;
use App\Entity\User;
use App\Ultility\Ultility;
use Illuminate\Http\Request;

class TeacherCategoryController extends SiteController
{
	public function __construct(){
        parent::__construct();
    }
	
    public function index(Request $request) {
        try {
            $teachers = Teacher::join('users', 'users.id', '=', 'teacher.user_id')
                ->select(
                    'teacher.*',
                    'users.*'
                )->where('is_approve', 1)
                ->orderBy('teacher_id', 'desc');

            $word = $request->input('word');
            $field = $request->input('field');
            $academic = $request->input('academic');

            if (!empty($word)){
                $teachers =  $teachers->where('users.name', 'like', '%'.$word.'%');
            }
            if (!empty($field)){
                $teachers =  $teachers->where('teacher.teaching_field', 'like', '%'.$field.'%');
            }
            if (!empty($academic)){
                $teachers =  $teachers->where('teacher.form_of_teaching', 'like', '%'.$academic.'%');
            }

            $teachers = $teachers->paginate(16);
            $teachers->appends(['word' => $request->input('word'), 'field' => $request->input('field'), 'academic' => $request->input('academic')]);

            return view('site.learnClass.teacher_category', compact('teachers'));

        } catch (\Exception $e) {
            return view('erros.404');
        }
    }



    public function searchAjax(Request $request) {
        $teachers = User::join('teacher', 'teacher.user_id', '=', 'users.id')
            ->select(
                'users.*',
                'teacher.*'
            );

        $word = $request->input('word');
        if (!empty($request->input('word'))){
            $teachers =  $teachers->where('users.name', 'like', '%'.$word.'%');
        }else {
            $teachers =  $teachers->orWhere('users.name', 'like', '%'.$word.'%');
        }
        $teachers = $teachers->offset(0)
            ->limit(5)->get();

        return response([
            'status' => 200,
            'products' => $teachers
        ])->header('Content-Type', 'text/plain');
    }



}