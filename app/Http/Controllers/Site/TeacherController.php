<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 7/4/2018
 * Time: 2:04 PM
 */

namespace App\Http\Controllers\Site;


use App\Entity\Classroom;
use App\Entity\ClassroomStudent;
use App\Entity\Input;
use App\Entity\Notification;
use App\Entity\Post;
use App\Entity\Teacher;
use App\Entity\User;
use App\Entity\MailConfig;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\Auth;
use App\Mail\Mail;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Http\Request;

class TeacherController extends SiteController
{
    public function index($teacherId) {
       try {
            $teacher = Teacher::join('users', 'users.id', '=', 'teacher.user_id')
                ->select(
                    'teacher.*',
                    'users.*'
                )
                ->where('teacher.teacher_id', $teacherId)
                ->first();
                $postModel = new Post();

            $products = $postModel->where('posts.post_type', 'product')
                ->join('products', 'products.post_id', '=', 'posts.post_id')
                ->leftJoin('classroom', 'classroom.product_id', '=', 'products.product_id')
                ->leftJoin('teacher', 'teacher.teacher_id', '=', 'classroom.teacher_id')
                ->leftJoin('users', 'users.id', '=', 'teacher.user_id')
                ->select(
                    'products.price',
                    'products.image_list',
                    'products.discount',
                    'products.price_deal',
                    'products.code',
                    'products.product_id',
                    'products.properties',
                    'products.buy_together',
                    'products.buy_after',
                    'products.discount_start',
                    'products.discount_end',
                    'posts.*',
                    'teacher.teacher_id',
                    'users.name as user_name',
                    'users.image as user_image',
                    'classroom.started_time',
                    'classroom.started_date',
                    'classroom.ended_date',
                    'classroom.min_student',
                    'classroom.number_lesson',
                    'classroom.classroom_id'
                )
                ->where('teacher.teacher_id', $teacherId)
                ->distinct()
                ->get();

			foreach ($products as $id => $product) {
                $inputs = Input::where('post_id', $product->post_id)->get();
                foreach ($inputs as $input) {
                    $products[$id][$input->type_input_slug] = $input->content;
                }
            }

            return view('site.learnClass.teacher_detail', compact('teacher', 'products'));
       } catch (\Exception $e) {
           return view('errors.404');
       }
    }

    public function register (Request $request) {
        // nếu chưa có tài khoản
        if (!Auth::check()) {
            $validation = Validator::make($request->all(), [
                'email' => 'required | unique:users',
                'name' => 'required',
            ]);

            // if validation fail return error
            if ($validation->fails()) {
                return redirect('trang/dang-ky-tro-thanh-giao-vien')
                    ->withErrors($validation)
                    ->withInput();
            }

            $fileName = null;
			if ($request->has('image')) {
				$image = $request->file('image');
				if (!empty($image)) {
					$image->move('upload',$image->getClientOriginalName());
					$fileName =  'upload/'.$image->getClientOriginalName();
				}
			}

            $files = array();
			if ($request->has('filename')) {
				foreach ($request->file('filename') as $file) {
					if (!empty($file)) {
						$file->move('upload/files',$file->getClientOriginalName());
						$files[] =  'upload/files/'.$file->getClientOriginalName();
					}
				}
			}
           

            $fileUrls = null;
            if (!empty($files)) {
                $fileUrls = implode(',', $files);
            }

            $userID = Teacher::insertTeacher($request);

            User::where('id', $userID)->update([
                'image' => $fileName
            ]);

            Teacher::where('user_id', $userID)->update([
                'files' => $fileUrls
            ]);

//            $notificationModel = new Notification();
//            $notificationModel->insert([
//                'teacher_id' => $teacher->teacher_id,
//                'title' => 'Đăng ký',
//                'content' => 'Đăng ký lớp học thành công',
//                'detail' => 'Đăng ký lớp học thành công . Vui lòng chờ Admin phê duyệt để bạn có thể bắt đầu giảng dạy. Xin cảm ơn!',
//                'status' => '0',
//                'kind' => '3',
////            'url' => route('notificationClassroom'),
//                'created_at' => new \DateTime(),
//                'updated_at' => new \DateTime()
//            ]);

			// gửi mail thông báo đăng ký thành giáo viên
			$this->sendMail($request);

            return redirect('/trang/dang-ky-giao-vien-thanh-cong');
        }

        // Nếu đã có tài khoản
        $userId = Auth::user()->id;
        Teacher::insert([
            'user_id' => $userId,
            'where_working' => $request->input('where_working'),
            'academic_standard' => $request->input('academic_standard'),
            'current_job' => $request->input('current_job'),
            'teaching_field' => $request->input('teaching_field'),
            'form_of_teaching' => $request->input('form_of_teaching'),
            'teaching_subject' => $request->input('teaching_subject'),
            'is_approve' => 0,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        User::where('id', $userId)->update([
            'user_type' => 'giao_vien',
        ]);

        // gửi mail thông báo đăng ký thành giáo viên
        $this->sendMail($request);

        return redirect('/trang/dang-ky-giao-vien-thanh-cong');
    }

    private function sendMail($request) {
        try {
            $subject =  'Đăng ký thành giáo viên';
            if (Auth::check()) {
                $content = 'Anh (chị)'. Auth::user()->name.' Vừa đăng ký trở thành giáo viên tại phòng học trực tuyến.';
                $content .= '<br> Thông tin liên hệ:';
                $content .= '<br> Số điện thoại: '. Auth::user()->phone;
                $content .= '<br> Email: '.Auth::user()->email;
            } else {
                $content = 'Anh (chị)'. $request->input('name').' Vừa đăng ký trở thành giáo viên tại phòng học trực tuyến.';
                $content .= '<br> Thông tin liên hệ:';
                $content .= '<br> Số điện thoại: '.$request->input('phone');
                $content .= '<br> Email: '.$request->input('email');
            }

            MailConfig::sendMail('', $subject, $content);

        } catch (\Exception $e) {
            return null;
        }
    }

    public function sendMailUser (Request $request) {
        try {
            $teacher = Teacher::where('user_id',  Auth::user()->id)->first();
            if (empty($teacher)) {
                $message = "Tài khoản của bạn không phải là tài khoản giáo viên.";

                 return view('site.learnClass.teacher_mail_user', compact('message'));
            }

            $classroom = Classroom::where('teacher_id', $teacher->teacher_id)
                ->where('classroom_id', $request->input('classroom_id'))->first();

            if (empty($classroom)) {
                $message = "Bạn không quản lý lớp học này hoặc lớp học này không tồn tại.";

                 return view('site.learnClass.teacher_mail_user', compact('message'));
            }

            $users = ClassroomStudent::join('users', 'users.id', '=', 'classroom_student.user_id')
                ->select('users.email')
                ->where('classroom_id', $request->input('classroom_id'))->get();

            $subject = $request->input('subject');
            $message = $request->input('content');

            // get email to
            $emailSend = array();
            foreach($users as $user) {
                $emailSend[] =  $user->email;
            }

            MailConfig::sendMail($emailSend, $subject, $message);

            $message = 'Bạn đã gửi mail thông báo cho học sinh thành công.';

             return view('site.learnClass.teacher_mail_user', compact('message'));
        } catch (\Exception $e) {
            $message = "Lỗi xảy ra trong quá trình gửi mail cho học sinh";

            return view('site.learnClass.teacher_mail_user', compact('message'));
        }
    }
}