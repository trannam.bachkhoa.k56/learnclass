<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 8/2/2018
 * Time: 11:41 AM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;



class MailResetPassword  extends Mailable
{
    use Queueable, SerializesModels;

    protected $customerName;
    protected $customerAddress;
    protected $customerEmail;
    protected $newPassword;
    protected $information;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $customerName, $customerAddress, $customerEmail, $newPassword, $information)
    {
        $this->customerName = $customerName;
        $this->customerAddress = $customerAddress;
        $this->customerEmail = $customerEmail;
        $this->newPassword = $newPassword;
        $this->information = $information;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.forget_password')->with([
            'customerName' => $this->customerName,
            'customerAddress' => $this->customerAddress,
            'customerEmail' => $this->customerEmail,
            'newPassword' => $this->newPassword,
            'information' => $this->information,
        ]);
    }
}