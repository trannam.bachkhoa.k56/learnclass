<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/6/2017
 * Time: 2:15 PM
 */

namespace App\Ultility;


class Location
{
      private $locationMenu = array(
          'menu-chinh' => 'menu chính',
          'side-left-menu' => 'sidebar menu trái',
          'footer-first' => 'chân trang thứ 1',
          'footer-second' => 'chân trang thứ 2',
          'footer-third' => 'chân trang thứ 3',
          'level-learn' => "menu các cấp học",
          'objects' => "Các môn học",
          'menu-footer' => 'menu cuối trang',
          'menu-product' => 'menu sản phẩm',
          'anh-menu' => 'ảnh menu',
          'sider-bar-blog' => 'sidebar blog menu trái',
          'menu-hoi-dap' => 'menu trang hỏi đáp',
          'menu-huong-dan' => 'menu hướng dẫn',
          'menu-giao-vien' => 'menu giáo viên',
      );
      public function getLocationMenu() {
          return $this->locationMenu;
      }
}
