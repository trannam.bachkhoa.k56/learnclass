<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 9/21/2018
 * Time: 10:37 AM
 */

namespace App\Ultility;


class NotificationMobile
{
    public static function pushNotification ($to, $title, $body) {
        try {
            // dữ liệu thông báo
            $data = [
                'to' => $to,
                'title' => $title,
                'body' => $body,
                'sound' => 'default',
            ];

            $service_url = 'https://exp.host/--/api/v2/push/send';
            $curl = curl_init($service_url);

            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            $curl_response = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $decoded = json_decode($curl_response, true);

            if($httpcode == 200){
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }

    }
}