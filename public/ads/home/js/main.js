(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
jQuery(document).ready(function() {
    new WOW().init();
	jQuery("#menu").mmenu({
		drag: true,
		pageScroll: {
			scroll: true,
			update: true
		}
	});

    $("#owl-partner").owlCarousel({
        items: 6,
        itemsDesktop: [1024, 5],
        itemsDesktopSmall: [900, 4],
        itemsTablet: [600, 3],
        itemsMobile : [320,1],
        navigation: true,
        pagination: false,
        slideSpeed: 500,
        paginationSpeed: 1000,
        autoPlay: 5000,
        navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    });
    
});
function number_format_price(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
// Scroll To Top
$(document).ready(function(){
    $(".v2_bnc_icon_scrolltop").click(function(event){
        $('html, body').animate({ scrollTop: 0 }, 1000);
    });
    // Hide,Show ScrollToTop
    var num = 100;  
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > num) {   
            $('.v2_bnc_scrolltop').addClass('fixed');
        }
        else
        {
            $('.v2_bnc_scrolltop').removeClass('fixed');
        }
    });
}); 
// End Scroll To Top 

 $(document).on('change', '#package,#period', function(event) {
    var price = 0;
    var month = 0;

    if ($('#period').val() != '')
        month = $('#period').val();

    if ($('#package').find('option:selected').data('price') != '' && $('#package').val() != '')
        price = $('#package').find('option:selected').data('price');

    var total = 0;
    total = parseInt(price)*parseInt(month);

    if (total > 0)
    {
        $('.block-total-price').show();
        $('#total-price').html(number_format_price(total)+'<sup>đ</sup>');
    }
    else
        $('.block-total-price').hide();
 });

 $(document).on('submit', '#order_package_form', function(event) {
    event.preventDefault();
    event.stopPropagation();
    var form_order = $('#order_package_form').serialize() + '&_token=' + $.cookie('_token')
    $('#order_package_submit').attr('disabled','disabled');
    $('.loading').html('<i class="fa fa-spinner fa-spin"></i>');
    $.ajax({
        url: '/payment/order_package',
        type: 'POST',
        data: form_order,
        success: function (res) {
            if (res.error) {
                $('.area-error').html('');
                $.each(res.msg,function(index, el) { 
                    $('.'+index+'-msg-error').text(el);
                    $('.'+index+'-msg-error').css({
                        display: 'block'
                    });
                    $('.'+index+'-msg-error').parent().find('input').css('border-color','#dc3545');
                });
            } else {
                
                $('#dat-hang').hide();
                $('.order-success').show();
                $('.oid').text(res.oid);
            }
            $('.loading').html('');
            $('#order_package_submit').removeAttr('disabled');
        }
    });
 });
 $(document).on('click', '.btn-signup-package', function(event) {
    $('#dat-hang').show();
    $('.order-success').hide();
    var code = $(this).data('code');
    $('#package').val(code).trigger('change');
 });

 $(document).on('click','.btn-back-to-register',function(){
    $('#dat-hang').show();
    $('.order-success').hide();
   
 })