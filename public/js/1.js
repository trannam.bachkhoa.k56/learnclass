function closeLinks() {
    $("#main-links li").removeClass("selected").children(".link-forms-container").slideUp("slow")
}

function animateIn() {
    var e = setTimeout(function () {
        clearTimeout(e), $(".is-preload.item-remover").each(function (e) {
            var t = $(this), n = 200 * e, o = setTimeout(function () {
                clearTimeout(o), t.css("visibility", "visible").addClass("is-loaded").removeClass("item-remover")
            }, n)
        }), $(window).bind("scroll", scrollHandler), scrolly = !1
    }, 600)
}

function popCloser(e) {
    $("#wrapper, #developer-tag").removeClass("blurer"), $("#developer-tag").removeClass("x-hidden"), $("#contact_popup, #follow_popup").fadeOut("slow")
}

function saveContact() {
    $(document).off("click", "#contact_submit", saveContact);
    var e = $("#contact_name"), t = $("#contact_email"), n = $("#contact_mobile"), o = $("#contact_message"),
        i = $("#contact_subscribe");
    if (window.FormData) {
        var r = new FormData;
        r.append("name", e.val()), r.append("email", t.val()), r.append("mobile", n.val()), r.append("message", o.val()), r.append("subscribe", checkboxValueConversion(i)), $.ajax({
            url: "/contact",
            type: "POST",
            data: r,
            processData: !1,
            contentType: !1,
            beforeSend: function (n) {
                function i() {
                    t.val() && checkEmail(t.val()) || (r = !0, t.addClass("error")), e.val() || (r = !0, e.addClass("error")), o.val() || (r = !0, o.addClass("error"))
                }

                var r = !1;
                return i(), r ? ($(document).on("click", "#contact_submit", saveContact), !1) : (n.setRequestHeader("X-CSRF-TOKEN", $('[name="_token"]').val()), void $("#contact_submit").text("Please wait..."))
            },
            success: function (r) {
                try {
                    if (r.success) {
                        $("#contact_submit").text(r.message), e.val(""), t.val(""), n.val(""), o.val(""), i.prop("checked", !1);
                        var s = setTimeout(function () {
                            clearTimeout(s), popCloser()
                        }, 3e3)
                    } else $("#contact_submit").text(r.message), $(document).on("click", "#contact_submit", saveContact)
                } catch (a) {
                    $("#contact_submit").text("Try again"), $(document).on("click", "#contact_submit", saveContact)
                }
            },
            error: function () {
                $("#contact_submit").text("Try again"), $(document).on("click", "#contact_submit", saveContact)
            }
        })
    } else alert("please use better browser")
}

function saveSubscriber() {
    $(document).off("click", "#subscribe_submit", saveSubscriber);
    var e = $("#subscribe_name"), t = $("#subscribe_email");
    if (window.FormData) {
        var n = new FormData;
        n.append("name", e.val()), n.append("email", t.val()), $.ajax({
            url: "/subscribe",
            type: "POST",
            data: n,
            processData: !1,
            contentType: !1,
            beforeSend: function (e) {
                function n() {
                    t.val() && checkEmail(t.val()) || (o = !0, t.addClass("error"))
                }

                var o = !1;
                return n(), o ? ($(document).on("click", "#subscribe_submit", saveSubscriber), !1) : (e.setRequestHeader("X-CSRF-TOKEN", $('[name="_token"]').val()), void $("#subscribe_submit").text("Please wait..."))
            },
            success: function (n) {
                try {
                    if (n.success) {
                        $("#subscribe_submit").text(n.message), e.val(""), t.val("");
                        var o = setTimeout(function () {
                            clearTimeout(o), popCloser()
                        }, 3e3)
                    } else $("#subscribe_submit").text(n.message), $(document).on("click", "#subscribe_submit", saveSubscriber)
                } catch (i) {
                    $("#subscribe_submit").text("Try again"), $(document).on("click", "#subscribe_submit", saveSubscriber)
                }
            },
            error: function () {
                $("#subscribe_submit").text("Try again"), $(document).on("click", "#subscribe_submit", saveSubscriber)
            }
        })
    } else alert("please use better browser")
}

function saveAppreciate() {
    $(document).off("click", ".appreciater", saveAppreciate);
    var e = window.location.href;
    $.getJSON(e + "?appreciate=true", function (e) {
        e.success ? $(".appreciater").text("Thank you").removeClass("appreciater") : $(document).on("click", ".appreciater", saveAppreciate)
    })
}

function checkboxValueConversion(e) {
    return e.is(":checked") ? 1 : 0
}

function workContentScroll() {
    var e = $(this).scrollTop(), t = e + $(this).height(), n = $("#work-details-container"), o = n.height(),
        i = $(".works-enlarge").width() + 250;
    t >= o ? n.css({position: "fixed", left: i, bottom: "0px"}) : n.css({position: "static", left: 0, bottom: "auto"})
}

function initWorks() {
    var e = $(window).innerWidth();
    $(".works-enlarge").css("max-width", e - 471);
    var t = $(window).height(), n = $("#work-details-container"), o = n.height();
    if (t - 95 > o) {
        setTimeout(function () {
            var e = $(".works-enlarge").width() + 250;
            n.css({position: "fixed", left: e, top: "0"}).removeClass("invisible")
        }, 3e3)
    } else {
        var i = $("#work-content");
        i.height();
        i.scroll(workContentScroll), n.removeClass("invisible"), e > 768 && $(".works-enlarge").css({"min-height": o + 100})
    }
}

!function (e, t) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function (e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : this, function (e, t) {
    function n(e) {
        var t = e.length, n = ie.type(e);
        return "function" !== n && !ie.isWindow(e) && (!(1 !== e.nodeType || !t) || ("array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e))
    }

    function o(e, t, n) {
        if (ie.isFunction(t)) return ie.grep(e, function (e, o) {
            return !!t.call(e, o, e) !== n
        });
        if (t.nodeType) return ie.grep(e, function (e) {
            return e === t !== n
        });
        if ("string" == typeof t) {
            if (he.test(t)) return ie.filter(t, e, n);
            t = ie.filter(t, e)
        }
        return ie.grep(e, function (e) {
            return ie.inArray(e, t) >= 0 !== n
        })
    }

    function i(e, t) {
        do e = e[t]; while (e && 1 !== e.nodeType);
        return e
    }

    function r(e) {
        var t = we[e] = {};
        return ie.each(e.match(be) || [], function (e, n) {
            t[n] = !0
        }), t
    }

    function s() {
        fe.addEventListener ? (fe.removeEventListener("DOMContentLoaded", a, !1), e.removeEventListener("load", a, !1)) : (fe.detachEvent("onreadystatechange", a), e.detachEvent("onload", a))
    }

    function a() {
        (fe.addEventListener || "load" === event.type || "complete" === fe.readyState) && (s(), ie.ready())
    }

    function l(e, t, n) {
        if (void 0 === n && 1 === e.nodeType) {
            var o = "data-" + t.replace(Ee, "-$1").toLowerCase();
            if (n = e.getAttribute(o), "string" == typeof n) {
                try {
                    n = "true" === n || "false" !== n && ("null" === n ? null : +n + "" === n ? +n : ke.test(n) ? ie.parseJSON(n) : n)
                } catch (i) {
                }
                ie.data(e, t, n)
            } else n = void 0
        }
        return n
    }

    function c(e) {
        var t;
        for (t in e) if (("data" !== t || !ie.isEmptyObject(e[t])) && "toJSON" !== t) return !1;
        return !0
    }

    function u(e, t, n, o) {
        if (ie.acceptData(e)) {
            var i, r, s = ie.expando, a = e.nodeType, l = a ? ie.cache : e, c = a ? e[s] : e[s] && s;
            if (c && l[c] && (o || l[c].data) || void 0 !== n || "string" != typeof t) return c || (c = a ? e[s] = U.pop() || ie.guid++ : s), l[c] || (l[c] = a ? {} : {toJSON: ie.noop}), ("object" == typeof t || "function" == typeof t) && (o ? l[c] = ie.extend(l[c], t) : l[c].data = ie.extend(l[c].data, t)), r = l[c], o || (r.data || (r.data = {}), r = r.data), void 0 !== n && (r[ie.camelCase(t)] = n), "string" == typeof t ? (i = r[t], null == i && (i = r[ie.camelCase(t)])) : i = r, i
        }
    }

    function d(e, t, n) {
        if (ie.acceptData(e)) {
            var o, i, r = e.nodeType, s = r ? ie.cache : e, a = r ? e[ie.expando] : ie.expando;
            if (s[a]) {
                if (t && (o = n ? s[a] : s[a].data)) {
                    ie.isArray(t) ? t = t.concat(ie.map(t, ie.camelCase)) : t in o ? t = [t] : (t = ie.camelCase(t), t = t in o ? [t] : t.split(" ")), i = t.length;
                    for (; i--;) delete o[t[i]];
                    if (n ? !c(o) : !ie.isEmptyObject(o)) return
                }
                (n || (delete s[a].data, c(s[a]))) && (r ? ie.cleanData([e], !0) : ne.deleteExpando || s != s.window ? delete s[a] : s[a] = null)
            }
        }
    }

    function h() {
        return !0
    }

    function p() {
        return !1
    }

    function f() {
        try {
            return fe.activeElement
        } catch (e) {
        }
    }

    function m(e) {
        var t = De.split("|"), n = e.createDocumentFragment();
        if (n.createElement) for (; t.length;) n.createElement(t.pop());
        return n
    }

    function g(e, t) {
        var n, o, i = 0,
            r = typeof e.getElementsByTagName !== Se ? e.getElementsByTagName(t || "*") : typeof e.querySelectorAll !== Se ? e.querySelectorAll(t || "*") : void 0;
        if (!r) for (r = [], n = e.childNodes || e; null != (o = n[i]); i++) !t || ie.nodeName(o, t) ? r.push(o) : ie.merge(r, g(o, t));
        return void 0 === t || t && ie.nodeName(e, t) ? ie.merge([e], r) : r
    }

    function v(e) {
        _e.test(e.type) && (e.defaultChecked = e.checked)
    }

    function y(e, t) {
        return ie.nodeName(e, "table") && ie.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
    }

    function b(e) {
        return e.type = (null !== ie.find.attr(e, "type")) + "/" + e.type, e
    }

    function w(e) {
        var t = Ye.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }

    function x(e, t) {
        for (var n, o = 0; null != (n = e[o]); o++) ie._data(n, "globalEval", !t || ie._data(t[o], "globalEval"))
    }

    function T(e, t) {
        if (1 === t.nodeType && ie.hasData(e)) {
            var n, o, i, r = ie._data(e), s = ie._data(t, r), a = r.events;
            if (a) {
                delete s.handle, s.events = {};
                for (n in a) for (o = 0, i = a[n].length; i > o; o++) ie.event.add(t, n, a[n][o])
            }
            s.data && (s.data = ie.extend({}, s.data))
        }
    }

    function S(e, t) {
        var n, o, i;
        if (1 === t.nodeType) {
            if (n = t.nodeName.toLowerCase(), !ne.noCloneEvent && t[ie.expando]) {
                i = ie._data(t);
                for (o in i.events) ie.removeEvent(t, o, i.handle);
                t.removeAttribute(ie.expando)
            }
            "script" === n && t.text !== e.text ? (b(t).text = e.text, w(t)) : "object" === n ? (t.parentNode && (t.outerHTML = e.outerHTML), ne.html5Clone && e.innerHTML && !ie.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === n && _e.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : "option" === n ? t.defaultSelected = t.selected = e.defaultSelected : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue)
        }
    }

    function k(t, n) {
        var o, i = ie(n.createElement(t)).appendTo(n.body),
            r = e.getDefaultComputedStyle && (o = e.getDefaultComputedStyle(i[0])) ? o.display : ie.css(i[0], "display");
        return i.detach(), r
    }

    function E(e) {
        var t = fe, n = Ke[e];
        return n || (n = k(e, t), "none" !== n && n || (Je = (Je || ie("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement), t = (Je[0].contentWindow || Je[0].contentDocument).document, t.write(), t.close(), n = k(e, t), Je.detach()), Ke[e] = n), n
    }

    function C(e, t) {
        return {
            get: function () {
                var n = e();
                if (null != n) return n ? void delete this.get : (this.get = t).apply(this, arguments)
            }
        }
    }

    function z(e, t) {
        if (t in e) return t;
        for (var n = t.charAt(0).toUpperCase() + t.slice(1), o = t, i = ht.length; i--;) if (t = ht[i] + n, t in e) return t;
        return o
    }

    function L(e, t) {
        for (var n, o, i, r = [], s = 0, a = e.length; a > s; s++) o = e[s], o.style && (r[s] = ie._data(o, "olddisplay"), n = o.style.display, t ? (r[s] || "none" !== n || (o.style.display = ""), "" === o.style.display && Le(o) && (r[s] = ie._data(o, "olddisplay", E(o.nodeName)))) : (i = Le(o), (n && "none" !== n || !i) && ie._data(o, "olddisplay", i ? n : ie.css(o, "display"))));
        for (s = 0; a > s; s++) o = e[s], o.style && (t && "none" !== o.style.display && "" !== o.style.display || (o.style.display = t ? r[s] || "" : "none"));
        return e
    }

    function N(e, t, n) {
        var o = lt.exec(t);
        return o ? Math.max(0, o[1] - (n || 0)) + (o[2] || "px") : t
    }

    function _(e, t, n, o, i) {
        for (var r = n === (o ? "border" : "content") ? 4 : "width" === t ? 1 : 0, s = 0; 4 > r; r += 2) "margin" === n && (s += ie.css(e, n + ze[r], !0, i)), o ? ("content" === n && (s -= ie.css(e, "padding" + ze[r], !0, i)), "margin" !== n && (s -= ie.css(e, "border" + ze[r] + "Width", !0, i))) : (s += ie.css(e, "padding" + ze[r], !0, i), "padding" !== n && (s += ie.css(e, "border" + ze[r] + "Width", !0, i)));
        return s
    }

    function P(e, t, n) {
        var o = !0, i = "width" === t ? e.offsetWidth : e.offsetHeight, r = et(e),
            s = ne.boxSizing && "border-box" === ie.css(e, "boxSizing", !1, r);
        if (0 >= i || null == i) {
            if (i = tt(e, t, r), (0 > i || null == i) && (i = e.style[t]), ot.test(i)) return i;
            o = s && (ne.boxSizingReliable() || i === e.style[t]), i = parseFloat(i) || 0
        }
        return i + _(e, t, n || (s ? "border" : "content"), o, r) + "px"
    }

    function M(e, t, n, o, i) {
        return new M.prototype.init(e, t, n, o, i)
    }

    function A() {
        return setTimeout(function () {
            pt = void 0
        }), pt = ie.now()
    }

    function j(e, t) {
        var n, o = {height: e}, i = 0;
        for (t = t ? 1 : 0; 4 > i; i += 2 - t) n = ze[i], o["margin" + n] = o["padding" + n] = e;
        return t && (o.opacity = o.width = e), o
    }

    function H(e, t, n) {
        for (var o, i = (bt[t] || []).concat(bt["*"]), r = 0, s = i.length; s > r; r++) if (o = i[r].call(n, t, e)) return o
    }

    function D(e, t, n) {
        var o, i, r, s, a, l, c, u, d = this, h = {}, p = e.style, f = e.nodeType && Le(e), m = ie._data(e, "fxshow");
        n.queue || (a = ie._queueHooks(e, "fx"), null == a.unqueued && (a.unqueued = 0, l = a.empty.fire, a.empty.fire = function () {
            a.unqueued || l()
        }), a.unqueued++, d.always(function () {
            d.always(function () {
                a.unqueued--, ie.queue(e, "fx").length || a.empty.fire()
            })
        })), 1 === e.nodeType && ("height" in t || "width" in t) && (n.overflow = [p.overflow, p.overflowX, p.overflowY], c = ie.css(e, "display"), u = "none" === c ? ie._data(e, "olddisplay") || E(e.nodeName) : c, "inline" === u && "none" === ie.css(e, "float") && (ne.inlineBlockNeedsLayout && "inline" !== E(e.nodeName) ? p.zoom = 1 : p.display = "inline-block")), n.overflow && (p.overflow = "hidden", ne.shrinkWrapBlocks() || d.always(function () {
            p.overflow = n.overflow[0], p.overflowX = n.overflow[1], p.overflowY = n.overflow[2]
        }));
        for (o in t) if (i = t[o], mt.exec(i)) {
            if (delete t[o], r = r || "toggle" === i, i === (f ? "hide" : "show")) {
                if ("show" !== i || !m || void 0 === m[o]) continue;
                f = !0
            }
            h[o] = m && m[o] || ie.style(e, o)
        } else c = void 0;
        if (ie.isEmptyObject(h)) "inline" === ("none" === c ? E(e.nodeName) : c) && (p.display = c); else {
            m ? "hidden" in m && (f = m.hidden) : m = ie._data(e, "fxshow", {}), r && (m.hidden = !f), f ? ie(e).show() : d.done(function () {
                ie(e).hide()
            }), d.done(function () {
                var t;
                ie._removeData(e, "fxshow");
                for (t in h) ie.style(e, t, h[t])
            });
            for (o in h) s = H(f ? m[o] : 0, o, d), o in m || (m[o] = s.start, f && (s.end = s.start, s.start = "width" === o || "height" === o ? 1 : 0))
        }
    }

    function R(e, t) {
        var n, o, i, r, s;
        for (n in e) if (o = ie.camelCase(n), i = t[o], r = e[n], ie.isArray(r) && (i = r[1], r = e[n] = r[0]), n !== o && (e[o] = r, delete e[n]), s = ie.cssHooks[o], s && "expand" in s) {
            r = s.expand(r), delete e[o];
            for (n in r) n in e || (e[n] = r[n], t[n] = i)
        } else t[o] = i
    }

    function O(e, t, n) {
        var o, i, r = 0, s = yt.length, a = ie.Deferred().always(function () {
            delete l.elem
        }), l = function () {
            if (i) return !1;
            for (var t = pt || A(), n = Math.max(0, c.startTime + c.duration - t), o = n / c.duration || 0, r = 1 - o, s = 0, l = c.tweens.length; l > s; s++) c.tweens[s].run(r);
            return a.notifyWith(e, [c, r, n]), 1 > r && l ? n : (a.resolveWith(e, [c]), !1)
        }, c = a.promise({
            elem: e,
            props: ie.extend({}, t),
            opts: ie.extend(!0, {specialEasing: {}}, n),
            originalProperties: t,
            originalOptions: n,
            startTime: pt || A(),
            duration: n.duration,
            tweens: [],
            createTween: function (t, n) {
                var o = ie.Tween(e, c.opts, t, n, c.opts.specialEasing[t] || c.opts.easing);
                return c.tweens.push(o), o
            },
            stop: function (t) {
                var n = 0, o = t ? c.tweens.length : 0;
                if (i) return this;
                for (i = !0; o > n; n++) c.tweens[n].run(1);
                return t ? a.resolveWith(e, [c, t]) : a.rejectWith(e, [c, t]), this
            }
        }), u = c.props;
        for (R(u, c.opts.specialEasing); s > r; r++) if (o = yt[r].call(c, e, u, c.opts)) return o;
        return ie.map(u, H, c), ie.isFunction(c.opts.start) && c.opts.start.call(e, c), ie.fx.timer(ie.extend(l, {
            elem: e,
            anim: c,
            queue: c.opts.queue
        })), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
    }

    function $(e) {
        return function (t, n) {
            "string" != typeof t && (n = t, t = "*");
            var o, i = 0, r = t.toLowerCase().match(be) || [];
            if (ie.isFunction(n)) for (; o = r[i++];) "+" === o.charAt(0) ? (o = o.slice(1) || "*", (e[o] = e[o] || []).unshift(n)) : (e[o] = e[o] || []).push(n)
        }
    }

    function I(e, t, n, o) {
        function i(a) {
            var l;
            return r[a] = !0, ie.each(e[a] || [], function (e, a) {
                var c = a(t, n, o);
                return "string" != typeof c || s || r[c] ? s ? !(l = c) : void 0 : (t.dataTypes.unshift(c), i(c), !1)
            }), l
        }

        var r = {}, s = e === Bt;
        return i(t.dataTypes[0]) || !r["*"] && i("*")
    }

    function q(e, t) {
        var n, o, i = ie.ajaxSettings.flatOptions || {};
        for (o in t) void 0 !== t[o] && ((i[o] ? e : n || (n = {}))[o] = t[o]);
        return n && ie.extend(!0, e, n), e
    }

    function W(e, t, n) {
        for (var o, i, r, s, a = e.contents, l = e.dataTypes; "*" === l[0];) l.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
        if (i) for (s in a) if (a[s] && a[s].test(i)) {
            l.unshift(s);
            break
        }
        if (l[0] in n) r = l[0]; else {
            for (s in n) {
                if (!l[0] || e.converters[s + " " + l[0]]) {
                    r = s;
                    break
                }
                o || (o = s)
            }
            r = r || o
        }
        return r ? (r !== l[0] && l.unshift(r), n[r]) : void 0
    }

    function B(e, t, n, o) {
        var i, r, s, a, l, c = {}, u = e.dataTypes.slice();
        if (u[1]) for (s in e.converters) c[s.toLowerCase()] = e.converters[s];
        for (r = u.shift(); r;) if (e.responseFields[r] && (n[e.responseFields[r]] = t), !l && o && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = r, r = u.shift()) if ("*" === r) r = l; else if ("*" !== l && l !== r) {
            if (s = c[l + " " + r] || c["* " + r], !s) for (i in c) if (a = i.split(" "), a[1] === r && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                s === !0 ? s = c[i] : c[i] !== !0 && (r = a[0], u.unshift(a[1]));
                break
            }
            if (s !== !0) if (s && e["throws"]) t = s(t); else try {
                t = s(t)
            } catch (d) {
                return {state: "parsererror", error: s ? d : "No conversion from " + l + " to " + r}
            }
        }
        return {state: "success", data: t}
    }

    function F(e, t, n, o) {
        var i;
        if (ie.isArray(t)) ie.each(t, function (t, i) {
            n || Yt.test(e) ? o(e, i) : F(e + "[" + ("object" == typeof i ? t : "") + "]", i, n, o)
        }); else if (n || "object" !== ie.type(t)) o(e, t); else for (i in t) F(e + "[" + i + "]", t[i], n, o)
    }

    function V() {
        try {
            return new e.XMLHttpRequest
        } catch (t) {
        }
    }

    function X() {
        try {
            return new e.ActiveXObject("Microsoft.XMLHTTP")
        } catch (t) {
        }
    }

    function Y(e) {
        return ie.isWindow(e) ? e : 9 === e.nodeType && (e.defaultView || e.parentWindow)
    }

    var U = [], Q = U.slice, Z = U.concat, G = U.push, J = U.indexOf, K = {}, ee = K.toString, te = K.hasOwnProperty,
        ne = {}, oe = "1.11.1", ie = function (e, t) {
            return new ie.fn.init(e, t)
        }, re = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, se = /^-ms-/, ae = /-([\da-z])/gi, le = function (e, t) {
            return t.toUpperCase()
        };
    ie.fn = ie.prototype = {
        jquery: oe, constructor: ie, selector: "", length: 0, toArray: function () {
            return Q.call(this)
        }, get: function (e) {
            return null != e ? 0 > e ? this[e + this.length] : this[e] : Q.call(this)
        }, pushStack: function (e) {
            var t = ie.merge(this.constructor(), e);
            return t.prevObject = this, t.context = this.context, t
        }, each: function (e, t) {
            return ie.each(this, e, t)
        }, map: function (e) {
            return this.pushStack(ie.map(this, function (t, n) {
                return e.call(t, n, t)
            }))
        }, slice: function () {
            return this.pushStack(Q.apply(this, arguments))
        }, first: function () {
            return this.eq(0)
        }, last: function () {
            return this.eq(-1)
        }, eq: function (e) {
            var t = this.length, n = +e + (0 > e ? t : 0);
            return this.pushStack(n >= 0 && t > n ? [this[n]] : [])
        }, end: function () {
            return this.prevObject || this.constructor(null)
        }, push: G, sort: U.sort, splice: U.splice
    }, ie.extend = ie.fn.extend = function () {
        var e, t, n, o, i, r, s = arguments[0] || {}, a = 1, l = arguments.length, c = !1;
        for ("boolean" == typeof s && (c = s, s = arguments[a] || {}, a++), "object" == typeof s || ie.isFunction(s) || (s = {}), a === l && (s = this, a--); l > a; a++) if (null != (i = arguments[a])) for (o in i) e = s[o], n = i[o], s !== n && (c && n && (ie.isPlainObject(n) || (t = ie.isArray(n))) ? (t ? (t = !1, r = e && ie.isArray(e) ? e : []) : r = e && ie.isPlainObject(e) ? e : {}, s[o] = ie.extend(c, r, n)) : void 0 !== n && (s[o] = n));
        return s
    }, ie.extend({
        expando: "jQuery" + (oe + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (e) {
            throw new Error(e)
        }, noop: function () {
        }, isFunction: function (e) {
            return "function" === ie.type(e)
        }, isArray: Array.isArray || function (e) {
            return "array" === ie.type(e)
        }, isWindow: function (e) {
            return null != e && e == e.window
        }, isNumeric: function (e) {
            return !ie.isArray(e) && e - parseFloat(e) >= 0
        }, isEmptyObject: function (e) {
            var t;
            for (t in e) return !1;
            return !0
        }, isPlainObject: function (e) {
            var t;
            if (!e || "object" !== ie.type(e) || e.nodeType || ie.isWindow(e)) return !1;
            try {
                if (e.constructor && !te.call(e, "constructor") && !te.call(e.constructor.prototype, "isPrototypeOf")) return !1
            } catch (n) {
                return !1
            }
            if (ne.ownLast) for (t in e) return te.call(e, t);
            for (t in e) ;
            return void 0 === t || te.call(e, t)
        }, type: function (e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? K[ee.call(e)] || "object" : typeof e
        }, globalEval: function (t) {
            t && ie.trim(t) && (e.execScript || function (t) {
                e.eval.call(e, t)
            })(t)
        }, camelCase: function (e) {
            return e.replace(se, "ms-").replace(ae, le)
        }, nodeName: function (e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
        }, each: function (e, t, o) {
            var i, r = 0, s = e.length, a = n(e);
            if (o) {
                if (a) for (; s > r && (i = t.apply(e[r], o), i !== !1); r++) ; else for (r in e) if (i = t.apply(e[r], o), i === !1) break
            } else if (a) for (; s > r && (i = t.call(e[r], r, e[r]), i !== !1); r++) ; else for (r in e) if (i = t.call(e[r], r, e[r]), i === !1) break;
            return e
        }, trim: function (e) {
            return null == e ? "" : (e + "").replace(re, "")
        }, makeArray: function (e, t) {
            var o = t || [];
            return null != e && (n(Object(e)) ? ie.merge(o, "string" == typeof e ? [e] : e) : G.call(o, e)), o
        }, inArray: function (e, t, n) {
            var o;
            if (t) {
                if (J) return J.call(t, e, n);
                for (o = t.length, n = n ? 0 > n ? Math.max(0, o + n) : n : 0; o > n; n++) if (n in t && t[n] === e) return n
            }
            return -1
        }, merge: function (e, t) {
            for (var n = +t.length, o = 0, i = e.length; n > o;) e[i++] = t[o++];
            if (n !== n) for (; void 0 !== t[o];) e[i++] = t[o++];
            return e.length = i, e
        }, grep: function (e, t, n) {
            for (var o, i = [], r = 0, s = e.length, a = !n; s > r; r++) o = !t(e[r], r), o !== a && i.push(e[r]);
            return i
        }, map: function (e, t, o) {
            var i, r = 0, s = e.length, a = n(e), l = [];
            if (a) for (; s > r; r++) i = t(e[r], r, o), null != i && l.push(i); else for (r in e) i = t(e[r], r, o), null != i && l.push(i);
            return Z.apply([], l)
        }, guid: 1, proxy: function (e, t) {
            var n, o, i;
            return "string" == typeof t && (i = e[t], t = e, e = i), ie.isFunction(e) ? (n = Q.call(arguments, 2), o = function () {
                return e.apply(t || this, n.concat(Q.call(arguments)))
            }, o.guid = e.guid = e.guid || ie.guid++, o) : void 0
        }, now: function () {
            return +new Date
        }, support: ne
    }), ie.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (e, t) {
        K["[object " + t + "]"] = t.toLowerCase()
    });
    var ce = function (e) {
        function t(e, t, n, o) {
            var i, r, s, a, l, c, d, p, f, m;
            if ((t ? t.ownerDocument || t : I) !== M && P(t), t = t || M, n = n || [], !e || "string" != typeof e) return n;
            if (1 !== (a = t.nodeType) && 9 !== a) return [];
            if (j && !o) {
                if (i = ye.exec(e)) if (s = i[1]) {
                    if (9 === a) {
                        if (r = t.getElementById(s), !r || !r.parentNode) return n;
                        if (r.id === s) return n.push(r), n
                    } else if (t.ownerDocument && (r = t.ownerDocument.getElementById(s)) && O(t, r) && r.id === s) return n.push(r), n
                } else {
                    if (i[2]) return K.apply(n, t.getElementsByTagName(e)), n;
                    if ((s = i[3]) && x.getElementsByClassName && t.getElementsByClassName) return K.apply(n, t.getElementsByClassName(s)), n
                }
                if (x.qsa && (!H || !H.test(e))) {
                    if (p = d = $, f = t, m = 9 === a && e, 1 === a && "object" !== t.nodeName.toLowerCase()) {
                        for (c = E(e), (d = t.getAttribute("id")) ? p = d.replace(we, "\\$&") : t.setAttribute("id", p), p = "[id='" + p + "'] ", l = c.length; l--;) c[l] = p + h(c[l]);
                        f = be.test(e) && u(t.parentNode) || t, m = c.join(",")
                    }
                    if (m) try {
                        return K.apply(n, f.querySelectorAll(m)), n
                    } catch (g) {
                    } finally {
                        d || t.removeAttribute("id")
                    }
                }
            }
            return z(e.replace(le, "$1"), t, n, o)
        }

        function n() {
            function e(n, o) {
                return t.push(n + " ") > T.cacheLength && delete e[t.shift()], e[n + " "] = o
            }

            var t = [];
            return e
        }

        function o(e) {
            return e[$] = !0, e
        }

        function i(e) {
            var t = M.createElement("div");
            try {
                return !!e(t)
            } catch (n) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }

        function r(e, t) {
            for (var n = e.split("|"), o = e.length; o--;) T.attrHandle[n[o]] = t
        }

        function s(e, t) {
            var n = t && e,
                o = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || U) - (~e.sourceIndex || U);
            if (o) return o;
            if (n) for (; n = n.nextSibling;) if (n === t) return -1;
            return e ? 1 : -1
        }

        function a(e) {
            return function (t) {
                var n = t.nodeName.toLowerCase();
                return "input" === n && t.type === e
            }
        }

        function l(e) {
            return function (t) {
                var n = t.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && t.type === e
            }
        }

        function c(e) {
            return o(function (t) {
                return t = +t, o(function (n, o) {
                    for (var i, r = e([], n.length, t), s = r.length; s--;) n[i = r[s]] && (n[i] = !(o[i] = n[i]))
                })
            })
        }

        function u(e) {
            return e && typeof e.getElementsByTagName !== Y && e
        }

        function d() {
        }

        function h(e) {
            for (var t = 0, n = e.length, o = ""; n > t; t++) o += e[t].value;
            return o
        }

        function p(e, t, n) {
            var o = t.dir, i = n && "parentNode" === o, r = W++;
            return t.first ? function (t, n, r) {
                for (; t = t[o];) if (1 === t.nodeType || i) return e(t, n, r)
            } : function (t, n, s) {
                var a, l, c = [q, r];
                if (s) {
                    for (; t = t[o];) if ((1 === t.nodeType || i) && e(t, n, s)) return !0
                } else for (; t = t[o];) if (1 === t.nodeType || i) {
                    if (l = t[$] || (t[$] = {}), (a = l[o]) && a[0] === q && a[1] === r) return c[2] = a[2];
                    if (l[o] = c, c[2] = e(t, n, s)) return !0
                }
            }
        }

        function f(e) {
            return e.length > 1 ? function (t, n, o) {
                for (var i = e.length; i--;) if (!e[i](t, n, o)) return !1;
                return !0
            } : e[0]
        }

        function m(e, n, o) {
            for (var i = 0, r = n.length; r > i; i++) t(e, n[i], o);
            return o
        }

        function g(e, t, n, o, i) {
            for (var r, s = [], a = 0, l = e.length, c = null != t; l > a; a++) (r = e[a]) && (!n || n(r, o, i)) && (s.push(r), c && t.push(a));
            return s
        }

        function v(e, t, n, i, r, s) {
            return i && !i[$] && (i = v(i)), r && !r[$] && (r = v(r, s)), o(function (o, s, a, l) {
                var c, u, d, h = [], p = [], f = s.length, v = o || m(t || "*", a.nodeType ? [a] : a, []),
                    y = !e || !o && t ? v : g(v, h, e, a, l), b = n ? r || (o ? e : f || i) ? [] : s : y;
                if (n && n(y, b, a, l), i) for (c = g(b, p), i(c, [], a, l), u = c.length; u--;) (d = c[u]) && (b[p[u]] = !(y[p[u]] = d));
                if (o) {
                    if (r || e) {
                        if (r) {
                            for (c = [], u = b.length; u--;) (d = b[u]) && c.push(y[u] = d);
                            r(null, b = [], c, l)
                        }
                        for (u = b.length; u--;) (d = b[u]) && (c = r ? te.call(o, d) : h[u]) > -1 && (o[c] = !(s[c] = d))
                    }
                } else b = g(b === s ? b.splice(f, b.length) : b), r ? r(null, s, b, l) : K.apply(s, b)
            })
        }

        function y(e) {
            for (var t, n, o, i = e.length, r = T.relative[e[0].type], s = r || T.relative[" "], a = r ? 1 : 0, l = p(function (e) {
                return e === t
            }, s, !0), c = p(function (e) {
                return te.call(t, e) > -1
            }, s, !0), u = [function (e, n, o) {
                return !r && (o || n !== L) || ((t = n).nodeType ? l(e, n, o) : c(e, n, o))
            }]; i > a; a++) if (n = T.relative[e[a].type]) u = [p(f(u), n)]; else {
                if (n = T.filter[e[a].type].apply(null, e[a].matches), n[$]) {
                    for (o = ++a; i > o && !T.relative[e[o].type]; o++) ;
                    return v(a > 1 && f(u), a > 1 && h(e.slice(0, a - 1).concat({value: " " === e[a - 2].type ? "*" : ""})).replace(le, "$1"), n, o > a && y(e.slice(a, o)), i > o && y(e = e.slice(o)), i > o && h(e))
                }
                u.push(n)
            }
            return f(u)
        }

        function b(e, n) {
            var i = n.length > 0, r = e.length > 0, s = function (o, s, a, l, c) {
                var u, d, h, p = 0, f = "0", m = o && [], v = [], y = L, b = o || r && T.find.TAG("*", c),
                    w = q += null == y ? 1 : Math.random() || .1, x = b.length;
                for (c && (L = s !== M && s); f !== x && null != (u = b[f]); f++) {
                    if (r && u) {
                        for (d = 0; h = e[d++];) if (h(u, s, a)) {
                            l.push(u);
                            break
                        }
                        c && (q = w)
                    }
                    i && ((u = !h && u) && p--, o && m.push(u))
                }
                if (p += f, i && f !== p) {
                    for (d = 0; h = n[d++];) h(m, v, s, a);
                    if (o) {
                        if (p > 0) for (; f--;) m[f] || v[f] || (v[f] = G.call(l));
                        v = g(v)
                    }
                    K.apply(l, v), c && !o && v.length > 0 && p + n.length > 1 && t.uniqueSort(l)
                }
                return c && (q = w, L = y), m
            };
            return i ? o(s) : s
        }

        var w, x, T, S, k, E, C, z, L, N, _, P, M, A, j, H, D, R, O, $ = "sizzle" + -new Date, I = e.document, q = 0,
            W = 0, B = n(), F = n(), V = n(), X = function (e, t) {
                return e === t && (_ = !0), 0
            }, Y = "undefined", U = 1 << 31, Q = {}.hasOwnProperty, Z = [], G = Z.pop, J = Z.push, K = Z.push, ee = Z.slice,
            te = Z.indexOf || function (e) {
                for (var t = 0, n = this.length; n > t; t++) if (this[t] === e) return t;
                return -1
            },
            ne = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            oe = "[\\x20\\t\\r\\n\\f]", ie = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", re = ie.replace("w", "w#"),
            se = "\\[" + oe + "*(" + ie + ")(?:" + oe + "*([*^$|!~]?=)" + oe + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + re + "))|)" + oe + "*\\]",
            ae = ":(" + ie + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + se + ")*)|.*)\\)|)",
            le = new RegExp("^" + oe + "+|((?:^|[^\\\\])(?:\\\\.)*)" + oe + "+$", "g"),
            ce = new RegExp("^" + oe + "*," + oe + "*"), ue = new RegExp("^" + oe + "*([>+~]|" + oe + ")" + oe + "*"),
            de = new RegExp("=" + oe + "*([^\\]'\"]*?)" + oe + "*\\]", "g"), he = new RegExp(ae),
            pe = new RegExp("^" + re + "$"), fe = {
                ID: new RegExp("^#(" + ie + ")"),
                CLASS: new RegExp("^\\.(" + ie + ")"),
                TAG: new RegExp("^(" + ie.replace("w", "w*") + ")"),
                ATTR: new RegExp("^" + se),
                PSEUDO: new RegExp("^" + ae),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + oe + "*(even|odd|(([+-]|)(\\d*)n|)" + oe + "*(?:([+-]|)" + oe + "*(\\d+)|))" + oe + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + ne + ")$", "i"),
                needsContext: new RegExp("^" + oe + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + oe + "*((?:-\\d)?\\d*)" + oe + "*\\)|)(?=[^-]|$)", "i")
            }, me = /^(?:input|select|textarea|button)$/i, ge = /^h\d$/i, ve = /^[^{]+\{\s*\[native \w/,
            ye = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, be = /[+~]/, we = /'|\\/g,
            xe = new RegExp("\\\\([\\da-f]{1,6}" + oe + "?|(" + oe + ")|.)", "ig"), Te = function (e, t, n) {
                var o = "0x" + t - 65536;
                return o !== o || n ? t : 0 > o ? String.fromCharCode(o + 65536) : String.fromCharCode(o >> 10 | 55296, 1023 & o | 56320)
            };
        try {
            K.apply(Z = ee.call(I.childNodes), I.childNodes), Z[I.childNodes.length].nodeType
        } catch (Se) {
            K = {
                apply: Z.length ? function (e, t) {
                    J.apply(e, ee.call(t))
                } : function (e, t) {
                    for (var n = e.length, o = 0; e[n++] = t[o++];) ;
                    e.length = n - 1
                }
            }
        }
        x = t.support = {}, k = t.isXML = function (e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return !!t && "HTML" !== t.nodeName
        }, P = t.setDocument = function (e) {
            var t, n = e ? e.ownerDocument || e : I, o = n.defaultView;
            return n !== M && 9 === n.nodeType && n.documentElement ? (M = n, A = n.documentElement, j = !k(n), o && o !== o.top && (o.addEventListener ? o.addEventListener("unload", function () {
                P()
            }, !1) : o.attachEvent && o.attachEvent("onunload", function () {
                P()
            })), x.attributes = i(function (e) {
                return e.className = "i", !e.getAttribute("className")
            }), x.getElementsByTagName = i(function (e) {
                return e.appendChild(n.createComment("")), !e.getElementsByTagName("*").length
            }), x.getElementsByClassName = ve.test(n.getElementsByClassName) && i(function (e) {
                return e.innerHTML = "<div class='a'></div><div class='a i'></div>", e.firstChild.className = "i", 2 === e.getElementsByClassName("i").length
            }), x.getById = i(function (e) {
                return A.appendChild(e).id = $, !n.getElementsByName || !n.getElementsByName($).length
            }), x.getById ? (T.find.ID = function (e, t) {
                if (typeof t.getElementById !== Y && j) {
                    var n = t.getElementById(e);
                    return n && n.parentNode ? [n] : []
                }
            }, T.filter.ID = function (e) {
                var t = e.replace(xe, Te);
                return function (e) {
                    return e.getAttribute("id") === t
                }
            }) : (delete T.find.ID, T.filter.ID = function (e) {
                var t = e.replace(xe, Te);
                return function (e) {
                    var n = typeof e.getAttributeNode !== Y && e.getAttributeNode("id");
                    return n && n.value === t
                }
            }), T.find.TAG = x.getElementsByTagName ? function (e, t) {
                return typeof t.getElementsByTagName !== Y ? t.getElementsByTagName(e) : void 0
            } : function (e, t) {
                var n, o = [], i = 0, r = t.getElementsByTagName(e);
                if ("*" === e) {
                    for (; n = r[i++];) 1 === n.nodeType && o.push(n);
                    return o
                }
                return r
            }, T.find.CLASS = x.getElementsByClassName && function (e, t) {
                return typeof t.getElementsByClassName !== Y && j ? t.getElementsByClassName(e) : void 0
            }, D = [], H = [], (x.qsa = ve.test(n.querySelectorAll)) && (i(function (e) {
                e.innerHTML = "<select msallowclip=''><option selected=''></option></select>", e.querySelectorAll("[msallowclip^='']").length && H.push("[*^$]=" + oe + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || H.push("\\[" + oe + "*(?:value|" + ne + ")"), e.querySelectorAll(":checked").length || H.push(":checked")
            }), i(function (e) {
                var t = n.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && H.push("name" + oe + "*[*^$|!~]?="), e.querySelectorAll(":enabled").length || H.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), H.push(",.*:")
            })), (x.matchesSelector = ve.test(R = A.matches || A.webkitMatchesSelector || A.mozMatchesSelector || A.oMatchesSelector || A.msMatchesSelector)) && i(function (e) {
                x.disconnectedMatch = R.call(e, "div"), R.call(e, "[s!='']:x"), D.push("!=", ae)
            }), H = H.length && new RegExp(H.join("|")), D = D.length && new RegExp(D.join("|")), t = ve.test(A.compareDocumentPosition), O = t || ve.test(A.contains) ? function (e, t) {
                var n = 9 === e.nodeType ? e.documentElement : e, o = t && t.parentNode;
                return e === o || !(!o || 1 !== o.nodeType || !(n.contains ? n.contains(o) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(o)))
            } : function (e, t) {
                if (t) for (; t = t.parentNode;) if (t === e) return !0;
                return !1
            }, X = t ? function (e, t) {
                if (e === t) return _ = !0, 0;
                var o = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return o ? o : (o = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & o || !x.sortDetached && t.compareDocumentPosition(e) === o ? e === n || e.ownerDocument === I && O(I, e) ? -1 : t === n || t.ownerDocument === I && O(I, t) ? 1 : N ? te.call(N, e) - te.call(N, t) : 0 : 4 & o ? -1 : 1)
            } : function (e, t) {
                if (e === t) return _ = !0, 0;
                var o, i = 0, r = e.parentNode, a = t.parentNode, l = [e], c = [t];
                if (!r || !a) return e === n ? -1 : t === n ? 1 : r ? -1 : a ? 1 : N ? te.call(N, e) - te.call(N, t) : 0;
                if (r === a) return s(e, t);
                for (o = e; o = o.parentNode;) l.unshift(o);
                for (o = t; o = o.parentNode;) c.unshift(o);
                for (; l[i] === c[i];) i++;
                return i ? s(l[i], c[i]) : l[i] === I ? -1 : c[i] === I ? 1 : 0
            }, n) : M
        }, t.matches = function (e, n) {
            return t(e, null, null, n)
        }, t.matchesSelector = function (e, n) {
            if ((e.ownerDocument || e) !== M && P(e), n = n.replace(de, "='$1']"), !(!x.matchesSelector || !j || D && D.test(n) || H && H.test(n))) try {
                var o = R.call(e, n);
                if (o || x.disconnectedMatch || e.document && 11 !== e.document.nodeType) return o
            } catch (i) {
            }
            return t(n, M, null, [e]).length > 0
        }, t.contains = function (e, t) {
            return (e.ownerDocument || e) !== M && P(e), O(e, t)
        }, t.attr = function (e, t) {
            (e.ownerDocument || e) !== M && P(e);
            var n = T.attrHandle[t.toLowerCase()],
                o = n && Q.call(T.attrHandle, t.toLowerCase()) ? n(e, t, !j) : void 0;
            return void 0 !== o ? o : x.attributes || !j ? e.getAttribute(t) : (o = e.getAttributeNode(t)) && o.specified ? o.value : null
        }, t.error = function (e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, t.uniqueSort = function (e) {
            var t, n = [], o = 0, i = 0;
            if (_ = !x.detectDuplicates, N = !x.sortStable && e.slice(0), e.sort(X), _) {
                for (; t = e[i++];) t === e[i] && (o = n.push(i));
                for (; o--;) e.splice(n[o], 1)
            }
            return N = null, e
        }, S = t.getText = function (e) {
            var t, n = "", o = 0, i = e.nodeType;
            if (i) {
                if (1 === i || 9 === i || 11 === i) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) n += S(e)
                } else if (3 === i || 4 === i) return e.nodeValue
            } else for (; t = e[o++];) n += S(t);
            return n
        }, T = t.selectors = {
            cacheLength: 50,
            createPseudo: o,
            match: fe,
            attrHandle: {},
            find: {},
            relative: {
                ">": {dir: "parentNode", first: !0},
                " ": {dir: "parentNode"},
                "+": {dir: "previousSibling", first: !0},
                "~": {dir: "previousSibling"}
            },
            preFilter: {
                ATTR: function (e) {
                    return e[1] = e[1].replace(xe, Te), e[3] = (e[3] || e[4] || e[5] || "").replace(xe, Te), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                }, CHILD: function (e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e
                }, PSEUDO: function (e) {
                    var t, n = !e[6] && e[2];
                    return fe.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && he.test(n) && (t = E(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                }
            },
            filter: {
                TAG: function (e) {
                    var t = e.replace(xe, Te).toLowerCase();
                    return "*" === e ? function () {
                        return !0
                    } : function (e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                }, CLASS: function (e) {
                    var t = B[e + " "];
                    return t || (t = new RegExp("(^|" + oe + ")" + e + "(" + oe + "|$)")) && B(e, function (e) {
                        return t.test("string" == typeof e.className && e.className || typeof e.getAttribute !== Y && e.getAttribute("class") || "");
                    })
                }, ATTR: function (e, n, o) {
                    return function (i) {
                        var r = t.attr(i, e);
                        return null == r ? "!=" === n : !n || (r += "", "=" === n ? r === o : "!=" === n ? r !== o : "^=" === n ? o && 0 === r.indexOf(o) : "*=" === n ? o && r.indexOf(o) > -1 : "$=" === n ? o && r.slice(-o.length) === o : "~=" === n ? (" " + r + " ").indexOf(o) > -1 : "|=" === n && (r === o || r.slice(0, o.length + 1) === o + "-"))
                    }
                }, CHILD: function (e, t, n, o, i) {
                    var r = "nth" !== e.slice(0, 3), s = "last" !== e.slice(-4), a = "of-type" === t;
                    return 1 === o && 0 === i ? function (e) {
                        return !!e.parentNode
                    } : function (t, n, l) {
                        var c, u, d, h, p, f, m = r !== s ? "nextSibling" : "previousSibling", g = t.parentNode,
                            v = a && t.nodeName.toLowerCase(), y = !l && !a;
                        if (g) {
                            if (r) {
                                for (; m;) {
                                    for (d = t; d = d[m];) if (a ? d.nodeName.toLowerCase() === v : 1 === d.nodeType) return !1;
                                    f = m = "only" === e && !f && "nextSibling"
                                }
                                return !0
                            }
                            if (f = [s ? g.firstChild : g.lastChild], s && y) {
                                for (u = g[$] || (g[$] = {}), c = u[e] || [], p = c[0] === q && c[1], h = c[0] === q && c[2], d = p && g.childNodes[p]; d = ++p && d && d[m] || (h = p = 0) || f.pop();) if (1 === d.nodeType && ++h && d === t) {
                                    u[e] = [q, p, h];
                                    break
                                }
                            } else if (y && (c = (t[$] || (t[$] = {}))[e]) && c[0] === q) h = c[1]; else for (; (d = ++p && d && d[m] || (h = p = 0) || f.pop()) && ((a ? d.nodeName.toLowerCase() !== v : 1 !== d.nodeType) || !++h || (y && ((d[$] || (d[$] = {}))[e] = [q, h]), d !== t));) ;
                            return h -= i, h === o || h % o === 0 && h / o >= 0
                        }
                    }
                }, PSEUDO: function (e, n) {
                    var i, r = T.pseudos[e] || T.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                    return r[$] ? r(n) : r.length > 1 ? (i = [e, e, "", n], T.setFilters.hasOwnProperty(e.toLowerCase()) ? o(function (e, t) {
                        for (var o, i = r(e, n), s = i.length; s--;) o = te.call(e, i[s]), e[o] = !(t[o] = i[s])
                    }) : function (e) {
                        return r(e, 0, i)
                    }) : r
                }
            },
            pseudos: {
                not: o(function (e) {
                    var t = [], n = [], i = C(e.replace(le, "$1"));
                    return i[$] ? o(function (e, t, n, o) {
                        for (var r, s = i(e, null, o, []), a = e.length; a--;) (r = s[a]) && (e[a] = !(t[a] = r))
                    }) : function (e, o, r) {
                        return t[0] = e, i(t, null, r, n), !n.pop()
                    }
                }), has: o(function (e) {
                    return function (n) {
                        return t(e, n).length > 0
                    }
                }), contains: o(function (e) {
                    return function (t) {
                        return (t.textContent || t.innerText || S(t)).indexOf(e) > -1
                    }
                }), lang: o(function (e) {
                    return pe.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(xe, Te).toLowerCase(), function (t) {
                        var n;
                        do if (n = j ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-"); while ((t = t.parentNode) && 1 === t.nodeType);
                        return !1
                    }
                }), target: function (t) {
                    var n = e.location && e.location.hash;
                    return n && n.slice(1) === t.id
                }, root: function (e) {
                    return e === A
                }, focus: function (e) {
                    return e === M.activeElement && (!M.hasFocus || M.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                }, enabled: function (e) {
                    return e.disabled === !1
                }, disabled: function (e) {
                    return e.disabled === !0
                }, checked: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected
                }, selected: function (e) {
                    return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                }, empty: function (e) {
                    for (e = e.firstChild; e; e = e.nextSibling) if (e.nodeType < 6) return !1;
                    return !0
                }, parent: function (e) {
                    return !T.pseudos.empty(e)
                }, header: function (e) {
                    return ge.test(e.nodeName)
                }, input: function (e) {
                    return me.test(e.nodeName)
                }, button: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                }, text: function (e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                }, first: c(function () {
                    return [0]
                }), last: c(function (e, t) {
                    return [t - 1]
                }), eq: c(function (e, t, n) {
                    return [0 > n ? n + t : n]
                }), even: c(function (e, t) {
                    for (var n = 0; t > n; n += 2) e.push(n);
                    return e
                }), odd: c(function (e, t) {
                    for (var n = 1; t > n; n += 2) e.push(n);
                    return e
                }), lt: c(function (e, t, n) {
                    for (var o = 0 > n ? n + t : n; --o >= 0;) e.push(o);
                    return e
                }), gt: c(function (e, t, n) {
                    for (var o = 0 > n ? n + t : n; ++o < t;) e.push(o);
                    return e
                })
            }
        }, T.pseudos.nth = T.pseudos.eq;
        for (w in{radio: !0, checkbox: !0, file: !0, password: !0, image: !0}) T.pseudos[w] = a(w);
        for (w in{submit: !0, reset: !0}) T.pseudos[w] = l(w);
        return d.prototype = T.filters = T.pseudos, T.setFilters = new d, E = t.tokenize = function (e, n) {
            var o, i, r, s, a, l, c, u = F[e + " "];
            if (u) return n ? 0 : u.slice(0);
            for (a = e, l = [], c = T.preFilter; a;) {
                (!o || (i = ce.exec(a))) && (i && (a = a.slice(i[0].length) || a), l.push(r = [])), o = !1, (i = ue.exec(a)) && (o = i.shift(), r.push({
                    value: o,
                    type: i[0].replace(le, " ")
                }), a = a.slice(o.length));
                for (s in T.filter) !(i = fe[s].exec(a)) || c[s] && !(i = c[s](i)) || (o = i.shift(), r.push({
                    value: o,
                    type: s,
                    matches: i
                }), a = a.slice(o.length));
                if (!o) break
            }
            return n ? a.length : a ? t.error(e) : F(e, l).slice(0)
        }, C = t.compile = function (e, t) {
            var n, o = [], i = [], r = V[e + " "];
            if (!r) {
                for (t || (t = E(e)), n = t.length; n--;) r = y(t[n]), r[$] ? o.push(r) : i.push(r);
                r = V(e, b(i, o)), r.selector = e
            }
            return r
        }, z = t.select = function (e, t, n, o) {
            var i, r, s, a, l, c = "function" == typeof e && e, d = !o && E(e = c.selector || e);
            if (n = n || [], 1 === d.length) {
                if (r = d[0] = d[0].slice(0), r.length > 2 && "ID" === (s = r[0]).type && x.getById && 9 === t.nodeType && j && T.relative[r[1].type]) {
                    if (t = (T.find.ID(s.matches[0].replace(xe, Te), t) || [])[0], !t) return n;
                    c && (t = t.parentNode), e = e.slice(r.shift().value.length)
                }
                for (i = fe.needsContext.test(e) ? 0 : r.length; i-- && (s = r[i], !T.relative[a = s.type]);) if ((l = T.find[a]) && (o = l(s.matches[0].replace(xe, Te), be.test(r[0].type) && u(t.parentNode) || t))) {
                    if (r.splice(i, 1), e = o.length && h(r), !e) return K.apply(n, o), n;
                    break
                }
            }
            return (c || C(e, d))(o, t, !j, n, be.test(e) && u(t.parentNode) || t), n
        }, x.sortStable = $.split("").sort(X).join("") === $, x.detectDuplicates = !!_, P(), x.sortDetached = i(function (e) {
            return 1 & e.compareDocumentPosition(M.createElement("div"))
        }), i(function (e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || r("type|href|height|width", function (e, t, n) {
            return n ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), x.attributes && i(function (e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || r("value", function (e, t, n) {
            return n || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue
        }), i(function (e) {
            return null == e.getAttribute("disabled")
        }) || r(ne, function (e, t, n) {
            var o;
            return n ? void 0 : e[t] === !0 ? t.toLowerCase() : (o = e.getAttributeNode(t)) && o.specified ? o.value : null
        }), t
    }(e);
    ie.find = ce, ie.expr = ce.selectors, ie.expr[":"] = ie.expr.pseudos, ie.unique = ce.uniqueSort, ie.text = ce.getText, ie.isXMLDoc = ce.isXML, ie.contains = ce.contains;
    var ue = ie.expr.match.needsContext, de = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, he = /^.[^:#\[\.,]*$/;
    ie.filter = function (e, t, n) {
        var o = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === o.nodeType ? ie.find.matchesSelector(o, e) ? [o] : [] : ie.find.matches(e, ie.grep(t, function (e) {
            return 1 === e.nodeType
        }))
    }, ie.fn.extend({
        find: function (e) {
            var t, n = [], o = this, i = o.length;
            if ("string" != typeof e) return this.pushStack(ie(e).filter(function () {
                for (t = 0; i > t; t++) if (ie.contains(o[t], this)) return !0
            }));
            for (t = 0; i > t; t++) ie.find(e, o[t], n);
            return n = this.pushStack(i > 1 ? ie.unique(n) : n), n.selector = this.selector ? this.selector + " " + e : e, n
        }, filter: function (e) {
            return this.pushStack(o(this, e || [], !1))
        }, not: function (e) {
            return this.pushStack(o(this, e || [], !0))
        }, is: function (e) {
            return !!o(this, "string" == typeof e && ue.test(e) ? ie(e) : e || [], !1).length
        }
    });
    var pe, fe = e.document, me = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, ge = ie.fn.init = function (e, t) {
        var n, o;
        if (!e) return this;
        if ("string" == typeof e) {
            if (n = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : me.exec(e), !n || !n[1] && t) return !t || t.jquery ? (t || pe).find(e) : this.constructor(t).find(e);
            if (n[1]) {
                if (t = t instanceof ie ? t[0] : t, ie.merge(this, ie.parseHTML(n[1], t && t.nodeType ? t.ownerDocument || t : fe, !0)), de.test(n[1]) && ie.isPlainObject(t)) for (n in t) ie.isFunction(this[n]) ? this[n](t[n]) : this.attr(n, t[n]);
                return this
            }
            if (o = fe.getElementById(n[2]), o && o.parentNode) {
                if (o.id !== n[2]) return pe.find(e);
                this.length = 1, this[0] = o
            }
            return this.context = fe, this.selector = e, this
        }
        return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : ie.isFunction(e) ? "undefined" != typeof pe.ready ? pe.ready(e) : e(ie) : (void 0 !== e.selector && (this.selector = e.selector, this.context = e.context), ie.makeArray(e, this))
    };
    ge.prototype = ie.fn, pe = ie(fe);
    var ve = /^(?:parents|prev(?:Until|All))/, ye = {children: !0, contents: !0, next: !0, prev: !0};
    ie.extend({
        dir: function (e, t, n) {
            for (var o = [], i = e[t]; i && 9 !== i.nodeType && (void 0 === n || 1 !== i.nodeType || !ie(i).is(n));) 1 === i.nodeType && o.push(i), i = i[t];
            return o
        }, sibling: function (e, t) {
            for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
            return n
        }
    }), ie.fn.extend({
        has: function (e) {
            var t, n = ie(e, this), o = n.length;
            return this.filter(function () {
                for (t = 0; o > t; t++) if (ie.contains(this, n[t])) return !0
            })
        }, closest: function (e, t) {
            for (var n, o = 0, i = this.length, r = [], s = ue.test(e) || "string" != typeof e ? ie(e, t || this.context) : 0; i > o; o++) for (n = this[o]; n && n !== t; n = n.parentNode) if (n.nodeType < 11 && (s ? s.index(n) > -1 : 1 === n.nodeType && ie.find.matchesSelector(n, e))) {
                r.push(n);
                break
            }
            return this.pushStack(r.length > 1 ? ie.unique(r) : r)
        }, index: function (e) {
            return e ? "string" == typeof e ? ie.inArray(this[0], ie(e)) : ie.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        }, add: function (e, t) {
            return this.pushStack(ie.unique(ie.merge(this.get(), ie(e, t))))
        }, addBack: function (e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), ie.each({
        parent: function (e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        }, parents: function (e) {
            return ie.dir(e, "parentNode")
        }, parentsUntil: function (e, t, n) {
            return ie.dir(e, "parentNode", n)
        }, next: function (e) {
            return i(e, "nextSibling")
        }, prev: function (e) {
            return i(e, "previousSibling")
        }, nextAll: function (e) {
            return ie.dir(e, "nextSibling")
        }, prevAll: function (e) {
            return ie.dir(e, "previousSibling")
        }, nextUntil: function (e, t, n) {
            return ie.dir(e, "nextSibling", n)
        }, prevUntil: function (e, t, n) {
            return ie.dir(e, "previousSibling", n)
        }, siblings: function (e) {
            return ie.sibling((e.parentNode || {}).firstChild, e)
        }, children: function (e) {
            return ie.sibling(e.firstChild)
        }, contents: function (e) {
            return ie.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : ie.merge([], e.childNodes)
        }
    }, function (e, t) {
        ie.fn[e] = function (n, o) {
            var i = ie.map(this, t, n);
            return "Until" !== e.slice(-5) && (o = n), o && "string" == typeof o && (i = ie.filter(o, i)), this.length > 1 && (ye[e] || (i = ie.unique(i)), ve.test(e) && (i = i.reverse())), this.pushStack(i)
        }
    });
    var be = /\S+/g, we = {};
    ie.Callbacks = function (e) {
        e = "string" == typeof e ? we[e] || r(e) : ie.extend({}, e);
        var t, n, o, i, s, a, l = [], c = !e.once && [], u = function (r) {
            for (n = e.memory && r, o = !0, s = a || 0, a = 0, i = l.length, t = !0; l && i > s; s++) if (l[s].apply(r[0], r[1]) === !1 && e.stopOnFalse) {
                n = !1;
                break
            }
            t = !1, l && (c ? c.length && u(c.shift()) : n ? l = [] : d.disable())
        }, d = {
            add: function () {
                if (l) {
                    var o = l.length;
                    !function r(t) {
                        ie.each(t, function (t, n) {
                            var o = ie.type(n);
                            "function" === o ? e.unique && d.has(n) || l.push(n) : n && n.length && "string" !== o && r(n)
                        })
                    }(arguments), t ? i = l.length : n && (a = o, u(n))
                }
                return this
            }, remove: function () {
                return l && ie.each(arguments, function (e, n) {
                    for (var o; (o = ie.inArray(n, l, o)) > -1;) l.splice(o, 1), t && (i >= o && i--, s >= o && s--)
                }), this
            }, has: function (e) {
                return e ? ie.inArray(e, l) > -1 : !(!l || !l.length)
            }, empty: function () {
                return l = [], i = 0, this
            }, disable: function () {
                return l = c = n = void 0, this
            }, disabled: function () {
                return !l
            }, lock: function () {
                return c = void 0, n || d.disable(), this
            }, locked: function () {
                return !c
            }, fireWith: function (e, n) {
                return !l || o && !c || (n = n || [], n = [e, n.slice ? n.slice() : n], t ? c.push(n) : u(n)), this
            }, fire: function () {
                return d.fireWith(this, arguments), this
            }, fired: function () {
                return !!o
            }
        };
        return d
    }, ie.extend({
        Deferred: function (e) {
            var t = [["resolve", "done", ie.Callbacks("once memory"), "resolved"], ["reject", "fail", ie.Callbacks("once memory"), "rejected"], ["notify", "progress", ie.Callbacks("memory")]],
                n = "pending", o = {
                    state: function () {
                        return n
                    }, always: function () {
                        return i.done(arguments).fail(arguments), this
                    }, then: function () {
                        var e = arguments;
                        return ie.Deferred(function (n) {
                            ie.each(t, function (t, r) {
                                var s = ie.isFunction(e[t]) && e[t];
                                i[r[1]](function () {
                                    var e = s && s.apply(this, arguments);
                                    e && ie.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[r[0] + "With"](this === o ? n.promise() : this, s ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    }, promise: function (e) {
                        return null != e ? ie.extend(e, o) : o
                    }
                }, i = {};
            return o.pipe = o.then, ie.each(t, function (e, r) {
                var s = r[2], a = r[3];
                o[r[1]] = s.add, a && s.add(function () {
                    n = a
                }, t[1 ^ e][2].disable, t[2][2].lock), i[r[0]] = function () {
                    return i[r[0] + "With"](this === i ? o : this, arguments), this
                }, i[r[0] + "With"] = s.fireWith
            }), o.promise(i), e && e.call(i, i), i
        }, when: function (e) {
            var t, n, o, i = 0, r = Q.call(arguments), s = r.length,
                a = 1 !== s || e && ie.isFunction(e.promise) ? s : 0, l = 1 === a ? e : ie.Deferred(),
                c = function (e, n, o) {
                    return function (i) {
                        n[e] = this, o[e] = arguments.length > 1 ? Q.call(arguments) : i, o === t ? l.notifyWith(n, o) : --a || l.resolveWith(n, o)
                    }
                };
            if (s > 1) for (t = new Array(s), n = new Array(s), o = new Array(s); s > i; i++) r[i] && ie.isFunction(r[i].promise) ? r[i].promise().done(c(i, o, r)).fail(l.reject).progress(c(i, n, t)) : --a;
            return a || l.resolveWith(o, r), l.promise()
        }
    });
    var xe;
    ie.fn.ready = function (e) {
        return ie.ready.promise().done(e), this
    }, ie.extend({
        isReady: !1, readyWait: 1, holdReady: function (e) {
            e ? ie.readyWait++ : ie.ready(!0)
        }, ready: function (e) {
            if (e === !0 ? !--ie.readyWait : !ie.isReady) {
                if (!fe.body) return setTimeout(ie.ready);
                ie.isReady = !0, e !== !0 && --ie.readyWait > 0 || (xe.resolveWith(fe, [ie]), ie.fn.triggerHandler && (ie(fe).triggerHandler("ready"), ie(fe).off("ready")))
            }
        }
    }), ie.ready.promise = function (t) {
        if (!xe) if (xe = ie.Deferred(), "complete" === fe.readyState) setTimeout(ie.ready); else if (fe.addEventListener) fe.addEventListener("DOMContentLoaded", a, !1), e.addEventListener("load", a, !1); else {
            fe.attachEvent("onreadystatechange", a), e.attachEvent("onload", a);
            var n = !1;
            try {
                n = null == e.frameElement && fe.documentElement
            } catch (o) {
            }
            n && n.doScroll && !function i() {
                if (!ie.isReady) {
                    try {
                        n.doScroll("left")
                    } catch (e) {
                        return setTimeout(i, 50)
                    }
                    s(), ie.ready()
                }
            }()
        }
        return xe.promise(t)
    };
    var Te, Se = "undefined";
    for (Te in ie(ne)) break;
    ne.ownLast = "0" !== Te, ne.inlineBlockNeedsLayout = !1, ie(function () {
        var e, t, n, o;
        n = fe.getElementsByTagName("body")[0], n && n.style && (t = fe.createElement("div"), o = fe.createElement("div"), o.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(o).appendChild(t), typeof t.style.zoom !== Se && (t.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", ne.inlineBlockNeedsLayout = e = 3 === t.offsetWidth, e && (n.style.zoom = 1)), n.removeChild(o))
    }), function () {
        var e = fe.createElement("div");
        if (null == ne.deleteExpando) {
            ne.deleteExpando = !0;
            try {
                delete e.test
            } catch (t) {
                ne.deleteExpando = !1
            }
        }
        e = null
    }(), ie.acceptData = function (e) {
        var t = ie.noData[(e.nodeName + " ").toLowerCase()], n = +e.nodeType || 1;
        return (1 === n || 9 === n) && (!t || t !== !0 && e.getAttribute("classid") === t)
    };
    var ke = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, Ee = /([A-Z])/g;
    ie.extend({
        cache: {},
        noData: {"applet ": !0, "embed ": !0, "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},
        hasData: function (e) {
            return e = e.nodeType ? ie.cache[e[ie.expando]] : e[ie.expando], !!e && !c(e)
        },
        data: function (e, t, n) {
            return u(e, t, n)
        },
        removeData: function (e, t) {
            return d(e, t)
        },
        _data: function (e, t, n) {
            return u(e, t, n, !0)
        },
        _removeData: function (e, t) {
            return d(e, t, !0)
        }
    }), ie.fn.extend({
        data: function (e, t) {
            var n, o, i, r = this[0], s = r && r.attributes;
            if (void 0 === e) {
                if (this.length && (i = ie.data(r), 1 === r.nodeType && !ie._data(r, "parsedAttrs"))) {
                    for (n = s.length; n--;) s[n] && (o = s[n].name, 0 === o.indexOf("data-") && (o = ie.camelCase(o.slice(5)), l(r, o, i[o])));
                    ie._data(r, "parsedAttrs", !0)
                }
                return i
            }
            return "object" == typeof e ? this.each(function () {
                ie.data(this, e)
            }) : arguments.length > 1 ? this.each(function () {
                ie.data(this, e, t)
            }) : r ? l(r, e, ie.data(r, e)) : void 0
        }, removeData: function (e) {
            return this.each(function () {
                ie.removeData(this, e)
            })
        }
    }), ie.extend({
        queue: function (e, t, n) {
            var o;
            return e ? (t = (t || "fx") + "queue", o = ie._data(e, t), n && (!o || ie.isArray(n) ? o = ie._data(e, t, ie.makeArray(n)) : o.push(n)), o || []) : void 0
        }, dequeue: function (e, t) {
            t = t || "fx";
            var n = ie.queue(e, t), o = n.length, i = n.shift(), r = ie._queueHooks(e, t), s = function () {
                ie.dequeue(e, t)
            };
            "inprogress" === i && (i = n.shift(), o--), i && ("fx" === t && n.unshift("inprogress"), delete r.stop, i.call(e, s, r)), !o && r && r.empty.fire()
        }, _queueHooks: function (e, t) {
            var n = t + "queueHooks";
            return ie._data(e, n) || ie._data(e, n, {
                empty: ie.Callbacks("once memory").add(function () {
                    ie._removeData(e, t + "queue"), ie._removeData(e, n)
                })
            })
        }
    }), ie.fn.extend({
        queue: function (e, t) {
            var n = 2;
            return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? ie.queue(this[0], e) : void 0 === t ? this : this.each(function () {
                var n = ie.queue(this, e, t);
                ie._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && ie.dequeue(this, e)
            })
        }, dequeue: function (e) {
            return this.each(function () {
                ie.dequeue(this, e)
            })
        }, clearQueue: function (e) {
            return this.queue(e || "fx", [])
        }, promise: function (e, t) {
            var n, o = 1, i = ie.Deferred(), r = this, s = this.length, a = function () {
                --o || i.resolveWith(r, [r])
            };
            for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; s--;) n = ie._data(r[s], e + "queueHooks"), n && n.empty && (o++, n.empty.add(a));
            return a(), i.promise(t)
        }
    });
    var Ce = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, ze = ["Top", "Right", "Bottom", "Left"],
        Le = function (e, t) {
            return e = t || e, "none" === ie.css(e, "display") || !ie.contains(e.ownerDocument, e)
        }, Ne = ie.access = function (e, t, n, o, i, r, s) {
            var a = 0, l = e.length, c = null == n;
            if ("object" === ie.type(n)) {
                i = !0;
                for (a in n) ie.access(e, t, a, n[a], !0, r, s)
            } else if (void 0 !== o && (i = !0, ie.isFunction(o) || (s = !0), c && (s ? (t.call(e, o), t = null) : (c = t, t = function (e, t, n) {
                return c.call(ie(e), n)
            })), t)) for (; l > a; a++) t(e[a], n, s ? o : o.call(e[a], a, t(e[a], n)));
            return i ? e : c ? t.call(e) : l ? t(e[0], n) : r
        }, _e = /^(?:checkbox|radio)$/i;
    !function () {
        var e = fe.createElement("input"), t = fe.createElement("div"), n = fe.createDocumentFragment();
        if (t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", ne.leadingWhitespace = 3 === t.firstChild.nodeType, ne.tbody = !t.getElementsByTagName("tbody").length, ne.htmlSerialize = !!t.getElementsByTagName("link").length, ne.html5Clone = "<:nav></:nav>" !== fe.createElement("nav").cloneNode(!0).outerHTML, e.type = "checkbox", e.checked = !0, n.appendChild(e), ne.appendChecked = e.checked, t.innerHTML = "<textarea>x</textarea>", ne.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue, n.appendChild(t), t.innerHTML = "<input type='radio' checked='checked' name='t'/>", ne.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, ne.noCloneEvent = !0, t.attachEvent && (t.attachEvent("onclick", function () {
            ne.noCloneEvent = !1
        }), t.cloneNode(!0).click()), null == ne.deleteExpando) {
            ne.deleteExpando = !0;
            try {
                delete t.test
            } catch (o) {
                ne.deleteExpando = !1
            }
        }
    }(), function () {
        var t, n, o = fe.createElement("div");
        for (t in{
            submit: !0,
            change: !0,
            focusin: !0
        }) n = "on" + t, (ne[t + "Bubbles"] = n in e) || (o.setAttribute(n, "t"), ne[t + "Bubbles"] = o.attributes[n].expando === !1);
        o = null
    }();
    var Pe = /^(?:input|select|textarea)$/i, Me = /^key/, Ae = /^(?:mouse|pointer|contextmenu)|click/,
        je = /^(?:focusinfocus|focusoutblur)$/, He = /^([^.]*)(?:\.(.+)|)$/;
    ie.event = {
        global: {},
        add: function (e, t, n, o, i) {
            var r, s, a, l, c, u, d, h, p, f, m, g = ie._data(e);
            if (g) {
                for (n.handler && (l = n, n = l.handler, i = l.selector), n.guid || (n.guid = ie.guid++), (s = g.events) || (s = g.events = {}), (u = g.handle) || (u = g.handle = function (e) {
                    return typeof ie === Se || e && ie.event.triggered === e.type ? void 0 : ie.event.dispatch.apply(u.elem, arguments)
                }, u.elem = e), t = (t || "").match(be) || [""], a = t.length; a--;) r = He.exec(t[a]) || [], p = m = r[1], f = (r[2] || "").split(".").sort(), p && (c = ie.event.special[p] || {}, p = (i ? c.delegateType : c.bindType) || p, c = ie.event.special[p] || {}, d = ie.extend({
                    type: p,
                    origType: m,
                    data: o,
                    handler: n,
                    guid: n.guid,
                    selector: i,
                    needsContext: i && ie.expr.match.needsContext.test(i),
                    namespace: f.join(".")
                }, l), (h = s[p]) || (h = s[p] = [], h.delegateCount = 0, c.setup && c.setup.call(e, o, f, u) !== !1 || (e.addEventListener ? e.addEventListener(p, u, !1) : e.attachEvent && e.attachEvent("on" + p, u))), c.add && (c.add.call(e, d), d.handler.guid || (d.handler.guid = n.guid)), i ? h.splice(h.delegateCount++, 0, d) : h.push(d), ie.event.global[p] = !0);
                e = null
            }
        },
        remove: function (e, t, n, o, i) {
            var r, s, a, l, c, u, d, h, p, f, m, g = ie.hasData(e) && ie._data(e);
            if (g && (u = g.events)) {
                for (t = (t || "").match(be) || [""], c = t.length; c--;) if (a = He.exec(t[c]) || [], p = m = a[1], f = (a[2] || "").split(".").sort(), p) {
                    for (d = ie.event.special[p] || {}, p = (o ? d.delegateType : d.bindType) || p, h = u[p] || [], a = a[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), l = r = h.length; r--;) s = h[r], !i && m !== s.origType || n && n.guid !== s.guid || a && !a.test(s.namespace) || o && o !== s.selector && ("**" !== o || !s.selector) || (h.splice(r, 1), s.selector && h.delegateCount--, d.remove && d.remove.call(e, s));
                    l && !h.length && (d.teardown && d.teardown.call(e, f, g.handle) !== !1 || ie.removeEvent(e, p, g.handle), delete u[p])
                } else for (p in u) ie.event.remove(e, p + t[c], n, o, !0);
                ie.isEmptyObject(u) && (delete g.handle, ie._removeData(e, "events"))
            }
        },
        trigger: function (t, n, o, i) {
            var r, s, a, l, c, u, d, h = [o || fe], p = te.call(t, "type") ? t.type : t,
                f = te.call(t, "namespace") ? t.namespace.split(".") : [];
            if (a = u = o = o || fe, 3 !== o.nodeType && 8 !== o.nodeType && !je.test(p + ie.event.triggered) && (p.indexOf(".") >= 0 && (f = p.split("."), p = f.shift(), f.sort()), s = p.indexOf(":") < 0 && "on" + p, t = t[ie.expando] ? t : new ie.Event(p, "object" == typeof t && t), t.isTrigger = i ? 2 : 3, t.namespace = f.join("."), t.namespace_re = t.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = o), n = null == n ? [t] : ie.makeArray(n, [t]), c = ie.event.special[p] || {}, i || !c.trigger || c.trigger.apply(o, n) !== !1)) {
                if (!i && !c.noBubble && !ie.isWindow(o)) {
                    for (l = c.delegateType || p, je.test(l + p) || (a = a.parentNode); a; a = a.parentNode) h.push(a), u = a;
                    u === (o.ownerDocument || fe) && h.push(u.defaultView || u.parentWindow || e)
                }
                for (d = 0; (a = h[d++]) && !t.isPropagationStopped();) t.type = d > 1 ? l : c.bindType || p, r = (ie._data(a, "events") || {})[t.type] && ie._data(a, "handle"), r && r.apply(a, n), r = s && a[s], r && r.apply && ie.acceptData(a) && (t.result = r.apply(a, n), t.result === !1 && t.preventDefault());
                if (t.type = p, !i && !t.isDefaultPrevented() && (!c._default || c._default.apply(h.pop(), n) === !1) && ie.acceptData(o) && s && o[p] && !ie.isWindow(o)) {
                    u = o[s], u && (o[s] = null), ie.event.triggered = p;
                    try {
                        o[p]()
                    } catch (m) {
                    }
                    ie.event.triggered = void 0, u && (o[s] = u)
                }
                return t.result
            }
        },
        dispatch: function (e) {
            e = ie.event.fix(e);
            var t, n, o, i, r, s = [], a = Q.call(arguments), l = (ie._data(this, "events") || {})[e.type] || [],
                c = ie.event.special[e.type] || {};
            if (a[0] = e, e.delegateTarget = this, !c.preDispatch || c.preDispatch.call(this, e) !== !1) {
                for (s = ie.event.handlers.call(this, e, l), t = 0; (i = s[t++]) && !e.isPropagationStopped();) for (e.currentTarget = i.elem, r = 0; (o = i.handlers[r++]) && !e.isImmediatePropagationStopped();) (!e.namespace_re || e.namespace_re.test(o.namespace)) && (e.handleObj = o, e.data = o.data, n = ((ie.event.special[o.origType] || {}).handle || o.handler).apply(i.elem, a), void 0 !== n && (e.result = n) === !1 && (e.preventDefault(), e.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, e), e.result
            }
        },
        handlers: function (e, t) {
            var n, o, i, r, s = [], a = t.delegateCount, l = e.target;
            if (a && l.nodeType && (!e.button || "click" !== e.type)) for (; l != this; l = l.parentNode || this) if (1 === l.nodeType && (l.disabled !== !0 || "click" !== e.type)) {
                for (i = [], r = 0; a > r; r++) o = t[r], n = o.selector + " ", void 0 === i[n] && (i[n] = o.needsContext ? ie(n, this).index(l) >= 0 : ie.find(n, this, null, [l]).length), i[n] && i.push(o);
                i.length && s.push({elem: l, handlers: i})
            }
            return a < t.length && s.push({elem: this, handlers: t.slice(a)}), s
        },
        fix: function (e) {
            if (e[ie.expando]) return e;
            var t, n, o, i = e.type, r = e, s = this.fixHooks[i];
            for (s || (this.fixHooks[i] = s = Ae.test(i) ? this.mouseHooks : Me.test(i) ? this.keyHooks : {}), o = s.props ? this.props.concat(s.props) : this.props, e = new ie.Event(r), t = o.length; t--;) n = o[t], e[n] = r[n];
            return e.target || (e.target = r.srcElement || fe), 3 === e.target.nodeType && (e.target = e.target.parentNode), e.metaKey = !!e.metaKey, s.filter ? s.filter(e, r) : e
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "), filter: function (e, t) {
                return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function (e, t) {
                var n, o, i, r = t.button, s = t.fromElement;
                return null == e.pageX && null != t.clientX && (o = e.target.ownerDocument || fe, i = o.documentElement, n = o.body, e.pageX = t.clientX + (i && i.scrollLeft || n && n.scrollLeft || 0) - (i && i.clientLeft || n && n.clientLeft || 0), e.pageY = t.clientY + (i && i.scrollTop || n && n.scrollTop || 0) - (i && i.clientTop || n && n.clientTop || 0)), !e.relatedTarget && s && (e.relatedTarget = s === e.target ? t.toElement : s), e.which || void 0 === r || (e.which = 1 & r ? 1 : 2 & r ? 3 : 4 & r ? 2 : 0), e
            }
        },
        special: {
            load: {noBubble: !0}, focus: {
                trigger: function () {
                    if (this !== f() && this.focus) try {
                        return this.focus(), !1
                    } catch (e) {
                    }
                }, delegateType: "focusin"
            }, blur: {
                trigger: function () {
                    return this === f() && this.blur ? (this.blur(), !1) : void 0
                }, delegateType: "focusout"
            }, click: {
                trigger: function () {
                    return ie.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
                }, _default: function (e) {
                    return ie.nodeName(e.target, "a")
                }
            }, beforeunload: {
                postDispatch: function (e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                }
            }
        },
        simulate: function (e, t, n, o) {
            var i = ie.extend(new ie.Event, n, {type: e, isSimulated: !0, originalEvent: {}});
            o ? ie.event.trigger(i, null, t) : ie.event.dispatch.call(t, i), i.isDefaultPrevented() && n.preventDefault()
        }
    }, ie.removeEvent = fe.removeEventListener ? function (e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n, !1)
    } : function (e, t, n) {
        var o = "on" + t;
        e.detachEvent && (typeof e[o] === Se && (e[o] = null), e.detachEvent(o, n))
    }, ie.Event = function (e, t) {
        return this instanceof ie.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && e.returnValue === !1 ? h : p) : this.type = e, t && ie.extend(this, t), this.timeStamp = e && e.timeStamp || ie.now(), void(this[ie.expando] = !0)) : new ie.Event(e, t)
    }, ie.Event.prototype = {
        isDefaultPrevented: p,
        isPropagationStopped: p,
        isImmediatePropagationStopped: p,
        preventDefault: function () {
            var e = this.originalEvent;
            this.isDefaultPrevented = h, e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
        },
        stopPropagation: function () {
            var e = this.originalEvent;
            this.isPropagationStopped = h, e && (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0)
        },
        stopImmediatePropagation: function () {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = h, e && e.stopImmediatePropagation && e.stopImmediatePropagation(), this.stopPropagation()
        }
    }, ie.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function (e, t) {
        ie.event.special[e] = {
            delegateType: t, bindType: t, handle: function (e) {
                var n, o = this, i = e.relatedTarget, r = e.handleObj;
                return (!i || i !== o && !ie.contains(o, i)) && (e.type = r.origType, n = r.handler.apply(this, arguments), e.type = t), n
            }
        }
    }), ne.submitBubbles || (ie.event.special.submit = {
        setup: function () {
            return !ie.nodeName(this, "form") && void ie.event.add(this, "click._submit keypress._submit", function (e) {
                var t = e.target, n = ie.nodeName(t, "input") || ie.nodeName(t, "button") ? t.form : void 0;
                n && !ie._data(n, "submitBubbles") && (ie.event.add(n, "submit._submit", function (e) {
                    e._submit_bubble = !0
                }), ie._data(n, "submitBubbles", !0))
            })
        }, postDispatch: function (e) {
            e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && ie.event.simulate("submit", this.parentNode, e, !0))
        }, teardown: function () {
            return !ie.nodeName(this, "form") && void ie.event.remove(this, "._submit")
        }
    }), ne.changeBubbles || (ie.event.special.change = {
        setup: function () {
            return Pe.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (ie.event.add(this, "propertychange._change", function (e) {
                "checked" === e.originalEvent.propertyName && (this._just_changed = !0)
            }), ie.event.add(this, "click._change", function (e) {
                this._just_changed && !e.isTrigger && (this._just_changed = !1), ie.event.simulate("change", this, e, !0)
            })), !1) : void ie.event.add(this, "beforeactivate._change", function (e) {
                var t = e.target;
                Pe.test(t.nodeName) && !ie._data(t, "changeBubbles") && (ie.event.add(t, "change._change", function (e) {
                    !this.parentNode || e.isSimulated || e.isTrigger || ie.event.simulate("change", this.parentNode, e, !0)
                }), ie._data(t, "changeBubbles", !0))
            })
        }, handle: function (e) {
            var t = e.target;
            return this !== t || e.isSimulated || e.isTrigger || "radio" !== t.type && "checkbox" !== t.type ? e.handleObj.handler.apply(this, arguments) : void 0
        }, teardown: function () {
            return ie.event.remove(this, "._change"), !Pe.test(this.nodeName)
        }
    }), ne.focusinBubbles || ie.each({focus: "focusin", blur: "focusout"}, function (e, t) {
        var n = function (e) {
            ie.event.simulate(t, e.target, ie.event.fix(e), !0)
        };
        ie.event.special[t] = {
            setup: function () {
                var o = this.ownerDocument || this, i = ie._data(o, t);
                i || o.addEventListener(e, n, !0), ie._data(o, t, (i || 0) + 1)
            }, teardown: function () {
                var o = this.ownerDocument || this, i = ie._data(o, t) - 1;
                i ? ie._data(o, t, i) : (o.removeEventListener(e, n, !0), ie._removeData(o, t))
            }
        }
    }), ie.fn.extend({
        on: function (e, t, n, o, i) {
            var r, s;
            if ("object" == typeof e) {
                "string" != typeof t && (n = n || t, t = void 0);
                for (r in e) this.on(r, t, n, e[r], i);
                return this
            }
            if (null == n && null == o ? (o = t, n = t = void 0) : null == o && ("string" == typeof t ? (o = n, n = void 0) : (o = n, n = t, t = void 0)), o === !1) o = p; else if (!o) return this;
            return 1 === i && (s = o, o = function (e) {
                return ie().off(e), s.apply(this, arguments)
            }, o.guid = s.guid || (s.guid = ie.guid++)), this.each(function () {
                ie.event.add(this, e, o, n, t)
            })
        }, one: function (e, t, n, o) {
            return this.on(e, t, n, o, 1)
        }, off: function (e, t, n) {
            var o, i;
            if (e && e.preventDefault && e.handleObj) return o = e.handleObj, ie(e.delegateTarget).off(o.namespace ? o.origType + "." + o.namespace : o.origType, o.selector, o.handler), this;
            if ("object" == typeof e) {
                for (i in e) this.off(i, t, e[i]);
                return this
            }
            return (t === !1 || "function" == typeof t) && (n = t, t = void 0), n === !1 && (n = p), this.each(function () {
                ie.event.remove(this, e, n, t)
            })
        }, trigger: function (e, t) {
            return this.each(function () {
                ie.event.trigger(e, t, this)
            })
        }, triggerHandler: function (e, t) {
            var n = this[0];
            return n ? ie.event.trigger(e, t, n, !0) : void 0
        }
    });
    var De = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        Re = / jQuery\d+="(?:null|\d+)"/g, Oe = new RegExp("<(?:" + De + ")[\\s/>]", "i"), $e = /^\s+/,
        Ie = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, qe = /<([\w:]+)/,
        We = /<tbody/i, Be = /<|&#?\w+;/, Fe = /<(?:script|style|link)/i, Ve = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Xe = /^$|\/(?:java|ecma)script/i, Ye = /^true\/(.*)/, Ue = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, Qe = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: ne.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        }, Ze = m(fe), Ge = Ze.appendChild(fe.createElement("div"));
    Qe.optgroup = Qe.option, Qe.tbody = Qe.tfoot = Qe.colgroup = Qe.caption = Qe.thead, Qe.th = Qe.td, ie.extend({
        clone: function (e, t, n) {
            var o, i, r, s, a, l = ie.contains(e.ownerDocument, e);
            if (ne.html5Clone || ie.isXMLDoc(e) || !Oe.test("<" + e.nodeName + ">") ? r = e.cloneNode(!0) : (Ge.innerHTML = e.outerHTML, Ge.removeChild(r = Ge.firstChild)), !(ne.noCloneEvent && ne.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || ie.isXMLDoc(e))) for (o = g(r), a = g(e), s = 0; null != (i = a[s]); ++s) o[s] && S(i, o[s]);
            if (t) if (n) for (a = a || g(e), o = o || g(r), s = 0; null != (i = a[s]); s++) T(i, o[s]); else T(e, r);
            return o = g(r, "script"), o.length > 0 && x(o, !l && g(e, "script")), o = a = i = null, r
        }, buildFragment: function (e, t, n, o) {
            for (var i, r, s, a, l, c, u, d = e.length, h = m(t), p = [], f = 0; d > f; f++) if (r = e[f], r || 0 === r) if ("object" === ie.type(r)) ie.merge(p, r.nodeType ? [r] : r); else if (Be.test(r)) {
                for (a = a || h.appendChild(t.createElement("div")), l = (qe.exec(r) || ["", ""])[1].toLowerCase(), u = Qe[l] || Qe._default, a.innerHTML = u[1] + r.replace(Ie, "<$1></$2>") + u[2], i = u[0]; i--;) a = a.lastChild;
                if (!ne.leadingWhitespace && $e.test(r) && p.push(t.createTextNode($e.exec(r)[0])), !ne.tbody) for (r = "table" !== l || We.test(r) ? "<table>" !== u[1] || We.test(r) ? 0 : a : a.firstChild, i = r && r.childNodes.length; i--;) ie.nodeName(c = r.childNodes[i], "tbody") && !c.childNodes.length && r.removeChild(c);
                for (ie.merge(p, a.childNodes), a.textContent = ""; a.firstChild;) a.removeChild(a.firstChild);
                a = h.lastChild
            } else p.push(t.createTextNode(r));
            for (a && h.removeChild(a), ne.appendChecked || ie.grep(g(p, "input"), v), f = 0; r = p[f++];) if ((!o || -1 === ie.inArray(r, o)) && (s = ie.contains(r.ownerDocument, r), a = g(h.appendChild(r), "script"), s && x(a), n)) for (i = 0; r = a[i++];) Xe.test(r.type || "") && n.push(r);
            return a = null, h
        }, cleanData: function (e, t) {
            for (var n, o, i, r, s = 0, a = ie.expando, l = ie.cache, c = ne.deleteExpando, u = ie.event.special; null != (n = e[s]); s++) if ((t || ie.acceptData(n)) && (i = n[a], r = i && l[i])) {
                if (r.events) for (o in r.events) u[o] ? ie.event.remove(n, o) : ie.removeEvent(n, o, r.handle);
                l[i] && (delete l[i], c ? delete n[a] : typeof n.removeAttribute !== Se ? n.removeAttribute(a) : n[a] = null, U.push(i))
            }
        }
    }), ie.fn.extend({
        text: function (e) {
            return Ne(this, function (e) {
                return void 0 === e ? ie.text(this) : this.empty().append((this[0] && this[0].ownerDocument || fe).createTextNode(e));
            }, null, e, arguments.length)
        }, append: function () {
            return this.domManip(arguments, function (e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = y(this, e);
                    t.appendChild(e)
                }
            })
        }, prepend: function () {
            return this.domManip(arguments, function (e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = y(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        }, before: function () {
            return this.domManip(arguments, function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        }, after: function () {
            return this.domManip(arguments, function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        }, remove: function (e, t) {
            for (var n, o = e ? ie.filter(e, this) : this, i = 0; null != (n = o[i]); i++) t || 1 !== n.nodeType || ie.cleanData(g(n)), n.parentNode && (t && ie.contains(n.ownerDocument, n) && x(g(n, "script")), n.parentNode.removeChild(n));
            return this
        }, empty: function () {
            for (var e, t = 0; null != (e = this[t]); t++) {
                for (1 === e.nodeType && ie.cleanData(g(e, !1)); e.firstChild;) e.removeChild(e.firstChild);
                e.options && ie.nodeName(e, "select") && (e.options.length = 0)
            }
            return this
        }, clone: function (e, t) {
            return e = null != e && e, t = null == t ? e : t, this.map(function () {
                return ie.clone(this, e, t)
            })
        }, html: function (e) {
            return Ne(this, function (e) {
                var t = this[0] || {}, n = 0, o = this.length;
                if (void 0 === e) return 1 === t.nodeType ? t.innerHTML.replace(Re, "") : void 0;
                if (!("string" != typeof e || Fe.test(e) || !ne.htmlSerialize && Oe.test(e) || !ne.leadingWhitespace && $e.test(e) || Qe[(qe.exec(e) || ["", ""])[1].toLowerCase()])) {
                    e = e.replace(Ie, "<$1></$2>");
                    try {
                        for (; o > n; n++) t = this[n] || {}, 1 === t.nodeType && (ie.cleanData(g(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (i) {
                    }
                }
                t && this.empty().append(e)
            }, null, e, arguments.length)
        }, replaceWith: function () {
            var e = arguments[0];
            return this.domManip(arguments, function (t) {
                e = this.parentNode, ie.cleanData(g(this)), e && e.replaceChild(t, this)
            }), e && (e.length || e.nodeType) ? this : this.remove()
        }, detach: function (e) {
            return this.remove(e, !0)
        }, domManip: function (e, t) {
            e = Z.apply([], e);
            var n, o, i, r, s, a, l = 0, c = this.length, u = this, d = c - 1, h = e[0], p = ie.isFunction(h);
            if (p || c > 1 && "string" == typeof h && !ne.checkClone && Ve.test(h)) return this.each(function (n) {
                var o = u.eq(n);
                p && (e[0] = h.call(this, n, o.html())), o.domManip(e, t)
            });
            if (c && (a = ie.buildFragment(e, this[0].ownerDocument, !1, this), n = a.firstChild, 1 === a.childNodes.length && (a = n), n)) {
                for (r = ie.map(g(a, "script"), b), i = r.length; c > l; l++) o = a, l !== d && (o = ie.clone(o, !0, !0), i && ie.merge(r, g(o, "script"))), t.call(this[l], o, l);
                if (i) for (s = r[r.length - 1].ownerDocument, ie.map(r, w), l = 0; i > l; l++) o = r[l], Xe.test(o.type || "") && !ie._data(o, "globalEval") && ie.contains(s, o) && (o.src ? ie._evalUrl && ie._evalUrl(o.src) : ie.globalEval((o.text || o.textContent || o.innerHTML || "").replace(Ue, "")));
                a = n = null
            }
            return this
        }
    }), ie.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (e, t) {
        ie.fn[e] = function (e) {
            for (var n, o = 0, i = [], r = ie(e), s = r.length - 1; s >= o; o++) n = o === s ? this : this.clone(!0), ie(r[o])[t](n), G.apply(i, n.get());
            return this.pushStack(i)
        }
    });
    var Je, Ke = {};
    !function () {
        var e;
        ne.shrinkWrapBlocks = function () {
            if (null != e) return e;
            e = !1;
            var t, n, o;
            return n = fe.getElementsByTagName("body")[0], n && n.style ? (t = fe.createElement("div"), o = fe.createElement("div"), o.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(o).appendChild(t), typeof t.style.zoom !== Se && (t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", t.appendChild(fe.createElement("div")).style.width = "5px", e = 3 !== t.offsetWidth), n.removeChild(o), e) : void 0
        }
    }();
    var et, tt, nt = /^margin/, ot = new RegExp("^(" + Ce + ")(?!px)[a-z%]+$", "i"), it = /^(top|right|bottom|left)$/;
    e.getComputedStyle ? (et = function (e) {
        return e.ownerDocument.defaultView.getComputedStyle(e, null)
    }, tt = function (e, t, n) {
        var o, i, r, s, a = e.style;
        return n = n || et(e), s = n ? n.getPropertyValue(t) || n[t] : void 0, n && ("" !== s || ie.contains(e.ownerDocument, e) || (s = ie.style(e, t)), ot.test(s) && nt.test(t) && (o = a.width, i = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = o, a.minWidth = i, a.maxWidth = r)), void 0 === s ? s : s + ""
    }) : fe.documentElement.currentStyle && (et = function (e) {
        return e.currentStyle
    }, tt = function (e, t, n) {
        var o, i, r, s, a = e.style;
        return n = n || et(e), s = n ? n[t] : void 0, null == s && a && a[t] && (s = a[t]), ot.test(s) && !it.test(t) && (o = a.left, i = e.runtimeStyle, r = i && i.left, r && (i.left = e.currentStyle.left), a.left = "fontSize" === t ? "1em" : s, s = a.pixelLeft + "px", a.left = o, r && (i.left = r)), void 0 === s ? s : s + "" || "auto"
    }), !function () {
        function t() {
            var t, n, o, i;
            n = fe.getElementsByTagName("body")[0], n && n.style && (t = fe.createElement("div"), o = fe.createElement("div"), o.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(o).appendChild(t), t.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", r = s = !1, l = !0, e.getComputedStyle && (r = "1%" !== (e.getComputedStyle(t, null) || {}).top, s = "4px" === (e.getComputedStyle(t, null) || {width: "4px"}).width, i = t.appendChild(fe.createElement("div")), i.style.cssText = t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", i.style.marginRight = i.style.width = "0", t.style.width = "1px", l = !parseFloat((e.getComputedStyle(i, null) || {}).marginRight)), t.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", i = t.getElementsByTagName("td"), i[0].style.cssText = "margin:0;border:0;padding:0;display:none", a = 0 === i[0].offsetHeight, a && (i[0].style.display = "", i[1].style.display = "none", a = 0 === i[0].offsetHeight), n.removeChild(o))
        }

        var n, o, i, r, s, a, l;
        n = fe.createElement("div"), n.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", i = n.getElementsByTagName("a")[0], (o = i && i.style) && (o.cssText = "float:left;opacity:.5", ne.opacity = "0.5" === o.opacity, ne.cssFloat = !!o.cssFloat, n.style.backgroundClip = "content-box", n.cloneNode(!0).style.backgroundClip = "", ne.clearCloneStyle = "content-box" === n.style.backgroundClip, ne.boxSizing = "" === o.boxSizing || "" === o.MozBoxSizing || "" === o.WebkitBoxSizing, ie.extend(ne, {
            reliableHiddenOffsets: function () {
                return null == a && t(), a
            }, boxSizingReliable: function () {
                return null == s && t(), s
            }, pixelPosition: function () {
                return null == r && t(), r
            }, reliableMarginRight: function () {
                return null == l && t(), l
            }
        }))
    }(), ie.swap = function (e, t, n, o) {
        var i, r, s = {};
        for (r in t) s[r] = e.style[r], e.style[r] = t[r];
        i = n.apply(e, o || []);
        for (r in t) e.style[r] = s[r];
        return i
    };
    var rt = /alpha\([^)]*\)/i, st = /opacity\s*=\s*([^)]*)/, at = /^(none|table(?!-c[ea]).+)/,
        lt = new RegExp("^(" + Ce + ")(.*)$", "i"), ct = new RegExp("^([+-])=(" + Ce + ")", "i"),
        ut = {position: "absolute", visibility: "hidden", display: "block"},
        dt = {letterSpacing: "0", fontWeight: "400"}, ht = ["Webkit", "O", "Moz", "ms"];
    ie.extend({
        cssHooks: {
            opacity: {
                get: function (e, t) {
                    if (t) {
                        var n = tt(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {"float": ne.cssFloat ? "cssFloat" : "styleFloat"},
        style: function (e, t, n, o) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var i, r, s, a = ie.camelCase(t), l = e.style;
                if (t = ie.cssProps[a] || (ie.cssProps[a] = z(l, a)), s = ie.cssHooks[t] || ie.cssHooks[a], void 0 === n) return s && "get" in s && void 0 !== (i = s.get(e, !1, o)) ? i : l[t];
                if (r = typeof n, "string" === r && (i = ct.exec(n)) && (n = (i[1] + 1) * i[2] + parseFloat(ie.css(e, t)), r = "number"), null != n && n === n && ("number" !== r || ie.cssNumber[a] || (n += "px"), ne.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), !(s && "set" in s && void 0 === (n = s.set(e, n, o))))) try {
                    l[t] = n
                } catch (c) {
                }
            }
        },
        css: function (e, t, n, o) {
            var i, r, s, a = ie.camelCase(t);
            return t = ie.cssProps[a] || (ie.cssProps[a] = z(e.style, a)), s = ie.cssHooks[t] || ie.cssHooks[a], s && "get" in s && (r = s.get(e, !0, n)), void 0 === r && (r = tt(e, t, o)), "normal" === r && t in dt && (r = dt[t]), "" === n || n ? (i = parseFloat(r), n === !0 || ie.isNumeric(i) ? i || 0 : r) : r
        }
    }), ie.each(["height", "width"], function (e, t) {
        ie.cssHooks[t] = {
            get: function (e, n, o) {
                return n ? at.test(ie.css(e, "display")) && 0 === e.offsetWidth ? ie.swap(e, ut, function () {
                    return P(e, t, o)
                }) : P(e, t, o) : void 0
            }, set: function (e, n, o) {
                var i = o && et(e);
                return N(e, n, o ? _(e, t, o, ne.boxSizing && "border-box" === ie.css(e, "boxSizing", !1, i), i) : 0)
            }
        }
    }), ne.opacity || (ie.cssHooks.opacity = {
        get: function (e, t) {
            return st.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
        }, set: function (e, t) {
            var n = e.style, o = e.currentStyle, i = ie.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : "",
                r = o && o.filter || n.filter || "";
            n.zoom = 1, (t >= 1 || "" === t) && "" === ie.trim(r.replace(rt, "")) && n.removeAttribute && (n.removeAttribute("filter"), "" === t || o && !o.filter) || (n.filter = rt.test(r) ? r.replace(rt, i) : r + " " + i)
        }
    }), ie.cssHooks.marginRight = C(ne.reliableMarginRight, function (e, t) {
        return t ? ie.swap(e, {display: "inline-block"}, tt, [e, "marginRight"]) : void 0
    }), ie.each({margin: "", padding: "", border: "Width"}, function (e, t) {
        ie.cssHooks[e + t] = {
            expand: function (n) {
                for (var o = 0, i = {}, r = "string" == typeof n ? n.split(" ") : [n]; 4 > o; o++) i[e + ze[o] + t] = r[o] || r[o - 2] || r[0];
                return i
            }
        }, nt.test(e) || (ie.cssHooks[e + t].set = N)
    }), ie.fn.extend({
        css: function (e, t) {
            return Ne(this, function (e, t, n) {
                var o, i, r = {}, s = 0;
                if (ie.isArray(t)) {
                    for (o = et(e), i = t.length; i > s; s++) r[t[s]] = ie.css(e, t[s], !1, o);
                    return r
                }
                return void 0 !== n ? ie.style(e, t, n) : ie.css(e, t)
            }, e, t, arguments.length > 1)
        }, show: function () {
            return L(this, !0)
        }, hide: function () {
            return L(this)
        }, toggle: function (e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function () {
                Le(this) ? ie(this).show() : ie(this).hide()
            })
        }
    }), ie.Tween = M, M.prototype = {
        constructor: M, init: function (e, t, n, o, i, r) {
            this.elem = e, this.prop = n, this.easing = i || "swing", this.options = t, this.start = this.now = this.cur(), this.end = o, this.unit = r || (ie.cssNumber[n] ? "" : "px")
        }, cur: function () {
            var e = M.propHooks[this.prop];
            return e && e.get ? e.get(this) : M.propHooks._default.get(this)
        }, run: function (e) {
            var t, n = M.propHooks[this.prop];
            return this.pos = t = this.options.duration ? ie.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : M.propHooks._default.set(this), this
        }
    }, M.prototype.init.prototype = M.prototype, M.propHooks = {
        _default: {
            get: function (e) {
                var t;
                return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = ie.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
            }, set: function (e) {
                ie.fx.step[e.prop] ? ie.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[ie.cssProps[e.prop]] || ie.cssHooks[e.prop]) ? ie.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
            }
        }
    }, M.propHooks.scrollTop = M.propHooks.scrollLeft = {
        set: function (e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, ie.easing = {
        linear: function (e) {
            return e
        }, swing: function (e) {
            return .5 - Math.cos(e * Math.PI) / 2
        }
    }, ie.fx = M.prototype.init, ie.fx.step = {};
    var pt, ft, mt = /^(?:toggle|show|hide)$/, gt = new RegExp("^(?:([+-])=|)(" + Ce + ")([a-z%]*)$", "i"),
        vt = /queueHooks$/, yt = [D], bt = {
            "*": [function (e, t) {
                var n = this.createTween(e, t), o = n.cur(), i = gt.exec(t), r = i && i[3] || (ie.cssNumber[e] ? "" : "px"),
                    s = (ie.cssNumber[e] || "px" !== r && +o) && gt.exec(ie.css(n.elem, e)), a = 1, l = 20;
                if (s && s[3] !== r) {
                    r = r || s[3], i = i || [], s = +o || 1;
                    do a = a || ".5", s /= a, ie.style(n.elem, e, s + r); while (a !== (a = n.cur() / o) && 1 !== a && --l)
                }
                return i && (s = n.start = +s || +o || 0, n.unit = r, n.end = i[1] ? s + (i[1] + 1) * i[2] : +i[2]), n
            }]
        };
    ie.Animation = ie.extend(O, {
        tweener: function (e, t) {
            ie.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
            for (var n, o = 0, i = e.length; i > o; o++) n = e[o], bt[n] = bt[n] || [], bt[n].unshift(t)
        }, prefilter: function (e, t) {
            t ? yt.unshift(e) : yt.push(e)
        }
    }), ie.speed = function (e, t, n) {
        var o = e && "object" == typeof e ? ie.extend({}, e) : {
            complete: n || !n && t || ie.isFunction(e) && e,
            duration: e,
            easing: n && t || t && !ie.isFunction(t) && t
        };
        return o.duration = ie.fx.off ? 0 : "number" == typeof o.duration ? o.duration : o.duration in ie.fx.speeds ? ie.fx.speeds[o.duration] : ie.fx.speeds._default, (null == o.queue || o.queue === !0) && (o.queue = "fx"), o.old = o.complete, o.complete = function () {
            ie.isFunction(o.old) && o.old.call(this), o.queue && ie.dequeue(this, o.queue)
        }, o
    }, ie.fn.extend({
        fadeTo: function (e, t, n, o) {
            return this.filter(Le).css("opacity", 0).show().end().animate({opacity: t}, e, n, o)
        }, animate: function (e, t, n, o) {
            var i = ie.isEmptyObject(e), r = ie.speed(t, n, o), s = function () {
                var t = O(this, ie.extend({}, e), r);
                (i || ie._data(this, "finish")) && t.stop(!0)
            };
            return s.finish = s, i || r.queue === !1 ? this.each(s) : this.queue(r.queue, s)
        }, stop: function (e, t, n) {
            var o = function (e) {
                var t = e.stop;
                delete e.stop, t(n)
            };
            return "string" != typeof e && (n = t, t = e, e = void 0), t && e !== !1 && this.queue(e || "fx", []), this.each(function () {
                var t = !0, i = null != e && e + "queueHooks", r = ie.timers, s = ie._data(this);
                if (i) s[i] && s[i].stop && o(s[i]); else for (i in s) s[i] && s[i].stop && vt.test(i) && o(s[i]);
                for (i = r.length; i--;) r[i].elem !== this || null != e && r[i].queue !== e || (r[i].anim.stop(n), t = !1, r.splice(i, 1));
                (t || !n) && ie.dequeue(this, e)
            })
        }, finish: function (e) {
            return e !== !1 && (e = e || "fx"), this.each(function () {
                var t, n = ie._data(this), o = n[e + "queue"], i = n[e + "queueHooks"], r = ie.timers,
                    s = o ? o.length : 0;
                for (n.finish = !0, ie.queue(this, e, []), i && i.stop && i.stop.call(this, !0), t = r.length; t--;) r[t].elem === this && r[t].queue === e && (r[t].anim.stop(!0), r.splice(t, 1));
                for (t = 0; s > t; t++) o[t] && o[t].finish && o[t].finish.call(this);
                delete n.finish
            })
        }
    }), ie.each(["toggle", "show", "hide"], function (e, t) {
        var n = ie.fn[t];
        ie.fn[t] = function (e, o, i) {
            return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(j(t, !0), e, o, i)
        }
    }), ie.each({
        slideDown: j("show"),
        slideUp: j("hide"),
        slideToggle: j("toggle"),
        fadeIn: {opacity: "show"},
        fadeOut: {opacity: "hide"},
        fadeToggle: {opacity: "toggle"}
    }, function (e, t) {
        ie.fn[e] = function (e, n, o) {
            return this.animate(t, e, n, o)
        }
    }), ie.timers = [], ie.fx.tick = function () {
        var e, t = ie.timers, n = 0;
        for (pt = ie.now(); n < t.length; n++) e = t[n], e() || t[n] !== e || t.splice(n--, 1);
        t.length || ie.fx.stop(), pt = void 0
    }, ie.fx.timer = function (e) {
        ie.timers.push(e), e() ? ie.fx.start() : ie.timers.pop()
    }, ie.fx.interval = 13, ie.fx.start = function () {
        ft || (ft = setInterval(ie.fx.tick, ie.fx.interval))
    }, ie.fx.stop = function () {
        clearInterval(ft), ft = null
    }, ie.fx.speeds = {slow: 600, fast: 200, _default: 400}, ie.fn.delay = function (e, t) {
        return e = ie.fx ? ie.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function (t, n) {
            var o = setTimeout(t, e);
            n.stop = function () {
                clearTimeout(o)
            }
        })
    }, function () {
        var e, t, n, o, i;
        t = fe.createElement("div"), t.setAttribute("className", "t"), t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", o = t.getElementsByTagName("a")[0], n = fe.createElement("select"), i = n.appendChild(fe.createElement("option")), e = t.getElementsByTagName("input")[0], o.style.cssText = "top:1px", ne.getSetAttribute = "t" !== t.className, ne.style = /top/.test(o.getAttribute("style")), ne.hrefNormalized = "/a" === o.getAttribute("href"), ne.checkOn = !!e.value, ne.optSelected = i.selected, ne.enctype = !!fe.createElement("form").enctype, n.disabled = !0, ne.optDisabled = !i.disabled, e = fe.createElement("input"), e.setAttribute("value", ""), ne.input = "" === e.getAttribute("value"), e.value = "t", e.setAttribute("type", "radio"), ne.radioValue = "t" === e.value
    }();
    var wt = /\r/g;
    ie.fn.extend({
        val: function (e) {
            var t, n, o, i = this[0];
            return arguments.length ? (o = ie.isFunction(e), this.each(function (n) {
                var i;
                1 === this.nodeType && (i = o ? e.call(this, n, ie(this).val()) : e, null == i ? i = "" : "number" == typeof i ? i += "" : ie.isArray(i) && (i = ie.map(i, function (e) {
                    return null == e ? "" : e + ""
                })), t = ie.valHooks[this.type] || ie.valHooks[this.nodeName.toLowerCase()], t && "set" in t && void 0 !== t.set(this, i, "value") || (this.value = i))
            })) : i ? (t = ie.valHooks[i.type] || ie.valHooks[i.nodeName.toLowerCase()], t && "get" in t && void 0 !== (n = t.get(i, "value")) ? n : (n = i.value, "string" == typeof n ? n.replace(wt, "") : null == n ? "" : n)) : void 0
        }
    }), ie.extend({
        valHooks: {
            option: {
                get: function (e) {
                    var t = ie.find.attr(e, "value");
                    return null != t ? t : ie.trim(ie.text(e))
                }
            }, select: {
                get: function (e) {
                    for (var t, n, o = e.options, i = e.selectedIndex, r = "select-one" === e.type || 0 > i, s = r ? null : [], a = r ? i + 1 : o.length, l = 0 > i ? a : r ? i : 0; a > l; l++) if (n = o[l], !(!n.selected && l !== i || (ne.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && ie.nodeName(n.parentNode, "optgroup"))) {
                        if (t = ie(n).val(), r) return t;
                        s.push(t)
                    }
                    return s
                }, set: function (e, t) {
                    for (var n, o, i = e.options, r = ie.makeArray(t), s = i.length; s--;) if (o = i[s], ie.inArray(ie.valHooks.option.get(o), r) >= 0) try {
                        o.selected = n = !0
                    } catch (a) {
                        o.scrollHeight
                    } else o.selected = !1;
                    return n || (e.selectedIndex = -1), i
                }
            }
        }
    }), ie.each(["radio", "checkbox"], function () {
        ie.valHooks[this] = {
            set: function (e, t) {
                return ie.isArray(t) ? e.checked = ie.inArray(ie(e).val(), t) >= 0 : void 0
            }
        }, ne.checkOn || (ie.valHooks[this].get = function (e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    });
    var xt, Tt, St = ie.expr.attrHandle, kt = /^(?:checked|selected)$/i, Et = ne.getSetAttribute, Ct = ne.input;
    ie.fn.extend({
        attr: function (e, t) {
            return Ne(this, ie.attr, e, t, arguments.length > 1)
        }, removeAttr: function (e) {
            return this.each(function () {
                ie.removeAttr(this, e)
            })
        }
    }), ie.extend({
        attr: function (e, t, n) {
            var o, i, r = e.nodeType;
            if (e && 3 !== r && 8 !== r && 2 !== r) return typeof e.getAttribute === Se ? ie.prop(e, t, n) : (1 === r && ie.isXMLDoc(e) || (t = t.toLowerCase(), o = ie.attrHooks[t] || (ie.expr.match.bool.test(t) ? Tt : xt)), void 0 === n ? o && "get" in o && null !== (i = o.get(e, t)) ? i : (i = ie.find.attr(e, t), null == i ? void 0 : i) : null !== n ? o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : (e.setAttribute(t, n + ""), n) : void ie.removeAttr(e, t))
        }, removeAttr: function (e, t) {
            var n, o, i = 0, r = t && t.match(be);
            if (r && 1 === e.nodeType) for (; n = r[i++];) o = ie.propFix[n] || n, ie.expr.match.bool.test(n) ? Ct && Et || !kt.test(n) ? e[o] = !1 : e[ie.camelCase("default-" + n)] = e[o] = !1 : ie.attr(e, n, ""), e.removeAttribute(Et ? n : o)
        }, attrHooks: {
            type: {
                set: function (e, t) {
                    if (!ne.radioValue && "radio" === t && ie.nodeName(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        }
    }), Tt = {
        set: function (e, t, n) {
            return t === !1 ? ie.removeAttr(e, n) : Ct && Et || !kt.test(n) ? e.setAttribute(!Et && ie.propFix[n] || n, n) : e[ie.camelCase("default-" + n)] = e[n] = !0, n
        }
    }, ie.each(ie.expr.match.bool.source.match(/\w+/g), function (e, t) {
        var n = St[t] || ie.find.attr;
        St[t] = Ct && Et || !kt.test(t) ? function (e, t, o) {
            var i, r;
            return o || (r = St[t], St[t] = i, i = null != n(e, t, o) ? t.toLowerCase() : null, St[t] = r), i
        } : function (e, t, n) {
            return n ? void 0 : e[ie.camelCase("default-" + t)] ? t.toLowerCase() : null
        }
    }), Ct && Et || (ie.attrHooks.value = {
        set: function (e, t, n) {
            return ie.nodeName(e, "input") ? void(e.defaultValue = t) : xt && xt.set(e, t, n)
        }
    }), Et || (xt = {
        set: function (e, t, n) {
            var o = e.getAttributeNode(n);
            return o || e.setAttributeNode(o = e.ownerDocument.createAttribute(n)), o.value = t += "", "value" === n || t === e.getAttribute(n) ? t : void 0
        }
    }, St.id = St.name = St.coords = function (e, t, n) {
        var o;
        return n ? void 0 : (o = e.getAttributeNode(t)) && "" !== o.value ? o.value : null
    }, ie.valHooks.button = {
        get: function (e, t) {
            var n = e.getAttributeNode(t);
            return n && n.specified ? n.value : void 0
        }, set: xt.set
    }, ie.attrHooks.contenteditable = {
        set: function (e, t, n) {
            xt.set(e, "" !== t && t, n)
        }
    }, ie.each(["width", "height"], function (e, t) {
        ie.attrHooks[t] = {
            set: function (e, n) {
                return "" === n ? (e.setAttribute(t, "auto"), n) : void 0
            }
        }
    })), ne.style || (ie.attrHooks.style = {
        get: function (e) {
            return e.style.cssText || void 0
        }, set: function (e, t) {
            return e.style.cssText = t + ""
        }
    });
    var zt = /^(?:input|select|textarea|button|object)$/i, Lt = /^(?:a|area)$/i;
    ie.fn.extend({
        prop: function (e, t) {
            return Ne(this, ie.prop, e, t, arguments.length > 1)
        }, removeProp: function (e) {
            return e = ie.propFix[e] || e, this.each(function () {
                try {
                    this[e] = void 0, delete this[e]
                } catch (t) {
                }
            })
        }
    }), ie.extend({
        propFix: {"for": "htmlFor", "class": "className"}, prop: function (e, t, n) {
            var o, i, r, s = e.nodeType;
            if (e && 3 !== s && 8 !== s && 2 !== s) return r = 1 !== s || !ie.isXMLDoc(e), r && (t = ie.propFix[t] || t, i = ie.propHooks[t]), void 0 !== n ? i && "set" in i && void 0 !== (o = i.set(e, n, t)) ? o : e[t] = n : i && "get" in i && null !== (o = i.get(e, t)) ? o : e[t]
        }, propHooks: {
            tabIndex: {
                get: function (e) {
                    var t = ie.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : zt.test(e.nodeName) || Lt.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        }
    }), ne.hrefNormalized || ie.each(["href", "src"], function (e, t) {
        ie.propHooks[t] = {
            get: function (e) {
                return e.getAttribute(t, 4)
            }
        }
    }), ne.optSelected || (ie.propHooks.selected = {
        get: function (e) {
            var t = e.parentNode;
            return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
        }
    }), ie.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
        ie.propFix[this.toLowerCase()] = this
    }), ne.enctype || (ie.propFix.enctype = "encoding");
    var Nt = /[\t\r\n\f]/g;
    ie.fn.extend({
        addClass: function (e) {
            var t, n, o, i, r, s, a = 0, l = this.length, c = "string" == typeof e && e;
            if (ie.isFunction(e)) return this.each(function (t) {
                ie(this).addClass(e.call(this, t, this.className))
            });
            if (c) for (t = (e || "").match(be) || []; l > a; a++) if (n = this[a], o = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Nt, " ") : " ")) {
                for (r = 0; i = t[r++];) o.indexOf(" " + i + " ") < 0 && (o += i + " ");
                s = ie.trim(o), n.className !== s && (n.className = s)
            }
            return this
        }, removeClass: function (e) {
            var t, n, o, i, r, s, a = 0, l = this.length, c = 0 === arguments.length || "string" == typeof e && e;
            if (ie.isFunction(e)) return this.each(function (t) {
                ie(this).removeClass(e.call(this, t, this.className))
            });
            if (c) for (t = (e || "").match(be) || []; l > a; a++) if (n = this[a], o = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Nt, " ") : "")) {
                for (r = 0; i = t[r++];) for (; o.indexOf(" " + i + " ") >= 0;) o = o.replace(" " + i + " ", " ");
                s = e ? ie.trim(o) : "", n.className !== s && (n.className = s)
            }
            return this
        }, toggleClass: function (e, t) {
            var n = typeof e;
            return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : this.each(ie.isFunction(e) ? function (n) {
                ie(this).toggleClass(e.call(this, n, this.className, t), t)
            } : function () {
                if ("string" === n) for (var t, o = 0, i = ie(this), r = e.match(be) || []; t = r[o++];) i.hasClass(t) ? i.removeClass(t) : i.addClass(t); else (n === Se || "boolean" === n) && (this.className && ie._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : ie._data(this, "__className__") || "")
            })
        }, hasClass: function (e) {
            for (var t = " " + e + " ", n = 0, o = this.length; o > n; n++) if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(Nt, " ").indexOf(t) >= 0) return !0;
            return !1
        }
    }), ie.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (e, t) {
        ie.fn[t] = function (e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }
    }), ie.fn.extend({
        hover: function (e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }, bind: function (e, t, n) {
            return this.on(e, null, t, n)
        }, unbind: function (e, t) {
            return this.off(e, null, t)
        }, delegate: function (e, t, n, o) {
            return this.on(t, e, n, o)
        }, undelegate: function (e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
        }
    });
    var _t = ie.now(), Pt = /\?/,
        Mt = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    ie.parseJSON = function (t) {
        if (e.JSON && e.JSON.parse) return e.JSON.parse(t + "");
        var n, o = null, i = ie.trim(t + "");
        return i && !ie.trim(i.replace(Mt, function (e, t, i, r) {
            return n && t && (o = 0), 0 === o ? e : (n = i || t, o += !r - !i, "")
        })) ? Function("return " + i)() : ie.error("Invalid JSON: " + t)
    }, ie.parseXML = function (t) {
        var n, o;
        if (!t || "string" != typeof t) return null;
        try {
            e.DOMParser ? (o = new DOMParser, n = o.parseFromString(t, "text/xml")) : (n = new ActiveXObject("Microsoft.XMLDOM"), n.async = "false", n.loadXML(t))
        } catch (i) {
            n = void 0
        }
        return n && n.documentElement && !n.getElementsByTagName("parsererror").length || ie.error("Invalid XML: " + t), n
    };
    var At, jt, Ht = /#.*$/, Dt = /([?&])_=[^&]*/, Rt = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        Ot = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, $t = /^(?:GET|HEAD)$/, It = /^\/\//,
        qt = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, Wt = {}, Bt = {}, Ft = "*/".concat("*");
    try {
        jt = location.href
    } catch (Vt) {
        jt = fe.createElement("a"), jt.href = "", jt = jt.href
    }
    At = qt.exec(jt.toLowerCase()) || [], ie.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: jt,
            type: "GET",
            isLocal: Ot.test(At[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Ft,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {xml: /xml/, html: /html/, json: /json/},
            responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
            converters: {"* text": String, "text html": !0, "text json": ie.parseJSON, "text xml": ie.parseXML},
            flatOptions: {url: !0, context: !0}
        },
        ajaxSetup: function (e, t) {
            return t ? q(q(e, ie.ajaxSettings), t) : q(ie.ajaxSettings, e)
        },
        ajaxPrefilter: $(Wt),
        ajaxTransport: $(Bt),
        ajax: function (e, t) {
            function n(e, t, n, o) {
                var i, u, v, y, w, T = t;
                2 !== b && (b = 2, a && clearTimeout(a), c = void 0, s = o || "", x.readyState = e > 0 ? 4 : 0, i = e >= 200 && 300 > e || 304 === e, n && (y = W(d, x, n)), y = B(d, y, x, i), i ? (d.ifModified && (w = x.getResponseHeader("Last-Modified"), w && (ie.lastModified[r] = w), w = x.getResponseHeader("etag"), w && (ie.etag[r] = w)), 204 === e || "HEAD" === d.type ? T = "nocontent" : 304 === e ? T = "notmodified" : (T = y.state, u = y.data, v = y.error, i = !v)) : (v = T, (e || !T) && (T = "error", 0 > e && (e = 0))), x.status = e, x.statusText = (t || T) + "", i ? f.resolveWith(h, [u, T, x]) : f.rejectWith(h, [x, T, v]), x.statusCode(g), g = void 0, l && p.trigger(i ? "ajaxSuccess" : "ajaxError", [x, d, i ? u : v]), m.fireWith(h, [x, T]), l && (p.trigger("ajaxComplete", [x, d]), --ie.active || ie.event.trigger("ajaxStop")))
            }

            "object" == typeof e && (t = e, e = void 0), t = t || {};
            var o, i, r, s, a, l, c, u, d = ie.ajaxSetup({}, t), h = d.context || d,
                p = d.context && (h.nodeType || h.jquery) ? ie(h) : ie.event, f = ie.Deferred(),
                m = ie.Callbacks("once memory"), g = d.statusCode || {}, v = {}, y = {}, b = 0, w = "canceled", x = {
                    readyState: 0, getResponseHeader: function (e) {
                        var t;
                        if (2 === b) {
                            if (!u) for (u = {}; t = Rt.exec(s);) u[t[1].toLowerCase()] = t[2];
                            t = u[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    }, getAllResponseHeaders: function () {
                        return 2 === b ? s : null
                    }, setRequestHeader: function (e, t) {
                        var n = e.toLowerCase();
                        return b || (e = y[n] = y[n] || e, v[e] = t), this
                    }, overrideMimeType: function (e) {
                        return b || (d.mimeType = e), this
                    }, statusCode: function (e) {
                        var t;
                        if (e) if (2 > b) for (t in e) g[t] = [g[t], e[t]]; else x.always(e[x.status]);
                        return this
                    }, abort: function (e) {
                        var t = e || w;
                        return c && c.abort(t), n(0, t), this
                    }
                };
            if (f.promise(x).complete = m.add, x.success = x.done, x.error = x.fail, d.url = ((e || d.url || jt) + "").replace(Ht, "").replace(It, At[1] + "//"), d.type = t.method || t.type || d.method || d.type, d.dataTypes = ie.trim(d.dataType || "*").toLowerCase().match(be) || [""], null == d.crossDomain && (o = qt.exec(d.url.toLowerCase()), d.crossDomain = !(!o || o[1] === At[1] && o[2] === At[2] && (o[3] || ("http:" === o[1] ? "80" : "443")) === (At[3] || ("http:" === At[1] ? "80" : "443")))), d.data && d.processData && "string" != typeof d.data && (d.data = ie.param(d.data, d.traditional)), I(Wt, d, t, x), 2 === b) return x;
            l = d.global, l && 0 === ie.active++ && ie.event.trigger("ajaxStart"), d.type = d.type.toUpperCase(), d.hasContent = !$t.test(d.type), r = d.url, d.hasContent || (d.data && (r = d.url += (Pt.test(r) ? "&" : "?") + d.data, delete d.data), d.cache === !1 && (d.url = Dt.test(r) ? r.replace(Dt, "$1_=" + _t++) : r + (Pt.test(r) ? "&" : "?") + "_=" + _t++)), d.ifModified && (ie.lastModified[r] && x.setRequestHeader("If-Modified-Since", ie.lastModified[r]), ie.etag[r] && x.setRequestHeader("If-None-Match", ie.etag[r])), (d.data && d.hasContent && d.contentType !== !1 || t.contentType) && x.setRequestHeader("Content-Type", d.contentType), x.setRequestHeader("Accept", d.dataTypes[0] && d.accepts[d.dataTypes[0]] ? d.accepts[d.dataTypes[0]] + ("*" !== d.dataTypes[0] ? ", " + Ft + "; q=0.01" : "") : d.accepts["*"]);
            for (i in d.headers) x.setRequestHeader(i, d.headers[i]);
            if (d.beforeSend && (d.beforeSend.call(h, x, d) === !1 || 2 === b)) return x.abort();
            w = "abort";
            for (i in{success: 1, error: 1, complete: 1}) x[i](d[i]);
            if (c = I(Bt, d, t, x)) {
                x.readyState = 1, l && p.trigger("ajaxSend", [x, d]), d.async && d.timeout > 0 && (a = setTimeout(function () {
                    x.abort("timeout")
                }, d.timeout));
                try {
                    b = 1, c.send(v, n)
                } catch (T) {
                    if (!(2 > b)) throw T;
                    n(-1, T)
                }
            } else n(-1, "No Transport");
            return x
        },
        getJSON: function (e, t, n) {
            return ie.get(e, t, n, "json")
        },
        getScript: function (e, t) {
            return ie.get(e, void 0, t, "script")
        }
    }), ie.each(["get", "post"], function (e, t) {
        ie[t] = function (e, n, o, i) {
            return ie.isFunction(n) && (i = i || o, o = n, n = void 0), ie.ajax({
                url: e,
                type: t,
                dataType: i,
                data: n,
                success: o
            })
        }
    }), ie.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
        ie.fn[t] = function (e) {
            return this.on(t, e)
        }
    }), ie._evalUrl = function (e) {
        return ie.ajax({url: e, type: "GET", dataType: "script", async: !1, global: !1, "throws": !0})
    }, ie.fn.extend({
        wrapAll: function (e) {
            if (ie.isFunction(e)) return this.each(function (t) {
                ie(this).wrapAll(e.call(this, t))
            });
            if (this[0]) {
                var t = ie(e, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
                    for (var e = this; e.firstChild && 1 === e.firstChild.nodeType;) e = e.firstChild;
                    return e
                }).append(this)
            }
            return this
        }, wrapInner: function (e) {
            return this.each(ie.isFunction(e) ? function (t) {
                ie(this).wrapInner(e.call(this, t))
            } : function () {
                var t = ie(this), n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        }, wrap: function (e) {
            var t = ie.isFunction(e);
            return this.each(function (n) {
                ie(this).wrapAll(t ? e.call(this, n) : e)
            })
        }, unwrap: function () {
            return this.parent().each(function () {
                ie.nodeName(this, "body") || ie(this).replaceWith(this.childNodes)
            }).end()
        }
    }), ie.expr.filters.hidden = function (e) {
        return e.offsetWidth <= 0 && e.offsetHeight <= 0 || !ne.reliableHiddenOffsets() && "none" === (e.style && e.style.display || ie.css(e, "display"))
    }, ie.expr.filters.visible = function (e) {
        return !ie.expr.filters.hidden(e)
    };
    var Xt = /%20/g, Yt = /\[\]$/, Ut = /\r?\n/g, Qt = /^(?:submit|button|image|reset|file)$/i,
        Zt = /^(?:input|select|textarea|keygen)/i;
    ie.param = function (e, t) {
        var n, o = [], i = function (e, t) {
            t = ie.isFunction(t) ? t() : null == t ? "" : t, o[o.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
        };
        if (void 0 === t && (t = ie.ajaxSettings && ie.ajaxSettings.traditional), ie.isArray(e) || e.jquery && !ie.isPlainObject(e)) ie.each(e, function () {
            i(this.name, this.value)
        }); else for (n in e) F(n, e[n], t, i);
        return o.join("&").replace(Xt, "+")
    }, ie.fn.extend({
        serialize: function () {
            return ie.param(this.serializeArray())
        }, serializeArray: function () {
            return this.map(function () {
                var e = ie.prop(this, "elements");
                return e ? ie.makeArray(e) : this
            }).filter(function () {
                var e = this.type;
                return this.name && !ie(this).is(":disabled") && Zt.test(this.nodeName) && !Qt.test(e) && (this.checked || !_e.test(e))
            }).map(function (e, t) {
                var n = ie(this).val();
                return null == n ? null : ie.isArray(n) ? ie.map(n, function (e) {
                    return {name: t.name, value: e.replace(Ut, "\r\n")}
                }) : {name: t.name, value: n.replace(Ut, "\r\n")}
            }).get()
        }
    }), ie.ajaxSettings.xhr = void 0 !== e.ActiveXObject ? function () {
        return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && V() || X()
    } : V;
    var Gt = 0, Jt = {}, Kt = ie.ajaxSettings.xhr();
    e.ActiveXObject && ie(e).on("unload", function () {
        for (var e in Jt) Jt[e](void 0, !0)
    }), ne.cors = !!Kt && "withCredentials" in Kt, Kt = ne.ajax = !!Kt, Kt && ie.ajaxTransport(function (e) {
        if (!e.crossDomain || ne.cors) {
            var t;
            return {
                send: function (n, o) {
                    var i, r = e.xhr(), s = ++Gt;
                    if (r.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields) for (i in e.xhrFields) r[i] = e.xhrFields[i];
                    e.mimeType && r.overrideMimeType && r.overrideMimeType(e.mimeType), e.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                    for (i in n) void 0 !== n[i] && r.setRequestHeader(i, n[i] + "");
                    r.send(e.hasContent && e.data || null), t = function (n, i) {
                        var a, l, c;
                        if (t && (i || 4 === r.readyState)) if (delete Jt[s], t = void 0, r.onreadystatechange = ie.noop, i) 4 !== r.readyState && r.abort(); else {
                            c = {}, a = r.status, "string" == typeof r.responseText && (c.text = r.responseText);
                            try {
                                l = r.statusText
                            } catch (u) {
                                l = ""
                            }
                            a || !e.isLocal || e.crossDomain ? 1223 === a && (a = 204) : a = c.text ? 200 : 404
                        }
                        c && o(a, l, c, r.getAllResponseHeaders())
                    }, e.async ? 4 === r.readyState ? setTimeout(t) : r.onreadystatechange = Jt[s] = t : t()
                }, abort: function () {
                    t && t(void 0, !0)
                }
            }
        }
    }), ie.ajaxSetup({
        accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
        contents: {script: /(?:java|ecma)script/},
        converters: {
            "text script": function (e) {
                return ie.globalEval(e), e
            }
        }
    }), ie.ajaxPrefilter("script", function (e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1)
    }), ie.ajaxTransport("script", function (e) {
        if (e.crossDomain) {
            var t, n = fe.head || ie("head")[0] || fe.documentElement;
            return {
                send: function (o, i) {
                    t = fe.createElement("script"), t.async = !0, e.scriptCharset && (t.charset = e.scriptCharset), t.src = e.url, t.onload = t.onreadystatechange = function (e, n) {
                        (n || !t.readyState || /loaded|complete/.test(t.readyState)) && (t.onload = t.onreadystatechange = null, t.parentNode && t.parentNode.removeChild(t), t = null, n || i(200, "success"))
                    }, n.insertBefore(t, n.firstChild)
                }, abort: function () {
                    t && t.onload(void 0, !0)
                }
            }
        }
    });
    var en = [], tn = /(=)\?(?=&|$)|\?\?/;
    ie.ajaxSetup({
        jsonp: "callback", jsonpCallback: function () {
            var e = en.pop() || ie.expando + "_" + _t++;
            return this[e] = !0, e
        }
    }), ie.ajaxPrefilter("json jsonp", function (t, n, o) {
        var i, r, s,
            a = t.jsonp !== !1 && (tn.test(t.url) ? "url" : "string" == typeof t.data && !(t.contentType || "").indexOf("application/x-www-form-urlencoded") && tn.test(t.data) && "data");
        return a || "jsonp" === t.dataTypes[0] ? (i = t.jsonpCallback = ie.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, a ? t[a] = t[a].replace(tn, "$1" + i) : t.jsonp !== !1 && (t.url += (Pt.test(t.url) ? "&" : "?") + t.jsonp + "=" + i), t.converters["script json"] = function () {
            return s || ie.error(i + " was not called"), s[0]
        }, t.dataTypes[0] = "json", r = e[i], e[i] = function () {
            s = arguments
        }, o.always(function () {
            e[i] = r, t[i] && (t.jsonpCallback = n.jsonpCallback, en.push(i)), s && ie.isFunction(r) && r(s[0]), s = r = void 0
        }), "script") : void 0
    }), ie.parseHTML = function (e, t, n) {
        if (!e || "string" != typeof e) return null;
        "boolean" == typeof t && (n = t, t = !1), t = t || fe;
        var o = de.exec(e), i = !n && [];
        return o ? [t.createElement(o[1])] : (o = ie.buildFragment([e], t, i), i && i.length && ie(i).remove(), ie.merge([], o.childNodes))
    };
    var nn = ie.fn.load;
    ie.fn.load = function (e, t, n) {
        if ("string" != typeof e && nn) return nn.apply(this, arguments);
        var o, i, r, s = this, a = e.indexOf(" ");
        return a >= 0 && (o = ie.trim(e.slice(a, e.length)), e = e.slice(0, a)), ie.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (r = "POST"), s.length > 0 && ie.ajax({
            url: e,
            type: r,
            dataType: "html",
            data: t
        }).done(function (e) {
            i = arguments, s.html(o ? ie("<div>").append(ie.parseHTML(e)).find(o) : e)
        }).complete(n && function (e, t) {
            s.each(n, i || [e.responseText, t, e])
        }), this
    }, ie.expr.filters.animated = function (e) {
        return ie.grep(ie.timers, function (t) {
            return e === t.elem
        }).length
    };
    var on = e.document.documentElement;
    ie.offset = {
        setOffset: function (e, t, n) {
            var o, i, r, s, a, l, c, u = ie.css(e, "position"), d = ie(e), h = {};
            "static" === u && (e.style.position = "relative"), a = d.offset(), r = ie.css(e, "top"), l = ie.css(e, "left"), c = ("absolute" === u || "fixed" === u) && ie.inArray("auto", [r, l]) > -1, c ? (o = d.position(), s = o.top, i = o.left) : (s = parseFloat(r) || 0, i = parseFloat(l) || 0), ie.isFunction(t) && (t = t.call(e, n, a)), null != t.top && (h.top = t.top - a.top + s), null != t.left && (h.left = t.left - a.left + i), "using" in t ? t.using.call(e, h) : d.css(h)
        }
    }, ie.fn.extend({
        offset: function (e) {
            if (arguments.length) return void 0 === e ? this : this.each(function (t) {
                ie.offset.setOffset(this, e, t)
            });
            var t, n, o = {top: 0, left: 0}, i = this[0], r = i && i.ownerDocument;
            return r ? (t = r.documentElement, ie.contains(t, i) ? (typeof i.getBoundingClientRect !== Se && (o = i.getBoundingClientRect()), n = Y(r), {
                top: o.top + (n.pageYOffset || t.scrollTop) - (t.clientTop || 0),
                left: o.left + (n.pageXOffset || t.scrollLeft) - (t.clientLeft || 0)
            }) : o) : void 0
        }, position: function () {
            if (this[0]) {
                var e, t, n = {top: 0, left: 0}, o = this[0];
                return "fixed" === ie.css(o, "position") ? t = o.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), ie.nodeName(e[0], "html") || (n = e.offset()), n.top += ie.css(e[0], "borderTopWidth", !0), n.left += ie.css(e[0], "borderLeftWidth", !0)), {
                    top: t.top - n.top - ie.css(o, "marginTop", !0),
                    left: t.left - n.left - ie.css(o, "marginLeft", !0)
                }
            }
        }, offsetParent: function () {
            return this.map(function () {
                for (var e = this.offsetParent || on; e && !ie.nodeName(e, "html") && "static" === ie.css(e, "position");) e = e.offsetParent;
                return e || on
            })
        }
    }), ie.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (e, t) {
        var n = /Y/.test(t);
        ie.fn[e] = function (o) {
            return Ne(this, function (e, o, i) {
                var r = Y(e);
                return void 0 === i ? r ? t in r ? r[t] : r.document.documentElement[o] : e[o] : void(r ? r.scrollTo(n ? ie(r).scrollLeft() : i, n ? i : ie(r).scrollTop()) : e[o] = i)
            }, e, o, arguments.length, null)
        }
    }), ie.each(["top", "left"], function (e, t) {
        ie.cssHooks[t] = C(ne.pixelPosition, function (e, n) {
            return n ? (n = tt(e, t), ot.test(n) ? ie(e).position()[t] + "px" : n) : void 0
        })
    }), ie.each({Height: "height", Width: "width"}, function (e, t) {
        ie.each({padding: "inner" + e, content: t, "": "outer" + e}, function (n, o) {
            ie.fn[o] = function (o, i) {
                var r = arguments.length && (n || "boolean" != typeof o),
                    s = n || (o === !0 || i === !0 ? "margin" : "border");
                return Ne(this, function (t, n, o) {
                    var i;
                    return ie.isWindow(t) ? t.document.documentElement["client" + e] : 9 === t.nodeType ? (i = t.documentElement, Math.max(t.body["scroll" + e], i["scroll" + e], t.body["offset" + e], i["offset" + e], i["client" + e])) : void 0 === o ? ie.css(t, n, s) : ie.style(t, n, o, s)
                }, t, r ? o : void 0, r, null)
            }
        })
    }), ie.fn.size = function () {
        return this.length
    }, ie.fn.andSelf = ie.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
        return ie
    });
    var rn = e.jQuery, sn = e.$;
    return ie.noConflict = function (t) {
        return e.$ === ie && (e.$ = sn), t && e.jQuery === ie && (e.jQuery = rn), ie
    }, typeof t === Se && (e.jQuery = e.$ = ie), ie
}), function () {
    function e() {
    }

    function t(e, t) {
        for (var n = e.length; n--;) if (e[n].listener === t) return n;
        return -1
    }

    function n(e) {
        return function () {
            return this[e].apply(this, arguments)
        }
    }

    var o = e.prototype, i = this, r = i.EventEmitter;
    o.getListeners = function (e) {
        var t, n, o = this._getEvents();
        if ("object" == typeof e) {
            t = {};
            for (n in o) o.hasOwnProperty(n) && e.test(n) && (t[n] = o[n])
        } else t = o[e] || (o[e] = []);
        return t
    }, o.flattenListeners = function (e) {
        var t, n = [];
        for (t = 0; e.length > t; t += 1) n.push(e[t].listener);
        return n
    }, o.getListenersAsObject = function (e) {
        var t, n = this.getListeners(e);
        return n instanceof Array && (t = {}, t[e] = n), t || n
    }, o.addListener = function (e, n) {
        var o, i = this.getListenersAsObject(e), r = "object" == typeof n;
        for (o in i) i.hasOwnProperty(o) && -1 === t(i[o], n) && i[o].push(r ? n : {listener: n, once: !1});
        return this
    }, o.on = n("addListener"), o.addOnceListener = function (e, t) {
        return this.addListener(e, {listener: t, once: !0})
    }, o.once = n("addOnceListener"), o.defineEvent = function (e) {
        return this.getListeners(e), this
    }, o.defineEvents = function (e) {
        for (var t = 0; e.length > t; t += 1) this.defineEvent(e[t]);
        return this
    }, o.removeListener = function (e, n) {
        var o, i, r = this.getListenersAsObject(e);
        for (i in r) r.hasOwnProperty(i) && (o = t(r[i], n), -1 !== o && r[i].splice(o, 1));
        return this
    }, o.off = n("removeListener"), o.addListeners = function (e, t) {
        return this.manipulateListeners(!1, e, t)
    }, o.removeListeners = function (e, t) {
        return this.manipulateListeners(!0, e, t)
    }, o.manipulateListeners = function (e, t, n) {
        var o, i, r = e ? this.removeListener : this.addListener, s = e ? this.removeListeners : this.addListeners;
        if ("object" != typeof t || t instanceof RegExp) for (o = n.length; o--;) r.call(this, t, n[o]); else for (o in t) t.hasOwnProperty(o) && (i = t[o]) && ("function" == typeof i ? r.call(this, o, i) : s.call(this, o, i));
        return this
    }, o.removeEvent = function (e) {
        var t, n = typeof e, o = this._getEvents();
        if ("string" === n) delete o[e]; else if ("object" === n) for (t in o) o.hasOwnProperty(t) && e.test(t) && delete o[t]; else delete this._events;
        return this
    }, o.removeAllListeners = n("removeEvent"), o.emitEvent = function (e, t) {
        var n, o, i, r, s = this.getListenersAsObject(e);
        for (i in s) if (s.hasOwnProperty(i)) for (o = s[i].length; o--;) n = s[i][o], n.once === !0 && this.removeListener(e, n.listener), r = n.listener.apply(this, t || []), r === this._getOnceReturnValue() && this.removeListener(e, n.listener);
        return this
    }, o.trigger = n("emitEvent"), o.emit = function (e) {
        var t = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(e, t)
    }, o.setOnceReturnValue = function (e) {
        return this._onceReturnValue = e, this
    }, o._getOnceReturnValue = function () {
        return !this.hasOwnProperty("_onceReturnValue") || this._onceReturnValue
    }, o._getEvents = function () {
        return this._events || (this._events = {})
    }, e.noConflict = function () {
        return i.EventEmitter = r, e
    }, "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function () {
        return e
    }) : "object" == typeof module && module.exports ? module.exports = e : this.EventEmitter = e
}.call(this), function (e) {
    function t(t) {
        var n = e.event;
        return n.target = n.target || n.srcElement || t, n
    }

    var n = document.documentElement, o = function () {
    };
    n.addEventListener ? o = function (e, t, n) {
        e.addEventListener(t, n, !1)
    } : n.attachEvent && (o = function (e, n, o) {
        e[n + o] = o.handleEvent ? function () {
            var n = t(e);
            o.handleEvent.call(o, n)
        } : function () {
            var n = t(e);
            o.call(e, n)
        }, e.attachEvent("on" + n, e[n + o])
    });
    var i = function () {
    };
    n.removeEventListener ? i = function (e, t, n) {
        e.removeEventListener(t, n, !1)
    } : n.detachEvent && (i = function (e, t, n) {
        e.detachEvent("on" + t, e[t + n]);
        try {
            delete e[t + n]
        } catch (o) {
            e[t + n] = void 0
        }
    });
    var r = {bind: o, unbind: i};
    "function" == typeof define && define.amd ? define("eventie/eventie", r) : e.eventie = r
}(this), function (e, t) {
    "function" == typeof define && define.amd ? define(["eventEmitter/EventEmitter", "eventie/eventie"], function (n, o) {
        return t(e, n, o)
    }) : "object" == typeof exports ? module.exports = t(e, require("wolfy87-eventemitter"), require("eventie")) : e.imagesLoaded = t(e, e.EventEmitter, e.eventie)
}(window, function (e, t, n) {
    function o(e, t) {
        for (var n in t) e[n] = t[n];
        return e
    }

    function i(e) {
        return "[object Array]" === h.call(e)
    }

    function r(e) {
        var t = [];
        if (i(e)) t = e; else if ("number" == typeof e.length) for (var n = 0, o = e.length; o > n; n++) t.push(e[n]); else t.push(e);
        return t
    }

    function s(e, t, n) {
        if (!(this instanceof s)) return new s(e, t);
        "string" == typeof e && (e = document.querySelectorAll(e)), this.elements = r(e), this.options = o({}, this.options), "function" == typeof t ? n = t : o(this.options, t), n && this.on("always", n), this.getImages(), c && (this.jqDeferred = new c.Deferred);
        var i = this;
        setTimeout(function () {
            i.check()
        })
    }

    function a(e) {
        this.img = e
    }

    function l(e) {
        this.src = e, p[e] = this
    }

    var c = e.jQuery, u = e.console, d = void 0 !== u, h = Object.prototype.toString;
    s.prototype = new t, s.prototype.options = {}, s.prototype.getImages = function () {
        this.images = [];
        for (var e = 0, t = this.elements.length; t > e; e++) {
            var n = this.elements[e];
            "IMG" === n.nodeName && this.addImage(n);
            var o = n.nodeType;
            if (o && (1 === o || 9 === o || 11 === o)) for (var i = n.querySelectorAll("img"), r = 0, s = i.length; s > r; r++) {
                var a = i[r];
                this.addImage(a)
            }
        }
    }, s.prototype.addImage = function (e) {
        var t = new a(e);
        this.images.push(t)
    }, s.prototype.check = function () {
        function e(e, i) {
            return t.options.debug && d && u.log("confirm", e, i), t.progress(e), n++, n === o && t.complete(), !0
        }

        var t = this, n = 0, o = this.images.length;
        if (this.hasAnyBroken = !1, !o) return void this.complete();
        for (var i = 0; o > i; i++) {
            var r = this.images[i];
            r.on("confirm", e), r.check()
        }
    }, s.prototype.progress = function (e) {
        this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded;
        var t = this;
        setTimeout(function () {
            t.emit("progress", t, e), t.jqDeferred && t.jqDeferred.notify && t.jqDeferred.notify(t, e)
        })
    }, s.prototype.complete = function () {
        var e = this.hasAnyBroken ? "fail" : "done";
        this.isComplete = !0;
        var t = this;
        setTimeout(function () {
            if (t.emit(e, t), t.emit("always", t), t.jqDeferred) {
                var n = t.hasAnyBroken ? "reject" : "resolve";
                t.jqDeferred[n](t)
            }
        })
    }, c && (c.fn.imagesLoaded = function (e, t) {
        var n = new s(this, e, t);
        return n.jqDeferred.promise(c(this))
    }), a.prototype = new t, a.prototype.check = function () {
        var e = p[this.img.src] || new l(this.img.src);
        if (e.isConfirmed) return void this.confirm(e.isLoaded, "cached was confirmed");
        if (this.img.complete && void 0 !== this.img.naturalWidth) return void this.confirm(0 !== this.img.naturalWidth, "naturalWidth");
        var t = this;
        e.on("confirm", function (e, n) {
            return t.confirm(e.isLoaded, n), !0
        }), e.check()
    }, a.prototype.confirm = function (e, t) {
        this.isLoaded = e, this.emit("confirm", this, t)
    };
    var p = {};
    return l.prototype = new t, l.prototype.check = function () {
        if (!this.isChecked) {
            var e = new Image;
            n.bind(e, "load", this), n.bind(e, "error", this), e.src = this.src, this.isChecked = !0
        }
    }, l.prototype.handleEvent = function (e) {
        var t = "on" + e.type;
        this[t] && this[t](e)
    }, l.prototype.onload = function (e) {
        this.confirm(!0, "onload"), this.unbindProxyEvents(e)
    }, l.prototype.onerror = function (e) {
        this.confirm(!1, "onerror"), this.unbindProxyEvents(e)
    }, l.prototype.confirm = function (e, t) {
        this.isConfirmed = !0, this.isLoaded = e, this.emit("confirm", this, t)
    }, l.prototype.unbindProxyEvents = function (e) {
        n.unbind(e.target, "load", this), n.unbind(e.target, "error", this)
    }, s
}), !function (e, t) {
    "use strict";
    "function" == typeof define && define.amd ? define("jquery-bridget/jquery-bridget", ["jquery"], function (n) {
        t(e, n)
    }) : "object" == typeof module && module.exports ? module.exports = t(e, require("jquery")) : e.jQueryBridget = t(e, e.jQuery)
}(window, function (e, t) {
    "use strict";

    function n(n, r, a) {
        function l(e, t, o) {
            var i, r = "$()." + n + '("' + t + '")';
            return e.each(function (e, l) {
                var c = a.data(l, n);
                if (!c) return void s(n + " not initialized. Cannot call methods, i.e. " + r);
                var u = c[t];
                if (!u || "_" == t.charAt(0)) return void s(r + " is not a valid method");
                var d = u.apply(c, o);
                i = void 0 === i ? d : i
            }), void 0 !== i ? i : e
        }

        function c(e, t) {
            e.each(function (e, o) {
                var i = a.data(o, n);
                i ? (i.option(t), i._init()) : (i = new r(o, t), a.data(o, n, i))
            })
        }

        a = a || t || e.jQuery, a && (r.prototype.option || (r.prototype.option = function (e) {
            a.isPlainObject(e) && (this.options = a.extend(!0, this.options, e))
        }), a.fn[n] = function (e) {
            if ("string" == typeof e) {
                var t = i.call(arguments, 1);
                return l(this, e, t)
            }
            return c(this, e), this
        }, o(a))
    }

    function o(e) {
        !e || e && e.bridget || (e.bridget = n)
    }

    var i = Array.prototype.slice, r = e.console, s = "undefined" == typeof r ? function () {
    } : function (e) {
        r.error(e)
    };
    return o(t || e.jQuery), n
}), function (e, t) {
    "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", t) : "object" == typeof module && module.exports ? module.exports = t() : e.EvEmitter = t()
}(this, function () {
    function e() {
    }

    var t = e.prototype;
    return t.on = function (e, t) {
        if (e && t) {
            var n = this._events = this._events || {}, o = n[e] = n[e] || [];
            return -1 == o.indexOf(t) && o.push(t), this
        }
    }, t.once = function (e, t) {
        if (e && t) {
            this.on(e, t);
            var n = this._onceEvents = this._onceEvents || {}, o = n[e] = n[e] || {};
            return o[t] = !0, this
        }
    }, t.off = function (e, t) {
        var n = this._events && this._events[e];
        if (n && n.length) {
            var o = n.indexOf(t);
            return -1 != o && n.splice(o, 1), this
        }
    }, t.emitEvent = function (e, t) {
        var n = this._events && this._events[e];
        if (n && n.length) {
            var o = 0, i = n[o];
            t = t || [];
            for (var r = this._onceEvents && this._onceEvents[e]; i;) {
                var s = r && r[i];
                s && (this.off(e, i), delete r[i]), i.apply(this, t), o += s ? 0 : 1, i = n[o]
            }
            return this
        }
    }, e
}), function (e, t) {
    "use strict";
    "function" == typeof define && define.amd ? define("get-size/get-size", [], function () {
        return t()
    }) : "object" == typeof module && module.exports ? module.exports = t() : e.getSize = t()
}(window, function () {
    "use strict";

    function e(e) {
        var t = parseFloat(e), n = -1 == e.indexOf("%") && !isNaN(t);
        return n && t
    }

    function t() {
    }

    function n() {
        for (var e = {
            width: 0,
            height: 0,
            innerWidth: 0,
            innerHeight: 0,
            outerWidth: 0,
            outerHeight: 0
        }, t = 0; c > t; t++) {
            var n = l[t];
            e[n] = 0
        }
        return e
    }

    function o(e) {
        var t = getComputedStyle(e);
        return t || a("Style returned " + t + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), t
    }

    function i() {
        if (!u) {
            u = !0;
            var t = document.createElement("div");
            t.style.width = "200px", t.style.padding = "1px 2px 3px 4px", t.style.borderStyle = "solid", t.style.borderWidth = "1px 2px 3px 4px", t.style.boxSizing = "border-box";
            var n = document.body || document.documentElement;
            n.appendChild(t);
            var i = o(t);
            r.isBoxSizeOuter = s = 200 == e(i.width), n.removeChild(t)
        }
    }

    function r(t) {
        if (i(), "string" == typeof t && (t = document.querySelector(t)), t && "object" == typeof t && t.nodeType) {
            var r = o(t);
            if ("none" == r.display) return n();
            var a = {};
            a.width = t.offsetWidth, a.height = t.offsetHeight;
            for (var u = a.isBorderBox = "border-box" == r.boxSizing, d = 0; c > d; d++) {
                var h = l[d], p = r[h], f = parseFloat(p);
                a[h] = isNaN(f) ? 0 : f
            }
            var m = a.paddingLeft + a.paddingRight, g = a.paddingTop + a.paddingBottom,
                v = a.marginLeft + a.marginRight, y = a.marginTop + a.marginBottom,
                b = a.borderLeftWidth + a.borderRightWidth, w = a.borderTopWidth + a.borderBottomWidth, x = u && s,
                T = e(r.width);
            T !== !1 && (a.width = T + (x ? 0 : m + b));
            var S = e(r.height);
            return S !== !1 && (a.height = S + (x ? 0 : g + w)), a.innerWidth = a.width - (m + b), a.innerHeight = a.height - (g + w), a.outerWidth = a.width + v, a.outerHeight = a.height + y, a
        }
    }

    var s, a = "undefined" == typeof console ? t : function (e) {
        },
        l = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"],
        c = l.length, u = !1;
    return r
}), function (e, t) {
    "use strict";
    "function" == typeof define && define.amd ? define("desandro-matches-selector/matches-selector", t) : "object" == typeof module && module.exports ? module.exports = t() : e.matchesSelector = t()
}(window, function () {
    "use strict";
    var e = function () {
        var e = Element.prototype;
        if (e.matches) return "matches";
        if (e.matchesSelector) return "matchesSelector";
        for (var t = ["webkit", "moz", "ms", "o"], n = 0; n < t.length; n++) {
            var o = t[n], i = o + "MatchesSelector";
            if (e[i]) return i
        }
    }();
    return function (t, n) {
        return t[e](n)
    }
}), function (e, t) {
    "function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["desandro-matches-selector/matches-selector"], function (n) {
        return t(e, n)
    }) : "object" == typeof module && module.exports ? module.exports = t(e, require("desandro-matches-selector")) : e.fizzyUIUtils = t(e, e.matchesSelector)
}(window, function (e, t) {
    var n = {};
    n.extend = function (e, t) {
        for (var n in t) e[n] = t[n];
        return e
    }, n.modulo = function (e, t) {
        return (e % t + t) % t
    }, n.makeArray = function (e) {
        var t = [];
        if (Array.isArray(e)) t = e; else if (e && "number" == typeof e.length) for (var n = 0; n < e.length; n++) t.push(e[n]); else t.push(e);
        return t
    }, n.removeFrom = function (e, t) {
        var n = e.indexOf(t);
        -1 != n && e.splice(n, 1)
    }, n.getParent = function (e, n) {
        for (; e != document.body;) if (e = e.parentNode, t(e, n)) return e
    }, n.getQueryElement = function (e) {
        return "string" == typeof e ? document.querySelector(e) : e
    }, n.handleEvent = function (e) {
        var t = "on" + e.type;
        this[t] && this[t](e)
    }, n.filterFindElements = function (e, o) {
        e = n.makeArray(e);
        var i = [];
        return e.forEach(function (e) {
            if (e instanceof HTMLElement) {
                if (!o) return void i.push(e);
                t(e, o) && i.push(e);
                for (var n = e.querySelectorAll(o), r = 0; r < n.length; r++) i.push(n[r])
            }
        }), i
    }, n.debounceMethod = function (e, t, n) {
        var o = e.prototype[t], i = t + "Timeout";
        e.prototype[t] = function () {
            var e = this[i];
            e && clearTimeout(e);
            var t = arguments, r = this;
            this[i] = setTimeout(function () {
                o.apply(r, t), delete r[i]
            }, n || 100)
        }
    }, n.docReady = function (e) {
        "complete" == document.readyState ? e() : document.addEventListener("DOMContentLoaded", e)
    }, n.toDashed = function (e) {
        return e.replace(/(.)([A-Z])/g, function (e, t, n) {
            return t + "-" + n
        }).toLowerCase()
    };
    var o = e.console;
    return n.htmlInit = function (t, i) {
        n.docReady(function () {
            var r = n.toDashed(i), s = "data-" + r, a = document.querySelectorAll("[" + s + "]"),
                l = document.querySelectorAll(".js-" + r), c = n.makeArray(a).concat(n.makeArray(l)),
                u = s + "-options", d = e.jQuery;
            c.forEach(function (e) {
                var n, r = e.getAttribute(s) || e.getAttribute(u);
                try {
                    n = r && JSON.parse(r)
                } catch (a) {
                    return void(o && o.error("Error parsing " + s + " on " + e.className + ": " + a))
                }
                var l = new t(e, n);
                d && d.data(e, i, l)
            })
        })
    }, n
}), function (e, t) {
    "function" == typeof define && define.amd ? define("outlayer/item", ["ev-emitter/ev-emitter", "get-size/get-size"], t) : "object" == typeof module && module.exports ? module.exports = t(require("ev-emitter"), require("get-size")) : (e.Outlayer = {}, e.Outlayer.Item = t(e.EvEmitter, e.getSize))
}(window, function (e, t) {
    "use strict";

    function n(e) {
        for (var t in e) return !1;
        return t = null, !0
    }

    function o(e, t) {
        e && (this.element = e, this.layout = t, this.position = {x: 0, y: 0}, this._create())
    }

    function i(e) {
        return e.replace(/([A-Z])/g, function (e) {
            return "-" + e.toLowerCase()
        })
    }

    var r = document.documentElement.style, s = "string" == typeof r.transition ? "transition" : "WebkitTransition",
        a = "string" == typeof r.transform ? "transform" : "WebkitTransform",
        l = {WebkitTransition: "webkitTransitionEnd", transition: "transitionend"}[s], c = {
            transform: a,
            transition: s,
            transitionDuration: s + "Duration",
            transitionProperty: s + "Property",
            transitionDelay: s + "Delay"
        }, u = o.prototype = Object.create(e.prototype);
    u.constructor = o, u._create = function () {
        this._transn = {ingProperties: {}, clean: {}, onEnd: {}}, this.css({position: "absolute"})
    }, u.handleEvent = function (e) {
        var t = "on" + e.type;
        this[t] && this[t](e)
    }, u.getSize = function () {
        this.size = t(this.element)
    }, u.css = function (e) {
        var t = this.element.style;
        for (var n in e) {
            var o = c[n] || n;
            t[o] = e[n]
        }
    }, u.getPosition = function () {
        var e = getComputedStyle(this.element), t = this.layout._getOption("originLeft"),
            n = this.layout._getOption("originTop"), o = e[t ? "left" : "right"], i = e[n ? "top" : "bottom"],
            r = this.layout.size, s = -1 != o.indexOf("%") ? parseFloat(o) / 100 * r.width : parseInt(o, 10),
            a = -1 != i.indexOf("%") ? parseFloat(i) / 100 * r.height : parseInt(i, 10);
        s = isNaN(s) ? 0 : s, a = isNaN(a) ? 0 : a, s -= t ? r.paddingLeft : r.paddingRight, a -= n ? r.paddingTop : r.paddingBottom, this.position.x = s, this.position.y = a
    }, u.layoutPosition = function () {
        var e = this.layout.size, t = {}, n = this.layout._getOption("originLeft"),
            o = this.layout._getOption("originTop"), i = n ? "paddingLeft" : "paddingRight", r = n ? "left" : "right",
            s = n ? "right" : "left", a = this.position.x + e[i];
        t[r] = this.getXValue(a), t[s] = "";
        var l = o ? "paddingTop" : "paddingBottom", c = o ? "top" : "bottom", u = o ? "bottom" : "top",
            d = this.position.y + e[l];
        t[c] = this.getYValue(d), t[u] = "", this.css(t), this.emitEvent("layout", [this])
    }, u.getXValue = function (e) {
        var t = this.layout._getOption("horizontal");
        return this.layout.options.percentPosition && !t ? e / this.layout.size.width * 100 + "%" : e + "px"
    }, u.getYValue = function (e) {
        var t = this.layout._getOption("horizontal");
        return this.layout.options.percentPosition && t ? e / this.layout.size.height * 100 + "%" : e + "px"
    }, u._transitionTo = function (e, t) {
        this.getPosition();
        var n = this.position.x, o = this.position.y, i = parseInt(e, 10), r = parseInt(t, 10),
            s = i === this.position.x && r === this.position.y;
        if (this.setPosition(e, t), s && !this.isTransitioning) return void this.layoutPosition();
        var a = e - n, l = t - o, c = {};
        c.transform = this.getTranslate(a, l), this.transition({
            to: c,
            onTransitionEnd: {transform: this.layoutPosition},
            isCleaning: !0
        })
    }, u.getTranslate = function (e, t) {
        var n = this.layout._getOption("originLeft"), o = this.layout._getOption("originTop");
        return e = n ? e : -e, t = o ? t : -t, "translate3d(" + e + "px, " + t + "px, 0)"
    }, u.goTo = function (e, t) {
        this.setPosition(e, t), this.layoutPosition()
    }, u.moveTo = u._transitionTo, u.setPosition = function (e, t) {
        this.position.x = parseInt(e, 10), this.position.y = parseInt(t, 10)
    }, u._nonTransition = function (e) {
        this.css(e.to), e.isCleaning && this._removeStyles(e.to);
        for (var t in e.onTransitionEnd) e.onTransitionEnd[t].call(this)
    }, u.transition = function (e) {
        if (!parseFloat(this.layout.options.transitionDuration)) return void this._nonTransition(e);
        var t = this._transn;
        for (var n in e.onTransitionEnd) t.onEnd[n] = e.onTransitionEnd[n];
        for (n in e.to) t.ingProperties[n] = !0, e.isCleaning && (t.clean[n] = !0);
        if (e.from) {
            this.css(e.from);
            var o = this.element.offsetHeight;
            o = null
        }
        this.enableTransition(e.to), this.css(e.to), this.isTransitioning = !0
    };
    var d = "opacity," + i(a);
    u.enableTransition = function () {
        if (!this.isTransitioning) {
            var e = this.layout.options.transitionDuration;
            e = "number" == typeof e ? e + "ms" : e, this.css({
                transitionProperty: d,
                transitionDuration: e,
                transitionDelay: this.staggerDelay || 0
            }), this.element.addEventListener(l, this, !1)
        }
    }, u.onwebkitTransitionEnd = function (e) {
        this.ontransitionend(e)
    }, u.onotransitionend = function (e) {
        this.ontransitionend(e)
    };
    var h = {"-webkit-transform": "transform"};
    u.ontransitionend = function (e) {
        if (e.target === this.element) {
            var t = this._transn, o = h[e.propertyName] || e.propertyName;
            if (delete t.ingProperties[o], n(t.ingProperties) && this.disableTransition(), o in t.clean && (this.element.style[e.propertyName] = "", delete t.clean[o]), o in t.onEnd) {
                var i = t.onEnd[o];
                i.call(this), delete t.onEnd[o]
            }
            this.emitEvent("transitionEnd", [this])
        }
    }, u.disableTransition = function () {
        this.removeTransitionStyles(), this.element.removeEventListener(l, this, !1), this.isTransitioning = !1
    }, u._removeStyles = function (e) {
        var t = {};
        for (var n in e) t[n] = "";
        this.css(t)
    };
    var p = {transitionProperty: "", transitionDuration: "", transitionDelay: ""};
    return u.removeTransitionStyles = function () {
        this.css(p)
    }, u.stagger = function (e) {
        e = isNaN(e) ? 0 : e, this.staggerDelay = e + "ms"
    }, u.removeElem = function () {
        this.element.parentNode.removeChild(this.element), this.css({display: ""}), this.emitEvent("remove", [this])
    }, u.remove = function () {
        return s && parseFloat(this.layout.options.transitionDuration) ? (this.once("transitionEnd", function () {
            this.removeElem()
        }), void this.hide()) : void this.removeElem()
    }, u.reveal = function () {
        delete this.isHidden, this.css({display: ""});
        var e = this.layout.options, t = {}, n = this.getHideRevealTransitionEndProperty("visibleStyle");
        t[n] = this.onRevealTransitionEnd, this.transition({
            from: e.hiddenStyle,
            to: e.visibleStyle,
            isCleaning: !0,
            onTransitionEnd: t
        })
    }, u.onRevealTransitionEnd = function () {
        this.isHidden || this.emitEvent("reveal")
    }, u.getHideRevealTransitionEndProperty = function (e) {
        var t = this.layout.options[e];
        if (t.opacity) return "opacity";
        for (var n in t) return n
    }, u.hide = function () {
        this.isHidden = !0, this.css({display: ""});
        var e = this.layout.options, t = {}, n = this.getHideRevealTransitionEndProperty("hiddenStyle");
        t[n] = this.onHideTransitionEnd, this.transition({
            from: e.visibleStyle,
            to: e.hiddenStyle,
            isCleaning: !0,
            onTransitionEnd: t
        })
    }, u.onHideTransitionEnd = function () {
        this.isHidden && (this.css({display: "none"}), this.emitEvent("hide"))
    }, u.destroy = function () {
        this.css({position: "", left: "", right: "", top: "", bottom: "", transition: "", transform: ""})
    }, o
}), function (e, t) {
    "use strict";
    "function" == typeof define && define.amd ? define("outlayer/outlayer", ["ev-emitter/ev-emitter", "get-size/get-size", "fizzy-ui-utils/utils", "./item"], function (n, o, i, r) {
        return t(e, n, o, i, r)
    }) : "object" == typeof module && module.exports ? module.exports = t(e, require("ev-emitter"), require("get-size"), require("fizzy-ui-utils"), require("./item")) : e.Outlayer = t(e, e.EvEmitter, e.getSize, e.fizzyUIUtils, e.Outlayer.Item)
}(window, function (e, t, n, o, i) {
    "use strict";

    function r(e, t) {
        var n = o.getQueryElement(e);
        if (!n) return void(l && l.error("Bad element for " + this.constructor.namespace + ": " + (n || e)));
        this.element = n, c && (this.$element = c(this.element)), this.options = o.extend({}, this.constructor.defaults), this.option(t);
        var i = ++d;
        this.element.outlayerGUID = i, h[i] = this, this._create();
        var r = this._getOption("initLayout");
        r && this.layout()
    }

    function s(e) {
        function t() {
            e.apply(this, arguments)
        }

        return t.prototype = Object.create(e.prototype), t.prototype.constructor = t, t
    }

    function a(e) {
        if ("number" == typeof e) return e;
        var t = e.match(/(^\d*\.?\d*)(\w*)/), n = t && t[1], o = t && t[2];
        if (!n.length) return 0;
        n = parseFloat(n);
        var i = f[o] || 1;
        return n * i
    }

    var l = e.console, c = e.jQuery, u = function () {
    }, d = 0, h = {};
    r.namespace = "outlayer", r.Item = i, r.defaults = {
        containerStyle: {position: "relative"},
        initLayout: !0,
        originLeft: !0,
        originTop: !0,
        resize: !0,
        resizeContainer: !0,
        transitionDuration: "0.4s",
        hiddenStyle: {opacity: 0, transform: "scale(0.001)"},
        visibleStyle: {opacity: 1, transform: "scale(1)"}
    };
    var p = r.prototype;
    o.extend(p, t.prototype), p.option = function (e) {
        o.extend(this.options, e)
    }, p._getOption = function (e) {
        var t = this.constructor.compatOptions[e];
        return t && void 0 !== this.options[t] ? this.options[t] : this.options[e]
    }, r.compatOptions = {
        initLayout: "isInitLayout",
        horizontal: "isHorizontal",
        layoutInstant: "isLayoutInstant",
        originLeft: "isOriginLeft",
        originTop: "isOriginTop",
        resize: "isResizeBound",
        resizeContainer: "isResizingContainer"
    }, p._create = function () {
        this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), o.extend(this.element.style, this.options.containerStyle);
        var e = this._getOption("resize");
        e && this.bindResize()
    }, p.reloadItems = function () {
        this.items = this._itemize(this.element.children)
    }, p._itemize = function (e) {
        for (var t = this._filterFindItemElements(e), n = this.constructor.Item, o = [], i = 0; i < t.length; i++) {
            var r = t[i], s = new n(r, this);
            o.push(s)
        }
        return o
    }, p._filterFindItemElements = function (e) {
        return o.filterFindElements(e, this.options.itemSelector)
    }, p.getItemElements = function () {
        return this.items.map(function (e) {
            return e.element
        })
    }, p.layout = function () {
        this._resetLayout(), this._manageStamps();
        var e = this._getOption("layoutInstant"), t = void 0 !== e ? e : !this._isLayoutInited;
        this.layoutItems(this.items, t), this._isLayoutInited = !0
    }, p._init = p.layout, p._resetLayout = function () {
        this.getSize()
    }, p.getSize = function () {
        this.size = n(this.element)
    }, p._getMeasurement = function (e, t) {
        var o, i = this.options[e];
        i ? ("string" == typeof i ? o = this.element.querySelector(i) : i instanceof HTMLElement && (o = i), this[e] = o ? n(o)[t] : i) : this[e] = 0
    }, p.layoutItems = function (e, t) {
        e = this._getItemsForLayout(e), this._layoutItems(e, t), this._postLayout()
    }, p._getItemsForLayout = function (e) {
        return e.filter(function (e) {
            return !e.isIgnored
        })
    }, p._layoutItems = function (e, t) {
        if (this._emitCompleteOnItems("layout", e), e && e.length) {
            var n = [];
            e.forEach(function (e) {
                var o = this._getItemLayoutPosition(e);
                o.item = e, o.isInstant = t || e.isLayoutInstant, n.push(o)
            }, this), this._processLayoutQueue(n)
        }
    }, p._getItemLayoutPosition = function () {
        return {x: 0, y: 0}
    }, p._processLayoutQueue = function (e) {
        this.updateStagger(), e.forEach(function (e, t) {
            this._positionItem(e.item, e.x, e.y, e.isInstant, t)
        }, this)
    }, p.updateStagger = function () {
        var e = this.options.stagger;
        return null === e || void 0 === e ? void(this.stagger = 0) : (this.stagger = a(e), this.stagger)
    }, p._positionItem = function (e, t, n, o, i) {
        o ? e.goTo(t, n) : (e.stagger(i * this.stagger), e.moveTo(t, n))
    }, p._postLayout = function () {
        this.resizeContainer()
    }, p.resizeContainer = function () {
        var e = this._getOption("resizeContainer");
        if (e) {
            var t = this._getContainerSize();
            t && (this._setContainerMeasure(t.width, !0), this._setContainerMeasure(t.height, !1))
        }
    }, p._getContainerSize = u, p._setContainerMeasure = function (e, t) {
        if (void 0 !== e) {
            var n = this.size;
            n.isBorderBox && (e += t ? n.paddingLeft + n.paddingRight + n.borderLeftWidth + n.borderRightWidth : n.paddingBottom + n.paddingTop + n.borderTopWidth + n.borderBottomWidth), e = Math.max(e, 0), this.element.style[t ? "width" : "height"] = e + "px"
        }
    }, p._emitCompleteOnItems = function (e, t) {
        function n() {
            i.dispatchEvent(e + "Complete", null, [t])
        }

        function o() {
            s++, s == r && n()
        }

        var i = this, r = t.length;
        if (!t || !r) return void n();
        var s = 0;
        t.forEach(function (t) {
            t.once(e, o)
        })
    }, p.dispatchEvent = function (e, t, n) {
        var o = t ? [t].concat(n) : n;
        if (this.emitEvent(e, o), c) if (this.$element = this.$element || c(this.element), t) {
            var i = c.Event(t);
            i.type = e, this.$element.trigger(i, n)
        } else this.$element.trigger(e, n)
    }, p.ignore = function (e) {
        var t = this.getItem(e);
        t && (t.isIgnored = !0)
    }, p.unignore = function (e) {
        var t = this.getItem(e);
        t && delete t.isIgnored
    }, p.stamp = function (e) {
        e = this._find(e), e && (this.stamps = this.stamps.concat(e), e.forEach(this.ignore, this))
    }, p.unstamp = function (e) {
        e = this._find(e), e && e.forEach(function (e) {
            o.removeFrom(this.stamps, e), this.unignore(e)
        }, this)
    }, p._find = function (e) {
        return e ? ("string" == typeof e && (e = this.element.querySelectorAll(e)), e = o.makeArray(e)) : void 0
    }, p._manageStamps = function () {
        this.stamps && this.stamps.length && (this._getBoundingRect(), this.stamps.forEach(this._manageStamp, this))
    }, p._getBoundingRect = function () {
        var e = this.element.getBoundingClientRect(), t = this.size;
        this._boundingRect = {
            left: e.left + t.paddingLeft + t.borderLeftWidth,
            top: e.top + t.paddingTop + t.borderTopWidth,
            right: e.right - (t.paddingRight + t.borderRightWidth),
            bottom: e.bottom - (t.paddingBottom + t.borderBottomWidth)
        }
    }, p._manageStamp = u, p._getElementOffset = function (e) {
        var t = e.getBoundingClientRect(), o = this._boundingRect, i = n(e), r = {
            left: t.left - o.left - i.marginLeft,
            top: t.top - o.top - i.marginTop,
            right: o.right - t.right - i.marginRight,
            bottom: o.bottom - t.bottom - i.marginBottom
        };
        return r
    }, p.handleEvent = o.handleEvent, p.bindResize = function () {
        e.addEventListener("resize", this), this.isResizeBound = !0
    }, p.unbindResize = function () {
        e.removeEventListener("resize", this), this.isResizeBound = !1
    }, p.onresize = function () {
        this.resize()
    }, o.debounceMethod(r, "onresize", 100), p.resize = function () {
        this.isResizeBound && this.needsResizeLayout() && this.layout()
    }, p.needsResizeLayout = function () {
        var e = n(this.element), t = this.size && e;
        return t && e.innerWidth !== this.size.innerWidth
    }, p.addItems = function (e) {
        var t = this._itemize(e);
        return t.length && (this.items = this.items.concat(t)), t
    }, p.appended = function (e) {
        var t = this.addItems(e);
        t.length && (this.layoutItems(t, !0), this.reveal(t))
    }, p.prepended = function (e) {
        var t = this._itemize(e);
        if (t.length) {
            var n = this.items.slice(0);
            this.items = t.concat(n), this._resetLayout(), this._manageStamps(), this.layoutItems(t, !0), this.reveal(t), this.layoutItems(n)
        }
    }, p.reveal = function (e) {
        if (this._emitCompleteOnItems("reveal", e), e && e.length) {
            var t = this.updateStagger();
            e.forEach(function (e, n) {
                e.stagger(n * t), e.reveal()
            })
        }
    }, p.hide = function (e) {
        if (this._emitCompleteOnItems("hide", e), e && e.length) {
            var t = this.updateStagger();
            e.forEach(function (e, n) {
                e.stagger(n * t), e.hide()
            })
        }
    }, p.revealItemElements = function (e) {
        var t = this.getItems(e);
        this.reveal(t)
    }, p.hideItemElements = function (e) {
        var t = this.getItems(e);
        this.hide(t)
    }, p.getItem = function (e) {
        for (var t = 0; t < this.items.length; t++) {
            var n = this.items[t];
            if (n.element == e) return n
        }
    }, p.getItems = function (e) {
        e = o.makeArray(e);
        var t = [];
        return e.forEach(function (e) {
            var n = this.getItem(e);
            n && t.push(n)
        }, this), t
    }, p.remove = function (e) {
        var t = this.getItems(e);
        this._emitCompleteOnItems("remove", t), t && t.length && t.forEach(function (e) {
            e.remove(), o.removeFrom(this.items, e)
        }, this)
    }, p.destroy = function () {
        var e = this.element.style;
        e.height = "", e.position = "", e.width = "", this.items.forEach(function (e) {
            e.destroy()
        }), this.unbindResize();
        var t = this.element.outlayerGUID;
        delete h[t], delete this.element.outlayerGUID, c && c.removeData(this.element, this.constructor.namespace)
    }, r.data = function (e) {
        e = o.getQueryElement(e);
        var t = e && e.outlayerGUID;
        return t && h[t]
    }, r.create = function (e, t) {
        var n = s(r);
        return n.defaults = o.extend({}, r.defaults),
            o.extend(n.defaults, t), n.compatOptions = o.extend({}, r.compatOptions), n.namespace = e, n.data = r.data, n.Item = s(i), o.htmlInit(n, e), c && c.bridget && c.bridget(e, n), n
    };
    var f = {ms: 1, s: 1e3};
    return r.Item = i, r
}), function (e, t) {
    "function" == typeof define && define.amd ? define(["outlayer/outlayer", "get-size/get-size"], t) : "object" == typeof module && module.exports ? module.exports = t(require("outlayer"), require("get-size")) : e.Masonry = t(e.Outlayer, e.getSize)
}(window, function (e, t) {
    var n = e.create("masonry");
    return n.compatOptions.fitWidth = "isFitWidth", n.prototype._resetLayout = function () {
        this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns(), this.colYs = [];
        for (var e = 0; e < this.cols; e++) this.colYs.push(0);
        this.maxY = 0
    }, n.prototype.measureColumns = function () {
        if (this.getContainerWidth(), !this.columnWidth) {
            var e = this.items[0], n = e && e.element;
            this.columnWidth = n && t(n).outerWidth || this.containerWidth
        }
        var o = this.columnWidth += this.gutter, i = this.containerWidth + this.gutter, r = i / o, s = o - i % o,
            a = s && 1 > s ? "round" : "floor";
        r = Math[a](r), this.cols = Math.max(r, 1)
    }, n.prototype.getContainerWidth = function () {
        var e = this._getOption("fitWidth"), n = e ? this.element.parentNode : this.element, o = t(n);
        this.containerWidth = o && o.innerWidth
    }, n.prototype._getItemLayoutPosition = function (e) {
        e.getSize();
        var t = e.size.outerWidth % this.columnWidth, n = t && 1 > t ? "round" : "ceil",
            o = Math[n](e.size.outerWidth / this.columnWidth);
        o = Math.min(o, this.cols);
        for (var i = this._getColGroup(o), r = Math.min.apply(Math, i), s = i.indexOf(r), a = {
            x: this.columnWidth * s,
            y: r
        }, l = r + e.size.outerHeight, c = this.cols + 1 - i.length, u = 0; c > u; u++) this.colYs[s + u] = l;
        return a
    }, n.prototype._getColGroup = function (e) {
        if (2 > e) return this.colYs;
        for (var t = [], n = this.cols + 1 - e, o = 0; n > o; o++) {
            var i = this.colYs.slice(o, o + e);
            t[o] = Math.max.apply(Math, i)
        }
        return t
    }, n.prototype._manageStamp = function (e) {
        var n = t(e), o = this._getElementOffset(e), i = this._getOption("originLeft"), r = i ? o.left : o.right,
            s = r + n.outerWidth, a = Math.floor(r / this.columnWidth);
        a = Math.max(0, a);
        var l = Math.floor(s / this.columnWidth);
        l -= s % this.columnWidth ? 0 : 1, l = Math.min(this.cols - 1, l);
        for (var c = this._getOption("originTop"), u = (c ? o.top : o.bottom) + n.outerHeight, d = a; l >= d; d++) this.colYs[d] = Math.max(u, this.colYs[d])
    }, n.prototype._getContainerSize = function () {
        this.maxY = Math.max.apply(Math, this.colYs);
        var e = {height: this.maxY};
        return this._getOption("fitWidth") && (e.width = this._getContainerFitWidth()), e
    }, n.prototype._getContainerFitWidth = function () {
        for (var e = 0, t = this.cols; --t && 0 === this.colYs[t];) e++;
        return (this.cols - e) * this.columnWidth - this.gutter
    }, n.prototype.needsResizeLayout = function () {
        var e = this.containerWidth;
        return this.getContainerWidth(), e != this.containerWidth
    }, n
}), !function (e, t, n, o) {
    function i(e) {
        for (var t = -1, n = e ? e.length : 0, o = []; ++t < n;) {
            var i = e[t];
            i && o.push(i)
        }
        return o
    }

    function r(e) {
        return "[object Function]" === Object.prototype.toString.call(e)
    }

    function s(e) {
        return "[object Array]" === Object.prototype.toString.call(e)
    }

    function a(e, t, n, o) {
        function i(e, t) {
            return 1 - 3 * t + 3 * e
        }

        function r(e, t) {
            return 3 * t - 6 * e
        }

        function s(e) {
            return 3 * e
        }

        function a(e, t, n) {
            return ((i(t, n) * e + r(t, n)) * e + s(t)) * e
        }

        function l(e, t, n) {
            return 3 * i(t, n) * e * e + 2 * r(t, n) * e + s(t)
        }

        function c(t) {
            for (var o = t, i = 0; 8 > i; ++i) {
                var r = l(o, e, n);
                if (0 === r) return o;
                var s = a(o, e, n) - t;
                o -= s / r
            }
            return o
        }

        if (4 !== arguments.length) return !1;
        for (var u = 0; 4 > u; ++u) if ("number" != typeof arguments[u] || isNaN(arguments[u]) || !isFinite(arguments[u])) return !1;
        e = Math.min(e, 1), n = Math.min(n, 1), e = Math.max(e, 0), n = Math.max(n, 0);
        var d = function (i) {
            return e === t && n === o ? i : a(c(i), t, o)
        };
        return d
    }

    function l(e) {
        if (e) for (var t = (new Date).getTime(), n = 0, i = f.State.calls.length; i > n; n++) if (f.State.calls[n]) {
            var r = f.State.calls[n], s = r[0], a = r[2], d = r[3];
            d || (d = f.State.calls[n][3] = t - 16);
            for (var g = Math.min((t - d) / a.duration, 1), v = 0, y = s.length; y > v; v++) {
                var b = s[v], w = b.element;
                if (p.data(w, u)) {
                    var x = !1;
                    a.display && "none" !== a.display && (m.setPropertyValue(w, "display", a.display), f.State.calls[n][2].display = !1);
                    for (var T in b) if ("element" !== T) {
                        var S, k = b[T], E = "string" == typeof k.easing ? f.Easings[k.easing] : k.easing;
                        if (S = 1 === g ? k.endValue : k.startValue + (k.endValue - k.startValue) * E(g), k.currentValue = S, m.Hooks.registered[T]) {
                            var C = m.Hooks.getRoot(T), z = p.data(w, u).rootPropertyValueCache[C];
                            z && (k.rootPropertyValue = z)
                        }
                        var L = m.setPropertyValue(w, T, k.currentValue + (0 === parseFloat(S) ? "" : k.unitType), k.rootPropertyValue, k.scrollData);
                        m.Hooks.registered[T] && (p.data(w, u).rootPropertyValueCache[C] = m.Normalizations.registered[C] ? m.Normalizations.registered[C]("extract", null, L[1]) : L[1]), "transform" === L[0] && (x = !0)
                    }
                    a.mobileHA && p.data(w, u).transformCache.translate3d === o && (p.data(w, u).transformCache.translate3d = "(0, 0, 0)", x = !0), x && m.flushTransformCache(w)
                }
            }
            1 === g && c(n)
        }
        f.State.isTicking && h(l)
    }

    function c(e) {
        for (var t = f.State.calls[e][0], n = f.State.calls[e][1], i = f.State.calls[e][2], r = !1, s = 0, a = t.length; a > s; s++) {
            var l = t[s].element;
            if ("none" !== i.display || i.loop || m.setPropertyValue(l, "display", i.display), p.queue(l)[1] !== o && /\.velocityQueueEntryFlag/i.test(p.queue(l)[1]) || p.data(l, u) && (p.data(l, u).isAnimating = !1, p.data(l, u).rootPropertyValueCache = {}, i.mobileHA && !f.State.isGingerbread && (delete p.data(l, u).transformCache.translate3d, m.flushTransformCache(l))), s === a - 1 && !i.loop && i.complete) {
                var c = n.jquery ? n.get() : n;
                i.complete.call(c, c)
            }
            i.queue !== !1 && p.dequeue(l, i.queue)
        }
        f.State.calls[e] = !1;
        for (var d = 0, h = f.State.calls.length; h > d; d++) if (f.State.calls[d] !== !1) {
            r = !0;
            break
        }
        r === !1 && (f.State.isTicking = !1, delete f.State.calls, f.State.calls = [])
    }

    var u = "velocity", d = function () {
        if (n.documentMode) return n.documentMode;
        for (var e = 7; e > 4; e--) {
            var t = n.createElement("div");
            if (t.innerHTML = "<!--[if IE " + e + "]><span></span><![endif]-->", t.getElementsByTagName("span").length) return t = null, e
        }
        return o
    }(), h = t.requestAnimationFrame || function () {
        var e = 0;
        return t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || function (t) {
            var n, o = (new Date).getTime();
            return n = Math.max(0, 16 - (o - e)), e = o + n, setTimeout(function () {
                t(o + n)
            }, n)
        }
    }();
    if (7 >= d) {
        if (t.jQuery) return void(t.jQuery.fn.velocity = t.jQuery.fn.animate);
        throw new Error("For IE<=7, Velocity falls back to jQuery, which must first be loaded.")
    }
    if (8 === d && !t.jQuery) throw new Error("For IE8, Velocity requires jQuery to be loaded.");
    if (e.Velocity !== o && !e.Velocity.Utilities) throw new Error("Velocity's namespace is occupied. Aborting.");
    var p = t.jQuery || e.Velocity.Utilities, f = e.Velocity = e.velocity = p.extend(e.Velocity || {}, {
        State: {
            isMobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
            isAndroid: /Android/i.test(navigator.userAgent),
            isGingerbread: /Android 2\.3\.[3-7]/i.test(navigator.userAgent),
            prefixElement: n.createElement("div"),
            prefixMatches: {},
            scrollAnchor: null,
            scrollPropertyLeft: null,
            scrollPropertyTop: null,
            isTicking: !1,
            calls: []
        },
        CSS: {},
        Sequences: {},
        Easings: {},
        defaults: {
            queue: "",
            duration: 400,
            easing: "swing",
            complete: null,
            display: null,
            loop: !1,
            delay: !1,
            mobileHA: !0,
            _cacheValues: !0
        },
        animate: function () {
        },
        debug: !1
    });
    t.pageYOffset !== o ? (f.State.scrollAnchor = t, f.State.scrollPropertyLeft = "pageXOffset", f.State.scrollPropertyTop = "pageYOffset") : (f.State.scrollAnchor = n.documentElement || n.body.parentNode || n.body, f.State.scrollPropertyLeft = "scrollLeft", f.State.scrollPropertyTop = "scrollTop"), function () {
        var e = {};
        p.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function (t, n) {
            e[n] = function (e) {
                return Math.pow(e, t + 2)
            }
        }), p.extend(e, {
            Sine: function (e) {
                return 1 - Math.cos(e * Math.PI / 2)
            }, Circ: function (e) {
                return 1 - Math.sqrt(1 - e * e)
            }, Elastic: function (e) {
                return 0 === e || 1 === e ? e : -Math.pow(2, 8 * (e - 1)) * Math.sin((80 * (e - 1) - 7.5) * Math.PI / 15)
            }, Back: function (e) {
                return e * e * (3 * e - 2)
            }, Bounce: function (e) {
                for (var t, n = 4; e < ((t = Math.pow(2, --n)) - 1) / 11;) ;
                return 1 / Math.pow(4, 3 - n) - 7.5625 * Math.pow((3 * t - 2) / 22 - e, 2)
            }
        }), p.each(e, function (e, t) {
            f.Easings["easeIn" + e] = t, f.Easings["easeOut" + e] = function (e) {
                return 1 - t(1 - e)
            }, f.Easings["easeInOut" + e] = function (e) {
                return .5 > e ? t(2 * e) / 2 : 1 - t(-2 * e + 2) / 2
            }
        }), f.Easings.linear = function (e) {
            return e
        }, f.Easings.swing = function (e) {
            return .5 - Math.cos(e * Math.PI) / 2
        }, f.Easings.ease = a(.25, .1, .25, 1), f.Easings["ease-in"] = a(.42, 0, 1, 1), f.Easings["ease-out"] = a(0, 0, .58, 1), f.Easings["ease-in-out"] = a(.42, 0, .58, 1), f.Easings.spring = function (e) {
            return 1 - Math.cos(4.5 * e * Math.PI) * Math.exp(6 * -e)
        }
    }();
    var m = f.CSS = {
        RegEx: {
            valueUnwrap: /^[A-z]+\((.*)\)$/i,
            wrappedValueAlreadyExtracted: /[0-9.]+ [0-9.]+ [0-9.]+( [0-9.]+)?/,
            valueSplit: /([A-z]+\(.+\))|(([A-z0-9#-.]+?)(?=\s|$))/gi
        },
        Hooks: {
            templates: {
                color: ["Red Green Blue Alpha", "255 255 255 1"],
                backgroundColor: ["Red Green Blue Alpha", "255 255 255 1"],
                borderColor: ["Red Green Blue Alpha", "255 255 255 1"],
                outlineColor: ["Red Green Blue Alpha", "255 255 255 1"],
                textShadow: ["Color X Y Blur", "black 0px 0px 0px"],
                boxShadow: ["Color X Y Blur Spread", "black 0px 0px 0px 0px"],
                clip: ["Top Right Bottom Left", "0px 0px 0px 0px"],
                backgroundPosition: ["X Y", "0% 0%"],
                transformOrigin: ["X Y Z", "50% 50% 0%"],
                perspectiveOrigin: ["X Y", "50% 50%"]
            }, registered: {}, register: function () {
                var e, t, n;
                if (d) for (e in m.Hooks.templates) {
                    t = m.Hooks.templates[e], n = t[0].split(" ");
                    var o = t[1].match(m.RegEx.valueSplit);
                    "Color" === n[0] && (n.push(n.shift()), o.push(o.shift()), m.Hooks.templates[e] = [n.join(" "), o.join(" ")])
                }
                for (e in m.Hooks.templates) {
                    t = m.Hooks.templates[e], n = t[0].split(" ");
                    for (var i in n) {
                        var r = e + n[i], s = i;
                        m.Hooks.registered[r] = [e, s]
                    }
                }
            }, getRoot: function (e) {
                var t = m.Hooks.registered[e];
                return t ? t[0] : e
            }, cleanRootPropertyValue: function (e, t) {
                return m.RegEx.valueUnwrap.test(t) && (t = t.match(m.Hooks.RegEx.valueUnwrap)[1]), m.Values.isCSSNullValue(t) && (t = m.Hooks.templates[e][1]), t
            }, extractValue: function (e, t) {
                var n = m.Hooks.registered[e];
                if (n) {
                    var o = n[0], i = n[1];
                    return t = m.Hooks.cleanRootPropertyValue(o, t), t.toString().match(m.RegEx.valueSplit)[i]
                }
                return t
            }, injectValue: function (e, t, n) {
                var o = m.Hooks.registered[e];
                if (o) {
                    var i, r, s = o[0], a = o[1];
                    return n = m.Hooks.cleanRootPropertyValue(s, n), i = n.toString().match(m.RegEx.valueSplit), i[a] = t, r = i.join(" ")
                }
                return n
            }
        },
        Normalizations: {
            registered: {
                clip: function (e, t, n) {
                    switch (e) {
                        case"name":
                            return "clip";
                        case"extract":
                            var o;
                            return m.RegEx.wrappedValueAlreadyExtracted.test(n) ? o = n : (o = n.toString().match(m.RegEx.valueUnwrap), o && (o = o[1].replace(/,(\s+)?/g, " "))), o;
                        case"inject":
                            return "rect(" + n + ")"
                    }
                }, opacity: function (e, t, n) {
                    if (8 >= d) switch (e) {
                        case"name":
                            return "filter";
                        case"extract":
                            var o = n.toString().match(/alpha\(opacity=(.*)\)/i);
                            return n = o ? o[1] / 100 : 1;
                        case"inject":
                            return t.style.zoom = 1, parseFloat(n) >= 1 ? "" : "alpha(opacity=" + parseInt(100 * parseFloat(n), 10) + ")"
                    } else switch (e) {
                        case"name":
                            return "opacity";
                        case"extract":
                            return n;
                        case"inject":
                            return n
                    }
                }
            }, register: function () {
                function e(e) {
                    var t, n = /^#?([a-f\d])([a-f\d])([a-f\d])$/i, o = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;
                    return e = e.replace(n, function (e, t, n, o) {
                        return t + t + n + n + o + o
                    }), t = o.exec(e), t ? "rgb(" + (parseInt(t[1], 16) + " " + parseInt(t[2], 16) + " " + parseInt(t[3], 16)) + ")" : "rgb(0 0 0)"
                }

                var t = ["translateX", "translateY", "scale", "scaleX", "scaleY", "skewX", "skewY", "rotateZ"];
                9 >= d || (t = t.concat(["transformPerspective", "translateZ", "scaleZ", "rotateX", "rotateY"]));
                for (var n = 0, i = t.length; i > n; n++) !function () {
                    var e = t[n];
                    m.Normalizations.registered[e] = function (t, n, i) {
                        switch (t) {
                            case"name":
                                return "transform";
                            case"extract":
                                return p.data(n, u).transformCache[e] === o ? /^scale/i.test(e) ? 1 : 0 : p.data(n, u).transformCache[e].replace(/[()]/g, "");
                            case"inject":
                                var r = !1;
                                switch (e.substr(0, e.length - 1)) {
                                    case"translate":
                                        r = !/(%|px|em|rem|\d)$/i.test(i);
                                        break;
                                    case"scal":
                                    case"scale":
                                        f.State.isAndroid && p.data(n, u).transformCache[e] === o && (i = 1), r = !/(\d)$/i.test(i);
                                        break;
                                    case"skew":
                                        r = !/(deg|\d)$/i.test(i);
                                        break;
                                    case"rotate":
                                        r = !/(deg|\d)$/i.test(i)
                                }
                                return r || (p.data(n, u).transformCache[e] = "(" + i + ")"), p.data(n, u).transformCache[e]
                        }
                    }
                }();
                for (var r = ["color", "backgroundColor", "borderColor", "outlineColor"], n = 0, s = r.length; s > n; n++) !function () {
                    var t = r[n];
                    m.Normalizations.registered[t] = function (n, i, r) {
                        switch (n) {
                            case"name":
                                return t;
                            case"extract":
                                var s;
                                if (m.RegEx.wrappedValueAlreadyExtracted.test(r)) s = r; else {
                                    var a, l = {
                                        aqua: "rgb(0, 255, 255);",
                                        black: "rgb(0, 0, 0)",
                                        blue: "rgb(0, 0, 255)",
                                        fuchsia: "rgb(255, 0, 255)",
                                        gray: "rgb(128, 128, 128)",
                                        green: "rgb(0, 128, 0)",
                                        lime: "rgb(0, 255, 0)",
                                        maroon: "rgb(128, 0, 0)",
                                        navy: "rgb(0, 0, 128)",
                                        olive: "rgb(128, 128, 0)",
                                        purple: "rgb(128, 0, 128)",
                                        red: "rgb(255, 0, 0)",
                                        silver: "rgb(192, 192, 192)",
                                        teal: "rgb(0, 128, 128)",
                                        white: "rgb(255, 255, 255)",
                                        yellow: "rgb(255, 255, 0)"
                                    };
                                    /^[A-z]+$/i.test(r) ? a = l[r] !== o ? l[r] : l.black : /^#([A-f\d]{3}){1,2}$/i.test(r) ? a = e(r) : /^rgba?\(/i.test(r) || (a = l.black), s = (a || r).toString().match(m.RegEx.valueUnwrap)[1].replace(/,(\s+)?/g, " ")
                                }
                                return 8 >= d || 3 !== s.split(" ").length || (s += " 1"), s;
                            case"inject":
                                return 8 >= d ? 4 === r.split(" ").length && (r = r.split(/\s+/).slice(0, 3).join(" ")) : 3 === r.split(" ").length && (r += " 1"), (8 >= d ? "rgb" : "rgba") + "(" + r.replace(/\s+/g, ",").replace(/\.(\d)+(?=,)/g, "") + ")"
                        }
                    }
                }()
            }
        },
        Names: {
            camelCase: function (e) {
                return e.replace(/-(\w)/g, function (e, t) {
                    return t.toUpperCase()
                })
            }, prefixCheck: function (e) {
                if (f.State.prefixMatches[e]) return [f.State.prefixMatches[e], !0];
                for (var t = ["", "Webkit", "Moz", "ms", "O"], n = 0, o = t.length; o > n; n++) {
                    var i;
                    if (i = 0 === n ? e : t[n] + e.replace(/^\w/, function (e) {
                        return e.toUpperCase()
                    }), "string" == typeof f.State.prefixElement.style[i]) return f.State.prefixMatches[e] = i, [i, !0]
                }
                return [e, !1]
            }
        },
        Values: {
            isCSSNullValue: function (e) {
                return 0 == e || /^(none|auto|transparent|(rgba\(0, ?0, ?0, ?0\)))$/i.test(e)
            }, getUnitType: function (e) {
                return /^(rotate|skew)/i.test(e) ? "deg" : /(^(scale|scaleX|scaleY|scaleZ|opacity|alpha|fillOpacity|flexGrow|flexHeight|zIndex|fontWeight)$)|color/i.test(e) ? "" : "px"
            }, getDisplayType: function (e) {
                var t = e.tagName.toString().toLowerCase();
                return /^(b|big|i|small|tt|abbr|acronym|cite|code|dfn|em|kbd|strong|samp|var|a|bdo|br|img|map|object|q|script|span|sub|sup|button|input|label|select|textarea)$/i.test(t) ? "inline" : /^(li)$/i.test(t) ? "list-item" : "block"
            }
        },
        getPropertyValue: function (e, n, i, r) {
            function s(e, n) {
                var i = 0;
                if (8 >= d) i = p.css(e, n); else {
                    if (!r) {
                        if ("height" === n && "border-box" !== m.getPropertyValue(e, "boxSizing").toString().toLowerCase()) return e.offsetHeight - (parseFloat(m.getPropertyValue(e, "borderTopWidth")) || 0) - (parseFloat(m.getPropertyValue(e, "borderBottomWidth")) || 0) - (parseFloat(m.getPropertyValue(e, "paddingTop")) || 0) - (parseFloat(m.getPropertyValue(e, "paddingBottom")) || 0);
                        if ("width" === n && "border-box" !== m.getPropertyValue(e, "boxSizing").toString().toLowerCase()) return e.offsetWidth - (parseFloat(m.getPropertyValue(e, "borderLeftWidth")) || 0) - (parseFloat(m.getPropertyValue(e, "borderRightWidth")) || 0) - (parseFloat(m.getPropertyValue(e, "paddingLeft")) || 0) - (parseFloat(m.getPropertyValue(e, "paddingRight")) || 0)
                    }
                    var a;
                    a = p.data(e, u) === o ? t.getComputedStyle(e, null) : p.data(e, u).computedStyle ? p.data(e, u).computedStyle : p.data(e, u).computedStyle = t.getComputedStyle(e, null), d && "borderColor" === n && (n = "borderTopColor"), i = 9 === d && "filter" === n ? a.getPropertyValue(n) : a[n], ("" === i || null === i) && (i = e.style[n])
                }
                if ("auto" === i && /^(top|right|bottom|left)$/i.test(n)) {
                    var l = s(e, "position");
                    ("fixed" === l || "absolute" === l && /top|left/i.test(n)) && (i = p(e).position()[n] + "px")
                }
                return i
            }

            var a;
            if (m.Hooks.registered[n]) {
                var l = n, c = m.Hooks.getRoot(l);
                i === o && (i = m.getPropertyValue(e, m.Names.prefixCheck(c)[0])), m.Normalizations.registered[c] && (i = m.Normalizations.registered[c]("extract", e, i)), a = m.Hooks.extractValue(l, i)
            } else if (m.Normalizations.registered[n]) {
                var h, g;
                h = m.Normalizations.registered[n]("name", e), "transform" !== h && (g = s(e, m.Names.prefixCheck(h)[0]), m.Values.isCSSNullValue(g) && m.Hooks.templates[n] && (g = m.Hooks.templates[n][1])), a = m.Normalizations.registered[n]("extract", e, g)
            }
            return /^[\d-]/.test(a) || (a = s(e, m.Names.prefixCheck(n)[0])), m.Values.isCSSNullValue(a) && (a = 0), f.debug >= 2 && void 0, a
        },
        setPropertyValue: function (e, n, o, i, r) {
            var s = n;
            if ("scroll" === n) r.container ? r.container["scroll" + r.direction] = o : "Left" === r.direction ? t.scrollTo(o, r.alternateValue) : t.scrollTo(r.alternateValue, o); else if (m.Normalizations.registered[n] && "transform" === m.Normalizations.registered[n]("name", e)) m.Normalizations.registered[n]("inject", e, o), s = "transform", o = p.data(e, u).transformCache[n]; else {
                if (m.Hooks.registered[n]) {
                    var a = n, l = m.Hooks.getRoot(n);
                    i = i || m.getPropertyValue(e, l), o = m.Hooks.injectValue(a, o, i), n = l
                }
                if (m.Normalizations.registered[n] && (o = m.Normalizations.registered[n]("inject", e, o), n = m.Normalizations.registered[n]("name", e)), s = m.Names.prefixCheck(n)[0], 8 >= d) try {
                    e.style[s] = o
                } catch (c) {
                } else e.style[s] = o;
                f.debug >= 2 && void 0
            }
            return [s, o]
        },
        flushTransformCache: function (e) {
            var t, n, o, i = "";
            for (t in p.data(e, u).transformCache) n = p.data(e, u).transformCache[t], "transformPerspective" !== t ? (9 === d && "rotateZ" === t && (t = "rotate"), i += t + n + " ") : o = n;
            o && (i = "perspective" + o + " " + i), m.setPropertyValue(e, "transform", i)
        }
    };
    m.Hooks.register(), m.Normalizations.register(), f.animate = function () {
        function e(e) {
            var t = e;
            return "string" == typeof e ? f.Easings[e] || (t = !1) : t = !!s(e) && a.apply(null, e), t === !1 && (t = f.Easings[f.defaults.easing] ? f.defaults.easing : "swing"), t
        }

        function c() {
            function t() {
                function t(t) {
                    var n = o, i = o, l = o;
                    return s(t) ? (n = t[0], !s(t[1]) && /^[\d-]/.test(t[1]) || r(t[1]) ? l = t[1] : ("string" == typeof t[1] || s(t[1])) && (i = e(t[1]), t[2] && (l = t[2]))) : n = t, i = i || c.easing, r(n) && (n = n.call(a, w, b)), r(l) && (l = l.call(a, w, b)), [n || 0, i, l]
                }

                function x(e, t) {
                    var n, o;
                    return o = (t || 0).toString().toLowerCase().replace(/[%A-z]+$/, function (e) {
                        return n = e, ""
                    }), n || (n = m.Values.getUnitType(e)), [o, n]
                }

                function T() {
                    var e = {
                            parent: a.parentNode,
                            position: m.getPropertyValue(a, "position"),
                            fontSize: m.getPropertyValue(a, "fontSize")
                        }, t = e.position === k.lastPosition && e.parent === k.lastParent,
                        o = e.fontSize === k.lastFontSize && e.parent === k.lastParent;
                    k.lastParent = e.parent, k.lastPosition = e.position, k.lastFontSize = e.fontSize, null === k.remToPxRatio && (k.remToPxRatio = parseFloat(m.getPropertyValue(n.body, "fontSize")) || 16);
                    var i = {
                        overflowX: null,
                        overflowY: null,
                        boxSizing: null,
                        width: null,
                        minWidth: null,
                        maxWidth: null,
                        height: null,
                        minHeight: null,
                        maxHeight: null,
                        paddingLeft: null
                    }, r = {}, s = 10;
                    if (r.remToPxRatio = k.remToPxRatio, d) var l = /^auto$/i.test(a.currentStyle.width),
                        c = /^auto$/i.test(a.currentStyle.height);
                    t && o || (i.overflowX = m.getPropertyValue(a, "overflowX"), i.overflowY = m.getPropertyValue(a, "overflowY"), i.boxSizing = m.getPropertyValue(a, "boxSizing"), i.width = m.getPropertyValue(a, "width", null, !0), i.minWidth = m.getPropertyValue(a, "minWidth"), i.maxWidth = m.getPropertyValue(a, "maxWidth") || "none", i.height = m.getPropertyValue(a, "height", null, !0), i.minHeight = m.getPropertyValue(a, "minHeight"), i.maxHeight = m.getPropertyValue(a, "maxHeight") || "none", i.paddingLeft = m.getPropertyValue(a, "paddingLeft")), t ? (r.percentToPxRatioWidth = k.lastPercentToPxWidth, r.percentToPxRatioHeight = k.lastPercentToPxHeight) : (m.setPropertyValue(a, "overflowX", "hidden"), m.setPropertyValue(a, "overflowY", "hidden"), m.setPropertyValue(a, "boxSizing", "content-box"), m.setPropertyValue(a, "width", s + "%"), m.setPropertyValue(a, "minWidth", s + "%"), m.setPropertyValue(a, "maxWidth", s + "%"), m.setPropertyValue(a, "height", s + "%"), m.setPropertyValue(a, "minHeight", s + "%"), m.setPropertyValue(a, "maxHeight", s + "%")), o ? r.emToPxRatio = k.lastEmToPx : m.setPropertyValue(a, "paddingLeft", s + "em"), t || (r.percentToPxRatioWidth = k.lastPercentToPxWidth = (parseFloat(m.getPropertyValue(a, "width", null, !0)) || 1) / s, r.percentToPxRatioHeight = k.lastPercentToPxHeight = (parseFloat(m.getPropertyValue(a, "height", null, !0)) || 1) / s), o || (r.emToPxRatio = k.lastEmToPx = (parseFloat(m.getPropertyValue(a, "paddingLeft")) || 1) / s);
                    for (var u in i) null !== i[u] && m.setPropertyValue(a, u, i[u]);
                    return d ? (l && m.setPropertyValue(a, "width", "auto"), c && m.setPropertyValue(a, "height", "auto")) : (m.setPropertyValue(a, "height", "auto"), i.height !== m.getPropertyValue(a, "height", null, !0) && m.setPropertyValue(a, "height", i.height), m.setPropertyValue(a, "width", "auto"), i.width !== m.getPropertyValue(a, "width", null, !0) && m.setPropertyValue(a, "width", i.width)), f.debug >= 1 && void 0, r
                }

                if (0 === w && y && r(y.begin)) {
                    var C = g.jquery ? g.get() : g;
                    y.begin.call(C, C)
                }
                if ("scroll" === S) {
                    var z, L, N, _ = /^x$/i.test(c.axis) ? "Left" : "Top", P = parseFloat(c.offset) || 0;
                    c.container ? c.container.jquery || c.container.nodeType ? (c.container = c.container[0] || c.container, z = c.container["scroll" + _], N = z + p(a).position()[_.toLowerCase()] + P) : c.container = null : (z = f.State.scrollAnchor[f.State["scrollProperty" + _]], L = f.State.scrollAnchor[f.State["scrollProperty" + ("Left" === _ ? "Top" : "Left")]], N = p(a).offset()[_.toLowerCase()] + P), h = {
                        scroll: {
                            rootPropertyValue: !1,
                            startValue: z,
                            currentValue: z,
                            endValue: N,
                            unitType: "",
                            easing: c.easing,
                            scrollData: {container: c.container, direction: _, alternateValue: L}
                        }, element: a
                    }
                } else if ("reverse" === S) {
                    if (!p.data(a, u).tweensContainer) return void p.dequeue(a, c.queue);
                    "none" === p.data(a, u).opts.display && (p.data(a, u).opts.display = "block"), p.data(a, u).opts.loop = !1, p.data(a, u).opts.begin = null, p.data(a, u).opts.complete = null, c = p.extend({}, p.data(a, u).opts, y);
                    var M = p.extend(!0, {}, p.data(a, u).tweensContainer);
                    for (var A in M) if ("element" !== A) {
                        var j = M[A].startValue;
                        M[A].startValue = M[A].currentValue = M[A].endValue, M[A].endValue = j, y && (M[A].easing = c.easing)
                    }
                    h = M
                } else if ("start" === S) {
                    var M;
                    p.data(a, u).tweensContainer && p.data(a, u).isAnimating === !0 && (M = p.data(a, u).tweensContainer);
                    for (var H in v) {
                        var D = t(v[H]), R = D[0], O = D[1], $ = D[2];
                        H = m.Names.camelCase(H);
                        var I = m.Hooks.getRoot(H), q = !1;
                        if (m.Names.prefixCheck(I)[1] !== !1 || m.Normalizations.registered[I] !== o) {
                            c.display && "none" !== c.display && /opacity|filter/.test(H) && !$ && 0 !== R && ($ = 0), c._cacheValues && M && M[H] ? ($ === o && ($ = M[H].endValue + M[H].unitType), q = p.data(a, u).rootPropertyValueCache[I]) : m.Hooks.registered[H] ? $ === o ? (q = m.getPropertyValue(a, I), $ = m.getPropertyValue(a, H, q)) : q = m.Hooks.templates[I][1] : $ === o && ($ = m.getPropertyValue(a, H));
                            var W, B, F, V;
                            W = x(H, $), $ = W[0], F = W[1], W = x(H, R), R = W[0].replace(/^([+-\/*])=/, function (e, t) {
                                return V = t, ""
                            }), B = W[1], $ = parseFloat($) || 0, R = parseFloat(R) || 0;
                            var X;
                            if ("%" === B && (/^(fontSize|lineHeight)$/.test(H) ? (R /= 100, B = "em") : /^scale/.test(H) ? (R /= 100, B = "") : /(Red|Green|Blue)$/i.test(H) && (R = R / 100 * 255, B = "")), /[\/*]/.test(V)) B = F; else if (F !== B && 0 !== $) if (0 === R) B = F; else {
                                X = X || T();
                                var Y = /margin|padding|left|right|width|text|word|letter/i.test(H) || /X$/.test(H) ? "x" : "y";
                                switch (F) {
                                    case"%":
                                        $ *= "x" === Y ? X.percentToPxRatioWidth : X.percentToPxRatioHeight;
                                        break;
                                    case"em":
                                        $ *= X.emToPxRatio;
                                        break;
                                    case"rem":
                                        $ *= X.remToPxRatio;
                                        break;
                                    case"px":
                                }
                                switch (B) {
                                    case"%":
                                        $ *= 1 / ("x" === Y ? X.percentToPxRatioWidth : X.percentToPxRatioHeight);
                                        break;
                                    case"em":
                                        $ *= 1 / X.emToPxRatio;
                                        break;
                                    case"rem":
                                        $ *= 1 / X.remToPxRatio;
                                        break;
                                    case"px":
                                }
                            }
                            switch (V) {
                                case"+":
                                    R = $ + R;
                                    break;
                                case"-":
                                    R = $ - R;
                                    break;
                                case"*":
                                    R = $ * R;
                                    break;
                                case"/":
                                    R = $ / R
                            }
                            h[H] = {
                                rootPropertyValue: q,
                                startValue: $,
                                currentValue: $,
                                endValue: R,
                                unitType: B,
                                easing: O
                            }, f.debug && void 0
                        } else f.debug && void 0
                    }
                    h.element = a
                }
                h.element && (E.push(h), p.data(a, u).tweensContainer = h, p.data(a, u).opts = c, p.data(a, u).isAnimating = !0, w === b - 1 ? (f.State.calls.length > 1e4 && (f.State.calls = i(f.State.calls)), f.State.calls.push([E, g, c]), f.State.isTicking === !1 && (f.State.isTicking = !0, l())) : w++)
            }

            var a = this, c = p.extend({}, f.defaults, y), h = {};
            if ("stop" === S) return p.queue(a, "string" == typeof y ? y : "", []), !0;
            switch (p.data(a, u) === o && p.data(a, u, {
                isAnimating: !1,
                computedStyle: null,
                tweensContainer: null,
                rootPropertyValueCache: {},
                transformCache: {}
            }), c.duration.toString().toLowerCase()) {
                case"fast":
                    c.duration = 200;
                    break;
                case"normal":
                    c.duration = 400;
                    break;
                case"slow":
                    c.duration = 600;
                    break;
                default:
                    c.duration = parseFloat(c.duration) || 1
            }
            c.easing = e(c.easing), /^\d/.test(c.delay) && p.queue(a, c.queue, function (e) {
                f.velocityQueueEntryFlag = !0, setTimeout(e, parseFloat(c.delay))
            }), c.display && (c.display = c.display.toString().toLowerCase()), c.mobileHA = c.mobileHA && f.State.isMobile, c.queue === !1 ? t() : p.queue(a, c.queue, function (e) {
                f.velocityQueueEntryFlag = !0, t(e)
            }), "" !== c.queue && "fx" !== c.queue || "inprogress" === p.queue(a)[0] || p.dequeue(a)
        }

        var h, g, v, y;
        this.jquery || t.Zepto && t.Zepto.zepto.isZ(this) ? (h = !0, g = this, v = arguments[0], y = arguments[1]) : (h = !1, g = arguments[0].jquery ? arguments[0].get() : arguments[0], v = arguments[1], y = arguments[2]);
        var b = g.length || 1, w = 0;
        if ("stop" !== v && !p.isPlainObject(y)) {
            var x = h ? 1 : 2;
            y = {};
            for (var T = x; T < arguments.length; T++) !s(arguments[T]) && /^\d/.test(arguments[T]) ? y.duration = parseFloat(arguments[T]) : "string" == typeof arguments[T] ? y.easing = arguments[T] : s(arguments[T]) && 4 === arguments[T].length ? y.easing = arguments[T] : r(arguments[T]) && (y.complete = arguments[T])
        }
        var S;
        switch (v) {
            case"scroll":
                S = "scroll";
                break;
            case"reverse":
                S = "reverse";
                break;
            case"stop":
                S = "stop";
                break;
            default:
                if (!p.isPlainObject(v) || p.isEmptyObject(v)) return "string" == typeof v && f.Sequences[v] ? (p.each(g, function (e, t) {
                    f.Sequences[v].call(t, t, y || {}, e, b)
                }), g) : (f.debug && void 0, g);
                S = "start"
        }
        var k = {
            lastParent: null,
            lastPosition: null,
            lastFontSize: null,
            lastPercentToPxWidth: null,
            lastPercentToPxHeight: null,
            lastEmToPx: null,
            remToPxRatio: null
        }, E = [];
        if (y && y.complete && !r(y.complete) && (y.complete = null), h) g.each(c); else if (g.nodeType) c.call(g); else if (g[0] && g[0].nodeType) for (var C in g) c.call(g[C]);
        var z, L = p.extend({}, f.defaults, y);
        if (L.loop = parseInt(L.loop), z = 2 * L.loop - 1, L.loop) for (var N = 0; z > N; N++) {
            var _ = {delay: L.delay};
            L.complete && N === z - 1 && (_.complete = L.complete), h ? g.velocity("reverse", _) : f.animate(g, "reverse", _)
        }
        return g
    };
    var g = t.jQuery || t.Zepto;
    g && (g.fn.velocity = f.animate, g.fn.velocity.defaults = f.defaults), p.each(["Down", "Up"], function (e, t) {
        f.Sequences["slide" + t] = function (e, n) {
            var o = p.extend({}, n), i = {
                height: null,
                marginTop: null,
                marginBottom: null,
                paddingTop: null,
                paddingBottom: null,
                overflow: null,
                overflowX: null,
                overflowY: null
            }, r = o.begin, s = o.complete, a = !1;
            o.display = "Down" === t ? o.display || "block" : o.display || "none", o.begin = function () {
                function n() {
                    e.style.display = "block", i.height = f.CSS.getPropertyValue(e, "height"), e.style.height = "auto", f.CSS.getPropertyValue(e, "height") === i.height && (a = !0), f.CSS.setPropertyValue(e, "height", i.height + "px")
                }

                if ("Down" === t) {
                    i.overflow = [f.CSS.getPropertyValue(e, "overflow"), 0], i.overflowX = [f.CSS.getPropertyValue(e, "overflowX"), 0], i.overflowY = [f.CSS.getPropertyValue(e, "overflowY"), 0], e.style.overflow = "hidden", e.style.overflowX = "visible", e.style.overflowY = "hidden", n();
                    for (var o in i) /^overflow/.test(o) || (i[o] = [f.CSS.getPropertyValue(e, o), 0]);
                    e.style.display = "none"
                } else {
                    n();
                    for (var o in i) i[o] = [0, f.CSS.getPropertyValue(e, o)];
                    e.style.overflow = "hidden", e.style.overflowX = "visible", e.style.overflowY = "hidden"
                }
                r && r.call(e, e)
            }, o.complete = function (e) {
                var n = "Down" === t ? 0 : 1;
                a === !0 ? i.height[n] = "auto" : i.height[n] += "px";
                for (var o in i) e.style[o] = i[o][n];
                s && s.call(e, e)
            }, f.animate(e, i, o)
        }
    }), p.each(["In", "Out"], function (e, t) {
        f.Sequences["fade" + t] = function (e, n, o, i) {
            var r = p.extend({}, n), s = {opacity: "In" === t ? 1 : 0};
            o !== i - 1 && (r.complete = r.begin = null), r.display || (r.display = "In" === t ? f.CSS.Values.getDisplayType(e) : "none"), f.animate(this, s, r)
        }
    })
}(window.jQuery || window.Zepto || window, window, document), function (e) {
    var t = !1, n = !1, o = 5e3, i = 2e3, r = 0, s = function () {
        var e = document.getElementsByTagName("script"), e = e[e.length - 1].src.split("?")[0];
        return 0 < e.split("/").length ? e.split("/").slice(0, -1).join("/") + "/" : ""
    }();
    Array.prototype.forEach || (Array.prototype.forEach = function (e, t) {
        for (var n = 0, o = this.length; n < o; ++n) e.call(t, this[n], n, this)
    });
    var l = window.requestAnimationFrame || !1, c = window.cancelAnimationFrame || !1;
    ["ms", "moz", "webkit", "o"].forEach(function (e) {
        l || (l = window[e + "RequestAnimationFrame"]), c || (c = window[e + "CancelAnimationFrame"] || window[e + "CancelRequestAnimationFrame"])
    });
    var u = window.MutationObserver || window.WebKitMutationObserver || !1, d = {
        zindex: "auto",
        cursoropacitymin: 0,
        cursoropacitymax: 1,
        cursorcolor: "#424242",
        cursorwidth: "5px",
        cursorborder: "1px solid #fff",
        cursorborderradius: "5px",
        scrollspeed: 60,
        mousescrollstep: 24,
        touchbehavior: !1,
        hwacceleration: !0,
        usetransition: !0,
        boxzoom: !1,
        dblclickzoom: !0,
        gesturezoom: !0,
        grabcursorenabled: !0,
        autohidemode: !0,
        background: "",
        iframeautoresize: !0,
        cursorminheight: 32,
        preservenativescrolling: !0,
        railoffset: !1,
        bouncescroll: !0,
        spacebarenabled: !0,
        railpadding: {top: 0, right: 0, left: 0, bottom: 0},
        disableoutline: !0,
        horizrailenabled: !0,
        railalign: "right",
        railvalign: "bottom",
        enabletranslate3d: !0,
        enablemousewheel: !0,
        enablekeyboard: !0,
        smoothscroll: !0,
        sensitiverail: !0,
        enablemouselockapi: !0,
        cursorfixedheight: !1,
        directionlockdeadzone: 6,
        hidecursordelay: 400,
        nativeparentscrolling: !0,
        enablescrollonselection: !0,
        overflowx: !0,
        overflowy: !0,
        cursordragspeed: .3,
        rtlmode: !1,
        cursordragontouch: !1
    }, h = !1, p = function () {
        if (h) return h;
        var e = document.createElement("DIV"),
            t = {haspointerlock: "pointerLockElement" in document || "mozPointerLockElement" in document || "webkitPointerLockElement" in document};
        t.isopera = "opera" in window, t.isopera12 = t.isopera && "getUserMedia" in navigator, t.isie = "all" in document && "attachEvent" in e && !t.isopera, t.isieold = t.isie && !("msInterpolationMode" in e.style), t.isie7 = t.isie && !t.isieold && (!("documentMode" in document) || 7 == document.documentMode), t.isie8 = t.isie && "documentMode" in document && 8 == document.documentMode, t.isie9 = t.isie && "performance" in window && 9 <= document.documentMode, t.isie10 = t.isie && "performance" in window && 10 <= document.documentMode, t.isie9mobile = /iemobile.9/i.test(navigator.userAgent), t.isie9mobile && (t.isie9 = !1), t.isie7mobile = !t.isie9mobile && t.isie7 && /iemobile/i.test(navigator.userAgent), t.ismozilla = "MozAppearance" in e.style, t.iswebkit = "WebkitAppearance" in e.style, t.ischrome = "chrome" in window, t.ischrome22 = t.ischrome && t.haspointerlock, t.ischrome26 = t.ischrome && "transition" in e.style, t.cantouch = "ontouchstart" in document.documentElement || "ontouchstart" in window, t.hasmstouch = window.navigator.msPointerEnabled || !1, t.ismac = /^mac$/i.test(navigator.platform), t.isios = t.cantouch && /iphone|ipad|ipod/i.test(navigator.platform), t.isios4 = t.isios && !("seal" in Object), t.isandroid = /android/i.test(navigator.userAgent), t.trstyle = !1, t.hastransform = !1, t.hastranslate3d = !1, t.transitionstyle = !1, t.hastransition = !1, t.transitionend = !1;
        for (var n = ["transform", "msTransform", "webkitTransform", "MozTransform", "OTransform"], o = 0; o < n.length; o++) if ("undefined" != typeof e.style[n[o]]) {
            t.trstyle = n[o];
            break
        }
        t.hastransform = 0 != t.trstyle, t.hastransform && (e.style[t.trstyle] = "translate3d(1px,2px,3px)", t.hastranslate3d = /translate3d/.test(e.style[t.trstyle])), t.transitionstyle = !1, t.prefixstyle = "", t.transitionend = !1;
        for (var n = "transition webkitTransition MozTransition OTransition OTransition msTransition KhtmlTransition".split(" "), i = " -webkit- -moz- -o- -o -ms- -khtml-".split(" "), r = "transitionend webkitTransitionEnd transitionend otransitionend oTransitionEnd msTransitionEnd KhtmlTransitionEnd".split(" "), o = 0; o < n.length; o++) if (n[o] in e.style) {
            t.transitionstyle = n[o], t.prefixstyle = i[o], t.transitionend = r[o];
            break
        }
        t.ischrome26 && (t.prefixstyle = i[1]), t.hastransition = t.transitionstyle;
        e:{
            for (n = ["-moz-grab", "-webkit-grab", "grab"], (t.ischrome && !t.ischrome22 || t.isie) && (n = []), o = 0; o < n.length; o++) if (i = n[o], e.style.cursor = i, e.style.cursor == i) {
                n = i;
                break e
            }
            n = "url(http://www.google.com/intl/en_ALL/mapfiles/openhand.cur),n-resize"
        }
        return t.cursorgrabvalue = n, t.hasmousecapture = "setCapture" in e, t.hasMutationObserver = !1 !== u, h = t
    }, f = function (a, h) {
        function f() {
            var e = b.win;
            if ("zIndex" in e) return e.zIndex();
            for (; 0 < e.length && 9 != e[0].nodeType;) {
                var t = e.css("zIndex");
                if (!isNaN(t) && 0 != t) return parseInt(t);
                e = e.parent()
            }
            return !1
        }

        function g(e, t, n) {
            return t = e.css(t), e = parseFloat(t), isNaN(e) ? (e = S[t] || 0, n = 3 == e ? n ? b.win.outerHeight() - b.win.innerHeight() : b.win.outerWidth() - b.win.innerWidth() : 1, b.isie8 && e && (e += 1), n ? e : 0) : e
        }

        function v(e, t, n, o) {
            b._bind(e, t, function (o) {
                o = o ? o : window.event;
                var i = {
                    original: o,
                    target: o.target || o.srcElement,
                    type: "wheel",
                    deltaMode: "MozMousePixelScroll" == o.type ? 0 : 1,
                    deltaX: 0,
                    deltaZ: 0,
                    preventDefault: function () {
                        return o.preventDefault ? o.preventDefault() : o.returnValue = !1, !1
                    },
                    stopImmediatePropagation: function () {
                        o.stopImmediatePropagation ? o.stopImmediatePropagation() : o.cancelBubble = !0
                    }
                };
                return "mousewheel" == t ? (i.deltaY = -.025 * o.wheelDelta, o.wheelDeltaX && (i.deltaX = -.025 * o.wheelDeltaX)) : i.deltaY = o.detail, n.call(e, i)
            }, o)
        }

        function y(e, t, n) {
            var o, i;
            if (0 == e.deltaMode ? (o = -Math.floor(e.deltaX * (b.opt.mousescrollstep / 54)), i = -Math.floor(e.deltaY * (b.opt.mousescrollstep / 54))) : 1 == e.deltaMode && (o = -Math.floor(e.deltaX * b.opt.mousescrollstep), i = -Math.floor(e.deltaY * b.opt.mousescrollstep)), t && 0 == o && i && (o = i, i = 0), o && (b.scrollmom && b.scrollmom.stop(), b.lastdeltax += o, b.debounced("mousewheelx", function () {
                var e = b.lastdeltax;
                b.lastdeltax = 0, b.rail.drag || b.doScrollLeftBy(e)
            }, 120)), i) {
                if (b.opt.nativeparentscrolling && n && !b.ispage && !b.zoomactive) if (0 > i) {
                    if (b.getScrollTop() >= b.page.maxh) return !0
                } else if (0 >= b.getScrollTop()) return !0;
                b.scrollmom && b.scrollmom.stop(),
                    b.lastdeltay += i, b.debounced("mousewheely", function () {
                    var e = b.lastdeltay;
                    b.lastdeltay = 0, b.rail.drag || b.doScrollBy(e)
                }, 120)
            }
            return e.stopImmediatePropagation(), e.preventDefault()
        }

        var b = this;
        if (this.version = "3.4.0", this.name = "nicescroll", this.me = h, this.opt = {
            doc: e("body"),
            win: !1
        }, e.extend(this.opt, d), this.opt.snapbackspeed = 80, a) for (var w in b.opt) "undefined" != typeof a[w] && (b.opt[w] = a[w]);
        this.iddoc = (this.doc = b.opt.doc) && this.doc[0] ? this.doc[0].id || "" : "", this.ispage = /BODY|HTML/.test(b.opt.win ? b.opt.win[0].nodeName : this.doc[0].nodeName), this.haswrapper = !1 !== b.opt.win, this.win = b.opt.win || (this.ispage ? e(window) : this.doc), this.docscroll = this.ispage && !this.haswrapper ? e(window) : this.win, this.body = e("body"), this.iframe = this.isfixed = this.viewport = !1, this.isiframe = "IFRAME" == this.doc[0].nodeName && "IFRAME" == this.win[0].nodeName, this.istextarea = "TEXTAREA" == this.win[0].nodeName, this.forcescreen = !1, this.canshowonmouseevent = "scroll" != b.opt.autohidemode, this.page = this.view = this.onzoomout = this.onzoomin = this.onscrollcancel = this.onscrollend = this.onscrollstart = this.onclick = this.ongesturezoom = this.onkeypress = this.onmousewheel = this.onmousemove = this.onmouseup = this.onmousedown = !1, this.scroll = {
            x: 0,
            y: 0
        }, this.scrollratio = {
            x: 0,
            y: 0
        }, this.cursorheight = 20, this.scrollvaluemax = 0, this.observerremover = this.observer = this.scrollmom = this.scrollrunning = this.checkrtlmode = !1;
        do this.id = "ascrail" + i++; while (document.getElementById(this.id));
        this.hasmousefocus = this.hasfocus = this.zoomactive = this.zoom = this.selectiondrag = this.cursorfreezed = this.cursor = this.rail = !1, this.visibility = !0, this.hidden = this.locked = !1, this.cursoractive = !0, this.overflowx = b.opt.overflowx, this.overflowy = b.opt.overflowy, this.nativescrollingarea = !1, this.checkarea = 0, this.events = [], this.saved = {}, this.delaylist = {}, this.synclist = {}, this.lastdeltay = this.lastdeltax = 0, this.detected = p();
        var x = e.extend({}, this.detected);
        if (this.ishwscroll = (this.canhwscroll = x.hastransform && b.opt.hwacceleration) && b.haswrapper, this.istouchcapable = !1, x.cantouch && x.ischrome && !x.isios && !x.isandroid && (this.istouchcapable = !0, x.cantouch = !1), x.cantouch && x.ismozilla && !x.isios && (this.istouchcapable = !0, x.cantouch = !1), b.opt.enablemouselockapi || (x.hasmousecapture = !1, x.haspointerlock = !1), this.delayed = function (e, t, n, o) {
            var i = b.delaylist[e], r = (new Date).getTime();
            return !(!o && i && i.tt) && (i && i.tt && clearTimeout(i.tt), void(i && i.last + n > r && !i.tt ? b.delaylist[e] = {
                last: r + n,
                tt: setTimeout(function () {
                    b.delaylist[e].tt = 0, t.call()
                }, n)
            } : i && i.tt || (b.delaylist[e] = {last: r, tt: 0}, setTimeout(function () {
                t.call()
            }, 0))))
        }, this.debounced = function (e, t, n) {
            var o = b.delaylist[e];
            (new Date).getTime(), b.delaylist[e] = t, o || setTimeout(function () {
                var t = b.delaylist[e];
                b.delaylist[e] = !1, t.call()
            }, n)
        }, this.synched = function (e, t) {
            return b.synclist[e] = t, function () {
                b.onsync || (l(function () {
                    b.onsync = !1;
                    for (e in b.synclist) {
                        var t = b.synclist[e];
                        t && t.call(b), b.synclist[e] = !1
                    }
                }), b.onsync = !0)
            }(), e
        }, this.unsynched = function (e) {
            b.synclist[e] && (b.synclist[e] = !1)
        }, this.css = function (e, t) {
            for (var n in t) b.saved.css.push([e, n, e.css(n)]), e.css(n, t[n])
        }, this.scrollTop = function (e) {
            return "undefined" == typeof e ? b.getScrollTop() : b.setScrollTop(e)
        }, this.scrollLeft = function (e) {
            return "undefined" == typeof e ? b.getScrollLeft() : b.setScrollLeft(e)
        }, BezierClass = function (e, t, n, o, i, r, s) {
            this.st = e, this.ed = t, this.spd = n, this.p1 = o || 0, this.p2 = i || 1, this.p3 = r || 0, this.p4 = s || 1, this.ts = (new Date).getTime(), this.df = this.ed - this.st
        }, BezierClass.prototype = {
            B2: function (e) {
                return 3 * e * e * (1 - e)
            }, B3: function (e) {
                return 3 * e * (1 - e) * (1 - e)
            }, B4: function (e) {
                return (1 - e) * (1 - e) * (1 - e)
            }, getNow: function () {
                var e = 1 - ((new Date).getTime() - this.ts) / this.spd, t = this.B2(e) + this.B3(e) + this.B4(e);
                return 0 > e ? this.ed : this.st + Math.round(this.df * t)
            }, update: function (e, t) {
                return this.st = this.getNow(), this.ed = e, this.spd = t, this.ts = (new Date).getTime(), this.df = this.ed - this.st, this
            }
        }, this.ishwscroll) {
            this.doc.translate = {
                x: 0,
                y: 0,
                tx: "0px",
                ty: "0px"
            }, x.hastranslate3d && x.isios && this.doc.css("-webkit-backface-visibility", "hidden");
            var T = function () {
                var e = b.doc.css(x.trstyle);
                return !(!e || "matrix" != e.substr(0, 6)) && e.replace(/^.*\((.*)\)$/g, "$1").replace(/px/g, "").split(/, +/)
            };
            this.getScrollTop = function (e) {
                if (!e) {
                    if (e = T()) return 16 == e.length ? -e[13] : -e[5];
                    if (b.timerscroll && b.timerscroll.bz) return b.timerscroll.bz.getNow()
                }
                return b.doc.translate.y
            }, this.getScrollLeft = function (e) {
                if (!e) {
                    if (e = T()) return 16 == e.length ? -e[12] : -e[4];
                    if (b.timerscroll && b.timerscroll.bh) return b.timerscroll.bh.getNow()
                }
                return b.doc.translate.x
            }, this.notifyScrollEvent = document.createEvent ? function (e) {
                var t = document.createEvent("UIEvents");
                t.initUIEvent("scroll", !1, !0, window, 1), e.dispatchEvent(t)
            } : document.fireEvent ? function (e) {
                var t = document.createEventObject();
                e.fireEvent("onscroll"), t.cancelBubble = !0
            } : function (e, t) {
            }, x.hastranslate3d && b.opt.enabletranslate3d ? (this.setScrollTop = function (e, t) {
                b.doc.translate.y = e, b.doc.translate.ty = -1 * e + "px", b.doc.css(x.trstyle, "translate3d(" + b.doc.translate.tx + "," + b.doc.translate.ty + ",0px)"), t || b.notifyScrollEvent(b.win[0])
            }, this.setScrollLeft = function (e, t) {
                b.doc.translate.x = e, b.doc.translate.tx = -1 * e + "px", b.doc.css(x.trstyle, "translate3d(" + b.doc.translate.tx + "," + b.doc.translate.ty + ",0px)"), t || b.notifyScrollEvent(b.win[0])
            }) : (this.setScrollTop = function (e, t) {
                b.doc.translate.y = e, b.doc.translate.ty = -1 * e + "px", b.doc.css(x.trstyle, "translate(" + b.doc.translate.tx + "," + b.doc.translate.ty + ")"), t || b.notifyScrollEvent(b.win[0])
            }, this.setScrollLeft = function (e, t) {
                b.doc.translate.x = e, b.doc.translate.tx = -1 * e + "px", b.doc.css(x.trstyle, "translate(" + b.doc.translate.tx + "," + b.doc.translate.ty + ")"), t || b.notifyScrollEvent(b.win[0])
            })
        } else this.getScrollTop = function () {
            return b.docscroll.scrollTop()
        }, this.setScrollTop = function (e) {
            return b.docscroll.scrollTop(e)
        }, this.getScrollLeft = function () {
            return b.docscroll.scrollLeft()
        }, this.setScrollLeft = function (e) {
            return b.docscroll.scrollLeft(e)
        };
        this.getTarget = function (e) {
            return !!e && (e.target ? e.target : !!e.srcElement && e.srcElement)
        }, this.hasParent = function (e, t) {
            if (!e) return !1;
            for (var n = e.target || e.srcElement || e || !1; n && n.id != t;) n = n.parentNode || !1;
            return !1 !== n
        };
        var S = {thin: 1, medium: 3, thick: 5};
        this.getOffset = function () {
            if (b.isfixed) return {top: parseFloat(b.win.css("top")), left: parseFloat(b.win.css("left"))};
            if (!b.viewport) return b.win.offset();
            var e = b.win.offset(), t = b.viewport.offset();
            return {top: e.top - t.top + b.viewport.scrollTop(), left: e.left - t.left + b.viewport.scrollLeft()}
        }, this.updateScrollBar = function (e) {
            if (b.ishwscroll) b.rail.css({height: b.win.innerHeight()}), b.railh && b.railh.css({width: b.win.innerWidth()}); else {
                var t = b.getOffset(), n = t.top, o = t.left, n = n + g(b.win, "border-top-width", !0);
                b.win.outerWidth(), b.win.innerWidth();
                var o = o + (b.rail.align ? b.win.outerWidth() - g(b.win, "border-right-width") - b.rail.width : g(b.win, "border-left-width")),
                    i = b.opt.railoffset;
                i && (i.top && (n += i.top), b.rail.align && i.left && (o += i.left)), b.locked || b.rail.css({
                    top: n,
                    left: o,
                    height: e ? e.h : b.win.innerHeight()
                }), b.zoom && b.zoom.css({
                    top: n + 1,
                    left: 1 == b.rail.align ? o - 20 : o + b.rail.width + 4
                }), b.railh && !b.locked && (n = t.top, o = t.left, e = b.railh.align ? n + g(b.win, "border-top-width", !0) + b.win.innerHeight() - b.railh.height : n + g(b.win, "border-top-width", !0), o += g(b.win, "border-left-width"), b.railh.css({
                    top: e,
                    left: o,
                    width: b.railh.width
                }))
            }
        }, this.doRailClick = function (e, t, n) {
            var o;
            b.locked || (b.cancelEvent(e), t ? (t = n ? b.doScrollLeft : b.doScrollTop, o = n ? (e.pageX - b.railh.offset().left - b.cursorwidth / 2) * b.scrollratio.x : (e.pageY - b.rail.offset().top - b.cursorheight / 2) * b.scrollratio.y, t(o)) : (t = n ? b.doScrollLeftBy : b.doScrollBy, o = n ? b.scroll.x : b.scroll.y, e = n ? e.pageX - b.railh.offset().left : e.pageY - b.rail.offset().top, n = n ? b.view.w : b.view.h, t(o >= e ? n : -n)))
        }, b.hasanimationframe = l, b.hascancelanimationframe = c, b.hasanimationframe ? b.hascancelanimationframe || (c = function () {
            b.cancelAnimationFrame = !0
        }) : (l = function (e) {
            return setTimeout(e, 15 - Math.floor(+new Date / 1e3) % 16)
        }, c = clearInterval), this.init = function () {
            if (b.saved.css = [], x.isie7mobile) return !0;
            if (x.hasmstouch && b.css(b.ispage ? e("html") : b.win, {"-ms-touch-action": "none"}), b.zindex = "auto", b.zindex = b.ispage || "auto" != b.opt.zindex ? b.opt.zindex : f() || "auto", !b.ispage && "auto" != b.zindex && b.zindex > r && (r = b.zindex), b.isie && 0 == b.zindex && "auto" == b.opt.zindex && (b.zindex = "auto"), !b.ispage || !x.cantouch && !x.isieold && !x.isie9mobile) {
                var i = b.docscroll;
                b.ispage && (i = b.haswrapper ? b.win : b.doc), x.isie9mobile || b.css(i, {"overflow-y": "hidden"}), b.ispage && x.isie7 && ("BODY" == b.doc[0].nodeName ? b.css(e("html"), {"overflow-y": "hidden"}) : "HTML" == b.doc[0].nodeName && b.css(e("body"), {"overflow-y": "hidden"})), x.isios && !b.ispage && !b.haswrapper && b.css(e("body"), {"-webkit-overflow-scrolling": "touch"});
                var a = e(document.createElement("div"));
                a.css({
                    position: "relative",
                    top: 0,
                    "float": "right",
                    width: b.opt.cursorwidth,
                    height: "0px",
                    "background-color": b.opt.cursorcolor,
                    border: b.opt.cursorborder,
                    "background-clip": "padding-box",
                    "-webkit-border-radius": b.opt.cursorborderradius,
                    "-moz-border-radius": b.opt.cursorborderradius,
                    "border-radius": b.opt.cursorborderradius
                }), a.hborder = parseFloat(a.outerHeight() - a.innerHeight()), b.cursor = a;
                var l = e(document.createElement("div"));
                l.attr("id", b.id), l.addClass("nicescroll-rails");
                var c, d, h, p = ["left", "right"];
                for (h in p) d = p[h], (c = b.opt.railpadding[d]) ? l.css("padding-" + d, c + "px") : b.opt.railpadding[d] = 0;
                if (l.append(a), l.width = Math.max(parseFloat(b.opt.cursorwidth), a.outerWidth()) + b.opt.railpadding.left + b.opt.railpadding.right, l.css({
                    width: l.width + "px",
                    zIndex: b.zindex,
                    background: b.opt.background,
                    cursor: "default"
                }), l.visibility = !0, l.scrollable = !0, l.align = "left" == b.opt.railalign ? 0 : 1, b.rail = l, a = b.rail.drag = !1, b.opt.boxzoom && !b.ispage && !x.isieold && (a = document.createElement("div"), b.bind(a, "click", b.doZoom), b.zoom = e(a), b.zoom.css({
                    cursor: "pointer",
                    "z-index": b.zindex,
                    backgroundImage: "url(" + s + "zoomico.png)",
                    height: 18,
                    width: 18,
                    backgroundPosition: "0px 0px"
                }), b.opt.dblclickzoom && b.bind(b.win, "dblclick", b.doZoom), x.cantouch && b.opt.gesturezoom && (b.ongesturezoom = function (e) {
                    return 1.5 < e.scale && b.doZoomIn(e), .8 > e.scale && b.doZoomOut(e), b.cancelEvent(e)
                }, b.bind(b.win, "gestureend", b.ongesturezoom))), b.railh = !1, b.opt.horizrailenabled) {
                    b.css(i, {"overflow-x": "hidden"}), a = e(document.createElement("div")), a.css({
                        position: "relative",
                        top: 0,
                        height: b.opt.cursorwidth,
                        width: "0px",
                        "background-color": b.opt.cursorcolor,
                        border: b.opt.cursorborder,
                        "background-clip": "padding-box",
                        "-webkit-border-radius": b.opt.cursorborderradius,
                        "-moz-border-radius": b.opt.cursorborderradius,
                        "border-radius": b.opt.cursorborderradius
                    }), a.wborder = parseFloat(a.outerWidth() - a.innerWidth()), b.cursorh = a;
                    var g = e(document.createElement("div"));
                    g.attr("id", b.id + "-hr"), g.addClass("nicescroll-rails"), g.height = Math.max(parseFloat(b.opt.cursorwidth), a.outerHeight()), g.css({
                        height: g.height + "px",
                        zIndex: b.zindex,
                        background: b.opt.background
                    }), g.append(a), g.visibility = !0, g.scrollable = !0, g.align = "top" == b.opt.railvalign ? 0 : 1, b.railh = g, b.railh.drag = !1
                }
                if (b.ispage ? (l.css({
                    position: "fixed",
                    top: "0px",
                    height: "100%"
                }), l.align ? l.css({right: "0px"}) : l.css({left: "0px"}), b.body.append(l), b.railh && (g.css({
                    position: "fixed",
                    left: "0px",
                    width: "100%"
                }), g.align ? g.css({bottom: "0px"}) : g.css({top: "0px"}), b.body.append(g))) : (b.ishwscroll ? ("static" == b.win.css("position") && b.css(b.win, {position: "relative"}), i = "HTML" == b.win[0].nodeName ? b.body : b.win, b.zoom && (b.zoom.css({
                    position: "absolute",
                    top: 1,
                    right: 0,
                    "margin-right": l.width + 4
                }), i.append(b.zoom)), l.css({
                    position: "absolute",
                    top: 0
                }), l.align ? l.css({right: 0}) : l.css({left: 0}), i.append(l), g && (g.css({
                    position: "absolute",
                    left: 0,
                    bottom: 0
                }), g.align ? g.css({bottom: 0}) : g.css({top: 0}), i.append(g))) : (b.isfixed = "fixed" == b.win.css("position"), i = b.isfixed ? "fixed" : "absolute", b.isfixed || (b.viewport = b.getViewport(b.win[0])), b.viewport && (b.body = b.viewport, 0 == /relative|absolute/.test(b.viewport.css("position")) && b.css(b.viewport, {position: "relative"})), l.css({position: i}), b.zoom && b.zoom.css({position: i}), b.updateScrollBar(), b.body.append(l), b.zoom && b.body.append(b.zoom), b.railh && (g.css({position: i}), b.body.append(g))), x.isios && b.css(b.win, {
                    "-webkit-tap-highlight-color": "rgba(0,0,0,0)",
                    "-webkit-touch-callout": "none"
                }), x.isie && b.opt.disableoutline && b.win.attr("hideFocus", "true"), x.iswebkit && b.opt.disableoutline && b.win.css({outline: "none"})), !1 === b.opt.autohidemode ? (b.autohidedom = !1, b.rail.css({opacity: b.opt.cursoropacitymax}), b.railh && b.railh.css({opacity: b.opt.cursoropacitymax})) : !0 === b.opt.autohidemode ? (b.autohidedom = e().add(b.rail), x.isie8 && (b.autohidedom = b.autohidedom.add(b.cursor)), b.railh && (b.autohidedom = b.autohidedom.add(b.railh)), b.railh && x.isie8 && (b.autohidedom = b.autohidedom.add(b.cursorh))) : "scroll" == b.opt.autohidemode ? (b.autohidedom = e().add(b.rail), b.railh && (b.autohidedom = b.autohidedom.add(b.railh))) : "cursor" == b.opt.autohidemode ? (b.autohidedom = e().add(b.cursor), b.railh && (b.autohidedom = b.autohidedom.add(b.cursorh))) : "hidden" == b.opt.autohidemode && (b.autohidedom = !1, b.hide(), b.locked = !1), x.isie9mobile) b.scrollmom = new m(b), b.onmangotouch = function (e) {
                    e = b.getScrollTop();
                    var t = b.getScrollLeft();
                    if (e == b.scrollmom.lastscrolly && t == b.scrollmom.lastscrollx) return !0;
                    var n = e - b.mangotouch.sy, o = t - b.mangotouch.sx;
                    if (0 != Math.round(Math.sqrt(Math.pow(o, 2) + Math.pow(n, 2)))) {
                        var i = 0 > n ? -1 : 1, r = 0 > o ? -1 : 1, s = +new Date;
                        b.mangotouch.lazy && clearTimeout(b.mangotouch.lazy), 80 < s - b.mangotouch.tm || b.mangotouch.dry != i || b.mangotouch.drx != r ? (b.scrollmom.stop(), b.scrollmom.reset(t, e), b.mangotouch.sy = e, b.mangotouch.ly = e, b.mangotouch.sx = t, b.mangotouch.lx = t, b.mangotouch.dry = i, b.mangotouch.drx = r, b.mangotouch.tm = s) : (b.scrollmom.stop(), b.scrollmom.update(b.mangotouch.sx - o, b.mangotouch.sy - n), b.mangotouch.tm = s, n = Math.max(Math.abs(b.mangotouch.ly - e), Math.abs(b.mangotouch.lx - t)), b.mangotouch.ly = e, b.mangotouch.lx = t, 2 < n && (b.mangotouch.lazy = setTimeout(function () {
                            b.mangotouch.lazy = !1, b.mangotouch.dry = 0, b.mangotouch.drx = 0, b.mangotouch.tm = 0, b.scrollmom.doMomentum(30)
                        }, 100)))
                    }
                }, l = b.getScrollTop(), g = b.getScrollLeft(), b.mangotouch = {
                    sy: l,
                    ly: l,
                    dry: 0,
                    sx: g,
                    lx: g,
                    drx: 0,
                    lazy: !1,
                    tm: 0
                }, b.bind(b.docscroll, "scroll", b.onmangotouch); else {
                    if (x.cantouch || b.istouchcapable || b.opt.touchbehavior || x.hasmstouch) {
                        b.scrollmom = new m(b), b.ontouchstart = function (t) {
                            if (t.pointerType && 2 != t.pointerType) return !1;
                            if (!b.locked) {
                                if (x.hasmstouch) for (var n = !!t.target && t.target; n;) {
                                    var o = e(n).getNiceScroll();
                                    if (0 < o.length && o[0].me == b.me) break;
                                    if (0 < o.length) return !1;
                                    if ("DIV" == n.nodeName && n.id == b.id) break;
                                    n = !!n.parentNode && n.parentNode
                                }
                                if (b.cancelScroll(), (n = b.getTarget(t)) && /INPUT/i.test(n.nodeName) && /range/i.test(n.type)) return b.stopPropagation(t);
                                if (!("clientX" in t) && "changedTouches" in t && (t.clientX = t.changedTouches[0].clientX, t.clientY = t.changedTouches[0].clientY), b.forcescreen && (o = t, t = {original: t.original ? t.original : t}, t.clientX = o.screenX, t.clientY = o.screenY), b.rail.drag = {
                                    x: t.clientX,
                                    y: t.clientY,
                                    sx: b.scroll.x,
                                    sy: b.scroll.y,
                                    st: b.getScrollTop(),
                                    sl: b.getScrollLeft(),
                                    pt: 2,
                                    dl: !1
                                }, b.ispage || !b.opt.directionlockdeadzone) b.rail.drag.dl = "f"; else {
                                    var o = e(window).width(), i = e(window).height(),
                                        r = Math.max(document.body.scrollWidth, document.documentElement.scrollWidth),
                                        s = Math.max(document.body.scrollHeight, document.documentElement.scrollHeight),
                                        i = Math.max(0, s - i), o = Math.max(0, r - o);
                                    b.rail.drag.ck = !b.rail.scrollable && b.railh.scrollable ? 0 < i && "v" : !(!b.rail.scrollable || b.railh.scrollable) && (0 < o && "h"), b.rail.drag.ck || (b.rail.drag.dl = "f")
                                }
                                if (b.opt.touchbehavior && b.isiframe && x.isie && (o = b.win.position(), b.rail.drag.x += o.left, b.rail.drag.y += o.top), b.hasmoving = !1, b.lastmouseup = !1, b.scrollmom.reset(t.clientX, t.clientY), !x.cantouch && !this.istouchcapable && !x.hasmstouch) {
                                    if (!n || !/INPUT|SELECT|TEXTAREA/i.test(n.nodeName)) return !b.ispage && x.hasmousecapture && n.setCapture(), b.cancelEvent(t);
                                    /SUBMIT|CANCEL|BUTTON/i.test(e(n).attr("type")) && (pc = {
                                        tg: n,
                                        click: !1
                                    }, b.preventclick = pc)
                                }
                            }
                        }, b.ontouchend = function (e) {
                            return (!e.pointerType || 2 == e.pointerType) && (b.rail.drag && 2 == b.rail.drag.pt && (b.scrollmom.doMomentum(), b.rail.drag = !1, b.hasmoving && (b.hasmoving = !1, b.lastmouseup = !0, b.hideCursor(), x.hasmousecapture && document.releaseCapture(), !x.cantouch)) ? b.cancelEvent(e) : void 0)
                        };
                        var v = b.opt.touchbehavior && b.isiframe && !x.hasmousecapture;
                        b.ontouchmove = function (t, n) {
                            if (t.pointerType && 2 != t.pointerType) return !1;
                            if (b.rail.drag && 2 == b.rail.drag.pt) {
                                if (x.cantouch && "undefined" == typeof t.original) return !0;
                                if (b.hasmoving = !0, b.preventclick && !b.preventclick.click && (b.preventclick.click = b.preventclick.tg.onclick || !1, b.preventclick.tg.onclick = b.onpreventclick), t = e.extend({original: t}, t), "changedTouches" in t && (t.clientX = t.changedTouches[0].clientX, t.clientY = t.changedTouches[0].clientY), b.forcescreen) {
                                    var o = t;
                                    t = {original: t.original ? t.original : t}, t.clientX = o.screenX, t.clientY = o.screenY
                                }
                                if (o = ofy = 0, v && !n) {
                                    var i = b.win.position(), o = -i.left;
                                    ofy = -i.top
                                }
                                var r = t.clientY + ofy, i = r - b.rail.drag.y, s = t.clientX + o,
                                    a = s - b.rail.drag.x, l = b.rail.drag.st - i;
                                if (b.ishwscroll && b.opt.bouncescroll ? 0 > l ? l = Math.round(l / 2) : l > b.page.maxh && (l = b.page.maxh + Math.round((l - b.page.maxh) / 2)) : (0 > l && (r = l = 0), l > b.page.maxh && (l = b.page.maxh, r = 0)), b.railh && b.railh.scrollable) {
                                    var c = b.rail.drag.sl - a;
                                    b.ishwscroll && b.opt.bouncescroll ? 0 > c ? c = Math.round(c / 2) : c > b.page.maxw && (c = b.page.maxw + Math.round((c - b.page.maxw) / 2)) : (0 > c && (s = c = 0), c > b.page.maxw && (c = b.page.maxw, s = 0))
                                }
                                if (o = !1, b.rail.drag.dl) o = !0, "v" == b.rail.drag.dl ? c = b.rail.drag.sl : "h" == b.rail.drag.dl && (l = b.rail.drag.st); else {
                                    var i = Math.abs(i), a = Math.abs(a), u = b.opt.directionlockdeadzone;
                                    if ("v" == b.rail.drag.ck) {
                                        if (i > u && a <= .3 * i) return b.rail.drag = !1, !0;
                                        a > u && (b.rail.drag.dl = "f", e("body").scrollTop(e("body").scrollTop()))
                                    } else if ("h" == b.rail.drag.ck) {
                                        if (a > u && i <= .3 * az) return b.rail.drag = !1, !0;
                                        i > u && (b.rail.drag.dl = "f", e("body").scrollLeft(e("body").scrollLeft()))
                                    }
                                }
                                if (b.synched("touchmove", function () {
                                    b.rail.drag && 2 == b.rail.drag.pt && (b.prepareTransition && b.prepareTransition(0), b.rail.scrollable && b.setScrollTop(l), b.scrollmom.update(s, r), b.railh && b.railh.scrollable ? (b.setScrollLeft(c), b.showCursor(l, c)) : b.showCursor(l), x.isie10 && document.selection.clear())
                                }), x.ischrome && b.istouchcapable && (o = !1), o) return b.cancelEvent(t)
                            }
                        }
                    }
                    if (b.onmousedown = function (e, t) {
                        if (!b.rail.drag || 1 == b.rail.drag.pt) {
                            if (b.locked) return b.cancelEvent(e);
                            b.cancelScroll(), b.rail.drag = {
                                x: e.clientX,
                                y: e.clientY,
                                sx: b.scroll.x,
                                sy: b.scroll.y,
                                pt: 1,
                                hr: !!t
                            };
                            var n = b.getTarget(e);
                            return !b.ispage && x.hasmousecapture && n.setCapture(), b.isiframe && !x.hasmousecapture && (b.saved.csspointerevents = b.doc.css("pointer-events"), b.css(b.doc, {"pointer-events": "none"})), b.cancelEvent(e)
                        }
                    }, b.onmouseup = function (e) {
                        if (b.rail.drag && (x.hasmousecapture && document.releaseCapture(), b.isiframe && !x.hasmousecapture && b.doc.css("pointer-events", b.saved.csspointerevents), 1 == b.rail.drag.pt)) return b.rail.drag = !1, b.cancelEvent(e)
                    }, b.onmousemove = function (e) {
                        if (b.rail.drag && 1 == b.rail.drag.pt) {
                            if (x.ischrome && 0 == e.which) return b.onmouseup(e);
                            if (b.cursorfreezed = !0, b.rail.drag.hr) {
                                b.scroll.x = b.rail.drag.sx + (e.clientX - b.rail.drag.x), 0 > b.scroll.x && (b.scroll.x = 0);
                                var t = b.scrollvaluemaxw;
                                b.scroll.x > t && (b.scroll.x = t)
                            } else b.scroll.y = b.rail.drag.sy + (e.clientY - b.rail.drag.y), 0 > b.scroll.y && (b.scroll.y = 0), t = b.scrollvaluemax, b.scroll.y > t && (b.scroll.y = t);
                            return b.synched("mousemove", function () {
                                b.rail.drag && 1 == b.rail.drag.pt && (b.showCursor(), b.rail.drag.hr ? b.doScrollLeft(Math.round(b.scroll.x * b.scrollratio.x), b.opt.cursordragspeed) : b.doScrollTop(Math.round(b.scroll.y * b.scrollratio.y), b.opt.cursordragspeed))
                            }), b.cancelEvent(e)
                        }
                    }, x.cantouch || b.opt.touchbehavior) b.onpreventclick = function (e) {
                        if (b.preventclick) return b.preventclick.tg.onclick = b.preventclick.click, b.preventclick = !1, b.cancelEvent(e)
                    }, b.bind(b.win, "mousedown", b.ontouchstart), b.onclick = !x.isios && function (e) {
                        return !b.lastmouseup || (b.lastmouseup = !1, b.cancelEvent(e))
                    }, b.opt.grabcursorenabled && x.cursorgrabvalue && (b.css(b.ispage ? b.doc : b.win, {cursor: x.cursorgrabvalue}), b.css(b.rail, {cursor: x.cursorgrabvalue})); else {
                        var y = function (e) {
                            if (b.selectiondrag) {
                                if (e) {
                                    var t = b.win.outerHeight();
                                    e = e.pageY - b.selectiondrag.top, 0 < e && e < t && (e = 0), e >= t && (e -= t), b.selectiondrag.df = e
                                }
                                0 != b.selectiondrag.df && (b.doScrollBy(2 * -Math.floor(b.selectiondrag.df / 6)), b.debounced("doselectionscroll", function () {
                                    y()
                                }, 50))
                            }
                        };
                        b.hasTextSelected = "getSelection" in document ? function () {
                            return 0 < document.getSelection().rangeCount
                        } : "selection" in document ? function () {
                            return "None" != document.selection.type
                        } : function () {
                            return !1
                        }, b.onselectionstart = function (e) {
                            b.ispage || (b.selectiondrag = b.win.offset())
                        }, b.onselectionend = function (e) {
                            b.selectiondrag = !1
                        }, b.onselectiondrag = function (e) {
                            b.selectiondrag && b.hasTextSelected() && b.debounced("selectionscroll", function () {
                                y(e)
                            }, 250)
                        }
                    }
                    x.hasmstouch && (b.css(b.rail, {"-ms-touch-action": "none"}), b.css(b.cursor, {"-ms-touch-action": "none"}), b.bind(b.win, "MSPointerDown", b.ontouchstart), b.bind(document, "MSPointerUp", b.ontouchend), b.bind(document, "MSPointerMove", b.ontouchmove), b.bind(b.cursor, "MSGestureHold", function (e) {
                        e.preventDefault()
                    }), b.bind(b.cursor, "contextmenu", function (e) {
                        e.preventDefault()
                    })), this.istouchcapable && (b.bind(b.win, "touchstart", b.ontouchstart), b.bind(document, "touchend", b.ontouchend), b.bind(document, "touchcancel", b.ontouchend), b.bind(document, "touchmove", b.ontouchmove)), b.bind(b.cursor, "mousedown", b.onmousedown), b.bind(b.cursor, "mouseup", b.onmouseup), b.railh && (b.bind(b.cursorh, "mousedown", function (e) {
                        b.onmousedown(e, !0)
                    }), b.bind(b.cursorh, "mouseup", function (e) {
                        if (!b.rail.drag || 2 != b.rail.drag.pt) return b.rail.drag = !1, b.hasmoving = !1, b.hideCursor(), x.hasmousecapture && document.releaseCapture(), b.cancelEvent(e)
                    })), (b.opt.cursordragontouch || !x.cantouch && !b.opt.touchbehavior) && (b.rail.css({cursor: "default"}), b.railh && b.railh.css({cursor: "default"}), b.jqbind(b.rail, "mouseenter", function () {
                        b.canshowonmouseevent && b.showCursor(), b.rail.active = !0
                    }), b.jqbind(b.rail, "mouseleave", function () {
                        b.rail.active = !1, b.rail.drag || b.hideCursor()
                    }), b.opt.sensitiverail && (b.bind(b.rail, "click", function (e) {
                        b.doRailClick(e, !1, !1)
                    }), b.bind(b.rail, "dblclick", function (e) {
                        b.doRailClick(e, !0, !1)
                    }), b.bind(b.cursor, "click", function (e) {
                        b.cancelEvent(e)
                    }), b.bind(b.cursor, "dblclick", function (e) {
                        b.cancelEvent(e)
                    })), b.railh && (b.jqbind(b.railh, "mouseenter", function () {
                        b.canshowonmouseevent && b.showCursor(), b.rail.active = !0
                    }), b.jqbind(b.railh, "mouseleave", function () {
                        b.rail.active = !1, b.rail.drag || b.hideCursor()
                    }), b.opt.sensitiverail && (b.bind(b.railh, "click", function (e) {
                        b.doRailClick(e, !1, !0)
                    }), b.bind(b.railh, "dblclick", function (e) {
                        b.doRailClick(e, !0, !0)
                    }), b.bind(b.cursorh, "click", function (e) {
                        b.cancelEvent(e)
                    }), b.bind(b.cursorh, "dblclick", function (e) {
                        b.cancelEvent(e)
                    })))), x.cantouch || b.opt.touchbehavior ? (b.bind(x.hasmousecapture ? b.win : document, "mouseup", b.ontouchend), b.bind(document, "mousemove", b.ontouchmove), b.onclick && b.bind(document, "click", b.onclick), b.opt.cursordragontouch && (b.bind(b.cursor, "mousedown", b.onmousedown), b.bind(b.cursor, "mousemove", b.onmousemove), b.cursorh && b.bind(b.cursorh, "mousedown", b.onmousedown), b.cursorh && b.bind(b.cursorh, "mousemove", b.onmousemove))) : (b.bind(x.hasmousecapture ? b.win : document, "mouseup", b.onmouseup), b.bind(document, "mousemove", b.onmousemove), b.onclick && b.bind(document, "click", b.onclick), !b.ispage && b.opt.enablescrollonselection && (b.bind(b.win[0], "mousedown", b.onselectionstart), b.bind(document, "mouseup", b.onselectionend), b.bind(b.cursor, "mouseup", b.onselectionend), b.cursorh && b.bind(b.cursorh, "mouseup", b.onselectionend), b.bind(document, "mousemove", b.onselectiondrag)), b.zoom && (b.jqbind(b.zoom, "mouseenter", function () {
                        b.canshowonmouseevent && b.showCursor(), b.rail.active = !0
                    }), b.jqbind(b.zoom, "mouseleave", function () {
                        b.rail.active = !1, b.rail.drag || b.hideCursor()
                    }))), b.opt.enablemousewheel && (b.isiframe || b.bind(x.isie && b.ispage ? document : b.docscroll, "mousewheel", b.onmousewheel), b.bind(b.rail, "mousewheel", b.onmousewheel), b.railh && b.bind(b.railh, "mousewheel", b.onmousewheelhr)), !b.ispage && !x.cantouch && !/HTML|BODY/.test(b.win[0].nodeName) && (b.win.attr("tabindex") || b.win.attr({tabindex: o++}), b.jqbind(b.win, "focus", function (e) {
                        t = b.getTarget(e).id || !0, b.hasfocus = !0, b.canshowonmouseevent && b.noticeCursor()
                    }), b.jqbind(b.win, "blur", function (e) {
                        t = !1, b.hasfocus = !1
                    }), b.jqbind(b.win, "mouseenter", function (e) {
                        n = b.getTarget(e).id || !0, b.hasmousefocus = !0, b.canshowonmouseevent && b.noticeCursor()
                    }), b.jqbind(b.win, "mouseleave", function () {
                        n = !1, b.hasmousefocus = !1
                    }))
                }
                if (b.onkeypress = function (e) {
                    if (b.locked && 0 == b.page.maxh) return !0;
                    e = e ? e : window.e;
                    var o = b.getTarget(e);
                    if (o && /INPUT|TEXTAREA|SELECT|OPTION/.test(o.nodeName) && (!o.getAttribute("type") && !o.type || !/submit|button|cancel/i.tp)) return !0;
                    if (b.hasfocus || b.hasmousefocus && !t || b.ispage && !t && !n) {
                        if (o = e.keyCode, b.locked && 27 != o) return b.cancelEvent(e);
                        var i = e.ctrlKey || !1, r = e.shiftKey || !1, s = !1;
                        switch (o) {
                            case 38:
                            case 63233:
                                b.doScrollBy(72), s = !0;
                                break;
                            case 40:
                            case 63235:
                                b.doScrollBy(-72), s = !0;
                                break;
                            case 37:
                            case 63232:
                                b.railh && (i ? b.doScrollLeft(0) : b.doScrollLeftBy(72), s = !0);
                                break;
                            case 39:
                            case 63234:
                                b.railh && (i ? b.doScrollLeft(b.page.maxw) : b.doScrollLeftBy(-72), s = !0);
                                break;
                            case 33:
                            case 63276:
                                b.doScrollBy(b.view.h), s = !0;
                                break;
                            case 34:
                            case 63277:
                                b.doScrollBy(-b.view.h), s = !0;
                                break;
                            case 36:
                            case 63273:
                                b.railh && i ? b.doScrollPos(0, 0) : b.doScrollTo(0), s = !0;
                                break;
                            case 35:
                            case 63275:
                                b.railh && i ? b.doScrollPos(b.page.maxw, b.page.maxh) : b.doScrollTo(b.page.maxh), s = !0;
                                break;
                            case 32:
                                b.opt.spacebarenabled && (r ? b.doScrollBy(b.view.h) : b.doScrollBy(-b.view.h), s = !0);
                                break;
                            case 27:
                                b.zoomactive && (b.doZoom(), s = !0)
                        }
                        if (s) return b.cancelEvent(e)
                    }
                }, b.opt.enablekeyboard && b.bind(document, x.isopera && !x.isopera12 ? "keypress" : "keydown", b.onkeypress), b.bind(window, "resize", b.lazyResize), b.bind(window, "orientationchange", b.lazyResize), b.bind(window, "load", b.lazyResize), x.ischrome && !b.ispage && !b.haswrapper) {
                    var w = b.win.attr("style"), l = parseFloat(b.win.css("width")) + 1;
                    b.win.css("width", l), b.synched("chromefix", function () {
                        b.win.attr("style", w)
                    })
                }
                b.onAttributeChange = function (e) {
                    b.lazyResize(250)
                }, !b.ispage && !b.haswrapper && (!1 !== u ? (b.observer = new u(function (e) {
                    e.forEach(b.onAttributeChange)
                }), b.observer.observe(b.win[0], {
                    childList: !0,
                    characterData: !1,
                    attributes: !0,
                    subtree: !1
                }), b.observerremover = new u(function (e) {
                    e.forEach(function (e) {
                        if (0 < e.removedNodes.length) for (var t in e.removedNodes) if (e.removedNodes[t] == b.win[0]) return b.remove()
                    })
                }), b.observerremover.observe(b.win[0].parentNode, {
                    childList: !0,
                    characterData: !1,
                    attributes: !1,
                    subtree: !1
                })) : (b.bind(b.win, x.isie && !x.isie9 ? "propertychange" : "DOMAttrModified", b.onAttributeChange), x.isie9 && b.win[0].attachEvent("onpropertychange", b.onAttributeChange), b.bind(b.win, "DOMNodeRemoved", function (e) {
                    e.target == b.win[0] && b.remove()
                }))), !b.ispage && b.opt.boxzoom && b.bind(window, "resize", b.resizeZoom), b.istextarea && b.bind(b.win, "mouseup", b.lazyResize), b.checkrtlmode = !0, b.lazyResize(30)
            }
            if ("IFRAME" == this.doc[0].nodeName) {
                var T = function (t) {
                    b.iframexd = !1;
                    try {
                        var n = "contentDocument" in this ? this.contentDocument : this.contentWindow.document
                    } catch (o) {
                        b.iframexd = !0, n = !1
                    }
                    return b.iframexd ? ("console" in window && void 0, !0) : (b.forcescreen = !0, b.isiframe && (b.iframe = {
                        doc: e(n),
                        html: b.doc.contents().find("html")[0],
                        body: b.doc.contents().find("body")[0]
                    }, b.getContentSize = function () {
                        return {
                            w: Math.max(b.iframe.html.scrollWidth, b.iframe.body.scrollWidth),
                            h: Math.max(b.iframe.html.scrollHeight, b.iframe.body.scrollHeight)
                        }
                    }, b.docscroll = e(b.iframe.body)), !x.isios && b.opt.iframeautoresize && !b.isiframe && (b.win.scrollTop(0), b.doc.height(""), t = Math.max(n.getElementsByTagName("html")[0].scrollHeight, n.body.scrollHeight), b.doc.height(t)), b.lazyResize(30), x.isie7 && b.css(e(b.iframe.html), {"overflow-y": "hidden"}), b.css(e(b.iframe.body), {"overflow-y": "hidden"}), "contentWindow" in this ? b.bind(this.contentWindow, "scroll", b.onscroll) : b.bind(n, "scroll", b.onscroll), b.opt.enablemousewheel && b.bind(n, "mousewheel", b.onmousewheel), b.opt.enablekeyboard && b.bind(n, x.isopera ? "keypress" : "keydown", b.onkeypress), (x.cantouch || b.opt.touchbehavior) && (b.bind(n, "mousedown", b.onmousedown), b.bind(n, "mousemove", function (e) {
                        b.onmousemove(e, !0)
                    }), b.opt.grabcursorenabled && x.cursorgrabvalue && b.css(e(n.body), {cursor: x.cursorgrabvalue})), b.bind(n, "mouseup", b.onmouseup), void(b.zoom && (b.opt.dblclickzoom && b.bind(n, "dblclick", b.doZoom), b.ongesturezoom && b.bind(n, "gestureend", b.ongesturezoom))))
                };
                this.doc[0].readyState && "complete" == this.doc[0].readyState && setTimeout(function () {
                    T.call(b.doc[0], !1)
                }, 500), b.bind(this.doc, "load", T)
            }
        }, this.showCursor = function (e, t) {
            b.cursortimeout && (clearTimeout(b.cursortimeout), b.cursortimeout = 0), b.rail && (b.autohidedom && (b.autohidedom.stop().css({opacity: b.opt.cursoropacitymax}), b.cursoractive = !0), b.rail.drag && 1 == b.rail.drag.pt || ("undefined" != typeof e && !1 !== e && (b.scroll.y = Math.round(1 * e / b.scrollratio.y)), "undefined" != typeof t && (b.scroll.x = Math.round(1 * t / b.scrollratio.x))), b.cursor.css({
                height: b.cursorheight,
                top: b.scroll.y
            }), b.cursorh && (!b.rail.align && b.rail.visibility ? b.cursorh.css({
                width: b.cursorwidth,
                left: b.scroll.x + b.rail.width
            }) : b.cursorh.css({
                width: b.cursorwidth,
                left: b.scroll.x
            }), b.cursoractive = !0), b.zoom && b.zoom.stop().css({opacity: b.opt.cursoropacitymax}))
        }, this.hideCursor = function (e) {
            !b.cursortimeout && b.rail && b.autohidedom && (b.cursortimeout = setTimeout(function () {
                b.rail.active && b.showonmouseevent || (b.autohidedom.stop().animate({opacity: b.opt.cursoropacitymin}), b.zoom && b.zoom.stop().animate({opacity: b.opt.cursoropacitymin}), b.cursoractive = !1), b.cursortimeout = 0
            }, e || b.opt.hidecursordelay))
        }, this.noticeCursor = function (e, t, n) {
            b.showCursor(t, n), b.rail.active || b.hideCursor(e)
        }, this.getContentSize = b.ispage ? function () {
            return {
                w: Math.max(document.body.scrollWidth, document.documentElement.scrollWidth),
                h: Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
            }
        } : b.haswrapper ? function () {
            return {
                w: b.doc.outerWidth() + parseInt(b.win.css("paddingLeft")) + parseInt(b.win.css("paddingRight")),
                h: b.doc.outerHeight() + parseInt(b.win.css("paddingTop")) + parseInt(b.win.css("paddingBottom"))
            }
        } : function () {
            return {w: b.docscroll[0].scrollWidth, h: b.docscroll[0].scrollHeight}
        }, this.onResize = function (e, t) {
            if (!b.win) return !1;
            if (!b.haswrapper && !b.ispage) {
                if ("none" == b.win.css("display")) return b.visibility && b.hideRail().hideRailHr(), !1;
                !b.hidden && !b.visibility && b.showRail().showRailHr()
            }
            var n = b.page.maxh, o = b.page.maxw, i = b.view.w;
            if (b.view = {
                w: b.ispage ? b.win.width() : parseInt(b.win[0].clientWidth),
                h: b.ispage ? b.win.height() : parseInt(b.win[0].clientHeight)
            }, b.page = t ? t : b.getContentSize(), b.page.maxh = Math.max(0, b.page.h - b.view.h), b.page.maxw = Math.max(0, b.page.w - b.view.w), b.page.maxh == n && b.page.maxw == o && b.view.w == i) {
                if (b.ispage) return b;
                if (n = b.win.offset(), b.lastposition && (o = b.lastposition, o.top == n.top && o.left == n.left)) return b;
                b.lastposition = n
            }
            return 0 == b.page.maxh ? (b.hideRail(), b.scrollvaluemax = 0, b.scroll.y = 0, b.scrollratio.y = 0, b.cursorheight = 0, b.setScrollTop(0), b.rail.scrollable = !1) : b.rail.scrollable = !0, 0 == b.page.maxw ? (b.hideRailHr(), b.scrollvaluemaxw = 0, b.scroll.x = 0, b.scrollratio.x = 0, b.cursorwidth = 0, b.setScrollLeft(0), b.railh.scrollable = !1) : b.railh.scrollable = !0, b.locked = 0 == b.page.maxh && 0 == b.page.maxw, b.locked ? (b.ispage || b.updateScrollBar(b.view), !1) : (b.hidden || b.visibility ? !b.hidden && !b.railh.visibility && b.showRailHr() : b.showRail().showRailHr(), b.istextarea && b.win.css("resize") && "none" != b.win.css("resize") && (b.view.h -= 20), b.cursorheight = Math.min(b.view.h, Math.round(b.view.h * (b.view.h / b.page.h))), b.cursorheight = b.opt.cursorfixedheight ? b.opt.cursorfixedheight : Math.max(b.opt.cursorminheight, b.cursorheight), b.cursorwidth = Math.min(b.view.w, Math.round(b.view.w * (b.view.w / b.page.w))), b.cursorwidth = b.opt.cursorfixedheight ? b.opt.cursorfixedheight : Math.max(b.opt.cursorminheight, b.cursorwidth), b.scrollvaluemax = b.view.h - b.cursorheight - b.cursor.hborder, b.railh && (b.railh.width = 0 < b.page.maxh ? b.view.w - b.rail.width : b.view.w, b.scrollvaluemaxw = b.railh.width - b.cursorwidth - b.cursorh.wborder), b.checkrtlmode && b.railh && (b.checkrtlmode = !1, b.opt.rtlmode && 0 == b.scroll.x && b.setScrollLeft(b.page.maxw)), b.ispage || b.updateScrollBar(b.view), b.scrollratio = {
                x: b.page.maxw / b.scrollvaluemaxw,
                y: b.page.maxh / b.scrollvaluemax
            }, b.getScrollTop() > b.page.maxh ? b.doScrollTop(b.page.maxh) : (b.scroll.y = Math.round(b.getScrollTop() * (1 / b.scrollratio.y)), b.scroll.x = Math.round(b.getScrollLeft() * (1 / b.scrollratio.x)), b.cursoractive && b.noticeCursor()), b.scroll.y && 0 == b.getScrollTop() && b.doScrollTo(Math.floor(b.scroll.y * b.scrollratio.y)), b)
        }, this.resize = b.onResize, this.lazyResize = function (e) {
            return e = isNaN(e) ? 30 : e, b.delayed("resize", b.resize, e),
                b
        }, this._bind = function (e, t, n, o) {
            b.events.push({
                e: e,
                n: t,
                f: n,
                b: o,
                q: !1
            }), e.addEventListener ? e.addEventListener(t, n, o || !1) : e.attachEvent ? e.attachEvent("on" + t, n) : e["on" + t] = n
        }, this.jqbind = function (t, n, o) {
            b.events.push({e: t, n: n, f: o, q: !0}), e(t).bind(n, o)
        }, this.bind = function (e, t, n, o) {
            var i = "jquery" in e ? e[0] : e;
            "mousewheel" == t ? "onwheel" in b.win ? b._bind(i, "wheel", n, o || !1) : (e = "undefined" != typeof document.onmousewheel ? "mousewheel" : "DOMMouseScroll", v(i, e, n, o || !1), "DOMMouseScroll" == e && v(i, "MozMousePixelScroll", n, o || !1)) : i.addEventListener ? (x.cantouch && /mouseup|mousedown|mousemove/.test(t) && b._bind(i, "mousedown" == t ? "touchstart" : "mouseup" == t ? "touchend" : "touchmove", function (e) {
                if (e.touches) {
                    if (2 > e.touches.length) {
                        var t = e.touches.length ? e.touches[0] : e;
                        t.original = e, n.call(this, t)
                    }
                } else e.changedTouches && (t = e.changedTouches[0], t.original = e, n.call(this, t))
            }, o || !1), b._bind(i, t, n, o || !1), x.cantouch && "mouseup" == t && b._bind(i, "touchcancel", n, o || !1)) : b._bind(i, t, function (e) {
                return (e = e || window.event || !1) && e.srcElement && (e.target = e.srcElement), "pageY" in e || (e.pageX = e.clientX + document.documentElement.scrollLeft, e.pageY = e.clientY + document.documentElement.scrollTop), !1 !== n.call(i, e) && !1 !== o || b.cancelEvent(e)
            })
        }, this._unbind = function (e, t, n, o) {
            e.removeEventListener ? e.removeEventListener(t, n, o) : e.detachEvent ? e.detachEvent("on" + t, n) : e["on" + t] = !1
        }, this.unbindAll = function () {
            for (var e = 0; e < b.events.length; e++) {
                var t = b.events[e];
                t.q ? t.e.unbind(t.n, t.f) : b._unbind(t.e, t.n, t.f, t.b)
            }
        }, this.cancelEvent = function (e) {
            return !!(e = e.original ? e.original : e ? e : window.event || !1) && (e.preventDefault && e.preventDefault(), e.stopPropagation && e.stopPropagation(), e.preventManipulation && e.preventManipulation(), e.cancelBubble = !0, e.cancel = !0, e.returnValue = !1)
        }, this.stopPropagation = function (e) {
            return !!(e = e.original ? e.original : e ? e : window.event || !1) && (e.stopPropagation ? e.stopPropagation() : (e.cancelBubble && (e.cancelBubble = !0), !1))
        }, this.showRail = function () {
            return 0 == b.page.maxh || !b.ispage && "none" == b.win.css("display") || (b.visibility = !0, b.rail.visibility = !0, b.rail.css("display", "block")), b
        }, this.showRailHr = function () {
            return b.railh ? (0 == b.page.maxw || !b.ispage && "none" == b.win.css("display") || (b.railh.visibility = !0, b.railh.css("display", "block")), b) : b
        }, this.hideRail = function () {
            return b.visibility = !1, b.rail.visibility = !1, b.rail.css("display", "none"), b
        }, this.hideRailHr = function () {
            return b.railh ? (b.railh.visibility = !1, b.railh.css("display", "none"), b) : b
        }, this.show = function () {
            return b.hidden = !1, b.locked = !1, b.showRail().showRailHr()
        }, this.hide = function () {
            return b.hidden = !0, b.locked = !0, b.hideRail().hideRailHr()
        }, this.toggle = function () {
            return b.hidden ? b.show() : b.hide()
        }, this.remove = function () {
            b.stop(), b.cursortimeout && clearTimeout(b.cursortimeout), b.doZoomOut(), b.unbindAll(), !1 !== b.observer && b.observer.disconnect(), !1 !== b.observerremover && b.observerremover.disconnect(), b.events = [], b.cursor && (b.cursor.remove(), b.cursor = null), b.cursorh && (b.cursorh.remove(), b.cursorh = null), b.rail && (b.rail.remove(), b.rail = null), b.railh && (b.railh.remove(), b.railh = null), b.zoom && (b.zoom.remove(), b.zoom = null);
            for (var e = 0; e < b.saved.css.length; e++) {
                var t = b.saved.css[e];
                t[0].css(t[1], "undefined" == typeof t[2] ? "" : t[2])
            }
            return b.saved = !1, b.me.data("__nicescroll", ""), b.me = null, b.doc = null, b.docscroll = null, b.win = null, b
        }, this.scrollstart = function (e) {
            return this.onscrollstart = e, b
        }, this.scrollend = function (e) {
            return this.onscrollend = e, b
        }, this.scrollcancel = function (e) {
            return this.onscrollcancel = e, b
        }, this.zoomin = function (e) {
            return this.onzoomin = e, b
        }, this.zoomout = function (e) {
            return this.onzoomout = e, b
        }, this.isScrollable = function (t) {
            if (t = t.target ? t.target : t, "OPTION" == t.nodeName) return !0;
            for (; t && 1 == t.nodeType && !/BODY|HTML/.test(t.nodeName);) {
                var n = e(t), n = n.css("overflowY") || n.css("overflowX") || n.css("overflow") || "";
                if (/scroll|auto/.test(n)) return t.clientHeight != t.scrollHeight;
                t = !!t.parentNode && t.parentNode
            }
            return !1
        }, this.getViewport = function (t) {
            for (t = !(!t || !t.parentNode) && t.parentNode; t && 1 == t.nodeType && !/BODY|HTML/.test(t.nodeName);) {
                var n = e(t), o = n.css("overflowY") || n.css("overflowX") || n.css("overflow") || "";
                if (/scroll|auto/.test(o) && t.clientHeight != t.scrollHeight || 0 < n.getNiceScroll().length) return n;
                t = !!t.parentNode && t.parentNode
            }
            return !1
        }, this.onmousewheel = function (e) {
            if (b.locked) return !0;
            if (b.rail.drag) return b.cancelEvent(e);
            if (!b.rail.scrollable) return !b.railh || !b.railh.scrollable || b.onmousewheelhr(e);
            var t = +new Date, n = !1;
            return b.opt.preservenativescrolling && b.checkarea + 600 < t && (b.nativescrollingarea = b.isScrollable(e), n = !0), b.checkarea = t, !!b.nativescrollingarea || ((e = y(e, !1, n)) && (b.checkarea = 0), e)
        }, this.onmousewheelhr = function (e) {
            if (b.locked || !b.railh.scrollable) return !0;
            if (b.rail.drag) return b.cancelEvent(e);
            var t = +new Date, n = !1;
            return b.opt.preservenativescrolling && b.checkarea + 600 < t && (b.nativescrollingarea = b.isScrollable(e), n = !0), b.checkarea = t, !!b.nativescrollingarea || (b.locked ? b.cancelEvent(e) : y(e, !0, n))
        }, this.stop = function () {
            return b.cancelScroll(), b.scrollmon && b.scrollmon.stop(), b.cursorfreezed = !1, b.scroll.y = Math.round(b.getScrollTop() * (1 / b.scrollratio.y)), b.noticeCursor(), b
        }, this.getTransitionSpeed = function (e) {
            var t = Math.round(10 * b.opt.scrollspeed);
            return e = Math.min(t, Math.round(e / 20 * b.opt.scrollspeed)), 20 < e ? e : 0
        }, b.opt.smoothscroll ? b.ishwscroll && x.hastransition && b.opt.usetransition ? (this.prepareTransition = function (e, t) {
            var n = t ? 20 < e ? e : 0 : b.getTransitionSpeed(e),
                o = n ? x.prefixstyle + "transform " + n + "ms ease-out" : "";
            return b.lasttransitionstyle && b.lasttransitionstyle == o || (b.lasttransitionstyle = o, b.doc.css(x.transitionstyle, o)), n
        }, this.doScrollLeft = function (e, t) {
            var n = b.scrollrunning ? b.newscrolly : b.getScrollTop();
            b.doScrollPos(e, n, t)
        }, this.doScrollTop = function (e, t) {
            var n = b.scrollrunning ? b.newscrollx : b.getScrollLeft();
            b.doScrollPos(n, e, t)
        }, this.doScrollPos = function (e, t, n) {
            var o = b.getScrollTop(), i = b.getScrollLeft();
            return (0 > (b.newscrolly - o) * (t - o) || 0 > (b.newscrollx - i) * (e - i)) && b.cancelScroll(), 0 == b.opt.bouncescroll && (0 > t ? t = 0 : t > b.page.maxh && (t = b.page.maxh), 0 > e ? e = 0 : e > b.page.maxw && (e = b.page.maxw)), (!b.scrollrunning || e != b.newscrollx || t != b.newscrolly) && (b.newscrolly = t, b.newscrollx = e, b.newscrollspeed = n || !1, !b.timer && void(b.timer = setTimeout(function () {
                var n, o, i = b.getScrollTop(), r = b.getScrollLeft();
                n = e - r, o = t - i, n = Math.round(Math.sqrt(Math.pow(n, 2) + Math.pow(o, 2))), n = b.newscrollspeed && 1 < b.newscrollspeed ? b.newscrollspeed : b.getTransitionSpeed(n), b.newscrollspeed && 1 >= b.newscrollspeed && (n *= b.newscrollspeed), b.prepareTransition(n, !0), b.timerscroll && b.timerscroll.tm && clearInterval(b.timerscroll.tm), 0 < n && (!b.scrollrunning && b.onscrollstart && b.onscrollstart.call(b, {
                    type: "scrollstart",
                    current: {x: r, y: i},
                    request: {x: e, y: t},
                    end: {x: b.newscrollx, y: b.newscrolly},
                    speed: n
                }), x.transitionend ? b.scrollendtrapped || (b.scrollendtrapped = !0, b.bind(b.doc, x.transitionend, b.onScrollEnd, !1)) : (b.scrollendtrapped && clearTimeout(b.scrollendtrapped), b.scrollendtrapped = setTimeout(b.onScrollEnd, n)), b.timerscroll = {
                    bz: new BezierClass(i, b.newscrolly, n, 0, 0, .58, 1),
                    bh: new BezierClass(r, b.newscrollx, n, 0, 0, .58, 1)
                }, b.cursorfreezed || (b.timerscroll.tm = setInterval(function () {
                    b.showCursor(b.getScrollTop(), b.getScrollLeft())
                }, 60))), b.synched("doScroll-set", function () {
                    b.timer = 0, b.scrollendtrapped && (b.scrollrunning = !0), b.setScrollTop(b.newscrolly), b.setScrollLeft(b.newscrollx), b.scrollendtrapped || b.onScrollEnd()
                })
            }, 50)))
        }, this.cancelScroll = function () {
            if (!b.scrollendtrapped) return !0;
            var e = b.getScrollTop(), t = b.getScrollLeft();
            return b.scrollrunning = !1, x.transitionend || clearTimeout(x.transitionend), b.scrollendtrapped = !1, b._unbind(b.doc, x.transitionend, b.onScrollEnd), b.prepareTransition(0), b.setScrollTop(e), b.railh && b.setScrollLeft(t), b.timerscroll && b.timerscroll.tm && clearInterval(b.timerscroll.tm), b.timerscroll = !1, b.cursorfreezed = !1, b.showCursor(e, t), b
        }, this.onScrollEnd = function () {
            b.scrollendtrapped && b._unbind(b.doc, x.transitionend, b.onScrollEnd), b.scrollendtrapped = !1, b.prepareTransition(0), b.timerscroll && b.timerscroll.tm && clearInterval(b.timerscroll.tm), b.timerscroll = !1;
            var e = b.getScrollTop(), t = b.getScrollLeft();
            return b.setScrollTop(e), b.railh && b.setScrollLeft(t), b.noticeCursor(!1, e, t), b.cursorfreezed = !1, 0 > e ? e = 0 : e > b.page.maxh && (e = b.page.maxh), 0 > t ? t = 0 : t > b.page.maxw && (t = b.page.maxw), e != b.newscrolly || t != b.newscrollx ? b.doScrollPos(t, e, b.opt.snapbackspeed) : (b.onscrollend && b.scrollrunning && b.onscrollend.call(b, {
                type: "scrollend",
                current: {x: t, y: e},
                end: {x: b.newscrollx, y: b.newscrolly}
            }), void(b.scrollrunning = !1))
        }) : (this.doScrollLeft = function (e, t) {
            var n = b.scrollrunning ? b.newscrolly : b.getScrollTop();
            b.doScrollPos(e, n, t)
        }, this.doScrollTop = function (e, t) {
            var n = b.scrollrunning ? b.newscrollx : b.getScrollLeft();
            b.doScrollPos(n, e, t)
        }, this.doScrollPos = function (e, t, n) {
            function o() {
                if (b.cancelAnimationFrame) return !0;
                if (b.scrollrunning = !0, d = 1 - d) return b.timer = l(o) || 1;
                var e = 0, t = sy = b.getScrollTop();
                if (b.dst.ay) {
                    var t = b.bzscroll ? b.dst.py + b.bzscroll.getNow() * b.dst.ay : b.newscrolly, n = t - sy;
                    (0 > n && t < b.newscrolly || 0 < n && t > b.newscrolly) && (t = b.newscrolly), b.setScrollTop(t), t == b.newscrolly && (e = 1)
                } else e = 1;
                var i = sx = b.getScrollLeft();
                b.dst.ax ? (i = b.bzscroll ? b.dst.px + b.bzscroll.getNow() * b.dst.ax : b.newscrollx, n = i - sx, (0 > n && i < b.newscrollx || 0 < n && i > b.newscrollx) && (i = b.newscrollx), b.setScrollLeft(i), i == b.newscrollx && (e += 1)) : e += 1, 2 == e ? (b.timer = 0, b.cursorfreezed = !1, b.bzscroll = !1, b.scrollrunning = !1, 0 > t ? t = 0 : t > b.page.maxh && (t = b.page.maxh), 0 > i ? i = 0 : i > b.page.maxw && (i = b.page.maxw), i != b.newscrollx || t != b.newscrolly ? b.doScrollPos(i, t) : b.onscrollend && b.onscrollend.call(b, {
                    type: "scrollend",
                    current: {x: sx, y: sy},
                    end: {x: b.newscrollx, y: b.newscrolly}
                })) : b.timer = l(o) || 1
            }

            if (t = "undefined" == typeof t || !1 === t ? b.getScrollTop(!0) : t, b.timer && b.newscrolly == t && b.newscrollx == e) return !0;
            b.timer && c(b.timer), b.timer = 0;
            var i = b.getScrollTop(), r = b.getScrollLeft();
            (0 > (b.newscrolly - i) * (t - i) || 0 > (b.newscrollx - r) * (e - r)) && b.cancelScroll(), b.newscrolly = t, b.newscrollx = e, b.bouncescroll && b.rail.visibility || (0 > b.newscrolly ? b.newscrolly = 0 : b.newscrolly > b.page.maxh && (b.newscrolly = b.page.maxh)), b.bouncescroll && b.railh.visibility || (0 > b.newscrollx ? b.newscrollx = 0 : b.newscrollx > b.page.maxw && (b.newscrollx = b.page.maxw)), b.dst = {}, b.dst.x = e - r, b.dst.y = t - i, b.dst.px = r, b.dst.py = i;
            var s = Math.round(Math.sqrt(Math.pow(b.dst.x, 2) + Math.pow(b.dst.y, 2)));
            b.dst.ax = b.dst.x / s, b.dst.ay = b.dst.y / s;
            var a = 0, u = s;
            if (0 == b.dst.x ? (a = i, u = t, b.dst.ay = 1, b.dst.py = 0) : 0 == b.dst.y && (a = r, u = e, b.dst.ax = 1, b.dst.px = 0), s = b.getTransitionSpeed(s), n && 1 >= n && (s *= n), b.bzscroll = 0 < s && (b.bzscroll ? b.bzscroll.update(u, s) : new BezierClass(a, u, s, 0, 1, 0, 1)), !b.timer) {
                (i == b.page.maxh && t >= b.page.maxh || r == b.page.maxw && e >= b.page.maxw) && b.checkContentSize();
                var d = 1;
                b.cancelAnimationFrame = !1, b.timer = 1, b.onscrollstart && !b.scrollrunning && b.onscrollstart.call(b, {
                    type: "scrollstart",
                    current: {x: r, y: i},
                    request: {x: e, y: t},
                    end: {x: b.newscrollx, y: b.newscrolly},
                    speed: s
                }), o(), (i == b.page.maxh && t >= i || r == b.page.maxw && e >= r) && b.checkContentSize(), b.noticeCursor()
            }
        }, this.cancelScroll = function () {
            return b.timer && c(b.timer), b.timer = 0, b.bzscroll = !1, b.scrollrunning = !1, b
        }) : (this.doScrollLeft = function (e, t) {
            var n = b.getScrollTop();
            b.doScrollPos(e, n, t)
        }, this.doScrollTop = function (e, t) {
            var n = b.getScrollLeft();
            b.doScrollPos(n, e, t)
        }, this.doScrollPos = function (e, t, n) {
            var o = e > b.page.maxw ? b.page.maxw : e;
            0 > o && (o = 0);
            var i = t > b.page.maxh ? b.page.maxh : t;
            0 > i && (i = 0), b.synched("scroll", function () {
                b.setScrollTop(i), b.setScrollLeft(o)
            })
        }, this.cancelScroll = function () {
        }), this.doScrollBy = function (e, t) {
            var n = 0,
                n = t ? Math.floor((b.scroll.y - e) * b.scrollratio.y) : (b.timer ? b.newscrolly : b.getScrollTop(!0)) - e;
            if (b.bouncescroll) {
                var o = Math.round(b.view.h / 2);
                n < -o ? n = -o : n > b.page.maxh + o && (n = b.page.maxh + o)
            }
            return b.cursorfreezed = !1, py = b.getScrollTop(!0), 0 > n && 0 >= py ? b.noticeCursor() : n > b.page.maxh && py >= b.page.maxh ? (b.checkContentSize(), b.noticeCursor()) : void b.doScrollTop(n)
        }, this.doScrollLeftBy = function (e, t) {
            var n = 0,
                n = t ? Math.floor((b.scroll.x - e) * b.scrollratio.x) : (b.timer ? b.newscrollx : b.getScrollLeft(!0)) - e;
            if (b.bouncescroll) {
                var o = Math.round(b.view.w / 2);
                n < -o ? n = -o : n > b.page.maxw + o && (n = b.page.maxw + o)
            }
            return b.cursorfreezed = !1, px = b.getScrollLeft(!0), 0 > n && 0 >= px || n > b.page.maxw && px >= b.page.maxw ? b.noticeCursor() : void b.doScrollLeft(n)
        }, this.doScrollTo = function (e, t) {
            t && Math.round(e * b.scrollratio.y), b.cursorfreezed = !1, b.doScrollTop(e)
        }, this.checkContentSize = function () {
            var e = b.getContentSize();
            (e.h != b.page.h || e.w != b.page.w) && b.resize(!1, e)
        }, b.onscroll = function (e) {
            b.rail.drag || b.cursorfreezed || b.synched("scroll", function () {
                b.scroll.y = Math.round(b.getScrollTop() * (1 / b.scrollratio.y)), b.railh && (b.scroll.x = Math.round(b.getScrollLeft() * (1 / b.scrollratio.x))), b.noticeCursor()
            })
        }, b.bind(b.docscroll, "scroll", b.onscroll), this.doZoomIn = function (t) {
            if (!b.zoomactive) {
                b.zoomactive = !0, b.zoomrestore = {style: {}};
                var n,
                    o = "position top left zIndex backgroundColor marginTop marginBottom marginLeft marginRight".split(" "),
                    i = b.win[0].style;
                for (n in o) {
                    var s = o[n];
                    b.zoomrestore.style[s] = "undefined" != typeof i[s] ? i[s] : ""
                }
                return b.zoomrestore.style.width = b.win.css("width"), b.zoomrestore.style.height = b.win.css("height"), b.zoomrestore.padding = {
                    w: b.win.outerWidth() - b.win.width(),
                    h: b.win.outerHeight() - b.win.height()
                }, x.isios4 && (b.zoomrestore.scrollTop = e(window).scrollTop(), e(window).scrollTop(0)), b.win.css({
                    position: x.isios4 ? "absolute" : "fixed",
                    top: 0,
                    left: 0,
                    "z-index": r + 100,
                    margin: "0px"
                }), o = b.win.css("backgroundColor"), ("" == o || /transparent|rgba\(0, 0, 0, 0\)|rgba\(0,0,0,0\)/.test(o)) && b.win.css("backgroundColor", "#fff"), b.rail.css({"z-index": r + 101}), b.zoom.css({"z-index": r + 102}), b.zoom.css("backgroundPosition", "0px -18px"), b.resizeZoom(), b.onzoomin && b.onzoomin.call(b), b.cancelEvent(t)
            }
        }, this.doZoomOut = function (t) {
            if (b.zoomactive) return b.zoomactive = !1, b.win.css("margin", ""), b.win.css(b.zoomrestore.style), x.isios4 && e(window).scrollTop(b.zoomrestore.scrollTop), b.rail.css({"z-index": b.zindex}), b.zoom.css({"z-index": b.zindex}), b.zoomrestore = !1, b.zoom.css("backgroundPosition", "0px 0px"), b.onResize(), b.onzoomout && b.onzoomout.call(b), b.cancelEvent(t)
        }, this.doZoom = function (e) {
            return b.zoomactive ? b.doZoomOut(e) : b.doZoomIn(e)
        }, this.resizeZoom = function () {
            if (b.zoomactive) {
                var t = b.getScrollTop();
                b.win.css({
                    width: e(window).width() - b.zoomrestore.padding.w + "px",
                    height: e(window).height() - b.zoomrestore.padding.h + "px"
                }), b.onResize(), b.setScrollTop(Math.min(b.page.maxh, t))
            }
        }, this.init(), e.nicescroll.push(this)
    }, m = function (e) {
        var t = this;
        this.nc = e, this.steptime = this.lasttime = this.speedy = this.speedx = this.lasty = this.lastx = 0, this.snapy = this.snapx = !1, this.demuly = this.demulx = 0, this.lastscrolly = this.lastscrollx = -1, this.timer = this.chky = this.chkx = 0, this.time = function () {
            return +new Date
        }, this.reset = function (e, n) {
            t.stop();
            var o = t.time();
            t.steptime = 0, t.lasttime = o, t.speedx = 0, t.speedy = 0, t.lastx = e, t.lasty = n, t.lastscrollx = -1, t.lastscrolly = -1
        }, this.update = function (e, n) {
            var o = t.time();
            t.steptime = o - t.lasttime, t.lasttime = o;
            var o = n - t.lasty, i = e - t.lastx, r = t.nc.getScrollTop(), s = t.nc.getScrollLeft(), r = r + o,
                s = s + i;
            t.snapx = 0 > s || s > t.nc.page.maxw, t.snapy = 0 > r || r > t.nc.page.maxh, t.speedx = i, t.speedy = o, t.lastx = e, t.lasty = n
        }, this.stop = function () {
            t.nc.unsynched("domomentum2d"), t.timer && clearTimeout(t.timer), t.timer = 0, t.lastscrollx = -1, t.lastscrolly = -1
        }, this.doSnapy = function (e, n) {
            var o = !1;
            0 > n ? (n = 0, o = !0) : n > t.nc.page.maxh && (n = t.nc.page.maxh, o = !0), 0 > e ? (e = 0, o = !0) : e > t.nc.page.maxw && (e = t.nc.page.maxw, o = !0), o && t.nc.doScrollPos(e, n, t.nc.opt.snapbackspeed)
        }, this.doMomentum = function (e) {
            var n = t.time(), o = e ? n + e : t.lasttime;
            e = t.nc.getScrollLeft();
            var i = t.nc.getScrollTop(), r = t.nc.page.maxh, s = t.nc.page.maxw;
            if (t.speedx = 0 < s ? Math.min(60, t.speedx) : 0, t.speedy = 0 < r ? Math.min(60, t.speedy) : 0, o = o && 50 >= n - o, (0 > i || i > r || 0 > e || e > s) && (o = !1), e = !(!t.speedx || !o) && t.speedx, t.speedy && o && t.speedy || e) {
                var a = Math.max(16, t.steptime);
                50 < a && (e = a / 50, t.speedx *= e, t.speedy *= e, a = 50), t.demulxy = 0, t.lastscrollx = t.nc.getScrollLeft(), t.chkx = t.lastscrollx, t.lastscrolly = t.nc.getScrollTop(), t.chky = t.lastscrolly;
                var l = t.lastscrollx, c = t.lastscrolly, u = function () {
                    var e = 600 < t.time() - n ? .04 : .02;
                    t.speedx && (l = Math.floor(t.lastscrollx - t.speedx * (1 - t.demulxy)), t.lastscrollx = l, 0 > l || l > s) && (e = .1), t.speedy && (c = Math.floor(t.lastscrolly - t.speedy * (1 - t.demulxy)), t.lastscrolly = c, 0 > c || c > r) && (e = .1), t.demulxy = Math.min(1, t.demulxy + e), t.nc.synched("domomentum2d", function () {
                        t.speedx && (t.nc.getScrollLeft() != t.chkx && t.stop(), t.chkx = l, t.nc.setScrollLeft(l)), t.speedy && (t.nc.getScrollTop() != t.chky && t.stop(), t.chky = c, t.nc.setScrollTop(c)), t.timer || (t.nc.hideCursor(), t.doSnapy(l, c))
                    }), 1 > t.demulxy ? t.timer = setTimeout(u, a) : (t.stop(), t.nc.hideCursor(), t.doSnapy(l, c))
                };
                u()
            } else t.doSnapy(t.nc.getScrollLeft(), t.nc.getScrollTop())
        }
    }, g = e.fn.scrollTop;
    e.cssHooks.pageYOffset = {
        get: function (t, n, o) {
            return (n = e.data(t, "__nicescroll") || !1) && n.ishwscroll ? n.getScrollTop() : g.call(t)
        }, set: function (t, n) {
            var o = e.data(t, "__nicescroll") || !1;
            return o && o.ishwscroll ? o.setScrollTop(parseInt(n)) : g.call(t, n), this
        }
    }, e.fn.scrollTop = function (t) {
        if ("undefined" == typeof t) {
            var n = !!this[0] && (e.data(this[0], "__nicescroll") || !1);
            return n && n.ishwscroll ? n.getScrollTop() : g.call(this)
        }
        return this.each(function () {
            var n = e.data(this, "__nicescroll") || !1;
            n && n.ishwscroll ? n.setScrollTop(parseInt(t)) : g.call(e(this), t)
        })
    };
    var v = e.fn.scrollLeft;
    e.cssHooks.pageXOffset = {
        get: function (t, n, o) {
            return (n = e.data(t, "__nicescroll") || !1) && n.ishwscroll ? n.getScrollLeft() : v.call(t)
        }, set: function (t, n) {
            var o = e.data(t, "__nicescroll") || !1;
            return o && o.ishwscroll ? o.setScrollLeft(parseInt(n)) : v.call(t, n), this
        }
    }, e.fn.scrollLeft = function (t) {
        if ("undefined" == typeof t) {
            var n = !!this[0] && (e.data(this[0], "__nicescroll") || !1);
            return n && n.ishwscroll ? n.getScrollLeft() : v.call(this)
        }
        return this.each(function () {
            var n = e.data(this, "__nicescroll") || !1;
            n && n.ishwscroll ? n.setScrollLeft(parseInt(t)) : v.call(e(this), t)
        })
    };
    var y = function (t) {
        var n = this;
        if (this.length = 0, this.name = "nicescrollarray", this.each = function (e) {
            for (var t = 0; t < n.length; t++) e.call(n[t]);
            return n
        }, this.push = function (e) {
            n[n.length] = e, n.length++
        }, this.eq = function (e) {
            return n[e]
        }, t) for (a = 0; a < t.length; a++) {
            var o = e.data(t[a], "__nicescroll") || !1;
            o && (this[this.length] = o, this.length++)
        }
        return this
    };
    !function (e, t, n) {
        for (var o = 0; o < t.length; o++) n(e, t[o])
    }(y.prototype, "show hide toggle onResize resize remove stop doScrollPos".split(" "), function (e, t) {
        e[t] = function () {
            var e = arguments;
            return this.each(function () {
                this[t].apply(this, e)
            })
        }
    }), e.fn.getNiceScroll = function (t) {
        return "undefined" == typeof t ? new y(this) : e.data(this[t], "__nicescroll") || !1
    }, e.extend(e.expr[":"], {
        nicescroll: function (t) {
            return !!e.data(t, "__nicescroll")
        }
    }), e.fn.niceScroll = function (t, n) {
        "undefined" == typeof n && "object" == typeof t && !("jquery" in t) && (n = t, t = !1);
        var o = new y;
        "undefined" == typeof n && (n = {}), t && (n.doc = e(t), n.win = e(this));
        var i = !("doc" in n);
        return !i && !("win" in n) && (n.win = e(this)), this.each(function () {
            var t = e(this).data("__nicescroll") || !1;
            t || (n.doc = i ? e(this) : n.doc, t = new f(n, e(this)), e(this).data("__nicescroll", t)), o.push(t)
        }), 1 == o.length ? o[0] : o
    }, window.NiceScroll = {
        getjQuery: function () {
            return e
        }
    }, e.nicescroll || (e.nicescroll = new y, e.nicescroll.options = d)
}(jQuery), $(".link-forms-container").click(function (e) {
    return !1
}), $("#main-links li").click(function () {
    var e = $(this);
    e.hasClass("selected") ? $("#bg-overlay").click() : ($("#main-links li").removeClass("selected"), $("#main-links li .link-forms-container").css("display", "none"), e.addClass("selected"), e.children(".link-forms-container").slideDown("slow"), $("#bg-overlay").removeClass("x-hidden"))
}), $("#bg-overlay").click(function () {
    $("#bg-overlay").addClass("x-hidden"), closeLinks()
});
var checkEmail = function (e) {
    var t = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return t.test(e)
};
$(document).on("focus", "input, textarea", function (e) {
    $(this).removeClass("error")
}), $(document).on("click", ".searcher_icon", function (e) {
    $("#searcher").fadeIn("slow")
}), $(document).on("click", ".close_searcher", function (e) {
    $("#searcher").fadeOut("slow")
}), $(document).on("click", ".follow_us", function (e) {
    return $("#wrapper").addClass("blurer"), $("#developer-tag").addClass("x-hidden"), $("#follow_popup").fadeIn("slow"), !1
}), $(document).on("click", ".contact_us", function (e) {
    return $("#wrapper").addClass("blurer"), $("#developer-tag").addClass("x-hidden"), $("#contact_popup").fadeIn("slow"), !1
}), $(document).on("click", ".popup_bg, .pop_closer", popCloser), $(document).on("keypress", "#searchText", function (e) {
    if (13 == e.keyCode) {
        var t = $("#searchText").val();
        if ("" != t) {
            var n = "/?search=" + t;
            window.location.href = n
        }
    }
}), $(document).on("keypress", ".contact_fields", function (e) {
    13 == e.keyCode && $("#contact_submit").click()
}), $(document).on("click", "#contact_submit", saveContact), $(document).on("keypress", ".subscribe_fields", function (e) {
    13 == e.keyCode && $("#subscribe_submit").click()
}), $(document).on("click", "#subscribe_submit", saveSubscriber), $(document).on("click", ".appreciater", saveAppreciate), $(window).resize(function () {
    var e = $("#main"), t = 0, n = parseInt(e.css("margin-left"), 10);
    n >= 0 && (t = 250);
    var o = $(this).innerHeight(), i = o;
    e.css({height: i});
    var r = $(this).innerWidth(), s = r < 768 ? "100%" : r - t, a = r < 935 ? i - 60 : i - 90;
    $("#main-nav").css({height: a}), $("#work-content").css({width: s});
    var l = $("#contact-form").innerHeight();
    l > i ? $("#contact-form").css({height: i}) : $("#contact-form").css({height: l})
}), $(window).trigger("resize"), $("#contact-form, #main-nav, #work-content").css("overflow-y", "auto").niceScroll({
    background: "#2C3031",
    cursorcolor: "#808183",
    cursorborder: "none medium"
}), $("#bamboo-menu").click(function () {
    var e = $("#main");
    if ($(this).hasClass("selected")) {
        $(this).removeClass("selected"), $.velocity.animate(e, {translateX: "0"}, {
            duration: 600,
            easing: "easeInOutSine"
        });
        var t = setTimeout(function () {
            clearTimeout(t), $("#main-nav").css("top", 95)
        }, 600)
    } else $("#main-nav").css("top", 0), $(this).addClass("selected"), $.velocity.animate(e, {translateX: "250px"}, {
        duration: 600,
        easing: "easeInOutSine"
    })
}), $(function () {
    initWorks()
});