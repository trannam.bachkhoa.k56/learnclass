<!DOCTYPE html>
<html lang="en">
    <head>
      <title></title>
      <base href="">
      <meta name="ROBOTS" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="keywords" content="" />
      <meta name="description" content="">
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="geo.position" content="10.763945;106.656201" />
      <meta name="robots" content="noindex">
      <meta name="googlebot" content="noindex">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="" />
      <meta property="og:description" content="" />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="" />
      <meta property="og:image" content="" />
      <link type="image/x-icon" href="" rel="SHORTCUT ICON"/>
      <!-- CSS -->
      <link rel="stylesheet" href="css/bootstrap.min.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/animate.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/hover.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.carousel.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.theme.default.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/fontawesome-all.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/extract.css" media="all" type="text/css" />
      <!-- SCSS -->
      <link rel="stylesheet" href="scss/styles.css" media="all" type="text/css" />
      <link rel="stylesheet" href="scss/reponsive.css" media="all" type="text/css" />
      <!-- JS -->

     <link rel="stylesheet" href="css/style.css" media="all" type="text/css" />
      <script src="js/jquery3.1.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.bundle.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/transition.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
  
      <script src="js/WOW.js"></script>
   </head>
   <body>
      <!-- HEADER -->
      <?php include('common/header.php')?>
      <!-- /header -->
      <!-- SLIDER -->
      <section class="breadcrumb">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <h1>Giáo viên</h1>
                  <ul>
                     <li><a href="">Trang chủ</a></li>
                     <li>/</li>
                     <li><a href="">Tin tức</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </section>
      <section class="categorySingle mg-40">
         <div class="container">
            <div class="row">
               <div class="col-lg-3 col-md-12 col-sm-12 col-12">
                  <div class="left mdds-no  mdtext-ct">
                     <img src="images/bannerteacher.jpg" alt="" class="pd5 br brcl-grey">
                  </div>
               </div>
               <div class="col-lg-9 col-md-12 sol-sm-12 col-12">
                  <h2 class="f28 clblack ds-bolck br-bottom brcl-grey pdbottom10 text-b text-up pdtop15 mdtext-ct mdpdtop25">Đội ngũ giáo viên chuyên nghiệp</h2>
                  <p class="text-js f14 clblack">Nulla facilisi. Donec vel feugiat urna, vel sagittis enim. Quisque eros odio, cursus id libero ac, ornare viverra quam. Vestibulum diam diam, varius id tortor vitae, gravida congue risus. Fusce vitae ex vitae neque dignissim vulputate. Fusce et massa sodales ex scelerisque finibus. Vestibulum porttitor erat lacus, id tincidunt ex luctus nec. Suspendisse tempus porttitor libero, eu cursus ipsum. Phasellus et gravida neque, quis porttitor metus. Phasellus tempor enim enim, eget interdum quam vulputate sed. Maecenas ac dolor justo. Maecenas rhoncus, metus a condimentum suscipit, purus ligula sollicitudin nisi, scelerisque porttitor metus odio vel nisl. In hac habitasse platea dictumst. Vivamus ut lacus diam. Vestibulum porttitor erat lacus, id tincidunt ex luctus nec. Suspendisse tempus porttitor libero.</p>
               </div>
            </div>
         </div>
      </section>
      <section class="listteach mg-50">
         <div class="container">
            <div class="row justify-content-lg-center">
               <div class="col-lg-10 col-md-12 col-sm-12 col-12">
                  <h3 class="f28 clblack ds-bolck text-b text-ct pdbottom20">Quản lý</h3>
                  <div class="owl-carousel">
                     <div class="item">
                        <div class="itemsingle pd10 br-rs5">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/tea1.jpg" alt="">
                                    </a>
                                 </div>
                              </div>
                              <div class="icon">
                                 <ul>
                                    <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-012 ds-block br br-orang"><i class="fab fa-facebook-f"></i></a></li>
                                    <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-twitter"></i></a></li>
                                    <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-linkedin-in"></i></a></li>
                                 </ul>
                              </div>
                           </div>
                           <div class="content cl-black f14 pd">
                              <h3><a href="" class="text-ca clgrey f20 clhr-orang">Mr Jon Vet</a></h3>
                              <p>Manager</p>
                              <p>Nulla facilisi. Donec vel feugiat urna, vel sagittis enim. Quisque eros odio, cursus id libero ac, ornare viverra quam. Vestibulum diam diam, varius id tortor vitae, gravida congue risus.</p>
                              <div class="date">
                                 <p>Monday - Friday : 9:00 - 15:00</p>
                                 <p>Saturday - Sunday :  10:00 - 15:00</p>
                              </div>
                              <a href="" class="text-up clwhite clhr-orang bgorang pd-10 pd-015 br-rs10 ds-inline bghr-white f16">Xem thêm</a>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="itemsingle pd10 br-rs5">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/tea2.jpg" alt="">
                                    </a>
                                 </div>
                              </div>
                              <div class="icon">
                                 <ul>
                                    <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-012 ds-block br br-orang"><i class="fab fa-facebook-f"></i></a></li>
                                    <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-twitter"></i></a></li>
                                    <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-linkedin-in"></i></a></li>
                                 </ul>
                              </div>
                           </div>
                           <div class="content cl-black f14 pd">
                              <h3><a href="" class="text-ca clgrey f20 clhr-orang">Mr Jon Vet</a></h3>
                              <p>Manager</p>
                              <p>Nulla facilisi. Donec vel feugiat urna, vel sagittis enim. Quisque eros odio, cursus id libero ac, ornare viverra quam. Vestibulum diam diam, varius id tortor vitae, gravida congue risus.</p>
                              <div class="date">
                                 <p>Monday - Friday : 9:00 - 15:00</p>
                                 <p>Saturday - Sunday :  10:00 - 15:00</p>
                              </div>
                              <a href="" class="text-up clwhite clhr-orang bgorang pd-10 pd-015 br-rs10 ds-inline bghr-white f16">Xem thêm</a>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="itemsingle pd10 br-rs5">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/tea3.jpg" alt="">
                                    </a>
                                 </div>
                              </div>
                              <div class="icon">
                                 <ul>
                                    <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-012 ds-block br br-orang"><i class="fab fa-facebook-f"></i></a></li>
                                    <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-twitter"></i></a></li>
                                    <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-linkedin-in"></i></a></li>
                                 </ul>
                              </div>
                           </div>
                           <div class="content cl-black f14 pd">
                              <h3><a href="" class="text-ca clgrey f20 clhr-orang">Mr Jon Vet</a></h3>
                              <p>Manager</p>
                              <p>Nulla facilisi. Donec vel feugiat urna, vel sagittis enim. Quisque eros odio, cursus id libero ac, ornare viverra quam. Vestibulum diam diam, varius id tortor vitae, gravida congue risus.</p>
                              <div class="date">
                                 <p>Monday - Friday : 9:00 - 15:00</p>
                                 <p>Saturday - Sunday :  10:00 - 15:00</p>
                              </div>
                              <a href="" class="text-up clwhite clhr-orang bgorang pd-10 pd-015 br-rs10 ds-inline bghr-white f16">Xem thêm</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <script>
                  $('.owl-carousel').owlCarousel({
                     loop:false,
                     margin:20,
                     responsiveClass:true,
                     autoplay:true,
                     autoplayTimeout:5000,
                     navigation : true,
                     nav:true,
                     responsive:{
                         0:{
                             items:1,
                             nav:false
                         },
                         600:{
                             items:2,
                             nav:false
                         },
                         1000:{
                             items:3,
                             nav:true,
                            
                         }
                     }
                 });
                 // $('.owl-prev').html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');
                 // $('.owl-next').html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
               </script>
               </div>
               
            </div>
         </div>
         
      </section>
       <section class="Starlear formcateNew mg-50">
         <div class="bgapactity">
            <div class="container">
               <div class="row justify-content-lg-center">
                  <div class=" col-12 titleIndex mdpdtop30">
                  <h3> <span class="clwhite">Đặt chỗ ngồi của bạn</span></h3>
                  <p><span class="clwhite">Vui lòng nhập vào form đăng kí</span></p>
                  <div class="borderTitle">
                     <div class="left" style="background: #fff"></div>
                     <div class="center clwhite"><i class="fas fa-graduation-cap"></i></div>
                     <div class="right" style="background: #fff"></div>
                  </div>
                  </div>
                  <div class="col-lg-10 col-md-12 col-sm-12 col-12 formContact">
                  <form action="" method="get" accept-charset="utf-8" class="">
                     <div class="form-group row mbpdleft10">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                           <label for="" class="text-b f14 clwhite">Họ *</label>
                           <input type="text"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey bgwhite" id="staticEmail"  placeholder="Nhập họ ...">
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                           <label for="" class="text-b f14 clwhite">Tên *</label>
                           <input type="text"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey bgwhite" id="staticEmail"  placeholder="Nhập tên">
                        </div>
                     </div>
                     <div class="form-group row mbpdleft10">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                           <label for="" class="text-b f14 clwhite">Website *</label>
                           <input type="text"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey bgwhite" id="staticEmail"  placeholder="Nhập họ ...">
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                           <label for="" class="text-b f14 clwhite">Tiêu đề *</label>
                           <input type="text"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey bgwhite" id="staticEmail"  placeholder="Nhập tên">
                        </div>
                     </div>
                     <div class="form-group row mbpdleft10">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                           <label for="" class="text-b f14 clwhite">Website *</label>
                           <select id="inputState" class="form-control">
                             <option selected>Choose...</option>
                             <option>...</option>
                           </select>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                           <label for="" class="text-b f14 clwhite">Tiêu đề *</label>
                           <select id="inputState" class="form-control">
                             <option selected>Choose...</option>
                             <option>...</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group row mdmgtop15 mbpdleft10">
                        <div class="col-12 pd pdright10">
                           <label for="" class="text-b f14 clwhite ">Bình luận *</label>
                           <textarea name="" rows="8" placeholder="Vui lòng để lại bình luận" class="w100 pdleft10 br brcl-input br-rs5 clgrey"></textarea>
                        </div>
                        
                     </div>
                     <div class="form-group row mbpdleft10">
                        <button class="pd-10 pd-020 bgnone clwhite f18 br br-white bghr-orang text-up text-b br-rs5   ">Gửi thông tin liên hệ</button>
                        
                     </div>
                  </form>     
               </div>
                  
               </div>
            </div>
         </div>
      </section>
     
      
      <!-- FOOTER -->
      <?php include('common/footer.php')?>
   </body>
</html>