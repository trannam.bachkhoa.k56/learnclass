<!DOCTYPE html>
<html lang="en">
    <head>
      <title></title>
      <base href="">
      <meta name="ROBOTS" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="keywords" content="" />
      <meta name="description" content="">
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="geo.position" content="10.763945;106.656201" />
      <meta name="robots" content="noindex">
      <meta name="googlebot" content="noindex">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="" />
      <meta property="og:description" content="" />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="" />
      <meta property="og:image" content="" />
      <link type="image/x-icon" href="" rel="SHORTCUT ICON"/>
      <!-- CSS -->
      <link rel="stylesheet" href="css/bootstrap.min.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/animate.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/hover.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.carousel.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.theme.default.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/fontawesome-all.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/extract.css" media="all" type="text/css" />
      <!-- SCSS -->
      <link rel="stylesheet" href="scss/styles.css" media="all" type="text/css" />
      <link rel="stylesheet" href="scss/reponsive.css" media="all" type="text/css" />
      <!-- JS -->

     <link rel="stylesheet" href="css/style.css" media="all" type="text/css" />
      <script src="js/jquery3.1.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.bundle.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/transition.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
  
      <script src="js/WOW.js"></script>
   </head>
   <body>
      <!-- HEADER -->
      <?php include('common/header.php')?>
      <!-- /header -->
      <!-- SLIDER -->
       <section class="breadcrumb ds-inherit pd">
         <div class="bgbread">
            <div class="container">
               <div class="row">
                  <div class="col-12 pdtop10">
                     <h1>Danh sách khóa học</h1>
                     <ul>
                        <li><a href="">Trang chủ</a></li>
                        <li>/</li>
                        <li><a href="">Danh sách khóa học</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="category">
         <div class="container">
            <div class="row">
               <div class="col-lg-3 col-md-3"></div>
               <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                  <marquee>

                  <ul>
                     <li><a href=""><img src="images/running.gif" alt="">Toán 4 - Thầy Nguyễn Xuân Thanh - Buổi 2: 7h30</a></li>
                     
                        <li><a href=""><img src="images/running.gif" alt="">Học photoshop Cơ bản -Thầy Phạm Văn Hoa - Buổi 3 </a></li>
                        
                  </ul>
                  
                    </marquee> 
               </div>
            </div>
         </div>
         </div>
      </section>
      <section class="categoryCourse">
         <div class="container">
            <div class="row">
               <div class="col-lg-3 col-md-3 col-sm-12 col-12 catesidebar noPadding ">
                  <div class="content">
                     <div class="titleside">
                        <h2><i class="fas fa-bars"></i>Danh mục khóa học</h2>
                     </div>
                     <div class="title">
                        <h3>các cấp học</h3>
                     </div>
                     <ul class="submenu-mb">
                        <li class="item">
                           <span><i class="fas fa-plus-square"></i></span>
                           <a href="" class="current">
                           Cấp tiểu học
                           </a>
                           <ul class="submenu-mb1">
                              <li><a href="#"><i class="far fa-check-square"></i> Lớp 1</a></li>
                              <li><a href="#"><i class="far fa-check-square"></i> Lớp 2</a></li>
                              <li><a href="#"><i class="far fa-check-square"></i> Lớp 3 </a></li>
                           </ul>
                        </li>
                        <li class="item">
                           <span><i class="fas fa-plus-square"></i></span>
                           <a href="" class="current">
                           Cấp THCS
                           </a>
                           <ul class="submenu-mb1">
                              <li><a href="#"><i class="far fa-check-square"></i> Lớp 1</a></li>
                              <li><a href="#"><i class="far fa-check-square"></i> Lớp 2</a></li>
                              <li><a href="#"><i class="far fa-check-square"></i> Lớp 3 </a></li>
                           </ul>
                        </li>
                        <li class="item">
                           <span><i class="fas fa-plus-square"></i></span>
                           <a href="" class="current">
                           Cấp THPT
                           </a>
                           <ul class="submenu-mb1">
                              <li><a href="#"><i class="far fa-check-square"></i> Lớp 1</a></li>
                              <li><a href="#"><i class="far fa-check-square"></i> Lớp 2</a></li>
                              <li><a href="#"><i class="far fa-check-square"></i> Lớp 3 </a></li>
                           </ul>
                        </li>
                     </ul>
                   
                           <script>

                         $(document).ready(function(){          
                                $('ul.submenu-mb>li.item').click(function(){
                                
                                 if($(this).hasClass('active')) {
                                     $(this).find('ul').slideUp();
                                     //$(this).find('i').css('transform','rotate(360deg)');
                                     $(this).find('i').css('transition','all 1s ease');
                                     $(this).removeClass('active');
                                    $(this).addClass('color');
                                     $(this).find('span').html('<i class="fas fa-plus-square"></i>');
                                 }
                                 else
                                 {
                                    $(this).find('ul').slideDown();
                                    $(this).addClass('active');
                                    // $(this).find('i').addClass('fa-rotate-180');
                                    //$(this).find('i').css('transform','rotate(180deg)');
                                    $(this).find('i').css('transition','all 1s ease');
                                     $(this).find('span').html('<i class="fas fa-minus-square"></i>');
                                      // $(this).find('span').addClass('fa-minus-square');

                                   
                                 }
                               });
                         });
                         </script>
                    
                     <!-- END CẤP HỌC -->
                     <div class="title">
                        <h3><i class="fas fa-male"></i>các môn học</h3>
                     </div>
                     <ul class="subjects">
                        <li><a href="#"><i class="far fa-check-square"></i> Toán học</a></li>
                        <li><a href="#"><i class="far fa-check-square"></i> Tiếng Anh</a></li>
                        <li><a href="#"><i class="far fa-check-square"></i> Văn học</a></li>
                        <li><a href="#"><i class="far fa-check-square"></i> Vật lý </a></li>
                        <li><a href="#"><i class="far fa-check-square"></i> Toán học</a></li>
                        <li><a href="#"><i class="far fa-check-square"></i> Toán học</a></li>
                        <li><a href="#"><i class="far fa-check-square"></i> Toán học</a></li>
                        <li><a href="#"><i class="far fa-check-square"></i> Ôn thi đại học</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-9 col-md-9 col-sm-12 col-12 contenitem">
                  <div class="row">
                     <div class="col-12 contentform">
                        <form action="category_submit" method="get" accept-charset="utf-8">
                           <input type="" name="" placeholder="Nhập tên khóa học">
                           <button type="submit"><i class="fas fa-search-plus"></i> Tìm kiếm </button>
                        </form>
                        <p class="note">Vui lòng nhập từ khóa tìm kiếm phù hợp với bạn</p>
                     </div>
                  </div>

                  <div class="row listCate">
                     <div class="col-lg-6 col-md-6 col-sm-12 col-12 titleLeft">
                        <p><a href="">Khóa học tiếng anh</a></p>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-12 col-12 showRight">
                        <div class="showall"><a href="">Xem tắt cả  <i class="fas fa-plus-square"></i></a></div>
                     </div>
                     <div class="owl-carousel">
                        <div class="item">
                           <div class="itemclass">
                              <div class="img">
                                 <div class="CropImg">
                                    <div class="thumbs">
                                       <a href="" title="">
                                       <img src="images/kh1.jpg" alt="">
                                       </a>
                                    </div>
                                 </div>
                                 <div class="icon">
                                    <img src="images/iconkhoahoc.jpg" alt="">
                                    <span>Nguyễn Văn A</span>
                                    <p class="price">Sắp bắt đầu</p>
                                 </div>
                              </div>
                              <div class="content">
                                 <h4><a href="">khóa học photoshop cơ bản22</a></h4>
                                 <div class="centertable">
                                    <div class="des">
                                       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                                    </div>
                                 </div>
                              </div>
                              <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                              <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                              <p class="spanicon"><span class="star"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></span> <span class="price">900.000 đ</span></p>
                              <a href="" title="" class="link">Ghi danh học ngay</a>
                           </div>
                        </div>
                        <div class="item">
                           <div class="itemclass">
                              <div class="img">
                                 <div class="CropImg">
                                    <div class="thumbs">
                                       <a href="" title="">
                                       <img src="images/kh1.jpg" alt="">
                                       </a>
                                    </div>
                                 </div>
                                 <div class="icon">
                                    <img src="images/iconkhoahoc.jpg" alt="">
                                    <span>Nguyễn Văn A</span>
                                    <p class="price">1.000.000 VNĐ</p>
                                 </div>
                              </div>
                              <div class="content">
                                 <h4><a href="">khóa học photoshop cơ bản22</a></h4>
                                 <div class="centertable">
                                    <div class="des">
                                       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                                    </div>
                                 </div>
                              </div>
                              <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                              <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                              <a href="" title="" class="link">Ghi danh học ngay</a>
                           </div>
                        </div>
                        <div class="item">
                           <div class="itemclass">
                              <div class="img">
                                 <div class="CropImg">
                                    <div class="thumbs">
                                       <a href="" title="">
                                       <img src="images/kh1.jpg" alt="">
                                       </a>
                                    </div>
                                 </div>
                                 <div class="icon">
                                    <img src="images/iconkhoahoc.jpg" alt="">
                                    <span>Nguyễn Văn A</span>
                                    <p class="price">1.000.000 VNĐ</p>
                                 </div>
                              </div>
                              <div class="content">
                                 <h4><a href="">khóa học photoshop cơ bản22</a></h4>
                                 <div class="centertable">
                                    <div class="des">
                                       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                                    </div>
                                 </div>
                              </div>
                              <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                              <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                              <a href="" title="" class="link">Ghi danh học ngay</a>
                           </div>
                        </div>
                     </div>
                     <script>
                        $('.owl-carousel').owlCarousel({
                           loop:true,
                           margin:10,
                           responsiveClass:true,
                           autoplay:true,
                           autoplayTimeout:5000,
                           navigation : true,
                           nav:true,
                           responsive:{
                               0:{
                                   items:1,
                                   nav:true
                               },
                               600:{
                                   items:2,
                                   nav:true
                               },
                               1000:{
                                   items:3,
                                   nav:true,
                                   loop:true
                               }
                           }
                        });
                        // $('.owl-prev').html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');
                        // $('.owl-next').html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
                     </script>
                  </div>



                   <div class="row listCate">
                     <div class="col-lg-6 col-md-6 col-sm-12 col-12 titleLeft">
                        <p><a href="">Khóa học tiếng anh</a></p>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-12 col-12 showRight">
                        <div class="showall"><a href="">Xem tắt cả</a></div>
                     </div>
                     <div class="owl-carousel">
                        <div class="item">
                           <div class="itemclass">
                              <div class="img">
                                 <div class="CropImg">
                                    <div class="thumbs">
                                       <a href="" title="">
                                       <img src="images/kh1.jpg" alt="">
                                       </a>
                                    </div>
                                 </div>
                                 <div class="icon">
                                    <img src="images/iconkhoahoc.jpg" alt="">
                                    <span>Nguyễn Văn A</span>
                                    <p class="price">Sắp bắt đầu</p>
                                 </div>
                              </div>
                              <div class="content">
                                 <h4><a href="">khóa học photoshop cơ bản22</a></h4>
                                 <div class="centertable">
                                    <div class="des">
                                       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                                    </div>
                                 </div>
                              </div>
                              <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                              <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                              <p><span class="star"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></span> <span class="price">900.000 đ</span></p>
                              <a href="" title="" class="link">Ghi danh học ngay</a>
                           </div>
                        </div>
                        <div class="item">
                           <div class="itemclass">
                              <div class="img">
                                 <div class="CropImg">
                                    <div class="thumbs">
                                       <a href="" title="">
                                       <img src="images/kh1.jpg" alt="">
                                       </a>
                                    </div>
                                 </div>
                                 <div class="icon">
                                    <img src="images/iconkhoahoc.jpg" alt="">
                                    <span>Nguyễn Văn A</span>
                                    <p class="price">1.000.000 VNĐ</p>
                                 </div>
                              </div>
                              <div class="content">
                                 <h4><a href="">khóa học photoshop cơ bản22</a></h4>
                                 <div class="centertable">
                                    <div class="des">
                                       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                                    </div>
                                 </div>
                              </div>
                              <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                              <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                              <a href="" title="" class="link">Ghi danh học ngay</a>
                           </div>
                        </div>
                        <div class="item">
                           <div class="itemclass">
                              <div class="img">
                                 <div class="CropImg">
                                    <div class="thumbs">
                                       <a href="" title="">
                                       <img src="images/kh1.jpg" alt="">
                                       </a>
                                    </div>
                                 </div>
                                 <div class="icon">
                                    <img src="images/iconkhoahoc.jpg" alt="">
                                    <span>Nguyễn Văn A</span>
                                    <p class="price">1.000.000 VNĐ</p>
                                 </div>
                              </div>
                              <div class="content">
                                 <h4><a href="">khóa học photoshop cơ bản22</a></h4>
                                 <div class="centertable">
                                    <div class="des">
                                       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                                    </div>
                                 </div>
                              </div>
                              <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                              <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                              <a href="" title="" class="link">Ghi danh học ngay</a>
                           </div>
                        </div>
                     </div>
                     <script>
                        $('.owl-carousel').owlCarousel({
                           loop:true,
                           margin:10,
                           responsiveClass:true,
                           autoplay:true,
                           autoplayTimeout:5000,
                           navigation : true,
                           nav:true,
                           responsive:{
                               0:{
                                   items:1,
                                   nav:true
                               },
                               600:{
                                   items:2,
                                   nav:true
                               },
                               1000:{
                                   items:3,
                                   nav:true,
                                   loop:true
                               }
                           }
                        });
                        // $('.owl-prev').html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');
                        // $('.owl-next').html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
                     </script>
                  </div>
                  <!-- END OWL -->
                  <div class="row">
                     <div class="col-12">
                        <div class="contentshow">
                           <a href=""><i class="fas fa-plus-square"></i> Xem Thêm</a>
                        </div>
                     </div>
                  </div>

               </div>

               <!-- END CONTEND -->

            </div>
         </div>
      </section>
      <!-- FOOTER -->
      <?php include('common/footer.php')?>
   </body>
</html>