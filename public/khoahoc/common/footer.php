 <footer>
         <div class="footerTop">
            <div class="container">
               <div class="row">
                  <div class="col-lg-7 col-md-12 col-sm-12 col-12 left">
                     <p><span class="icon"><i class="fas fa-phone-volume"></i></span> Nếu bạn gặp khó khăn hãy gọi ngay cho chúng tôi <span class="phone">0934553435</span></p>
                  </div>
                  <div class="col-lg-5 col-md-12 col-sm-12 col-12 right">
                     <a href="">Đăng kí nhận hướng dẫn tăng <span>90%</span> thu nhập</a>
                  </div>
               </div>
            </div>
         </div>
         <div class="footerContent">
            <div class="bgcontent">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="title">
                           <h3>Chúng tôi là ai?</h3>
                           <div class="bordertitle"></div>
                           <div class="borderf"></div>
                        </div>
                        <div class="content">
                           Trường học trực tuyến là đơn vị hàng đầu Việt Nam về lĩnh vực giáo dục, quy tụ các giáo viên hàng đầu về các lĩnh vực, ngành nghề, môn học các cấp. 
                           Chúng tôi cung cấp các phòng học trực tuyến chất lượng cao, tiêu chuẩn quốc tế. Hình ảnh chất lượng full HD
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="title">
                           <h3>Hướng dẫn?</h3>
                            <div class="bordertitle"></div>
                           <div class="borderf"></div>
                        </div>
                        <div class="contentul">
                           <ul>
                              <li><a href="">Hướng dẫn đăng kí học</a></li>
                              <li><a href="">Hướng dẫn tham gia khóa học</a></li>
                              <li><a href="">Đăng kí gia sư</a></li>
                              <li><a href="">Hướng dẫn thanh toán</a></li>
                              <li><a href="">Tham gia phòng họp</a></li>
                              <li><a href="">Đăng kí họp</a></li>
                              <li><a href="">Hướng dẫn thanh toán</a></li>
                              <li><a href="">Tham gia phòng họp</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="title">
                           <h3>Điều khoản</h3>
                            <div class="bordertitle"></div>
                           <div class="borderf"></div>
                        </div>
                        <div class="contentul">
                           <ul>
                              <li><a href="">Điều khoản thanh toán</a></li>
                              <li><a href="">Hình thức thanh toán</a></li>
                              <li><a href="">Chính sách dạy học</a></li>
                              <li><a href="">Chính sách học sinh</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="title">
                           <h3>Thông tin liện hệ</h3>
                            <div class="bordertitle"></div>
                           <div class="borderf"></div>
                        </div>
                        <div class="contentullast">
                           <ul>
                              <li><a href=""><i class="fas fa-phone-volume"></i>093455345</a></li>
                              <li><a href=""><i class="fas fa-phone-volume"></i>Địa chỉ : 733 Giải Phóng , Giát Bát , Hà Nội</a></li>
                              <li><a href=""><i class="fas fa-phone-volume"></i>Email: info@phonghoctructuyen.vn</a></li>
                              <li><a href=""><i class="fas fa-phone-volume"></i>Website:phonghoctructuyen.vn</a></li>
                           </ul>
                           <h4>Mạng xã hội</h4>
                           <div class="icon"><i class="fab fa-facebook-square"></i><i class="fab fa-google-plus-g"></i><i class="fab fa-youtube"></i><i class="fab fa-twitter"></i></div>
                           <img src="images/bcthuong.png" alt="">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-12">
                        <div class="listfooter">
                           <ul>
                              <li><a href="">Danh sách khóa học</a></li>
                              <li>|</li>
                              <li><a href="">Tăng thu nhập 90% ngay</a></li>
                              <li>|</li>
                              <li><a href="">Tin tức hoạt động</a></li>
                              <li>|</li>
                              <li><a href="">Hỏi đáp</a></li>
                              <li>|</li>
                              <li><a href="">Hướng dẫn</a></li>
                              <li>|</li>
                              <li><a href="">Liên hệ</a></li>
                              <li>|</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="copyring">
            <p class="mbpd-10"><img src="images/logocopy.png" alt="">CopyRight 2018 đơn vị phát triển Công ty cổ phần công nghệ VN3C Việt Nam - Công ty TNHH Goldkids Việt Nam</p>
         </div>
      </footer>