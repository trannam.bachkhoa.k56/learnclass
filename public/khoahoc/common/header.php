<header id="header" class="">
   <div class="headerTop hiddenMobile">
      <div class="container">
         <div class="col-12 noPadding">
            <div class="hdleft">
               <ul>
                  <li> <i class="fas fa-phone-volume"></i> HotLine : <span>0943 455 343</span></li>
                  <li> <i class="fas fa-envelope"></i> Email : info@phonghoctructuyen.vn</li>
               </ul>
            </div>
            <div class="hdright">
               <ul>
                  <li><a href="" title="" class="clhr-red bghr-white text-de"><i class="fas fa-graduation-cap"></i>Vào lớp học ngay</a></li>
                  <li><a href="" title="" class="text-de bghr-white "><i class="far fa-user-circle "></i>Đăng kí</a></li>
                  <li><a href="" title="" class="text-de bghr-white "><i class="fas fa-user-lock"></i>Đăng nhập</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- MENU -->
   <div class="hedermenu">
      <div class="container">
         <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-12">
               <div class="logo showMobile">
                  <a href="/" title=""><img src="images/logo.png" alt=""></a>
               </div>
            </div>
            <!-- 	  </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                 
                  <li class="nav-item">
                  	<a href="nav-link" title=""><i class="far fa-list-alt"></i></a>
               </div> -->
            <div class="col-lg-9 col-md-9 col-sm-12 col-12 noPadding">
               <div class="menuright">
                  <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand logo" href="#"><img src="images/logo.png" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>

                     <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                           <li class="nav-item active">
                              <a href="nav-link" title=""><i class="far fa-list-alt"></i></a>
                              <a class="nav-link" href="#">Danh sách khóa học</a>
                           </li>
                           <li class="nav-item">   
                              <a href="nav-link" title=""><i class="far fa-lightbulb"></i></a>
                              <a class="nav-link" href="#">Tăng thu nhập <span>90%</span> ngay</a>
                           </li>
                           <li class="nav-item">
                              <a href="nav-link" title=""><i class="fas fa-bullhorn"></i></a>
                              <a class="nav-link" href="#">Tin tức hoạt động</a>
                           </li>
                           <li class="nav-item">
                              <a href="nav-link" title=""><i class="fab fa-earlybirds"></i></a>
                              <a class="nav-link" href="#">Hỏi đáp</a>
                           </li>
                           <li class="nav-item">
                              <a href="nav-link" title=""><i class="fas fa-assistive-listening-systems"></i></a>
                              <a class="nav-link" href="#">Hướng dẫn</a>
                           </li>
                           <li class="nav-item">
                              <a href="nav-link" title=""><i class="fab fa-whatsapp"></i></a>
                              <a class="nav-link" href="#">Liên hệ</a>
                           </li>
                        </ul>
                     </div>
                  </nav>  
               </div>
            </div>
         </div>
        
      </div>
   </div>
</header>