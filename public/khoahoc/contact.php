<!DOCTYPE html>
<html lang="en">
    <head>
      <title></title>
      <base href="">
      <meta name="ROBOTS" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="keywords" content="" />
      <meta name="description" content="">
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="geo.position" content="10.763945;106.656201" />
      <meta name="robots" content="noindex">
      <meta name="googlebot" content="noindex">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="" />
      <meta property="og:description" content="" />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="" />
      <meta property="og:image" content="" />
      <link type="image/x-icon" href="" rel="SHORTCUT ICON"/>
      <!-- CSS -->
      <link rel="stylesheet" href="css/bootstrap.min.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/animate.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/hover.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.carousel.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.theme.default.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/fontawesome-all.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/extract.css" media="all" type="text/css" />
      <!-- SCSS -->
      <link rel="stylesheet" href="scss/styles.css" media="all" type="text/css" />
      <link rel="stylesheet" href="scss/reponsive.css" media="all" type="text/css" />
      <!-- JS -->

     <link rel="stylesheet" href="css/style.css" media="all" type="text/css" />
      <script src="js/jquery3.1.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.bundle.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/transition.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
  
      <script src="js/WOW.js"></script>
   </head>
   <body>
      <!-- HEADER -->
      <?php include('common/header.php')?>
      <!-- /header -->
      <!-- SLIDER -->
      <section class="breadcrumb">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <h1>liên hệ</h1>
                  <ul>
                     <li><a href="">Trang chủ</a></li>
                     <li>/</li>
                     <li><a href="">liên hệ</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </section>
      <section class="map">
         <div class="container">
            <div class="row"> 
               <div class="col-12"> 
                  <div class="contentMap"> 

                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="contact mg-40">
         <div class="container">
            <div class="row"> 
               <div class="col-lg-3 col-md-3 col-sm-12 col-12 sidebarContact">
                  <div class="content"> 
                     <div class="CropImg">
                        <div class="thumbs">
                          <a href="" title="">
                           <img src="images/callgirl.png" alt="">
                            </a>
                        </div>
                    </div>
                  </div>
                  <div class="contentdes mgleft20 mdmgleft0 mbmgbottom30 ">
                     <div class="company text-lt pd-20 mgtop20 mbtext-ct">
                        <h3 class="brcl-grey ds-block clgrey f22 mdf18 text-ca br-bottom brcl-grey pdbottom10 mbtext-ct">Địa chỉ</h3>
                        <p class="f14 clgrey">Xóm 4 , Xã Hữu Hòa , Thanh Trì , Hà Nội</p>
                     </div>

                      <div class="company text-lt pd-20 mbtext-ct">
                        <h3 class="brcl-grey ds-block clgrey f22 mdf18 text-ca br-bottom brcl-grey pdbottom10 mbtext-ct">Thông tin liên hệ</h3>
                        <p class="f14 clgrey"><span>Offie : 123123456</span></p>
                        <p class="f14 clgrey"><span>Offie : 123123456</span></p>
                        <p class="f14 clgrey"><span>Offie : 123123456</span></p>
                     </div>
                     <div class="company text-lt pd-20">
                       <h3 class="brcl-grey ds-block clgrey f22 mdf18 text-ca br-bottom brcl-grey pdbottom10 mbtext-ct">Theo dõi</h3>
                        <ul class="pd mbtext-ct">
                           <li class="ds-inline"><a href="" class="ds-block clgrey f16 clhr-red  mg-05"><i class="fab fa-facebook-f"></i></a></li>
                           <li class="ds-inline"><a href="" class="ds-block clgrey f16 clhr-red  mg-05"><i class="fab fa-twitter"></i></a></li>
                           <li class="ds-inline"><a href="" class="ds-block clgrey f16 clhr-red  mg-05"><i class="fab fa-google-plus-g"></i></a></li>
                        </ul>
                     </div>
                  </div>
               </div>

               <div class="col-lg-9 col-md-9 col-sm-12 col-12 formContact">
                  <h2 class="text-ca f28 clgrey text-lt mdf24 mbtext-ct ">Nhập thông tin liên hệ</h2>
                  <form action="" method="get" accept-charset="utf-8" class="mgtop40 mgleft20 mdmgtop20">
                     <div class="form-group row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                           <label for="" class="text-b f14 clgrey">Họ *</label>
                           <input type="text"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey" id="staticEmail"  placeholder="Nhập họ ...">
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                           <label for="" class="text-b f14 clgrey">Tên *</label>
                           <input type="text"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey" id="staticEmail"  placeholder="Nhập tên">
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                           <label for="" class="text-b f14 clgrey">Website *</label>
                           <input type="text"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey" id="staticEmail"  placeholder="Nhập họ ...">
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                           <label for="" class="text-b f14 clgrey">Tiêu đề *</label>
                           <input type="text"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey" id="staticEmail"  placeholder="Nhập tên">
                        </div>
                     </div>
                     <div class="form-group row ">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                           <label for="" class="text-b f14 clgrey">Website *</label>
                           <select id="inputState" class="form-control">
                             <option selected>Choose...</option>
                             <option>...</option>
                           </select>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                           <label for="" class="text-b f14 clgrey">Tiêu đề *</label>
                           <select id="inputState" class="form-control">
                             <option selected>Choose...</option>
                             <option>...</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group row mdmgtop15">
                        <div class="col-12 pd pdright10">
                           <label for="" class="text-b f14 clgrey ">Bình luận *</label>
                           <textarea name="" rows="8" placeholder="Vui lòng để lại bình luận" class="w100 pdleft10 br brcl-input br-rs5 clgrey"></textarea>
                        </div>
                        
                     </div>
                     <div class="form-group row">
                        <button class="pd-10 pd-020 bgblue clwhite f18 br br-blue text-up text-b br-rs5 bghr-white clhr-blue  ">Gửi liên hệ</button>
                        
                     </div>
                  </form>     
               </div>
            </div>
         </div>   
      </section>  
      
      <!-- FOOTER -->
      <?php include('common/footer.php')?>
   </body>
</html>