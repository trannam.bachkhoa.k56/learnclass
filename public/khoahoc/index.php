<!DOCTYPE html>
<html lang="en">
   <head>
      <title></title>
      <base href="">
      <meta name="ROBOTS" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="keywords" content="" />
      <meta name="description" content="">
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="geo.position" content="10.763945;106.656201" />
      <meta name="robots" content="noindex">
      <meta name="googlebot" content="noindex">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="" />
      <meta property="og:description" content="" />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="" />
      <meta property="og:image" content="" />
      <link type="image/x-icon" href="" rel="SHORTCUT ICON"/>
      <!-- CSS -->
      <link rel="stylesheet" href="css/bootstrap.min.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/animate.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/hover.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.carousel.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.theme.default.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/fontawesome-all.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/extract.css" media="all" type="text/css" />
      <!-- SCSS -->
      <link rel="stylesheet" href="scss/styles.css" media="all" type="text/css" />
      <link rel="stylesheet" href="scss/reponsive.css" media="all" type="text/css" />
      <!-- JS -->

     <link rel="stylesheet" href="css/style.css" media="all" type="text/css" />
      <script src="js/jquery3.1.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.bundle.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/transition.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
  
      <script src="js/WOW.js"></script>
   </head>
   <body>
      <!-- HEADER -->
       <?php include('common/header.php')?>
      <!-- /header -->
      <!-- SLIDER -->
      <section class="slider hiddenTAB">
         <div class="row">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
             
               <div class="carousel-inner">
                  <div class="carousel-item active">
                     <img src="images/sl1.jpg" alt="...">
                     <div class="carousel-caption d-none d-md-block">
                        <div class="content">
                           <h5>Thu nhập đến 20 triệu/tháng</h5>
                        <p>Đăng kí làm giáo viên ngay hôm nay để tăng <span>90%</span> thu nhập chỉ ngay tháng đầu tiên</p>
                        <a href="" title="" class="bghr-grey text-de">Đăng kí ngay<i class="fas fa-angle-double-right"></i></a>
                        </div>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <img src="images/sl1.jpg" alt="...">
                     <div class="carousel-caption d-none d-md-block">
                        <div class="content">
                           <h5>Thu nhập đến 20 triệu/tháng</h5>
                        <p>Đăng kí làm giáo viên ngay hôm nay để tăng <span>90%</span> thu nhập chỉ ngay tháng đầu tiên</p>
                        <a href="" title="" class="bghr-grey text-de">Đăng kí ngay<i class="fas fa-angle-double-right"></i></a>
                        </div>
                     </div>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <img src="images/sl1.jpg" alt="...">
                     <div class="carousel-caption d-none d-md-block">
                        <div class="content">
                           <h5>Thu nhập đến 20 triệu/tháng</h5>
                        <p>Đăng kí làm giáo viên ngay hôm nay để tăng <span>90%</span> thu nhập chỉ ngay tháng đầu tiên</p>
                        <a href="" title="" class="bghr-grey text-de">Đăng kí ngay<i class="fas fa-angle-double-right"></i></a>
                        </div>
                     </div>
                     </div>
                  </div>
               </div>
               <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
               <span class="carousel-control-prev-icon" aria-hidden="true"></span>
               <span class="sr-only">Previous</span>
               </a>
               <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
               <span class="carousel-control-next-icon" aria-hidden="true"></span>
               <span class="sr-only">Next</span>
               </a>
            </div>
         </div>
      </section>
      <!-- SOLOGEN -->
      <section class="sologan">
         <div class="row">
            <div class="col-lg-4 col-md-4 som-sm-12 col-12 noPadding itemsologan">
               <div class="itemsl">
                  <h3><i class="far fa-hand-peace"></i>Đăng kí học mọi lúc mọi nơi</h3>
                  <p class="des">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. Duis cursus quam at mi suscipit semper.</p>
                  <ul>
                     <li><a href="" title=""><i class="far fa-check-square"></i> 1.Lorem ipsum dolor sit amet, consectetur </a></li>
                     <li><a href="" title=""><i class="far fa-check-square"></i> 2.Lorem ipsum dolor sit amet, consectetur </a></li>
                     <li><a href="" title=""><i class="far fa-check-square"></i> 3.Lorem ipsum dolor sit amet, consectetur </a></li>
                     <li><a href="" title=""><i class="far fa-check-square"></i> 4.Lorem ipsum dolor sit amet, consectetur </a></li>
                  </ul>
               </div>
            </div>
            <div class="col-lg-4 col-md-4 som-sm-12 col-12 noPadding itemsologan activesl">
               <div class="itemsl active">
                  <h3><i class="far fa-user-circle"></i>Đội ngũ giáo viên năng động</h3>
                  <p class="des">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. Duis cursus quam at mi suscipit semper.</p>
                  <ul>
                     <li><a href="" title=""><i class="far fa-check-square"></i> 1.Lorem ipsum dolor sit amet, consectetur </a></li>
                     <li><a href="" title=""><i class="far fa-check-square"></i> 2.Lorem ipsum dolor sit amet, consectetur </a></li>
                     <li><a href="" title=""><i class="far fa-check-square"></i> 3.Lorem ipsum dolor sit amet, consectetur </a></li>
                     <li><a href="" title=""><i class="far fa-check-square"></i> 4.Lorem ipsum dolor sit amet, consectetur </a></li>
                  </ul>
               </div>
            </div>
            <div class="col-lg-4 col-md-4 som-sm-12 col-12 noPadding itemsologan">
               <div class="itemsl">
                  <h3><i class="fas fa-dollar-sign"></i>Tối ưu các hóa chi phí</h3>
                  <p class="des">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. Duis cursus quam at mi suscipit semper.</p>
                <ul>
                     <li><a href="" title=""><i class="far fa-check-square"></i> 1.Lorem ipsum dolor sit amet, consectetur </a></li>
                     <li><a href="" title=""><i class="far fa-check-square"></i> 2.Lorem ipsum dolor sit amet, consectetur </a></li>
                     <li><a href="" title=""><i class="far fa-check-square"></i> 3.Lorem ipsum dolor sit amet, consectetur </a></li>
                     <li><a href="" title=""><i class="far fa-check-square"></i> 4.Lorem ipsum dolor sit amet, consectetur </a></li>
                  </ul>
               </div>
            </div>
         </div>
         </div>
      </section>
      <!-- TĂNG THU NHẬP -->
      <section class="income">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-12 col-12 incomeLeft hiddenMobile">
                  <div class="img">
                     <a href="" title=""><img src="images/khachhang.png" alt=""></a>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-12 incomeRight">
                  <div class="iccenter">
                     <div class="content">
                        <h3>Tăng đên <span>90%</span> thu nhập</h3>
                        <p class="des">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. Duis cursus quam at mi suscipit semper. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris interdum est scelerisque nibh porttitor dictum. In sed lectus lacus. Vivamus sodales lacus non sem efficitur convallis. Aenean ex lorem, porta pharetra ornare eu, posuere ut libero. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae</p>
                        <a href="" title="" class="bghr-grey clhr-white">Tham gia ngay <i class="fas fa-angle-double-right"></i></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- CÁC LỚP BẮT ĐẦU -->
      <section class="classStar">
         <div class="container">
            <div class="row">
               <div class="col-12 titleIndex">
                  <h3>các lớp học hoặc sắp bắt đầu</h3>
                  <p>Nhanh tay chọn lớp phù hợp</p>
                  <div class="borderTitle">
                     <div class="left"></div>
                     <div class="center"><i class="fas fa-graduation-cap"></i></div>
                     <div class="right"></div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="menutab">
                  <nav>
                     <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Toán</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Lý</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Hóa</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact1" role="tab" aria-controls="nav-contact1" aria-selected="false">Ngoại ngữ</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact2" role="tab" aria-controls="nav-contact2" aria-selected="false">Kỹ năng sống</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact3" role="tab" aria-controls="nav-contact3" aria-selected="false">Học nghề</a>
                     </div>
                  </nav>
               </div>
            </div>
            <div class="tab-content" id="nav-tabContent">
               <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                  <div class="row">
                     <div class="col-lg-3 col-md-6 col-sm-12 col-12 itemcol">
                        <div class="itemclass">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/kh1.jpg" alt="">
                                    </a>
                                 </div>
                              </div>
                              <div class="icon">
                                 <img src="images/iconkhoahoc.jpg" alt="">
                                 <span>Nguyễn Văn A</span>
                                 <p class="price">1.000.000 VNĐ</p>
                              </div>
                           </div>
                           
                           <div class="content">
                              <h4><a href="">khóa học photoshop cơ bản22</a></h4>
                              <div class="centertable">
                                 <div class="des">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                                 </div>
                              </div>
                           </div>
                           <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                           <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                           <a href="" title="" class="link">Ghi danh học ngay</a>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-6 col-sm-12 col-12 itemcol">
                        <div class="itemclass">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/kh1.jpg" alt="">
                                    </a>
                                 </div>
                              </div>
                              <div class="icon">
                                 <img src="images/iconkhoahoc.jpg" alt="">
                                 <span>Nguyễn Văn A</span>
                                 <p class="price">1.000.000 VNĐ</p>
                              </div>
                           </div>
                           
                           <div class="content">
                              <h4><a href="">khóa học photoshop cơ bản22</a></h4>
                              <div class="centertable">
                                 <div class="des">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                                 </div>
                              </div>
                           </div>
                           <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                           <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                           <a href="" title="" class="link">Ghi danh học ngay</a>
                        </div>
                     </div>
                      <div class="col-lg-3 col-md-6 col-sm-12 col-12 itemcol">
                        <div class="itemclass">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/kh1.jpg" alt="">
                                    </a>
                                 </div>
                              </div>
                              <div class="icon">
                                 <img src="images/iconkhoahoc.jpg" alt="">
                                 <span>Nguyễn Văn A</span>
                                 <p class="price">1.000.000 VNĐ</p>
                              </div>
                           </div>
                           
                           <div class="content">
                              <h4><a href="">khóa học photoshop cơ bản22</a></h4>
                              <div class="centertable">
                                 <div class="des">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                                 </div>
                              </div>
                           </div>
                           <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                           <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                           <a href="" title="" class="link">Ghi danh học ngay</a>
                        </div>
                     </div>
                      <div class="col-lg-3 col-md-6 col-sm-12 col-12 itemcol">
                        <div class="itemclass">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/kh1.jpg" alt="">
                                    </a>
                                 </div>
                              </div>
                              <div class="icon">
                                 <img src="images/iconkhoahoc.jpg" alt="">
                                 <span>Nguyễn Văn A</span>
                                 <p class="price">1.000.000 VNĐ</p>
                              </div>
                           </div>
                           
                           <div class="content">
                              <h4><a href="">khóa học photoshop cơ bản22</a></h4>
                              <div class="centertable">
                                 <div class="des">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                                 </div>
                              </div>
                           </div>
                           <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                           <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                           <a href="" title="" class="link">Ghi danh học ngay</a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                  <div class="row">
                     <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="itemclass">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/kh2.jpg" alt="">
                                    </a>
                                 </div>
                              </div>
                              <div class="icon">
                              <img src="images/iconkhoahoc.jpg" alt="">
                              <p class="price">1.000.000 VNĐ</p>
                              </div>
                           </div>
                           <div class="content">
                              <h4><a href="">khóa học photoshop cơ bản22</a></h4>
                              <div class="centertable">
                                 <div class="des">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                                 </div>
                              </div>
                           </div>
                           <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                           <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                           <a href="" title="" class="link">Ghi danh học ngay</a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                  <div class="row">
                     <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="itemclass">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/kh1.jpg" alt="">
                                    </a>
                                 </div>
                              </div>
                              <div class="icon">
                              <img src="images/iconkhoahoc.jpg" alt="">
                              <p class="price">1.000.000 VNĐ</p>
                              </div>
                           </div>
                        
                           <div class="content">
                              <h4><a href="">khóa học photoshop cơ bản22</a></h4>
                              <div class="centertable">
                                 <div class="des">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                                 </div>
                              </div>
                           </div>
                           <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                           <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                           <a href="" title="" class="link">Ghi danh học ngay</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--  <div class="col-lg-3 col-md-3 col-sm-6 col-12">
               <div class="itemclass">
               	<div class="img">
                                 <div class="CropImg">
                                   <div class="thumbs">
                                     <a href="" title="">
                                       <img src="images/kh3.jpg" alt="">
                                     </a>
                                   </div>
                                 </div>
                               </div>
                               <div class="icon">
                               	<img src="images/iconkhoahoc.jpg" alt="">
                               </div>
                               <div class="content">
                               	<h4>khóa học photoshop cơ bản22</h4>
                               	<div class="centertable">
                               		<div class="des">
                               			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                               		</div>
                               	</div>
                               </div>
                               <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                               <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                               <a href="" title="" class="link">Ghi danh học ngay</a>
               </div>
               </div> -->
            <!-- <div class="tab-content" id="myTabContent">
               <div class="col-lg-3 col-md-3 col-sm-6 col-12">
               	<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
               		<div class="itemclass">
               			<div class="img">
                                   <div class="CropImg">
                                     <div class="thumbs">
                                       <a href="" title="">
                                         <img src="images/kh1.jpg" alt="">
                                       </a>
                                     </div>
                                   </div>
                                 </div>
                                 <div class="icon">
                                 	<img src="images/iconkhoahoc.jpg" alt="">
                                 </div>
                                 <div class="content">
                                 	<h4>khóa học photoshop cơ bản</h4>
                                 	<div class="centertable">
                                 		<div class="des">
                                 			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac aliquam nibh. Ut in rhoncus quam. </p>
                                 		</div>
                                 	</div>
                                 </div>
                                 <p class="timw"><i class="far fa-clock"></i><span>Bắt đầu : </span>7h15 ngày 5/6 - 16/6/2018</p>
                                 <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi : </span> 12 buổi</p>
                                 <a href="" title="" class="link">Ghi danh học ngay</a>
               		</div>
               	</div>
               </div>
               </div>
               <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">1.Lorem</div>
               <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">.2222222.</div> -->
         </div>
      </section>
      <!-- GIÁO VIÊN CỦA CHÚNG TÔI -->
      <section class="teacher">
         <div class="container">
            <div class="row">
               <div class="col-12 titleIndex">
                  <h3>Giáo viên của chúng tôi</h3>
                  <p>Hệ thống chuyên nghiệp</p>
                  <div class="borderTitle">
                     <div class="left"></div>
                     <div class="center"><i class="fas fa-graduation-cap"></i></div>
                     <div class="right"></div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-lg-11 col-md-12 col-sm-12 col-12">
                  <div class="owl-carousel">
                     <div class="item">
                        <div class="itemteacher">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/giaovien1.jpg" alt="">
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <a href="" class="titletea">Nguyễn Văn A</a>
                           <p class="des">Giảng viên trường Đại Học Hà Nội</p>
                        </div>
                     </div>
                     <div class="item">
                        <div class="itemteacher">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/giaovien2.jpg" alt="">
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <a href="" class="titletea">Nguyễn Văn A</a>
                           <p class="des">Giảng viên trường Đại Học Hà Nội</p>
                        </div>
                     </div>
                     <div class="item">
                        <div class="itemteacher">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/giaovien3.jpg" alt="">
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <a href="" class="titletea">Nguyễn Văn A</a>
                           <p class="des">Giảng viên trường Đại Học Hà Nội</p>
                        </div>
                     </div>
                     <div class="item">
                        <div class="itemteacher">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/giaovien4.jpg" alt="">
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <a href="" class="titletea">Nguyễn Văn A</a>
                           <p class="des">Giảng viên trường Đại Học Hà Nội</p>
                        </div>
                     </div>
                  </div>
                  <script>
                  $('.owl-carousel').owlCarousel({
                     loop:true,
                     margin:10,
                     responsiveClass:true,
                     autoplay:true,
                     autoplayTimeout:5000,
                     navigation : true,
                     nav:true,
                     responsive:{
                         0:{
                             items:2,
                             nav:true
                         },
                         600:{
                             items:3,
                             nav:true
                         },
                         1000:{
                             items:4,
                             nav:true,
                             loop:true
                         }
                     }
                 });
                 // $('.owl-prev').html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');
                 // $('.owl-next').html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
               </script>
               </div>
            
            </div>
         </div>
      </section>
      <!-- BẮT ĐẦU HỌC PHÍ -->
      <section class="Starlear">
         <div class="bgapactity">
            <div class="container">
               <div class="row">
                  <div class="col-lg-5 col-md-5 col-sm-12 col-12 hiddenMobile">
                     <div class="banner">
                        <img src="images/banner2.png" alt="">
                     </div>
                  </div>
                  <div class="col-lg-7 col-md-7 col-sm-12 col-12 starRight">
                     <div class="contentStar">
                        <h4 class="title">rất đơn giản bắt đầu học với 3 bước</h4>
                        <div class="itemsteep">
                           <div class="number">
                           <span>1</span>
                           </div>
                           <div class="right">
                              <a href="">Lựa chọn lớp học phù hợp yêu thích của bạn</a>
                              <p class="des">Nhanh chóng tham gia lớp học ngay khi đủ người</p>
                           </div>
                        </div>
                        <div class="itemsteep">
                           <div class="number">
                          <span>2</span>
                           </div>
                           <div class="right">
                              <a href="">Lựa chọn lớp học phù hợp yêu thích của bạn</a>
                              <p class="des">Nhanh chóng tham gia lớp học ngay khi đủ người</p>
                           </div>
                        </div>
                        <div class="itemsteep">
                           <div class="number">
                            <span>3</span>
                           </div>
                           <div class="right">
                              <a href="">Lựa chọn lớp học phù hợp yêu thích của bạn</a>
                              <p class="des">Nhanh chóng tham gia lớp học ngay khi đủ người</p>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- KHÁCH HÀNG NÓI VỀ CHÚNG TÔI -->
      <section class="customer">
         <div class="container">
            <div class="row">
               <div class="col-12 titleIndex">
                  <h3>khách hàng nới  về chúng tôi</h3>
                  <p>Khách hàng tin tưởng vào phòng học trực tuyến</p>
                  <div class="borderTitle">
                     <div class="left"></div>
                     <div class="center"><i class="fas fa-graduation-cap"></i></div>
                     <div class="right"></div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-lg-11 col-md-12 col-sm-12">
                  <div class="owl-carousel">
                     <div class="item">
                        <div class="itemcustomer">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/k1.png" alt="">
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <div class="content">
                              <a href="" class="titletea">Very happy to find this intube </a>
                              <p class="des">Giảng viên trường Đại Học Hà Nội</p>
                              <p><i class="fas fa-minus"></i><span>Maria Doet</span></p>
                           </div>
                        </div>
                     </div>

                     <div class="item">
                        <div class="itemcustomer">
                           <div class="img">
                              <div class="CropImg">
                                 <div class="thumbs">
                                    <a href="" title="">
                                    <img src="images/k2.png" alt="">
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <div class="content">
                              <a href="" class="titletea">Nguyễn Văn A</a>
                              <p class="des">Giảng viên trường Đại Học Hà Nội</p>
                               <p><i class="fas fa-minus"></i><span>Maria Doet</span></p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <script>
               $('.owl-carousel').owlCarousel({
                                    loop:true,
                                    margin:20,
                                    responsiveClass:true,
                                    autoplay:true,
                                    autoplayTimeout:7000,
                                    nav:true,
                                    responsive:{
                                        0:{
                                            items:1,
                                            nav:true
                                        },
                                        600:{
                                            items:1,
                                            nav:true
                                        },
                                        1000:{
                                            items:2,
                                            nav:true,
                                            loop:true
                                        }
                                    }
                                });
                                // $('.owl-prev').html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');
                                // $('.owl-next').html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
            </script>
         </div>
      </section>
      <!-- HOẠT ĐỘNG DẠY HỌC -->
      <section class="workLear">
         <div class="container">
            <div class="row">
               <div class="col-12 titleIndex">
                  <h3>Hoạt động dạy học</h3>
                  <p>Hoạt động dạy học trực tuyến</p>
                  <div class="borderTitle">
                     <div class="left"></div>
                     <div class="center"><i class="fas fa-graduation-cap"></i></div>
                     <div class="right"></div>
                  </div>      
               </div>
            </div>
            <div class="row">
               <div class="col-lg-4 col-md-6 col-sm-12 col-12 work">
                  <div class="itemwork">
                     <div class="img">
                        <div class="CropImg">
                           <div class="thumbs">
                              <a href="" title="">
                              <img src="images/dh1.jpg" alt="">
                              </a>
                           </div>
                        </div>
                        <div class="content">
                           <a href="" class="title">What is good about us?</a>
                          <p class="datetime"><b>Date:</b>20|<b>Category:</b>Campus|<b>Author:</b>Ateeq</p>
                           <p>Date:20 Desc2016 author ateeq conpany to poll befee is not simply... <a href="">Read More<i class="fas fa-angle-double-right"></i></a></p>
                          
                        </div>
                     </div>
                    
                  </div>
               </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-12 work">
                  <div class="itemwork">
                     <div class="img">
                        <div class="CropImg">
                           <div class="thumbs">
                              <a href="" title="">
                              <img src="images/dh2.jpg" alt="">
                              </a>
                           </div>
                        </div>
                        <div class="content">
                           <a href="" class="title">What is good about us?</a>
                           <p class="datetime"><b>Date:</b>20|<b>Category:</b>Campus|<b>Author:</b>Ateeq</p>
                           <p>Date:20 Desc2016 author ateeq conpany to poll befee is not simply... <a href="">Read More<i class="fas fa-angle-double-right"></i></a></p>
                          
                        </div>
                     </div>
                    
                  </div>
               </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-12 work">
                  <div class="itemwork">
                     <div class="img">
                        <div class="CropImg">
                           <div class="thumbs">
                              <a href="" title="">
                              <img src="images/dh3.jpg" alt="">
                              </a>
                           </div>
                        </div>
                        <div class="content">
                           <a href="" class="title">What is good about us?</a>
                          <p class="datetime"><b>Date:</b>20|<b>Category:</b>Campus|<b>Author:</b>Ateeq</p>
                           <p>Date:20 Desc2016 author ateeq conpany to poll befee is not simply...<a href="">Read More<i class="fas fa-angle-double-right"></i></a></p>
                           
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row reamore">
               <div class="col-12"><a href="" class="bghr-grey clhr-white">Read More... </a></div>
               
            </div>
         </div>
      </section>
      <!-- ĐỐI TÁC -->
      <section class="partner">
         <div class="container">
            <div class="row">
               <div class="col-12 titleIndex">
                  <h3>Đối tác của chúng tôi</h3>
                  <p>Hệ thống đối tác toàn quốc</p>
                  <div class="borderTitle">
                     <div class="left"></div>
                     <div class="center"><i class="fas fa-graduation-cap"></i></div>
                     <div class="right"></div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="owl-carousel">
                  <div class="item">
                     <div class="itempartner">
                        <div class="img">
                           <a href="" title="">
                           <img src="images/doitac1.png" alt="">
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="itempartner">
                        <div class="img">
                           <a href="" title="">
                           <img src="images/doitac2.png" alt="">
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="itempartner">
                        <div class="img">
                           <a href="" title="">
                           <img src="images/doitac3.png" alt="">
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
               <script>
                  $('.owl-carousel').owlCarousel({
                     loop:true,
                     margin:10,
                     center: true,
                     responsiveClass:true,
                     autoplay:true,
                     autoplayTimeout:7000,
                     nav:true,
                     responsive:{
                         0:{
                             items:1,
                             nav:true
                         },
                         600:{
                             items:2,
                             nav:true
                         },
                         1000:{
                             items:3,
                             nav:true,
                             loop:true
                         }
                     }
                 });
                 // $('.owl-prev').html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');
                 // $('.owl-next').html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
               </script>
            </div>
         </div>
      </section>
      <!-- FOOTER -->
     <?php include('common/footer.php')?>
   </body>
</html>