<!DOCTYPE html>
<html lang="en">
    <head>
      <title></title>
      <base href="">
      <meta name="ROBOTS" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="keywords" content="" />
      <meta name="description" content="">
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="geo.position" content="10.763945;106.656201" />
      <meta name="robots" content="noindex">
      <meta name="googlebot" content="noindex">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="" />
      <meta property="og:description" content="" />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="" />
      <meta property="og:image" content="" />
      <link type="image/x-icon" href="" rel="SHORTCUT ICON"/>
      <!-- CSS -->
      <link rel="stylesheet" href="css/bootstrap.min.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/animate.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/hover.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.carousel.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.theme.default.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/fontawesome-all.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/extract.css" media="all" type="text/css" />
      <!-- SCSS -->
      <link rel="stylesheet" href="scss/styles.css" media="all" type="text/css" />
      <link rel="stylesheet" href="scss/reponsive.css" media="all" type="text/css" />
      <!-- JS -->

     <link rel="stylesheet" href="css/style.css" media="all" type="text/css" />
      <script src="js/jquery3.1.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.bundle.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/transition.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
  
      <script src="js/WOW.js"></script>
   </head>
   <body>
      <!-- HEADER -->
      <?php include('common/header.php')?>
      <!-- /header -->
      <!-- SLIDER -->
      <section class="breadcrumb">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <h1>Blog của chúng tôi</h1>
                  <ul>
                     <li><a href="">Trang chủ</a></li>
                     <li>/</li>
                     <li><a href="">Tin tức</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </section>
      <section class="categoryNew new mg-40">
         <div class="container">
            <div class="row">
               <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                  <div class="itemctnew brbottom pd10">
                     <div class="img br pd5 brcl-grey">
                        <div class="CropImg CropImg30">
                           <div class="thumbs ">
                              <a href="" title="">
                              <img src="images/catenew1.jpg" alt="" class="">
                              </a>
                           </div>
                        </div>
                     </div>
                     <a href="" class="text-b f20 pd-15 ds-block clblack clhr-orang">Một số tiêu đề hay của bài viết mới của chúng tôi.</a>
                     <div class="des f14 clblack pdbottom20">
                        <p>Lorem ipsum dolor ngồi amet, consectetur adipiscing elit. Lorem ipsum dolor ngồi amet, consectetur adipiscing elit. Lorem ipsum dolor ngồi amet, consectetur adipiscing elit. Lorem ipsum dolor ngồi amet, consectetur adipiscing elit. Lorem ipsum dolor ngồi amet, consectetur adipiscing elit. Lorem ipsum dolor ngồi amet, consectetur adipiscing elit. Lorem ipsum dolor ngồi amet, consectetur adipiscing elit. Lorem ipsum dolor ngồi amet, consectetur adipiscing elit. </p>
                     </div>
                     <div class="tag">
                        <div class="w70 clblack  ds-in f14">
                           <strong>Tags :</strong>
                           <a href="" class="ds-inline bghr-orang mg-05 f14 clblack clhr-black">Something</a>
                        </div>
                        <div class="w25 clblack  ds-in f14 fl-rt text-rt">
                           <a href="" class="f14 cldenhat clhr-orang mg-05"><i class="fab fa-facebook-f"></i></a>
                           <a href="" class="f14 cldenhat clhr-orang mg-05"><i class="fab fa-twitter"></i></a>
                           <a href="" class="f14 cldenhat clhr-orang mg-05"><i class="fab fa-linkedin-in"></i></a>
                        </div>
                     </div>
                  </div>
                
               </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                  <div class="sideRight mgtop10">
                     <div class="itemRight pd10  br  brcl-xam mgbottom30">
                        <h3 class="f20 clblack br-bottom brcl-xam text-bnone pd-10 text-ca">Danh mục Bolg</h3>
                        <ul class="pd-10">
                           <li><a href="" class="f14 ds-block clgrey clhr-orang pd-05"><i class="fas fa-angle-double-right f12 pdright5"></i>Chăm sóc răng</a></li>
                        </ul>
                        
                     </div>
                     <div class="itemRight pd10  br  brcl-xam  mgbottom30">
                        <h3 class="f20 clblack br-bottom brcl-xam text-bnone pd-10 text-ca">Dịch vụ nha khoa</h3>
                        <ul class="pd-10">
                           <li><a href="" class="f14 ds-block clgrey clhr-orang pd-05"><i class="fas fa-angle-double-right f12 pdright5"></i>Chăm sóc răng</a></li>
                        </ul>
                        
                     </div>
                     <div class="itemRight pd10  br  brcl-xam  mgbottom30">
                        <h3 class="f20 clblack br-bottom brcl-xam text-bnone pd-10 text-ca">từ khóa tìm kiếm</h3>
                        <ul class="pd-10">
                           <li class="mg-5 ds-inline"><a href="" class="f14 ds-inline clblack pd-5 bgxam pd-07 clhr-white bghr-orang mg-05">Chăm sóc răng</a></li>
                           <li class="mg-5 ds-inline"><a href="" class="f14 ds-inline clblack pd-5 bgxam pd-07 clhr-white bghr-orang mg-05">Chăm sóc răng</a></li>
                        </ul>
                     </div>
                     <div class="itemRight pd10  br  brcl-xam  mgbottom30 mgtop10">
                        <h3 class="f20 clblack br-bottom brcl-xam text-bnone pd-10 text-ca">Bài viết phổ biến</h3>
                        <div class="itemnewRight cldenhat br-bottom brcl-xam">
                           <a href="" class="f14 clblack ds-block pd-10 text-b clhr-orang">Tiêu đề bài đăng mới của tôi xuất hiện</a>
                           <div class="img ds-inline">
                              <img src="images/itemn1.jpg" alt="" class=" w40 ds-in fl-lt mbw50">
                              <p class="ds-in pd-010 text-lt">Lorem ipsum dolor ngồi ametxsssssssssssss </p>
                           </div>

                      
                           <a href="" class="cldenhat clhr-orang ds-block pd-10">Đọc thêm ...</a>
                        </div>
                        
                     </div>
                  </div>
                </div>
            </div>
         </div>
      </section>
    
      
     
      
      <!-- FOOTER -->
      <?php include('common/footer.php')?>
   </body>
</html>