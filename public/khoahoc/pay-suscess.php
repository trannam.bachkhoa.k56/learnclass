<!DOCTYPE html>
<html lang="en">
    <head>
      <title></title>
      <base href="">
      <meta name="ROBOTS" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="keywords" content="" />
      <meta name="description" content="">
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="geo.position" content="10.763945;106.656201" />
      <meta name="robots" content="noindex">
      <meta name="googlebot" content="noindex">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="" />
      <meta property="og:description" content="" />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="" />
      <meta property="og:image" content="" />
      <link type="image/x-icon" href="" rel="SHORTCUT ICON"/>
      <!-- CSS -->
      <link rel="stylesheet" href="css/bootstrap.min.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/animate.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/hover.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.carousel.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.theme.default.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/fontawesome-all.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/extract.css" media="all" type="text/css" />
      <!-- SCSS -->
      <link rel="stylesheet" href="scss/styles.css" media="all" type="text/css" />
      <link rel="stylesheet" href="scss/reponsive.css" media="all" type="text/css" />
      <!-- JS -->

     <link rel="stylesheet" href="css/style.css" media="all" type="text/css" />
      <script src="js/jquery3.1.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.bundle.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/transition.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
  
      <script src="js/WOW.js"></script>
   </head>
   <body>
      <!-- HEADER -->
      <?php include('common/header.php')?>
      <!-- /header -->
      <!-- SLIDER -->
      
      <section class="resTeacher Payteacher bgxamnhat">
         <div class="container">
            <div class="row justify-content-lg-center">
               <div class="col-12">
                  <h2 class="clblack f24 pd-30 text-ct">
                     Thanh toán
                  </h2>
                 
               </div>
               <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                  <div class="listpay">
                    <ul>
                      <li class="ds-inline mg-010"><a href="" class="f16 text-up clorang activepay clorang clhr-orang text-b">Bước 2 : Đặt mua và hoàn tất</a></li>
                    </ul>
                  </div>
                  <div class="content mg-20">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link clorang" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Hướng dẫn đăng kí <span class="mbds-none">thanh toán tại chỗ (COD)</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link active clorang" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Đặt mua</a>
                    </li>
                   
                  </ul>
                  <div class="tab-content" id="myTabContent mgtop10">
                    <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
                      <p class="pd10 bgwhite clblack pdleft15">Bước 1: Nhập đầy đủ các thông tin của bạn vào mẫu trên
                      Bước 2: Schoolbus.vn sẽ gọi điện thoại để xác nhận đơn hàng.
                      Bước 3: Nhân viên của chúng tôi sẽ đến địa chỉ mà bạn đăng ký để nhận tiền (phí thu tiền: ~30.000)
                      Bước 4: Số tiền nộp sẽ được cộng vào tài khoản schoolbus.vn của bạn.
                      Toàn bộ quy trình trên sẽ diễn ra tối đa trong vòng 24 giờ làm việc.</p>
                      
                    </div>
                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                      <div class="bgwhite clblack pd10">
                        <p> - Số tiền thanh toán : <span class="ds-inline bggrey clwhite pdright30 pdleft10">100.000</span> VNĐ</p>

                        <form class="bgwhite pd15 mg-30" style="border: 1px solid #ddd;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .05);">
                     <div class="form-group row">
                         <label for="staticEmail" class="col-sm-12 col-form-label text-inline"><span class="text-b700">Chú ý : </span><span class="clred pd-05">(*)</span> là những trường bắt buộc</label>
                     </div>
                    <div class="form-group row">
                      <div class="form-group col-md-6">
                        <label for="staticEmail" class=""><span class="text-b700">Tên riêng</span><span class="clred pd-05">(*)</span></label>
                        <div class="">
                         <input type="text" class="form-control f14" id="inputPassword" placeholder="Tên riêng">
                        <!-- <p class="f14 text-inline clred pd-5 pdleft10 mgbottom0">Vui lòng nhập tên riêng</p> -->
                        </div>
                      </div>
                      <div class="form-group col-md-6">
                        <label for="staticEmail" class=""><span class="text-b700">Hòm thư điện tử</span><span class="clred pd-05">(*)</span></label>
                        <div class="">
                         <input type="text" class="form-control f14" id="inputPassword" placeholder="Tên riêng">
                        <!-- <p class="f14 text-inline clred pd-5 pdleft10 mgbottom0">Vui lòng nhập tên riêng</p> -->
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="form-group col-md-6">
                        <label for="staticEmail" class=""><span class="text-b700">Điện thoại</span><span class="clred pd-05">(*)</span></label>
                        <div class="">
                         <input type="text" class="form-control f14" id="inputPassword" placeholder="Tên riêng">
                        <!-- <p class="f14 text-inline clred pd-5 pdleft10 mgbottom0">Vui lòng nhập tên riêng</p> -->
                        </div>
                      </div>
                      <div class="form-group col-md-6">
                        <label for="staticEmail" class=""><span class="text-b700">Địa chỉ</span><span class="clred pd-05">(*)</span></label>
                        <div class="">
                         <input type="text" class="form-control f14" id="inputPassword" placeholder="Tên riêng">
                        <!-- <p class="f14 text-inline clred pd-5 pdleft10 mgbottom0">Vui lòng nhập tên riêng</p> -->
                        </div>
                      </div>
                    </div>
                      <div class="form-group row">
                      <div class="form-group col-md-6">
                        <label for="staticEmail" class=""><span class="text-b700">Thành phố</span><span class="clred pd-05">(*)</span></label>
                        <div class="">
                         <select id="inputState" class="form-control">
                          <option selected>Choose...</option>
                          <option>...</option>
                        </select>
                        </div>
                      </div>
                      <div class="form-group col-md-6">
                        <label for="staticEmail" class=""><span class="text-b700">Quận , huyện</span><span class="clred pd-05">(*)</span></label>
                        <div class="">
                        <select id="inputState" class="form-control">
                          <option selected>Choose...</option>
                          <option>...</option>
                        </select>
                        </div>
                      </div>
                    </div>
                   
                    <div class="form-group row">
                      <div class="form-group col-md-12">
                        <label for="staticEmail" class=""><span class="text-b700">Giới thiệu ngắn</span><span class="clred pd-05">(*)</span></label>
                        <div class="">
                         <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                    </div>
                    </div>
                   
                    <div class="form-group row">
                    
                      <div class="col-sm-12 pdtop10">
                      <button class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite ">Đặt mua</button>
                      </div>
                      
                    </div>
                  </form>
                      </div>
                    </div>
                  
                  </div>

                  </div>
               </div>
            </div>
         </div>
      </section>
    
     
      
      <!-- FOOTER -->
      <?php include('common/footer.php')?>
   </body>
</html>