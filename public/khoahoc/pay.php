<!DOCTYPE html>
<html lang="en">
    <head>
      <title></title>
      <base href="">
      <meta name="ROBOTS" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="keywords" content="" />
      <meta name="description" content="">
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="geo.position" content="10.763945;106.656201" />
      <meta name="robots" content="noindex">
      <meta name="googlebot" content="noindex">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="" />
      <meta property="og:description" content="" />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="" />
      <meta property="og:image" content="" />
      <link type="image/x-icon" href="" rel="SHORTCUT ICON"/>
      <!-- CSS -->
      <link rel="stylesheet" href="css/bootstrap.min.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/animate.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/hover.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.carousel.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.theme.default.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/fontawesome-all.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/extract.css" media="all" type="text/css" />
      <!-- SCSS -->
      <link rel="stylesheet" href="scss/styles.css" media="all" type="text/css" />
      <link rel="stylesheet" href="scss/reponsive.css" media="all" type="text/css" />
      <!-- JS -->

     <link rel="stylesheet" href="css/style.css" media="all" type="text/css" />
      <script src="js/jquery3.1.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.bundle.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/transition.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
  
      <script src="js/WOW.js"></script>
   </head>
   <body>
      <!-- HEADER -->
      <?php include('common/header.php')?>
      <!-- /header -->
      <!-- SLIDER -->
      
      <section class="resTeacher Payteacher bgxamnhat">
         <div class="container">
            <div class="row justify-content-lg-center">
               <div class="col-12">
                  <h2 class="clblack f24 pd-30 text-ct">
                     Thanh toán
                  </h2>
                 
               </div>
               <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                  <div class="listpay">
                    <ul>
                      <li class="ds-inline mg-010"><a href="" class="f16 text-up clorang activepay clorang clhr-orang text-b">Bước 1 : phương thức thanh toán</a></li>
                    
                    </ul>
                  </div>
                  <div class="content">
                    <p class="pd-10 mgbottom0 clgrey">Hãy chọn một trong các phương thức thanh toán dưới đây để tiếp tục.</p>
                    <div class="itempay mgbottom15">
                      <a href="" class="f16 ds-block"><i class="fas fa-shipping-fast clwhite bgorang ds-inline f30 pd20"></i>
                        <span class="contenpay clblack bgwhite ds-inline pd20 vertop ds-inline w45 lgw80 mbw100  br-bottom br2 brcl-orang clhr-orang">Giao khóa học và thu tiền tận nơi(COD)<span class="clwhite bgorang text-rt pd-2 pd-04 ds-inline">đề xuất</span>
                        </span>
                      </a>
                    </div>
                    <div class="itempay mgbottom15">
                      <a href="" class="f16 ds-block"><i class="fas fa-shipping-fast clwhite bgorang ds-inline f30 pd20"></i>
                        <span class="contenpay clblack bgwhite ds-inline pd20 vertop ds-inline w45 lgw80  mbw100 br-bottom br2 brcl-orang clhr-orang">Thanh toán trực tuyết bắng ATM<span class="clwhite bgorang text-rt pd-2 pd-04 ds-inline">đề xuất</span>
                        </span>
                      </a>
                    </div>
                    <div class="itempay mgbottom15">
                      <a href="" class="f16 ds-block"><i class="fas fa-shipping-fast clwhite bgorang ds-inline f30 pd20"></i>
                        <span class="contenpay clblack bgwhite ds-inline pd20 vertop ds-inline w45 lgw80  mbw100 br-bottom br2 brcl-orang clhr-orang">Chuyển khoản khách hàng<span class="clwhite bgorang text-rt pd-2 pd-04 ds-inline">đề xuất</span>
                        </span>
                      </a>
                    </div>

                  </div>
               </div>
            </div>
         </div>
      </section>
    
     
      
      <!-- FOOTER -->
      <?php include('common/footer.php')?>
   </body>
</html>