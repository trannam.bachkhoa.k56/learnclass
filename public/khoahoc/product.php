<!DOCTYPE html>
<html lang="en">
    <head>
      <title></title>
      <base href="">
      <meta name="ROBOTS" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="keywords" content="" />
      <meta name="description" content="">
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="geo.position" content="10.763945;106.656201" />
      <meta name="robots" content="noindex">
      <meta name="googlebot" content="noindex">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="" />
      <meta property="og:description" content="" />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="" />
      <meta property="og:image" content="" />
      <link type="image/x-icon" href="" rel="SHORTCUT ICON"/>
      <!-- CSS -->
      <link rel="stylesheet" href="css/bootstrap.min.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/animate.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/hover.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.carousel.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.theme.default.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/fontawesome-all.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/extract.css" media="all" type="text/css" />
      <!-- SCSS -->
      <link rel="stylesheet" href="scss/styles.css" media="all" type="text/css" />
      <link rel="stylesheet" href="scss/reponsive.css" media="all" type="text/css" />
      <!-- JS -->

     <link rel="stylesheet" href="css/style.css" media="all" type="text/css" />
      <script src="js/jquery3.1.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.bundle.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/transition.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
  
      <script src="js/WOW.js"></script>
   </head>
   <body>
      <!-- HEADER -->
      <?php include('common/header.php')?>
      <!-- /header -->
      <!-- SLIDER -->

      <section class="breadcrumb ds-inherit pd">
         <div class="bgbread">
            <div class="container">
               <div class="row">
                  <div class="col-12 pdtop10">
                     <h1 class="mbf20">Danh sách khóa học</h1>
                     <ul>
                        <li><a href="">Trang chủ</a></li>
                        <li>/</li>
                        <li><a href="">Danh sách khóa học</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>

   
      <section class="product pdtop50">
         <div class="container">
            <div class="row">
               <div class="col-lg-8 col-md-8 col-sm-12 mdpd">
                  <div class="content bgwhite pd20">
                     <div class="img">
                        <div class="CropImg CropImg60">
                           <div class="thumbs">
                              <a href="" title="">
                              <img src="images/product.jpg" alt="">
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row pdtop20 pdbottom20 bgwhite mg ">
                     <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="itemimg mbmgbottom10">
                           
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="itemimg mbmgbottom10">
                           
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <div class="itemimg mbmgbottom10">
                           
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-12">
                        <div class="titlepr ds-block text-ct">
                           <h2 class="pd20 clxanhdam f18 mg"><i class="fas fa-info-circle pdright5 clxanhdam"></i>Thông tin khóa học</h2>
                        </div>
                        <div class="contenttable">
                           <table class="table table-striped ">
                             <tbody class="text-up text-ct text-b">
                               <tr class="f16 mbf12">
                                
                                 <td class="w20 bggrey"><i class="fas fa-calendar-alt clwhite f28 vertop mgtop10 mbmgleft10 mbmgbottom5"></i>
                                    <span class="f15 mbf12 ds-inline pdleft10 mgtop8">
                                       <p class="mgbottom0 text-b700 clblack br-bottom brcl-black ">17h:00</p>
                                       <p class="date mgbottom0 clblack">20/6/2018</p>
                                    </span>
                                  </td>
                                 <td class="clxanhdam text-lt w55"><span class="pdleft10 pdtop10 ds-block">Buổi 1  : Giới thiệu học sinh</span></td>
                                 <td class="clwhite bggrey w25"><span class="pdtop10 ds-block">Đã hoàn thành</span></td>
                               </tr>
                               <tr class="f16 mbf12">
                               
                                 <td class="bgxanhlacay"><i class="far fa-question-circle clwhite f28 vertop mgtop10 mbmgleft10 mbmgbottom5"></i>
                                    <span class="f15 mbf12 ds-inline pdleft10 mgtop8">
                                       <p class="mgbottom0 text-b700 clblack br-bottom brcl-black ">17h:00</p>
                                       <p class="date mgbottom0 clblack">20/6/2018</p>
                                    </span>
                                 </td>
                                     <td class="clxanhdam text-lt w55"><span class="pdleft10 pdtop10 ds-block">Buổi 1  : Giới thiệu học sinh</span></td>
                                 <td class="clwhite bgred"><span class="pdtop10 ds-block">Đang diễn ra</span></td>
                               </tr>
                               <tr class="f16 mbf12">
                              
                                <td class="bgxanhlacay"><i class="fas fa-calendar-alt clwhite f28 vertop mgtop10 mbmgleft10 mbmgbottom5"></i>
                                    <span class="f15 mbf12 ds-inline pdleft10 mgtop8">
                                       <p class="mgbottom0 text-b700 clblack br-bottom brcl-black ">17h:00</p>
                                       <p class="date mgbottom0 clblack">20/6/2018</p>
                                    </span>
                                 </td>
                                      <td class="clxanhdam text-lt w55"><span class="pdleft10 pdtop10 ds-block">Buổi 1  : Giới thiệu học sinh</span></td>
                                 <td class="clwhite bgorang"><span class="pdtop10 ds-block">Đăng kí vào lớp</span></td>
                               </tr>
                             </tbody>
                           </table>
                        </div>

                        <div class="titlepr ds-block text-ct">
                           <h2 class="pd20 clxanhdam f18 mg"><i class="fas fa-info-circle pdright5 clxanhdam"></i>Thông tin khóa học</h2>
                        </div>
                        <div class="des mg bgwhite pd20">
                           <p class="clblack f16 text-js">
                              Tôi là Chiêu,
                              Có kinh nghiệm 3 năm làm thiết kế.
                              Là con người năng động,sáng tạo, chịu áp lực công việc lớn, làm việc đúng thời gian, dễ tính, có khả năng viết lách và vẽ tay, có trách nhiệm với công việc, có kinh nghiệm thiết kế logo ở đa dạng lĩnh vực như: Công nghệ, thời trang, nhà hàng, khách sạn, bất động sản, giáo dục, v.v.
                              Các thiết kế logo của tôi có thể sử dụng cho nhiều mục đích khác nhau như:
                              - Logo cho công ty, thương hiệu, sản phẩm, dịch vụ
                              - Logo cho một chương trình
                              - Logo đại diện của một tổ chức
                              Các dạng logo tôi thường thiết kế:
                              - Logo dạng chữ
                              - Logo dạng biểu tượng
                              - Logo dạng chữ kết hợp biểu tượng
                              - Logo dạng lời hay
                              Phong cách thiết kế của tôi tinh tế, sang trọng, hiện đại. Có khả năng điều chỉnh theo định hướng thiết kế mà khách hàng mong muốn, tạo ra mẫu logo thể hiện được tinh thần, tầm nhìn cho các sản phẩm, dịch vụ, thương hiệu của khách hàng.
                              Chi tiết bộ hướng dẫn sử dụng logo (nếu bạn yêu cầu thêm dịch vụ này):
                              - Phân tích ý nghĩa biểu tượng thương hiệu
                              - Quy định về màu sắc của logo
                              - Quy định về font chữ thương hiệu
                              - Quy chuẩn logo trên lưới
                              - Quy định khoảng cách an toàn/tối thiểu của logo
                           </p>
                        </div>
                        <div class="contentques bgques mgtop20 mgbottom20">         
                           <div class="titlepr titleqes ds-block text-ct">
                              <h2 class="pd20 clblack f18 mg">Câu hỏi thường gặp</h2>
                              <div class="brquesleft"></div>
                              <div class="brquesright"></div>
                           </div>
                           <div class="ques pd10">
                              <ul>
                                 <li class="br brcl-black pd8 mg-5">
                                    <a href="" class="f16 clblack clhr-orang">1.Tôi cần chuẩn bị những gì để tham gia khóa học ?</a>
                                 </li>
                                 <li class="br brcl-black pd8 mg-5">
                                    <a href="" class="f16 clblack clhr-orang">1.Tôi cần chuẩn bị những gì để tham gia khóa học ?</a>
                                 </li>
                              </ul>
                           </div>
                        </div>  
                     </div>


                  </div>
               </div>
               <!-- END LEFT  -->
               <div class="col-lg-4 col-md-4 col-sm-12 col-12 mdpd">
                  <div class="row">
                     <div class="col-12">
                        <div class="productRight bgwhite mg-020 mdmg-010 pd-20">
                           <p class="text-ct br-bottom brcl-black pdbottom10 mg-020 mgbottom20"><span class="clred f25 text-b700">1.000.000đ</span><span class="clgrey f14">/12 buổi</span></p>
                           <ul class="pdleft20 pdbottom20">
                              <li class="pd-5"><a href="" class="f16 lgf14 text-b clblack clhr-orang"><i class="far fa-check-square pdright5"></i>Đã tham gia: 11 Học sinh</a></li>
                              <li class="pd-5"><a href="" class="f16 lgf14 text-b clblack clhr-orang"><i class="far fa-check-square pdright5"></i>Môn học: Tiếng Anh</a></li>
                              <li class="pd-5"><a href="" class="f16 lgf14 text-b clblack clhr-orang"><i class="far fa-check-square pdright5"></i>Đối tượng học sinh: Lớp 7 -12</a></li>
                               <li class="pd-5"><a href="" class="f16 lgf14 text-b clblack clhr-orang"><i class="far fa-check-square pdright5"></i>Số buổi: 12 buổi</a></li>
                                <li class="pd-5"><a href="" class="f16 lgf14 text-b clblack clhr-orang"><i class="far fa-check-square pdright5"></i>Thời lượng: 75 phút/ buổi</a></li>
                           </ul>
                           <div class="buttonpr text-ct">
                              <a href="" class="clwhite bgorang f24 lgf18 mdf14 pd-5 pd-015 br-rs5 text-up br ds-inline clhr-orang bghr-white mgbottom10"><i class="fab fa-earlybirds pdright10"></i>Tham gia khóa học</a>
                              <a href="" class="f15 ds-block clhr-blue text-deup clxanhdam pdbottom2 text-b">Hướng dẫn đăng kí tham gia</a>
                           </div>
                        </div>


                         <div class="productRight bgwhite mg-020 mdmg-010 pd-20 mg-30">
                            <p class="text-ct br-bottom brcl-black pdbottom10 mg-020 mgbottom20"><span class=" f18 text-b700 clblack">Thông tin giáo viên</span></p>
                           <div class="lefttc w45 mdw100 ds-inline">
                             <div class="img text-rt pdright10 mbtext-ct">
                                <a href="" class="clorang"><img src="images/giaovien1.jpg" alt="" class="w80 br br-rs50 mbw50"></a>
                                <p class="text-ct mgleft20 f15 clblack text-b mgtop15 lgmgtop40 mdmgtop10 mdmgbottom5">Tổng số lớp học</p>
                                <p class="text-ct mgleft20 f15 clblack text-b mdmgbottom5">14</p>
                             </div>
                              
                           </div>
                           <div class="lefttc w45 mdw100 ds-inline pdtop10 vertop mdtext-ct">
                              <h3><a href="" class="clblack f14 text-bnone">Dante Nguyễn</a></h3>
                              <div class="icon clorang f16 lgf14 mg-10"><i class="fas fa-star mgright5 mgbottom10"></i><i class="fas fa-star mgright5"></i><i class="fas fa-star mgright5"></i><i class="fas fa-star mgright5"></i><i class="fas fa-star mgright5"></i>
                              </div>

                              <a href="" class="clwhite bgprxanh br br-blue bghr-white clhr-blue f16 pd-7 pd-015 br-rs5 lgpd-05 lgf12 mdpd-10 mdpd-015 ">Liên hệ trực tiếp</a>
                              <p class="text-ct mgleft20 f15 clblack text-b mgtop50 mgright30 lgpd lgmgleft0 lgmgright0 lgmgtop10" >Tổng số học sinh</p>
                              <p class="text-ct mgleft20 f15 clblack text-b mgright30">345</p>
                           </div>
                         </div>

                         <h2 class="clxanhdam f20 mgtop20 mdmg-010 mgbottom10 mgleft20 lgf16">Các lớp học của Dante Nguyễn</h2>
                        <!--  <div class="bgwhite mg-020 br-bottom brcl-xam itemPr mbw100">
                           <div class="itempr">
                              <div class="img w35 ds-inline vertop mgtop15">
                                  <div class="CropImg CropImg40 lgCropImg">
                                    <div class="thumbs">
                                       <a href="" title="">
                                       <img src="images/product1.jpg" alt="">
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div class="w60 text-lt ds-inline">
                                 <h4><a href="" class="clblack f14 clhr-orang text-bnone lgf12">Thiết kế bộ nhận diện thương hiệu chuyên nghiệp</a></h4>
                                 <p>
                                    <span class="f16 clxanhdam text-lt">1.990.000đ</span>
                                    <span class="text-rt f14 cldenhat fl-rt lgfl-lt lgpdbottom10"><span class="pdright15"><i class="fas fa-shopping-cart pdright5"></i>0</span> <span><i class="far fa-eye pdright5"></i>118</span></span>
                                 </p>
                              </div>
                           </div>
                         </div> -->

                          <div class="bgwhite mg-020 mdmg-010 br-bottom brcl-xam itemPr mbw100">
                           <div class="itempr">
                              <div class="img w35 ds-inline vertop mgtop15 mbmg5">
                                  <div class="CropImg CropImg40 lgCropImg">
                                    <div class="thumbs">
                                       <a href="" title="">
                                       <img src="images/product1.jpg" alt="" class="">
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div class="w60 text-lt ds-inline">
                                 <h4><a href="" class="clblack f14 clhr-orang text-bnone lgf12">Thiết kế bộ nhận diện thương hiệu chuyên nghiệp</a></h4>
                                 <p>
                                    <span class="f16 clxanhdam text-lt">1.990.000đ</span>
                                    <span class="text-rt f14 cldenhat fl-rt lgfl-lt lgpdbottom10"><span class="pdright15"><i class="fas fa-shopping-cart pdright5"></i>0</span> <span><i class="far fa-eye pdright5"></i>118</span></span>
                                 </p>
                              </div>
                           </div>
                         </div>

                          <div class="bgwhite mg-020 mdmg-010 br-bottom brcl-xam itemPr mbw100">
                           <div class="itempr">
                              <div class="img w35 ds-inline vertop mgtop15 mbmg5">
                                  <div class="CropImg CropImg40 lgCropImg">
                                    <div class="thumbs">
                                       <a href="" title="">
                                       <img src="images/product1.jpg" alt="">
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div class="w60 text-lt ds-inline">
                                 <h4><a href="" class="clblack f14 clhr-orang text-bnone lgf12">Thiết kế bộ nhận diện thương hiệu chuyên nghiệp</a></h4>
                                 <p>
                                    <span class="f16 clxanhdam text-lt">1.990.000đ</span>
                                    <span class="text-rt f14 cldenhat fl-rt lgfl-lt lgpdbottom10"><span class="pdright15"><i class="fas fa-shopping-cart pdright5"></i>0</span> <span><i class="far fa-eye pdright5"></i>118</span></span>
                                 </p>
                              </div>
                           </div>
                         </div>


                        <div class="bgwhite mg-20 mg-020 mdmg-010 text-ct br-bottom brcl-xam">
                           <p class="clblack f16 pdtop20 pdbottom10">Nhập số điện thoại để được tư vấn</p>
                           <div class="contefr text-ct w100 pd-050 lgpd-05">
                              <form action="" method="get" accept-charset="utf-8" class="w100 pdbottom20 lgpdbottom5 mbpdbottom20">
                               
                                <div class="input-group">
                                   <div class="input-group-prepend">
                                     <span class="input-group-text pd15 f16 lgpd5 lgf14" id="inputGroupPrepend2"><i class="fas fa-phone fasnguocicon clxanhlacay "></i></span>
                                   </div>
                                   <input type="text" class="form-control" id="validationDefaultUsername" placeholder="Username" aria-describedby="inputGroupPrepend2" required>
                                 </div>
                                   <button type="" class="pd-5 pd-015 f18 mgtop10 lgf16">Yêu cầu tư vấn</button>
                              </form>
                           </div>

                           
                        </div>

                        <div class="text-ct">
                           <p class="f16 clblack">Hoặc gọi cho chúng tôi <span class="clxanhlacay f22 text-b"><i class="fab fa-whatsapp pd-05"></i>096 160 4224</span></p>
                           <p class="f14 cldenhat">Hỗ trợ & Tư vấn 9h -18h , T2 - T6</p>
                        </div>

                     </div>
                  </div>
               </div>




            </div>
         </div>
      </section>
  
    
      <!-- FOOTER -->
      <?php include('common/footer.php')?>
   </body>
</html>