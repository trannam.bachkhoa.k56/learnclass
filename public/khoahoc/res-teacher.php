<!DOCTYPE html>
<html lang="en">
    <head>
      <title></title>
      <base href="">
      <meta name="ROBOTS" content="index, follow" />
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="keywords" content="" />
      <meta name="description" content="">
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="geo.position" content="10.763945;106.656201" />
      <meta name="robots" content="noindex">
      <meta name="googlebot" content="noindex">
      <meta property="og:type" content="website" />
      <meta property="og:title" content="" />
      <meta property="og:description" content="" />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="" />
      <meta property="og:image" content="" />
      <link type="image/x-icon" href="" rel="SHORTCUT ICON"/>
      <!-- CSS -->
      <link rel="stylesheet" href="css/bootstrap.min.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/animate.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/hover.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.carousel.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/owl.theme.default.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/fontawesome-all.css" media="all" type="text/css" />
      <link rel="stylesheet" href="css/extract.css" media="all" type="text/css" />
      <!-- SCSS -->
      <link rel="stylesheet" href="scss/styles.css" media="all" type="text/css" />
      <link rel="stylesheet" href="scss/reponsive.css" media="all" type="text/css" />
      <!-- JS -->

     <link rel="stylesheet" href="css/style.css" media="all" type="text/css" />
      <script src="js/jquery3.1.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.bundle.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/transition.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
  
      <script src="js/WOW.js"></script>
   </head>
   <body>
      <!-- HEADER -->
      <?php include('common/header.php')?>
      <!-- /header -->
      <!-- SLIDER -->
      
      <section class="resTeacher bgxamnhat">
         <div class="container">
            <div class="row justify-content-lg-center">
               <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                  <h2 class="clblack f24 pd-30 text-ct">
                     Đăng kí tài khoản giảng viên
                  </h2>
                  <form class="bgwhite pd15 mg-30" style="border: 1px solid #ddd;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .05);">
                     <div class="form-group row">
                         <label for="staticEmail" class="col-sm-12 col-form-label text-inline"><span class="text-b700">Chú ý : </span><span class="clred pd-05">(*)</span> là những trường bắt buộc</label>
                     </div>
                    <div class="form-group row">
                      <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Tên riêng</span><span class="clred pd-05">(*)</span></label>
                      <div class="col-sm-10">
                       <input type="text" class="form-control f14" id="inputPassword" placeholder="Tên riêng">
                      <!-- <p class="f14 text-inline clred pd-5 pdleft10 mgbottom0">Vui lòng nhập tên riêng</p> -->
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Mật khẩu</span><span class="clred pd-05">(*)</span></label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control f14" id="inputPassword" placeholder="Mật khẩu">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Giới thiệu ngắn</span><span class="clred pd-05">(*)</span></label>
                      <div class="col-sm-10">
                       <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Avatar</span><span class="clred pd-05">(*)</span></label>
                      <div class="col-sm-10">
                        <img src="images/avatar.png" alt="">
                        <input type="file" class="form-control f14" id="inputPassword" placeholder="">
                      </div>
                    </div>
                    <div class="form-group row">
                     <div class="col-sm-2"></div>
                      <div class="col-sm-10 pdtop30">
                      <button class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite ">Gửi duyệt</button>
                      </div>
                      
                    </div>
                  </form>
               </div>
            </div>
         </div>
      </section>
    
     
      
      <!-- FOOTER -->
      <?php include('common/footer.php')?>
   </body>
</html>