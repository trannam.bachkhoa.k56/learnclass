
// ......................................................
// ..................RTCMultiConnection Code.............
// ......................................................

function onHand(e) {
	var onHand = 'push_hand';

	connection.send(onHand);
	appendDIV(onHand);	
}
		
document.getElementById('inputQuestion').onkeyup = function(e) {
    if (e.keyCode != 13) return;

    // removing trailing/leading whitespace
    this.value = this.value.replace(/^\s+|\s+$/g, '');
    if (!this.value.length) return;

    connection.send(this.value);
		appendDIV(this.value);
		this.value = '';
	};
	
	var chatContainer = document.querySelector('.contentChat');
	var handContainer = document.querySelector('.hand');
	
	function appendDIV(event) {
		var div = document.createElement('div');
		// Nếu là trạng thái dơ tay
		if (event.data === 'push_hand' || event === 'push_hand') {
			//if (event.userid) {
				div.innerHTML = '<div class="item">' +
					'<i class="fa fa-hand-paper zoomAnimation clred mgright5"></i> <b>'+ (event.userid || 'Bạn') +'</b>' + ' đang giơ tay!' + '</div>';
				handContainer.append(div);
			$.notify((event.userid || 'Bạn') + ' đang giơ tay!',  {  title:"Thông báo lớp học"  });
			var audioElement = document.getElementById('audioNotification');
			audioElement.play();
			
			 setTimeout(function() {
				$('.hand').empty();
			}, 15000);
			//}
		} else {
			div.innerHTML = '<span style="color: '+getRandomColor(event.userid)+'">' + (event.userid || 'Bạn') + ':</span> ' + (event.data || event);
			chatContainer.append(div);
			div.tabIndex = 0;
			div.focus();
			var audioElement = document.getElementById('audioNotification');
			audioElement.play();
			$.notify((event.userid || 'Bạn')  + ' nói: ' + (event.data || event),  {  title:"Thông báo lớp học"  });
			document.getElementById('inputQuestion').focus();
		}
	}
	var colorUserIds = {};
	function getRandomColor(userID) {
	if (typeof  colorUserIds[userID] == 'undefined'){
	  var letters = '0123456789ABCDEF';
	  var color = '#';
	  for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	  }
	  
	  colorUserIds[userID] = color;
	  
	  return color;
	}
		return colorUserIds[userID];
	}
	
var connection = new RTCMultiConnection();

// Using getScreenId.js to capture screen from any domain
// You do NOT need to deploy Chrome Extension YOUR-Self!!
connection.getScreenConstraints = function(callback) {
    getScreenConstraints(function(error, screen_constraints) {
        if (!error) {
            screen_constraints = connection.modifyScreenConstraints(screen_constraints);
            callback(error, screen_constraints);
            return;
        }
        throw error;
    });
};
for(var i = 0; i < 40; i++) {
    $('.fz-'+i).attr('style',  'font-size: '+ i +'px');
}
// by default, socket.io server is assumed to be deployed on your own URL
connection.socketURL = 'https://server.phonghoctructuyen.com/';


connection.socketMessageEvent = 'audio-video-screen-demo';

connection.fileReceived = {};
connection.enableFileSharing = true;
connection.chunkSize = 60 * 1000;

connection.onopen = function() {
    //document.getElementById('share-file').disabled = false;
    //document.getElementById('input-text-chat').disabled = false;

    if (connection.isInitiator) {
        if (connection.selectedFile) {
            connection.send(connection.selectedFile, event.userid);
        }
        return;
    }

    //document.querySelector('input[type=file]').disabled = true;

    if (connection.isInitiatorConnected && connection.lastFile) {
        connection.send(connection.lastFile, event.userid);
    }

    if (!connection.isInitiatorConnected) {
        connection.initiatorId = event.userid;
        connection.isInitiatorConnected = true;
    }
};
// connection.session = {
    // audio: false,
    // video: true,
	// data: true
// };

// connection.mediaConstraints = {
    // audio: true,
    // video: true
// };

// connection.sdpConstraints.mandatory = {
    // OfferToReceiveAudio: true,
    // OfferToReceiveVideo: true
// };

var teacherName = $('#teacherName').val();
connection.videosContainer = document.getElementById('student-video');
connection.filesContainer = document.getElementById('file-container');
connection.onmessage = appendDIV;

var heightSharefile = $('#file-container').text().length;
var heightContentChat = $('.contentChat').text().length;
 setInterval(function() {
    var heightContentChatNew = $('.contentChat').text().length;
	if (heightContentChatNew > heightContentChat) {
		$('#notifyChat').empty();
		$('#notifyChat').append('<i class="fas fa-circle"></i>');
	}
	
	var heightSharefileNew = $('#file-container').text().length;
	if (heightSharefileNew > heightSharefile) {
		$('#notifyFile').empty();
		$('#notifyFile').append('<i class="fas fa-circle"></i>');
	}
}, 3000);
	

$('.hideNotify').click(function() {
	heightSharefile = $('#file-container').text().length;
	heightContentChat = $('.contentChat').text().length;
	$(this).find('.notify').empty();
});

$('#inputQuestion').keypress(function() { 
	heightContentChat = $('.contentChat').text().length;
	$('#notifyChat').empty();
	$(this).focus();
});


connection.onstream = function(event) {
    if(document.getElementById(event.streamid)) {
        var existing = document.getElementById(event.streamid);
        existing.parentNode.removeChild(existing);
    }
	console.log(event);
	var isStudent = false;
    var width = '100%';
	if (event.stream.isScreen && $('#videos-boardFirst').html()=== "") {
		$('#images-boardFirst').empty();
		
		connection.videosContainer = document.getElementById('videos-boardFirst');
	} 
	// else if (event.stream.isScreen && $('#videos-boardSecond').html()=== "") {
		// $('#images-boardSecond').empty();
		// connection.videosContainer = document.getElementById('videos-boardSecond');
	// } else if (event.stream.isScreen && $('#videos-boardThird').html()=== "") {
		// $('#images-boardThird').empty();
		// connection.videosContainer = document.getElementById('videos-boardThird');
	// } else if (event.stream.isScreen && $('#videos-boardFourth').html()=== "") {
		// $('#images-boardFourth').empty();
		// connection.videosContainer = document.getElementById('videos-boardFourth');
	// } else if (event.stream.isScreen && $('#videos-boardFifth').html()=== "") {
		// $('#images-boardFifth').empty();
		// connection.videosContainer = document.getElementById('videos-boardFifth');
		// indexVideo ++;
	// }
	else if (teacherName == event.userid  || ('j' + teacherName) == event.userid) {
		$('#teacher').empty();
        connection.videosContainer = document.getElementById('teacher');
    }  else {
		isStudent = true;
        width = '50%';
    }

    var width = parseInt(connection.videosContainer.clientWidth / 2) - 20;
	
	if (isStudent) {
		var mediaElement = getMediaElement(event.mediaElement, {
			title: event.userid,
			buttons: ['mute-audio', 'full-screen'],
			width: width,
			showOnMouseEnter: false
		});
		var userId = event.userid;
		connection.videosContainer = document.getElementById(userId);
		connection.videosContainer.innerHTML = "";
		connection.videosContainer.appendChild(mediaElement);	
		console.log(userId);
		var propertiesUser = userId.split('.');
		$('.'+propertiesUser[0]).find('.online').removeAttr('style');
		$('.'+propertiesUser[0]).find('.online').attr('style', 'color: green; font-size: 10px;');
		
	} else if (event.userID == 'screen_teacher') {
		connection.videosContainer = null;
	} else {
		console.log('test')
		var mediaElement = getMediaElement(event.mediaElement, {
			title: event.userid,
			buttons: ['full-screen'],
			width: width,
			showOnMouseEnter: false
		});
		connection.videosContainer.appendChild(mediaElement);
	}

    setTimeout(function() {
        mediaElement.media.play();
    }, 5000);

    mediaElement.id = event.streamid;
};
connection.onstreamended = function(event) {
    var mediaElement = document.getElementById(event.streamid);
    if(mediaElement) {

        mediaElement.parentNode.removeChild(mediaElement);
    }
};

setInterval(function(){ 
	if ($('#teacher').html() == '') {
		alert("Giáo viên hiện tại đang vắng mặt, vui lòng truy cập lại lớp học khi có giáo viên");
		location.reload();
	}
}, 10000);
// ......................................................
// ......................Handling Room-ID................
// ......................................................

(function() {
    var params = {},
        r = /([^&=]+)=?([^&]*)/g;

    function d(s) {
        return decodeURIComponent(s.replace(/\+/g, ' '));
    }
    var match, search = window.location.search;
    while (match = r.exec(search.substring(1)))
        params[d(match[1])] = d(match[2]);
    window.params = params;
})();

document.getElementById('teacherName').onkeyup = function() {
    localStorage.setItem(connection.socketMessageEvent, this.value);
};

var hashString = location.hash.replace('#', '');
if(hashString.length && hashString.indexOf('comment-') == 0) {
    hashString = '';
}

var roomid = params.roomid;
if(!roomid && hashString.length) {
    roomid = hashString;
}

if(roomid && roomid.length) {
    document.getElementById('teacherName').value = roomid;
    localStorage.setItem(connection.socketMessageEvent, roomid);

    // auto-join-room
    (function reCheckRoomPresence() {
        connection.checkPresence(roomid, function(isRoomExists) {
            if(isRoomExists) {
                connection.join(roomid);
                return;
            }

            setTimeout(reCheckRoomPresence, 5000);
        });
    })();

    disableInputButtons();
}

