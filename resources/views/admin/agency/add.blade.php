@extends('admin.layout.admin')

@section('title', 'Thêm mới đại lý')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới đại lý
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Đại lý</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('agency.store') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-12">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">THÔNG TIN ĐẠI LÝ</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên đại lý</label>
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Tên đại lý" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Số điện thoại</label>
                                    <input type="number" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Số điện thoại" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Địa chỉ</label>
                                    <input type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="Địa chỉ" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">% triết khấu</label>
                                    <input type="text" class="form-control" name="percent" value="{{ old('percent') }}"  placeholder="% triết khẩu" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mã đại lý</label>
                                    <input type="text" class="form-control" name="code" value="{{ old('code') }}" placeholder="Viết tắt tên đơn vị công tác. tên viết tắt của cá nhân" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Thông tin chuyển khoản</label>
                                    <textarea class="form-control" name="bank_information"  rows="10" cols="80">{{ old('bank_information') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('name'))
                                    <label for="exampleInputEmail1">{{ $errors->first('name') }}</label>
                                @endif
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('phone'))
                                    <label for="exampleInputEmail1">{{ $errors->first('phone') }}</label>
                                @endif
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('address'))
                                    <label for="exampleInputEmail1">{{ $errors->first('address') }}</label>
                                @endif
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('email'))
                                    <label for="exampleInputEmail1">{{ $errors->first('email') }}</label>
                                @endif
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('percent'))
                                    <label for="exampleInputEmail1">{{ $errors->first('percent') }}</label>
                                @endif
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('code'))
                                    <label for="exampleInputEmail1">{{ $errors->first('code') }}</label>
                                @endif
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('bank_information'))
                                    <label for="exampleInputEmail1">{{ $errors->first('bank_information') }}</label>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Thêm mới</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

