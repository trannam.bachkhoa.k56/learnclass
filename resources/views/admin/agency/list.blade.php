@extends('admin.layout.admin')

@section('title', 'Quản lý đại lý')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý lớp học
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý đại lý</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <a href="{{ route('agency.create') }}"><button class="btn btn-primary">Thêm mới</button></a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="agencies" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Thao tác</th>
                                <th>Tên đại lý</th>
                                <th>% triết khấu</th>
                                <th>Mã đại lý</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection
@push('scripts')
    <script>
        $(function() {
            $('#agencies').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('datatable_agency') !!}',
                columns: [
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                    { data: 'name', name: 'name', render: function ( data, type, row, meta ) {
                            var html = row.name + ' <i class="fa fa-info-circle"  data-toggle="collapse" href="#collapse'+row.agency_id+'" aria-expanded="false" aria-controls="collapseExample"></i>' +
                               '<div class="collapse" id="collapse'+row.agency_id+'">\n' +
                                '                        <div class="well">\n' +
                                '                            Email: '+ row.email +'<br>' +
                                '                            Số điện thoại: '+ row.phone +'\n' +
                                '                        </div>\n' +
                                '                    </div>';

                            return html;
                        }
                    },
                    { data: 'percent', name: 'percent' },
                    { data: 'code', name: 'code' , render: function ( data, type, row, meta ) {
                        var html = row.code + ' <i class="fa fa-info-circle"  data-toggle="collapse" href="#bankInformation'+row.agency_id+'" aria-expanded="false" aria-controls="collapseExample"></i>' +
                            '<div class="collapse" id="bankInformation'+row.agency_id+'">\n' +
                            '                        <div class="well">\n' +
                                                      row.bank_information +
                            '                        </div>\n' +
                            '                    </div>';

                            return html;
                        }
                    }
                ]
            });
        });
    </script>
@endpush
