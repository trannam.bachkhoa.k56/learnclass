@extends('admin.layout.admin')

@section('title', 'Chỉnh sửa Đánh giá lớp học')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chỉnh sửa Đánh giá lớp học
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Đánh giá lớp học</a></li>
            <li class="active">Chỉnh sửa</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('appraise-class.update', ['appraise_class_id' => $appraiseClass->appraise_class_id]) }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <div class="col-xs-12 col-md-12">
                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Học sinh đánh giá lớp học</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Sinh viên</label>
                                    <select class="form-control select2" name="student_id" required>
                                        @foreach($students as $student)
                                            <option value="{{ $student->id }}" @if($student->id == $appraiseClass->student_id) selected @endif>
                                                {{ $student->name }}-{{ $student->id }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Lóp học</label>
                                    <select class="form-control select2" name="classroom_id" required>
                                        @foreach($classrooms as $classroom)
                                            <option value="{{ $classroom->classroom_id }}" @if($classroom->classroom_id == $appraiseClass->classroom_id) selected @endif>
                                                {{ $classroom->name }}-{{ $classroom->classroom_id }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Điểm đánh giá</label>
                                    <input type="number" class="form-control" name="point" placeholder="Điểm đánh giá" value="{{ $appraiseClass->point }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nội dung</label>
                                    <textarea class="form-control" name="description" rows="4" cols="80"/>{{ $appraiseClass->description }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Ngày đánh giá:</label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" name="created_at" id="datepicker" value="{{ $appraiseClass->created_at }}" />
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Chỉnh sửa</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

