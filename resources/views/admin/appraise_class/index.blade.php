@extends('admin.layout.admin')

@section('title', 'Quản lý đánh giá học sinh')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý đánh giá học sinh
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý đánh giá học sinh</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <a href="{{ route('appraise-class.create') }}"><button class="btn btn-primary">Thêm mới</button></a>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <table id="products" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Thao tác</th>
                                <th>Học sinh</th>
                                <th>Lớp học</th>
                                <th>Điểm đánh giá</th>
                                <th>Nội dung</th>
                                <th>Thời gian</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection
@push('scripts')
    <script>
        $(function() {
            $('#products').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('datatable_appraise-classroom') !!}',
                columns: [
                    { data: 'appraise_class_id', name: 'appraise_class.appraise_class_id' },
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                    { data: 'student_name', name: 'user_student.name' },
                    { data: 'classroom_name', name: 'classroom.name' },
                    { data: 'point', name: 'appraise_class.point' },
                    { data: 'description', name: 'appraise_class.description' },
                    { data: 'created_at', name: 'appraise_class.created_at' }
                ]
            });
        });
    </script>
@endpush
