@extends('admin.layout.admin')

@section('title', 'Thêm mới thẻ thanh toán' )

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới tempates
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Thẻ thanh toán</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('cards.store') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-6">
    
                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->

                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mã thẻ</label>
                                    <input type="text" class="form-control" name="code" placeholder="Mã thẻ" value="{{ $randomCode }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Số tiền</label>
                                    <input type="text" class="form-control formatPrice" name="coin" placeholder="Số tiền" value="" required>
                                </div>
                                <script>
                                    $('.formatPrice').priceFormat({
                                        prefix: '',
                                        centsLimit: 0,
                                        thousandsSeparator: '.'
                                    });
                                </script>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Trạng thái sử dụng</label>
                                    <select name="is_use" class="form-control">
                                        <option value="0">Chưa sử dụng</option>
                                        <option value="1">Đã sử dụng</option>
                                    </select>
                                </div>

                                <div class="form-group" style="color: red;">
                                    @if ($errors->has('code'))
                                        <label for="exampleInputEmail1">{{ $errors->first('code') }}</label>
                                    @endif
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Thêm mới</button>
                            </div>
                    </div>
                    <!-- /.box -->

                </div>
            </form>
        </div>
    </section>
@endsection

