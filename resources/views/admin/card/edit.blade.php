@extends('admin.layout.admin')

@section('title', 'Chỉnh sửa '.$card->title )

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chỉnh sửa thẻ thanh toán {{ $card->title }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Thanh toán trực tuyến</a></li>
            <li class="active">Chỉnh sửa</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('cards.update', ['card_id' => $card->card_id]) }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <div class="col-xs-12 col-md-6">
                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã thẻ thanh toán</label>
                                <input type="text" class="form-control" name="code" placeholder="Tiêu đề"
                                       value="{{ $card->code }}" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Số tiền</label>
                                <input type="text" class="form-control formatPrice" name="coin" placeholder="Số tiền" value="{{ $card->coin }}" required>
                            </div>
                            <script>
                                $('.formatPrice').priceFormat({
                                    prefix: '',
                                    centsLimit: 0,
                                    thousandsSeparator: '.'
                                });
                            </script>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Trạng thái sử dụng</label>
                                <select name="is_use" class="form-control">
                                    <option value="0" {{ $card->is_use == 0 ? 'selected' : '' }}>Chưa sử dụng</option>
                                    <option value="1" {{ $card->is_use == 1 ? 'selected' : '' }}>Đã sử dụng</option>
                                </select>
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('code'))
                                    <label for="exampleInputEmail1">{{ $errors->first('code') }}</label>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                        </div>
                    </div>
                    <!-- /.box -->

                </div>
                
            </form>
        </div>
    </section>
@endsection

