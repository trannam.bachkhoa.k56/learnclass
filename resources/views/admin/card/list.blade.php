@extends('admin.layout.admin')

@section('title', 'Danh sách thẻ thanh toán' )

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thẻ thanh toán
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách thẻ thanh toán</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a  href="{{ route('cards.create') }}"><button class="btn btn-primary">Thêm mới</button> </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="cards" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th>Mã thẻ</th>
                                <th>Số tiền</th>
                                <th>user_id</th>
                                <th>Trạng thái</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')

@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/numeral.min.js') }}"></script>
    <script>
        $(function() {
            $('#cards').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('datatable_card') !!}',
                columns: [
                    { data: 'card_id', name: 'cards.card_id' },
                    { data: 'code', name: 'cards.code' },
                    { data: 'coin', name: 'cards.coin', render: function (data, type, row, meta) {
                         return numeral(data).format('0,0') + ' Xu'
                    }},
                    { data: 'user_id', name: 'cards.user_id' },
                    { data: 'is_use', name: 'cards.is_use', render: function (data, type, row, meta) {
                        console.log(data)
                        if (data == 1 && data) {
                            return 'Đã sử dụng';
                        }

                        return 'Chưa sử dụng';
                    }},
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                ]
            });
        });

    </script>
@endpush
