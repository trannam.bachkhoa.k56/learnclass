@extends('admin.layout.admin')

@section('title', 'Thêm mới lớp học')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới lớp học
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Lớp học</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('classroom.store') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-12">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">THÔNG TIN CƠ BẢN CỦA LỚP HỌC</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên lớp học</label>
                                    <input type="text" class="form-control" name="name" placeholder="Tên lớp học" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Đường dẫn</label>
                                    <input type="text" class="form-control" name="slug" placeholder="Đường dẫn">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mô tả</label>
                                    <textarea rows="4" class="form-control" name="description"
                                              placeholder=""></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mã lớp học</label>
                                    <input type="text" class="form-control" name="code" placeholder="Mã lớp học" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Lựa chọn giáo viên</label>
                                    <select class="form-control" name="teacher_id" required>
                                        @foreach($teachers as $teacher)
                                            <option value="{{ $teacher->teacher_id }}">{{ $teacher->name }}-{{ $teacher->teacher_id }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Giá</label>
                                    <input type="text" class="form-control formatPrice" name="price" placeholder="price" min="1">
                                </div>
                                <div class="form-group">
                                    <label>Ngày bắt đầu:</label>
                                    <div class="input-group">
                                        <input type="date" class="form-control" id="start" name="date_start" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Ngày kết thúc:</label>
                                    <div class="input-group">
                                        <input type="date" class="form-control" id="end" name="date_end" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="radio"  class="flat-red" name="is_approve" value="1" />
                                    Phê duyệt
                                    <input type="radio"  class="flat-red" name="is_approve" value="0" checked />
                                    Chưa đủ điều kiện phê duyệt
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Ảnh lớp học</label>
                                    <input type="button" onclick="return uploadImage(this);" value="Ảnh lớp học"
                                           size="20"/>
                                    <img src="" width="80" height="70"/>
                                    <input name="image" type="hidden" value=""/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <!-- Nội dung thêm mới -->
                                <div class="box box-primary boxCateScoll">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Chọn danh mục</h3>
                                    </div>
                                    <!-- /.box-header -->

                                    <div class="box-body">

                                        @foreach($categories as $cate)
                                            <div class="form-group">
                                                <label>
                                                    <input type="checkbox" name="parents[]" value="{{ $cate->category_id }}" class="flat-red">
                                                    {{ $cate->title }}
                                                </label>
                                            </div>
                                            @foreach($cate['sub_children'] as $child)
                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" name="parents[]" value="{{ $child['category_id'] }}" class="flat-red" >
                                                        {{ $child['title'] }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        @endforeach

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nội dung lớp học</label>
                                    <textarea class="editor" id="properties" name="content" rows="10" cols="80"></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Học viên:</label>
                                    <select class="select2 form-control " name="student_list[]" multiple="multiple">
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }} - {{ $user->id }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">FILES GIẢNG DẠY</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="input-group control-group increment" >
                            <input type="file" name="filename[]" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>  Thêm tệp</button>
                            </div>
                        </div>
                        <div class="clone hide">
                            <div class="control-group input-group" style="margin-top:10px">
                                <input type="file" name="filename[]" class="form-control">
                                <div class="input-group-btn">
                                    <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Xóa tệp</button>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">

                            $(document).ready(function() {

                                $(".btn-success").click(function(){
                                    var html = $(".clone").html();
                                    $(".increment").after(html);
                                });

                                $("body").on("click",".btn-danger",function(){
                                    $(this).parents(".control-group").remove();
                                });

                            });

                        </script>
                        <!-- /.box-body -->
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Thêm mới</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

