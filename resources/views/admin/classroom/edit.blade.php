@extends('admin.layout.admin')

@section('title', 'Chỉnh sửa lớp học '.$classroom->name)

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Chỉnh sửa lớp học {{ $classroom->name }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Lớp học</a></li>
            <li class="active">Chỉnh sửa</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('classroom.update', ['classroom_id' => $classroom->classroom_id]) }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <div class="col-xs-12 col-md-12">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">THÔNG TIN CƠ BẢN CỦA LỚP HỌC</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên lớp học</label>
                                    <input type="text" class="form-control" name="name" placeholder="Tên lớp học" value="{{ $classroom->name }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Đường dẫn</label>
                                    <input type="text" class="form-control" name="slug" placeholder="Đường dẫn" value="{{ $product->slug }}" />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mô tả</label>
                                    <textarea rows="4" class="form-control" name="description"
                                              placeholder="">{{ $product->description }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mã lớp học</label>
                                    <input type="text" class="form-control" name="code" placeholder="Mã lớp học" value="{{ $product->code }}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Lựa chọn giáo viên</label>
                                    <select class="form-control" name="teacher_id" required>
                                        @foreach($teachers as $teacher)
                                            <option value="{{ $teacher->teacher_id }}" @if($teacher->teacher_id == $classroom->teacher_id) selected @endif>
                                                {{ $teacher->name }}-{{ $teacher->teacher_id }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Giá</label>
                                    <input type="text" class="form-control formatPrice" name="price" placeholder="cost" min="1" value="{{ $product->price }}" />
                                </div>
                                <div class="form-group">
                                    <label>Ngày bắt đầu:</label>
                                    <div class="input-group">
                                        <input type="date" class="form-control" id="start" name="started_date" value="{{ $classroom->started_date }}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Ngày kết thúc:</label>
                                    <div class="input-group">
                                        <input type="date" class="form-control" id="ended_date" name="ended_date" value="{{ $classroom->ended_date }}" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label >
                                        <input type="radio"  class="flat-red" name="is_approve" value="1" @if($classroom->is_approve == 1) checked @endif/>
                                        Phê duyệt
                                    </label>
                                    <label>
                                        <input type="radio"  class="flat-red" name="is_approve" value="0" @if($classroom->is_approve == 0) checked @endif />
                                        Chưa đủ điều kiện phê duyệt
                                    </label>
                                    <br/>
                                    <i>Chú thích trạng thái phê duyệt</i>
                                    <textarea class="form-control" name="approve"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Ảnh lớp học</label>
                                    <input type="button" onclick="return uploadImage(this);" value="Ảnh lớp học"
                                           size="20"/>
                                    <img src="{{ $product->image }}" width="80" height="70"/>
                                    <input name="image" type="hidden" value="{{ $product->image }}"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="box box-primary boxCateScoll">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Chọn danh mục</h3>
                                    </div>
                                    <!-- /.box-header -->

                                    <div class="box-body">

                                        @foreach($categories as $cate)
                                            <div class="form-group">
                                                <label>
                                                    <input type="checkbox" name="parents[]" value="{{ $cate->category_id }}" class="flat-red"
                                                           @if(in_array($cate->category_id, $categoryPost)) checked @endif/>
                                                    {{ $cate->title }}
                                                </label>
                                            </div>
                                            @foreach($cate['sub_children'] as $child)
                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" name="parents[]" value="{{ $child['category_id'] }}" class="flat-red"
                                                               @if(in_array($child['category_id'], $categoryPost)) checked @endif/>
                                                        {{ $child['title'] }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        @endforeach

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nội dung lớp học</label>
                                    <textarea class="editor" id="properties" name="content" rows="10" cols="80">{{ $product->content }}</textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">FILES GIẢNG DẠY</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="input-group control-group increment" >
                            <input type="file" name="filename[]" value="{{ $classroom->files }}" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>  Thêm tệp</button>
                            </div>
                        </div>
                        <div class="clone hide">
                            <div class="control-group input-group" style="margin-top:10px">
                                <input type="file" name="filename[]" value="{{ $classroom->files }}" class="form-control">
                                <div class="input-group-btn">
                                    <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Xóa tệp</button>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">

                            $(document).ready(function() {

                                $(".btn-success").click(function(){
                                    var html = $(".clone").html();
                                    $(".increment").after(html);
                                });

                                $("body").on("click",".btn-danger",function(){
                                    $(this).parents(".control-group").remove();
                                });

                            });

                        </script>
                        <!-- /.box-body -->
                    </div>



                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Sửa</button>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <div class="col-xs-12 col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">DANH SÁCH HỌC VIÊN</h3>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="products" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Thao tác</th>
                                        <th>Họ và tên</th>
                                        <th>Số điện thoại</th>
                                        <th>Email</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $id => $user )
                                        <tr>
                                            <td>
                                                <a  href="{{ route('remove_student_classroom', ['classroomId' => $classroom->classroom_id, 'userId' => $user->id]) }}" class="btn btn-danger btnDelete" >
                                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->phone }}</td>
                                            <td>{{ $user->email }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Bổ sung học viên</h3>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <form action="{{ route('add_student_classroom') }}" method="post">
                                    {!! csrf_field() !!}
                                    <div class="form-group">
                                        <label>Học viên:</label>
                                        <select class="select2 form-control " name="student_list[]" multiple="multiple">
                                            @foreach($allUsers as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }} - {{ $user->id }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <input type="hidden" value="{{ $classroom->classroom_id }}" name="classroom_id"/>
                                    <button class="btn btn-primary" type="submit">Thêm mới</button>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>


                </div>
            </section>
        </div>
    </div>

    <div class="col-xs-12 col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">DANH SÁCH BUỔI HỌC</h3>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <table id="products" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Thao tác</th>
                                        <th>Giáo viên</th>
                                        <th>Thời gian bắt đầu buổi học</th>
                                        <th>Thời gian kết thúc buổi học</th>
                                        <th>Tên buổi học</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($lessons as $id => $lesson )
                                        <tr>
                                            <td>
                                                <a  href="{{ route('remove_lesson_classroom', ['classroomId' => $classroom->classroom_id]) }}" class="btn btn-danger btnDelete">
                                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                            <td>{{ $lesson->teacher_name }}</td>
                                            <td>{{ $lesson->time_start }}</td>
                                            <td>{{ $lesson->time_end }}</td>
                                            <td>{!! $lesson->title !!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Thêm mới buổi học</h3>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <form action="{{ route('add_lesson_classroom') }}" method="post">
                                    {!! csrf_field() !!}
                                    <div class="form-group">
                                        <label>Tên Buổi học:</label>
                                        <input type="text" name="title" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Thời gian bắt đầu:</label>
                                        <div class="input-group">
                                            <input type="time" class="form-control"  name="time_start" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Thời gian kết thúc:</label>
                                        <div class="input-group">
                                            <input type="time" class="form-control"  name="time_end" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Ngày:</label>
                                        <div class="input-group">
                                            <input type="date" class="form-control"  name="date_at" />
                                        </div>
                                    </div>
                                    <input type="hidden" value="{{ $classroom->classroom_id }}" name="classroom_id"/>
                                    <button class="btn btn-primary" type="submit">Thêm mới</button>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>


                </div>
            </section>
        </div>
    </div>

@endsection

