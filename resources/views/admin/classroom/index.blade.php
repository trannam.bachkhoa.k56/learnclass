@extends('admin.layout.admin')

@section('title', 'Quản lý Lớp học')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý lớp học
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý lớp học</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <a href="{{ route('classroom.create') }}"><button class="btn btn-primary">Thêm mới</button></a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="products" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Thao tác</th>
                                    <th>Tên lớp học</th>
                                    <th>Giáo viên</th>
                                    <th>Ảnh</th>
                                    <th>Danh mục</th>
                                    <th>Giá</th>
                                    <th>Trạng thái </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection
@push('scripts')
    <script>
        $(function() {
            $('#products').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('datatable_classroom') !!}',
                columns: [
                    { data: 'classroom_id', name: 'classroom.classroom_id' },
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                    { data: 'name', name: 'classroom.name' },
                    { data: 'teacher_name', name: 'users.name' },
                    { data: 'image', name: 'classroom.image', orderable: false,
                        render: function ( data, type, row, meta ) {
                            return '<div class=""><img src="'+data+'" width="50" /></div>';
                        },
                        searchable: false  },
                    { data: 'category_string', name: 'posts.category_string' },
                    { data: 'price', name: 'products.price' },
                    { data: 'is_approve', name: 'classroom.is_approve',
                        render: function ( data, type, row, meta ) {
                            if (data == 1) {
                                return 'Phê duyệt';
                            } else {
                                return 'Chưa phê duyệt';
                            }
                        }
                    }
                ]
            });
        });
    </script>
@endpush
