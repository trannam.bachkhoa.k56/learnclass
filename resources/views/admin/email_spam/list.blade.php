@extends('admin.layout.admin')

@section('title', 'Danh sách Email Spam' )

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Email Spam
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách Email Spam</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a  href="{{ route('email-spam.create') }}"><button class="btn btn-primary">Thêm mới</button> </a>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%">STT</th>
                                <th>Email</th>
                                <th>Thao tác</th>
                                <th>Kiểm tra email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($emailSpams as $id => $emailSpam )
                                <tr>
                                    <td>{{ ($id+1) }}</td>
                                    <td>{{ $emailSpam->email }}</td>
                                    <td>
                                        <a href="{{ route('email-spam.edit', ['email_spam_id' => $emailSpam->email_spam_id]) }}">
                                            <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                        </a>
                                        <a  href="{{ route('email-spam.destroy', ['email_spam_id' => $emailSpam->email_spam_id]) }}" class="btn btn-danger btnDelete" data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <button class="btn btn-primary" data-email="{{ $emailSpam->email }}" data-password="{{ $emailSpam->password }}" onclick="return checkEmail(this);" data-loading-text="Loading...">
                                            <i class="fa fa-check-square-o" aria-hidden="true"></i> kiểm tra email
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>STT</th>
                                <th>Email</th>
                                <th>Thao tác</th>
                            </tr>
                            </tfoot>
                        </table>
                        <script>

                            function checkEmail(e) {
                                var $btn = $(e).button('loading')
                                // business logic...


                                var dataEmail = $(e).attr('data-email');
                                var dataPassword = $(e).attr('data-password');

                                $.ajax({
                                    type: "get",
                                    url: '{!! route('email_spam_test') !!}',
                                    data: {
                                        email: dataEmail,
                                        password: dataPassword
                                    },
                                    success: function(data){
                                        var obj = jQuery.parseJSON(data);

                                        if (obj.status == 200) {
                                            alert('Gửi mail thành công');
                                            $btn.button('reset')
                                            return;
                                        }

                                        alert('Gửi mail không thành công, vui lòng cấu hình lại');
                                        $btn.button('reset')
                                    }
                                });
                            }
                        </script>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
@endsection

