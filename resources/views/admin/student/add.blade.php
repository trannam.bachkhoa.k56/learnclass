@extends('admin.layout.admin')

@section('title', 'Thêm mới học sinh')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới Học sinh
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Học sinh</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('student.store') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-12">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">THÔNG TIN CƠ BẢN CỦA HỌC SINH</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Họ và tên</label>
                                    <input type="text" class="form-control" name="name" placeholder="Họ và tên" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Số điện thoại</label>
                                    <input type="number" class="form-control" name="phone" placeholder="Số điện thoại" required>
                                </div>
                                <div class="form-group">
                                    <label>Ngày sinh:</label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" name="birthday" id="datepicker">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Địa chỉ</label>
                                    <input type="text" class="form-control" name="address" placeholder="Địa chỉ" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">email</label>
                                    <input type="email" class="form-control" name="email" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mật khẩu</label>
                                    <input type="text" class="form-control" name="password" placeholder="Mật khẩu" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Facebook</label>
                                    <input type="text" class="form-control" name="facebook" placeholder="Faceboook" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Ảnh avatar</label>
                                    <input type="button" onclick="return uploadImage(this);" value="Ảnh avatar"
                                           size="20"/>
                                    <img src="" width="80" height="70"/>
                                    <input name="image" type="hidden" value=""/>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mô tả bản thân</label>
                                    <textarea class="editor" id="properties" name="description" rows="10" cols="80"/></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                    <!-- Nội dung thêm mới -->
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">THÔNG TIN BỔ SUNG CỦA HỌC SINH</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Trường học</label>
                                    <input type="text" class="form-control" name="school" placeholder="Trường học" >
                                </div>

                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Thêm mới</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

