@extends('admin.layout.admin')

@section('title', 'Quản lý học sinh đánh giá giáo viên')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý học sinh đánh giá giáo viên
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý lớp học</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <a href="{{ route('student-appraise-teacher.create') }}"><button class="btn btn-primary">Thêm mới</button></a>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <table id="products" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Thao tác</th>
                                <th>Học sinh</th>
                                <th>Giáo viên</th>
                                <th>Điểm đánh giá</th>
                                <th>Nội dung</th>
                                <th>Thời gian</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection
@push('scripts')
    <script>
        $(function() {
            $('#products').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('datatable_student-appraise-teacher') !!}',
                columns: [
                    { data: 'student_appraise_teacher_id', name: 'student_appraise_teacher.student_appraise_teacher_id' },
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                    { data: 'student_name', name: 'user_student.name' },
                    { data: 'teacher_name', name: 'user_teacher.name' },
                    { data: 'point', name: 'student_appraise_teacher.point' },
                    { data: 'description', name: 'student_appraise_teacher.description' },
                    { data: 'created_at', name: 'student_appraise_teacher.created_at' }
                ]
            });
        });
    </script>
@endpush
