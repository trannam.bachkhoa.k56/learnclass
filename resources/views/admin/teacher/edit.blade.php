@extends('admin.layout.admin')

@section('title', 'Thêm mới giáo viên hoặc gia sư')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chỉnh sửa Giáo viên or Gia Sư
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Giáo viên or Gia Sư</a></li>
            <li class="active">Chỉnh sửa</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('teacher.update', ['teacher_id' => $teacher->teacher_id]) }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <div class="col-xs-12 col-md-12">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">THÔNG TIN CƠ BẢN CỦA GIÁO VIÊN</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Họ và tên</label>
                                    <input type="text" class="form-control" name="name" placeholder="Họ và tên" value="{{ $user->name }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Số điện thoại</label>
                                    <input type="number" class="form-control" name="phone" placeholder="Số điện thoại" value="{{ $user->phone }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Ngày sinh:</label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" name="birthday" id="datepicker" value="{{ $user->birthday }}">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Địa chỉ</label>
                                    <input type="text" class="form-control" name="address" placeholder="Địa chỉ" value="{{ $user->address }}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">email</label>
                                    <input type="email" class="form-control" name="email" placeholder="Email" value="{{ $user->email }}" required>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" name="is_change_password" value="1" class="flat-red"> Chọn nếu muốn thay đổi mật khẩu
                                    <label for="exampleInputEmail1">Mật khẩu</label>
                                    <input type="password" class="form-control" name="password" placeholder="Mật khẩu" value="{{ $user->password }}" />
                                </div>
                                <div class="form-group">
                                    <label >
                                        <input type="radio"  class="flat-red" name="is_approve" value="1" @if($teacher->is_approve == 1) checked @endif/>
                                        Phê duyệt
                                    </label>
                                    <label>
                                        <input type="radio"  class="flat-red" name="is_approve" value="0" @if($teacher->is_approve == 0) checked @endif />
                                        Chưa đủ điều kiện phê duyệt
                                    </label>
                                    <br/>
                                    <i>Chú thích trạng thái phê duyệt</i>
                                    <textarea class="form-control" name="approve"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">
                                        <input type="radio"  class="flat-red" name="certification" value="0" @if($teacher->certification == 0) checked @endif />
                                        Không chứng nhận
                                        <input type="radio"  class="flat-red" name="certification" value="1" @if($teacher->certification == 1) checked @endif/>
                                        Chứng nhận uy tín
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Facebook</label>
                                    <input type="text" class="form-control" name="facebook" placeholder="Faceboook" value="{{ $user->facebook }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Ảnh avatar</label>
                                    <input type="button" onclick="return uploadImage(this);" value="Ảnh avatar"
                                           size="20"/>
                                    <img src="{{ $user->image }}" width="80" height="70"/>
                                    <input name="image" type="hidden" value="{{ $user->image }}" />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mô tả bản thân</label>
                                    <textarea class="editor" id="properties" name="description" rows="10" cols="80"/>{{ $user->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                    <!-- Nội dung thêm mới -->
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">THÔNG TIN GIẢNG DẠY</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Đơn vị công tác</label>
                                    <input type="text" class="form-control" name="where_working" placeholder="Đơn vị công ty" value="{{ $teacher->where_working }}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Trình độ học vấn</label>
                                    <input type="text" class="form-control" name="academic_standard" placeholder="Trình độ học vấn" value="{{ $teacher->academic_standard }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Công việc hiện tại</label>
                                    <input type="text" class="form-control" name="current_job" placeholder="Học vấn" value="{{ $teacher->current_job }}">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Lĩnh vực giảng dạy</label>
                                    <select class="form-control" name="teaching_field">
                                        @foreach(\App\Entity\SubPost::showSubPost('linh-vuc-giang-day',1000) as  $id=> $field)
                                            <option value="{{ $field->title }}" @if($field->title == $teacher->teaching_field) selected @else @endif>{{ $field->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Hình thức giảng dạy</label>
                                    <input type="text" class="form-control" name="form_of_teaching" placeholder="Hình thức giảng dạy" value="{{ $teacher->form_of_teaching }}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Môn giảng dạy</label>
                                    <input type="text" class="form-control" name="teaching_subject" placeholder="Môn giảng dạy" value="{{ $teacher->teaching_subject }}" >
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">FILES GIẢNG DẠY</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="input-group control-group increment" >
                            <input type="file" name="filename[]" value="{{ $teacher->files }}" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>  Thêm tệp</button>
                            </div>
                        </div>
                        <div class="clone hide">
                            <div class="control-group input-group" style="margin-top:10px">
                                <input type="file" name="filename[]" value="{{ $teacher->files }}" class="form-control">
                                <div class="input-group-btn">
                                    <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Xóa tệp</button>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">

                            $(document).ready(function() {

                                $(".btn-success").click(function(){
                                    var html = $(".clone").html();
                                    $(".increment").after(html);
                                });

                                $("body").on("click",".btn-danger",function(){
                                    $(this).parents(".control-group").remove();
                                });

                            });

                        </script>
                        <!-- /.box-body -->
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

