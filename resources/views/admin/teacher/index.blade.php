@extends('admin.layout.admin')

@section('title', 'Quản lý giảng viên')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý giảng viên
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý giảng viên</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <a href="{{ route('teacher.create') }}"><button class="btn btn-primary">Thêm mới</button></a>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <table id="products" class="table table-bordered table-striped">
                            <thead>
                            <tr>

                                <th>ID</th>
                                <th>Thao tác</th>
                                <th>Họ và tên</th>
                                <th>Số điện thoại</th>
                                <th>Email</th>
                                <th>Ảnh </th>
                                <th>Trạng thái </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection
@push('scripts')
    <script>
        $(function() {
            $('#products').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('datatable_teacher') !!}',
                columns: [
                    { data: 'teacher_id', name: 'teacher.teacher_id' },
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                    { data: 'name', name: 'users.name' },
                    { data: 'phone', name: 'users.phone' },
                    { data: 'email', name: 'users.email' },
                    { data: 'image', name: 'users.image', orderable: false,
                        render: function ( data, type, row, meta ) {
                            return '<div class=""><img src="/'+data+'" width="50" /></div>';
                        },
                        searchable: false  },
                    { data: 'is_approve', name: 'teacher.is_approve',
                        render: function ( data, type, row, meta ) {
                            if (data == 1) {
                                return 'Phê duyệt';
                            } else {
                                return 'Chưa phê duyệt';
                            }
                        }
                    }

                ]
            });
        });
    </script>
@endpush
