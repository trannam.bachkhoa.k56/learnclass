@extends('admin.layout.admin')

@section('title', 'Quản lý giáo viên đánh giá học sinh')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý giáo viên đánh giá học sinh
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý giáo viên đánh giá học sinh</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <a href="{{ route('teacher-appraise-student.create') }}"><button class="btn btn-primary">Thêm mới</button></a>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <table id="products" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Thao tác</th>
                                <th>Giáo viên</th>
                                <th>Học sinh</th>
                                <th>Điểm đánh giá</th>
                                <th>Thời gian</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection
@push('scripts')
    <script>
        $(function() {
            $('#products').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('datatable_teacher-appraise-student') !!}',
                columns: [
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                    { data: 'teacher_name', name: 'user_teacher.name' },
                    { data: 'student_name', name: 'user_student.name' },
                    { data: 'point', name: 'student_appraise_teacher.point' },
                    { data: 'history', name: 'student_appraise_teacher.history' }
                ]
            });
        });
    </script>
@endpush
