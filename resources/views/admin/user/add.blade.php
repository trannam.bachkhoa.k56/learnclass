@extends('admin.layout.admin')

@section('title', 'Thêm mới người dùng' )

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới thành viên
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Thành viên</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('users.store') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-6">
    
                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->

                            <div class="box-body">
                                @if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Phân quyền</label>
                                    <select class="form-control" name="roles">
                                        <option value="1">Thành viên</option>
                                        <option value="2">Biên tập viên</option>
                                        <option value="3">Quản trị</option>
                                        <option value="4">Quản trị toàn hệ thống</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Quyền Active theme</label>
                                    <select class="form-control" name="vip">
                                        <option value="0">Active 1 theme</option>
                                        <option value="1">Active 2 theme</option>
                                        <option value="2">Active thoải mái</option>
                                    </select>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input type="email" class="form-control" name="email" placeholder="Email" required />
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Họ và tên</label>
                                    <input type="text" class="form-control" name="name" placeholder="Họ và tên" />
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Số điện thoại</label>
                                    <input type="text" class="form-control" name="phone" placeholder="Số điện thoại" />
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tiền trong tài khoản</label>
                                    <input type="number" class="form-control" name="coint" placeholder="Tiền trong tài khoản" />
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mã giới thiệu</label>
                                    <select class="form-control" name="code">
                                        @foreach ($agencies as $agency)
                                            <option value="{{ $agency->code }}">Giảm giá {{ $agency->percent }}%</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Trạng thái sử dụng mã giới thiệu</label>
                                    <select class="form-control" name="used_code">
                                        <option value="0">Chưa sử dụng</option>
                                        <option value="1">Đã sử dụng</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mật khẩu</label>
                                    <input type="text" class="form-control" name="password" placeholder="Mật khẩu" />
                                </div>

                                <div class="form-group">
                                    <input type="button" onclick="return uploadImage(this);" value="Chọn ảnh"
                                           size="20"/>
                                    <img src="" width="80" height="70"/>
                                    <input name="image" type="hidden" value=""/>
                                </div>
                                
                                <div class="form-group" style="color: red;">
                                    @if ($errors->has('email'))
                                        <label for="exampleInputEmail1">{{ $errors->first('email') }}</label>
                                    @endif
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Thêm mới</button>
                            </div>
                    </div>
                    <!-- /.box -->

                </div>
            </form>
        </div>
    </section>
@endsection

