@extends('admin.layout.admin')

@section('title', 'Chỉnh sửa '.$user->email )

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chỉnh sửa Thông tin thành viên {{ $user->email }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Cài đặt thông tin</a></li>
            <li class="active">Chỉnh sửa</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('users.update', ['id' => $user->id]) }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <div class="col-xs-12 col-md-6">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            @if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
                            <div class="form-group">
                                <label for="exampleInputEmail1">Phân quyền</label>
                                <select class="form-control" name="role">
                                    <option value="1" @if($user->role == 1) selected @endif>Thành viên</option>
                                    <option value="2" @if($user->role == 2) selected @endif>Biên tập viên</option>
                                    <option value="3" @if($user->role == 3) selected @endif>Quản trị</option>
                                    <option value="4" @if($user->role == 4) selected @endif>Quản trị toàn hệ thống</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Quyền Active theme</label>
                                <select class="form-control" name="vip">
                                    <option value="0" @if($user->vip == 0) selected @endif>Active 1 theme</option>
                                    <option value="1" @if($user->vip == 1) selected @endif>Active 3 theme</option>
                                    <option value="2" @if($user->vip == 2) selected @endif>Active thoải mái</option>
                                </select>
                            </div>
                            @endif
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Email" required value="{{ $user->email }}"/>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Họ và tên</label>
                                <input type="text" class="form-control" name="name" placeholder="Họ và tên" value="{{ $user->name }}" />
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại</label>
                                <input type="text" class="form-control" name="phone" placeholder="Số điện thoại" value="{{ $user->phone }}"/>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tiền trong tài khoản</label>
                                <input type="number" class="form-control" name="coint" placeholder="Tiền trong tài khoản"  value="{{ $user->coint }}"/>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã giới thiệu</label>
                                <select class="form-control" name="code">
                                    @foreach ($agencies as $agency)
                                        <option value="{{ $agency->code }}" {{  ($agency->code == $user->code) ? 'selected' : '' }}>Giảm giá {{ $agency->percent }}%</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Trạng thái sử dụng mã giới thiệu</label>
                                <select class="form-control" name="used_code">
                                    <option value="0" {{ $user->used_code == 0 ? 'selected' : '' }}>Chưa sử dụng</option>
                                    <option value="1" {{ $user->used_code == 1 ? 'selected' : '' }}>Đã sử dụng</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <input type="checkbox" name="is_change_password" value="1" class="flat-red"> Chọn nếu muốn thay đổi mật khẩu
                                <label for="exampleInputEmail1">Mật khẩu</label>
                                <input type="password" class="form-control" name="password" placeholder="Mật khẩu" value="{{ $user->password }}" />
                            </div>

                            <div class="form-group">
                                <input type="button" onclick="return uploadImage(this);" value="Chọn ảnh"
                                       size="20"/>
                                <img src="{{ $user->image }}" width="80" height="70"/>
                                <input name="image" type="hidden" value="{{ $user->image }}"/>
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('email'))
                                    <label for="exampleInputEmail1">{{ $errors->first('email') }}</label>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                        </div>
                    </div>
                    <!-- /.box -->

                </div>
                
            </form>
        </div>
    </section>
@endsection

