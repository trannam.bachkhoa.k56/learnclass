@extends('site.layout.site')

@section('title', 'Không có quyền truy cập vào đây')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')
    
@section('content')
	<center>
		<h1>Bạn không có quyền truy cập vào trang này. </h1>
		<p color="red">(nếu bạn đang vào học thì do giáo viên chưa vào lớp. Đúng giờ học bạn ấn lại tham gia lớp học để kiểm tra giáo viên vào lớp chưa.)</p>
	</center>
	
	<hr><center>VN3C</center>
@endsection