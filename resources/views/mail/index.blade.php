<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="main" style="max-width: 700px;padding: 0 15px; background: #fff;margin:0 auto;font-size: 14px;color: black;font-family: Helvetica,Arial,sans-serif;font-weight: normal;">
    @php $information = \App\Entity\InformationGeneral::getInformation(); @endphp
    <div class="header" style="width: 100%;border-bottom: 1px solid #ccc;line-height: 40px">
        <div class="conten" style="width: 49%;text-align: left;display: inline-block">
			<a href="https://phonghoctructuyen.com"><img style="width: 100%;" src="https://phonghoctructuyen.com/public/library/images/thontinchung/logonew.png" alt="" /></a>
        </div>
        <div class="right" style="width: 49% ;text-align: right;display: inline-block;">
            <div class="phone" style="font-size: 16px;">
                Holine:
                <span style="color:red;font-weight: bold;">{{ $information['phone']}}</span>
            </div>
        </div>
    </div>
    <div class="maincontent" style="text-align: left;width: 100%;margin:20px 0">
        <div class="content" style="width: 100%; background:#f0f0f0;padding: 10px;">
            <p>
                {!! $content !!}
            </p>
        </div>
    </div>

    <div class="footer" style="width: 100%;background: url('http://phonghoctructuyen.com/images/bannerft.jpg');background-size: cover;color:#fff;padding:0 10px">
        <div style="background: #0a0909b3;padding:0 10px">
            <div class="col3" style="width: 90%;display: inline-block; vertical-align: top; padding:10px 10px">
                <h3 style="text-transform: uppercase;">Chúng tôi là ai?</h3>
                <p style="text-align: justify">Trường học trực tuyến là đơn vị hàng đầu Việt Nam về lĩnh vực giáo dục, quy tụ các giáo viên hàng đầu về các lĩnh vực, ngành nghề, môn học các cấp. Chúng tôi cung cấp các phòng học trực tuyến chất lượng cao, tiêu chuẩn quốc tế. Hình ảnh chất lượng full HD</p>
            </div>
            <div class="col3" style="width: 90%;display: inline-block;vertical-align: top;padding: 10px 10px;">
                <h3 style="text-transform: uppercase;">THông tin liên hệ</h3>
                <ul style="list-style: none;padding: 0">
                    <li><a href="" title="" style="display: block;padding:7px 0;padding-top:0;color: #fff;text-decoration: none">Hot line: {{ $information['phone'] }}</a></li>
                    <li><a href="" title="" style="display: block;padding:7px 0;color: #fff;text-decoration: none">Địa chỉ : {{ $information['address'] }}</a></li>
                    <li><a href="" title="" style="display: block;padding:7px 0;color: #fff;text-decoration: none">Email: {{ $information['email'] }}</a></li>
                    <li><a href="" title="" style="display: block;padding:7px 0;color: #fff;text-decoration: none">Website: phonghoctructuyen.com</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

</body>
</html>