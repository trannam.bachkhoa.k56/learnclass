@extends('site.layout.site')

@section('title', 'Quản lý giáo viên')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    {{--//thu vien data table--}}
    {{--<link rel="stylesheet" href="{{ asset('adminstration/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">--}}


    {{--<script src="{{ asset('adminstration/datatables.net/js/jquery.dataTables.min.js') }}"></script>--}}
    {{--<script src="{{ asset('adminstration/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>--}}
<style>
    .statsTeacher span
    {
        display: inline-block;
        color: #fff;
        padding: 4px 8px;
        border-radius: 3px;
    }
    .statsTeacher .suscess
    {
        background: green;
    }
    .statsTeacher .error
    {
        background:red;
    }
</style>
    <?php
    $user = \Illuminate\Support\Facades\Auth::user();
    ?>

    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20">Người dùng {{ $user->name }}</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Người dùng {{ $user->name }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="teacher bggray pdbottom30">
        <div class="container">
            @include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-5 col-sm-12 col-12 sidebarContact">
                    @include('site.module_learnclass.menu_user')
                </div>
                <div class="col-lg-9 col-md-7 col-sm-12 col-12 bg-white">
                    <div class="teacherRight clblack text-lt">
                        <h2 class="text-ca f26 pdbottom15">Quản lý giáo viên</h2>

                        <div class="row management_Teacher ">
                            <div class="col-lg-12 col-md-12">
                                <a href="{{ route('manegerteacher.create') }}" class="ds-inline f15 clwhite bgorang text-up text-b700 pd-4 pd-08 mgright5 mbds-block mbmg-10 mbpd-10 hvr-shutter-out-vertical titleTeacher">
                                    Thêm mới giáo viên
                                </a>
                                <table class="table table-hover" id="manergerTeacher">
                                    <thead>
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col">ID</th>
                                        <th scope="col">Họ và tên</th>
                                        <th scope="col">Ảnh đại diện</th>
                                        <th scope="col">Hình thức giảng dạy</th>
                                        <th scope="col">Chuyên ngành</th>
                                        <th scope="col">Môn học</th>
                                        {{--<th scope="col">Trạng thái</th>--}}
                                        <th scope="col">Thao tác</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $page = 1;
                                    $page = isset($_GET['page']) ? $_GET['page'] : '1'
                                    ?>
                                    @foreach($manerger_teachers as $id=>$teacher)
                                    <tr>
                                    <th scope="row">{{ (($page - 1) * 15) + ($id + 1)  }}</th>
                                        <td>{{ isset($teacher['id']) ? $teacher['id'] : '' }}</td>
                                    <td>{{ isset($teacher['name']) ? $teacher['name'] : '' }}</td>
                                    <td><img src="{{ isset($teacher['image']) ? asset($teacher['image']) : asset('public/khoahoc/images/teacheravatar.png') }}"></td>
                                    <td>{{ isset($teacher['current_job']) ? $teacher['current_job'] : '' }}</td>
                                    <td>{{ isset($teacher['teaching_field']) ? $teacher['teaching_field'] : '' }}</td>
                                        <td>{{ isset($teacher['teaching_subject']) ? $teacher['teaching_subject'] : '' }}</td>
                                        {{--<td class="statsTeacher">--}}
                                           {{--@if($teacher['is_approve']  == 1)--}}
                                               {{--<span class="suscess">Đã phê duyệt</span>--}}
                                            {{--@else--}}
                                                {{--<span class="error">Chưa phê duyệt</span>--}}
                                            {{--@endif--}}
                                        {{--</td>--}}
                                    <td><a href="{{ route('manegerteacher.edit', ['teacher_id' => $teacher->teacher_id]) }}" title="Sửa"><i class="far fa-edit"></i></a>
                                    <a href="{{ route('manegerteacher.destroy', ['teacher_id' => $teacher->teacher_id]) }}" title="Xóa" class="" data-toggle="modal" data-target="#myModalDelete123" onclick="return submitDelete(this);"><i class="far fa-trash-alt" ></i></a>
                                        {{--<a  href="{{ route('manegerteacher.destroy', ['teacher_id' => $teacher->teacher_id]) }}" >--}}
                                            {{--<i class="fa fa-trash-o" aria-hidden="true"></i>aaaaaaaaaaaaaaaaaaaaaa--}}
                                        {{--</a>--}}

                                    </td>
                                     @endforeach



                                    </tbody>
                                </table>
                                {{ $manerger_teachers->links() }}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="myModalDelete123" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Bạn có chắc chắn muốn xóa?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                    <form action="" class="submitDelete" method="post" >
                        {!! csrf_field() !!}
                        {{ method_field('DELETE') }}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Xóa</button>
                        </div>
                    </form>

            </div>
        </div>
    </div>

    {{--<div class="modal fade" id="myModalDelete123" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">--}}
        {{--<div class="modal-dialog" role="document">--}}
            {{--<div class="modal-content_1">--}}
                {{--<div class="modal-header">--}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
                    {{--<h4 class="modal-title" id="myModalLabel">Bạn có chắc chắn muốn xóa?</h4>--}}
                {{--</div>--}}
                {{--<form action="" class="submitDelete" method="post" >--}}
                    {{--{!! csrf_field() !!}--}}
                    {{--{{ method_field('DELETE') }}--}}
                    {{--<div class="modal-footer">--}}
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>--}}
                        {{--<button type="submit" class="btn btn-primary">Xóa</button>--}}
                    {{--</div>--}}
                {{--</form>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection
<!-- Modal -->

<script>
    function submitDelete(e) {
        var url = $(e).attr('href');
        console.log(url);
        $('.submitDelete').attr('action', url);
        return false;
    }
</script>
<style>
    .modal-content_1 {
        background: #fff;
    }
</style>


{{--@push('scripts')--}}
    {{--<script>--}}
        {{--$(function() {--}}
            {{--$('#manergerTeacher').DataTable({--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--ajax: '{!! route('datatable_teacher_maneger') !!}',--}}
                {{--columns: [--}}
                    {{--{ data: 'teacher_id', name: 'teacher.teacher_id' },--}}
                    {{--{ data: 'action', name: 'action', orderable: false, searchable: false },--}}
                    {{--{ data: 'name', name: 'users.name' },--}}
                    {{--{ data: 'phone', name: 'users.phone' },--}}
                    {{--{ data: 'email', name: 'users.email' },--}}
                    {{--{ data: 'image', name: 'users.image', orderable: false,--}}
                        {{--render: function ( data, type, row, meta ) {--}}
                            {{--return '<div class=""><img src="/'+data+'" width="50" /></div>';--}}
                        {{--},--}}
                        {{--searchable: false  },--}}
                    {{--{ data: 'is_approve', name: 'teacher.is_approve',--}}
                        {{--render: function ( data, type, row, meta ) {--}}
                            {{--if (data == 1) {--}}
                                {{--return 'Phê duyệt';--}}
                            {{--} else {--}}
                                {{--return 'Chưa phê duyệt';--}}
                            {{--}--}}
                        {{--}--}}
                    {{--}--}}

                {{--]--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
{{--@endpush--}}
