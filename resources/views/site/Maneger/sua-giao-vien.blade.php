@extends('site.layout.site')

@section('title','Thông tin tài khoản')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')


    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20">Người dùng {{ $user->name }}</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Người dùng {{ $user->name }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="teacher bggray pdbottom30">
        <div class="container">
            @include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-12 sidebarContact">
                    @include('site.module_learnclass.menu_user')
                </div>
                <div class="col-lg-9 col-md-8 col-sm-12 col-12 bg-white">
                    <div class="teacherRight clblack text-lt mbpdleft0 mgtop20">
                        <h2 class="f26">Thông tin giáo viên </h2>
                        <form role="form" action="{{ route('manegerteacher.update', ['teacher_id' => $teacher->teacher_id]) }}" method="POST" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            {{ method_field('PUT') }}
                            <div class="col-xs-12 col-md-12">

                                <!-- Nội dung thêm mới -->
                                <div class="box box-primary">
                                    {{--<div class="box-header with-border">--}}
                                        {{--<h3 class="box-title">THÔNG TIN CƠ BẢN CỦA GIÁO VIÊN</h3>--}}
                                    {{--</div>--}}
                                    <!-- /.box-header -->

                                    <div class="box-body">
                                        <div class="col-xs-12col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Họ và tên</label>
                                                <input type="text" class="form-control" name="name" placeholder="Họ và tên" value="{{ $user->name }}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Số điện thoại</label>
                                                <input type="number" class="form-control" name="phone" placeholder="Số điện thoại" value="{{ $user->phone }}" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Ngày sinh:</label>

                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" name="birthday" id="datepicker" value="{{ $user->birthday }}">
                                                    {{--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">--}}
                                                    {{--<link rel="stylesheet" href="/resources/demos/style.css">--}}
                                                    {{--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--}}
                                                    {{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
                                                    {{--<script>--}}
                                                        {{--$( function() {--}}
                                                            {{--$( "#datepicker" ).datepicker();--}}
                                                        {{--} );--}}
                                                    {{--</script>--}}
                                                    {{--<input type="text" id="datepicker">--}}

                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Địa chỉ</label>
                                                <input type="text" class="form-control" name="address" placeholder="Địa chỉ" value="{{ $user->address }}" >
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">email</label>
                                                <input type="email" class="form-control" name="email" placeholder="Email" value="{{ $user->email }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <input type="checkbox" name="is_change_password" value="1" class="flat-red"> Chọn nếu muốn thay đổi mật khẩu
                                                <label for="exampleInputEmail1">Mật khẩu</label>
                                                <input type="password" class="form-control" name="password" placeholder="Mật khẩu" value="{{ $user->password }}" />
                                            </div>


                                        </div>
                                        <div class="col-xs-12col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Facebook</label>
                                                <input type="text" class="form-control" name="facebook" placeholder="Faceboook" value="{{ $user->facebook }}">
                                            </div>
                                            <div class="form-group row">
                                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Avatar</span><span class="clred pd-05"></span></label>
                                                <div class="col-lg-10 col-md-8 col-sm-8">
                                                    <img id="blah"  src="{{ !empty($user->image) ? asset($user->image) : asset('khoahoc/images/avatar.png') }}" alt="" width="150">
                                                    <button class="btn btn-default addAvatar">TẢI LÊN AVATAR</button>
                                                    <input type='file' id="imgInp" name="image" onchange="readURL(this)" style="display: none"/>
                                                    <input type="hidden" value="{{ $user->image }}" name="avatar" />
                                                    <script>
                                                        function readURL(input) {
                                                            if (input.files && input.files[0]) {
                                                                var reader = new FileReader();

                                                                reader.onload = function(e) {
                                                                    $('#blah').attr('src', e.target.result);
                                                                }

                                                                reader.readAsDataURL(input.files[0]);
                                                            }
                                                        }
                                                        $('.addAvatar').click(function() {
                                                            $('#imgInp').click();
                                                            return false;
                                                        });
                                                    </script>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Mô tả bản thân</label>
                                                <textarea class="editor" id="properties" name="description" rows="10" cols="80"/>{{ $user->description }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->

                                <!-- Nội dung thêm mới -->
                                <div class="box box-warning">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">THÔNG TIN GIẢNG DẠY</h3>
                                    </div>
                                    <!-- /.box-header -->

                                    <div class="box-body">
                                        <div class="col-xs-12col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Đơn vị công tác</label>
                                                <input type="text" class="form-control" name="where_working" placeholder="Đơn vị công ty" value="{{ $teacher->where_working }}" >
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Trình độ học vấn</label>
                                                <input type="text" class="form-control" name="academic_standard" placeholder="Trình độ học vấn" value="{{ $teacher->academic_standard }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Công việc hiện tại</label>
                                                <input type="text" class="form-control" name="current_job" placeholder="Học vấn" value="{{ $teacher->current_job }}">
                                            </div>
                                        </div>
                                        <div class="col-xs-12col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Lĩnh vực giảng dạy</label>
                                                <select class="form-control" name="teaching_field">
                                                    @foreach(\App\Entity\SubPost::showSubPost('linh-vuc-giang-day',1000) as  $id=> $field)
                                                        <option value="{{ $field->title }}" @if($field->title == $teacher->teaching_field) selected @else @endif>{{ $field->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Hình thức giảng dạy</label>
                                                <input type="text" class="form-control" name="form_of_teaching" placeholder="Hình thức giảng dạy" value="{{ $teacher->form_of_teaching }}" >
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Môn giảng dạy</label>
                                                <input type="text" class="form-control" name="teaching_subject" placeholder="Môn giảng dạy" value="{{ $teacher->teaching_subject }}" >
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>

                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">FILES GIẢNG DẠY</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="input-group control-group increment" >
                                        <input type="file" name="filename[]" value="{{ $teacher->files }}" class="form-control">
                                        <div class="input-group-btn">
                                            <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>  Thêm tệp</button>
                                        </div>
                                    </div>
                                    <div class="clone hide">
                                        <div class="control-group input-group" style="margin-top:10px">
                                            <input type="file" name="filename[]" value="{{ $teacher->files }}" class="form-control">
                                            <div class="input-group-btn">
                                                <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Xóa tệp</button>
                                            </div>
                                        </div>
                                    </div>

                                    <script type="text/javascript">

                                        $(document).ready(function() {

                                            $(".btn-success").click(function(){
                                                var html = $(".clone").html();
                                                $(".increment").after(html);
                                            });

                                            $("body").on("click",".btn-danger",function(){
                                                $(this).parents(".control-group").remove();
                                            });

                                        });

                                    </script>
                                    <!-- /.box-body -->
                                </div>


                                <div class="form-group row">

                                    <div class="col-lg-10 col-md-12 col-sm-12 pdtop30">
                                        <button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite ">Lưu lại thông tin</button>

                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(function () {
            $('#datepicker').datepicker({
                autoclose: true
            })
        });

@endsection
