@extends('site.layout.site')

@section('title','Thông tin tài khoản')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')


    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20">Người dùng {{ $user->name }}</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Người dùng {{ $user->name }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="teacher bggray pdbottom30">
        <div class="container">
            @include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-12 sidebarContact">
                    @include('site.module_learnclass.menu_user')
                </div>
                <div class="col-lg-9 col-md-8 col-sm-12 col-12 bg-white">
                    <div class="teacherRight clblack text-lt mbpdleft0 mgtop20">
                        <h2 class="f26">Thêm mới học sinh </h2>
                        <form role="form" action="{{ route('manegerstudent.store') }}" method="POST" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            {{ method_field('POST') }}
                            <div class="col-xs-12 col-md-12">

                                <!-- Nội dung thêm mới -->
                                <div class="box box-primary">
                                    @foreach ($errors->all() as $error)

                                        <span class="help-block">
												<strong style="color: red;">Lỗi: {{ $error }}</strong>
											</span>

                                @endforeach
                                    <!-- /.box-header -->

                                    <div class="box-body">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Họ và tên</label>
                                                <input type="text" class="form-control" name="name" placeholder="Họ và tên" required value="{{ old('name') }} ">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Số điện thoại</label>
                                                <input type="number" class="form-control" name="phone" placeholder="Số điện thoại" required value="{{ old('phone') }} ">
                                            </div>
                                            <div class="form-group">
                                                <label>Ngày sinh:</label>

                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" name="birthday" id="datepicker" value="{{ old('birthday') }} ">
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Địa chỉ</label>
                                                <input type="text" class="form-control" name="address" placeholder="Địa chỉ"  value="{{ old('address') }} ">
                                            </div>
                                            <div class="form-group row">
                                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Avatar</span><span class="clred pd-05"></span></label>
                                                <div class="col-lg-10 col-md-8 col-sm-8">
                                                    <img id="student"  src="{{ asset('khoahoc/images/avatar.png') }}" alt="" width="150">
                                                    <button class="btn btn-default addAvatar">TẢI LÊN AVATAR</button>
                                                    <input type='file' id="imgInp" name="image" onchange="readURL(this)" style="display: block"/>
                                                    <input type="hidden" value="" name="avatar" />
                                                    <script>
                                                        function readURL(input) {
                                                            if (input.files && input.files[0]) {
                                                                var reader = new FileReader();

                                                                reader.onload = function(e) {
                                                                    $('#student').attr('src', e.target.result);
                                                                }

                                                                reader.readAsDataURL(input.files[0]);
                                                            }
                                                        }
                                                        $('.addAvatar').click(function() {
                                                            $('#imgInp').click();
                                                            return false;
                                                        });
                                                    </script>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">email</label>
                                                <input type="email" class="form-control" name="email" placeholder="Email" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Mật khẩu</label>
                                                <input type="password" class="form-control" name="password" placeholder="Mật khẩu" value="{{ old('password') }} " />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Facebook</label>
                                                <input type="text" class="form-control" name="facebook" placeholder="Faceboook"  value="{{ old('facebook') }} ">
                                            </div>
                                            {{--<div class="form-group">--}}
                                                {{--<label for="exampleInputEmail1">Ảnh avatar</label>--}}
                                                {{--<input type="button" onclick="return uploadImage(this);" value="Ảnh avatar"--}}
                                                       {{--size="20"/>--}}
                                                {{--<img src="" width="80" height="70"/>--}}
                                                {{--<input name="image" type="hidden" value=""/>--}}
                                            {{--</div>--}}
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Mô tả bản thân</label>
                                                <textarea class="editor" id="properties" name="description" rows="10" cols="80"/>{{ old('description') }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->

                                <!-- Nội dung thêm mới -->
                                <div class="box box-warning">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">THÔNG TIN BỔ SUNG CỦA HỌC SINH</h3>
                                    </div>
                                    <!-- /.box-header -->

                                    <div class="box-body">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Trường học</label>
                                                <input type="text" class="form-control" name="school" placeholder="Trường học" value="{{ old('school') }} ">
                                            </div>

                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Thêm mới</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(function () {
            $('#datepicker').datepicker({
                autoclose: true
            })
        });

@endsection
