<footer>
  <div class="footerTop">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-12 col-sm-12 col-12 left">
          <p><span class="icon"><i class="fas fa-phone-volume"></i></span> Nếu bạn gặp khó khăn hãy gọi ngay cho chúng tôi <span class="phone">0934553435</span></p>
        </div>
        <div class="col-lg-5 col-md-12 col-sm-12 col-12 right">
          <a href="/trang/tang-thu-nhap-90" class="hvr-sweep-to-right">Đăng kí nhận hướng dẫn tăng <span>90%</span> thu nhập</a>
        </div>
      </div>
    </div>
  </div>
  <div class="footerContent">
    <div class="bgcontent">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="title">
              <h3>Chúng tôi là ai ?</h3>
              <div class="bordertitle"></div>
              <div class="borderf"></div>
            </div>
            <div class="content">
                {!! isset($information['chung-toi-la-ai']) ?$information['chung-toi-la-ai'] :'' !!}
            </div>
			<ul class="app">
				<li><img src="{{ asset('images/app-store.png') }}"/></li>
				<li><img src="{{ asset('images/google-play.png') }}"/></li>
			</ul>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="title">
              <h3>Hướng dẫn ?</h3>
              <div class="bordertitle"></div>
              <div class="borderf"></div>
            </div>
            <div class="contentul">
              <ul>
                @foreach (\App\Entity\Menu::showWithLocation('footer-first') as $Main_menu_footer1)
                  @foreach (\App\Entity\MenuElement::showMenuPageArray($Main_menu_footer1->slug) as $id=>$menu_footer1)
                    <li><a href="{{ $menu_footer1['url'] }}" title="{{ $menu_footer1['title_show'] }}">{{ $menu_footer1['title_show'] }}</a></li>
                  @endforeach
                @endforeach
              </ul>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="title">
              <h3>Hỏi đáp ?</h3>
              <div class="bordertitle"></div>
              <div class="borderf"></div>
            </div>
            <div class="contentul">
              <ul>
                @foreach (\App\Entity\Menu::showWithLocation('footer-second') as $Main_menu_footer2)
                  @foreach (\App\Entity\MenuElement::showMenuPageArray($Main_menu_footer2->slug) as $id=>$menu_footer2)
                    <li><a href="{{ $menu_footer2['url'] }}" title="{{ $menu_footer2['title_show'] }}">{{ $menu_footer2['title_show'] }}</a></li>
                  @endforeach
                @endforeach
              </ul>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="title">
              <h3>Thông tin liên hệ</h3>
              <div class="bordertitle"></div>
              <div class="borderf"></div>
            </div>
            <div class="contentullast">
              <ul>
                <li><a href=""><i class="fas fa-phone-volume"></i>Hotline : {{ isset($information['hot-line']) ?$information['hot-line'] : '' }}</a></li>
                <li><a href=""><i class="fas fa-map-marker-alt"></i>Địa chỉ : {{ isset($information['dia-chi']) ?$information['dia-chi'] : '' }}</a></li>
                <li><a href=""><i class="far fa-envelope"></i>Email : {{ isset($information['email']) ?$information['email'] : '' }}</a></li>
                <li><a href=""><i class="fab fa-internet-explorer"></i>Website : {{ isset($information['website']) ?$information['website'] : '' }}</a></li>
              </ul>
              <h4>Mạng xã hội</h4>
              <div class="icon"><i class="fab fa-facebook-square"></i><i class="fab fa-google-plus-g"></i><i class="fab fa-youtube"></i><i class="fab fa-twitter"></i></div>
                <!-- <img src="{{ !empty($information['anh-bo-cong-thuong']) ?  asset($information['anh-bo-cong-thuong']) : '' }}"  alt="logo"> -->
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="listfooter">
              <ul>
                @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
                  @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menu)
                    <li><a href="{{ $menu['url'] }}" title="{!! $menu['title_show'] !!}">{!! $menu['title_show'] !!}</a></li>
                    <li>|</li>
                  @endforeach
                @endforeach
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="copyring">
    {!! isset($information['copyright']) ?$information['copyright'] :'' !!}
  </div>
  <!-- Load Facebook SDK for JavaScript -->

</footer>

<div id="toTop" class="backtotop"><i class="fa fa-angle-up"></i>Top</div>
<script>
	//top
	 // Hide the toTop button when the page loads.
	 $("#toTop").css("display", "none");
	 
	 // This function runs every time the user scrolls the page.
	$(window).scroll(function(){
	 
		// Check weather the user has scrolled down (if "scrollTop()"" is more than 0)
		if($(window).scrollTop() > 0){
	 
			// If it's more than or equal to 0, show the toTop button.
			$("#toTop").fadeIn("slow");
	 
		}
		else {
			// If it's less than 0 (at the top), hide the toTop button.
			console.log("is less");
			$("#toTop").fadeOut("slow");
		}
	});
	 
	 
	// When the user clicks the toTop button, we want the page to scroll to the top.
	$("#toTop").click(function(){
	 
		// Disable the default behaviour when a user clicks an empty anchor link.
		// (The page jumps to the top instead of // animating)
		event.preventDefault();
	 
		// Animate the scrolling motion.
		$("html, body").animate({
			scrollTop:0
		},"slow");
	});

</script>

<style>.fb-livechat, .fb-widget{display: none}.ctrlq.fb-button, .ctrlq.fb-close{position: fixed; right: 24px; cursor: pointer}.ctrlq.fb-button{z-index: 999; background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEyOCAxMjgiIGhlaWdodD0iMTI4cHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjggMTI4IiB3aWR0aD0iMTI4cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxnPjxyZWN0IGZpbGw9IiMwMDg0RkYiIGhlaWdodD0iMTI4IiB3aWR0aD0iMTI4Ii8+PC9nPjxwYXRoIGQ9Ik02NCwxNy41MzFjLTI1LjQwNSwwLTQ2LDE5LjI1OS00Niw0My4wMTVjMCwxMy41MTUsNi42NjUsMjUuNTc0LDE3LjA4OSwzMy40NnYxNi40NjIgIGwxNS42OTgtOC43MDdjNC4xODYsMS4xNzEsOC42MjEsMS44LDEzLjIxMywxLjhjMjUuNDA1LDAsNDYtMTkuMjU4LDQ2LTQzLjAxNUMxMTAsMzYuNzksODkuNDA1LDE3LjUzMSw2NCwxNy41MzF6IE02OC44NDUsNzUuMjE0ICBMNTYuOTQ3LDYyLjg1NUwzNC4wMzUsNzUuNTI0bDI1LjEyLTI2LjY1N2wxMS44OTgsMTIuMzU5bDIyLjkxLTEyLjY3TDY4Ljg0NSw3NS4yMTR6IiBmaWxsPSIjRkZGRkZGIiBpZD0iQnViYmxlX1NoYXBlIi8+PC9zdmc+) center no-repeat #0084ff; width: 60px; height: 60px; text-align: center; bottom: 30px; border: 0; outline: 0; border-radius: 60px; -webkit-border-radius: 60px; -moz-border-radius: 60px; -ms-border-radius: 60px; -o-border-radius: 60px; box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16); -webkit-transition: box-shadow .2s ease; background-size: 80%; transition: all .2s ease-in-out}.ctrlq.fb-button:focus, .ctrlq.fb-button:hover{transform: scale(1.1); box-shadow: 0 2px 8px rgba(0, 0, 0, .09), 0 4px 40px rgba(0, 0, 0, .24)}.fb-widget{background: #fff; z-index: 1000; position: fixed; width: 360px; height: 435px; overflow: hidden; opacity: 0; bottom: 0; right: 24px; border-radius: 6px; -o-border-radius: 6px; -webkit-border-radius: 6px; box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -webkit-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -moz-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -o-box-shadow: 0 5px 40px rgba(0, 0, 0, .16)}.fb-credit{text-align: center; margin-top: 8px}.fb-credit a{transition: none; color: #bec2c9; font-family: Helvetica, Arial, sans-serif; font-size: 12px; text-decoration: none; border: 0; font-weight: 400}.ctrlq.fb-overlay{z-index: 0; position: fixed; height: 100vh; width: 100vw; -webkit-transition: opacity .4s, visibility .4s; transition: opacity .4s, visibility .4s; top: 0; left: 0; background: rgba(0, 0, 0, .05); display: none}.ctrlq.fb-close{z-index: 4; padding: 0 6px; background: #365899; font-weight: 700; font-size: 11px; color: #fff; margin: 8px; border-radius: 3px}.ctrlq.fb-close::after{content: "X"; font-family: sans-serif}.bubble{width: 20px; height: 20px; background: #c00; color: #fff; position: absolute; z-index: 999999999; text-align: center; vertical-align: middle; top: -2px; left: -5px; border-radius: 50%;}.bubble-msg{width: 120px; left: -125px; top: -8px; position: relative; background: rgba(59, 89, 152, .8); color: #fff; padding: 5px 8px; border-radius: 8px; text-align: center; font-size: 13px;}</style><div class="fb-livechat"> <div class="ctrlq fb-overlay"></div><div class="fb-widget"> <div class="ctrlq fb-close"></div><div class="fb-page" data-href="https://www.facebook.com/elearningphonghoctructuyen/" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false"> </div><div class="fb-credit"> <a href="https://vn3c.com" target="_blank">Powered by VN3C</a> </div><div id="fb-root"></div></div><a href="https://m.me/elearningphonghoctructuyen" title="Gửi tin nhắn cho chúng tôi qua Facebook" class="ctrlq fb-button"> <div class="bubble">1</div><div class="bubble-msg">Nhắn tin với chúng tôi!</div></a></div><script src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9"></script><script>$(document).ready(function(){function detectmob(){if( navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i) ){return true;}else{return false;}}var t={delay: 125, overlay: $(".fb-overlay"), widget: $(".fb-widget"), button: $(".fb-button")}; setTimeout(function(){$("div.fb-livechat").fadeIn()}, 8 * t.delay); if(!detectmob()){$(".ctrlq").on("click", function(e){e.preventDefault(), t.overlay.is(":visible") ? (t.overlay.fadeOut(t.delay), t.widget.stop().animate({bottom: 0, opacity: 0}, 2 * t.delay, function(){$(this).hide("slow"), t.button.show()})) : t.button.fadeOut("medium", function(){t.widget.stop().show().animate({bottom: "30px", opacity: 1}, 2 * t.delay), t.overlay.fadeIn(t.delay)})})}});</script>
