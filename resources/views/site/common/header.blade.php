@if ( \Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
		@php
		$user = \Illuminate\Support\Facades\Auth::user();
		$teacher = \App\Entity\Teacher::detailTeacher($user->id);
		if (isset($teacher->teacher_id)) {
			$lessons = \App\Entity\Classroom::startTeacherClassroomNow($teacher->teacher_id);
		} else {
			$lessons = null;
		}
	
		@endphp
		@if (!empty($lessons))
			@foreach ($lessons as $lesson)
			<div class="notificationTop pd20 mbds-none">
				<div class="note boxShadow pd20 position">
					<div class="closed">
						Tắt thông báo
					</div>
					<div class="f30">Khóa học <b>{{ $lesson['title'] }} </b> {{ $lesson['message'] }}.<a href="/phong-hoc/{{ $lesson['classroom_id'] }}/{{ $lesson['lesson_id'] }}" class="join blinkAni">Vào lớp ngay</a></div>
				</div>
			</div>
			@endforeach
		@endif
	@endif
	@if ( \Illuminate\Support\Facades\Auth::check())
		@php
			$user = \Illuminate\Support\Facades\Auth::user();
			$lessons = \App\Entity\Classroom::startStudentClassroomNow($user->id);
			$appraiseClassroom = \App\Entity\Classroom::appraiseClassroom($user->id);
		@endphp
		@if (!empty($lessons))
			@foreach ($lessons as $lesson)
			<div class="notificationTop pd20 mbds-none">
				<div class="note boxShadow pd20 position">
					<div class="closed">
						Tắt thông báo
					</div>
					<div class="f30">Khóa học <b>{{ $lesson['title'] }} </b> {{ $lesson['message'] }}.<a href="/hoc-sinh-vao-phong-hoc/{{ $lesson['classroom_id'] }}/{{ $lesson['lesson_id'] }}" class="join">Vào lớp ngay</a></div>
				</div>
			</div>
			@endforeach
		@endif

		@if (!empty($appraiseClassroom))
		<div class="notificationTop pd20 mbds-none">
				<div class="note boxShadow pd20 position">
					<div class="closed">
						Tắt thông báo
					</div>
					<div class="f30">Bạn đã học xong khóa học <b>{{ $appraiseClassroom['title'] }} </b>. vui lòng để lại đánh giá của bạn.
						<button type="button" class="join" data-toggle="modal" data-target="#appraiseClassroom{{ $appraiseClassroom['classroom_id'] }}">
							Đánh giá lớp học
						</button>
					</div>
				</div>
			</div>
			<!-- Button trigger modal -->

			<!-- Modal -->
			
		@endif
	@endif
	<script>
		$('.notificationTop .closed').click(function(){
			$('.notificationTop').hide();
		});
	</script>
<header id="header" class="" role="banner">
	
    <div class="headerTop hiddenMobile">
        <div class="container">
            <div class="row noPadding">
                <div class="hdleft col-lg-6 col-md-3 col-sm-12 col-12">
                    <ul>
                        <li> <i class="fas fa-phone-volume"></i> HotLine : <span>{{ isset($information['hot-line']) ?$information['hot-line'] : '' }}</span></li>
                        <li> <i class="fas fa-envelope"></i> Email : {{ isset($information['email']) ?$information['email'] : '' }}</li>
                    </ul>
                </div>
                <div class="hdright col-lg-6 col-md-12 col-sm-12 col-12 text-rt" >
                    <ul>
                        @if (\Illuminate\Support\Facades\Auth::check())
                            <li>
                                <a href="/trang/lua-chon-phuong-thuc-thanh-toan" title="" class="item text-de bghr-white "><i class="fab fa-amazon-pay"></i>Nạp tiền</a>
                            </li>
							<li>
                                <a href="/trang/khoa-hoc-da-dang-ky" title="" class="item text-de bghr-white "><i class="fa fa-laptop"></i>Khóa học của tôi</a>
                            </li>
							<li class="position clickShow notification">
								<a title="" onclick="return seenNotification(this)" class="item text-de bghr-white position"><i class="fa fa-bell"></i>
									{{--Bắt sự kiện click vào chuông thông báo mất số đếm thông báo--}}
									<script>
                                        function seenNotification(e) {
                                            // Khi click đọc thông báo thì cho số lượng thông báo về 0
                                            $('#ajax_countNoti').empty();
                                            // Gọi ajax để db xử lý dữ liệu cho status về 1
                                            $.ajax({
                                                url: '{!! route('seenNotification') !!}',
                                                method: 'get',
                                                success: function(data){
                                                },
                                                error: function(){},
                                            });

                                            return true;
                                        }
									</script>
									@if ( \Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
										@if(!empty(\App\Entity\Notification::countReport()))
											<span id="ajax_countNoti" class="noti">{{ \App\Entity\Notification::countReport() }}</span>
										@endif
									@endif
								</a>
								<div class="mainShow mainUser pd10 absolute text-lt">
									<h4>Thông báo</h4>
									<div class="hr mgtop10"></div>
									<ul class="listNoti">
										@if ( \Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
											@foreach(\App\Entity\Notification::showNotify() as $notify)
												<li @if($notify->status == 0 || $notify->status == 1) style="background: #edf2fa;" @else style="background: white;" @endif>
													<a id="{{ $notify->notify_id }}" href="{{ route('notificationClassroom', ['notifyId' => $notify->notify_id]) }}" onclick="return readNotification(this)"><i class="far fa-hand-point-right mgright5"></i>{{ $notify->content }}</a>
												</li>
											@endforeach
										@endif
											{{--Bắt sự kiện click vào thông báo đổi màu background--}}
											<script>
                                                function readNotification(e) {
                                                    var id = $(e).attr('id');
                                                    // Gọi ajax để db xử lý dữ liệu cho status về 2 (đã đọc)
                                                    $.ajax({
                                                        url: '{!! route('readNotification') !!}',
                                                        method: 'get',
                                                        data: {
                                                            id: id
                                                        },
                                                        success: function(data){
                                                        },
                                                        error: function(){},
                                                    });

                                                    return true;
                                                }
											</script>
									</ul>
									<div class="hr mgtop10"></div>
									<div class="readMore"><a href="/trang/thong-bao">Xem tất cả</a></div>
								</div>
                            </li>
                            <li class="position clickShow">
                                <a title="Tài khoản" class="item text-de bghr-white "><i class="fas fa-user-circle"></i>Tài khoản</a>
								<div class="mainShow mainUser pd10 absolute text-lt">
									<div class="clearfix">
										<div class="avatarCicle40 mgright10">
                                            <?php
                                            	$user = \Illuminate\Support\Facades\Auth::user();
                                            ?>
											<a href="/thong-tin-ca-nhan"><img src="{{ !empty($user->image) ? asset($user->image) : asset('khoahoc/images/avatar.png') }}"/></a>
										</div>
										<div class="avatarInfo40">
											<p><b>{{ \Illuminate\Support\Facades\Auth::user()->name }}</b><br>
												@if (\Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
													<span>Giáo viên</span>
													@else
													<span>Thành viên</span>
												@endif
											</p>
											<!--<p><a href="#">ID: {{ \Illuminate\Support\Facades\Auth::user()->id }}</a></p>-->
										</div>
									</div>
									<div class="clearfix f14">
										<p>Tài khoản: {{ number_format(\Illuminate\Support\Facades\Auth::user()->coint, 0, ',', '.') }} VNĐ</p>
										<a class="pay" href="/trang/lua-chon-phuong-thuc-thanh-toan" class="btn btn-warning">Nạp tiền</a>
										<a class="pay" href="/trang/kich-hoat-khoa-hoc" class="btn btn-warning" style="background: green">Nạp thẻ</a>


									</div>
									<div class="hr mgtop10"></div>
									<ul class="navUser">
										<li>
											<a href="/thong-tin-ca-nhan"><i class="fa fa-users-cog"></i> Thông tin tài khoản</a>
										</li>
										<li>
											<a href="/trang/khoa-hoc-da-dang-ky"><i class="fa fa-laptop"></i> Khóa học đã đăng ký</a>
										</li>
										<li>
											<a href="/trang/ma-gioi-thieu"><i class="far fa-laugh-beam"></i> Mã giới thiệu</a>
										</li>
										@if (\Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
										<li>
											<a href="/trang/trang-thong-tin-giao-vien"><i class="fas fa-chalkboard-teacher"></i> Khóa học của tôi</a>
										</li>
										@endif
											@if (\Illuminate\Support\Facades\Auth::user()->user_type != 'giao_vien')
											<li>
												<a href="/trang/dang-ky-giao-vien-khi-da-co-tai-khoan"><i class="fa fa-user-tie"></i>  Trở thành giảng viên</a>
											</li>
											@else
											<li>
												<a href="/thong-tin-ca-nhan"><i class="fa fa-user-clock"></i>  Tài khoản giảng viên</a>
											</li>
										@endif
										
										<li>
											<a href="/doi-mat-khau"><i class="fas fa-unlock-alt"></i>  Đổi mật khẩu</a>
										</li>
										<li>
											<a href="{{ route('logoutHome') }}"><i class="fa fa-sign-out-alt"></i>  Đăng xuất</a>
										</li>
									</ul>
								</div>
                            </li>
                            
                        @else
                            <li><a href="/trang/dang-ky-tro-thanh-giao-vien" title="" class="item text-de bghr-white "><i class="far fa-user-circle "></i>Trở thành giáo viên</a></li>
                            <li><a href="/trang/dang-ky" title="" class="item text-de bghr-white "><i class="far fa-user-circle "></i>Đăng kí</a></li>
                            <li><a href="/trang/dang-nhap" title="" class="item text-de bghr-white "><i class="fas fa-user-lock"></i>Đăng nhập</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
	<script>
		$('.clickShow').click(function(e){
			var show = $(this).find('.mainShow');
			if (show.is(":hidden")) {
				e.stopPropagation();
				$('.mainShow').hide();
				show.show();
			}
			else {
				show.hide();
			}
		});
		$('body').click(function(){
			$('.mainShow').hide();
		});
	</script>
    <!-- MENU -->
    <div class="hedermenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                    <div class="logo showMobile">
                        <a href="/" title="" class="logosidabar"><img src="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}"  alt="logo"></a>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-12 noPadding">
                    <div class="menuright mbds-none logoMbmobile">
                        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                            <a class="navbar-brand logo" href="/"><img src="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}"  alt="logo"></a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mr-auto">
                                    @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
                                        @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menu)
                                            <li class="nav-item active">
                                                <a  class="nav-link hvr-icon-bob hvr-bounce-to-bottom" href="{{ $menu['url'] }}" title="">
													<i class="{!! $menu['image'] !!} hvr-icon"></i><br>
													{!! $menu['title_show'] !!}
												</a>
                                            </li>
                                        @endforeach
                                    @endforeach
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <!-- menu mobile -->
                    <div class="menuMobileLeft ds-none mbds-block">
                    	<button type="button" class="drawer-toggle drawer-hamburger">
					      <span class="sr-only">toggle navigation</span>
					      <span class="drawer-hamburger-icon"></span>
					      <!--  <span class="fas fa-align-left f30"><i class="fas fa-users-cog f30"></i></span> -->
					      <!--   <ul class="drawer-menu drawer-menu--right"> -->
					    </button>
					     <nav class="drawer-nav" role="navigation">
					      <ul class="drawer-menu">
					      	<li class="logomobile"><a class="drawer-brand" href="./"><img src="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}"  alt="logo"></a></li>
					      	@foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
                                @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menu)
					        		<li><a class="drawer-menu-item" href="{{ $menu['url'] }}"><i class="{!! $menu['image'] !!} hvr-icon"></i> {!! $menu['title_show'] !!}</a></li>
								@endforeach
                            @endforeach
					       <!--  <li><a class="drawer-menu-item" href="./top.html">Top</a></li> -->
					       
					       
					         <!--  <li class="drawer-dropdown nav-item dropdown">
							        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							          Dropdown1111111
							        </a>
							        <div class="dropdown-menu drawer-dropdown-menu" aria-labelledby="navbarDropdown">
							          <a class="dropdown-item drawer-dropdown-menu-item" href="#">Action</a>
							          <a class="dropdown-item drawer-dropdown-menu-item" href="#">Another action</a>
							          <div class="dropdown-divider"></div>
							          <a class="dropdown-item" href="#">Something else here</a>
							        </div>
							   </li> -->
					     
					       
					      </ul>
					    </nav>
					    

                    </div>
                    <div class="menuMobileRight ds-none mbds-block">
                    	<button type="button" class="dropdowul">
					     <i class="fas fa-ellipsis-v"></i>
					    </button>
					    <div class="menuhidden" role="">
					    	<div class="icon text-ct">
						    	<span class="hiddenmenu fl-lt"><i class="fas fa-times f20 clblack"></i></span>
						    	 <a href="/" title="" class="ds-none mbds-inline text-ct"><img src="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}"  alt="logo"></a>
					    	 </div>
						     <ul class="ds-none mbds-block menumobileRight">
	                        @if (\Illuminate\Support\Facades\Auth::check())
	                            <li>
	                                <a href="/trang/lua-chon-phuong-thuc-thanh-toan" title="" class="item text-de bghr-white "><i class="fab fa-amazon-pay"></i>Nạp tiền</a>
	                            </li>
									 <li>
										 <a href="/trang/kich-hoat-khoa-hoc" title="" class="item text-de bghr-white "><i class="fab fa-amazon-pay" style="color: green"></i>Nạp thẻ</a>
									 </li>


								<li>
	                                <a href="/trang/khoa-hoc-da-dang-ky" title="" class="item text-de bghr-white "><i class="fa fa-laptop"></i>Khóa học của tôi</a>
	                            </li>
								<li class="position  clickShow notification">
									<a title="" onclick="return seenNotification(this)" class="item text-de bghr-white"><i class="fa fa-bell"></i> Thông báo
										<span class="changeicon"><i class="fas fa-plus-square clorang iconshow"></i>	</span>
										{{--Bắt sự kiện click vào chuông thông báo mất số đếm thông báo--}}
										<script>
	                                        function seenNotification(e) {
	                                            // Khi click đọc thông báo thì cho số lượng thông báo về 0
	                                            $('#ajax_countNoti').empty();
	                                            // Gọi ajax để db xử lý dữ liệu cho status về 1
	                                            $.ajax({
	                                                url: '{!! route('seenNotification') !!}',
	                                                method: 'get',
	                                                success: function(data){
	                                                },
	                                                error: function(){},
	                                            });
	                                            return true;
	                                        }
										</script>
										@if ( \Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
											@if(!empty(\App\Entity\Notification::countReport()))
												<span id="ajax_countNoti" class="noti">{{ \App\Entity\Notification::showNotify($teacher->teacher_id)->count() }}</span>
											@endif
										@endif
									</a>
									<div class="mainShow mainUser pd10  text-lt">
									
										
										<ul class="listNoti">
											@if ( \Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
												@foreach(\App\Entity\Notification::showNotify() as $notify)
													<li @if($notify->status == 0 || $notify->status == 1) style="background: #edf2fa;" @else style="background: white;" @endif>
														<a id="{{ $notify->notify_id }}" href="{{$notify->URL}}" onclick="return readNotification(this)"><i class="far fa-hand-point-right mgright5"></i>{{ $notify->content }}</a>
													</li>
												@endforeach
											@endif
												{{--Bắt sự kiện click vào thông báo đổi màu background--}}
												<script>
	                                                function readNotification(e) {
	                                                    var id = $(e).attr('id');
	                                                    // Gọi ajax để db xử lý dữ liệu cho status về 2 (đã đọc)
	                                                    $.ajax({
	                                                        url: '{!! route('readNotification') !!}',
	                                                        method: 'get',
	                                                        data: {
	                                                            id: id
	                                                        },
	                                                        success: function(data){
	                                                        },
	                                                        error: function(){},
	                                                    });

	                                                    return true;
	                                                }
												</script>
										</ul>
									
										<div class="readMore"><a href="/trang/thong-bao">Xem tất cả</a></div>
									</div>
	                            </li>
	                            <li class="position  clickShow">
	                                <a title="Tài khoản" class="item text-de bghr-white "><i class="fas fa-user-circle"></i>Tài khoản</a>
	                                
	                                <span class="changeicon"><i class="fas fa-plus-square clorang iconshow"></i>	</span>
	                                	

									<div class="mainShow mainUser pd10  text-lt">
										<div class="clearfix text-ct">
											<div class="avatarCicle40 mgright10">
	                                            <?php
	                                            	$user = \Illuminate\Support\Facades\Auth::user();
	                                            ?>
												<a href="/thong-tin-ca-nhan"><img src="{{ !empty($user->image) ? asset($user->image) : asset('khoahoc/images/avatar.png') }}"/></a>
											</div>
											<div class="avatarInfo40">
												<p><b>{{ \Illuminate\Support\Facades\Auth::user()->name }}</b><br>
													@if (\Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
														<span>Giáo viên</span>
														@else
														<span>Thành viên</span>
													@endif
												</p>
												<!--<p><a href="#">ID: {{ \Illuminate\Support\Facades\Auth::user()->id }}</a></p>-->
											</div>
										</div>
										<div class="clearfix f14 text-ct">
											Tài khoản: {{ number_format(\Illuminate\Support\Facades\Auth::user()->coint, 0, ',', '.') }} VNĐ
											<a class="pay" href="/trang/lua-chon-phuong-thuc-thanh-toan" class="btn btn-warning">Nạp tiền</a>
										</div>
										
										<ul class="navUser">
											<li>
												<a href="/thong-tin-ca-nhan"><i class="fa fa-users-cog"></i> Thông tin tài khoản</a>
											</li>
											<li>
												<a href="/trang/khoa-hoc-da-dang-ky"><i class="fa fa-laptop"></i> Khóa học đã đăng ký</a>
											</li>
											@if (\Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
											<li>
												<a href="/trang/trang-thong-tin-giao-vien"><i class="fas fa-chalkboard-teacher"></i> Phòng học của tôi</a>
											</li>
											@endif
												@if (\Illuminate\Support\Facades\Auth::user()->user_type != 'giao_vien')
												<li>
													<a href="/trang/dang-ky-giao-vien-khi-da-co-tai-khoan"><i class="fa fa-user-tie"></i>  Trở thành giảng viên</a>
												</li>
												@else
												<li>
													<a href="/thong-tin-ca-nhan"><i class="fa fa-user-clock"></i>  Tài khoản giảng viên</a>
												</li>
											@endif
											
											<li>
												<a href="/doi-mat-khau"><i class="fas fa-unlock-alt"></i>  Đổi mật khẩu</a>
											</li>
											<li>
												<a href="{{ route('logoutHome') }}"><i class="fa fa-sign-out-alt"></i>  Đăng xuất</a>
											</li>
										</ul>
									</div>
	                            </li>
	                            
	                        @else
	                            <li><a href="/trang/dang-ky-tro-thanh-giao-vien" title="" class="item text-de bghr-white "><i class="far fa-user-circle "></i>Trở thành giáo viên</a></li>
	                            <li><a href="/trang/dang-ky" title="" class="item text-de bghr-white "><i class="far fa-user-circle "></i>Đăng kí</a></li>
	                            <li><a href="/trang/dang-nhap" title="" class="item text-de bghr-white "><i class="fas fa-user-lock"></i>Đăng nhập</a></li>
	                        @endif
	                    </ul>
	                    <script>
							//click show-hideen ul li tai khoan
							$(document).ready(function () {
								$('ul.menumobileRight>li.clickShow').click(function () {								
									if ($(this).hasClass('active')) {
										$(this).find('.mainShow').slideUp(1000);
										//$(this).find('i').css('transform','rotate(360deg)');
										$(this).find('i').css('transition', 'all 1s ease');
										$(this).removeClass('active');
										$(this).addClass('color');
										$(this).find('span.changeicon').html('<i class="fas fa-plus-square clorang iconshow"></i>');
										
									}
									else {
										$(this).find('.mainShow').slideDown(1000);
										$(this).addClass('active');
										$(this).find('i').css('transition', 'all 1s ease');
										$(this).find('span.changeicon').html('<i class="fas fa-minus-square clorang iconshow"></i>');
										
										// $(this).find('span').addClass('fa-minus-square');
									}
								});
							});
						</script>
	                    <!-- click -hideen jquery --> 
						<script>
							$(document).ready(function () {
								$('.dropdowul').click(function(){
									$('.menuhidden').css('width',300);	
									$('.drawer').drawer('close');
									$('body').css('background-color','rgba(0,0,0,.2)');

								});	
								$('.hiddenmenu').click(function(){
									$('.menuhidden').css('width',0);
									$('body').css('background-color','white');
								});

							});
							$(document).mouseup(function(e) 
								{
								    var container = $(".menuhidden");
								    // if the target of the click isn't the container nor a descendant of the container
								    if (!container.is(e.target) && container.has(e.target).length === 0) 
								    {
								        $('.menuhidden').css('width',0);
								        $('body').css('background-color','white');
								    }
								});
						</script>


						<script>
					    $(document).ready(function() {
					      $('.drawer').drawer();
					    });
					  </script>
						


					    </div>
                    </div>
               
                    <!-- end menu mobile -->
                </div>
            </div>

        </div>
    </div>
</header>






@if ( \Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
		@php
		$user = \Illuminate\Support\Facades\Auth::user();
		$teacher = \App\Entity\Teacher::detailTeacher($user->id);
		if (isset($teacher->teacher_id)) {
			$lessons = \App\Entity\Classroom::startTeacherClassroomNow($teacher->teacher_id);
		} else {
			$lessons = null;
		}
	
		@endphp
		@if (!empty($lessons))
			@foreach ($lessons as $lesson)
			<div class="notificationTop pd20 ds-none mbds-block">
				<div class="note boxShadow pd20 position">
					<div class="closed">
						Tắt thông báo
					</div>
					<div class="f30">Khóa học <b>{{ $lesson['title'] }} </b> {{ $lesson['message'] }}.<a href="/phong-hoc/{{ $lesson['classroom_id'] }}/{{ $lesson['lesson_id'] }}" class="join blinkAni">Vào lớp ngay</a></div>
				</div>
			</div>
			@endforeach
		@endif
	@endif
	@if ( \Illuminate\Support\Facades\Auth::check())
		@php
			$user = \Illuminate\Support\Facades\Auth::user();
			$lessons = \App\Entity\Classroom::startStudentClassroomNow($user->id);
			$appraiseClassroom = \App\Entity\Classroom::appraiseClassroom($user->id);
		@endphp
		@if (!empty($lessons))
			@foreach ($lessons as $lesson)
			<div class="notificationTop pd20 ds-none mbds-block">
				<div class="note boxShadow pd20 position">
					<div class="closed">
						Tắt thông báo
					</div>
					<div class="f30">Khóa học <b>{{ $lesson['title'] }} </b> {{ $lesson['message'] }}.<a href="/hoc-sinh-vao-phong-hoc/{{ $lesson['classroom_id'] }}/{{ $lesson['lesson_id'] }}" class="join">Vào lớp ngay</a></div>
				</div>
			</div>
			@endforeach
		@endif

		@if (!empty($appraiseClassroom))
		<div class="notificationTop pd20 ds-none mbds-block">
				<div class="note boxShadow pd20 position">
					<div class="closed">
						Tắt thông báo
					</div>
					<div class="f30">Bạn đã học xong khóa học <b>{{ $appraiseClassroom['title'] }} </b>. vui lòng để lại đánh giá của bạn.
						<button type="button" class="join" data-toggle="modal" data-target="#appraiseClassroom{{ $appraiseClassroom['classroom_id'] }}">
							Đánh giá lớp học
						</button>
					</div>
				</div>
			</div>
			<!-- Button trigger modal -->

			<!-- Modal -->
			<div class="modal fade" id="appraiseClassroom{{ $appraiseClassroom['classroom_id'] }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Đánh giá lớp học</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					<form action="{{ route('rating')}}" method="GET">
						<div class="modal-body">
							<h4 class="text-ct">Đánh giá lớp học</h4>  
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-4 col-6 col-form-label"><span class="text-b700">Đánh giá Lớp học</span><span class="clred pd-05">(*)</span></label>
								<span class="star my-rating{{ $appraiseClassroom['classroom_id'] }}"></span>
								<span class="live-rating{{ $appraiseClassroom['classroom_id'] }}" ></span>
								<input type="hidden" value="4" name="appraise" class="appraiseStart{{ $appraiseClassroom['classroom_id'] }}"/>
								<script>
									//Đồng bộ chiều cao các div
					
									$(function() {
										$(".my-rating{{ $appraiseClassroom['classroom_id'] }}").starRating({
											initialRating: 4,
											disableAfterRate: false,
											onHover: function(currentIndex, currentRating, $el){
												$('.live-rating{{ $appraiseClassroom['classroom_id'] }}').text(currentIndex);
											},
											onLeave: function(currentIndex, currentRating, $el){
												$('.live-rating{{ $appraiseClassroom['classroom_id'] }}').text(currentRating);
												$('.appraiseStart{{ $appraiseClassroom['classroom_id'] }}').val(currentRating);
											}
										});
									});
								</script> 
							</div>

							<div class="form-group row">
								<label for="staticEmail" class="col-sm-4 col-12 col-form-label"><span class="text-b700">Nội dung</span><span class="clred pd-05">(*)</span></label>
								<div class="col-sm-8 col-12">
									<textarea name="description" class="form-control f14" placeholder="Nội dung" required></textarea>
								</div>
							</div>

							<h4 class="text-ct">Đánh giá giáo viên</h4>  		
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-4 col-6 col-form-label"><span class="text-b700">Đánh giá giáo viên</span><span class="clred pd-05">(*)</span></label>
								<span class="star teacher-rating{{ $appraiseClassroom['classroom_id'] }}"></span>
								<span class="teacher-live-rating{{ $appraiseClassroom['classroom_id'] }}" ></span>
								<input type="hidden" value="4" name="appraise_teacher" class="appraiseTeacher{{ $appraiseClassroom['classroom_id'] }}"/>
								<script>
									//Đồng bộ chiều cao các div
					
									$(function() {
										$(".teacher-rating{{ $appraiseClassroom['classroom_id'] }}").starRating({
											initialRating: 4,
											disableAfterRate: false,
											onHover: function(currentIndex, currentRating, $el){
												$('.teacher-live-rating{{ $appraiseClassroom['classroom_id'] }}').text(currentIndex);
											},
											onLeave: function(currentIndex, currentRating, $el){
												$('.teacher-live-rating{{ $appraiseClassroom['classroom_id'] }}').text(currentRating);
												$('.appraiseTeacher{{ $appraiseClassroom['classroom_id'] }}').val(currentRating);
											}
										});
									});
								</script> 
							</div>

							<div class="form-group row">
								<label for="staticEmail" class="col-sm-4 col-12 col-form-label"><span class="text-b700">Nội dung</span><span class="clred pd-05">(*)</span></label>
								<div class="col-sm-8 col-12">
									<textarea name="description_teacher" class="form-control f14" placeholder="Nội dung" required></textarea>
								</div>
							</div>

							<input type="hidden" value="{{ $appraiseClassroom['classroom_id'] }}" name="classroom_id" />
							<input type="hidden" value="{{ $appraiseClassroom['teacher_id'] }}" name="teacher_id" />
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
							<button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite btnloadding">Đánh giá</button>
						</div>
					</form>
					</div>
				</div>
			</div>
		@endif
	@endif
	<script>
		$('.notificationTop .closed').click(function(){
			$('.notificationTop').hide();
		});
	</script>