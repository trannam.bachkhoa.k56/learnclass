@extends('site.layout.site')

@section('title',' Kích hoạt khóa học' )
@section('meta_description',  'Kích hoạt khóa học')
@section('keywords', '')

@section('content')
    <section class="activatedLearn mg-15">
        <div class="container">
            <div class="row justify-content-lg-center">
                <div class="col-lg-8 col-md-12 activatedLearnContent">
                    <div class="img text-ct ds-block">
                        <img src="{{ asset('khoahoc/images/iconkey.jpg') }}" alt="" class="ds-inline">
                        {{--<img src="{{ asset('khoahoc/images/lock.png') }}" alt="" class="ds-inline">--}}
                    </div>
                    <div class="contentactive">
                        <div class="row justify-content-lg-center">
                            <div class="col-lg-8 col-md-12">
                                <p class="f16 note"><span class="ds-inline pd-3 pd-015 clwhite mgright5">
                                      {{ $message }} </span>
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection

