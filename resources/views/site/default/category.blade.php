@extends('site.layout.site')

@section('title', $category->title)
@section('meta_description',  $category->description )
@section('keywords', '')

@section('content')
    <section class="breadcrumb ds-inherit pd">
		<div class="bgbread">
			<div class="container">
				<div class="row">
					<div class="col-12 pdtop15">
						<h1>{{ $category->title }}</h1>
						<ul>
							<li><a href="">Trang chủ</a></li>
							<li>/</li>
							<li><a href="">{{ $category->title }}</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
    </section>

    <section class="categoryNew pd-40 workLear">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-12 col-12">
					<div class="row">
                    @foreach ($posts as $post)
					<div class="col-lg-6 col-md-12 xol-sm-12 col-12 work">
						<div class="itemwork">
							<div class="img">
								<div class="CropImg">
									<div class="thumbs">
										<a href="{{ route('post', ['cate_slug' => 'tin-tuc-hoat-dong', 'post_slug' => $post->slug]) }}">
											<img src="{{ asset($post->image) }}">
										</a>
									</div>
								</div>
								<div class="content boxH">
									<h3><a href="{{ route('post', ['cate_slug' => 'tin-tuc-hoat-dong', 'post_slug' => $post->slug]) }}" class="title" title="{{ $post->title }}">
										{{ $post->title }}
									</a></h3>
									<p class="datetime"><b>Ngày đăng:</b> {{ isset($post['thoi-gian'])  ? $post['thoi-gian'] : '' }} | <b>Danh mục:</b>  {{ isset($post['danh-muc'])  ? $post['danh-muc'] : '' }}</p>
									<p>{{ \App\Ultility\Ultility::textLimit($post->description, 50) }} <a class="readMore" href="{{ route('post', ['cate_slug' => 'tin-tuc-hoat-dong', 'post_slug' => $post->slug]) }}">Đọc thêm<i class="fas fa-angle-double-right bounceRight"></i></a></p>

								</div>
							</div>
						</div>
					</div>
                    @endforeach
					</div>
					<script>
					//Đồng bộ chiều cao các div
						$(function() {
							$('.boxH').matchHeight();
						});
					</script>
                    <div class="pa pd-30">
                        {!! $posts->links() !!}
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-12">
                    @include('site.partials.slidebar')
                </div>
            </div>
        </div>
    </section>
@endsection

