@extends('site.layout.site')

@section('title','liên hệ' )
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20">Liên hệ</h1>
                        <ul>
                            <li><a href="/">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="map">
        <div class="container">
            <p style="color: red; font-size: 18px; padding: 25px 0 40px 0;">{!! (isset($success)) ? "Cảm ơn bạn đã gửi thông tin liên hệ, đăng ký mã thẻ cho chúng tôi, chúng tôi sẽ phản hồi trong thời gian sớm nhất." : '' !!}</p>
            <div class="row">
                <div class="col-12">
                    <div class="contentMap w100 mgtop50">
                        {!! !empty($information['googlemap']) ? $information['googlemap'] : ''  !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contact mg-40">
        <div class="container">

            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-12 sidebarContact">
                    <div class="content">
                        <div class="CropImg">
                            <div class="thumbs">
                                <a href="" title="">
                                    <img src="{{ !empty($information['banner-lien-he']) ?  asset($information['banner-lien-he']) : 'khoahoc/images/callgirl.png' }}" alt="Liên hệ  tư vấn">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="contentdes mgleft20 mdmgleft0 mbmgbottom30 ">
                        <div class="company text-lt pdbottom20 mgtop20 mbtext-ct">
                            <h3 class="brcl-grey ds-block clgrey f22 mdf18 text-ca br-bottom brcl-grey pdbottom10 mbtext-ct">Địa chỉ</h3>
                            <p class="f14 clgrey">{!! !empty($information['dia-chi']) ? $information['dia-chi'] : ''  !!}</p>
                        </div>

                        <div class="company text-lt pdbottom20 mbtext-ct">
                            <h3 class="brcl-grey ds-block clgrey f22 mdf18 text-ca br-bottom brcl-grey pdbottom10 mbtext-ct">Thông tin liên hệ</h3>
                            <p class="f14 clgrey"><span>Số điện thoại : {!! !empty($information['hot-line']) ? $information['hot-line'] : ''  !!}</span></p>
                            <p class="f14 clgrey"><span>Email : {!! !empty($information['email']) ? $information['email'] : ''  !!}</span></p>
                            {{--<p class="f14 clgrey"><span>Offie : 123123456</span></p>--}}
                            {{--<p class="f14 clgrey"><span>Offie : 123123456</span></p>--}}
                        </div>
                        <div class="company text-lt pdbottom20">
                            <h3 class="brcl-grey ds-block clgrey f22 mdf18 text-ca br-bottom brcl-grey pdbottom10 mbtext-ct">Theo dõi</h3>
                            <ul class="pd mbtext-ct">
                                <li class="ds-inline"><a href="facebook.com" class="ds-block clgrey f16 clhr-red  mg-05"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="ds-inline"><a href="twitter.com" class="ds-block clgrey f16 clhr-red  mg-05"><i class="fab fa-twitter"></i></a></li>
                                <li class="ds-inline"><a href="gmail.com" class="ds-block clgrey f16 clhr-red  mg-05"><i class="fab fa-google-plus-g"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-12 formContact">
                    <h2 class="text-ca f28 clgrey text-lt mdf24 mbtext-ct ">Nhập thông tin liên hệ</h2>
                    <form action="{{ route('sub_contact') }}" method="post" accept-charset="utf-8" class="mgtop40 mgleft20 mdmgtop20">
                        {!! csrf_field() !!}
                        <div class="form-group row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                                <label for="" class="text-b f14 clgrey">Tên khách hàng *</label>
                                <input type="text" name="name" class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey" id="staticEmail"  placeholder="Nhập họ tên ...">
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                                <label for="" class="text-b f14 clgrey">Điện thoại *</label>
                                <input type="text"  name="phone" class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey" id="staticEmail"  placeholder="Điện thoại">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                                <label for="" class="text-b f14 clgrey">Email *</label>
                                <input type="email" name="email"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey" id="staticEmail"  placeholder="Email ...">
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
                                <label for="" class="text-b f14 clgrey">Địa chỉ *</label>
                                <input type="text" name="address"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey" id="staticEmail"  placeholder="Địa chỉ ...">
                            </div>
                        </div>

                        <div class="form-group row mdmgtop15">
                            <div class="col-12 pd pdright10">
                                <label for="" class="text-b f14 clgrey ">Bình luận *</label>
                                <textarea name="message" rows="8" placeholder="Vui lòng để lại bình luận" class="w100 pdleft10 br brcl-input br-rs5 clgrey"></textarea>
                            </div>

                        </div>
                        <div class="form-group row">
                            <button class="pd-10 pd-020 bgorang clwhite f18 br br-orang text-up text-b br-rs5 bghr-white clhr-orang  ">Gửi liên hệ</button>
                           
                        </div>
                    </form>
                    <p style="color: red; font-size: 18px; padding: 25px 0 40px 0;">{!! (isset($success)) ? "Cảm ơn bạn đã gửi thông tin liên hệ cho chúng tôi, chúng tôi sẽ phản hồi trong thời gian sớm nhất " : '' !!}</p>
                </div>
            </div>
        </div>
    </section>
@endsection

