@extends('site.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
    @section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')
    
@section('content')
<div class="h1title">
   <h1 style="font-size:20px; margin: 0;text-indent: -9999px;"></h1>
</div>
@include('site.partials.slider')
<!-- SOLOGEN -->
<!-- TĂNG THU NHẬP -->
<section class="income">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12 incomeLeft hiddenMobile">
                <div class="img">
                    <a href="{{ isset($information['link-thu-nhap']) ?$information['link-thu-nhap']  :'#' }}" title="{{ isset($information['tieu-de-thu-nhap']) ?$information['tieu-de-thu-nhap']  :'' }}">
                        <img src="{{ asset($information['banner-thu-nhap']) ? $information['banner-thu-nhap'] :'' }}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12 incomeRight">
                <div class="iccenter">
                    <div class="content">
                        <h3>{!! isset($information['tieu-de-thu-nhap']) ?$information['tieu-de-thu-nhap']  :'' !!}</h3>
                        <div class="des">{!! isset($information['mo-ta-tieu-de-thu-nhap']) ? $information['mo-ta-tieu-de-thu-nhap'] : '' !!}</div>
                        <a href="{{ isset($information['link-thu-nhap']) ?$information['link-thu-nhap']  :'#' }}" title="{{ isset($information['tieu-de-thu-nhap']) ?$information['tieu-de-thu-nhap']  :'' }}" class="bghr-grey clhr-white">Tham gia ngay <i class="fas fa-angle-double-right bounceRight"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- CÁC LỚP BẮT ĐẦU -->
<section class="classStar">
    <div class="container">
        <div class="row">
            <div class="col-12 titleIndex">
                <h3>Các lớp học hoặc sắp bắt đầu</h3>
                <p>Nhanh tay chọn lớp phù hợp</p>
                <div class="borderTitle">
                    <div class="left"></div>
                    <div class="center"><i class="fas fa-graduation-cap"></i></div>
                    <div class="right"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="menutab">
                <nav>
                    <div class="nav nav-tabs mbds-none nav-tabPc" id="nav-tab" role="tablist">
                        @foreach (\App\Entity\Menu::showWithLocation('menu-product') as $menu)
                            @foreach (\App\Entity\MenuElement::showMenuPageArray($menu->slug) as $id => $menuElement)
                                <a class="nav-item nav-link {{ ($id == 0) ? 'active' : '' }}"
                                     id="nav-home-tab" data-toggle="tab" href="#tab{{ $id }}" role="tab{{ $id }}" aria-controls="nav-home" aria-selected="true">{{ $menuElement['title_show'] }}</a>
                            @endforeach
                        @endforeach
                    </div>
					<div class="nav nav-tabs ds-none mbds-inline nav-tabMb" id="nav-tab" role="tablist">
						<div class="dropdown">
						  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Menu chọn môn học
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							@foreach (\App\Entity\Menu::showWithLocation('menu-product') as $menu)
								@foreach (\App\Entity\MenuElement::showMenuPageArray($menu->slug) as $id => $menuElement)
									<a class="nav-item nav-link dropdown-item {{ ($id == 0) ? 'active' : '' }}"
										 id="nav-home-tab" data-toggle="tab" href="#tab{{ $id }}" role="tab{{ $id }}" aria-controls="nav-home" aria-selected="true">{{ $menuElement['title_show'] }}</a>
								@endforeach
							@endforeach
							
						  </div>
						</div>
					</div>
                </nav>
            </div>
        </div>
        <div class="tab-content" id="nav-tabContent">
            @foreach (\App\Entity\Menu::showWithLocation('menu-product') as $menu)
                @foreach (\App\Entity\MenuElement::showMenuPageArray($menu->slug) as $id => $menuElement)
            <div class="tab-pane fade show {{ ($id == 0) ? 'active' : '' }}" id="tab{{ $id }}" role="tabpane{{ $id }}" aria-labelledby="nav-home-tab">
                <div class="row">
                    <?php $urls = explode('/', $menuElement['url']) ?>
                        @if ( isset($urls[2]) )
                            @foreach (\App\Entity\Product::showProduct($urls[2], 4) as $id => $product)
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 itemcol mgbottom20">
                                    @include('site.module_learnclass.product_item', ['$product' => $product])
                                </div>
                            @endforeach
                        @endif
                </div>
            </div>
                @endforeach
            @endforeach
        </div>
		
    </div>
</section>
<!-- GIÁO VIÊN CỦA CHÚNG TÔI -->
<section class="teacher">
    <div class="container">
        <div class="row">
            <div class="col-12 titleIndex">
                <h3>Giáo viên của chúng tôi</h3>
                <p>Hệ thống chuyên nghiệp</p>
                <div class="borderTitle">
                    <div class="left"></div>
                    <div class="center"><i class="fas fa-graduation-cap"></i></div>
                    <div class="right"></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-11 col-md-12 col-sm-12 col-12">
                <div class="owl-carousel">
                    @foreach (\App\Entity\Menu::showWithLocation('menu-giao-vien') as $menu)
                        @foreach (\App\Entity\MenuElement::showMenuPageArray($menu->slug) as $id => $menuElement)
                    <div class="item">
                        <div class="itemteacher">
                            <div class="img">
                                <div class="CropImg">
                                    <div class="thumbs">
                                        <a href="{{ $menuElement['url'] }}" title="">
                                            <img src="{{ !empty($menuElement['image']) ? asset($menuElement['image']) : asset('/images/no_teacher.png')}}" alt="{{ $menuElement['title_show'] }}">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a href="{{ $menuElement['url'] }}" class="titletea">{{ $menuElement['title_show'] }}</a>

                        </div>
                    </div>
                        @endforeach
                    @endforeach
                </div>
                <script>
                    $('.owl-carousel').owlCarousel({
                        loop:true,
                        margin:10,
                        responsiveClass:true,
                        autoplay:true,
                        autoplayTimeout:5000,
                        navigation : true,
                        nav:true,
                        responsive:{
                            0:{
                                items:2,
                                nav:true
                            },
                            600:{
                                items:3,
                                nav:true
                            },
                            1000:{
                                items:4,
                                nav:true,
                                loop:true
                            }
                        }
                    });
                    // $('.owl-prev').html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');
                    // $('.owl-next').html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
                </script>
            </div>

        </div>
    </div>
</section>
<!-- BẮT ĐẦU HỌC PHÍ -->
<section class="Starlear">
    <div class="bgapactity">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-12 hiddenMobile">
                    <div class="banner">
                        <img src="{{ asset('images/teacher.png') }}" alt="">
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-12 starRight">
                    <div class="contentStar">
                        <h4 class="title">rất đơn giản bắt đầu học với 3 bước</h4>
                        @foreach(\App\Entity\SubPost::showSubPost('bat-dau-hoc',3) as  $id=> $star)
                            <div class="itemsteep">
                                <div class="number">
                                    <span><?php $idstar = 0 ;$idstar += $id + 1 ; echo $idstar ;?></span>
                                </div>
                                <div class="right">
                                    <a href="">{{ isset($star['tieu-de']) ? $star['tieu-de'] : '' }}</a>
                                    <p class="des">{{ isset($star['mo-ta']) ? $star['mo-ta'] : '' }}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- KHÁCH HÀNG NÓI VỀ CHÚNG TÔI -->
@include('site.partials.mycustom')
<!-- HOẠT ĐỘNG DẠY HỌC -->
@include('site.partials.worklear')

<!-- ĐỐI TÁC -->
@include('site.partials.partner')

@endsection

