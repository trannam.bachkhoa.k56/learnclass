@extends('site.layout.site')

@section('type_meta', 'article')
@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('meta_image', asset($product->image) )
@section('meta_url', route('product', [ 'post_slug' => $product->slug]) )

@section('content')
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20"> {{ $product->title }}</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">{{ $product->title }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="product pdtop50">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 mdpd">
                    <div class="content bgwhite pd20 boxShadow">
                        <h3>{{ $product->title }}</h3>
                        <!-- <div class="icon clorang f16 lgf14 mg-10"><i class="fas fa-star mgright5 mgbottom10"></i><i class="fas fa-star mgright5"></i><i class="fas fa-star mgright5"></i><i class="fas fa-star mgright5"></i><i class="fas fa-star mgright5"></i>
                        </div> -->
                        <div class="shared mgbottom10">
                            <!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-541a6755479ce315"></script>
                            <!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox_vp3r"></div>
                        </div>
                        <div class="img">
                            <div class="CropImg CropImg60">
                                <div class="thumbs">
                                    <a href="" title="">
                                        <img src="{{ asset($product->image) }}" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="row pdtop20 pdbottom20 bgwhite mg ">--}}
                        {{--<div class="col-lg-3 col-md-3 col-sm-6 col-6">--}}
                            {{--<div class="itemimg mbmgbottom10">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-md-3 col-sm-6 col-6">--}}
                            {{--<div class="itemimg mbmgbottom10">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-md-3 col-sm-6 col-6">--}}
                            {{--<div class="itemimg mbmgbottom10">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="row">
                        <div class="col-12">
                            <div class="titlepr ds-block text-ct">
                                <h2 class="pd20 clxanhdam f18 mg"><i class="fas fa-info-circle pdright5 clxanhdam"></i>Thông tin khóa học</h2>
                            </div>
                            <div class="contenttable pd10 bgwhite boxShadow">
                                <table class="table table-striped mg-0 tablePH">
                                    <tbody class="text-up text-ct text-b">
                                    @php
                                        $isStudent = false;
                                        if (\Illuminate\Support\Facades\Auth::check()) {
                                            foreach ($studentOfClassrooms as $studentOfClassroom) {
                                                if ($studentOfClassroom->user_id == \Illuminate\Support\Facades\Auth::user()->id) {
                                                    $isStudent = true;
                                                }
                                            }
                                        }
                                    @endphp
                                    @foreach ($lessons as $id => $lesson)
                                    @php
                                        $dateStart = $lesson->date_at.' '.$lesson->time_start;
                                        $timeStart = strtotime($dateStart);
                                        $dateEnd = $lesson->date_at.' '.$lesson->time_end;
                                        $timeEnd = strtotime($dateEnd);
                                    @endphp
                                    <tr class="f16 mdf13 mbf12">
                                        <td class="w20
                                        @if ($timeEnd < time())
                                                bggrey
                                        @elseif ( $timeEnd >= time() && $timeStart <= time() )
                                                bgxanhlacay
                                        @else
                                                bgorang
                                        @endif">
                                            <i class="fas fa-calendar-alt clwhite f28 vertop mgtop10 mbmgbottom5"></i>
                                            <span class="f15 mdf12 ds-inline pdleft10 mdpd-05 mgtop8">
                                               <p class="mgbottom0 text-b700 clwhite br-bottom brcl-black ">{{ \App\Ultility\Ultility::formatTime($lesson->time_start) }}</p>
                                               <p class="date mgbottom0 clwhite">{{ \App\Ultility\Ultility::formatDate($lesson->date_at) }}</p>
                                            </span>
                                        </td>
                                        <td class="clxanhdam text-lt w55 "><span class="pd10 ds-block">Buổi {{ ($id + 1) }}  : {{ $lesson->title }}</span></td>

                                        @if ($timeEnd < time())
                                            <td class="clwhite bggrey w25"><span class="pdtop10 ds-block">Đã hoàn thành</span></td>
                                            @elseif ($timeEnd > time() && $timeStart < time()
                                            && \Illuminate\Support\Facades\Auth::check()
                                            && $teacher->user_id == \Illuminate\Support\Facades\Auth::user()->id )
                                                <td class="clwhite bgxanhlacay">
                                                    <a href="/phong-hoc/{{ $product->classroom_id }}/{{ $lesson->lesson_id }}">
                                                        <span class="pdtop10 ds-block">Mở lớp học</span>
                                                    </a>
                                                </td>
                                            @elseif ( $timeEnd > time() && $timeStart < time() && $isStudent == false)
                                                <td class="clwhite bgred"><span class="pdtop10 ds-block">Đang diễn ra</span></td>
                                            @elseif ($timeEnd > time() && $timeStart < time() && $isStudent == true )
                                                <td class="clwhite bgxanhlacay">
                                                    <a href="/hoc-sinh-vao-phong-hoc/{{ $product->classroom_id }}/{{ $lesson->lesson_id }}">
                                                        <span class="pdtop10 ds-block">Vào buổi học</span>
                                                    </a>
                                                </td>
                                            @else
                                                <td class="clwhite bgorang"><span class="pdtop10 ds-block">Sắp bắt đầu</span></td>
                                        @endif
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="titlepr ds-block text-ct">
                                <h2 class="pd20 clxanhdam f18 mg"><i class="fas fa-info-circle pdright5 clxanhdam"></i>Nội dung khóa học</h2>
                            </div>
                            <div class="des mg bgwhite pd20 mdpd10 boxShadow">
                                <p class="clblack f16 text-js">
                                   {!! $product->content !!}
                                </p>
                            </div>
                            <div class="clearfix mgbottom20">
                                <div class="orClass"><span>CÂU HỎI THƯỜNG GẶP</span></div>
                            </div>
                            <div class="contentques bgques mgtop20 mgbottom20">
                                
                                <div class="ques">
                                    <ul class="accordion" id="collMain"> 
                                        @foreach (\App\Entity\Post::newPost('cau-hoi-thuong-gap') as $id => $question)
                                        <li class="br brcl-black pd8 mg-5 bgwhite">
                                            <a href="" class="f16 clblack clhr-orang ds-block" data-toggle="collapse" data-target="#coll{{ $id }}" aria-expanded="true" aria-controls="coll{{ $id }}">{{ ($id+1) }}. {{ $question->title }}</a>
                                            <div id="coll{{ $id }}" class="pdtop20 content collapse" aria-labelledby="headingOne" data-parent="#collMain">
                                                {!! $question->content !!}
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @if (!\App\Entity\AppraiseClass::showAppraise($product->classroom_id)->isEmpty() )
                            <div class="clearfix mgbottom20">
                                <div class="orClass"><span>Chứng nhận đã tham gia học</span></div>
                            </div>
                            <div class="contentques bgques mgtop20 mgbottom20">
                                <div class="clblack f16 text-js pd8 bgwhite ">
                                    @foreach (\App\Entity\AppraiseClass::showAppraise($product->classroom_id) as $appraiseClass) 
                                    <div class="item mgbottom10" style="border-bottom: 1px solide #e5e5e5">
                                        <span class="star product-people-rating{{ $appraiseClass->appraise_class_id }}"></span>
                                        <input type='hidden' value="{{ $appraiseClass->point }}" class="productPeopleAppraise{{ $appraiseClass->appraise_class_id }}"/>
                                        <script>
                                            $(function() {
                                                var appraise = $(".productPeopleAppraise{{ $appraiseClass->appraise_class_id }}").val();
                                                $(".product-people-rating{{ $appraiseClass->appraise_class_id }}").starRating({
                                                    initialRating: appraise,
                                                    strokeColor: '#894A00',
                                                    starSize: 30,
                                                    readOnly: true
                                                });
                                            });
                                        </script>
                                        <p><i>bởi {{ $appraiseClass->name }}</i> <span style="color: #4caf50"><i class="fas fa-user-shield"></i> Chứng nhận đã tham gia học</span></p>
                                        <p>{{ $appraiseClass->description }}</p>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif

                            <div class="clearfix mgbottom20">
                                <div class="orClass"><span>Bình luận</span></div>
                            </div>
                            <div class="contentques bgques mgtop20 mgbottom20">
                                @include('general.sub_comments', ['post_id' => $product->post_id] )
                            </div>


                        </div>
                    </div>
                </div>
                <!-- END LEFT  -->
                <div class="col-lg-4 col-md-4 col-sm-12 col-12 mdpd">
                    <div class="row">
                        <div class="col-12 listRightPro">
                            <div class="productRight bgwhite pd-20">
                                <p class="text-ct br-bottom brcl-black pdbottom10 mgbottom20">
                                    <span class="clred f25 text-b700">{{ (!empty($product->price)) ? number_format($product->price, 0, ',', '.')." đ" : 'Miễn phí' }}
                                    </span><span class="clgrey f14"> / {{ $lessons->count() }} buổi</span></p>

                                <ul class="pdleft20 mdpdleft5 pdbottom20">
                                    <li class="pd-5"><a href="" class="f16 lgf14 text-b clblack clhr-orang"><i class="far fa-check-square pdright5"></i>Đã tham gia: {{ $numberStudent }} Học sinh</a></li>
                                    <li class="pd-5"><a href="" class="f16 lgf14 text-b clblack clhr-orang"><i class="far fa-check-square pdright5"></i>Lớp học: {{ $product->title }}</a></li>
                                    <li class="pd-5"><a href="" class="f16 lgf14 text-b clblack clhr-orang"><i class="far fa-check-square pdright5"></i>Ngày bắt đầu: {{ App\Ultility\Ultility::formatDateTime($product->started_time, $product->started_date) }}</a></li>
                                    <li class="pd-5"><a href="" class="f16 lgf14 text-b clblack clhr-orang"><i class="far fa-check-square pdright5"></i>Số buổi: {{ $lessons->count() }} buổi</a></li>
                                    <li class="pd-5"><a href="" class="f16 lgf14 text-b clblack clhr-orang"><i class="far fa-check-square pdright5"></i>Đánh giá: <br>
                                        <div class="star product-rating{{ $product->classroom_id }} text-ct"></div>
                                        <input type='hidden' value="{{ \App\Entity\AppraiseClass::countAppraise($product->classroom_id) }}" class="productAppraise{{ $product->classroom_id }}"/>
                                        <script>
                                            $(function() {
                                                var appraise = $(".productAppraise{{ $product->classroom_id }}").val();
                                                $(".product-rating{{ $product->classroom_id }}").starRating({
                                                    initialRating: appraise,
                                                    strokeColor: '#894A00',
                                                    readOnly: true
                                                });
                                            });
                                        </script>
                                    </li>
                                </ul>
                                <div class="buttonpr text-ct">
                                    @if (strtotime ($product->ended_date ) < time() )
                                        <a disabled="disabled" title="" class="disable clwhite bgxam pd-3 pd-010 ds-inline f18">Hết thời hạn đăng ký</a>
                                    @elseif ($numberStudent < $product->min_student)
                                            @if (\Illuminate\Support\Facades\Auth::check() &&
                                            ($isStudent || $teacher->user_id == \Illuminate\Support\Facades\Auth::user()->id) )
                                                <a href="" disabled="disabled" class="clwhite bggrey f24 lgf18 mdf14 pd-5 pd-015 br-rs5 text-up br ds-inline clhr-orang bghr-white mgbottom10">
                                                    <i class="fab fa-earlybirds pdright10"></i> Đã tham gia khóa học
                                                </a>
                                                @elseif (\Illuminate\Support\Facades\Auth::check() && (\Illuminate\Support\Facades\Auth::user()->coint >= $product->price))
                                                    <a href="/tham-gia-lop-hoc/{{ $product->classroom_id }}"
                                                       onclick="return acceptClassroom(this);"
                                                       class="clwhite bgorang f24 lgf18 mdf14 pd-5 pd-015 br-rs5 text-up br ds-inline clhr-orang bghr-white mgbottom10">
                                                        <i class="fab fa-earlybirds pdright10"></i>Tham gia khóa học</a>

                                            @elseif(empty($product->price))
                                             <a href="/tham-gia-lop-hoc/{{ $product->classroom_id }}"
                                                onclick="return acceptClassroom(this);"
                                                class="clwhite bgorang f24 lgf18 mdf14 pd-5 pd-015 br-rs5 text-up br ds-inline clhr-orang bghr-white mgbottom10">
                                                <i class="fab fa-earlybirds pdright10"></i>Tham gia khóa học</a>
                                            @else
                                                <a data-toggle="modal" data-target="#payCourse" class="clwhite bgorang f24 lgf18 mdf14 pd-5 pd-015 br-rs5 text-up br ds-inline clhr-orang mgbottom10 btnReg hvr-rectangle-in">
                                                    Tham gia khóa học
                                                </a>
                                                @include('site.popup.pay_course', ['product' => $product] )
                                            @endif
                                    @else
                                        <a href="" disabled="disabled" class="clwhite bggrey f24 lgf18 mdf14 pd-5 pd-015 br-rs5 text-up br ds-inline clhr-orang bghr-white mgbottom10">
                                            <i class="fab fa-earlybirds pdright10"></i> Lớp đã đầy
                                        </a>
                                    @endif
                                    <a href="/trang/huong-dan" class="f15 ds-block clhr-blue text-deup clxanhdam pdbottom2 text-b">Hướng dẫn đăng kí tham gia</a>
                                </div>
                            </div>


                            <div class="productRight bgwhite  pd-20 mg-30 boxShadow">
                                <p class="text-ct br-bottom brcl-black pdbottom10 mg-020 mgbottom20"><span class=" f18 text-b700 clblack">Thông tin giáo viên</span></p>
                                <div class="lefttc w45 mdw100 ds-inline">
                                    <div class="img text-rt pdright10 mdtext-ct">
                                        <a href="" class="clorang">
                                            <img src="{{ isset($teacher->image) && !empty($teacher->image) ? asset($teacher->image) : asset('images/no_teacher.png')  }}" alt="" class="w80 br br-rs50 mbw50"></a>
                                       
                                    </div>

                                </div>
                                <div class="lefttc w45 mdw100 ds-inline pdtop10 vertop mdtext-ct">
                                    <div class="img text-rt pdright10 mdtext-ct">
                                        <h3><a href="" class="clblack f14 text-bnone">{{ $teacher->name }}</a></h3>
                                        <div class="icon clorang f16 lgf14 mg-10"><i class="fas fa-star mgright5 mgbottom10"></i><i class="fas fa-star mgright5"></i><i class="fas fa-star mgright5"></i><i class="fas fa-star mgright5"></i><i class="fas fa-star mgright5"></i>
                                        </div>
                                        <a href="" class="clwhite bgprxanh br br-blue bghr-white clhr-blue f16 pd-7 pd-015 br-rs5 lgpd-05 lgf12 mdpd-10 mdpd-015 ">Liên hệ trực tiếp</a>

                                    </div>
                                </div>
                                
                                <div class="w100">
                                    <div class="w49 ds-inline text-ct">
                                       <p class="text-ct pd-010 f15 clblack text-b mgtop15 lgmgtop40 mdmgtop10 mdmgbottom5">Tổng số lớp học</p>
                                        <p class="text-ct pd-010 f15 clblack text-b mdmgbottom5">{{ $totalClassroom }}</p>
                                    </div>
                                    <div class="w49 ds-inline text-ct">
                                       <p class="text-ct pd-010 f15 clblack text-b mgtop50 mgright30 lgpd lgmgleft0 lgmgright0 lgmgtop10" >Tổng số học sinh</p>
                                       <p class="text-ct pd-010 f15 clblack text-b mgright30">{{ $totalStudentOfTeacher }}</p>
                                    </div>
                                </div>
                            </div>

                            <h2 class="clxanhdam f20 mgtop20  mgbottom10 lgf16">Các lớp học của {{ $teacher->name }}</h2>
                            <div class="bgClassroomOfTeacher">
                                @foreach (\App\Entity\Classroom::getClassrooms($teacher->teacher_id) as $classroom)
                                <div class="bgwhite  br-bottom brcl-xam itemPr mbw100 boxShadow">
                                    <div class="itempr">
                                        <div class="img w35 ds-inline vertop mbmg5 pd-010">
                                            <div class="CropImg lgCropImg">
                                                <div class="thumbs">
                                                    <a href="{{ route('product', ['post_slug' => $classroom->slug ]) }}" title="{{ $classroom->title }}">
                                                        <img src="{{ isset($classroom->image) ? asset($classroom->image) : asset('images/noiimg.png') }}" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w60 text-lt ds-inline">
                                            <h4><a href="{{ route('product', ['post_slug' => $classroom->slug ]) }}" class="clblack f14 bold clhr-orang text-bnone lgf12">{{ $classroom->title }}</a></h4>
                                            <p class="n00">
                                                @if (empty($classroom->price))
                                                    <span class="f16 clxanhdam text-lt">Miễn phí</span>
                                                @else
                                                    <span class="f16 clxanhdam text-lt">{{ number_format($classroom->price, 0, ',', '.') }} đ</span>
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                @endforeach
                            </div>
                            <div class="text-ct">
                                <p class="f16 clblack">Hoặc gọi cho chúng tôi <span class="clxanhlacay f22 text-b"><i class="fab fa-whatsapp pd-05"></i>{{ $information['hot-line'] }}</span></p>
                                <p class="f14 cldenhat">Hỗ trợ & Tư vấn 9h -18h , T2 - T6</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


