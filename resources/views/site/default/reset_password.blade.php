@extends('site.layout.site')

@section('title','Thay đổi mật khẩu')
@section('meta_description', $information['meta_description'])
@section('keywords', $information['meta_keyword'])

@section('content')
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20">Người dùng {{ $user->name }}</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Người dùng {{ $user->name }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="teacher bggray pdbottom30">
        <div class="container">
			@include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-12 sidebarContact">
					@include('site.module_learnclass.menu_user')
                </div>
                <div class="col-lg-9 col-md-8 col-sm-12 col-12 ">
                    <div class="teacherRight clblack text-lt">
                        <h2 class="f26">Đổi mật khẩu</h2>
                        @if (session('success'))
                            <div class="alert alert-primary" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif
                        <form class="bgwhite pd15 mg-30" style="border: 1px solid #ddd; box-shadow: 0 1px 1px rgba(0, 0, 0, .05);" action="/doi-mat-khau" method="post" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Mật khẩu cũ</span><span class="clred pd-05">(*)</span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="password" class="form-control f14" name="password_old" placeholder="Mật khẩu cũ" value="" required>
                                </div>
                                @if (session('faidOldPassword'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ session('faidOldPassword') }}
                                    </div>
                                @endif

                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Mật khẩu mới:</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="password" class="form-control f14" name="password" placeholder="Mật khẩu mới" value="" >
                                </div>
                                @if (session('password'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ session('password') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Nhập lại mật khẩu mới:</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="password" class="form-control f14" name="password_confirmation" placeholder="Nhập lại mật khẩu mới" value="" >
                                </div>
                                @if (session('password_confirmation'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ session('password_confirmation') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-2 col-md-4 col-sm-4"></div>
                                <div class="col-lg-10 col-md-8 col-sm-8 pdtop30">
                                    <button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite ">Cập nhật mật khẩu mới</button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
