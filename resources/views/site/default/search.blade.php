@extends('site.layout.site')
@section('title', 'tim-kiem')
@section('keywords', '')

@section('content')
<div class="h1title" style="text-indent: -9999px; margin: 0;">
    <h1 style="font-size: 1px">Tìm kiếm đèn Led</h1>
    <p class="des"></p>
  </div>
  <section class="category">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="breadcrumbcate">
            <ul>
              <li><a href="/" title="">Trang chủ</a></li>
              <li> / </li>
             <!--  <li><a href="/" title="">Rạng Đông</a></li>
               <li> / </li> -->
              <li>Tìm kiếm</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12 col-12 cateleft">
            @include('site.partials.sidebar') 
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-12 cateRight">
            <div class="titlecate">
              <h2>Từ khóa tìm kiếm : <?php echo $_GET['word']?></h2>
            </div>
            <div class="content">
              <p>

              </p>
            </div>

            <div class="catefiter">
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-12 noPadding imtemfiter">
                  <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Sản phẩm công suất
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <form action="category_submit" method="get" accept-charset="utf-8">
                          <input type="text" name="" value="" placeholder="">
                          <i class="fa fa-search" aria-hidden="true"></i>
                      </form>
                      <ul>
                        <li><a href="" title="">Dưới 7W</a></li>
                        <li><a href="" title=""> 7W - 8W</a></li>
                        <li><a href="" title="">Dưới 7W</a></li>
                        <li><a href="" title=""> 7W - 8W</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-12 noPadding imtemfiter">
                  <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Sản phẩm Giá
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <form action="category_submit" method="get" accept-charset="utf-8">
                          <input type="text" name="" value="" placeholder="">
                          <i class="fa fa-search" aria-hidden="true"></i>
                      </form>
                      <ul>
                        <li><a href="" title="">Dưới 7W</a></li>
                        <li><a href="" title=""> 7W - 8W</a></li>
                        <li><a href="" title="">Dưới 7W</a></li>
                        <li><a href="" title=""> 7W - 8W</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-12 noPadding imtemfiter">
                  <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Sản phẩm Hãng
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <form action="category_submit" method="get" accept-charset="utf-8">
                          <input type="text" name="" value="" placeholder="">
                          <i class="fa fa-search" aria-hidden="true"></i>
                      </form>
                      <ul>
                        <li><a href="" title="">Dưới 7W</a></li>
                        <li><a href="" title=""> 7W - 8W</a></li>
                        <li><a href="" title="">Dưới 7W</a></li>
                        <li><a href="" title=""> 7W - 8W</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>  
              <div class="row">
                <div class="col-12 resetfiter">
                 
                </div>
              </div>

              <div class="row LitsProduct">
              
                @foreach($products as $id => $product)
                  @include('site.partials.itempr') 
                @endforeach
            


                </div>

                <div class="row">
                  <div class="col-12">
                    <div class="Listpage">
                      <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                          <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                              <span aria-hidden="true">&laquo;</span>
                              <span class="sr-only">Previous</span>
                            </a>
                          </li>
                          <li class="page-item active"><a class="page-link" href="#">1</a></li>
                          <li class="page-item"><a class="page-link" href="#">2</a></li>
                          <li class="page-item"><a class="page-link" href="#">3</a></li>
                          <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                              <span aria-hidden="true">&raquo;</span>
                              <span class="sr-only">Next</span>
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>


      </div>
    </div>
  </section>
@endsection
