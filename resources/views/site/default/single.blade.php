@extends('site.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )

@section('content')

   <section class="breadcrumb ds-inherit pd">
   		<div class="bgbread">
	        <div class="container">
	            <div class="row">
	                <div class="col-12 pdtop15">
	                    <div class="titl">{{ $post->title }}</div>
	                    <ul>
	                        <li><a href="">Trang chủ</a></li>
	                        <li>/</li>
	                        <li><a href="">{{ $post->title }}</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
    	</div>
    </section>

    <section class="categoryNew new mg-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-12 col-12">
                    <div class="itemctnew brbottom pd10">
                        <!--<div class="img br pd5 brcl-grey">
                            <div class="CropImg CropImg60">
                                <div class="thumbs ">
                                    <a href="" title="">
                                        <img src="{{ asset($post->image) }}" alt="" class="">
                                    </a>
                                </div>
                            </div>
                        </div> -->
                        <h1 class="text-b f20 pd-15 ds-block clblack clhr-orang">{{ $post->title }}</h1>
                        <div class="shared mgbottom20">
							<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-541a6755479ce315"></script>
							<!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox_vp3r"></div>
						</div>
						<div class="des f14 clblack">
                            <p><b>{{ isset($post->description) ?$post->description : 'Đang cập nhật' }}</b></p>
                        </div>
						
						<div class="relateNews">
							<ul>
								@foreach (\App\Entity\Post::relativeProduct($post->slug, 5) as $postCategory)
								<li>
									<h3 class="f18"><a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $postCategory->slug]) }}" title="">{{ $postCategory->title }}</a></h3>
								</li>
								@endforeach
							</ul>
						</div>
						<div class="mainContent">
							<p>{!! isset($post->content) ?$post->content : 'Đang cập nhật tin tức' !!}</p>
							<div class="col-lg-9 col-md-9 col-sm-12 col-12 formContact">
								<h2 class="text-ca f28 clgrey text-lt mdf24 mbtext-ct ">Để lại thông tin để được tư vấn</h2>
								<form action="{{ route('sub_contact') }}" method="post" accept-charset="utf-8" class="mgtop40 mgleft20 mdmgtop20">
									{!! csrf_field() !!}
									<div class="form-group row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
											<label for="" class="text-b f14 clgrey">Tên khách hàng *</label>
											<input type="text" name="name" class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey" id="staticEmail"  placeholder="Nhập họ tên ...">
										</div>
									</div>
									<div class="form-group row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
											<label for="" class="text-b f14 clgrey">Điện thoại *</label>
											<input type="text"  name="phone" class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey" id="staticEmail"  placeholder="Điện thoại">
										</div>
									</div>
									<div class="form-group row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">
											<label for="" class="text-b f14 clgrey">Email *</label>
											<input type="email" name="email"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey" id="staticEmail"  placeholder="Email ...">
										</div>
							
									</div>
									<input type="hidden" value="{{ $post->title }}" name="message"/>
								
									<div class="form-group row">
										<button class="pd-10 pd-020 bgorang clwhite f18 br br-orang text-up text-b br-rs5 bghr-white clhr-orang  ">Gửi Đăng ký</button>
									   
									</div>
							</form>
							</div>
						</div>
                        <div class="tag">
                            <div class="w70 clblack  ds-in f14">
                                @if(!empty($post->tags))
                                    <p></p>       
                                <strong>Tags :</strong>
                                @foreach(explode(',', $post->tags) as $tag)
                                        <a href="/tags?tag={{ $tag }}" class="ds-inline bghr-orang mg-05 f14 clblack clhr-black">{{ $tag }},</a>
                                @endforeach
								@endif

                            </div>
                            <div class="w25 clblack  ds-in f14 fl-rt text-rt">
                                <a href="" class="f14 cldenhat clhr-orang mg-05"><i class="fab fa-facebook-f"></i></a>
                                <a href="" class="f14 cldenhat clhr-orang mg-05"><i class="fab fa-twitter"></i></a>
                                <a href="" class="f14 cldenhat clhr-orang mg-05"><i class="fab fa-linkedin-in"></i></a>
                            </div>
                        </div>
						<div class="relateNews mgtop20">
							<h2>Bài viết liên quan</h3>
							<ul>
								@foreach (\App\Entity\Post::relativeProduct($post->slug, 5) as $post)
								<li>
									<h3><a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug]) }}" title="">{{ $post->title }}</a></h3>
								</li>
								@endforeach
							</ul>
						</div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-12">
                    @include('site.partials.slidebar')
                </div>
            </div>
        </div>
    </section>
@endsection

