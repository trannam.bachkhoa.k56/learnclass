@extends('site.layout.site')
@section('title', 'tags '.$tag)
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', '')

@section('content')
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1>Danh sách khóa học theo tag {{ $tag }}</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Danh sách khóa học</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="category">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6"></div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-12">
                    <marquee>
                        <ul>
                            @foreach (\App\Entity\Classroom::startClassroom() as $classroom)
                                <li><a href="">
                                        <img src="{{ asset('images/running.gif') }}" alt="">{{ $classroom->classroom_name }} - {{ $classroom->teacher_name }} - {{ $classroom->lesson_title }}:  {{ \App\Ultility\Ultility::formatTime($classroom->time_start) }}</a></li>
                            @endforeach
                        </ul>
                    </marquee>
                </div>
            </div>
        </div>
        </div>
    </section>

    <section class="categoryCourse pdbottom30">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-5 col-sm-12 col-12 catesidebar noPadding mbmgtop20">
                    <div class="content">
                        <div class="titleside">
                            <h2 class="onclickshowdiv"><i class="fas fa-bars"></i>Danh mục khóa học</h2>
                        </div>
                        <div class="hiddenshowside">
                            <div class="title">
                                <h3 >các cấp học</h3>
                            </div>
                            <form action="" method="get" id="filterProduct">
                                <ul class="submenu-mb">
                                    @foreach(\App\Entity\FilterGroup::showFilterGroup() as $id => $filterGroup)
                                        @if ($filterGroup->group_name != 'Các Môn Học')
                                            @php $active= ''; @endphp
                                            @foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
                                                @php

                                                    if(!empty($_GET['filter']) && in_array($filter->name_filter, $_GET['filter']) >0) {
                                                        $active= 'active color';
                                                    }
                                                @endphp
                                            @endforeach
                                            <li class="item {{ $active }}">
                                                <div class="mgbottom10 ds-block"><span><i class="fas fa-plus-square"></i></span>
                                                    {{ $filterGroup->group_name }}</div>

                                                <ul class="submenu-mb1" @if(!empty($active)) style="display:block" @endif>
                                                    @foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
                                                        <li>
                                                            <div class="checkbox tiny m-b-2">
                                                                <div class="checkbox-overlay">
                                                                    <input type="radio" name="filter[]" value="{{ $filter->name_filter }}"
                                                                           @if(!empty($_GET['filter']) && in_array($filter->name_filter, $_GET['filter']) >0) checked @endif
                                                                           onChange="return changeFilter(this);"
                                                                    />
                                                                    <div class="checkbox-container">
                                                                        <div class="checkbox-checkmark"></div>
                                                                    </div>
                                                                    <label>{{ $filter->name_filter }}</label>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                                <!-- END CẤP HỌC -->
                                <div class="title">
                                    <h3><i class="fas fa-male"></i>Các môn học</h3>
                                </div>
                                <ul class="subjects">
                                    @foreach(\App\Entity\FilterGroup::showFilterGroup() as $id => $filterGroup)
                                        @if ($filterGroup->group_name == 'Các Môn Học')
                                            @foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
                                                <li>
                                                    <div class="checkbox tiny m-b-2">
                                                        <div class="checkbox-overlay">
                                                            <input type="radio" name="filter[]" value="{{ $filter->name_filter }}"
                                                                   @if(!empty($_GET['filter']) && in_array($filter->name_filter, $_GET['filter']) >0) checked @endif
                                                                   onChange="return changeFilter(this);"
                                                            />
                                                            <div class="checkbox-container">
                                                                <div class="checkbox-checkmark"></div>
                                                            </div>
                                                            <label>{{ $filter->name_filter }}</label>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        @endif
                                    @endforeach
                                </ul>
                                <script>

                                    $(document).ready(function () {
                                        var widthside = $( window ).width();
                                        if(widthside <= 500)
                                        {
                                            $('.onclickshowdiv').click(function(){
                                                $('.hiddenshowside').toggle(800);
                                            });
                                        }

                                        $('ul.submenu-mb>li.item').click(function () {

                                            if ($(this).hasClass('active')) {
                                                $(this).find('ul').slideUp();
                                                //$(this).find('i').css('transform','rotate(360deg)');
                                                $(this).find('i').css('transition', 'all 1s ease');
                                                $(this).removeClass('active');
                                                $(this).addClass('color');
                                                $(this).find('span').html('<i class="fas fa-plus-square"></i>');
                                            }
                                            else {
                                                $(this).find('ul').slideDown();
                                                $(this).addClass('active');
                                                // $(this).find('i').addClass('fa-rotate-180');
                                                //$(this).find('i').css('transform','rotate(180deg)');
                                                $(this).find('i').css('transition', 'all 1s ease');
                                                $(this).find('span').html('<i class="fas fa-minus-square"></i>');
                                                // $(this).find('span').addClass('fa-minus-square');


                                            }
                                        });
                                    });
                                </script>
                            </form>
                        </div>
                    </div>
                    <div class="ads mgtop20">
                        <a href="https://phonghoctructuyen.com/tin-tuc-hoat-dong/khuyen-mai-hoc-phi-cho-hoc-sinh-tham-gia-nhap-hoc-nhan-ngay-khai-truong-5-9-2018"><img src="{{ asset('images/banner-khai-truong.gif') }}"/></a>
                    </div>
                </div>
                <script>
                    //Đồng bộ chiều cao các div
                    $(function() {
                        $('.boxP').matchHeight();
                    });
                    function changeFilter(e) {
                        $('#filterProduct').submit();
                    }
                </script>
                <div class="col-lg-9 col-md-7 col-sm-12 col-12 contenitem">
                    <div class="row">
                        <div class="col-12 contentform">
                            <form action="" method="get" accept-charset="utf-8">
                                <input type="search" name="word" placeholder="Nhập tên khóa học" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}" />
                                <button type="submit"><i class="fas fa-search-plus"></i> Tìm kiếm</button>
                            </form>
                            <p class="note">Vui lòng nhập từ khóa tìm kiếm phù hợp với bạn</p>
                        </div>
                    </div>

                    <div class="row listCate">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 titleLeft">
                            <p>Tìm kiếm với tag {{ $tag }}</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 showRight">
                            <div class="showall"><a href="">Xem tất cả <i class="fas fa-plus-square"></i></a></div>
                        </div>
                        @if ($countProduct == 0)
                            <div class="col-12 col-sm-12 col-md-12">
                                <p style="text-align: center;">Hiện tại chưa có khóa học nào cả.</p>
                            </div>
                        @endif
                        @foreach ($products as $product)
                            <div class="col-lg-4 col-md-12 col-sm-6 col-12 mgbottom20">
                                <div class="item">
                                    @include('site.module_learnclass.product_item', ['product' => $product])
                                </div>
                            </div>
                        @endforeach
                        <div class="col-12 Pagecate">
                            @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
                                {{ $products->links() }}
                            @endif
                        </div>

                    </div>

                    <!-- END CONTEND -->

                </div>
            </div>
    </section>
@endsection
