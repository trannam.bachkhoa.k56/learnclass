@extends('site.layout.site')

@section('title','Thông tin tài khoản')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20">Người dùng {{ $user->name }}</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Người dùng {{ $user->name }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="teacher bggray pdbottom30">
        <div class="container">
			@include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-12 sidebarContact">
                    @include('site.module_learnclass.menu_user')
                </div>
                <div class="col-lg-9 col-md-8 col-sm-12 col-12 ">
                    <div class="teacherRight clblack text-lt mbpdleft0 mgtop20">
                        <h2 class="f26">Thông tin cơ bản</h2>
                        <form class="bgwhite pd15 mg-30" style="border: 1px solid #ddd; box-shadow: 0 1px 1px rgba(0, 0, 0, .05);" action="/thong-tin-ca-nhan" method="post" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Họ và tên</span><span class="clred pd-05">(*)</span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="text" class="form-control f14" name="name" placeholder="Họ và tên" value="{{ $user->name }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Email:</span><span class="clred pd-05">(*)</span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="email" class="form-control f14" name="email" placeholder="email" value="{{ $user->email }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Số điện thoại:</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="number" class="form-control f14" name="phone" placeholder="Số điện thoại" value="{{ $user->phone }}" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Địa chỉ:</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="text" class="form-control f14" name="address" placeholder="Địa chỉ" value="{{ $user->address }}" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Avatar</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <img id="blah"  src="{{ !empty($user->image) ? asset($user->image) : asset('khoahoc/images/avatar.png') }}" alt="" width="150">
                                    <button class="btn btn-default addAvatar">TẢI LÊN AVATAR</button>
                                    <input type='file' id="imgInp" name="image" onchange="readURL(this)" style="display: none"/>
                                    <input type="hidden" value="{{ $user->image }}" name="avatar" />
                                    <script>
                                        function readURL(input) {
                                            if (input.files && input.files[0]) {
                                                var reader = new FileReader();

                                                reader.onload = function(e) {
                                                    $('#blah').attr('src', e.target.result);
                                                }

                                                reader.readAsDataURL(input.files[0]);
                                            }
                                        }
                                        $('.addAvatar').click(function() {
                                            $('#imgInp').click();
                                            return false;
                                        });
                                    </script>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Mô tả bản thân</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <textarea class="form-control editor" id="content" name="description" rows="3">{{ $user->description }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Giới tính</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <select class="form-control" name="sex">
                                        <option value="Nam" @if ($user->sex == 'Nam') selected @endif>Nam</option>
                                        <option value="Nữ" @if ($user->sex == 'Nữ') selected @endif>Nữ</option>
                                        <option value="Không phân biệt" @if ($user->sex == 'Không phân biệt') selected @endif>Không phân biệt</option>
                                    </select>
                                </div>
                            </div>
                            <h3 class="clblack f24 pd-30 text-ct">
                                Thông tin mạng xã hội
                            </h3>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Link Facebook</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="text" class="form-control" value="{{ $user->facebook }}" name="facebook" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Link Youtube</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="text" class="form-control" value="{{ $user->youtube }}" name="youtube" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Mạng xã hội khác</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="text" class="form-control" value="{{ $user->other_social }}" name="other_social" />
                                </div>
                            </div>
                            @if ($errors->has('email'))
                                <div class="alert alert-danger" role="alert">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                            @if ($errors->has('password'))
                                <div class="alert alert-danger" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </div>
                            @endif
                            <h3 class="clblack f24 pd-30 text-ct">
                                Thông tin giảng viên
                            </h3>
                            @if ($user->user_type == 'giao_vien')
                                <?php $teacher = \App\Entity\Teacher::detailTeacher($user->id); ?>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Đơn vị công tác</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="text" class="form-control f14" name="where_working" placeholder="Đơn vị công tác" value="{{ isset($teacher->where_working ) ? $teacher->where_working : '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Lĩnh vực giảng dạy</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <select class="form-control" name="teaching_field">
                                        @foreach(\App\Entity\SubPost::showSubPost('linh-vuc-giang-day',1000) as  $id=> $field)
                                            <option value="{{ $field->title }}" @isset($teacher->teaching_field) @if($field->title == $teacher->teaching_field) selected @else @endif @endisset>{{ $field->title }}</option>
                                        @endforeach
                                    </select>
                                    {{--<input type="text" class="form-control f14" name="teaching_field" placeholder="Lĩnh vực giảng dạy" value="{{ isset($teacher->teaching_field ) ? $teacher->teaching_field : '' }}" />--}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Trình độ học vấn</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="text" class="form-control f14" name="academic_standard" placeholder="Trình độ học vấn" value="{{ isset($teacher->academic_standard ) ? $teacher->academic_standard : '' }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Hình thức giảng dạy</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="text" class="form-control f14" name="form_of_teaching" placeholder="Giáo viên, gia sư" value="{{ isset($teacher->form_of_teaching ) ? $teacher->form_of_teaching : '' }}" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Công việc hiện tại</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="text" class="form-control f14" name="current_job" placeholder="Công việc hiện tại: giáo viên, sinh viên,..." value="{{ isset($teacher->current_job ) ? $teacher->current_job : '' }}" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Môn giảng dạy</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <input type="text" class="form-control f14" name="teaching_subject" placeholder="Môn giảng dạy chinh" value="{{ isset($teacher->teaching_subject ) ? $teacher->teaching_subject : '' }}" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4  col-form-label"><span class="text-b700">Tệp thông tin cá nhân</span><span class="clred pd-05"></span></label>
                                <div class="col-lg-10 col-md-8 col-sm-8">
                                    <div class="input-group control-group increment" >
                                        <input type="file" name="filename[]" value="" class="form-control">
                                        <div class="input-group-btn">
                                            <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>  Thêm tệp</button>
                                        </div>
                                    </div>
                                    <div class="clone hide">
                                        <div class="control-group input-group" style="margin-top:10px">
                                            <input type="file" name="filename[]" value="" class="form-control">
                                            <div class="input-group-btn">
                                                <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Xóa tệp</button>
                                            </div>
                                        </div>
                                    </div>

                                    <script type="text/javascript">

                                        $(document).ready(function() {

                                            $(".btn-success").click(function(){
                                                var html = $(".clone").html();
                                                $(".increment").after(html);
                                            });

                                            $("body").on("click",".btn-danger",function(){
                                                $(this).parents(".control-group").remove();
                                            });

                                        });

                                    </script>
                                    <style>
                                        .hide {
                                            display: none;
                                        }
                                    </style>
                                </div>
                            </div>
                            @endif
                            <div class="form-group row">
                                <div class="col-lg-2 col-md-4 col-sm-4 "></div>
                                <div class="col-lg-10 col-md-8 col-sm-8 pdtop30">
                                    <button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite ">Cập nhật lại thông tin</button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
