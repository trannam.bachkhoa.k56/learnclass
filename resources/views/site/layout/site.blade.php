<!DOCTYPE HTML>
<html>
<head>
    <meta property="fb:pages" content="" />
    <meta name="adx:sections" content="" />
    <meta name="p:domain_verify" content=""/>
    <meta name="google-site-verification" content="" />
    <meta name="google-site-verification" content="" />
    <meta http-equiv="content-type" content="text/html" />
    <meta charset="utf-8" />
    <title>
        @yield('title')
    </title>
	<meta name="title" content="@yield('title')"/>
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />

	<meta property="og:image:type" content="image/jpeg" />
	<meta property="og:locale" content="vi_VN"/>
	<meta property="og:type" content="@yield('type_meta')"/>

    <meta property="og:title" content="@yield('title')" />
    <meta property="og:description" content="@yield('meta_description')" />
    <meta property="og:url" content="@yield('meta_url')" />
    <meta property="og:image" content="@yield('meta_image')" />
    <meta property="og:image:secure_url" content="@yield('meta_image')" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="@yield('meta_description')" />
    <meta name="twitter:title" content="@yield('title')" />
    <meta name="twitter:image" content="@yield('meta_image')" />
	
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <link rel="icon" href="{{ isset($information['icon']) ? asset($information['icon']) :'' }}" type="image/x-icon" />


    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
     <link rel="stylesheet" href="{{ asset('css/drawer.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/hover.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome-all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/extract.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('adminstration/select2/dist/css/select2.min.css') }}">
    <!-- SCSS -->
    <link rel="stylesheet" href="{{ asset('scss/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('scss/reponsive.css') }}">
    <link rel="stylesheet" href="{{ asset('learn-online/css/star-rating-svg.css') }}">

    {{--CONVERT SCSS--}}
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
     <link rel="stylesheet" href="{{ asset('css/styles-thang.css') }}">
    <!-- JS -->

    <script src="{{ asset('js/jquery3.1.js') }}"></script>

    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

	<script src="{{ asset('js/drawer.min.js') }}"></script>
	<script src="{{ asset('js/iscroll.js') }}"></script>
    <script src="{{ asset('js/transition.min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('js/jquery.matchHeight-min.js') }}"></script>
    <!-- <script src="{{ asset('js/WOW.js') }}"></script> -->


    <script type="text/javascript" src="{{ asset('js/numeral.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('learn-online/js/jquery.star-rating-svg.js') }}"></script>
<!-- CK Editor -->
    <script src="{{ asset('adminstration/ckeditor/ckeditor.js') }}"></script>
	
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '265373321058760');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=265373321058760&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136496749-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-136496749-1');
	</script>

</head>
<body class="drawer drawer--left">

    @include('site.common.header') 

         @yield('content')
    @include('site.common.footer')
    <script>
        $(function () {
            $('.editor').each(function(e){
                CKEDITOR.replace( this.id, {

                });
            });

            setInterval(function(){
                $.ajax({
                    type: "GET",
                    url: '{!! route('check_login') !!}'
                });
            }, 10000);
        });

        function acceptClassroom (e) {
            var classroomId = $(e).attr('classroomId');
            $('#acceptCLassroomId').attr('href', $(e).attr('href'));
            $('#takePartInClassroom').modal('show');

            return false;
        }
        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token =  $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: '{!! route('subcribe_email') !!}',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data){
                    var obj = jQuery.parseJSON(data);
                    alert(obj.message);
                    location.reload();
                }
            });
            return false;
        }


        $(document).ready(function() {
          $('.btnloadding').on('click', function() {
            var $this = $(this);
            var loadingText = '<i class="fa fa-spinner fa-spin mgright5"></i> Vui lòng chờ...';
            if ($(this).html() !== loadingText) {
              $this.data('original-text', $(this).html());
              $this.html(loadingText);
            }   
            setTimeout(function() {
              $this.html($this.data('original-text'));
            }, 5000);
          });
        })


    </script>
    <script src="{{ asset('raty/jquery.raty.js') }}"></script>
    @include('site.popup.take_part_in_classroom')
</body>
</html>