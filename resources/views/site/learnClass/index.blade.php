<!DOCTYPE HTML>
<html>
<head>
    <meta property="fb:pages" content="" />
    <meta name="adx:sections" content="" />
    <meta name="p:domain_verify" content=""/>
    <meta name="google-site-verification" content="" />
    <meta name="google-site-verification" content="" />
    <meta http-equiv="content-type" content="text/html" />
    <meta charset="utf-8" />
    <title>
        Khóa học {{ $classroom->title }}
    </title>
    <meta name="description" content="@yield('meta_description')" />
    <meta property="og:type" content="website">
    <meta property="og:title" content="@yield('title')">
    <meta property="og:image" content="@yield('meta_image')">
    <meta property="og:description" content="@yield('meta_description')">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <link rel="icon" href="{{ isset($information['icon']) ? asset($information['icon']) :'' }}" type="image/x-icon" />


    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/hover.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome-all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/extract.css') }}">

    <!-- SCSS -->
    <link rel="stylesheet" href="{{ asset('scss/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('scss/reponsive.css') }}">

    {{--CONVERT SCSS--}}
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('learn-online/css/style_class_room.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('learn-online/css/getMediaElement.css') }}">
    <!-- JS -->

    <script src="{{ asset('js/jquery3.1.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/transition.min.js') }}"></script>
    <script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
	<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    {{--<script src="{{ asset('js/WOW.js') }}"></script>--}}

    <script type="text/javascript" src="{{ asset('js/numeral.min.js') }}"></script>
    <!-- CK Editor -->
    <script src="{{ asset('adminstration/ckeditor/ckeditor.js') }}"></script>

</head>
    <body>
    <section class="Learing">
        <div class="header pd10">
            <div class="row">
                <div class="col-md-1 col-sm-2">
                    <a href="http://phonghoctructuyen.com" class="logo"><img width="175" src="https://phonghoctructuyen.com/library/images/thontinchung/logonew.png"/></a>
                </div>
                <div class="col-md-6 col-sm-6">
                    <h1 class="f20 whitespace">
						{{ $classroom->title }}
					</h1>
                </div>
                <div class="col-md-5 col-sm-4 text-ct infoTime pdtop5">
                    <div class="text-rt whitespace">
                        <b class="f18">{{ $lesson->title }}<b>
                        <span class="f12">
                            ({{ \App\Ultility\Ultility::formatTime($lesson->time_start) }} - {{ \App\Ultility\Ultility::formatDate($lesson->date_at) }})
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="hr"></div>
        <div class="contentLearning">
			<div class="mainLearn">
				<div class="ViewFull boxFull">
					<div>
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active" ><a  class="active" href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Màn hình Chính</a></li>
							<li role="presentation" ><a  href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Bảng viết</a></li>
							{{-- <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Slide giảng dạy</a></li>
							<li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Công cụ hỗ trợ</a></li>
							<li role="presentation"><a href="#tab4" aria-controls="tab5" role="tab" data-toggle="tab">Công cụ hỗ trợ 2</a></li> --}}
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active pd10" id="tab1">
								<div id="videos-boardFirst"></div>
								@if ($isTeacher == 1)
								<div id="share-boardFirst" class="scale scaleTeacher boxShadow bgwhite pd30 mgauto">
									<div style="text-align: center;">
										<button class="btn btnShared" >Chia sẻ nội dung</button>
									</div>
									<div id="images-boardFirst" class="text-ct"><img width="100" src="{{ asset('images/arrowDown.png') }}"/></div>
									<div class="">
										Để tiện cho việc dạy học thông qua các phần mềm có sẵn trên máy tính như word, powerpoint. Giáo viên click vào nút chia sẻ nội dung để chia sẻ màn hình phần mềm.
									</div>
									<h2 class="mgtop20">HƯỚNG DẪN CÀI ĐẶT ỨNG DỤNG CHIA SẺ MÀN HÌNH</h2>
									<div>
										Giáo viên truy cập vào link sau: <a href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk">SCREEN CAPTURING</a><br>
										Sau khi truy cập vào Link trên, Giáo viên click vào nút Thêm vào chrome để hoàn tất quá trình cài đặt phần mềm chia sẻ ứng dụng.<br>
										<img class="mgtop10 mgbottom10" width="100%" src="{{ asset('images/them-moi-ung-dung.jpg') }}"/><br>
										Sau Khi cài đặt ứng dụng. Giáo viên vui lòng ấn phím "F5" trên bàn phím để load lại website Và click vào Nút "Chia sẻ nội dung" để bắt đầu chia sẻ ứng dụng.
									</div>
								</div>
								@else
								<div id="studentScreen" class="students scale scaleTeacher boxShadow bgwhite pd30 mgauto">
									<h2>Quy định tham gia phòng học của học sinh</h2>
									<p>
										- Học sinh phải trật tự không gây ồn ào khi tham gia lớp học<br>
										- Không tự ý phát biểu khi chưa được sự cho phép của giáo viên<br>
										- Nếu muốn phát biểu, Học sinh vui lòng click biểu tượng <i onclick="return onHand(this);" class="fa fa-hand-paper clred mgright5"></i> ở bên phải màn hình để được sự cho phép của giáo viên khi phát biểu.<br>
										- Không tự ý rời khỏi phòng học khi không được sự cho phép của giáo viên<br>
										- Có thái độ hợp tác khi vào học, không quấy rối trong lớp học. Hệ thống sẽ đẩy bạn vào trạng thái hình phạt nếu có thái độ phá quấy trong lớp.
									</p>
								</div>
										
								@endif
							</div>
							<div role="tabpanel" class="tab-pane pd10" id="tab2">
								<div id="widget-container" style="height: 100vh;border: 1px solid black; border-top:0; border-bottom: 0;"></div>
							</div>
							<!-- <div role="tabpanel" class="tab-pane pd10" id="tab3">
								@if ($isTeacher == 1)
									<div style="text-align: center;">
										<button id="share-boardThird" class="btn btn-primary" >Chia sẻ nội dung</button>
									</div>
								@endif
								<div id="videos-boardThird"></div>
								<div id="images-boardThird"><img width="100%" src="https://lh3.googleusercontent.com/fCRBUvY7xzfsQVvXQjBcjyjJMkSCIJIIq225DyH12VmQdhiWIOMfM65n9o8qKIA1pTyzqH_s9iCl4s05C4ycZLypCtPOTzXoD-Y1PdkVC4db2q7YyIU"/></div>
							</div>
							<div role="tabpanel" class="tab-pane pd10" id="tab4">
								@if ($isTeacher == 1)
									<div style="text-align: center;">
										<button id="share-boardFourth" class="btn btn-primary" >Chia sẻ nội dung</button>
									</div>
								@endif
								<div id="videos-boardFourth"></div>
								<div id="images-boardFourth"><img width="100%" src="https://lh3.googleusercontent.com/fCRBUvY7xzfsQVvXQjBcjyjJMkSCIJIIq225DyH12VmQdhiWIOMfM65n9o8qKIA1pTyzqH_s9iCl4s05C4ycZLypCtPOTzXoD-Y1PdkVC4db2q7YyIU"/></div>
							</div>
							<div role="tabpanel" class="tab-pane pd10" id="tab5">
								@if ($isTeacher == 1)
									<div style="text-align: center;">
										<button id="share-boardFifth" class="btn btn-primary" >Chia sẻ nội dung</button>
									</div>
								@endif
								<div id="videos-boardFifth"></div>
								<div id="images-boardFifth"><img width="100%" src="https://lh3.googleusercontent.com/fCRBUvY7xzfsQVvXQjBcjyjJMkSCIJIIq225DyH12VmQdhiWIOMfM65n9o8qKIA1pTyzqH_s9iCl4s05C4ycZLypCtPOTzXoD-Y1PdkVC4db2q7YyIU"/></div>
							</div>
							
						</div> -->
							
					</div>
				</div>
				<div class="sidebar boxFull">
					<input type="hidden" id="room-id" value="{{ $classroom->classroom_id }}">
					<!--<div class="Start box text-ct hiddenView">
					   
						<!--
						@if ($isTeacher == 1)
							<button id="open-room" class="btnBlue">Bắt đầu lớp học</button>
						@else
							<button id="join-room" class="bbtnBlue">Tham gia lớp học</button>
							<p><i>(Ấn vào đây để bắt đầu tham gia khóa học)</i></p>
						@endif
						-->
						<!-- Display the countdown timer in an element
						 <div class="row">
							<div class="col-md-8 pdtop10 text-lt"><i class="fa fa-clock"></i> <span id="clockstart"></span></div>
							<div class="col-md-4"><button class="btnOrange" onclick="return endClassroom(this);">Kết thúc</button></div>
						</div> 
						<script>
							// Set the date we're counting down to
							var countDownDate = new Date('{!! $lesson->date_at.' '.$lesson->time_end !!}').getTime();
							

							// Update the count down every 1 second
							var x = setInterval(function() {

								// Get todays date and time
								var now = new Date().getTime();

								// Find the distance between now an the count down date
								var distance = countDownDate - now;

								// Time calculations for days, hours, minutes and seconds
								var days = Math.floor(distance / (1000 * 60 * 60 * 24));
								var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
								var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
								var seconds = Math.floor((distance % (1000 * 60)) / 1000);

								// Display the result in the element with id="clockstart"
								document.getElementById("clockstart").innerHTML = days + "d " + hours + "h "
									+ minutes + "m " + seconds + "s ";

								// If the count down is finished, write some text
								if (distance < 0) {
									clearInterval(x);
									document.getElementById("clockstart").innerHTML = "EXPIRED";
								}
							}, 1000);
						</script>
						
					</div> -->
					<div class="avatarTeacher box pd10">
						<h3 class="f16 uppercase">Giáo viên: {{ $teacher->name }}</h3>
						<!-- <button class="btn btn-warning colorWhite" onclick="myFunction()"><i class="far fa-share-square"></i> GHIM</button> -->
						<div id="teacher">
							<img src="http://www.jbcnschool.edu.in/wp-content/uploads/2017/08/a-kindergarten-teacher.jpg"/>
						</div>
					</div>
					<div>
						<div class="hand">
							
						</div>
						<div class="listPeoPle box" id="listStu">
							<h3 class="f16 uppercase pd10">DANH SÁCH ({{ $userCounts }} HỌC SINH)</h3>
							<div id="student-video" class="{{ ($userCounts > 20) ? 'studentVideo' : '' }} ">
								<ul class="listStudent">
								@if ($students->isEmpty())
									không có học sinh đăng ký lớp này.
								@else
									@foreach ($students as $studentClassroom) 
									<li>
										<div class="position">
											<div class="showFace mgbottom10" id ="HS{{ $studentClassroom->id }}.{{ str_replace(' ', '_', $studentClassroom->name) }}" >
												
											</div>
											<h4 class="f12 whitespace HS{{ $studentClassroom->id }}" title="{{ $studentClassroom->name }}"> 
											<span onclick="return showVideoStudent(this)" class="clickShowFace"><i class="fas fa-video"></i></span>
											<i class="fas fa-circle online" style="color: gray; font-size: 10px;"></i> HS{{ $studentClassroom->id }}.{{ $studentClassroom->name }} 
											</h4>
										</div>
									</li>
									@endforeach
								@endif	
								</ul>
							</div>
						</div>
						<script>
							jQuery(function($){
							  var windowWidth = $(window).width();
							  var windowHeight = $(window).height();
							  var height = $('.avatarTeacher').height();
							  var heightScroll = windowHeight - height - 80;

							  // $(window).resize(function() {
								// if(windowHeight != $(window).height()) {
									// location.reload();
									// return;
								// }
								
							  // });
							  $('#listStu').slimscroll({
									height: heightScroll + 'px'
								});
							});
							
							
							function showVideoStudent(e) {
								var showFace = $(e).parent().parent().find('.showFace');
						
								if (showFace.is(":hidden")){
									$(e).attr('style', 'border: 1px solid red !important;');
									showFace.show();
								} else {
									$(e).removeAttr('style');
									showFace.hide();
								}
							}
						</script>
					</div>

				</div>
			</div>
            <div class="sidebarLearn">
                <ul class="listTool nav nav-tabs" role="tablist">
                    <!-- <li><a id="fullScreen"><i class="fa fa-compress"></i>Mở rộng</a></li>-->
					@if ($isTeacher == 0)
					<li onclick="return onHand(this);"><a ><i  class="fa fa-hand-paper clred mgright5"></i> Giơ tay</a></li>
					@endif
                    <!--<li role="presentation"><a href="#side1" aria-controls="side1" role="tab" data-toggle="tab"><i class="fa fa-users"></i>Lớp học</a></li>
                    <li role="presentation"><a href="#side2" aria-controls="side2" role="tab" data-toggle="tab"><i class="fa fa-paperclip"></i>Tài liệu</a></li>
                    <li role="presentation"><a href="#side3" aria-controls="side3" role="tab" data-toggle="tab"><i class="fa fa-comments"></i>Trao đổi</a></li> -->
                    <!-- <li><a data-toggle="modal" data-target="#rating"><i class="fa fa-star"></i>Đánh giá</a></li>-->
                </ul>
            </div>
			<!-- <div class="FileAttack" id="side2">
				<div class="mainFiles box">
					<h3 class="f16 uppercase">Tài liệu tham khảo</h3>
					<hr></hr>
					
					
				</div>
			</div> -->
			<div class="Message" id="side3">
				
				<div class="mainComments">
					<h3 class="f16 uppercase hideNotify"><span class="notify" id="notifyChat"></span></a> Trao đổi trong lớp</h3>
					<div class="mainMessage">
						<div class="contentChat" id="file-container">
						
						</div>
						<div class="inputComment input-group mgtop10">
							<input type="text" value="" class="form-control" id="inputQuestion" placeholder="Nhập tin nhắn" aria-describedby="chatShare"/>
							<div class="input-group-prepend">
								<button id="share-file" class="btn btn-primary" ><i class="fas fa-paperclip"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<script>
				$('.Message h3').click(function(){
					if($('.mainMessage').is(":hidden")){
						$('.mainMessage').show();
					}
					else {
						$('.mainMessage').hide();
					}
				});
			</script>
        </div>
        <script>
            $('#fullScreen').click(function(){
                if($('.ViewFull').hasClass("full")) {
                    $('.ViewFull').removeClass("full");
                    $('.avatarTeacher').removeClass("fullG");
                    $('.listTool').removeClass("fullT");
                    $(this).html("<i class='fa fa-compress'></i>Mở rộng</a>");
                    $('.hiddenView').show();
                }
                else {
                    $('.ViewFull').addClass("full");
                    $('.avatarTeacher').addClass("fullG");
                    $('.listTool').addClass("fullT");
                    $(this).html("<i class='fa fa-compress'></i>Thu hẹp</a>");
                    $('.hiddenView').hide();
                }
            });
			setInterval(function(){
				$.ajax({
					type: "GET",
					url: '{!! route('check_login') !!}'
				});
			}, 10000);
        </script>
    </section>

    <!-- Modal Rating -->
    <div class="modal fade" id="rating" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Đánh giá chất lượng lớp học</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Chức năng này chúng tôi đang phát triển.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Để sau</button>
                    <button type="button" class="btn btn-primary">Đánh giá</button>
                </div>
            </div>
        </div>
    </div>
	
	
<input type="hidden" value="GV.{{ str_replace(' ', '_', $teacher->name) }}" id="teacherName"/>
@if ($isTeacher == 0)
    <input type="hidden" value="HS{{ $student->id }}.{{ str_replace(' ', '_', $student->name) }}" id="studentName"/>
@endif

<audio id="audioNotification" style="display:none;">
  <source src="{{ asset('learn-online/quite-impressed.mp3') }}" type="audio/mpeg">
</audio>

<script src="{{ asset('learn-online/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('learn-online/js/bootstrap.js') }}"></script>
<script src="{{ asset('learn-online/js/bootstrap.bundle.js') }}"></script>
<script src="{{ asset('learn-online/js/push_notification.js') }}"></script>

<script src="{{ asset('learn-online/js/RTCMultiConnection.js') }}"></script>
<script src="https://server.phonghoctructuyen.com/node_modules/canvas-designer/dev/webrtc-handler.js"></script>
<script src="https://server.phonghoctructuyen.com/node_modules/canvas-designer/canvas-designer-widget.js"></script>
<script src="https://server.phonghoctructuyen.com/socket.io/socket.io.js"></script>

<script src="{{ asset('learn-online/js/FileBufferReader.js') }}"></script>
<script src="{{ asset('learn-online/js/getMediaElement.js') }}"></script>
<script src="{{ asset('learn-online/js/getScreenId.js') }}"></script>
<script src="{{ asset('learn-online/js/video-screen-sharing.js') }}"></script>
<script>
	// ......................................................
// .......................UI Code........................
// ......................................................
	// var socket = io('https://server.phonghoctructuyen.com/');
	// socket.on('server-notification', function (data) {
		// console.log(data);
	// });
	// designs 
	// here goes canvas designer
	/* var designer = new CanvasDesigner();

	// you can place widget.html anywhere
	designer.widgetHtmlURL = 'https://server.phonghoctructuyen.com//node_modules/canvas-designer/widget.html';
	designer.widgetJsURL = 'https://server.phonghoctructuyen.com//node_modules/canvas-designer/widget.min.js'

	designer.addSyncListener(function(data) {
		connection.send(data);
	});

	designer.setSelected('pencil');

	designer.setTools({
		pencil: true,
		text: true,
		image: true,
		pdf: true,
		eraser: true,
		line: true,
		arrow: true,
		dragSingle: true,
		dragMultiple: true,
		arc: true,
		rectangle: true,
		quadratic: false,
		bezier: true,
		marker: true,
		zoom: true,
		lineWidth: true,
		colorsPicker: true,
		extraOptions: true,
		code: true,
		undo: true
	});
	
	function showArticle() {

		designer.appendTo(document.getElementById('widget-container'));

		if (connection.isInitiator === true) {
			setTimeout(afterOpenOrRoom, 5000);
		}
	}
	function afterOpenOrRoom() {
		// capture canvas-2d stream
		// and share in realtime using RTCPeerConnection.addStream
		// requires: dev/webrtc-handler.js
		designer.captureStream(function(stream) {
			stream.isScreen = false;
			stream.streamid = stream.id;
			stream.type = 'local';

			
			//var video = document.createElement('video');
			//video.muted = true;
			//video.srcObject = stream;
			//video.play();
	

			connection.attachStreams.push(stream);
			connection.onstream({
				stream: stream,
				type: 'local',
				streamid: stream.id,
				// mediaElement: video
			});
		});
	}*/
	
	@if ($isTeacher == 1)
	
	//socket.emit('client-notification', { my: 'teacher' });

    navigator.getMedia = ( navigator.getUserMedia || // use the proper vendor prefix
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia);

    navigator.getMedia({video: true}, function() {
        connection.session = {
            audio: true,
			video: true,
            data: true
        };

        connection.mediaConstraints = {
            audio: true,
			video: true,
        };
    }, function() {
        connection.session = {
            audio: true,

            data: true
        };

        connection.mediaConstraints = {
            audio: true,

        };
    });
	connection.sdpConstraints.mandatory = {
		OfferToReceiveAudio: true,
		OfferToReceiveVideo: true
	};
	document.getElementById('share-boardFirst').onclick = function() {
		this.remove();
		connection.addStream({
			screen: true,
			oneway: true
		});
		
	};

	// document.getElementById('share-boardSecond').onclick = function() {
		// // this.remove();
		// connection.addStream({
			// screen: true,
			// oneway: true
		// });
	// };

	// document.getElementById('share-boardThird').onclick = function() {
		// // this.remove();
		// connection.addStream({
			// screen: true,
			// oneway: true
		// });
	// };

	// document.getElementById('share-boardFourth').onclick = function() {
		// // this.remove();
		// connection.addStream({
			// screen: true,
			// oneway: true
		// });
	// };

	// document.getElementById('share-boardFifth').onclick = function() {
		// // this.remove();
		// connection.addStream({
			// screen: true,
			// oneway: true
		// });
	// };
	
	// document.getElementById('open-room').onclick = function() {
		// disableInputButtons();
		// this.remove();
		// onpenClassroom();
		// connection.open(document.getElementById('teacherName').value, function() {});
	// };
	$( document ).ready(function() { 
		connection.openOrJoin(document.getElementById('teacherName').value, function() {
			if (designer.pointsLength <= 0) {
				// make sure that remote user gets all drawings synced.
				setTimeout(function() {
					connection.send('plz-sync-points');
				}, 1000);
			}
			//showArticle();	
		});
	});
	@else
		
		@if ($userCounts > 10)
			connection.session = {
				audio: true,
				data: true
			};

			connection.mediaConstraints = {
				audio: true,
			};
		@else 
			
			navigator.getMedia = ( navigator.getUserMedia || // use the proper vendor prefix
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia);

			navigator.getMedia({video: true}, function() {
				connection.session = {
					audio: true,
					video: true,
					data: true
				};

				connection.mediaConstraints = {
					audio: true,
					video: true
				};
			}, function() {
				connection.session = {
					audio: true,
					data: true
				};

				connection.mediaConstraints = {
					audio: true,
				
				};
			});
		@endif

	// connection.sdpConstraints.mandatory = {
		// OfferToReceiveAudio: true,
		// OfferToReceiveVideo: true
	// };
	$( document ).ready(function() { 
		beforeJoinARoom(function() {
			setTimeout(function() {
				connection.join(document.getElementById('teacherName').value, function() {
					//showArticle();	
				});
			}, 5000);
			
        });
	});

	function beforeJoinARoom(callback) {
        connection.userid = $('#studentName').val();

        callback();
    }
	@endif
	document.getElementById('share-file').onclick = function() {
		var fileSelector = new FileSelector();
		fileSelector.selectSingleFile(function(file) {
			connection.send(file);
		});
	};
    function onpenClassroom() {
        var classroom = $('#room-id').val();
		console.log(classroom);
        $.ajax({
            async: false,
            type: "GET",
            url: '{!! route("teacher_open_class", ['classroomID' => $classroom->classroom_id, 'lessonId' => $lesson->lesson_id]) !!}',
            success: function(result){
                console.log('classroom_open');
            },
            error: function(error) {
                alert('Có lỗi gì đó xảy ra, vui lòng f5');
            }
        });
    }
	
	function disableInputButtons() {
		@if ($isTeacher == 1)
			document.getElementById('open-room').disabled = true;
			
		@else
			document.getElementById('join-room').disabled = true;
		@endif
		document.getElementById('room-id').disabled = true;
	}
	function endClassroom(e) {
		location.reload();
	}
	
	function myFunction() {
	popupWindow = window.open(
		"https://phonghoctructuyen.com/man-hinh-giao-vien/18/30",'_blank','height=211,width=295,left=10,top=10')
	}	
	
</script>

    </body>
</html>