<!DOCTYPE html>
<html lang="en">
<head>
    <title>Lớp học trực tuyến</title>
    <meta name="ROBOTS" content="index, follow"/>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="title" content="@yield('title')"/>
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta itemprop="image" content="@yield('meta_image')" />
    <!-- facebook gooogle -->
    <!-- <meta property="fb:app_id" content="" />

    <!-- css he thong -->
    <link rel="stylesheet" href="{{ asset('learn-online/css/bootstrap.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('learn-online/css/bootstrap-grid.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('learn-online/css/bootstrap-reboot.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('learn-online/css/fontawesome-all.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('learn-online/css/style_class_room.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('learn-online/css/getMediaElement.css') }}">


    <!-- tab -->
    <!-- script he thong -->
</head>

<body>
123123
<div class="row">
    <div class="col-12 col-md-9">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="boardFirst-tab" data-toggle="tab" href="#boardFirst" role="tab" aria-controls="home" aria-selected="true">Bảng số 1</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="boardSecond-tab" data-toggle="tab" href="#boardSecond" role="tab" aria-controls="profile" aria-selected="false">Bảng số 2</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="boardThird-tab" data-toggle="tab" href="#boardThird" role="tab" aria-controls="contact" aria-selected="false">Bảng số 3</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="boardFourth-tab" data-toggle="tab" href="#boardFourth" role="tab" aria-controls="contact" aria-selected="false">Bảng số 4</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="boardFifth-tab" data-toggle="tab" href="#boardFifth" role="tab" aria-controls="contact" aria-selected="false">Bảng số 5</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="boardFirst" role="tabpanel" aria-labelledby="boardFirst-tab">
                @if ($isTeacher == 1)
				<button id="share-boardFirst" disabled>Bật bảng số 1</button>
				@endif
                <div id="videos-boardFirst"></div>
            </div>
            <div class="tab-pane fade" id="boardSecond" role="tabpanel" aria-labelledby="boardSecond-tab">
				@if ($isTeacher == 1)
                <button id="share-boardSecond" disabled>Bật bảng số 2</button>
				@endif
                <div id="videos-boardSecond"></div>
            </div>
            <div class="tab-pane fade" id="boardThird" role="tabpanel" aria-labelledby="boardThird-tab">
                @if ($isTeacher == 1)
				<button id="share-boardThird" disabled>Bật bảng số 3</button>
				@endif
                <div id="videos-boardThird"></div>
            </div>
            <div class="tab-pane fade" id="boardFourth" role="tabpanel" aria-labelledby="boardFourth-tab">
                @if ($isTeacher == 1)
				<button id="share-boardFourth" disabled>Bật bảng số 4</button>
				@endif
                <div id="videos-boardFourth"></div>
            </div>
            <div class="tab-pane fade" id="boardFifth" role="tabpanel" aria-labelledby="boardFifth-tab">
                @if ($isTeacher == 1)
				<button id="share-boardFifth" disabled>Bật bảng số 5</button>
				@endif
                <div id="videos-boardFifth"></div>
            </div>
        </div>
    </div>

    <div class="col-12 col-md-3">
        <div class="studentAndTeacher">
            <div class="">
                <div class="make-center">
                    <input type="text" id="room-id" value="#{{ $classroom->classroom_id }}">
					@if ($isTeacher == 1)
						<button id="open-room" class="btn btn-danger">Open Room</button>
					@else
						<button id="join-room" class="btn btn-warning">Join Room</button>
					@endif
                    
					
                </div>
                <button class="btn btn-primary" onclick="return endClassroom(this);">Kết thúc</button> /
                <span>1:50:36</span>
            </div>
            <div class="teacher">
                <div id="teacher"></div>
                <p>(GV) {{ $teacher->name }}</p>
            </div>
            <div>
                <ul class="nav nav-tabs" id="studentTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="join-tab" data-toggle="tab" href="#join" role="tab" aria-controls="home" aria-selected="true">Tham gia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="question-tab" data-toggle="tab" href="#question" role="tab" aria-controls="profile" aria-selected="false">Đặt câu hỏi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="document-tab" data-toggle="tab" href="#document" role="tab" aria-controls="contact" aria-selected="false">Tài liệu</a>
                    </li>
                </ul>
                <div class="tab-content" id="studentTabContent">
                    <div class="tab-pane fade show active" id="join" role="tabpanel" aria-labelledby="join-tab">
                        <div id="student-video"></div>
                    </div>
                    <div class="tab-pane fade" id="question" role="tabpanel" aria-labelledby="question-tab">
                        <div class="contentChat">
						</div>
						<input type="text" value="" class="form-control" id="inputQuestion" placeholder="Đặt câu hỏi ở đây"/>
                    </div>
                    <div class="tab-pane fade" id="document" role="tabpanel" aria-labelledby="document-tab">
                        <button id="share-file" class="btn btn-primary" >Chia sẻ file</button>
						<div id="file-container">
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" value="t.{{ $teacher->name }}" id="teacherName"/>
@if ($isTeacher == 0)
    <input type="hidden" value="s{{ $student->student_id }}.{{ $student->name }}" id="studentName"/>
@endif


<script src="{{ asset('learn-online/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('learn-online/js/bootstrap.js') }}"></script>
<script src="{{ asset('learn-online/js/bootstrap.bundle.js') }}"></script>

<script src="{{ asset('learn-online/js/RTCMultiConnection.js') }}"></script>
<script src="https://server.phonghoctructuyen.com/socket.io/socket.io.js"></script>
<script src="{{ asset('learn-online/js/FileBufferReader.js') }}"></script>
<script src="{{ asset('learn-online/js/getMediaElement.js') }}"></script>
<script src="{{ asset('learn-online/js/getScreenId.js') }}"></script>
<script>
	// ......................................................
// .......................UI Code........................
// ......................................................
	@if ($isTeacher == 1)
	document.getElementById('share-boardFirst').onclick = function() {
		this.disabled = true;
		connection.addStream({
			screen: true,
			oneway: true
		});
		
	};

	document.getElementById('share-boardSecond').onclick = function() {
		this.disabled = true;
		connection.addStream({
			screen: true,
			oneway: true
		});
	};

	document.getElementById('share-boardThird').onclick = function() {
		this.disabled = true;
		connection.addStream({
			screen: true,
			oneway: true
		});
	};

	document.getElementById('share-boardFourth').onclick = function() {
		this.disabled = true;
		connection.addStream({
			screen: true,
			oneway: true
		});
	};

	document.getElementById('share-boardFifth').onclick = function() {
		this.disabled = true;
		connection.addStream({
			screen: true,
			oneway: true
		});
	};
	
	document.getElementById('open-room').onclick = function() {
		disableInputButtons();
		connection.open(document.getElementById('teacherName').value, function() {});
	};
	
	@else
	document.getElementById('join-room').onclick = function() {
		disableInputButtons();
		beforeJoinARoom(function() {
            connection.join(document.getElementById('teacherName').value, function() {});
        });

	};
	function beforeJoinARoom(callback) {
        connection.userid = $('#studentName').val();

        callback();
    }
	@endif
	document.getElementById('share-file').onclick = function() {
		var fileSelector = new FileSelector();
		fileSelector.selectSingleFile(function(file) {
			connection.send(file);
		});
	};
    function onpenClassroom() {
        var classroom = $('#teacherName').val();
        $.ajax({
            async: false,
            type: "GET",
            url: '{!! route("teacher_open_class", ['classroomCode' => $classroom->code]) !!}',
            data: {
                classroom: classroom
            },
            success: function(result){
                console.log('classroom_open');
            },
            error: function(error) {
                alert('Có lỗi gì đó xảy ra, vui lòng f5');
            }
        });
    }
	
	function disableInputButtons() {
		@if ($isTeacher == 1)
			document.getElementById('open-room').disabled = true;
			document.getElementById('share-boardFirst').disabled = false;
			document.getElementById('share-boardSecond').disabled = false;
			document.getElementById('share-boardThird').disabled = false;
			document.getElementById('share-boardFourth').disabled = false;
			document.getElementById('share-boardFifth').disabled = false;
		@else
			document.getElementById('join-room').disabled = true;
		@endif
		document.getElementById('room-id').disabled = true;
	}
	function endClassroom(e) {
		location.reload();
	}
	
</script>
<script src="{{ asset('learn-online/js/video-screen-sharing.js') }}"></script>

</body>
</html>
