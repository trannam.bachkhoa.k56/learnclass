@extends('site.layout.site')

@section('title', $classroom->name)
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <?php
    $user = \Illuminate\Support\Facades\Auth::user();
    $teacher = \App\Entity\Teacher::detailTeacher($user->id);
    ?>
    <section class="breadcrumb ds-inherit pd">
		<div class="bgbread">
			<div class="container">
				<div class="row">
					<div class="col-12 pdtop15">
						<h1>Lớp học {{ $classroom->name }}</h1>
						<ul>
							<li><a href="">Trang chủ</a></li>
							<li>/</li>
							<li><a href="">Lớp học {{ $classroom->name }}</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
    </section>

    <section class="teacher" style="background-color: #e5e5e5;">
        <div class="container">
            @include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-12 sidebarContact">
                    @include('site.module_learnclass.menu_user')
                </div>
                <div class="col-lg-9 col-md-8 col-sm-12 col-12">
                    @if (isset($teacher->teacher_id) )
                            <div class="row contentRgihtInfo pd20 bgwhite mgbottom15">
                                <div class="col-lg-3 pd">
                                    <div class="img">
                                        <div class="CropImg CropImg80">
                                            <div class="thumbs">
                                                <a href="{{ route('lesson', ['classroomSlug' => $classroom->slug, 'classroomId' => $classroom->classroom_id ]) }}" title="">
                                                    <img src="{{ isset($classroom->image) ? asset($classroom->image) : asset('images/noiimg.png') }}" class="">
                                                </a>
                                            </div>
                                          
                                        </div>
                                          @php
                                                $dateStart = $classroom->started_date.' '.$classroom->started_time;
                                                $timeStart = strtotime($dateStart);
                                                $dateEnd = $classroom->ended_date;
                                                $timeEnd = strtotime($dateEnd);
                                            @endphp
                                            @if ($classroom->recruitment == 'tuyen-dung')
                                                <div class="public item">Đang tuyển sinh</div>
                                            @elseif ($timeStart > time())
                                               <div class="public item">Sắp bắt đầu</div>
                                            @elseif ( $timeStart <= time() && $timeEnd >= time() )
                                                <div class="start item">Đang diễn ra</div>
                                            @else
                                                <div class="complete item">Đã hoàn thành</div>
                                            @endif
                                    </div>

                                </div>
                                <div class="col-lg-9 pd00">
                                    <div class="content contentTop text-lt mgleft25 mdmgleft0">
                                        <h3 class="text-ca f26">{{ $classroom->title }}</h3>
                                        <p class="clred f16 price"><span class="mgright15 text">{{ (!empty($classroom->price)) ? number_format($classroom->price, 0, ',', '.').' đ' : 'Miễn phí' }}</span></p>
                                        <ul>
                                            <li><a href=""><i class="fas fa-users"></i>Giới hạn học sinh: {{ $classroom->min_student }} người</a></li>
                                            <li><a href=""><i class="far fa fa-user"></i>Số người đã đăng ký: 5</a></li>
                                            <li><a href=""><i class="fas fa-clock"></i>{{ ($classroom->recruitment == 'tuyen-dung') ? 'Dự kiến' : 'Bắt đầu' }}: {{ App\Ultility\Ultility::formatDateTime($classroom->started_time, $classroom->started_date) }}</a></li>
                                        </ul>
                                        <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" class="ds-inline f15 clwhite bgorang text-up text-b700 pd-5 pd-010 mgright5 posedit mdds-none mbds-none">
                                            <i class="far fa-edit"></i> SỬA KHÓA HỌC
                                        </a>

                                        <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" class="ds-inline f15 clwhite bgorang text-up text-b700 pd-5 pd-010 mgright5 posedit ds-none mdds-block mbds-none">
                                            <i class="far fa-edit"></i> SỬA KHÓA HỌC
                                        </a>

                                        <a  data-toggle="collapse" href="#sendMail" role="button" aria-expanded="false" aria-controls="sendMail" class="ds-inline f15 clwhite bgorang text-up br text-b700 pd-5 pd-010 mgright5 poseditmobi ">
                                            <i class="far fa-envelope"></i> Gửi mail cho học sinh
                                        </a>

                                        <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" class="ds-inline f15 clwhite bgorang text-up text-b700 pd-5 pd-010 mgright5 ds-none mdds-block mgtop5">
                                            <i class="far fa-edit"></i> SỬA KHÓA HỌC
                                        </a>

                                    </div>

                                </div>
                                <div class="col-lg-12 pd">
                                    <div class="listClassInfo text-lt">
                                        <div class="collapse pdbottom30 mgleft25 mdmgleft0" id="sendMail">
                                            <form action="{{ route('teacher_send_mail_user') }}" method="post" class="mgtop20">
                                                {!! csrf_field() !!}
                                                <div class="box-body">
                                                    <input type="hidden" value="{{ $classroom->classroom_id }}" name="classroom_id"/>
                                                    <div class="form-group">
                                                        <label>Chủ đề</label>
                                                        <input type="text" class="form-control" name="subject" placeholder="Chủ đề">
                                                    </div>
                                                    <div>
                                                        <label>Nội dung</label>
                                                        <textarea class="editor" id="content" name="content" rows="10" cols="80" placeholder="Nội dung"/></textarea>
                                                    </div>

                                                </div>
                                                <div class="box-footer clearfix">
                                                    <button type="submit" class="pull-right btn btn-default mgtop15 bgorang clwhite br" id="sendEmail">Gửi mail
                                                        <i class="fa fa-arrow-circle-right"></i></button>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="collapse" id="collapseExample">
                                            <div class="card-body pd00">
                                                <h3 class="f24 mgbottom0 mgtop25">Thông tin khóa học</h3>
                                                <form class="bgwhite pd15 mg-30" style="border: 1px solid #ddd; box-shadow: 0 1px 1px rgba(0, 0, 0, .05);" action="{{ route('update_classroom', ['classroomId' => $classroom->classroom_id ]) }}" method="post" enctype="multipart/form-data">
                                                    {!! csrf_field() !!}
                                                    <div class="form-group row">
                                                        <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Tên lớp học</span><span class="clred pd-05">(*)</span></label>
                                                        <div class="col-lg-10 col-md-8 col-sm-8">
                                                            <input type="text" class="form-control f14" name="name" placeholder="Tên lớp học" value="{{ $classroom->title }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Ngày dự kiến bắt đầu học</span><span class="clred pd-05">(*)</span></label>
                                                        <div class="col-lg-10 col-md-8 col-sm-8">
                                                            <input id="date" type="date" value="{{ $classroom->started_date }}" name="started_date">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Giờ dự kiến bắt đầu học</span><span class="clred pd-05">(*)</span></label>
                                                        <div class="col-lg-10 col-md-8 col-sm-8">
                                                            <input id="date" type="time" value="{{ $classroom->started_time }}" name="started_time">
                                                            <i>SA: sáng, CH: chiều</i>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Số buổi học</span><span class="clred pd-05">(*)</span></label>
                                                        <div class="col-lg-10 col-md-8 col-sm-8">
                                                            <input type="number" min="0" class="form-control f14" name="number_lesson" placeholder="Số buổi học" value="{{ $classroom->number_lesson }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Số học viên tối đa</span><span class="clred pd-05">(*)</span></label>
                                                        <div class="col-lg-10 col-md-8 col-sm-8">
                                                            <input type="number" min="0" class="form-control f14" name="min_student" placeholder="Số học viên tối đa" value="{{ $classroom->min_student }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Hình thức tuyển sinh</span><span class="clred pd-05">(*)</span></label>
                                                        <div class="col-lg-10 col-md-8 col-sm-8">
                                                            <select name="recruitment" class="form-control">
                                                                <option value="tuyen-dung" {{ ($classroom->recruitment == 'tuyen-dung') ? 'selected' : '' }}>Tuyển sinh xong mới dạy</option>
                                                                <option value="day" {{ ($classroom->recruitment == 'day') ? 'selected' : '' }}>Dạy luôn</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Ảnh nền lớp học</span><span class="clred pd-05"></span></label>
                                                        <div class="col-lg-10 col-md-8 col-sm-8">
                                                            <img id="blah"  src="{{ !empty($classroom->image) ? asset($classroom->image) : asset('khoahoc/images/avatar.png') }}" alt="" width="150">
                                                            <button class="btn btn-default addAvatar">Tải ảnh lên</button>
                                                            <input type='file' id="imgInp" name="image" onchange="readURL(this)" style="display: none"/>
                                                            <input type="hidden" value="{{ $classroom->image }}" name="avatar" />
                                                            <script>
                                                                function readURL(input) {
                                                                    if (input.files && input.files[0]) {
                                                                        var reader = new FileReader();

                                                                        reader.onload = function(e) {
                                                                            $('#blah').attr('src', e.target.result);
                                                                        }

                                                                        reader.readAsDataURL(input.files[0]);
                                                                    }
                                                                }
                                                                $('.addAvatar').click(function() {
                                                                    $('#imgInp').click();
                                                                    return false;
                                                                });
                                                            </script>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Chọn cấp học</span><span class="clred pd-05">(*)</span></label>
                                                        <div class="col-lg-10 col-md-8 col-sm-8">
                                                            <select name="filter[]" class="form-control f14">
                                                                @foreach(\App\Entity\FilterGroup::showFilterGroup() as $id => $filterGroup)
                                                                    @if ($filterGroup->group_name != 'Các Môn Học')
                                                                        <option  disabled> {{ $filterGroup->group_name }}</option>
                                                                        @foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
                                                                            <option value="{!! $filter->name_filter !!}"
                                                                                    @if ( !empty($classroom->filter) && in_array($filter->name_filter, explode(',', $classroom->filter )) > 0 ) selected @endif >
                                                                                {!! $filter->name_filter !!}
                                                                            </option>
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Chọn môn học</span><span class="clred pd-05">(*)</span></label>
                                                        <div class="col-lg-10 col-md-8 col-sm-8">
                                                            <select name="filter[]" class="form-control f14">
                                                                @foreach(\App\Entity\FilterGroup::showFilterGroup() as $id => $filterGroup)
                                                                    @if ($filterGroup->group_name == 'Các Môn Học')
                                                                        @foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
                                                                            <option value="{!! $filter->name_filter !!}"
                                                                                    @if ( !empty($classroom->filter) && in_array($filter->name_filter, explode(',', $classroom->filter ) ) > 0 ) selected @endif >
                                                                                {!! $filter->name_filter !!}
                                                                            </option>
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Mô tả lớp học</span><span class="clred pd-05">(*)</span></label>
                                                        <div class="col-lg-10 col-md-8 col-sm-8">
                                                            <textarea class="form-control" name="description" rows="3">{{ $classroom->description }}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Giá</span><span class="clred pd-05">(*)</span></label>
                                                        <div class="col-lg-10 col-md-8 col-sm-8">
                                                            <input type="number" min="0" class="form-control f14" name="price" placeholder="Giá" value="{{ $classroom->price }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Nội dung lớp học</span><span class="clred pd-05">(*)</span></label>
                                                        <div class="col-lg-10 col-md-8 col-sm-8">
                                                            <textarea class="form-control editor" id="contentClassroom" name="content" rows="3">{{ $classroom->content }}</textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        @foreach (explode(',', $classroom->files) as $file)
                                                            @if (!empty($file))
                                                                <div class="row mgbottom5">
                                                                    <div class="col-2">
                                                                        File tài liệu
                                                                    </div>
                                                                    <div class="col-8">
                                                                        <input type="hidden" name="filenameOld[]" value="{{ $file }}">
                                                                        <p><i class="fas fa-file"></i> {{ str_replace("upload/files/","",$file) }}</p>
                                                                    </div>
                                                                    <div class="col-2">
                                                                        <a href="{{ asset($file) }}">
                                                                            <button type="button" class="btn btn-primary"><i class="fas fa-download"></i></button>
                                                                        </a>
                                                                        <button type="button" class="btn btn-danger" onclick="return removeFiles(this);"> <i class="far fa-trash-alt bgred"></i></button>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                    <script>
                                                        function removeFiles (e) {
                                                            $(e).parent().parent().remove();
                                                        }
                                                    </script>
                                                    <div class="form-group row">
                                                        <label for="staticEmail" class="col-lg-2 col-md-4 col-sm-4 col-form-label"><span class="text-b700">Tệp thông tin lớp học</span><span class="clred pd-05"></span></label>
                                                        <div class="col-lg-10 col-md-8 col-sm-8">
                                                            <div class="input-group control-group increment" >
                                                                <input type="file" name="filename[]" value="" class="form-control" >
                                                                <div class="input-group-btn">
                                                                    <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>  Thêm tệp</button>
                                                                </div>
                                                            </div>
                                                            <div class="clone hide">
                                                                <div class="control-group input-group" style="margin-top:10px">
                                                                    <input type="file" name="filename[]" value="" class="form-control">
                                                                    <div class="input-group-btn">
                                                                        <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Xóa tệp</button>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <script type="text/javascript">

                                                                $(document).ready(function() {

                                                                    $(".btn-success").click(function(){
                                                                        var html = $(".clone").html();
                                                                        $(".increment").after(html);
                                                                    });

                                                                    $("body").on("click",".btn-danger",function(){
                                                                        $(this).parents(".control-group").remove();
                                                                    });

                                                                });

                                                            </script>
                                                            <style>
                                                                .hide {
                                                                    display: none;
                                                                }
                                                            </style>
                                                        </div>
                                                        <p><i>Vui lòng nhập tài liệu tiếng việt không có dấu và không chứa dấu (,).</i></p>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-lg-2 col-md-4 col-sm-4"></div>
                                                        <div class="col-lg-10 col-md-8 col-sm-8 pdtop30">
                                                            <button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite ">Chỉnh sửa lớp học</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <table class="table table-striped mg-0 mgtop20 tablePH">
                                            <tbody class="text-up text-ct text-b">

                                            @foreach ($lessons as $id => $lesson)
                                                @php
                                                    $dateStart = $lesson->date_at.' '.$lesson->time_start;
                                                    $timeStart = strtotime($dateStart);
                                                    $dateEnd = $lesson->date_at.' '.$lesson->time_end;
                                                    $timeEnd = strtotime($dateEnd);
                                                @endphp
                                                <tr class="f16 mdf13 mbf12">
												@php
													if ($timeEnd < time())
                                                        $bgColor = 'bggrey';     
                                                    else if ( $timeEnd >= time() && $timeStart <= time() )
                                                        $bgColor = 'bgxanhlacay';       
                                                    else
                                                        $bgColor = 'bgorange';    
                                                @endphp
                                                    <td class="w20 {{ $bgColor }}" >
                                                        <i class="fas fa-calendar-alt clwhite f28 vertop mgtop10 mbmgbottom5"></i>
                                                        <span class="f15 mdf12 ds-inline pdleft10 mdpd-05 mgtop8">
                                                            <p class="mgbottom0 text-b700 clwhite br-bottom brcl-black ">{{ \App\Ultility\Ultility::formatTime($lesson->time_start) }}</p>
                                                            <p class="date mgbottom0 clwhite">{{ \App\Ultility\Ultility::formatDate($lesson->date_at) }}</p>
                                                        </span>
                                                    </td>
                                                    <td class="clxanhdam text-lt w40 "><span class="pd10 ds-block">Buổi {{ ($id + 1) }}  : {{ $lesson->title }}</span></td>

                                                    @if ($timeEnd < time())
                                                        <td class="clwhite bggrey w25"><span class="pdtop10 ds-block">Đã hoàn thành</span></td>
                                                    @elseif ($timeEnd > time() && $timeStart < time()
                                                    && \Illuminate\Support\Facades\Auth::check()
                                                    && $teacher->user_id == \Illuminate\Support\Facades\Auth::user()->id )
                                                        <td class="clwhite bgxanhlacay">
                                                            <a href="/phong-hoc/{{ $classroom->classroom_id }}/{{ $lesson->lesson_id }}">
                                                                <span class="pdtop10 ds-block">Mở lớp học</span>
                                                            </a>
                                                        </td>
                                                    @elseif ( $timeEnd > time() && $timeStart < time() && $isStudent == false)
                                                        <td class="clwhite bgred"><span class="pdtop10 ds-block">Đang diễn ra</span></td>
                                                    @elseif ($timeEnd > time() && $timeStart < time() && $isStudent == true )
                                                        <td class="clwhite bgxanhlacay">
                                                            <a href="/hoc-sinh-vao-phong-hoc/{{ $product->classroom_id }}/{{ $lesson->lesson_id }}">
                                                                <span class="pdtop10 ds-block">Vào buổi học</span>
                                                            </a>
                                                        </td>
                                                    @else
                                                        <td class="clwhite bgorange"><span class="pdtop10 ds-block">Sắp bắt đầu</span></td>
                                                    @endif
                                                    <td class="w20">
                                                        <a href="{{ route('edit_lesson', ['classroomId' => $classroom->classroom_id, 'lesson_id' => $lesson->lesson_id]) }}" class="f18 ds-inline clblack"><i class="far fa-edit"></i></a>
                                                        <a href="{{ route('destroy_lesson', ['classroomId' => $classroom->classroom_id, 'lesson_id' => $lesson->lesson_id]) }}" class="f18 ds-inline clblack"><i class="far fa-trash-alt"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                        <a href="{{ route('register_lesson', ['classroomSlug' => $classroom->slug, 'classroomId' => $classroom->classroom_id]) }}"><button class="ds-inline f15 clwhite bgorang text-up text-b700 pd-10 pd-025 mgright5 posedit mdds-none mgtop20">Thêm mới buổi học</button></a>

                                        <div class="teacherRight clblack text-lt pdleft10 mgtop40">
                                            <h4 class="f20 text-bnone">Danh sách học sinh</h4>
                                            <table class="table table-striped">
                                                <tbody>
                                                @foreach ($users as $user)
                                                    <tr class="text-ct">
                                                        <td class="text-lt">
                                                            {{ $user->name }}
                                                        </td>
                                                        <td>{{ $user->address }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    @endif
                </div>
            </div>
        </div>
    </section>


@endsection