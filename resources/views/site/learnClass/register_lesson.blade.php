@extends('site.layout.site')

@section('title', 'Đăng ký buổi học '.$classroom->name )
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <?php
    $user = \Illuminate\Support\Facades\Auth::user();
    $teacher = \App\Entity\Teacher::detailTeacher($user->id);
    ?>
    <section class="breadcrumb ds-inherit pd">
		<div class="bgbread">
			<div class="container">
				<div class="row">
					<div class="col-12 pdtop15">
						<h1>Đăng ký lớp học {{ $classroom->name }}</h1>
						<ul>
							<li><a href="">Trang chủ</a></li>
							<li>/</li>
							<li><a href="">Đăng ký lớp học {{ $classroom->name }}</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
    </section>

    <section class="teacher" style="background-color: #e5e5e5;">
        <div class="container">
            @include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-12 sidebarContact">
                    @include('site.module_learnclass.menu_user')
                </div>
                <div class="col-lg-9 col-md-8 col-sm-12 col-12">
                    <div class="teacherRight bgwhite clblack text-lt pdleft10">
                        <h2 class="f26">Đăng ký buổi học {{ $classroom->name }}</h2>
                        <form class=" pd15 mg-30" style="border: 1px solid #ddd; box-shadow: 0 1px 1px rgba(0, 0, 0, .05);" action="{{ route('register_lesson_post') }}" method="post" enctype="multipart/form-data" id="register_class">
                            {!! csrf_field() !!}
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Tên buổi học</span><span class="clred pd-05">(*)</span></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control f14" name="title" placeholder="Tên buổi học" value="{{ old('title') }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Ngày bắt đầu</span><span class="clred pd-05">(*)</span></label>
                                <div class="col-sm-10">
                                    <input id="date" type="date" value="{{ old('date_at') }}" name="date_at" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Giờ bắt đầu</span><span class="clred pd-05">(*)</span></label>
                                <div class="col-sm-10">
                                    <input id="date" type="time" value="{{ old('time_start') }}" name="time_start" class="time_start" >
                                    <i>SA: sáng, CH: chiều</i>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Giờ kết thúc</span><span class="clred pd-05">(*)</span></label>
                                <div class="col-sm-10">
                                    <input id="date" type="time" value="{{ old('time_end') }}" name="time_end" class="time_end">
                                    <i>SA: sáng, CH: chiều</i>
                                    <i style="color: red" class="f12 ds-inline print_timeend pdleft10"></i>

                                   <!--  <br><i style="color: red" class="f12 ds-inline print_error pdleft10"></i> -->
                                </div>
                            </div>
                            

                            <input type="hidden" value="{{ $classroom->classroom_id }}" name="classroom_id" />
                            <div class="form-group row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10 pdtop30">
                                    <button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite btnloadding ">Đăng ký buổi học</button>

                                </div>
                            </div>
                        </form>
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $( "#register_class" ).submit(function( event ) {
                                    var dateend =  $("input[name=time_end]").val();
                                    var datestar = $("input[name=time_start]").val();
                                    // if($("input[name=title]") != '' && $("input[name=date_at]") != ''  && dateend != '' && datestar != '' )
                                    // {
                                    //     $('.print_error').html('Vui lòng nhập đầy đủ (*)');
                                    //     return false;        
                                    // }
                                    if(datestar < dateend  )
                                    {
                                        return true;
                                    }
                                    else
                                    {
                                        $('.print_timeend').html('Giờ kết thúc phải lớn hơn giờ bắt đầu');
                                        return false;
                                    }
                                });                                 
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection