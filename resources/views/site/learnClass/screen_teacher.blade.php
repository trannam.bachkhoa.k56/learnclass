<!DOCTYPE HTML>
<html>
<head>
    <meta property="fb:pages" content="" />
    <meta name="adx:sections" content="" />
    <meta name="p:domain_verify" content=""/>
    <meta name="google-site-verification" content="" />
    <meta name="google-site-verification" content="" />
    <meta http-equiv="content-type" content="text/html" />
    <meta charset="utf-8" />
    <title>
        Màn hình máy tính
    </title>
    <meta name="description" content="@yield('meta_description')" />
    <meta property="og:type" content="website">
    <meta property="og:title" content="@yield('title')">
    <meta property="og:image" content="@yield('meta_image')">
    <meta property="og:description" content="@yield('meta_description')">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <link rel="icon" href="{{ isset($information['icon']) ? asset($information['icon']) :'' }}" type="image/x-icon" />


    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/hover.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome-all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/extract.css') }}">

    <!-- SCSS -->
    <link rel="stylesheet" href="{{ asset('scss/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('scss/reponsive.css') }}">

    {{--CONVERT SCSS--}}
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('learn-online/css/style_class_room.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('learn-online/css/getMediaElement.css') }}">
    <!-- JS -->

    <script src="{{ asset('js/jquery3.1.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/transition.min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    {{--<script src="{{ asset('js/WOW.js') }}"></script>--}}

    <script type="text/javascript" src="{{ asset('js/numeral.min.js') }}"></script>
    <!-- CK Editor -->
    <script src="{{ asset('adminstration/ckeditor/ckeditor.js') }}"></script>

</head>
    <body>
    <section class="Learing">
        <div class="row contentLearning">
            <div class="col-md-12 boxFull">
                <div class="avatarTeacher box">
                    <div id="teacher">
                        <img src="http://www.jbcnschool.edu.in/wp-content/uploads/2017/08/a-kindergarten-teacher.jpg"/>
                    </div>
                </div>
            </div>
        </div>
    </section>

<input type="hidden" value="screen_teacher.{{ $teacher->name }}" id="teacherName"/>

<div id="inputQuestion"></div>

<script src="{{ asset('learn-online/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('learn-online/js/bootstrap.js') }}"></script>
<script src="{{ asset('learn-online/js/bootstrap.bundle.js') }}"></script>

<script src="{{ asset('learn-online/js/RTCMultiConnection.js') }}"></script>
<script src="https://server.phonghoctructuyen.com/socket.io/socket.io.js"></script>
<script src="{{ asset('learn-online/js/FileBufferReader.js') }}"></script>
<script src="{{ asset('learn-online/js/getMediaElement.js') }}"></script>
<script src="{{ asset('learn-online/js/getScreenId.js') }}"></script>

<script>
	// ......................................................
// .......................UI Code........................
// ......................................................
	
	$( document ).ready(function() { 
		connection.open(document.getElementById('teacherName').value, function() {});
	});
		
</script>
<script src="{{ asset('learn-online/js/video-screen-sharing.js') }}"></script>
    </body>
</html>



