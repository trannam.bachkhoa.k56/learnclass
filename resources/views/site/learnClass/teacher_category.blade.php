@extends('site.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
	<section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1>Giáo viên tại Phòng Học Trực Tuyến</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Danh sách giáo viên</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="categorySingle mg-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-12 col-sm-12 col-12">
                    <div class="left mdds-no  mdtext-ct">
                        <img src="images/bannerteacher.jpg" alt="" class="pd5 br brcl-grey">
                    </div>
                </div>
                <div class="col-lg-9 col-md-12 sol-sm-12 col-12">
                    <h2 class="f28 clblack ds-bolck br-bottom brcl-grey pdbottom10 text-b text-up mdtext-ct mdpdtop25">Đội ngũ giáo viên chuyên nghiệp</h2>
                    <p class="text-js f14 clblack">• Với khát vọng trở thành đơn vị đi đầu trong đổi mới giáo dục tại Việt Nam, Phonghoctructuyen.com chú trọng đầu tư chiều sâu vào việc phát triển năng lực đội ngũ giáo viên để đảm bảo chương trình giáo dục ưu việt của toàn bộ hệ thống phải được triển khai bởi những nhà giáo hội tụ đủ Tâm - Tầm - Tài, cùng vững bước đồng hành trên hành trình đầy vinh quang và thử thách này. <br><br>
					• Cơ hội được đào tạo, được phát triển chuyên môn không ngừng mỗi ngày. Phonghoctructuyen.com đặc biệt quan tâm tới các chương trình đào tạo giáo viên để chuẩn hoá đội ngũ cũng như không ngừng nâng cao chuyên môn thông qua các chương trình đào tạo dạy học trực tuyến</p>
                </div>
            </div>
        </div>
    </section>

    <section class="listteach mg-50">
        <div class="container">
            <div class="row justify-content-lg-center">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <h3 class="f28 clblack ds-bolck text-b text-ct pdbottom20">Giáo viên</h3>
                    <div class="row justify-content-lg-center mgbottom30">
                        <div class="col-12 teacherFillter">
                            <form action="" method="get" class="search submitSearch" accept-charset="utf-8">
                               <div class="row">
                                   <div class="col-xl-4 col-lg-4 col-md-12">
                                       <div class="form-group row">
                                            <label for="staticEmail" class="col-xl-3 col-md-3 col-4 col-form-label f16 text-ct">Lĩnh vực</label>
                                            <div class="col-xl-8 col-md-9 col-8 rightinput">
                                                <select class="form-control" name="field" onchange="return submitSearch(this);">
                                                    <option value="">----------------</option>
                                                    @foreach(\App\Entity\SubPost::showSubPost('linh-vuc-giang-day',1000) as  $id=> $field)
                                                    <option value="{{ $field->title }}" @if(!empty($_GET['field'])){{ ($field->title == $_GET['field']) ? 'selected' : '' }} @endif > <i class="far fa-arrow-alt-circle-right"></i> {{ $field->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                       
                                   </div>
                                   <div class="col-xl-4 col-lg-4 col-md-12">
                                        <div class="form-group row">

                                            <label for="staticEmail" class="col-xl-3 col-md-3 col-4 col-form-label f16 text-ct "> Hình thức </label>
                                            <div class="col-xl-8 col-md-9 col-8 rightinput">
                                                {{--<input type="text" class="form-control" value="{{ (!empty($_GET['academic'])) ? $_GET['academic'] : '' }}"--}}
                                                       {{--placeholder="Trình độ học vấn ..." name="academic" aria-label="Recipient's username" aria-describedby="button-addon2">--}}
                                                <select class="form-control" name="academic" onchange="return submitSearch(this);">
                                                    <option value="">----------------</option>
                                                    <option value="Giáo viên" @if(!empty($_GET['academic'])){{ (($_GET['academic']) == 'Giáo viên') ? 'selected' : '' }} @endif><i class="far fa-arrow-alt-circle-right"></i> Giáo viên</option>
                                                    <option value="Gia sư" @if(!empty($_GET['academic'])){{ (($_GET['academic']) == 'Gia sư') ? 'selected' : '' }} @endif><i class="far fa-arrow-alt-circle-right"></i> Gia sư</option>
                                                </select>
                                            </div>
                                        </div>

                                   </div>
                                   <div class="col-xl-4 col-lg-4 col-md-12 position">
                                        <div class="input-group mb-8">
                                             <input type="text" autocomplete="off" class="form-control ipSearch" placeholder="Nhập tên giáo viên cần tìm ..." name="word" onKeyDown="javascript:searchAjax(event);"
                                                    onclick="return clickSearch(this);" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}" aria-label="Recipient's username" aria-describedby="button-addon2">
                                            <div class="input-group-append">
                                              <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Tìm kiếm</button>
                                            </div>
                                        </div>

                                        <div class="submitFrom" style="z-index: 9999">
                                        </div>
                                   </div>
                               </div>
                                <script>
                                    function searchAjax(e) {
                                        $.ajax({
                                            type: "get",
                                            url: '/tim-kiem-giao-vien-ajax',
                                            data: {
                                                word: $('.ipSearch').val() != '' ? $('.ipSearch').val(): '',
                                            },
                                            success: function(result){
                                                var obj = $.parseJSON( result);
                                                $('.search .submitFrom ').empty();

                                                if (obj.status == '404') {
                                                    return false;
                                                }

                                                $(".search .submitFrom").show();
                                                $.each(obj.products, function(index, element) {

                                                    var html = '<div class="content mg-5">';
                                                    html += '<ul><li class="pd-2"><a href="/thong-tin-chi-tiet-giao-vien/'+ element.teacher_id +'"><img src="'+ element.image +'" alt="'+ element.name +'" width="50px"/> Giáo viên: '+ element.name +'<span class="pdleft10"> ';
                                                    html += '</span></a></li></ul>';
                                                    html += '</div>';

                                                    $('.search .submitFrom ').append(html);

                                                });

                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {

                                            }

                                        });

                                        return false;
                                    }
                                    function clickSearch(e){
                                        if( $(e).val() ) {
                                            $(".search .submitFrom").show();
                                        }
                                    }
                                    function submitSearch(e) {
                                        $('.submitSearch').submit();
                                    }
                                </script>
                            </form>

                        </div> 
                    </div>
                    <div class="row">
                        @foreach ($teachers as $teacher)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                @include('site.module_learnclass.teacher_item', ['teacher' => $teacher])
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="col-12 Pagecate">
                    @if($teachers instanceof \Illuminate\Pagination\LengthAwarePaginator )
                        {{ $teachers->links() }}
                    @endif
                </div>
            </div>
        </div>

    </section>
@endsection