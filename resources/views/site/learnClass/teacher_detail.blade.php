@extends('site.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <section class="breadcrumb ds-inherit pd">
		<div class="bgbread">
			<div class="container">
				<div class="row">
					<div class="col-12 pdtop15">
						<h1>Giáo viên {{ $teacher->name }}</h1>
						<ul>
							<li><a href="">Trang chủ</a></li>
							<li>/</li>
							<li><a href="">Giáo viên {{ $teacher->name }}</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
    </section>
    <style type="text/css">
        

    </style>
    <section class="teacher bggray pdtop30 pdbottom30">
        <div class="container">
            <div class="row bgwhite">
                <div class="col-lg-3 col-md-4 col-sm-12 col-12 sideTeacher">
                    <div class="content pd-20">
                       
                        <div class="img">
                            <div class="CropImg">
                                <div class="thumbs">
                                    <a href="/thong-tin-chi-tiet-giao-vien/22" title="">
                                         <img src="{{ isset($teacher->image) && !empty($teacher->image) ? asset($teacher->image) : asset('images/no_teacher.png')  }}" alt="{{ $teacher->name }}" class="">
                                    </a>
                                </div>
                            </div>
                           
                        </div>
                        <div class="desTeacher f16">
                            <div class="rating mg-15">
                                <span class="f16 text-b700">Đánh giá</span>
                                <div class="icon ds-inline pd-010">
                                    <span class="star teacher-old-rating{{ $teacher->teacher_id }}"></span>
                                    <input type='hidden' value="{{ \App\Entity\StudentAppraiseTeacher::countAppraise($teacher->teacher_id) }}" class="teacherAppraise{{ $teacher->teacher_id }}"/>
                                    <script>
                                        //Đồng bộ chiều cao các div
                                        $(function() {
                                            $('.boxP').matchHeight();
                                        });
                                        $(function() {
                                            var appraise = $(".teacherAppraise{{  $teacher->teacher_id }}").val();
                                            $(".teacher-old-rating{{ $teacher->teacher_id }}").starRating({
                                                initialRating: appraise,
                                                strokeColor: '#894A00',
                                                starSize: 30,
                                                readOnly: true
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="clxam">({{ \App\Entity\StudentAppraiseTeacher::totalAppraise($teacher->teacher_id) }} đánh giá)</div>
                            </div>

                            <!-- <div class="tag">
                                <a href="">Thiết kế</a>
                                <a href="">Lập trình</a>
                                <a href="">Toán lớp 12</a>
                            </div> -->

                            <div class="contentdes mdmgleft0 mbmgbottom30 pdtop20">
                                <div class="icon text-ct pdbottom20">
                                    <ul>
                                        <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-012 ds-block br br-orang"><i class="fab fa-facebook-f"></i></a></li>
                                        <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-twitter"></i></a></li>
                                        <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-12 col-12 rightTeacher text-lt pd-015">
                    <div class="content pd-20 f16">
                        <h2 class="f25 clblack text-b text-ca">{{ isset($teacher->name) ?$teacher->name : 'Đang cập nhật'}}</h2>
                        <p class="mgbottom5 destext">Hình thức giảng dạy : {{ isset($teacher->form_of_teaching) ?$teacher->form_of_teaching : 'Đang cập nhật'}}</p>
                        <p class="destext">Giảng dạy : {{ isset($teacher->teaching_subject) ?$teacher->teaching_subject : 'Đang cập nhật'}}</p>


                        <h2 class="f25 clblack text-b pdtop20 text-ca">Giới thiệu</h2>
                        <p>
                           {!! isset($teacher->description) ?$teacher->description  : 'Đang cập nhật thông tin' !!}
                        </p>

                        <div class="imgcheck arrow-left mdds-none">
                        </div>
                        <div class="iconposCheck mdds-none">
                             <img src="{{ asset('public/images/huychuong.png') }}">
                        </div>

                        <div class="clearfix mgbottom20">
                            <div class="orClass"><span>Mọi người đánh giá về giáo viên</span></div>
                        </div>
                        <div class="contentques bgques mgtop20 mgbottom20">
                                <div class="clblack f16 text-js bgwhite ">
                                    @foreach (\App\Entity\StudentAppraiseTeacher::showAppraise($teacher->teacher_id) as $appraiseTeacher) 
                                    <div class="item mgbottom10" style="border-bottom: 1px solide #e5e5e5">
                                        <span class="star teacher-people-rating{{ $appraiseTeacher->student_appraise_teacher_id }}"></span>
                                        <input type='hidden' value="{{ $appraiseTeacher->point }}" class="teacherPeopleAppraise{{ $appraiseTeacher->student_appraise_teacher_id }}"/>
                                        <script>
                                            $(function() {
                                                var appraise = $(".teacherPeopleAppraise{{ $appraiseTeacher->student_appraise_teacher_id }}").val();
                                                $(".teacher-people-rating{{ $appraiseTeacher->student_appraise_teacher_id }}").starRating({
                                                    initialRating: appraise,
                                                    strokeColor: '#894A00',
                                                    starSize: 30,
                                                    readOnly: true
                                                });
                                            });
                                        </script>
                                        <p><i>bởi {{ $appraiseTeacher->name }}</i> <span style="color: #4caf50"><i class="fas fa-user-shield"></i> Chứng nhận đã tham gia học</span></p>
                                        <p>{{ $appraiseTeacher->description }}</p>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                    </div>
                    
                </div>
                <!-- <div class="col-lg-3 col-md-4 col-sm-12 col-12 sidebarContact">
                    <div class="teacherLeft text-lt pd10 boxShadow bgwhite">
                        <div class="content position">
                            @if (isset($teacher->certification) && $teacher->certification== 1)
							    <div class="guaranteed"><img src="{{ asset('images/chung-nhan.png') }}"/></div>
                            @endif
                            <div class="CropImg CropImgV">
                                <div class="thumbs">
                                    <a href="" title="">
                                        <img src="{{ isset($teacher->image) && !empty($teacher->image) ? asset($teacher->image) : asset('images/no_teacher.png')  }}" alt="{{ $teacher->name }}" class="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="spec pdtop20">
                            <h3 class="f20 clback">Kĩ năng</h3>
                            <p class="des f14">Nghề nghiệp: {{ $teacher->current_job }}</p>
                            <p class="des f14">Hình thức giảng dạy: {{ $teacher->form_of_teaching }}</p>
                        </div>
                        <div class="spec pdtop20">
                            <h3 class="f20 clback">Thông tin</h3>
                            <p class="des f14">Lĩnh vực : {{ $teacher->teaching_field }}</p>
                            <p class="des f14">Bằng cấp : {{ $teacher->academic_standard }}</p>
                        </div>
                        <div class="contentdes mdmgleft0 mbmgbottom30 pdtop20">
                            <div class="icon text-lt pdbottom20">
                                <ul>
                                    <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-012 ds-block br br-orang"><i class="fab fa-facebook-f"></i></a></li>
                                    <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-twitter"></i></a></li>
                                    <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-9 col-md-8 col-sm-12 col-12 ">
					 <div class="teacherRight clblack text-lt pd10 bgwhite boxShadow">
						
                        <h2 class="f26">{{ $teacher->name }}</h2>
                        <h4 class="f16 text-bnone">{{ $teacher->current_job }} - {{ $teacher->where_working }}</h4>
                        <div class="des f14 text-js">
                            <p>{!! $teacher->description !!}</p>
                        </div>

                        <style>
                            .method
                            {
                                background: #777;
                                width: {{ $teacher->teaching_methods }}%;
                            }
                            .skills
                            {
                                background: #3adb76;
                                width: {{ $teacher->communication_skill }}%;
                            }
                            .exp
                            {
                                background: #ffae00;
                                width: {{ $teacher->experience }}%;
                            }
                            .indexs
                            {
                                width: {{ $teacher->numeral }}%;
                                background: #ec5840;
                            }
                        </style>
                    </div> -->
					
				</div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 ">
					<div class="joinclass">
							<h3 class="pdtop20">Khóa Học Của {{ $teacher->name }}</h3>
							<p>Thông tin khóa hoc</p>
							<div class="borderTitle">
								<div class="left"></div>
								<div class="center"><i class="fas fa-graduation-cap"></i></div>
								<div class="right"></div>
							</div>
							<div class="listtable mgtop20 row">
								@foreach ($products as $id => $product)
									<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 itemcol">
										@include('site.module_learnclass.product_item', ['$product' => $product])
									</div>
								@endforeach
							</div>
						</div>
                   
                </div>
            </div>
        </div>
    </section>
@endsection