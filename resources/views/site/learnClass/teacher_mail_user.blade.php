@extends('site.layout.site')

@section('title', 'Gửi mail cho học sinh')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <section class="resTeacher bgxamnhat">
        <div class="container">
            <div class="row justify-content-lg-center">
                <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                    <h2 class="clblack f24 pd-30 text-ct">
                       Giáo viên gửi mail cho học sinh
                    </h2>
                    <div class="bgwhite pd15 mg-30" style="border: 1px solid #ddd; box-shadow: 0 1px 1px rgba(0, 0, 0, .05); "> 
                    {!! $message !!}
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection