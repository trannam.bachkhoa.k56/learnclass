<div class="sidebarInfoUser bgwhite">
    <h2 class="f16 clwhite bgxanhdam text-ct w100 pd-10 pd-bottom15">DANH MỤC THÔNG TIN</h2>
    <div class="listView">
        <ul class="pd text-lt pd-15 pd-010">
            <li>
                <a href="/thong-tin-ca-nhan" class="f16  ds-block pd10 pd-015"><i class="fa fa-users-cog"></i> Thông tin tài khoản</a>
            </li>
            <li>
                <a href="{{ route('manegerteacher.index') }}" class="f16  ds-block pd10 pd-015"><i class="fas fa-chalkboard-teacher"></i> Quản lý giáo viên</a>
            </li>
            <li>

                <a href="{{ route('manegerstudent.index') }}" class="f16  ds-block pd10 pd-015"><i class="fas fa-graduation-cap"></i> Quản lý học sinh</a>
            </li>

            <li>
                <a href="/trang/khoa-hoc-da-dang-ky" class="f16  ds-block pd10 pd-015"><i class="fa fa-laptop"></i> Khóa học đã đăng ký</a>
            </li>
			<li>
                <a href="/trang/ma-gioi-thieu" class="f16  ds-block pd10 pd-015"><i class="far fa-laugh-beam"></i> Mã giới thiệu</a>
            </li>
            @if (\Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
                <li>
                    <a href="/trang/trang-thong-tin-giao-vien" class="f16  ds-block pd10 pd-015"><i class="fas fa-chalkboard-teacher"></i> Khóa học của tôi</a>
                </li>
            @endif
            @if (\Illuminate\Support\Facades\Auth::user()->user_type != 'giao_vien')
                <li>
                    <a href="/trang/dang-ky-giao-vien-khi-da-co-tai-khoan" class="f16  ds-block pd10 pd-015"><i class="fa fa-user-tie"></i>  Trở thành giảng viên</a>
                </li>
            @else
                
            @endif

            <li>
                <a href="/doi-mat-khau" class="f16  ds-block pd10 pd-015"><i class="fas fa-unlock-alt"></i> Đổi mật khẩu</a>
            </li>
            <li>
                <a href="{{ route('logoutHome') }}" class="f16  ds-block pd10 pd-015"><i class="fa fa-sign-out-alt"></i>  Đăng xuất</a>
            </li>
        </ul>
    </div>
</div>