<div class="itemclass">
    <div class="img">
        <div class="CropImg">
            <div class="thumbs">
                <a href="{{ route('product', ['post_slug' => $product->slug ]) }}" title="">
                    <img src="{{ isset($product->image) ? asset($product->image) : asset('images/noiimg.png') }}" alt="">
                </a>
            </div>
        </div>
        <div class="icon">
            <img src="{{ !empty($product->user_image) ? asset($product->user_image) : asset('/images/no_teacher.png')}}" alt="{{ $product->user_name }}">
            <span>{{ $product->user_name }}</span>
			<div id="Status" class="carousel slide status" data-ride="carousel">
			  <div class="carousel-inner">
				<div class="carousel-item">
					<div class="clock item"><i class="fa fa-calendar-alt"></i> {{ App\Ultility\Ultility::formatDateTime($product->started_time, $product->started_date) }}</div>
				</div>
				@php
					$dateStart = $product->started_date.' '.$product->started_time;
					$timeStart = strtotime($dateStart);
					$dateEnd = $product->ended_date;
					$timeEnd = strtotime($dateEnd);
				@endphp
				@if ($product->recruitment == 'tuyen-dung')
					<div class="carousel-item active"><div class="public item">Đang tuyển sinh</div></div>
					@elseif ($timeStart > time())
					<div class="carousel-item active"><div class="public item">Sắp bắt đầu</div></div>
					@elseif ( $timeStart <= time() && $timeEnd >= time() )
					<div class="carousel-item active"><div class="start item">Đang diễn ra</div></div>
					@else
					<div class="carousel-item active"><div class="complete item">Đã hoàn thành</div></div>
				@endif
			  </div>
			</div>
        </div>
    </div>

    <div class="content">
        <h3 class="boxP mgbottom10"><a href="{{ route('product', ['post_slug' => $product->slug ]) }}">{{ $product->title }}</a></h3>
        <!-- <div class="centertable boxP">
            <div class="des">
                <p>{{ $product->description }}</p>
            </div>
        </div> -->
    </div>
    <p class="timw"><i class="far fa-clock"></i><span>{{ ($product->recruitment == 'tuyen-dung') ? 'Dự kiến' : 'Bắt đầu' }}: {{ App\Ultility\Ultility::formatDateTime($product->started_time, $product->started_date) }}</span></p>
    <p class="calu"><i class="fas fa-calendar-alt"></i><span>Số buổi: </span> {{ $product->number_lesson }} buổi</p>
	<p class="calu"><i class="fas fa-users"></i><b>Học sinh: </b> {{ \App\Entity\ClassroomStudent::countStudent($product->classroom_id) }} học sinh</p>
    <p class="spanicon">
        <span class="star product-rating{{ $product->classroom_id }}">

        </span>
        <input type='hidden' value="{{ \App\Entity\AppraiseClass::countAppraise($product->classroom_id) }}" class="productAppraise{{ $product->classroom_id }}"/>
        <script>
            //Đồng bộ chiều cao các div
			$(function() {
				$('.boxP').matchHeight();
			});
            $(function() {
                var appraise = $(".productAppraise{{ $product->classroom_id }}").val();
                $(".product-rating{{ $product->classroom_id }}").starRating({
                    initialRating: appraise,
                    strokeColor: '#894A00',
                    starSize: 30,
                    readOnly: true
                });
            });
        </script>
        <span class="price fl-rt f18 text-b clred">{{ (!empty($product->price)) ? number_format($product->price, 0, ',', '.').' đ' : 'Miễn phí' }}</span>
    </p>
    @if (\App\Entity\ClassroomStudent::countStudent($product->classroom_id) >= $product->min_student)
        <a disabled="disabled" title="" class="disable clwhite bgxam pd-3 pd-010 ds-inline f18">Lớp đã đầy</a>
	@elseif ($timeEnd < time() )
		<a disabled="disabled" title="" class="disable clwhite bgxam pd-3 pd-010 ds-inline f18">Hết thời hạn đăng ký</a>
	@else
            @if (\Illuminate\Support\Facades\Auth::check())
                <a href="/tham-gia-lop-hoc/{{ $product->classroom_id }}" onclick="return acceptClassroom(this);"  title="" class="link">Ghi danh học ngay</a>
            @else
                <a href="/trang/dang-nhap" title="" class="link">Ghi danh học ngay</a>
            @endif
    @endif

</div>
