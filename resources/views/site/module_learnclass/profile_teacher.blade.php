<div class="teacherLeft text-lt pd10 bgwhite boxShadow">
	<div class="listView">
		<ul>
            <li>
            <a href="/thong-tin-ca-nhan" class="f14 clblack ds-block pd10"><i class="fa fa-users-cog"></i> Thông tin tài khoản</a>
            </li>
            <li>
                <a href="/trang/khoa-hoc-da-dang-ky" class="f14 clblack ds-block pd10"><i class="fa fa-laptop"></i> Khóa học đã đăng ký</a>
            </li>
            @if (\Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
            <li>
                <a href="/trang/trang-thong-tin-giao-vien" class="f14 clblack ds-block pd10"><i class="fas fa-chalkboard-teacher"></i> Khóa học của tôi</a>
            </li>
            @endif
                @if (\Illuminate\Support\Facades\Auth::user()->user_type != 'giao_vien')
                <li>
                    <a href="/trang/dang-ky-giao-vien-khi-da-co-tai-khoan" class="f14 clblack ds-block pd10"><i class="fa fa-user-tie"></i>  Trở thành giảng viên</a>
                </li>
                @else
                <li>
                    <a href="/thong-tin-ca-nhan" class="f14 clblack ds-block pd10"><i class="fa fa-user-clock"></i>  Tài khoản giảng viên</a>
                </li>
            @endif
            
            <li>
                <a href="/doi-mat-khau" class="f14 clblack ds-block pd10"><i class="fas fa-unlock-alt"></i>  Đổi mật khẩu</a>
            </li>
            <li>
                <a href="{{ route('logoutHome') }}" class="f14 clblack ds-block pd10"><i class="fa fa-sign-out-alt"></i>  Đăng xuất</a>
            </li>	
		</ul>
	</div>
    <div class="content  ">
        <div class="CropImg CropImgV">
            <div class="thumbs">
                <a href="" title="">
                    <img src="{{ isset($user->image) && !empty($user->image) ? asset($user->image) : asset('images/no_teacher.png')  }}" alt="{{ $user->name }}" class="">
                </a>
            </div>
        </div>
    </div>
	@if (!empty($teacher))
	<h2 class="f26 mgtop10">{{ $teacher->current_job }}</h2>
	<h4 class="f20 text-bnone">{{ $teacher->current_job }} - {{ $teacher->where_working }}</h4>
    <div class="spec pdtop20">
        <h3 class="f20 clback">Kĩ năng</h3>
        <p class="des f14">Nghề nghiệp: {{ $teacher->current_job }}</p>
        <p class="des f14">Hình thức giảng dạy: {{ $teacher->form_of_teaching }}</p>
    </div>
    <div class="spec pdtop20">
        <h3 class="f20 clback">Thông tin</h3>
        <p class="des f14">Lĩnh vực : {{ $teacher->teaching_field }}</p>
        <p class="des f14">Bằng cấp : {{ $teacher->academic_standard }}</p>
    </div>
	@endif
    <div class="contentdes mdmgleft0 mbmgbottom30 pdtop20">
        <a href="/thong-tin-ca-nhan" class="ds-block pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite  text-ct"><i class="fas fa-cogs mgright5"></i>Chỉnh sửa thông tin</a>
    </div>
</div>