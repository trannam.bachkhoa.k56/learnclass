<div class="row infocustomer">
    <div class="col-lg-12 bgwhite mg-15">
        <div class="row pd20">
            <div class="col-lg-2 col-md-4 col-sm-12 col-12 ">
                <div class="img">
                    <div class="CropImg">
                        <div class="thumbs">
                            <a href="" title="">
                                <img src="{{ isset($user->image) && !empty($user->image) ? asset($user->image) : asset('images/no_teacher.png')  }}" alt="{{ $user->name }}" class="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-8 col-sm-12 col-12 itembrRight">
                <div class="content text-lt mdtext-ct mdmgtop20">
                    <h2 class="text-ca f26">{{ $user->name }}</h2>
                    <div class="userPrice ds-block pd-10">
                        <span class="f16">Tài khoản : <span class="f24 clred text-b700 mgright10">{{ (!empty($user->coint)) ? number_format($user->coint, 0, ',', '.').' đ' : '0' }} </span></span>
                        <a href="/trang/lua-chon-phuong-thuc-thanh-toan" class="ds-inline f15 clwhite bgorang text-up text-b700 pd-4 pd-08 mgright5 mbds-block mbmg-10 mbpd-10 hvr-shutter-out-vertical">
                            Nạp tiền
                        </a>
                        <!-- <a href="" class="ds-inline f15 clwhite bgxanhdam text-up text-b700 pd-4 pd-08 mgright5 mbds-block mbmg-10 mbpd-10">
                            Rút tiền
                        </a> -->

                    </div>
                    <?php
                    $user = \Illuminate\Support\Facades\Auth::user();
                    $teacher = \App\Entity\Teacher::detailTeacher($user->id);
                    ?>
                    <p class="f16 mgbottom5">Hình thức giảng dạy : {{ isset($teacher->form_of_teaching) ?$teacher->form_of_teaching : 'Đang cập nhật'}}</p>
                    <p class="f16 mgbottom5">Giảng dạy : {{ isset($teacher->teaching_subject) ?$teacher->teaching_subject : 'Đang cập nhật'}}</p>
                </div>
            </div>

            <div class="col-lg-6 col-md-12 col-sm-12 col-12 itemListclass mdmgtop20">
                <h3 class="text-lt">GIỚI THIỆU</h3>
				<div class="content text-lt">
                   {!!  !empty($user->description) ? $user->description : 'Đang cập nhật' !!}
                </div>
            </div>

        </div>
    </div>
</div>