<!-- <div class="sidebarInfoUser bgwhite">
    <h2 class="f16 clwhite bgxanhdam text-ct w100 pd-10 pd-bottom15">DANH MỤC THÔNG TIN</h2>
    <div class="listView">
        <ul class="pd text-lt pd-15 pd-010">
            <li>
            <a href="/thong-tin-ca-nhan" class="f16  ds-block pd10 pd-015"><i class="fas fa-address-book mgright5"></i> Thông tin tài khoản</a>
            </li>
            <li>
                <a href="/trang/khoa-hoc-da-dang-ky" class="f16  ds-block pd10 pd-015"><i class="fas fa-address-book mgright5"></i>Khóa học đã đăng ký</a>
            </li>
            @if (\Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
            <li>
                <a href="/trang/trang-thong-tin-giao-vien" class="f16  ds-block pd10 pd-015"><i class="fas fa-address-book mgright5"></i> Khóa học của tôi</a>
            </li>
            @endif
                @if (\Illuminate\Support\Facades\Auth::user()->user_type != 'giao_vien')
                <li>
                    <a href="/trang/dang-ky-giao-vien-khi-da-co-tai-khoan" class="f16  ds-block pd10 pd-015"><i class="fas fa-address-book mgright5"></i>  Trở thành giảng viên</a>
                </li>
                @else
                <li>
                    <a href="/thong-tin-ca-nhan" class="f16  ds-block pd10 pd-015"><i class="fas fa-address-book mgright5"></i>Tài khoản giảng viên</a>
                </li>
            @endif
            
            <li>
                <a href="/doi-mat-khau" class="f16  ds-block pd10 pd-015"><i class="fas fa-address-book mgright5"></i>Đổi mật khẩu</a>
            </li>
            <li>
                <a href="{{ route('logoutHome') }}" class="f16  ds-block pd10 pd-015"><i class="fas fa-address-book mgright5"></i>  Đăng xuất</a>
            </li>   
        </ul>
    </div>
</div> -->
 <div class="teacherLeft text-lt pd10 pd-015 bgwhite boxShadow">
	<div class="listView">
		<ul>
            <li>
            <a href="/thong-tin-ca-nhan" class="f16  ds-block pd10 pd-015"><i class="fa fa-users-cog"></i> Thông tin tài khoản</a>
            </li>
            <li>
                <a href="/trang/khoa-hoc-da-dang-ky" class="f16  ds-block pd10 pd-015"><i class="fa fa-laptop"></i> Khóa học đã đăng ký</a>
            </li>
            @if (\Illuminate\Support\Facades\Auth::user()->user_type == 'giao_vien')
            <li>
                <a href="/trang/trang-thong-tin-giao-vien" class="f16  ds-block pd10 pd-015"><i class="fas fa-chalkboard-teacher"></i> Khóa học của tôi</a>
            </li>
            @endif
                @if (\Illuminate\Support\Facades\Auth::user()->user_type != 'giao_vien')
                <li>
                    <a href="/trang/dang-ky-giao-vien-khi-da-co-tai-khoan" class="f16  ds-block pd10 pd-015"><i class="fa fa-user-tie"></i>  Trở thành giảng viên</a>
                </li>
                @else
                <li>
                    <a href="/thong-tin-ca-nhan" class="f16  ds-block pd10 pd-015"><i class="fa fa-user-clock"></i>  Tài khoản giảng viên</a>
                </li>
            @endif
            
            <li>
                <a href="/doi-mat-khau" class="f16  ds-block pd10 pd-015"><i class="fas fa-unlock-alt"></i>  Đổi mật khẩu</a>
            </li>
            <li>
                <a href="{{ route('logoutHome') }}" class="f16  ds-block pd10 pd-015"><i class="fa fa-sign-out-alt"></i>  Đăng xuất</a>
            </li>	
		</ul>
	</div>
    <div class="content">
        <div class="CropImg CropImgV">
            <div class="thumbs">
                <a href="" title="">
                    <img src="{{ isset($user->image) && !empty($user->image) ? asset($user->image) : asset('images/no_teacher.png')  }}" alt="{{ $user->name }}" class="">
                </a>
            </div>
        </div>
    </div>
    <div class="spec pdtop20">
        <h3 class="f20 clback">Thông tin cơ bản</h3>
        <p class="des f14">Email: {{ $user->email }}</p>
        <p class="des f14">Số điện thoại: {{ $user->phone }}</p>
        <p class="des f14">Địa chỉ: {{ $user->address }}</p>
    </div>
    <div class="spec pdtop20">
        <h3 class="f20 clback">Thông tin mạng xã hội</h3>
        <p class="des f14">Youtube : {{ $user->youtube }}</p>
        <p class="des f14">Facebook : {{ $user->facebook }}</p>
        <p class="des f14">Mạng xã hội khác : {{ $user->other_social }}</p>
    </div>
    <div class="contentdes mdmgleft0 mbmgbottom30 pdtop20">
        <div class="icon text-lt pdbottom20">
            <ul>
                <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-012 ds-block br br-orang"><i class="fab fa-facebook-f"></i></a></li>
                <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-twitter"></i></a></li>
                <li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-linkedin-in"></i></a></li>
            </ul>
        </div>
    </div>
</div>