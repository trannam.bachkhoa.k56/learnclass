<div class="item">
    <div class="itemsingle pd10">
        <div class="img mgbottom20">
            <div class="CropImg">
                <div class="thumbs">
                    <a href="{{ route('detail_teacher', ['teacherId' => $teacher->teacher_id ]) }}" title="{{ $teacher->name }}">
                        <img src="{{ isset($teacher->image) && !empty($teacher->image) ? asset($teacher->image) : asset('images/no_teacher.png')  }}" alt="">
                    </a>
                </div>
            </div>
            <!-- <div class="icon">
                <ul>
                    <li class="ds-inline mgright5">
                        <a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-012 ds-block br br-orang"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="ds-inline mgright5">
                        <a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-twitter"></i></a></li>
                    <li class="ds-inline mgright5">
                        <a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
            </div> -->
			<h3 class="name"><a href="{{ route('detail_teacher', ['teacherId' => $teacher->teacher_id ]) }}" class="text-ca clgrey f20 clhr-orang">{{ $teacher->name }}</a></h3>
            <p class="des">{{ $teacher->current_job }}</p>
        </div>
        <div class="content cl-black f14 pd">
            <div class="date">
                <p>Lĩnh vực : {{ $teacher->teaching_field }}</p>
                <div class="text-lt mgbottom10">
                    <span class="star teacher-old-rating{{ $teacher->teacher_id }}"></span>
                    ({{ \App\Entity\StudentAppraiseTeacher::totalAppraise($teacher->teacher_id) }} đánh giá)
                    <input type='hidden' value="{{ \App\Entity\StudentAppraiseTeacher::countAppraise($teacher->teacher_id) }}" class="teacherAppraise{{ $teacher->teacher_id }}"/>
                    <script>
                        //Đồng bộ chiều cao các div
                        $(function() {
                            $('.boxP').matchHeight();
                        });
                        $(function() {
                            var appraise = $(".teacherAppraise{{  $teacher->teacher_id }}").val();
                            $(".teacher-old-rating{{ $teacher->teacher_id }}").starRating({
                                initialRating: appraise,
                                strokeColor: '#894A00',
                                starSize: 30,
                                readOnly: true
                            });
                        });
                    </script>
                </div>
            </div>
            <a href="{{ route('detail_teacher', ['teacherId' => $teacher->teacher_id ]) }}" class="text-up clwhite clhr-orang bgorang pd-10 pd-015 br-rs10 ds-inline bghr-white f16">Xem chi tiết</a>
        </div>
    </div>
</div>