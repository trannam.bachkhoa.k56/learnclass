@extends('site.layout.site')

@section('title','Đăng ký lớp học thành công' )
@section('meta_description',  '')
@section('keywords', '')

@section('content')
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20">Tin tức hoạt động</h1>
                        <ul>
                            <li><a href="./">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="#">Tin tức hoạt động</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="categoryNew2 pd-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-12 col-12 bgxam pdtop10 pdbottom10">
                    <div class="sidebarnew">
                        <h2 class="f20 mgtop10 text-bnone pdtop10 pd-010 clorang mgborom20">Thông báo
                        </h2>
                        <ul class="list">
                            @if(\App\Entity\Notification::showNotifyClass(0 || 1 || 2 ||3,'giao_vien'))
                                <li class="ds-block">
                                    <a href="" title="" onclick="return seenNotification(this)" class="f16 ds-block pd-10 pd-010">Thông báo phòng học <span class="clred bgwhite f16 pd-06 ds-inline text-rt ">{{ \App\Entity\Notification::countNotifyDetail(0) }}</span> </a>
                                </li>
                            @elseif(\App\Entity\Notification::showNotifyClass(0 || 1 || 2 ||3,''))
                                <li class="ds-block">
                                    <a href="" title="" onclick="return seenNotification(this)" class="f16 ds-block pd-10 pd-010">Thông báo khóa học <span class="clred bgwhite f16 pd-06 ds-inline text-rt ">{{ \App\Entity\Notification::countNotifyDetail(1) }}</span></a>
                                </li>
                            @endif
                            <li class="ds-block">
                                <a href="" title="" onclick="return seenNotification(this)" class="f16 ds-block pd-10 pd-010">Thông báo nạp tiền <span class="clred bgwhite f16 pd-06 ds-inline text-rt ">{{ \App\Entity\Notification::countNotifyDetail(2) }}</span></a>
                            </li>
                            <li class="ds-block">
                                <a href="" title="" onclick="return seenNotification(this)" class="f16 ds-block pd-10 pd-010">Thông báo hệ thống <span class="clred bgwhite f16 pd-06 ds-inline text-rt ">{{ \App\Entity\Notification::countNotifyDetail(3) }}</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <script>
                    $(document).ready(function () {
                        var element = $('.sidebarnew');
                        var originalY = element.offset().top;

                        // Space between element and top of screen (when scrolling)
                        var topMargin = 40;

                        // Should probably be set in CSS; but here just for emphasis
                        element.css('position', 'relative');

                        $(window).on('scroll', function(event) {
                            var scrollTop = $(window).scrollTop();
                            var topContent = $('.contentRightNew2').height();
                            if (scrollTop < (topContent - 0)) {
                                element.stop(false, false).animate({
                                    top:
                                        scrollTop < originalY ? 0 : scrollTop - originalY + topMargin
                                }, 300);
                            }
                        });
                    });
                </script>
                <div class="col-lg-10 col-md-10 col-sm-12 col-12">
                    <div class="contentRightNew2">
                        <h3 class="text-bnone f20 mg-20">Thông báo @if(isset($classroom->name)) khóa học {{ $classroom->name }} @else phê duyệt @endif</h3>
                        <div class="content">
                            <p>@if(isset($classroom->detail)) {{ $classroom->detail }} @else {{ $teacher->detail }} @endif</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection