@extends('site.layout.site')

@section('title','Gửi đơn hàng thành công')
@section('meta_description','')
@section('keywords','')

@section('content')
    <style>
        section.orderPay
        {
        }
        section.orderPay h1
        {
            font-size: 24px;
            padding: 25px 0px;
            display: block;
        }
        section.orderPay  .pay h3.titleV
        {
            text-align: center;
        }
        section.orderPay  .pay h3.titleV i
        {
            padding-right: 5px;
        }
        section.orderPay .pay button
        {
            background: none;
            box-shadow: none;
            border: 1px solid #ccc;
            font-size: 16px;
            border-radius: 6px;
            padding: 7px 24px;
        }
        section.orderPay a
        {
            color: #000;
            text-decoration: none;
        }
        section.orderPay form
        {
            width: 100%;
        }
        section.orderPay form .nextpr
        {
            border: 1px dotted #ccc;
            padding: 2px 5px;
            font-size: 14px;
            display: inline-block;
        }
        .table-striped tbody tr:nth-of-type(odd)
        {
            background: none;
        }
        section.orderPay form .total
        {
            color: red;
        }
        section.orderPay form .content h3 a
        {

            font-size: 20px;
        }
        section.orderPay form .price
        {
            font-size: 14px;
        }
        section.orderPay form .price del
        {
            padding-right: 15px;
        }
    </style>
    <section class="orderPay">
        <div class="container bgpay">
            <div class="row">
                <div class="col-12">
                    <h1>Giỏ hàng</h1>
                </div>
                <form action="{{ route('send') }}" class="formCheckOut validate" method="post">
                    {{ csrf_field() }}
                    <div class="col-12 order">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">ảnh</th>
                                <th scope="col">Sản phẩm</th>
                                <th scope="col">Số lượng</th>
                                <th scope="col">Tổng số tiền</th>
                            </tr>
                            </thead>
                            <?php $sumPrice = 0;?>
                            <tbody>
                            @if (!empty($orderItems))
                                @foreach($orderItems as $id => $orderItem)
                                    <tr>
                                        <td >
                                            <a href="{{ route('post', ['cate_slug' => 'san-pham', 'post_slug' => $orderItem->slug]) }}">
                                                <img src="{{ !empty($orderItem->image) ?  asset($orderItem->image) : asset('/site/img/no-image.png') }}" alt="{{ $orderItem->title }}" width="50"> </a>
                                        </td>
                                        <td>
                                            <div class="content">
                                                <h3><a href="{{ route('post', ['cate_slug' => 'san-pham', 'post_slug' => $orderItem->slug]) }}">{{ $orderItem->title }}-{{$orderItem->color}}-{{ $orderItem->size }}</a></h3>
                                                <p>Thông số: {{ $orderItem->properties }}</p>
                                                <p class="price">
                                                    @if (!empty($orderItem->discount))
                                                        <span class="discont">Giá : <del>{{ number_format($orderItem->price , 0) }} VND</del>{{ number_format($orderItem->discount , 0) }} VND</span>
                                                    @else
                                                        <span class="discont">Giá : {{ number_format($orderItem->price , 0) }} VND</span>
                                                    @endif
                                                </p>
                                            </div>
                                        </td>
                                        <td>{{ $orderItem->quantity }}</td>
                                        <td>
                                        <span class="total totalPrice"><?php $sumPrice += !empty($orderItem->discount) ? ($orderItem->discount*$orderItem->quantity) : ($orderItem->price*$orderItem->quantity) ?>
                                            {{ !empty($orderItem->discount) ? number_format(($orderItem->discount*$orderItem->quantity) , 0) : number_format(($orderItem->price*$orderItem->quantity) , 0, ',', '.') }}</span>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            <tr>
                                <td colspan="3" rowspan="" headers="">
                                    <a href="" title="/" class="nextpr"><i class="fa fa-long-arrow-left"
                                                                           aria-hidden="true"></i>Tiếp tục mua hàng</a>
                                </td>
                                <td colspan="2" rowspan="" headers="">
                                    <a href="" title="" class="total">Thành tiền : <span class="sumPrice">{{ number_format($sumPrice , 0) }}</span> VND </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="pay">
                        <h3 class="titleV bgorange">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>Thông tin nhận hàng
                        </h3>
                        <div class="col-12 col-md-12">
                            <p>{{ $customer['ship_name'] }}</p>
                            <p>Điện thoại: {{ $customer['ship_phone'] }}</p>
                            <p>Email: {{ $customer['ship_email'] }}</p>
                            <p>Địa chỉ: {{ $customer['ship_address'] }}</p>
                        </div>
                        <div class="col-12 col-md-12">
                            <p class="titlePayment clearfix">Cảm ơn bạn đã mua hàng của chúng tôi, Mã đơn hàng của bạn là #{{ $orderId }}</p>
                            <p class="">chúng tôi sẽ xác nhận và gửi đơn hàng trong thời gian ngắn nhất.</p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection
