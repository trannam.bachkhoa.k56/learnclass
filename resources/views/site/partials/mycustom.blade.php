<section class="customer">
    <div class="container">
        <div class="row">
            <div class="col-12 titleIndex">
                <h3>khách hàng nói  về chúng tôi</h3>
                <p>Khách hàng tin tưởng vào phòng học trực tuyến</p>
                <div class="borderTitle">
                    <div class="left"></div>
                    <div class="center"><i class="fas fa-graduation-cap"></i></div>
                    <div class="right"></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-11 col-md-12 col-sm-12">
                <div class="owl-carousel">

                    @foreach(\App\Entity\SubPost::showSubPost('khach-hang',15) as  $id=> $custom)
                        <div class="item">
                            <div class="itemcustomer">
								<div class="quote"><i class="fa fa-quote-left"></i></div>
                                <div class="img">
                                    <div class="CropImg">
                                        <div class="thumbs">
                                            <a href="#" title="{{ isset($custom['ten-khach-hang']) ?$custom['ten-khach-hang'] : '' }}">
                                                <img src="{{ asset($custom['image']) ?$custom['image'] : '' }}" alt="{{ isset($custom['ten-khach-hang']) ?$custom['ten-khach-hang'] : '' }}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="content">
                                    <h3><a href="#" class="titletea" title="{{ isset($custom['ten-khach-hang']) ?$custom['ten-khach-hang'] : '' }}">{{ isset($custom['ten-khach-hang'])
                                    ?$custom['ten-khach-hang'] : '' }}</a></h3>
                                    <p class="des boxH">{{ isset($custom['mo-ta']) ?$custom['mo-ta'] : '' }}</p>
                                    <p class="position"><i class="fas fa-minus"></i> <span>{{ isset($custom['ten-phu']) ?$custom['ten-phu'] : '' }}</span></p>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
        <script>
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:20,
                responsiveClass:true,
                autoplay:true,
                autoplayTimeout:7000,
                nav:true,
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:1,
                        nav:true
                    },
                    1000:{
                        items:2,
                        nav:true,
                        loop:true
                    }
                }
				
            });
			$('.boxH').matchHeight();
            // $('.owl-prev').html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');
            // $('.owl-next').html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
        </script>
    </div>
</section>