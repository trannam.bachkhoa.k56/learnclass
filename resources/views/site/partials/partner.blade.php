<section class="partner">
    <div class="container">
        <div class="row">
            <div class="col-12 titleIndex">
                <h3>Đối tác của chúng tôi</h3>
                <p>Hệ thống đối tác toàn quốc</p>
                <div class="borderTitle">
                    <div class="left"></div>
                    <div class="center"><i class="fas fa-graduation-cap"></i></div>
                    <div class="right"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="owl-carousel">
                @foreach(\App\Entity\SubPost::showSubPost('doi-tac',15) as  $id => $partner)
                    <div class="item">
                        <div class="itempartner">
                            <div class="img">
                                <a href="#" title="{{ isset($partner['	tieu-de']) ? $partner['	tieu-de'] : '' }}">
                                    <img src="{{ asset($partner['image']) ?$partner['image']  :'' }}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <script>
			
			
						
                $('.owl-carousel').owlCarousel({
                    loop:true,
                    margin:10,
                    center: true,
                    responsiveClass:true,
                    autoplay:true,
                    autoplayTimeout:7000,
                    nav:true,
                    responsive:{
                        0:{
                            items:1,
                            nav:true
                        },
                        600:{
                            items:3,
                            nav:true
                        },
                        1000:{
                            items:4,
                            nav:true,
                            loop:true
                        },
						1500:{
                            items:5,
                            nav:true,
                            loop:true
                        }
                    }
                });
                // $('.owl-prev').html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');
                // $('.owl-next').html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
            </script>
        </div>
    </div>
</section>