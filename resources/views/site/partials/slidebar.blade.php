<div class="sideRight">
    <!-- <div class="itemRight pd10 boxShadow bgwhite mgbottom30">
        <h3 class="f20 clblack br-bottom brcl-xam text-bnone pd-10 text-ca">Danh mục</h3>
        <ul class="pd-10 text-lt">
            @foreach (\App\Entity\MenuElement::showMenuPageArray('danh-muc-tin-tuc') as $menu)
                <li><a href="{{ $menu['url'] }}" class="f16 ds-block clgrey clhr-orang pd-05 pd-5"><i class="fas fa-angle-double-right f12 pdright5"></i>{{ $menu['title_show'] }}</a></li>
            @endforeach
        </ul>

    </div> -->
    <div class="itemRight pd10 boxShadow bgwhite mgbottom30">
        <h3 class="f20 clblack br-bottom brcl-xam text-bnone pd-10 text-ca">Fanpage</h3>
        <div class="pd-10 w100">
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0&appId=1875296572729968&autoLogAppEvents=1';
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-page" data-href="https://www.facebook.com/goldkidsphonghoctructuyen" data-tabs="timeline" data-height="450" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/goldkidsphonghoctructuyen" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/goldkidsphonghoctructuyen/">Phòng học trực tuyến</a></blockquote></div>
        </div>

    </div>
    <div class="itemRight pd10 boxShadow bgwhite mgbottom30">
        <h3 class="f20 clblack br-bottom brcl-xam text-bnone pd-10 text-ca">Bài viết mới nhất</h3>
        @foreach (\App\Entity\Post::newPost('tin-tuc-hoat-dong') as $post)
            <div class="itemnewRight cldenhat br-bottom brcl-xam text-lt">
                <a href="{{ route('post', ['cate_slug' => 'tin-tuc-hoat-dong', 'post_slug' => $post->slug]) }}" class="f14 clblack ds-block pd-10 text-b clhr-orang">{{ $post->title }}</a>
                <div class="img ds-inline">
                    <img src="{{ asset($post->image) }}" alt="" class=" w40 ds-in fl-lt mbw50 mgright10">
                    <p class="ds-in pd-010 text-lt">{{ \App\Ultility\Ultility::textLimit($post->description, 20) }}</p>
                </div>


                <a href="{{ route('post', ['cate_slug' => 'tin-tuc-hoat-dong', 'post_slug' => $post->slug]) }}" class="cldenhat clhr-orang ds-block pd-10">Đọc thêm ...</a>
            </div>
        @endforeach
    </div>
</div>