
<section class="slider hiddenTAB">
    <div class="clearfix">
        <div id="sliderHome" class="carousel slide" data-ride="carousel">
			<div class="formRegister">
				<div class="arrow icondown text-ct">
					<i class="fa fa-angle-double-down"></i>
				</div>
				<div class="main">
					<h2 class="text-lt">Đăng ký học ngay</h2>
					<div class="des text-lt">
						Quý phụ huynh, học sinh và giáo viên vui lòng đăng ký tại đây. Tư vấn viên của Phòng học sẽ gọi điện kiểm tra kiến thức cũng như xắp xếp lớp học phù hợp cho quý vị.<br><br> 
						Chú ý các trường dữ liệu có dấu (*) là bắt buộc.
					</div>
					<form action="{{ route('sub_contact') }}" method="post" accept-charset="utf-8" class="mgtop20 mdmgtop20">
						{!! csrf_field() !!}
						<div class="row">
							<div class="col-md-4 col-sm-6">
								<select name="address">
									<option value="0">Chọn môn học (*)</option>
									@foreach(\App\Entity\FilterGroup::showFilterGroup() as $id => $filterGroup)
										@if ($filterGroup->group_name == 'Các Môn Học')
											@foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
												<option value="{!! $filter->name_filter !!}">{!! $filter->name_filter !!}</option>
											@endforeach
										@endif
									@endforeach
								</select>
							</div>
							<div class="col-md-4 col-sm-6">
								<select name="message">
									<option value="0">Chọn lớp học (*)</option>
									@foreach(\App\Entity\FilterGroup::showFilterGroup() as $id => $filterGroup)
										<option  disabled> {{ $filterGroup->group_name }}</option>
										@if ($filterGroup->group_name != 'Các Môn Học')
											@foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
												<option value="{!! $filter->name_filter !!}">{!! $filter->name_filter !!}</option>
											@endforeach
										@endif
									@endforeach
								</select>
							</div>
							<div class="col-md-4 col-sm-6">
								<input type="text" placeholder="Họ tên đầy đủ (*)" name="name" required/>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-6">
								<input type="number" placeholder="Số điện thoại liên lạc (*)" name="phone" required />
							</div>
							<div class="col-md-4 col-sm-6">
								<input type="email" class="form-control" name="email" placeholder="Email" required/>
							</div>
							<div class="col-md-4 col-sm-6 text-lt">
								<input class="btnOrange" type="submit" value="Đăng ký học" required/>
							</div>
						</div>
					</form>
				</div>
			</div>
            <div class="carousel-inner">
                @foreach(\App\Entity\SubPost::showSubPost('slider',3) as  $id=> $img)
                    <div class="carousel-item <?php if($id == 0) { echo 'active';}?>">
                        <img src="{{ isset($img['image']) ? asset($img['image']) : '' }}" alt="{{ isset($img['tieu-de']) ?$img['tieu-de'] :'' }}">
                        <div class="mask" data-animation="animated fadeIn"></div>
						 <!-- <div class="carousel-caption d-none d-md-block">
                            <div class="content">
                                <h3 data-animation="animated bounceInLeft">{{ isset($img['tieu-de']) ?$img['tieu-de'] :'' }} </h3>
                                <div class="des" data-animation="animated bounceInRight">{!! isset($img['mo-ta']) ?$img['mo-ta'] :'' !!}</div>
                               <a class="btnExtra" data-animation="animated zoomInUp" href="{{ isset($img['link']) ?$img['link'] :'' }}" title="{{ isset($img['tieu-de']) ?$img['tieu-de'] :'' }}" class="bghr-grey text-de">Đăng kí ngay<i class="fas fa-angle-double-right bounceRight"></i></a>
                            </div>
                        </div> -->
                    </div>
                @endforeach

            </div>
            <a class="carousel-control-prev control" href="#sliderHome" role="button" data-slide="prev">
                <i class="fa fa-angle-left"></i>
            </a>
            <a class="carousel-control-next control" href="#sliderHome" role="button" data-slide="next">
                <i class="fa fa-angle-right"></i>
            </a>
        </div>
		<script>
			var height = $(window).height();
			var height1 = $('header').height();
			var height2 = height - height1;
			$('#sliderHome .carousel-item').attr('style', 'height:' + height2 + 'px');
		</script>
    </div>
</section>
<script>
	/* Demo Scripts for Bootstrap Carousel and Animate.css article
* on SitePoint by Maria Antonietta Perna
*/
(function($) {
  //Function to animate slider captions
  function doAnimations(elems) {
    //Cache the animationend event in a variable
    var animEndEv = "webkitAnimationEnd animationend";

    elems.each(function() {
      var $this = $(this),
        $animationType = $this.data("animation");
      $this.addClass($animationType).one(animEndEv, function() {
        $this.removeClass($animationType);
      });
    });
  }

  //Variables on page load
  var $myCarousel = $("#sliderHome"),
    $firstAnimatingElems = $myCarousel
      .find(".carousel-item:first")
      .find("[data-animation ^= 'animated']");

  //Initialize carousel
  $myCarousel.carousel();

  //Animate captions in first slide on page load
  doAnimations($firstAnimatingElems);

  //Other slides to be animated on carousel slide event
  $myCarousel.on("slide.bs.carousel", function(e) {
    var $animatingElems = $(e.relatedTarget).find(
      "[data-animation ^= 'animated']"
    );
    doAnimations($animatingElems);
  });
})(jQuery);
</script>
<style>
    .carousel-control-prev-icon,.carousel-control-next-icon
    {
        position: absolute;
        top: -35%;
    }
</style>