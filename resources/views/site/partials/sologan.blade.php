<section class="sologan">
    <div class="row">
        @foreach(\App\Entity\SubPost::showSubPost('sologan',3) as  $id=> $sologan)
            <div class="col-lg-4 col-md-4 som-sm-12 col-12 noPadding itemsologan  <?php if($id == 1){ echo 'activesl';} ?>">
                <div class="itemsl <?php if($id == 1){ echo 'active';} ?>">
                    <h3 class="f18"><i class="{{ isset($sologan['icon-slogan']) ? $sologan['icon-slogan'] : '' }}"></i>{{ isset($sologan['tieu-de']) ? $sologan['tieu-de'] : '' }}</h3>
                    <p class="des">{{ isset($sologan['mo-ta']) ? $sologan['mo-ta'] : '' }}</p>
                    <!--<ul>
                        <li><a href="" title=""><i class="far fa-check-square"></i> 1.{{ isset($sologan['thong-tin-1']) ? $sologan['thong-tin-1'] : '' }} </a></li>
                        <li><a href="" title=""><i class="far fa-check-square"></i> 2.{{ isset($sologan['thong-tin-2']) ? $sologan['thong-tin-2'] : '' }} </a></li>
                        <li><a href="" title=""><i class="far fa-check-square"></i> 3.{{ isset($sologan['thong-tin-3']) ? $sologan['thong-tin-3'] : '' }} </a></li>
                        <li><a href="" title=""><i class="far fa-check-square"></i> 4.{{ isset($sologan['thong-tin-4']) ? $sologan['thong-tin-4'] : '' }} </a></li>
                    </ul>-->
                </div>
            </div>
        @endforeach
    </div>
</section>