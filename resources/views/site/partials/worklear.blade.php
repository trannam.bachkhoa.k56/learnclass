<section class="workLear pdbottom30">
    <div class="container">
        <div class="row">
            <div class="col-12 titleIndex">
                <h3>Hoạt động dạy học</h3>
                <p>Hoạt động dạy học trực tuyến</p>
                <div class="borderTitle">
                    <div class="left"></div>
                    <div class="center"><i class="fas fa-graduation-cap"></i></div>
                    <div class="right"></div>
                </div>
            </div>
        </div>
        <div class="row">

            @foreach(\App\Entity\Post::categoryShow('tin-tuc-hoat-dong',3) as $worklear)
                <div class="col-lg-4 col-md-6 col-sm-12 col-12 work">
                    <div class="itemwork">
                        <div class="img">
                            <div class="CropImg">
                                <div class="thumbs">
                                    <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $worklear->slug]) }}" title=" {{ isset($worklear->title) ?$worklear->title : '' }}">
                                        <img src="{{ !empty($worklear->image) ?  asset($worklear->image) : '' }}" alt="{{ isset($worklear->title) ?$worklear->title : '' }}">
                                    </a>
                                </div>
                            </div>
                            <div class="content">
                                <h3><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $worklear->slug]) }}" class="title" title="{{ isset($worklear->title) ?$worklear->title : '' }}">
                                    {{ isset($worklear->title) ?$worklear->title : '' }}
                                </a></h3>
                                <p class="datetime"><b>Ngày đăng:</b> {{ isset($worklear['thoi-gian'])  ?$worklear['thoi-gian'] : '' }} |<b>Danh mục:</b>
                                    {{ isset($worklear['danh-muc'])  ?$worklear['danh-muc'] : '' }}</p>
                                <p>{!! isset($worklear['description'])  ? \App\Ultility\Ultility::textLimit($worklear->description, 40) : '' !!}... <a class="readMore" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $worklear->slug]) }}">Đọc thêm<i class="fas fa-angle-double-right bounceRight"></i></a></p>

                            </div>
                        </div>

                    </div>
                </div>
            @endforeach



        </div>
        <div class="row reamore">
            <div class="col-12"><a href="{{ route('site_category_post', ['cate_slug' => 'tin-tuc-hoat-dong']) }}" class="bghr-grey clhr-white">Xem thêm... </a></div>

        </div>
    </div>
</section>