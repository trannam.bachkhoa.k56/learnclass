<div class="modal fade" id="payCourse" tabindex="-1" role="dialog" aria-labelledby="takePartInClassroom" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="takePartInClassroom">Lựa chọn phương thức thanh toán</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="item text-ct bgwhite">
                                <img  class=" pd-10 pdtop15" src="{{ asset('khoahoc/images/1.png') }}" height="">
                                <img  class=" pd-10 pdtop15" src="{{ asset('khoahoc/images/2.png') }}" height="">
                                <p class="f14 pdbottom10 title">Giao thẻ học tận nơi và thu tiền</p>
                                <form method="get" action="/trang/thanh-toan-truc-tiep">
                                    <input type="hidden" value="{{ $product->price }}" class="pricePayment" name="payment" />
                                    <input type="hidden" value="{{ $product->title }}" class="messagePayment" name="message" />
                                    <button type="submit" class="f14 text-up pd-4 pd-015 mgbottom20 button">thanh toán</button>
                                </form>
                            </div>
                        </div>
                        <div class=" col-lg-6 col-md-6">
                            <div class="item text-ct bgwhite">
                                <img  class=" pd-10 pdtop15" src="{{ asset('khoahoc/images/3.png') }}" height="">
                                <p class="f14 pdbottom10 title">Chuyển khoản ngân hàng</p>
                                <form method="get" action="/trang/thanh-toan-qua-chuyen-khoan">
                                    <input type="hidden" value="{{ $product->price }}" class="pricePayment" name="payment" />
                                    <input type="hidden" value="{{ $product->title }}" class="messagePayment" name="message" />
                                    <button type="submit" class="f14 text-up pd-4 pd-015 mgbottom20 button">thanh toán</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>