<div class="modal fade" id="takePartInClassroom" tabindex="-1" role="dialog" aria-labelledby="takePartInClassroom" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="takePartInClassroom">Xác nhận tham gia khóa học</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post">
                {!! csrf_field() !!}
                <div class="modal-body">
                    <p>Nhấn "Tiếp tục" để xác nhận mua khóa học</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary"><a href="" id="acceptCLassroomId" >Tiếp tục</a></button>
                </div>
            </form>
        </div>
    </div>
</div>