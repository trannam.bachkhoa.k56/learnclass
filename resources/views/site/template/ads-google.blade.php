@extends('site.layout.site')
@section('title', $category->title)
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', '')

@section('content')
    <script type="text/javascript" src="/ads/home/js/jquery.min.js"></script>
    <script src="/ads/home/js/bootstrap.min.js"></script>
    <script src="/ads/js/jquery.cookie.js"></script>
    <script src="/ads/home/js/owl.carousel.min.js"></script>
    <script src="/ads/home/js/jquery.mmenu.all.min.js"></script>
    <script src="/ads/home/js/main.js"></script>
    <script src='/ads/home/js/wow.js' type='text/javascript'></script>
    <link rel="stylesheet" href="/ads/home/css/bootstrap.min.css">
    <link href="/ads/home/css/owl.carousel.css" rel="stylesheet">
    <link rel="stylesheet" href="/ads/css/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="/ads/fonts/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/ads/home/css/jquery.mmenu.all.css">
    <link href="/ads/home/css/styles.css?v=1" rel="stylesheet">
    <link href="/ads/home/css/course.css" rel="stylesheet">
    <link href="/ads/home/css/responsive.css" rel="stylesheet">
    <link href="/ads/home/css/animate.css" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="/ads/home/css/jquery.fancybox.min.css" />
    <script src="/ads/home/js/jquery.fancybox.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i&amp;subset=vietnamese" rel="stylesheet">

    <div class="bg_mmenu">
        <div class="page">
            <main id="main">
                <section class="classroom mb45">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                                <div class="classroom__text">
                                    <div class="classroom__text-title">
                                        <h2>Phòng học trực tuyến</h2>
                                        <h4>Giải pháp cho Trung tâm đào tạo, Giáo viên, Trainer</h4>
                                    </div>
                                    <div class="classroom__text-desc hidden-sm hidden-xs">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img src="statics/home/images/icon-class-1.png" class="media-object" >
                                            </div>
                                            <div class="media-body">
                                                <p>Bạn muốn có 1 phòng học trực tuyến để<br>thay thế cho lớp học offline?</p>
                                            </div>
                                        </div>
                                        <div class="media">
                                            <div class="pull-left">
                                                <img src="statics/home/images/icon-class-2.png" class="media-object" >
                                            </div>
                                            <div class="media-body">
                                                <p>Bạn muốn có 1 hệ thống e-learning<br>chuyên nghiệp?</p>
                                            </div>
                                        </div>
                                        <div class="media">
                                            <div class="pull-left">
                                                <img src="statics/home/images/icon-class-3.png" class="media-object" >
                                            </div>
                                            <div class="media-body">
                                                <p>Dạy và Học bất kỳ lúc nào, bất kỳ đâu, chỉ với PC /<br>Laptop / Mobile kết nối Internet?</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="classroom__text-reg">
                                        <a href="register">DÙNG THỬ MIỄN PHÍ 14 NGÀY</a>
                                    </div>
                                    <div class="classroom___image-hidden">
                                        <div class="classroom___image text-center">
                                            <img src="statics/home/images/screen768_03.png">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 hidden-md">
                                <div class="classroom__image">
                                    <img src="statics/home/images/img-section-1.png" class="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="curved-bg"></div>
                </section>
                <section class="benefit mb45">
                    <div class="container">
                        <div class="title">
                            <h3>Lợi ích khi sử dụng EDUBIT</h3>
                        </div>
                        <div class="benefit__content">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" >
                                    <div class="benefit__content-item wow fadeInUp animated animated" data-wow-offset="30" data-wow-duration="1s" data-wow-delay="0.1s">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img src="statics/home/images/icon-benefit-1.png" class="media-object" >
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Dậy & Học mọi lúc - mọi nơi</h4>
                                                <p>Chỉ cần Laptop hoặc Mobile kết nối Internet</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="benefit__content-item wow fadeInUp animated animated" data-wow-offset="30" data-wow-duration="1.25s" data-wow-delay="0.2s">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img src="statics/home/images/icon-benefit-2.png" class="media-object" >
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Tích hợp thanh toán online</h4>
                                                <p>Visa / Master, Internet Banking,...</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="benefit__content-item wow fadeInUp animated animated" data-wow-offset="30" data-wow-duration="1.5s" data-wow-delay="0.4s">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img src="statics/home/images/icon-benefit-1.png" class="media-object" >
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Khởi tạo miễn phí & nhanh chóng</h4>
                                                <p>Tạo phòng học Miễn Phí trong 30 giây chỉ với 3 bước đơn giản</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="benefit__content-item wow fadeInUp animated animated" data-wow-offset="30" data-wow-duration="1.75s" data-wow-delay="0.6s">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img src="statics/home/images/icon-benefit-4.png" class="media-object" >
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Tiết kiệm tới 70% chi phí</h4>
                                                <p>Không mất chi phí thuê phòng, không mất thời gian đi lại</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="benefit__content-item wow fadeInUp animated animated" data-wow-offset="30" data-wow-duration="2s" data-wow-delay="0.8s">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img src="statics/home/images/icon-benefit-5.png" class="media-object" >
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Tự động thông báo lịch học</h4>
                                                <p>Tự động gửi thông báo nhắc lịch cho Học viên: Email, SMS, Push.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="benefit__content-item wow fadeInUp animated animated" data-wow-offset="30" data-wow-duration="2.25s" data-wow-delay="1s">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img src="statics/home/images/icon-benefit-6.png" class="media-object" >
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Tương tác trực tiếp vơi giáo viên</h4>
                                                <p>Học qua video có sẵn + phòng học tương tác trực tiếp với Giảng viên</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="benefit__content-item wow fadeInUp animated animated" data-wow-offset="30" data-wow-duration="2.5s" data-wow-delay="1.2s">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img src="statics/home/images/icon-benefit-7.png" class="media-object" >
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Băng thông rộng</h4>
                                                <p>Đường truyền Băng thông rộng trong nước</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="benefit__content-item wow fadeInUp animated animated" data-wow-offset="30" data-wow-duration="2.75s" data-wow-delay="1.4s">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img src="statics/home/images/icon-benefit-8.png" class="media-object" >
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Giải pháp icloud</h4>
                                                <p>Liên tục được nâng cấp chức năng mới</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="benefit__content-item wow fadeInUp animated animated" data-wow-offset="30" data-wow-duration="3s" data-wow-delay="1.6s">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img src="statics/home/images/icon-benefit-9.png" class="media-object" >
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Chuyển đổi tên miền</h4>
                                                <p>Hỗ trợ gắn tên miền riêng biệt, chạy độc độc như 1 website khác</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="function mb45">
                    <div class="container">
                        <div class="title">
                            <h3>Các chức năng chính của<br>hệ thống Edubit</h3>
                        </div>
                        <div class="function__content">
                            <div class="row">
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInLeft animated" data-wow-offset="30" data-wow-duration="1.5s" data-wow-delay="0.15s">
                                    <div class="function__item">
                                        <h3>Học trực tuyến, tương tác trực tiếp</h3>
                                        <ul>
                                            <li>Trao đổi tương tác Live, Quiz Online</li>
                                            <li>Điều khiển Mic, Webcam</li>
                                            <li>Chia sẻ màn hình, trình chiếu slide, video</li>
                                            <li>Chat 1-1 hoặc 1 - nhiều, Giơ tay phát biểu</li>
                                            <li>Bảng viết online: Vẽ, Viết, Lưu lại</li>
                                            <!-- <li>Livestream trên Facebook và Youtube</li> -->
                                            <li>Ghi hình buổi học để xem lại</li>
                                            <li>Hỗ trợ: Chrome, Coccoc, IOS, Android</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInRight animated" data-wow-offset="30" data-wow-duration="1.5s" data-wow-delay="0.15s">
                                    <div class="function__item">
                                        <h3>Quản trị dễ dàng</h3>
                                        <ul>
                                            <li>Upload video bảng giảng ghi hình sẵn</li>
                                            <li>Quản lý tài liệu, phần mềm, link</li>
                                            <li>Tạo mã khuyến mại (coupon)</li>
                                            <li>Cài đặt tên miền riêng</li>
                                            <li>API tích hợp với website bên ngoài</li>
                                            <li>Quản lý đơn hàng, học viên, thu nhập</li>
                                            <!-- <li>Thông báo EMAIL, SMS, Web Push</li> -->
                                            <li>Link chia sẻ đăng ký học, mời vào học</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="gallery">
                    <div class="container">
                        <div class="title">
                            <h3>Một số hình ảnh hệ thống Edubit</h3>
                        </div>
                        <div class="gallery__content">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="gallery__item">
                                        <a data-fancybox="gallery" href="statics/home/images/scr5.jpg"><img src="statics/home/images/scr5.jpg"></a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="gallery__item">
                                        <a data-fancybox="gallery" href="statics/home/images/scr4.jpg"><img src="statics/home/images/scr4.jpg"></a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="gallery__item">
                                        <a data-fancybox="gallery" href="statics/home/images/scr2.jpg"><img src="statics/home/images/scr2.jpg"></a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="gallery__item">
                                        <a data-fancybox="gallery" href="statics/home/images/scr3.jpg"><img src="statics/home/images/scr3.jpg"></a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="gallery__item">
                                        <a data-fancybox="gallery" href="statics/home/images/scr6.jpg"><img src="statics/home/images/scr6.jpg"></a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <div class="gallery__item">
                                        <a data-fancybox="gallery" href="statics/home/images/photo_lophoc.jpg"><img src="statics/home/images/photo_lophoc.jpg"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- <section class="course mb45 text-center">
                    <div class="container">
                        <div class="course__title">
                            <h3 class="wow fadeInDown animated animated" data-wow-offset="30" data-wow-duration="1.5s" data-wow-delay="0.15s">Chia sẻ & mua khóa học dễ dàng với Edubit</h3>
                            <p class="wow slideInUp animated animated" data-wow-offset="30" data-wow-duration="1.5s" data-wow-delay="0.15s">Mỗi khóa học sẽ có landing page riêng, tích hợp thanh toán online. Giảng viên, học viên có thể dễ dàng lấy link và chia sẻ.</p>
                        </div>
                        <div class="course__content">
                            <div class="course__content-img wow zoomIn animated animated" data-wow-offset="30" data-wow-duration="1.5s" data-wow-delay="0.15s">
                                <img src="statics/home/images/dskhoahoc.png" class="img-responsive">
                            </div>

                        </div>
                    </div>
                    <div class="curved-bg"></div>
                </section> -->
                <section class="partner mb45">
                    <div class="container">
                        <div class="title">
                            <h3>Đối tác tiêu biểu</h3>
                        </div>
                        <div class="partner__content">
                            <div class="owl-carousel owl-theme" id="owl-partner">
                                <div class="item">
                                    <div class="partner-item">
                                        <a href="#"><img src="statics/home/images/partner-img-1.png"></a>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="partner-item">
                                        <a href="#"><img src="statics/home/images/partner-img-2.png"></a>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="partner-item">
                                        <a href="#"><img src="statics/home/images/partner-img-3.png"></a>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="partner-item">
                                        <a href="#"><img src="statics/home/images/partner-img-7.png"></a>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="partner-item">
                                        <a href="#"><img src="statics/home/images/partner-img-8.png"></a>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="partner-item">
                                        <a href="#"><img src="statics/home/images/partner-img-9.png"></a>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="partner-item">
                                        <a href="#"><img src="statics/home/images/partner-img-10.png"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="course__content-btn text-center" >
                            <a href="register" style="margin: 30px 0;">DÙNG THỬ MIỄN PHÍ 14 NGÀY</a>
                        </div>
                    </div>
                </section>
            </main>


        </div>
    </div>
@endsection
