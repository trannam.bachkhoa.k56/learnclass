@extends('site.layout.site')
@section('title', $category->title)
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', '')

@section('content')
<div class="h1title" style="text-indent: -9999px; margin: 0;font-size: 1px;">
    <h1 style="font-size: 1px;">{{  $category->title }}</h1>
    <p class="des"> isset($information['meta_description']) ? $information['meta_description'] : ''</p>
</div>
  <section class="indexListPro categoryParent">
    <div class="container">
      <div class="row">
        <div class="col-12 titleParent">
          <h2>{{ $category->title }}</h2>
        </div>
      </div>
      <div class="row">
        @foreach (\App\Entity\Category::getChildrenCategory($category->category_id, 'product') as $childrenCate)
        <div class="col-lg-2 col-md-2 col-sm-3 col-4 itemtra noPadding">
          <div class="indexitem">
            <div class="img">
              <div class="CropImg">
                <div class="thumbs">
                  <a href="{{ route('site_category_product', ['cate_slug' => $childrenCate->slug])}}" title="{{ $childrenCate->title}}">
                    <img src="{{ asset($childrenCate->image) }}" alt="">
                  </a>
                </div>
              </div>
            </div>
            <div class="title">
              <h3><a href="{{ route('site_category_product', ['cate_slug' => $childrenCate->slug])}}" title="{{ $childrenCate->title}}">{{ $childrenCate->title}}</a></h3>
            </div>
          </div>
        </div>
        @endforeach
      </div>  
      <div class="row">
        <div class="col-12 des noPadding">
          <div class="content">
            <p>{!! $category->description !!}</p>
            
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
