@extends('site.layout.site')

@section('title','liên hệ' )
@section('meta_description',  '')
@section('keywords', '')

@section('content')
<section class="breadcrumb ds-inherit pd">
         <div class="bgbread">
            <div class="container">
               <div class="row">
                  <div class="col-12 pdtop15">
                     <h1 class="mbf20">Chi tiết thông báo</h1>
                     <ul>
                        <li><a href="./">Trang chủ</a></li>
                        <li>/</li>
                        <li><a href="#">chi tiết thông báo</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="categoryNew2 pd-30">
        <div class="container">
          <div class="row">
             <div class="col-lg-2 col-md-2 col-sm-12 col-12 bgxam pdtop10 pdbottom10">
              <div class="sidebarnew">
                <h2 class="f20 mgtop10 text-bnone pdtop10 pd-010 clorang mgborom20">Thông báo 
                </h2>
                <ul class="list">
                    @if(\App\Entity\Notification::showNotifyClass(0 || 1 || 2 ||3,'giao_vien'))
                        <li class="ds-block">
                            <a href="" title="" onclick="return seenNotification(this)" class="f16 ds-block pd-10 pd-010">Thông báo phòng học <span class="clred bgwhite f16 pd-06 ds-inline text-rt ">{{ \App\Entity\Notification::countNotifyDetail(0) }}</span> </a>
                        </li>
                    @elseif(\App\Entity\Notification::showNotifyClass(0 || 1 || 2 ||3,''))
                        <li class="ds-block">
                            <a href="" title="" onclick="return seenNotification(this)" class="f16 ds-block pd-10 pd-010">Thông báo khóa học <span class="clred bgwhite f16 pd-06 ds-inline text-rt ">{{ \App\Entity\Notification::countNotifyDetail(1) }}</span></a>
                        </li>
                    @endif
                    <li class="ds-block">
                        <a href="" title="" onclick="return seenNotification(this)" class="f16 ds-block pd-10 pd-010">Thông báo nạp tiền <span class="clred bgwhite f16 pd-06 ds-inline text-rt ">{{ \App\Entity\Notification::countNotifyDetail(2) }}</span></a>
                    </li>
                    <li class="ds-block">
                        <a href="" title="" onclick="return seenNotification(this)" class="f16 ds-block pd-10 pd-010">Thông báo hệ thống <span class="clred bgwhite f16 pd-06 ds-inline text-rt ">{{ \App\Entity\Notification::countNotifyDetail(3) }}</span></a>
                    </li>
                </ul>
              </div>
            </div>
            <script>
                          $(document).ready(function () {
                              var element = $('.sidebarnew');
                              var originalY = element.offset().top;
                           
                              // Space between element and top of screen (when scrolling)
                              var topMargin = 40;
                              
                              // Should probably be set in CSS; but here just for emphasis
                              element.css('position', 'relative');
                              
                              $(window).on('scroll', function(event) {
                                  var scrollTop = $(window).scrollTop();
                                  var topContent = $('.contentRightNew2').height();
                                  if (scrollTop < (topContent - 0)) {
                                      element.stop(false, false).animate({
                                          top:
                                              scrollTop < originalY ? 0 : scrollTop - originalY + topMargin
                                      }, 300);
                                  }
                           });
                        });
                          </script>
            <div class="col-lg-10 col-md-10 col-sm-12 col-12">
              <div class="contentRightNew2">
                <h3 class="text-bnone f20 mg-20">Khóa học photóhop của bạn đã đăng kí thành công</h3>
                <div class="content">
                

Vietbuild 2018 được tổ chức tại TP.HCM từ 21/06 - 25/06/2018 với quy mô lớn, quy tụ đông đảo nhãn hàng tham gia, đã diễn ra thành công tốt đẹp. LIXIL với khu trưng bày của 3 thương hiệu INAX, American Standard, cửa nhôm Tostem đã thu hút rất nhiều khách hàng đến tham quan và trải nghiệm. Đặc biệt, khu vực INAX với sản phẩm Regio mang lại sự ấn tượng về chiếc bồn cầu hiện đại, thông minh, sang trọng; không gian Everest tiện nghi, đẳng cấp; công nghệ Thermostat và Aqua Ceramic tương tác, chiêm ngưỡng ngay tại chỗ; và rất nhiều sản phẩm khác đã để lại dấu ấn trong lòng khách hàng tham quan. Không gian chung của LIXIL trong sự kiện Vietbuild lần này Ban Lãnh đạo LIXIL cắt băng khánh thành gian hàng vào ngày 21/06 Trong sự kiện lần này, LIXIL cũng như INAX vinh dự được đón tiếp những vị khách quý đã dành thời gian đến tham quan gian hàng như: Ông Nguyễn Văn Sinh - Thứ trưởng Bộ Xây dựng, Trưởng Ban chỉ đạo triển lãm Vietbuild; ông Nguyễn Tấn Vạn - Chủ tịch Hội KTS Việt Nam, Nguyên Thứ trưởng Bộ xây dựng; ông Khương Văn Mười - Phó Chủ tịch Hội KTS Việt Nam; ông Nguyễn Quang Cung - Phó Chủ tịch Hội Vật liệu Xây dựng Việt Nam và rất nhiều lãnh đạo khác. Tổng Giám đốc và Giám đốc Kinh doanh toàn quốc LIXIL Việt Nam giới thiệu về sản phẩm và công nghệ với các vị khách quý Khách hàng tham quan khu vực Shower Toilet Khu vực Gạch INAX Khách hàng hứng thú với mini game công nghệ Aqua Ceramic... Và những hoạt động khác
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
@endsection

