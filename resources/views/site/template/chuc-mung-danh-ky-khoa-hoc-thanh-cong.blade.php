@extends('site.layout.site')

@section('title', 'Chúc mừng bạn đã đăng ký lớp học thành công')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <section class="resTeacher Payteacher bgxamnhat">
        <div class="container">
            <div class="row justify-content-lg-center">
                <div class="col-12">
                    <h2 class="clblack f24 pd-30 text-ct">
                        Đăng ký lớp học
                    </h2>

                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                    <div class="content mg-20">
                        <div class="pd10 bgwhite clblack pdleft15">
                            {!! $post->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection