@extends('site.layout.site')

@section('title', 'Đăng ký trở thành giáo viên khi đã có tài khoản')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <section class="resTeacher bgxamnhat">
        <div class="container">
            <div class="row justify-content-lg-center">
                <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                    <h2 class="clblack f24 pd-30 text-ct">
                        Đăng kí tài khoản giảng viên
                    </h2>
                    <form class="bgwhite pd15 mg-30" style="border: 1px solid #ddd; box-shadow: 0 1px 1px rgba(0, 0, 0, .05);" action="{{ route('register_teacher') }}" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <h3 class="clblack f24 pd-30 text-ct">
                            Thông tin giảng dạy
                        </h3>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Đơn vị công tác</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="where_working" placeholder="Đơn vị công tác" value="{{ old('where_working') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Lĩnh vực giảng dạy</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="teaching_field" placeholder="Lĩnh vực giảng dạy" value="{{ old('teaching_field') }}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Trình độ học vấn</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="academic_standard" placeholder="Trình độ học vấn" value="{{ old('academic_standard') }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Hình thức giảng dạy</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="form_of_teaching" placeholder="Giáo viên, gia sư" value="{{ old('form_of_teaching') }}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Công việc hiện tại</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="current_job" placeholder="Công việc hiện tại: giáo viên, sinh viên,..." value="{{ old('current_job') }}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Môn giảng dạy</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="teaching_subject" placeholder="Môn giảng dạy chinh" value="{{ old('teaching_subject') }}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Tệp thông tin cá nhân</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <div class="input-group control-group increment" >
                                    <input type="file" name="filename[]" value="" class="form-control">
                                    <div class="input-group-btn">
                                        <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>  Thêm tệp</button>
                                    </div>
                                </div>
                                <div class="clone hide">
                                    <div class="control-group input-group" style="margin-top:10px">
                                        <input type="file" name="filename[]" value="" class="form-control">
                                        <div class="input-group-btn">
                                            <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Xóa tệp</button>
                                        </div>
                                    </div>
                                </div>

                                <script type="text/javascript">

                                    $(document).ready(function() {

                                        $(".btn-success").click(function(){
                                            var html = $(".clone").html();
                                            $(".increment").after(html);
                                        });

                                        $("body").on("click",".btn-danger",function(){
                                            $(this).parents(".control-group").remove();
                                        });

                                    });

                                </script>
                                <style>
                                    .hide {
                                        display: none;
                                    }
                                </style>
                            </div>
							<p><i>Vui lòng nhập tài liệu tiếng việt không có dấu và không chứa dấu (,).</i></p>
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 pdtop30">
                                <button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite btnloadding">Gửi duyệt</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection