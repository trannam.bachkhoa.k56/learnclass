@extends('site.layout.site')

@section('title', "Đăng ký tài khoản")
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <section class="resTeacher bgxamnhat">
        <div class="container">
            <div class="row justify-content-lg-center">
                <div class="col-xl-6 col-xl-offset-3 col-lg-9 col-lg-offset-3 col-md-12  col-sm-12 col-12">
                    <h2 class="clblack f24 pd-30 text-ct">
                        Đăng ký
                    </h2>
                    {{--<div class="form-group">
                        <div class="button-lg row">
							<div class="col-md-6 text-ct">
								<a class="loginf" href="{{ route('google_login') }}" id="Facebook" value="Facebook"><i class="fab fa-facebook-f"></i>Facebook</a>
							</div>	
							<div class="col-md-6 text-ct">
								<a class="loginfg" href="{{ $urlLoginFace }}" id="Google" value="Google"><i class="fab fa-google-plus-g"></i>Google+</a>
							</div>
                        </div>
                    </div>--}}
					<!-- <div class="clearfix"><div class="orClass"><span>hoặc</span></div></div> -->
                    <form class="bgwhite pd15 mgbottom30" style="border: 1px solid #ddd; box-shadow: 0 1px 1px rgba(0, 0, 0, .05);" action="/dang-ky" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label"><span class="text-b700">Họ và tên</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control f14" name="name" placeholder="Họ và tên" value="{{ old('name') }}" required>
                            </div>
                        </div>
						<div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label"><span class="text-b700">Điện thoại</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control f14" name="phone" placeholder="Điện thoại" value="{{ old('phone') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label"><span class="text-b700">Email:</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control f14" name="email" placeholder="email" value="{{ old('email') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label"><span class="text-b700">Mật khẩu:</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control f14" name="password" placeholder="Mật khẩu" value="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label"><span class="text-b700">Nhập lại Mật khẩu:</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control f14" name="password_confirmation" placeholder="Mật khẩu đăng nhập" value="" required>
                            </div>
                        </div>
                        @if ($errors->has('email'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                        @if ($errors->has('password'))
                            <div class="alert alert-danger" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif
                        <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8 pdtop30">
                                <button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite btnloadding">Đăng ký</button>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection