@extends('site.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <section class="resTeacher bgxamnhat">
        <div class="container">
            <div class="row justify-content-lg-center">
                <div class="col-xl-6 col-xl-offset-3 col-lg-9 col-lg-offset-3 col-md-12  col-sm-12 col-12">
                    <h2 class="clblack f24 pd-30 text-ct">
                        Đăng nhập
                    </h2>
					
                    {{--<div class="form-group mgbottom0">
                        <div class="button-lg row">
							<div class="col-md-6 text-ct">
								
								<a class="loginf" href="{{ $urlLoginFace }}" id="Google" value="Google">
									<i class="fab fa-facebook-f"></i>Facebook
									
								</a>
							</div>
							<div class="col-md-6 text-ct">
								<a class="loginfg" href="{{ route('google_login') }}" id="Facebook" value="Facebook">
									<i class="fab fa-google-plus-g"></i>Google+
								</a>
							</div>	
                        </div>
                    </div>--}}
					<!-- <div class="clearfix"><div class="orClass"><span>hoặc</span></div></div> -->
                    <form class="bgwhite pd15 mgbottom30" style="border: 1px solid #ddd; box-shadow: 0 1px 1px rgba(0, 0, 0, .05);" action="/dang-nhap" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label"><span class="text-b700">Tài khoản</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control f14" name="email" placeholder="Tài khoản đăng nhập" value="{{ old('name') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label"><span class="text-b700">Mật khẩu</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control f14" name="password" placeholder="Mật khẩu đăng nhập" value="" required>
                            </div>
                        </div>
                        <div class="checkbox col-md-8 offset-md-4">
							<div class="checkbox tiny m-b-2">
								<div class="checkbox-overlay">
									<input type="checkbox" />
									<div class="checkbox-container">
										<div class="checkbox-checkmark"></div>
									</div>
                                    <label>Ghi nhớ mật khẩu</label>
								</div>
							</div>
                        </div>
                        @if($errors->any())
                            @foreach ($errors->all() as $error)
                            <div class="alert alert-danger" role="alert">
                                <strong>{{ $error }}</strong>
                            </div>
                            @endforeach
                        @endif
                        <div class="form-group row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8 pdtop30">
                                <button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite btnloadding">Đăng nhập</button>

                             

                            </div>
                        </div>
						<div class="form-group row">
							<div class="col-sm-8 offset-4">
								<span class="left mgright20"><a href="/trang/quen-mat-khau" title="Quên mật khẩu">Quên mật khẩu ? </a></span>
								<span class="right"><a href="/dang-ky" title="">Bạn chưa có tài khoản ? </a></span>
							</div>
						</div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection