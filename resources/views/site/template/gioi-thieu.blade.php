@extends('site.layout.site')
@section('title', 'giới thiệu')
@section('meta_description',  '')

@section('content')
    <div class="h1title" style="text-indent: -9999px; margin: 0;font-size: 1px;">
        <h1 style="font-size: 1px">{{$post->title}}</h1>
    </div>
    <section class="category categoryNew detailNew">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbcate">
                        <ul>
                            <li><a href="/" title="">Trang chủ</a></li>
                            <li> / </li>

                            <li><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" title="" class="active"> {{$post->title}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-12 cateleft">
                    @include('site.partials.sidebar')
                </div>

                <div class="col-lg-9 col-md-9 col-sm-12 col-12  cateRight catenew">
                    <div class="titlecate titlenew">
                        <h2>{{$post->title}}</h2>
                    </div>
                    <div class="contentNew">
                        {!! $post->description !!}
                        {!! $post->content !!}
                    </div>


                    <div class="titlecate">
                        <h2>Tin tức cùng thể loại</h2>
                    </div>


                </div>
            </div>
        </div>


        </div>
        </div>
    </section>
@endsection
