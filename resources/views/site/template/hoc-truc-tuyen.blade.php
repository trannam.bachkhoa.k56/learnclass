<!DOCTYPE html>
<html lang="en">
<head>
    <title>Lớp học trực tuyến</title>
    <meta name="ROBOTS" content="index, follow"/>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="title" content="@yield('title')"/>
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta itemprop="image" content="@yield('meta_image')" />
    <!-- facebook gooogle -->
    <!-- <meta property="fb:app_id" content="" />

    <!-- css he thong -->
    <link rel="stylesheet" href="{{ asset('learn-online/css/bootstrap.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('learn-online/css/bootstrap-grid.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('learn-online/css/bootstrap-reboot.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('learn-online/css/fontawesome-all.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('learn-online/css/style_class_room.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('learn-online/css/getMediaElement.css') }}">
    <!-- tab -->
    <!-- script he thong -->
</head>

<body>
<div class="row">
    <div class="col-12 col-md-9">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="boardFirst-tab" data-toggle="tab" href="#boardFirst" role="tab" aria-controls="home" aria-selected="true">Bảng số 1</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="boardSecond-tab" data-toggle="tab" href="#boardSecond" role="tab" aria-controls="profile" aria-selected="false">Bảng số 2</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="boardThird-tab" data-toggle="tab" href="#boardThird" role="tab" aria-controls="contact" aria-selected="false">Bảng số 3</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="boardFourth-tab" data-toggle="tab" href="#boardFourth" role="tab" aria-controls="contact" aria-selected="false">Bảng số 4</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="boardFifth-tab" data-toggle="tab" href="#boardFifth" role="tab" aria-controls="contact" aria-selected="false">Bảng số 5</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="boardFirst" role="tabpanel" aria-labelledby="boardFirst-tab">
                <button id="share-boardFirst" disabled>Bật bảng số 1</button>
                <div id="videos-boardFirst"></div>
            </div>
            <div class="tab-pane fade" id="boardSecond" role="tabpanel" aria-labelledby="boardSecond-tab">
                <button id="share-boardSecond" disabled>Bật bảng số 2</button>
                <div id="videos-boardSecond"></div>
            </div>
            <div class="tab-pane fade" id="boardThird" role="tabpanel" aria-labelledby="boardThird-tab">
                <button id="share-boardThird" disabled>Bật bảng số 3</button>
                <div id="videos-boardThird"></div>
            </div>
            <div class="tab-pane fade" id="boardFourth" role="tabpanel" aria-labelledby="boardFourth-tab">
                <button id="share-boardFourth" disabled>Bật bảng số 4</button>
                <div id="videos-boardFourth"></div>
            </div>
            <div class="tab-pane fade" id="boardFifth" role="tabpanel" aria-labelledby="boardFifth-tab">
                <button id="share-boardFifth" disabled>Bật bảng số 5</button>
                <div id="videos-boardFifth"></div>
            </div>
        </div>
    </div>
    <input type="hidden" value="1" id="isTeacher">
    <div class="col-12 col-md-3">
        <div class="studentAndTeacher">
            <div class="">
                <div class="make-center">
                    <input type="text" id="room-id" value="abcdef">
                    <button id="open-room" class="btn btn-danger">Open Room</button>
                    <button id="join-room" class="btn btn-warning">Join Room</button>
                </div>
                <button class="btn btn-primary">Kết thúc</button> /
                <span>1:50:36</span>
            </div>
            <div class="teacher">
                <div id="teacher"></div>
                <p>(GV) Trần Hải Nam</p>
            </div>
            <div>
                <ul class="nav nav-tabs" id="studentTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="join-tab" data-toggle="tab" href="#join" role="tab" aria-controls="home" aria-selected="true">Tham gia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="question-tab" data-toggle="tab" href="#question" role="tab" aria-controls="profile" aria-selected="false">Đặt câu hỏi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="document-tab" data-toggle="tab" href="#document" role="tab" aria-controls="contact" aria-selected="false">Tài liệu</a>
                    </li>
                </ul>
                <div class="tab-content" id="studentTabContent">
                    <div class="tab-pane fade show active" id="join" role="tabpanel" aria-labelledby="join-tab">
                        <div id="student-video"></div>
                    </div>
                    <div class="tab-pane fade" id="question" role="tabpanel" aria-labelledby="question-tab">
                        Đặt câu hỏi tại đây
                    </div>
                    <div class="tab-pane fade" id="document" role="tabpanel" aria-labelledby="document-tab">
                        Tài liệu tham khảo
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script src="{{ asset('learn-online/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('learn-online/js/bootstrap.js') }}"></script>
    <script src="{{ asset('learn-online/js/bootstrap.bundle.js') }}"></script>

    <script src="{{ asset('learn-online/js/RTCMultiConnection.js') }}"></script>
    <script src="https://149.28.155.231:8080/socket.io/socket.io.js"></script>

    <script src="{{ asset('learn-online/js/getMediaElement.js') }}"></script>
    <script src="{{ asset('learn-online/js/getScreenId.js') }}"></script>

    <script src="{{ asset('learn-online/js/video-screen-sharing.js') }}"></script>
</body>
</html>
