@extends('site.layout.site')

@section('title',' Hướng dẫn' )
@section('meta_description',  ' Hướng dẫn sử dụng phòng học trực tuyến')
@section('keywords', '')

@section('content')
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1>Danh sách khóa học</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Danh sách khóa học</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="category">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">               
                    <marquee class="gioithieu">
                        <ul>
                            @foreach (\App\Entity\Classroom::startClassroom() as $classroom)
                                <li><a href="">
                                        <img src="{{ asset('images/running.gif') }}" alt="">{{ $classroom->classroom_name }} - {{ $classroom->teacher_name }} - {{ $classroom->lesson_title }}:  {{ \App\Ultility\Ultility::formatTime($classroom->time_start) }}</a></li>
                            @endforeach
                        </ul>
                    </marquee>
                </div>
            </div>
        </div>
        </div>
    </section>
    <style type="text/css" media="screen">

        .gtright .content
        {
            border-left: 2px dotted;
        }
        .sticky {
            position: fixed;
            top: 0;
            z-index: 100;
            border-top: 0;
        }
        .titleRighthd
        {
            border-bottom: 2px solid;
        }
       /*  .gtright
        {
            max-height: 1000px;
            overflow: scroll; em nhúng khong chay dc anh ạ
            

        } */
    </style>
  <!--   <script>
        $(document).ready(function () {
            var element = $('.contentleft');
            var originalY = element.offset().top;
			
            var topMargin = 20;
            
            element.css('position', 'relative');
            
            $(window).on('scroll', function(event) {
                var scrollTop = $(window).scrollTop();
                var topContent = $('.ContentView').height();
				  var widthside = $( window ).width();
                if (scrollTop < (topContent - 500) && widthside >= 500) {
					
                    element.stop(false, false).animate({
                        top:
                            scrollTop < originalY ? 0 : scrollTop - originalY + topMargin
                    }, 300);
                }
				if($(window).scrollTop() >= 220){
					$('.logoShow').show();
				}
				else
				{
					$('.logoShow').hide();
				}
			});
		});
	</script> -->

    <section class="categoryCourse">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-12 sidegt noPadding widgetScroll">
                    <div class="contentleft boxShadow pd20 bgwhite mgtop30">
						<!-- <a href="/" title="logo" class="logosidabar mgbottom20 ds-block text-ct"><img src="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}"  alt="logo"></a> -->
                       
                        @foreach (\App\Entity\Menu::showWithLocation('menu-huong-dan') as $Mainmenu_huong_dan)
                            @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu_huong_dan->slug) as $idhuongdan=>$element_huongdan)
                        <h3 class="clblack clhr-orang f16 titlegt">{{ isset($element_huongdan['title_show']) ? $element_huongdan['title_show'] : '' }}</h3>
                            @if (!empty($element_huongdan['children']))
        						<ul class="mgbottom20">
                                    @foreach ($element_huongdan['children'] as $element_huongdan2)
        							<li><a class="text-bnone f14 text-ca ds-block clblack pd-5" href="{{ isset($element_huongdan2['url']) ?$element_huongdan2['url'] : '' }}"
                                      class="clblack f14 ds-block pd-5" title="{{ isset($element_huongdan2['title_show']) ?$element_huongdan2['title_show'] : '' }}"><i class="fas fa-check pdleft5 mgright5"></i>
                                                {{ isset($element_huongdan2['title_show']) ?$element_huongdan2['title_show'] : '' }}</a>
                                    </li>
                                    @endforeach
        						</ul>
                             @endif
                            @endforeach
                        @endforeach



                       <!--  <ul class="pdleft20 f14">
=======
						<ul>
							@foreach (\App\Entity\Menu::showWithLocation('menu-huong-dan') as $Mainmenu)
							  @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menu)
								<li><a class="text-bnone f14 text-ca" href="#{{ $menu['url'] }}" title="{!! $menu['title_show'] !!}">{!! $menu['title_show'] !!}</a></li>
							  @endforeach
							@endforeach
						</ul>
                        <ul class="pdleft20 f14">


                            @foreach(\App\Entity\Post::categoryShow('danh-cho-hoc-sinh',100) as $introstudents)
                                <li><h3 class="text-bnone f14 text-ca"><a href="#{{ isset($introstudents['slug']) ?$introstudents['slug'] : '' }}"  class="clblack f14 ds-block pd-5" title="{{ isset($introstudents['title']) ?$introstudents['title'] : '' }}"><i class="fas fa-check pdleft5"></i>
                                            {{ isset($introstudents['title']) ?$introstudents['title'] : '' }}</a></h3>
                                </li>
                            @endforeach

                        </ul> -->
                    </div>
                </div>

                <div class="col-lg-9 col-md-8 col-sm-12 gtright f14 pd-30 ContentView">
                    <!-- <h2 class="text-up f24 clorang mgbottom30 ds-inline pdbottom10 titleRighthd">{{ isset($category['title']) ? $category['title'] : '' }}</h2> -->
                    <div class="mgbottom30"> 
                        <a id="{{ isset($element_huongdan2['menu_id']) ?$element_huongdan2['menu_id'] : '' }}" style="text-indent: -9999px" class="ds-block mgbottom20"></a>
                        <h3 class="f18 clorang mg-20"><i class="fas fa-plus mgright10"></i>{{ isset($post['title']) ?$post['title'] : '' }}</h3>
                        <div class="content pdleft20 brcl-orang mgleft5">
                        
                         {!! isset($post->content) ? $post->content : 'Đang cập nhật'  !!} 
                        </div>
                    </div>
                </div>

                {{-- <div class="col-lg-9 col-md-8 col-sm-12 gtright f14 pd-30 ContentView">
                    @foreach (\App\Entity\Menu::showWithLocation('menu-huong-dan') as $Mainmenu_huong_dan)
                        @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu_huong_dan->slug) as $idhuongdan=>$element_huongdan)
                            <!-- <h2 class="text-up f24 clorang mgbottom30 ds-inline pdbottom10 titleRighthd">{{ isset($element_huongdan['title_show']) ? $element_huongdan['title_show'] : '' }}</h2> -->
                            @if (!empty($element_huongdan['children']))
                                @foreach ($element_huongdan['children'] as $element_huongdan2)
                                <div class="mgbottom30"> 
                                    <a id="{{ isset($element_huongdan2['menu_id']) ?$element_huongdan2['menu_id'] : '' }}" style="text-indent: -9999px" class="ds-block mgbottom20"></a>
                                    <h3 class="f18 clorang mg-20"><i class="fas fa-plus mgright10"></i>{{ isset($element_huongdan2['title_show']) ?$element_huongdan2['title_show'] : '' }}</h3>
                                    <div class="content pdleft20 brcl-orang mgleft5">
                                    @php 
                                        $arrayslug = array();
                                        $urlslug = isset($element_huongdan2['url']) ? $element_huongdan2['url'] : '';
                                        $arrayslug = explode("/",$urlslug);
                                        $post =(\App\Entity\Post::getPostDetail($arrayslug[2]));

                                    @endphp
                                    
                                     {!! isset($post->content) ? $post->content : 'Đang cập nhật'  !!} 
                                    </div>
                                </div>
                                @endforeach
                            @endif
                        @endforeach
                    @endforeach
 <!--  -->
                    <!-- <h2 class="text-up f24 clorang mgbottom30 ds-inline pdbottom10">Hướng dẫn</h2>
                    @foreach(\App\Entity\Post::categoryShow('danh-cho-giang-vien',100) as $introteachers)
                        <div>
                            <a id="{{ isset($introteachers['slug']) ?$introteachers['slug'] : '' }}" style="text-indent: -9999px" class="ds-block mgbottom20"></a>
                            <h3 class="f18 clorang mg-20"><i class="fas fa-plus mgright10"></i>{{ isset($introteachers['title']) ?$introteachers['title'] : '' }}</h3>
                            <div class="content pdleft20 brcl-orang mgleft5">
                                {!! isset($introteachers['content']) ?$introteachers['content'] : ''  !!}
                            </div>
                        </div>
                    @endforeach

                    <h2 class="text-up f24 clorang mgbottom30 ds-inline pdbottom10">DÀNH CHO HỌC SINH</h2>
                    @foreach(\App\Entity\Post::categoryShow('danh-cho-hoc-sinh',100) as $introstudents)
                        <div>
                            <a id="{{ isset($introstudents['slug']) ?$introstudents['slug'] : '' }}" style="text-indent: -9999px" class="ds-block mgbottom20"></a>
                            <h3 class="f18 clorang mg-20"><i class="fas fa-plus mgright10"></i>{{ isset($introstudents['title']) ?$introstudents['title'] : '' }}</h3>
                            <div class="content pdleft20 brcl-orang mgleft5">
                                {!! isset($introstudents['content']) ?$introstudents['content'] : ''  !!}
                            </div>
                        </div>
                    @endforeach -->
                </div> --}}





                <!-- END CONTEND -->

            </div>
        </div>
    </section> 
@endsection

