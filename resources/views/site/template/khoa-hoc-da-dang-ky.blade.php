@extends('site.layout.site')

@section('title', 'Khóa học đã đăng ký')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <?php
    $user = \Illuminate\Support\Facades\Auth::user();
    ?>
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20">Người dùng {{ $user->name }}</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Người dùng {{ $user->name }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="teacher bggray pdbottom30">
        <div class="container">
			@include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-5 col-sm-12 col-12 sidebarContact">
                    @include('site.module_learnclass.menu_user')
                </div>
                <div class="col-lg-9 col-md-7 col-sm-12 col-12 ">
                    <div class="teacherRight clblack text-lt">
                        <h2 class="f24 text-b700 pdbottom20 ">Danh sách khóa học đã đăng ký</h2>
						<div class="row">
							@foreach (\App\Entity\Classroom::getClassroomsOfUser($user->id) as $classroom)
							<div class="col-xl-4 col-lg-6 col-md-12">
								 <div class="item">
                                    @include('site.module_learnclass.product_item', ['product' => $classroom])
                                </div>
							</div>
							@endforeach
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
