@extends('site.layout.site')

@section('title',' Kích hoạt khóa học' )
@section('meta_description',  'Kích hoạt khóa học')
@section('keywords', '')

@section('content')
    <section class="activatedLearn mg-15">
        <div class="container">
            <div class="row justify-content-lg-center">
                <div class="col-lg-8 col-md-12 activatedLearnContent">
                    <div class="img text-ct ds-block">
                        <img src="{{ asset('khoahoc/images/iconkey.jpg') }}" alt="" class="ds-inline">
                        {{--<img src="{{ asset('khoahoc/images/lock.png') }}" alt="" class="ds-inline">--}}
                    </div>
                    <div class="contentactive">
                        <div class="row justify-content-lg-center">
                            <div class="col-lg-8 col-md-12">
                                <p class="f16 note"><span class="ds-inline pd-3 pd-015 clwhite mgright5">
                                        Lưu ý</span> Mỗi thẻ học chỉ nạp được cho 1 tài khoản duy nhất. Không nạp được cho tài khoản thứ 2
                                </p>

                                <p class="f14 step "> <span class="number ds-inline pd-3 pd-010 clwhite">1</span> Bạn <span class="text-b700">chưa có</span>  thẻ ? Vui lòng nhấn vào đây<a href="/trang/lua-chon-phuong-thuc-thanh-toan">Để mua thẻ.</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="activeForm">
                        <form action="{{ route('unlock_card') }}" method="get" class="text-ct">
                            <input type="text" class="w100 ds-block text-ct pd-8 mgbottom20" name="code_card" placeholder="Nhập mã kích hoạt .ví dụ 1301572">
                            <button type="submit" class="ds-inline pd-8 pd-025 mgbottom20 f16 clwhite"><i class="fas fa-unlock"></i> Kích hoạt thẻ học</button>
                        </form>
                        @if($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger" role="alert">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="text-ct ds-block">
                        <p class="text-b700 mgbottom5">Nếu gặp khó khăn trong việc kích hoạt khóa học ? bạn vui lòng liên hệ với chúng tôi theo số:</p>
                        <p class="text-b700">093 455 3435 - 097 456 1735</p>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection

