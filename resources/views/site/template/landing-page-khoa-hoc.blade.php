<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta Tags -->
    <meta property="fb:pages" content="" />
    <meta name="adx:sections" content="" />
    <meta name="p:domain_verify" content=""/>
    <meta name="google-site-verification" content="" />
    <meta name="google-site-verification" content="" />
    <meta http-equiv="content-type" content="text/html" />
    <meta charset="utf-8" />
    <title>
        {{ $product->title }}
    </title>
    <meta name="title" content="{{ $product->title }}"/>
    <meta name="description" content="{{  !empty($product->meta_description) ? $product->meta_description : $product->description }}" />
    <meta name="keywords" content="{{ $product->meta_keyword }}" />

    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:locale" content="vi_VN"/>
    <meta property="og:type" content="article"/>

    <meta property="og:title" content="{{ $product->title }}" />
    <meta property="og:description" content="{{  !empty($product->meta_description) ? $product->meta_description : $product->description }}" />
    <meta property="og:url" content="{{ route('product', [ 'post_slug' => $product->slug])  }}" />
    <meta property="og:image" content="{{ asset($product->image) }}" />
    <meta property="og:image:secure_url" content="{{ asset($product->image) }}" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="{{  !empty($product->meta_description) ? $product->meta_description : $product->description }}" />
    <meta name="twitter:title" content="{{ $product->title }}" />
    <meta name="twitter:image" content="{{ route('product', [ 'post_slug' => $product->slug])  }}" />

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:600,700" rel="stylesheet">
    <link href="{{ asset('templateCourse/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('templateCourse/css/fontawesome-all.css') }}" rel="stylesheet">
    <link href="{{ asset('templateCourse/css/swiper.css') }}" rel="stylesheet">
    <link href="{{ asset('templateCourse/css/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('templateCourse/css/styles.css') }}" rel="stylesheet">

    <!-- Favicon  -->
    <link rel="icon" href="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}">
</head>
<body data-spy="scroll" data-target=".fixed-top">

<!-- Preloader -->
<div class="spinner-wrapper">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<!-- end of preloader -->


<!-- Navbar -->
<nav class="navbar navbar-expand-md navbar-dark navbar-custom fixed-top">
    <div class="container">
        <a class="navbar-brand logo-image page-scroll" href="">
            <img src="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}" alt="logo"></a>
    </div> <!-- end of container -->
</nav> <!-- end of navbar -->
<!-- end of navbar -->


<!-- Header -->
<header id="header" class="header">

    <div class="header-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xl-6 ">
                    <img class="img-fluid" src="{{ asset('templateCourse/images/banner-hocslim.png') }}"
                         alt="alternative">
                </div>
                <div class="col-lg-6 col-xl-6 ">
                    <div class="text-container">
                        <h1 class="h1-large">CÁCH ÍT NGƯỜI BIẾT ĐỂ CÓ <span
                                    id="js-rotating">WEBSITE CHẠY QUẢNG CÁO </span></h1>
                        <p class="p-large p-heading">Hãy tham gia khóa học làm website bất động sản trong 2 tuần, bạn sẽ
                            có 1 nền tàng công nghệ để làm bất kỳ website bất động sản nào bạn muốn mà ko phải tốn thêm
                            chi phí cho bên thứ 3.</p>
                        <a class="btn-solid-lg page-scroll" href="#registerForm">NHANH TAY ĐĂNG KÝ NÀO</a>
                    </div>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of header-content -->
</header> <!-- end of header -->
<!-- end of header -->


<!-- Objectives -->
<div id="approval" class="basic-1">
    <div class="row no-gutters">
        <div class="col-lg-6">

            <!-- Text Container -->
            <div class="area-1">
                <div class="text-container">
                    <span class="fa-stack fa-lg">
                        <img src="{{ isset($teacher->image) && !empty($teacher->image) ? asset($teacher->image) : asset('images/no_teacher.png')  }}" alt=""
                        style="width: 3rem;border-radius: 50%;margin-top: -30px;">
                    </span>
                    <h3>{{ $teacher->name }}</h3>
                    <p>{!! $teacher->description !!}</p>
                </div> <!-- end of text-container -->
            </div> <!-- end of area-1 -->
            <!-- end of text container -->

        </div> <!-- end of col -->
        <div class="col-lg-6">

            <!-- Text Container -->
            <div class="area-2">
                <div class="text-container">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-building fa-stack-1x fa-inverse"></i>
                            </span>
                    <h3>Thông tin khóa học</h3>
                    <h4 class="purple">Chỉ với:
                        <span class="purple">{{ (!empty($product->price)) ? number_format($product->price, 0, ',', '.')." VNĐ" : 'Miễn phí' }}
                        </span><span class="clgrey f14"> / {{ $lessons->count()*2 }} giờ</span>
                    </h4>
                    <p>Hình thức giảng dạy: <strong class="purple">dạy trực tuyến</strong>, chỉ cần laptop và tai nghe.</p>
                    <p>Ngày bắt
                        đầu: {{ App\Ultility\Ultility::formatDateTime($product->started_time, $product->started_date) }}</p>
                    <p>Số buổi: {{ $lessons->count() }} buổi</p>
                </div>
            </div> <!-- end of text-container -->
        </div> <!-- end of area-2 -->
        <!-- end of text container -->

    </div> <!-- end of col -->
</div> <!-- end of row -->
</div> <!-- end of basic-1 -->
<!-- end of objectives -->

<!-- Services -->
<div id="services" class="cards-1">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2><span class="purple">LỢI ÍCH</span> KHÓA HỌC</h2>
                <p class="p-heading p-large">Rất nhiều ưu đại, và lợi ích khi bạn tham gia khóa học làm website với bất động sản.</p>
            </div> <!-- end of col -->
        </div> <!-- end of row -->

        <div class="row">
            <div class="col-xs-12 col-lg-4">

                <!-- Card -->
                <div class="card text-center">
                    <div class="card-body">
                        <h4 class="card-title">HỌC TRỰC TUYẾN</h4>
                        <p>Bạn có thể tham gia khóa học tại bất kỳ đâu, nhưng lại vẫn có thể tương tác trực tiếp với thầy giáo để hiểu rõ khóa học.</p>
                    </div>
                </div>
                <!-- end of card -->

            </div> <!-- end of col -->
            <div class="col-xs-12 col-lg-4">

                <!-- Card -->
                <div class="card text-center">
                    <div class="card-body">
                        <h4 class="card-title">TẶNG 2 BỘ WEBSITE BẤT ĐỘNG SẢN</h4>
                        <p>Sau khi kết thúc khóa học, bạn sẽ được tặng 2 bộ theme website bất động sản với độ tùy biến cao.</p>
                    </div>
                </div>
                <!-- end of card -->

            </div> <!-- end of col -->

            <div class="col-xs-12 col-lg-4">

                <!-- Card -->
                <div class="card text-center">
                    <div class="card-body">
                        <h4 class="card-title">TẶNG GÓI HOSTING 6 THÁNG</h4>
                        <p>Our techniques and expertise allow us to be efficient during interviews and find out if the
                            candidate is fit soon in the process</p>
                    </div>
                </div>
                <!-- end of card -->

            </div> <!-- end of col -->

            <div class="col-xs-12 col-lg-4">

                <!-- Card -->
                <div class="card text-center">
                    <div class="card-body">
                        <h4 class="card-title">TỰ LÊN KẾ HOẠCH SEO VÀ CHECK SEO</h4>
                        <p>Khóa học sẽ giúp bạn lắm được những kiến thức cơ bản nhất về seo, giúp bạn bán hàng trên website của mình.</p>
                    </div>
                </div>
                <!-- end of card -->

            </div> <!-- end of col -->
            <div class="col-xs-12 col-lg-4">

                <!-- Card -->
                <div class="card text-center">
                    <div class="card-body">
                        <h4 class="card-title">TỰ LÊN KẾ HOẠCH CHẠY QUẢNG CÁO</h4>
                        <p>Bạn sẽ biết cách tối ưu hóa tiền quảng cáo, lên kế hoạch quảng cáo cho google một cách hiệu quả.</p>
                    </div>
                </div>
                <!-- end of card -->

            </div> <!-- end of col -->

            <div class="col-xs-12 col-lg-4">

                <!-- Card -->
                <div class="card text-center">
                    <div class="card-body">
                        <h4 class="card-title">RẤT NHIỀU KIẾN THỨC BỔ ÍCH KHÁC NỮA</h4>
                        <p>Tự cấu hình website, tự thay đổi bố cục, thay đổi thông tin website, tự upload website lên hosting để quản lý, ...</p>
                    </div>
                </div>
                <!-- end of card -->

            </div> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div> <!-- end of cards-1 -->
<!-- end of services -->


<!-- Jobs List -->
<div class="basic-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2><span class="purple-light">NỘI DUNG</span> KHÓA HỌC</h2>

            </div> <!-- end of col -->
        </div> <!-- end of row -->

        <!-- List -->
        <div class="row">
            @php
                $isStudent = false;
                if (\Illuminate\Support\Facades\Auth::check()) {
                    foreach ($studentOfClassrooms as $studentOfClassroom) {
                        if ($studentOfClassroom->user_id == \Illuminate\Support\Facades\Auth::user()->id) {
                            $isStudent = true;
                        }
                    }
                }
            @endphp
            @foreach ($lessons as $id => $lesson)
                @php
                    $dateStart = $lesson->date_at.' '.$lesson->time_start;
                    $timeStart = strtotime($dateStart);
                    $dateEnd = $lesson->date_at.' '.$lesson->time_end;
                    $timeEnd = strtotime($dateEnd);
                @endphp
                <div class="col-md-6">
                    <div class="text-container">
                        <h5 class="purple-light">Buổi {{ ($id + 1) }}  : {{ $lesson->title }}</h5>

                        <p class="p-small"><i class="fas fa-calendar-alt"></i>{{ \App\Ultility\Ultility::formatTime($lesson->time_start) }} {{ \App\Ultility\Ultility::formatDate($lesson->date_at) }}</p>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
            @endforeach
        </div> <!-- end of row -->

        <!-- end of list -->

    </div> <!-- end of container -->
</div> <!-- end of basic-2 -->
<!-- end of jobs list -->


<!-- Application Form -->
<div id="application" class="form-1">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">

                <!-- Text Container -->
                <div class="text-container">
                    <h3>3 Bước đơn giản để tham gia khóa học</h3>
                    <ul class="list-unstyled v-space-lg">
                        <li class="media">
                            <i class="fas fa-chevron-right"></i>
                            <div class="media-body">1. Điền và gửi thông tin vào form cho chúng tôi.</div>
                        </li>
                        <li class="media">
                            <i class="fas fa-chevron-right"></i>
                            <div class="media-body">2. Chúng tôi sẽ liên hệ với bạn để xác nhận thông tin.
                            </div>
                        </li>
                        <li class="media">
                            <i class="fas fa-chevron-right"></i>
                            <div class="media-body">3. Bạn sẽ được cấp tài khoản để tham gia khóa học.</div>
                        </li>
                    </ul>
                </div> <!-- end of text-container -->
                <!-- end of text container -->

            </div> <!-- end of col -->
            <div class="col-lg-6" id="registerForm">

                <!-- Form Container -->
                <form action="{{ route('sub_contact') }}" method="post" enctype="multipart/form-data" data-toggle="validator"
                      data-focus="false" onSubmit="return contact(this);">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <input type="text" class="form-control-input" id="gname" name="name" placeholder="Họ và tên" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control-input" id="gemail" name="email" placeholder="Email" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control-input" id="phone" name="phone" placeholder="Số điện thoại" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control-input" id="phone" name="address" placeholder="Địa chỉ" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <input type="hidden" class="form-control-input" id="gjob" name="message" value="{{ $product->title }}" >
                    <input type="hidden" class="form-control-input" id="gjob" name="is_ajax" value="1" >
                    <div class="form-group">
                        <button type="submit" class="form-control-submit-button">Đăng ký với chúng tôi</button>
                    </div>
                    <div class="form-message">
                        <div id="gmsgSubmit" class="h3 text-center hidden"></div>
                    </div>
                </form>
                <!-- end of form container -->

            </div> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div> <!-- end of form-1 -->
<!-- end of application form -->


<!-- Jobs Panel -->
<div id="jobs" class="filter">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2><span class="purple">CAM KẾT</span> KHÓA HỌC</h2>
                <p class="p-heading p-large">
                    Sau khi học xong khóa học học sinh sẽ tự xây dựng website bất động sản theo ý mình, không trùng lặp, tự lên kế hoạch chạy quảng cáo, seo, check seo cho website.
                </p>
                <p class="p-heading p-large">
                    <a data-toggle="modal" data-target="#payCourse" class="btn btn-warning">
                        MUA KHÓA HỌC
                    </a>
                </p>

                @include('site.popup.pay_course', ['product' => $product] )
            </div> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div> <!-- end of filter -->
<!-- end of jobs panel -->

<!-- Contact -->
<div id="contact" class="form-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center"><span class="purple-light">LIÊN HỆ NGAY VỚI</span> CHÚNG TÔI</h2>
                <ul class="list-unstyled v-space-lg">
                    <li class="address"><i class="fas fa-map-marker-alt"></i>
                        {{ isset($information['dia-chi']) ?$information['dia-chi'] : '' }}
                    </li>
                    <li><i class="fas fa-phone"></i>{{ isset($information['hot-line']) ?$information['hot-line'] : '' }}</li>
                    <li><i class="fas fa-envelope"></i><a class="underline" href="{{ isset($information['email']) ?$information['email'] : '' }}">{{ isset($information['email']) ?$information['email'] : '' }}</a>
                    </li>
                    <li><i class="fab fa-chrome"></i><a class="underline" href="https://phonghoctructuyen.com">phonghoctructuyen.com</a></li>
                </ul>
            </div> <!-- end of col -->
        </div> <!-- end of row -->
        <div class="row">
            <div class="col-lg-12">

                <!-- Contact Form -->
                <form action="" method="post" id="ContactForm" data-toggle="validator" data-focus="false" onSubmit="return contact(this);">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <input type="text" class="form-control-input" id="cname" name="name" required>
                        <label class="label-control" for="cname">Họ và tên</label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control-input" id="cemail" name="email" required>
                        <label class="label-control" for="cemail">Email</label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control-input" id="cemail" name="phone" required>
                        <label class="label-control" for="cemail">Điện thoại</label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control-input" id="cemail" name="address" required>
                        <label class="label-control" for="cemail">Địa chỉ</label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <input type="hidden" class="form-control-input" id="gjob" name="message" value="{{ $product->title }}" >
                    <input type="hidden" class="form-control-input" id="gjob" name="is_ajax" value="1" >
                    <div class="form-group">
                        <button type="submit" class="form-control-submit-button">Liên hệ</button>
                    </div>
                </form>
                <!-- end of contact form -->

            </div> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div> <!-- end of form-2 -->
<!-- end of contact -->


<!-- Scripts -->
<script src="{{ asset('templateCourse/js/jquery.min.js') }}"></script>
<!-- jQuery for Bootstrap's JavaScript plugins -->
<script src="{{ asset('templateCourse/js/popper.min.js') }}"></script> <!-- Popper tooltip library for Bootstrap -->
<script src="{{ asset('templateCourse/js/bootstrap.min.js') }}"></script> <!-- Bootstrap framework -->
<script src="{{ asset('templateCourse/js/jquery.easing.min.js') }}"></script>
<!-- jQuery Easing for smooth scrolling between anchors -->
<script src="{{ asset('templateCourse/js/swiper.min.js') }}"></script> <!-- Swiper for image and text sliders -->
<script src="{{ asset('templateCourse/js/jquery.magnific-popup.js') }}"></script> <!-- Magnific Popup for lightboxes -->
<script src="{{ asset('templateCourse/js/isotope.pkgd.min.js') }}"></script> <!-- Isotope for filter -->
<script src="{{ asset('templateCourse/js/jquery.countTo.js') }}"></script>
<!-- jQuery countTo for counting animation -->
<script src="{{ asset('templateCourse/js/morphext.min.js') }}"></script> <!-- Morphtext rotating text in the header-->
<script src="{{ asset('templateCourse/js/validator.min.js') }}"></script>
<!-- Validator.js - Bootstrap plugin that validates forms -->
<script src="{{ asset('templateCourse/js/scripts.js') }}"></script> <!-- Custom scripts -->
<script>
    function contact(e) {
        var data = $(e).serialize();
        var $btn = $(e).find('button').attr('disabled', 'disabled');

        $.ajax({
            type: "POST",
            url: '{!! route('sub_contact') !!}',
            data: data,
            success: function(result){
               alert('Cảm ơn bạn đã đăng ký khóa học, chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.');
                $btn.removeAttr('disabled');
            },
            error: function(error) {
                //alert('Lỗi gì đó đã xảy ra!')
            }

        });


        return false;
    }
</script>
</body>
</html>