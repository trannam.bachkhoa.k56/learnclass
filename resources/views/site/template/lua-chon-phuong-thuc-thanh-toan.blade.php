{{--@extends('site.layout.site')--}}

{{--@section('title', 'Lựa chọn phương thức thanh toán')--}}
{{--@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')--}}
{{--@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')--}}

{{--@section('content')--}}
    {{--<section class="resTeacher Payteacher bgxamnhat pdbottom30">--}}
        {{--<div class="container">--}}
            {{--<div class="row justify-content-lg-center">--}}
                {{--<div class="col-12">--}}
                    {{--<h1 class="clblack f24 pd-30 text-ct">--}}
                        {{--Thanh toán--}}
                    {{--</h1>--}}
                {{--</div>--}}
				{{--<div class="col-md-3">--}}
					{{--<ul class="stepProcess mgbottom20 boxShadow">--}}
						{{--<li class="active"><a>--}}
							{{--<div class="number">1</div>--}}
							{{--<b>Bước 1</b>--}}
							{{--<span>Lựa chọn hình thức thanh toán</span>--}}
						{{--</a></li>--}}
						{{--<li><a>--}}
							{{--<div class="number">2</div>--}}
							{{--<b>Bước 2</b>--}}
							{{--<span>Đặt mua và hoàn tất</span>--}}
						{{--</a></li>--}}
					{{--</ul>--}}
				{{--</div>--}}
                {{--<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">--}}
					{{--<div class="boxShadow bgwhite pd20">--}}
						{{--<h2 class="f16 text-up clorang activepay clorang clhr-orang text-b">Bước 1 : phương thức thanh toán</h2>--}}
						{{--<div class="content payment">--}}
							{{--<p class="pd-10 mgbottom0 clgrey">Chọn hình thức thanh toán phù hợp với bạn.</p>--}}
							{{--<div class="itempay mgbottom15">--}}
								{{--<a href="/trang/thanh-toan-truc-tiep" class="f16 ds-block">--}}
									{{--<span class="bgorang ds-inline f30 icon">--}}
										{{--<i class="fas fa-shipping-fast clwhite"></i>--}}
									{{--</span>--}}
									{{--<span class="contenpay clblack bgwhite ds-inline pd20 vertop ds-inline w45 lgw80 mbw100  br-bottom br2 brcl-orang clhr-orang">Giao thẻ học tận nơi và thu tiền<span class="clwhite bgorang text-rt pd-2 pd-04 ds-inline">Đề xuất</span>--}}
							{{--</span>--}}
								{{--</a>--}}
							{{--</div>--}}
							{{--<div class="itempay mgbottom15">--}}
								{{--<a href="/trang/thanh-toan-qua-chuyen-khoan" class="f16 ds-block">--}}
									{{--<span class="bgorang ds-inline f30 icon">--}}
										{{--<i class="fas fa-map-signs clwhite"></i>--}}
									{{--</span>--}}
									{{--<span class="contenpay clblack bgwhite ds-inline pd20 vertop ds-inline w45 lgw80  mbw100 br-bottom br2 brcl-orang clhr-orang">Chuyển khoản qua ngân hàng	<span class="clwhite bgorang text-rt pd-2 pd-04 ds-inline">Đề xuất</span>--}}
									{{--</span>--}}
								{{--</a>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
{{--@endsection--}}


@extends('site.layout.site')

@section('title','Thông tin tài khoản')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
 	@php
    $user = \Illuminate\Support\Facades\Auth::user();
    $teacher = \App\Entity\Teacher::detailTeacher($user->id);
    @endphp
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20">Người dùng {{ $user->name }}</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Người dùng {{ $user->name }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="teacher bggray pdbottom30">
        <div class="container">
			@include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-9 col-sm-12 col-12 sidebarContact">
                    @include('site.module_learnclass.menu_user')
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-12 recharge bgwhite noPadding">
                	<div class="row title">
                		<div class="col-12 contentTop text-ct">
							 <h2 class="f18 text-up clwhite mgbottom0 pd-10 text-b700 bgxanhdam">nạp tiền cho tài khoản  <span class="text-up">"{{ $user->name }}"</span></h2>
						</div>
                	</div>

                    <div class="PayscienceTop clblack text-lt mbpdleft0 mgbottom20 pd-020">

						<div class="row contentRightPay">

							<div class="col-md-12 contentUser ">
								<div class="text-lt pd-10 f14">
									<span class="mbpdleft5">Tài khoản :</span> <span class="clblue text-b700">Thường</span>
								</div>
							</div>
							<div class="borderpay col-12">

							</div>
							<div class="content text-ct  col-12">
								<h3 class="f22 text-up pd-20">Thuê Phòng học trực tuyến</h3>
							</div>
							<div class="col-lg-5 pdbottom30">
								<div class="content contentlt mbpd-015">
									<h4 class="f18 pbbottom10">
									Vì sao nên thuê PHÒNG HỌC TRỰC TUYẾN?
									</h4>
								<p class="f16">Bạn phải chi rất nhiều tiền để mua   <span class="text-b700">trang thiết bị dạy học</span>, chưa kể bạn phải lô chỗ để xe cho học sinh,.... khi thuê phòng học trực tuyến bạn được gì:</p>
									<ul>
										<li class="f16 pdbottom5"><span class="cricle ds-inline bgblack mgright5"></span>Bạn không phải tốn chi phí đi lại khi giảng dạy.</li>
										<li class="f16 pdbottom5"><span class="cricle ds-inline bgblack mgright5"></span>Chi phí mua trang thiết bị dạy học của bạn gần như không có.</li>
										<li class="f16 pdbottom5"><span class="cricle ds-inline bgblack mgright5"></span>Học sinh dễ dàng tham gia dạy và học mà không cần phụ huynh đưa đón.</li>
										<li class="f16 pdbottom5"><span class="cricle ds-inline bgblack mgright5"></span>Bạn vẫn có thể ở nhà bên vợ (chồng) con.</li>
									</ul>
								</div>

							</div>
							<div class="col-lg-7 pdbottom30">
								<div class="content contentrt mbpd-015">
									<h4 class="f18 pbbottom10">
									Vì sao thuê PHÒNG HỌC TRỰC TUYẾN?
									</h4>
								<p class="f16">Tham gia <span class="text-b700">Thuê Phòng </span>, bạn có thể giảng dạy online mà không phải đi bất cứ đâu. Bạn sẽ được hưởng thêm rất nhiều ưu đãi từ chúng tôi :</p>
									<ul class="list">
										<li class="pd-5 pdleft5 f16"><img class="pdright5" src="{{ asset('khoahoc/images/iconstar.png')}}">Được hỗ trợ tham gia dạy học từ cả học sinh và giáo viên.</li>
										<li class="pd-5 pdleft5 f16"><img class="pdright5" src="{{ asset('khoahoc/images/iconstar.png')}}">Không phải chịu thêm bất kỳ một chi phí, hay triết khấu nào.</li>
										<li class="pd-5 pdleft5 f16"><img class="pdright5" src="{{ asset('khoahoc/images/iconstar.png')}}">Dạy học cho học sinh trên toàn quốc mà không cần đi lại.</li>

									</ul>

								</div>
							</div>
							<div class="borderpay col-12">

							</div>
							<div class="content text-ct col-12">
								<h3 class="f22 text-up pd-20">Lựa chọn gói phù hợp</h3>
							</div>
							<div class="col-12 text-ct">
								<ul class="listTab ds-inline mgbottom20">
									<li class="ds-inline">
										<a  class=" f16 clblack pd-5 pd-030 ds-inline active text-b700 changeRent" data-value="3" onClick="return changeRent(this);">3 THÁNG</a>
									</li>
									<li class="ds-inline">
										<a class="f16 clblack pd-5 pd-030 ds-inline text-b700 changeRent" data-value="6" onClick="return changeRent(this);">6 THÁNG</a>
									</li>
									<li class="ds-inline">
										<a class="f16 clblack pd-5 pd-030 ds-inline  text-b700 changeRent" data-value="12" onClick="return changeRent(this);">12 THÁNG</a>
									</li>
								</ul>
							</div>
							<script>
								function changeRent(e) {
								    var val = $(e).attr('data-value');
									$('.changeRent').removeClass('active');

									$(e).addClass('active');
									var sum = val * 400000;
                                    $('.priceRent').html(numeral(sum).format('0,0'));

									$('.pricePayment').val(sum);
									$('.messagePayment').val('Nâng cấp tài khoản');
								}

								function selectUpdate(e) {
								    $('.button_opative').removeClass('active');
                                    $('.button_opative').attr('disabled', 'disabled');

								    $(e).addClass('active')
								}
							</script>
							<div class="">

							</div>
							<div class="col-lg-12">
								<div class="row listcontentTab mgtop30">
								<div class="contenttab col-lg-4 active mbmgbottom30">
									<div class="contentItem">
										<div class="titlehd text-ct">
								  			<h5 class="f16 pd-20 clwhite text-ca">Tài khoản thường</h5>
									  		<p class="f40 clwhite text-b700">Miễn phí</p>
									  		<p class="pdbottom20 mgbottom0"><span class="f14 clwhite">Đăng kí tài khoản</span></p>
								  		</div>
								  		<div class="list pd-15 pd-020">
								  			<ul class="pd0 pdbottom30">
									  			<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Hiển thị hồ sơ cá nhân công khai trên phonghoctructuyen.com</li>
									  			<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Được hỗ trợ dạy và học cho cả học sinh và giáo viên</li>
									  			<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Triết khấu <span class="text-b700">20% </span> cho học sinh tự giới thiệu</li>
									  			<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Triết khấu <span class="text-b700">40% </span> cho học sinh phòng học trực tuyến tự giới thiệu</li>
								  			</ul>
								  		</div>
									</div>
									<div class="button text-ct mgtop30 mgbottom15 text-ct ">
										<button class="text-up w100 f16 clwhite  pd-10  active button_opative" >đang sử dụng</button>
									</div>

								</div>
								<div class="contenttab col-lg-4 mbmgbottom30">
									<div class="contentItem">
							  		<div class="titlehd text-ct">
							  			<h5 class="f16 pd-20 clwhite text-ca">TÀI KHOẢN THUÊ PHÒNG HỌC</h5>
								  		<p class="f40 clwhite text-b700"><span class="priceRent">1,200,000</span> VNĐ</p>
								  		<p class="pdbottom20 mgbottom0"><span class="f14 clwhite pdbottom15">Thuê phòng học</span></p>
							  		</div>
							  		<div class="list pd-15 pd-020">
								  		<ul class="pd0 pdbottom30">
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Hiển thị hồ sơ cá nhân công khai trên phonghoctructuyen.com.</li>
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Được tạo lớp học.</li>
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Được hỗ trợ dạy và học cho cả học sinh và giáo viên.</li>
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Gửi mail cho học sinh thông báo.</li>
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Triết khấu <span class="text-b700">0% </span> khi học sinh đăng ký tham gia học.</li>
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>không giới hạn số giờ giảng dạy.</li>
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Được tạo nhiều khóa học trên phòng học trực tuyến.</li>

								  		</ul>
							  		</div>
							  		</div>
							  		<div class="button text-ct mgtop30 mgbottom15  text-ct ">
										<button class="text-up w100 f16 clwhite  pd-10 button_opative" onclick="return selectUpdate(this);">Nâng cấp tài khoản</button>
									</div>
								</div>
								<div class="contenttab col-lg-4 mbmgbottom30">
									<div class="maxbuy">
										<div class="postop bgxanhdam text-ct">
										<span class="clwhite f14 text-up ds-block pd-5">Mua nhiều nhất</span>
									</div>
									</div>
									<div class="contentItem">

							  		<div class="titlehd text-ct">
							  			<h5 class="f16 pd-20 clwhite text-ca">TÀI KHOẢN THUÊ PHÒNG HỌC</h5>
										<p class="f40 clwhite text-b700"><span class="priceRent">1,200,000</span> VNĐ</p>
								  		<p class="pdbottom20 mgbottom0"><span class="f14 clwhite pdbottom15">Thuê phòng học</span></p>
							  		</div>
							  		<div class="list pd-15 pd-020">
								  		<ul class="pd0 pdbottom30">
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Hiển thị hồ sơ cá nhân công khai trên phonghoctructuyen.com.</li>
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Được tạo lớp học.</li>
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Được hỗ trợ dạy và học cho cả học sinh và giáo viên.</li>
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Gửi mail cho học sinh thông báo.</li>
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Triết khấu 0% khi học sinh đăng ký tham gia học.</li>
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>không giới hạn số giờ giảng dạy.</li>
											<li class="pdbottom10"><i class="fa fa-check pdright5 clxanhdam" aria-hidden="true"></i>Được tạo nhiều khóa học trên phòng học trực tuyến.</li>

								  		</ul>
							  		</div>
							  	</div>
							  		<div class="button text-ct mgtop30 mgbottom15  text-ct ">
										<button class="text-up w100 f16 clwhite pd-10 button_opative" data-message="Nâng cấp tài khoản" onclick="return selectUpdate(this);">Nâng cấp tài khoản</button>
									</div>
								</div>
							</div>
							</div>

							<div class="col-12">

							</div>


						</div>


                    </div>
                    <div class="row justify-content-center pd-25 pd-020 bgpay">
						<div class="col-12 text-ct">
							<p class="f16 text-b700">Chọn hình thức thanh toán:</p>
						</div>
						<div class="col-xl-2 col-lg-3 col-md-6">
							<div class="item text-ct bgwhite">
								<img  class=" pd-10 pdtop15" src="{{ asset('khoahoc/images/1.png') }}" height="">
								<img  class=" pd-10 pdtop15" src="{{ asset('khoahoc/images/2.png') }}" height="">
								<p class="f14 pdbottom10 title">Giao thẻ học tận nơi và thu tiền</p>
								<form method="get" action="/trang/thanh-toan-truc-tiep">
									<input type="hidden" value="1200000" class="pricePayment" name="payment" />
									<input type="hidden" value="Nâng cấp tài khoản" class="messagePayment" name="message" />
									<button type="submit" class="f14 text-up pd-4 pd-015 mgbottom20 button">thanh toán</button>
								</form>
							</div>
						</div>
						<div class="col-xl-2 col-lg-3 col-md-6">
							<div class="item text-ct bgwhite">
								<img  class=" pd-10 pdtop15" src="{{ asset('khoahoc/images/3.png') }}" height="">
								<p class="f14 pdbottom10 title">Chuyển khoản ngân hàng</p>
								<form method="get" action="/trang/thanh-toan-qua-chuyen-khoan">
									<input type="hidden" value="1200000" class="pricePayment" name="payment" />
									<input type="hidden" value="Nâng cấp tài khoản" class="messagePayment" name="message" />
									<button type="submit" class="f14 text-up pd-4 pd-015 mgbottom20 button">thanh toán</button>
								</form>
							</div>
						</div>
					</div>



                    <div class="PayscienceBottom clblack text-lt mbpdleft0 mg-20 pd-020">
						<div class="col-lg-12">
							<div class="text-lt ds-inline">
								<p>Số dư xu hiện có:<span class="text-b700"> <font color="red">{{ $user->coint }} VNĐ</font></span> </p>
							</div>
							<div class="text-rt fl-rt ds-inline">
								<ul class="listright">
									<li class="ds-inline mgright30"><a href="" class="clblack"><img src="{{ asset('khoahoc/images/icon-shoping-cart.png')}}" class="pdright5">Lịch sử mua </a></li>
									<li class="ds-inline"><a href=""  class="clblack"><img src="{{ asset('khoahoc/images/icon-history.png')}}" class="pdright5">Lịch sử sử dụng</a></li>
								</ul>
							</div>
						</div>


						<div class="content text-ct  col-12 pd-20 mbmgtop25">
							<h3 class="f24 text-up">Sử dụng tài khoản của phòng học trực tuyến</h3>
						</div>

						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="itemsologan">
									<ul>
										<li><img src="{{ asset('khoahoc/images/icon-start.png') }}" class="pdright5">
										<span class="f16">Nâng cấp tài khoản</span></li>
									</ul>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="itemsologan">
									<ul>
										<li><img src="{{ asset('khoahoc/images/icon-start.png') }}" class="pdright5">
										<span class="f16">Tham gia khóa học</span></li>
									</ul>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div class="itemsologan">
									<ul>
										<li><img src="{{ asset('khoahoc/images/icon-start.png') }}" class="pdright5">
										<span class="f16">Đăng ký thuê phòng học</span></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-12 text-ct">
							<p class="pdbottom20 f14"></p>
						</div>

						<div class="border"></div>
						<div class="content text-ct  col-12 pd-20">
							<h3 class="f24 text-b700 text-up pdtop15 ">Chọn gói nạp tiền phù hợp</h3>
						</div>

						<div class="row mgtop30 selectCombo">
							<div class="col-xl-2 col-lg-0 col-md-0 col-sm-12 itemps"></div>
							<div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 itemps">
								<div class="itemcombo text-ct">
									<div class="top clwhite pd-20">
										<div class="number">
											<span class="f38 text-b700">500.000</span>
											<span>Xu</span>
										</div>
									<p class="price">500.000 VNĐ</p>
									</div>
									<div class="bottom">
									<p class="sale text-b700">Tiết kiệm 0%</p>
										<button class="pd-4 w80 text-up clwhite mgbottom20 f16 buttoncombo ds-inline button_opative buyXu"
												data-value="500000" data-message="Nạp xu" onclick="return buyXu(this);">chọn</button>
									</div>
								</div>
							</div>

							<div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 itemps">
								<div class="maxbuy">
									<div class="postop bgxanhdam text-ct">
										<span class="clwhite f14 text-up ds-block pd-5">Mua nhiều nhất</span>
									</div>
								</div>
								<div class="itemcombo text-ct">
									<div class="top clwhite pd-20">
										<div class="number">
											<span class="f38 text-b700">3.000.000</span>
											<span>Xu</span>
										</div>
									<p class="price">2.850.000 VNĐ</p>
									</div>
									<div class="bottom">
									<p class="sale text-b700">Tiết kiệm 5%</p>
										<button class="pd-4 w80 text-up clwhite mgbottom20 f16 buttoncombo ds-inline button_opative buyXu"
												data-value="2850000" data-message="Nạp xu" onclick="return buyXu(this);">chọn</button>
									</div>
								</div>
							</div>

							<div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 itemps">
								<div class="itemcombo text-ct">
									<div class="top clwhite pd-20">
										<div class="number">
											<span class="f38 text-b700">5.000.000</span>
											<span>Xu</span>
										</div>
									<p class="price">4.650.000 VNĐ</p>
									</div>
									<div class="bottom">
									<p class="sale text-b700">Tiết kiệm 7%</p>
										<button class="pd-4 w80 text-up clwhite mgbottom20 f16 buttoncombo ds-inline button_opative buyXu"
												data-value="4650000" data-message="Nạp xu" onclick="return buyXu(this);">chọn</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 f14 mg-15 text-ct">
							<p><i class="fa fa-info-circle pdright5" aria-hidden="true"></i>Xu không có hạn sử dụng. Vì vậy, càng mua nhiều bạn sẽ càng tiết kiệm được càng nhiều</p>

						</div>
						<script>
						function buyXu(e) {
							$('.buyXu').removeClass('active');
							$('.itemcombo').removeClass('active');
							$(e).addClass('active');
							$(e).parent().parent().addClass('active');
							var price = $(e).attr('data-value');
							var message = $(e).attr('data-message');

                            $('.pricePayment').val(price);
                            $('.messagePayment').val(message);
						}
						</script>
                    </div>
                     <div class="row justify-content-center pd-25 pd-020 bgpay">
						<div class="col-12 text-ct">
							<p class="f16 text-b700">Chọn hình thức thanh toán:</p>

						</div>
						 <div class="col-xl-2 col-lg-3 col-md-6">
							 <div class="item text-ct bgwhite">
								 <img  class=" pd-10 pdtop15" src="{{ asset('khoahoc/images/1.png') }}" height="">
								 <img  class=" pd-10 pdtop15" src="{{ asset('khoahoc/images/2.png') }}" height="">
								 <p class="f14 pdbottom10 title">Giao thẻ học tận nơi và thu tiền</p>
								 <form method="get" action="/trang/thanh-toan-truc-tiep">
									 <input type="hidden" value="500000" class="pricePayment" name="payment" />
									 <input type="hidden" value="" class="messagePayment" name="message" />
									 <button type="submit" class="f14 text-up pd-4 pd-015 mgbottom20 button">thanh toán</button>
								 </form>
							 </div>
						 </div>
						 <div class="col-xl-2 col-lg-3 col-md-6">
							 <div class="item text-ct bgwhite">
								 <img  class=" pd-10 pdtop15" src="{{ asset('khoahoc/images/3.png') }}" height="">
								 <p class="f14 pdbottom10 title">Chuyển khoản ngân hàng</p>
								 <form method="get" action="/trang/thanh-toan-qua-chuyen-khoan">
									 <input type="hidden" value="500000" class="pricePayment" name="payment" />
									 <input type="hidden" value="" class="messagePayment" name="message" />
									 <button type="submit" class="f14 text-up pd-4 pd-015 mgbottom20 button">thanh toán</button>
								 </form>
							 </div>
						 </div>
					</div>

                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
    	$(document).ready(function(){
		    $(".button_opative").click(function(){
		        $('.bgpay').css('opacity','1');
		    });

			$('.titlehd').matchHeight();
			$('.list').matchHeight();
			$('.itemps').matchHeight();
		});
    </script>

@endsection


