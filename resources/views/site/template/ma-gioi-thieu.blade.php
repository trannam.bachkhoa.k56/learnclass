@extends('site.layout.site')

@section('title', "Mã giảm giá")
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <?php
        $user = \Illuminate\Support\Facades\Auth::user();
    ?>
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20">Người dùng {{ $user->name }}</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Người dùng {{ $user->name }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="teacher bggray pdbottom30">
        <div class="container">
			@include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-12 sidebarContact">
					@include('site.module_learnclass.menu_user')
				</div>
                <div class="col-lg-9 col-md-8 col-sm-12 col-12 ">
                    <div class="teacherRight clblack text-lt mbpdleft0 mgtop20 boxShadow pd20 bgwhite f16">
                        <h2 class="f26 text-ct">Mã giới thiệu</h2>
                            <div class="clblack f24 pd-30 text-ct">
                                Mã giới thiệu áp dụng cho tài khoản đăng ký mới. Mã giới thiệu dùng để sử dụng giúp nâng cao thu nhập của người dùng. Chính sách mã giới thiệu như sau:
                            </div>
                            <p>- Đại lý cấp 1: 5% giá trị khóa học. Áp dụng cho tài khoản đăng ký mới. Đại lý sẽ được hưởng 5% cho tất cả các khóa học mà thành viên sử dụng mã này đăng ký. Mã được gắn mãi mãi, không tách rời. Đại lý sẽ có thu nhập thụ động 5% trọn đời khi học sinh sử dụng mã này <br><br>
							
							- Đại lý cấp 2: 1% giá trị khóa học áp dụng trọn đời cho tài khoản đăng ký mới. Chính sách tương tự như đại lý cấp 1 <br><br>
							
							- Không đăng ký đại lý: Mã giới thiệu sẽ được bảo lưu đến khi khách hàng có nhu cầu làm đại lý cho Phòng học trực tuyến. Mã giới thiệu được gắn trọn đời, không thể thay đổi</p>
							<p>Để có thể đăng ký làm đại lý chính thức, cũng như nắm bắt các chính sách cụ thể để tham gia làm đại lý cho phonghoctructuyen.com. Khách hàng vui lòng click vào <a href="/trang/lien-he">Liên hệ</a> để có thể gửi mail đến cho chúng tôi để được xét duyệt sớm nhất.</p>

                            @if (\Illuminate\Support\Facades\Auth::check())
                                @if (empty($user->code))
                                    <form class="bgwhite pd15 mg-30" style="border: 1px solid #ddd; box-shadow: 0 1px 1px rgba(0, 0, 0, .05);" action="{{ route('input_code_user') }}" method="post" enctype="multipart/form-data">
                                        {!! csrf_field() !!}
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Mã giới thiệu</span><span class="clred pd-05"></span></label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" value="" name="code" />
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10 pdtop30">
                                                <button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite ">Lưu mã giới thiệu</button>

                                            </div>
                                        </div>
                                    </form>
                                    @else
                                    <p> Bạn đang đã sử dụng mã code là: <b class="clred f18">{{ $user->code }}</b>.</p>
                                    @endif
                                @else
                                <p> Bạn chưa đăng nhập nên không thể sử dụng chức năng này.</p>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection