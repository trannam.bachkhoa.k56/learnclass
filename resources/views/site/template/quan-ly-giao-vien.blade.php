@extends('site.layout.site')

@section('title', 'Quản lý giáo viên')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    {{--//thu vien data table--}}
    <link rel="stylesheet" href="{{ asset('adminstration/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">


    <script src="{{ asset('adminstration/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminstration/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <?php
    $user = \Illuminate\Support\Facades\Auth::user();
    ?>
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20">Người dùng {{ $user->name }}</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Người dùng {{ $user->name }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="teacher bggray pdbottom30">
        <div class="container">
			@include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-5 col-sm-12 col-12 sidebarContact">
                    @include('site.module_learnclass.menu_user')
                </div>
                <div class="col-lg-9 col-md-7 col-sm-12 col-12 ">
                    <div class="teacherRight clblack text-lt">
                        <h2 class="text-ca f26 pdbottom15">Quản lý giáo viên</h2>
                      
						<div class="row management_Teacher ">
							<div class="col-lg-12 col-md-12"> 
                                <a href="/trang/them-moi-giao-vien" class="ds-inline f15 clwhite bgorang text-up text-b700 pd-4 pd-08 mgright5 mbds-block mbmg-10 mbpd-10 hvr-shutter-out-vertical titleTeacher">
                           Thêm mới giáo viên
                        </a>
                                <table class="table table-hover" id="manergerTeacher">
                                  <thead>
                                    <tr>
                                      <th scope="col">STT</th>
                                      <th scope="col">Họ và tên</th>
                                      <th scope="col">Ảnh đại diện</th>
                                      <th scope="col">Hình thức giảng dạy</th>
                                      <th scope="col">Chuyên ngành</th>
                                        <th scope="col">Trạng thái</th>
                                       <th scope="col">Thao tác</th>
                                    </tr>
                                  </thead>
                                  {{--<tbody>--}}
                                    {{--<tr>--}}
                                      {{--<th scope="row">1</th>--}}
                                      {{--<td>Nguyễn Xuân Thắng</td>--}}
                                       {{--<td><img src="http://phonghoc.local/upload/avarta1.jpg"></td>--}}
                                      {{--<td>Giáo Viên</td>--}}
                                      {{--<td>Công nghệ thông tin</td>--}}
                                      {{--<td><a href="" title="Sửa"><i class="far fa-edit"></i></a>--}}
                                        {{--<a href="" title="Xóa"><i class="far fa-trash-alt"></i></a></td>--}}

                                    {{--</tr>--}}
                                     {{--<tr>--}}
                                      {{--<th scope="row">1</th>--}}
                                      {{--<td>Nguyễn Xuân Thắng</td>--}}
                                       {{--<td><img src="http://phonghoc.local/upload/avarta1.jpg"></td>--}}
                                      {{--<td>Giáo Viên</td>--}}
                                      {{--<td>Công nghệ thông tin</td>--}}
                                      {{--<td><a href="" title="Sửa"><i class="far fa-edit"></i></a>--}}
                                        {{--<a href="" title="Xóa"><i class="far fa-trash-alt"></i></a></td>--}}

                                    {{--</tr>--}}
                                     {{--<tr>--}}
                                      {{--<th scope="row">1</th>--}}
                                      {{--<td>Nguyễn Xuân Thắng</td>--}}
                                       {{--<td><img src="http://phonghoc.local/upload/avarta1.jpg"></td>--}}
                                      {{--<td>Giáo Viên</td>--}}
                                      {{--<td>Công nghệ thông tin</td>--}}
                                      {{--<td><a href="" title="Sửa"><i class="far fa-edit"></i></a>--}}
                                        {{--<a href="" title="Xóa"><i class="far fa-trash-alt"></i></a></td>--}}

                                    {{--</tr>--}}
                                     {{--<tr>--}}
                                      {{--<th scope="row">1</th>--}}
                                      {{--<td>Nguyễn Xuân Thắng</td>--}}
                                       {{--<td><img src="http://phonghoc.local/upload/avarta1.jpg"></td>--}}
                                      {{--<td>Giáo Viên</td>--}}
                                      {{--<td>Công nghệ thông tin</td>--}}
                                      {{--<td><a href="" title="Sửa"><i class="far fa-edit"></i></a>--}}
                                        {{--<a href="" title="Xóa"><i class="far fa-trash-alt"></i></a></td>--}}

                                    {{--</tr>--}}
                                  {{----}}
                                  {{--</tbody>--}}
                                </table>
                                @include('admin.partials.popup_delete')
                                @include('admin.partials.visiable')
                            </div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection

@push('scripts')
    <script>
        $(function() {
            $('#manergerTeacher').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('datatable_teacher_maneger') !!}',
                columns: [
                    { data: 'teacher_id', name: 'teacher.teacher_id' },
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                    { data: 'name', name: 'users.name' },
                    { data: 'phone', name: 'users.phone' },
                    { data: 'email', name: 'users.email' },
                    { data: 'image', name: 'users.image', orderable: false,
                        render: function ( data, type, row, meta ) {
                            return '<div class=""><img src="/'+data+'" width="50" /></div>';
                        },
                        searchable: false  },
                    { data: 'is_approve', name: 'teacher.is_approve',
                        render: function ( data, type, row, meta ) {
                            if (data == 1) {
                                return 'Phê duyệt';
                            } else {
                                return 'Chưa phê duyệt';
                            }
                        }
                    }

                ]
            });
        });
    </script>
@endpush
