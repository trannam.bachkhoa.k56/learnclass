
@extends('site.layout.site')

@section('title', 'Khóa học đã đăng ký')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <?php
    $user = \Illuminate\Support\Facades\Auth::user();
    ?>
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20">Người dùng {{ $user->name }}</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Người dùng {{ $user->name }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="teacher bggray pdbottom30">
        <div class="container">
			@include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-5 col-sm-12 col-12 sidebarContact">
                    @include('site.module_learnclass.menu_user')
                </div>
                <div class="col-lg-9 col-md-7 col-sm-12 col-12 ">
                    <div class="teacherRight clblack text-lt">
                        <h2 class="text-ca f26 pdbottom15">Quản lí học sinh</h2>
                      
						<div class="row management_Teacher ">
							<div class="col-lg-12 col-md-12"> 
                                <a href="/trang/them-moi-hoc-sinh" class="ds-inline f15 clwhite bgorang text-up text-b700 pd-4 pd-08 mgright5 mbds-block mbmg-10 mbpd-10 hvr-shutter-out-vertical titleTeacher">
                           Thêm mới học sinh
                        </a>
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th scope="col">STT</th>
                                      <th scope="col">Họ và tên</th>
                                      <th scope="col">Ảnh đại diện</th>
                                      <th scope="col">Hình thức giảng dạy</th>
                                      <th scope="col">Chuyên ngành</th>
                                       <th scope="col">Thao tác</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <th scope="row">1</th>
                                      <td>Nguyễn Xuân Thắng</td>
                                       <td><img src="http://phonghoc.local/upload/avarta1.jpg"></td>
                                      <td>Giáo Viên</td>
                                      <td>Công nghệ thông tin</td>
                                      <td><a href="" title="Sửa"><i class="far fa-edit"></i></a>
                                        <a href="" title="Xóa"><i class="far fa-trash-alt"></i></a></td>

                                    </tr>
                                     <tr>
                                      <th scope="row">1</th>
                                      <td>Nguyễn Xuân Thắng</td>
                                       <td><img src="http://phonghoc.local/upload/avarta1.jpg"></td>
                                      <td>Giáo Viên</td>
                                      <td>Công nghệ thông tin</td>
                                      <td><a href="" title="Sửa"><i class="far fa-edit"></i></a>
                                        <a href="" title="Xóa"><i class="far fa-trash-alt"></i></a></td>

                                    </tr>
                                     <tr>
                                      <th scope="row">1</th>
                                      <td>Nguyễn Xuân Thắng</td>
                                       <td><img src="http://phonghoc.local/upload/avarta1.jpg"></td>
                                      <td>Giáo Viên</td>
                                      <td>Công nghệ thông tin</td>
                                      <td><a href="" title="Sửa"><i class="far fa-edit"></i></a>
                                        <a href="" title="Xóa"><i class="far fa-trash-alt"></i></a></td>

                                    </tr>
                                     <tr>
                                      <th scope="row">1</th>
                                      <td>Nguyễn Xuân Thắng</td>
                                       <td><img src="http://phonghoc.local/upload/avarta1.jpg"></td>
                                      <td>Giáo Viên</td>
                                      <td>Công nghệ thông tin</td>
                                      <td><a href="" title="Sửa"><i class="far fa-edit"></i></a>
                                        <a href="" title="Xóa"><i class="far fa-trash-alt"></i></a></td>

                                    </tr>
                                  
                                  </tbody>
                                </table>
                            </div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
