@extends('site.layout.site')

@section('title', "Quên mật khẩu")
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <section class="teacher mg-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 ">
                    <div class="teacherRight clblack text-lt pdleft10 mbpdleft0 mgtop20">
                        <h2 class="f26">Quên mật khẩu</h2>
                        <form class="bgwhite pd15 mg-30" style="border: 1px solid #ddd; box-shadow: 0 1px 1px rgba(0, 0, 0, .05);" action="{{ route('forget_password') }}" method="post" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Nhập email</span><span class="clred pd-05"></span></label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" value="" name="email" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10 pdtop30">
                                    <button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite ">Lấy lại mật khẩu</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection