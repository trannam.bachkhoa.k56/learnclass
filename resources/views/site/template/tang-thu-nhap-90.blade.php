@extends('site.layout.site')

@section('title', "Tăng đến 90% thu nhập ngay trong tháng đầu tiên")
@section('meta_description', "Hãy để Phòng học trực tuyến giúp bạn tăng thêm thu nhập với công việc dạy học")
@section('keywords', "tăng thu nhập, tăng thu nhập cho giáo viên")

<!-- SLIDER -->
@section('content')
<section class="top90 mbpd-20">
    <div class="container">
        <div class="row">
            <div class="col-12 text-ct textTop">
                <h1 class="f28 clblack text-b text-up slab">Tham gia làm giáo viên tại phòng học trực tuyến <br>
                    để tăng đến 90% thu nhập</h1>
                <p class="text-up clblue f22 vertop pdbottom30 ds-inline mgtop20 mbf18 slab">
                    ĐĂNG KÝ NGAY CHÚNG TÔI
                    <img class="mdds-none" src="{{ asset('images/muiten.png') }}" alt="">
                  
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                <div class="img  text-ct">
                    <img src="{{ !empty($information['banner-tang-thu-nhap-1']) ?  asset($information['banner-tang-thu-nhap-1']) : 'asset(images/bntang1.png)' }}" alt="">
                </div>
            </div>
            <div class="col-lg-5 col-md-12 col-sm-12 col-12 mgrgiht20 mgtop20 ">
                <form action="{{ route('sub_contact') }}" method="post" accept-charset="utf-8" class="pd-20 pd-025 clwhite w80 mgleft30 mdmgleft0 mdw100 pd-00 mbmgleft0">
					{!! csrf_field() !!}
                    <h3 class="f18 clwhite text-ct text-up slab">Đăng ký giáo viên trực tuyến</h3>
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="text-b700">Họ và tên : </label>
                        <input type="text" class="form-control" name="name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Họ và tên" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="text-b700">Điện thoại : </label>
                        <input type="number" class="form-control" name="phone"  id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Số điện thoại" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="text-b700">Email : </label>
                        <input type="email" class="form-control" name="email"  id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" required>
                    </div>
					<input type="hidden" value="Đăng ký trở thành giáo viên trực tuyến" name="message" />
                    <div class="form-group text-rt">
                        <button type="submit" class="btn  f18 bg-orang pd-5 pd-010 text-up ">Đăng kí</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
    </div>
</section>

<section class="res90 top90index mgtop30">
    <div class="container">
        <div class="row">
            <div class="col-12 text-ct">
                <h2 class="f28 clblack text-b text-up pdbottom30 mbf20 slab">AI CÓ THỂ ĐĂNG KÝ LÀM GIÁO VIÊN, GIA SƯ ? </h2>
            </div>
            <div class="row">
				<div class="col-lg-6 contenttop90 pd-20 mbpd-015 mbmg-015" style="color: #303030">
                    <div class="f22 text-js mbf18 ">  
                        {!! isset($post->content) ? $post->content : '' !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="img text-ct pdleft30 mdpdleft0">
                        <img src="{{ !empty($information['banner-tang-thu-nhap-2']) ?  asset($information['banner-tang-thu-nhap-2']) : 'asset(images/bntang2.png)' }}" alt="" class="">

                    </div>
                </div>
                
            </div>
		 </div>
    </div>
</section>
<section class="res90 top90index">
    <div class="container">
		<div class="row pdbottom30 mbpd-020">
			 @foreach(\App\Entity\SubPost::showSubPost('sologan-tang-thu-nhap',6) as  $id=> $sologantop)
			<div class="col-lg-5  offset-lg-1 col-md-9  offset-md-3 col-sm-12 col-12">
				<div class="itemtop90 pd-15">
					<i class="fas fa-check f38 mgright10 clblue"></i>
					<span class="text-up f16 text-b">{{ isset($sologantop['title']) ?$sologantop['title'] : ''}} </span>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>

<section class="bannertop90 res90">
    <div class="container">
        <div class="row">
            <div class="col-lg-12  text-ct">
                <h2 class="f28 clblack text-b text-up pd-20 mbf20 slab">Hợp tác với phòng học trực tuyến như thế nào ? </h2>
                <p class="f16 pdbottom30 clhometop mgbottom0">Hãy để lại email hoặc số điện thoại cho chúng tôi Để nhận được tài liệu giảng dạy,<br> cách thức làm việc
                    tại phòng học trực tuyến</p>
                <div class="w50 sendres pdbottom20 mbw100">
                    <form class="w100" onsubmit="return subcribeEmailSubmit(this)">
                        {{ csrf_field() }}
                        <div class="input ds-inline w70 mbw60">
                            <input type="text" name="" value="" placeholder="Email hoặc số điện thoại" class="emailSubmit w100 ds-block pdleft30 bgyellow">
                            <i class="fas fa-phone"></i>
                        </div>
                        <div class="button ds-inline w25 mbw35">
                            <button type="submit" class="clwhite f18 pd-05 pd-010 text-up bgblue w90 text-b700 mdf16 mbf16">Đăng kí</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="top90index">
    <div class="container">
        <div class="row">
            <div class="col-12 text-ct">
                <h2 class="f28 clblack text-b text-up pdbottom20 mbf20 slab">Các câu hỏi thường gặp </h2>
            </div>
            <div class="row ">
                 @foreach(\App\Entity\SubPost::showSubPost('cau-hoi-tang-thu-nhap',6) as  $id=> $questiontop)
                <div class="col-lg-5 offset-lg-1">
                    <div class="itemquest pd-20 mbpd-010">
                        <h3 class="pd-10"><i class="fas fa-question-circle clhometop f38 mgright10"></i>
                        <span class="f16 text-up"> {{ isset($questiontop['title']) ? $questiontop['title'] : ''}}</span>
                        </h3>
                        <p class="text-js f15" style="color:#2e2e2e;">{!! isset($questiontop['mo-ta']) ?$questiontop['mo-ta'] : '' !!}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- index -->
 @include('site.partials.mycustom') 

<section class="footertop90" style="background:#f5f5f5;padding:50px 0">
    <div class="container">
        <div class="row">
            <div class="col-12 text-ct pdtop20">
                <h2 class="f28 clblack text-b text-up pdbottom20 clblack mbf20 slab">Đăng kí ngay để nhận khóa học giảng dạy miễn phí</h2>
                <p class="f22 clhometop">Mở lớp tuyển sinh ngay để tăng đến 90% thu nhập </p>
                <a href=""  class="clwhite f24 bgred pd-7 pd-015 ds-inline text-b clhr-red bghr-white br mdf18">BẮT ĐẦU NGAY</a>
            </div>

        </div>
    </div>
</section>
@endsection