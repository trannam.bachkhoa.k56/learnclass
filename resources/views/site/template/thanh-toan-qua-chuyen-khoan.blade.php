@extends('site.layout.site')

@section('title', 'Thanh toán qua chuyển khoản')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
	<script src="{{ asset('adminstration/jquery.priceformat.js') }}"></script>
	@php
		if (\Illuminate\Support\Facades\Auth::check()) {
			$user = \Illuminate\Support\Facades\Auth::user();
			$teacher = \App\Entity\Teacher::detailTeacher($user->id);
		}
	@endphp
	<section class="breadcrumb ds-inherit pd">
		<div class="bgbread">
			<div class="container">
				<div class="row">
					<div class="col-12 pdtop15">
						<h1 class="mbf20">Thanh toán qua chuyển khoản ngân hàng</h1>
						<ul>
							<li><a href="">Trang chủ</a></li>
							<li>/</li>
							<li><a href="">Thanh toán qua chuyển khoản ngân hàng</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="teacher bggray pdbottom30 pdtop30">
		<div class="container">
			{{--@include('site.module_learnclass.profile_teacher_classroom')--}}
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-12 offset-2 recharge bgwhite noPadding">
					<div class="row title">
						<div class="col-12 contentTop text-ct">
							<h2 class="f18 text-up clwhite mgbottom0 pd-10 text-b700 bgxanhdam">THANH TOÁN QUA CHUYỂN KHOẢN NGÂN HÀNG </h2>
						</div>
					</div>

					<div class="PayscienceTop clblack text-lt mbpdleft0 mgbottom20 pd-020">
						<div class="pd20">
							<h2 class="f16 text-up clorang activepay clorang clhr-orang text-b">Cách thức thanh toán</h2>
							<div class="content mg-20">
								<div class="pd10 bgwhite clblack pdleft15">
									<p>Bạn có thể đến bất kỳ ngân hàng nào ở Việt Nam (hoặc sử dụng Internet Banking) để chuyển tiền theo thông tin bên dưới:</p>
									<table class="table">
										<thead>
											<th>Hình Ảnh</th>
											<th>Tên ngân hàng</th>
											<th>Số tài khoản</th>
											<th>Tên tài khoản</th>
											<th>Ngân hàng</th>
										</thead>
										@foreach (\App\Entity\OrderBank::showBank() as $bank)
										<tbody>
											<tr>
												<td><img src="{{ $bank->image }}" width="100" /></td>
												<td>{{ $bank->name_bank }}</td>
												<td>{{ $bank->number_bank }}</td>
												<td>{{ $bank->manager_account }}</td>
												<td>{{ $bank->branch }}</td>
											</tr>
										</tbody>
										@endforeach
									</table>
									<form action='https://www.2checkout.com/checkout/purchase' method='post'>
										<input type='hidden' name='sid' value='901406259' >
										<input type='hidden' name='mode' value='2CO' >
										<input type='hidden' name='li_0_type' value='product' >
										<input type='hidden' name='li_0_name' value='Example Product Name' >
										<input type='hidden' name='li_0_product_id' value='Example Product ID' >
										<input type='hidden' name='li_0__description' value='Example Product Description' >
										<input type='hidden' name='li_0_price' value='48.00' >
										<input type='hidden' name='li_0_quantity' value='2' >
										<input type='hidden' name='li_0_tangible' value='N' >
										<button type='submit' name='submit' class="btn btn-success" style="margin-left: 35%;margin-top: -8.5%;">
    										Thanh Toán Bằng ViSa
										</button>
									</form>
									<p>Nội dung chuyển khoản: <b>"PHTT {{ isset($user->id) ? 'ID '.$user->id : 'ten_ban so_dien_thoai' }} - {{ isset($_GET['message']) ? $_GET['message'] : ''  }} - (Mã quà tặng nếu có)".</b></p>
									<p><i>Chú ý: 1. Tiền sẽ được nạp vào tài khoản của quý khách sau tối đa 24h kể từ khi chúng tôi nhận được chuyển khoản.</i></p>
								</div>
							</div>
						</div>


					</div>


				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript">
        $(document).ready(function(){
            $(".button_opative").click(function(){
                $('.bgpay').css('opacity','1');
            });

            $('.titlehd').matchHeight();
            $('.list').matchHeight();
            $('.itemps').matchHeight();
        });
	</script>

@endsection