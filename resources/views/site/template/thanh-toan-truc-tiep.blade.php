@extends('site.layout.site')

@section('title', 'Thanh toán trực tiếp')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
	<script src="{{ asset('adminstration/jquery.priceformat.js') }}"></script>
	@php
		if (\Illuminate\Support\Facades\Auth::check()) {
			$user = \Illuminate\Support\Facades\Auth::user();
			$teacher = \App\Entity\Teacher::detailTeacher($user->id);
		}
	@endphp
	<section class="breadcrumb ds-inherit pd">
		<div class="bgbread">
			<div class="container">
				<div class="row">
					<div class="col-12 pdtop15">
						<h1 class="mbf20">Người dùng phòng học</h1>
						<ul>
							<li><a href="">Trang chủ</a></li>
							<li>/</li>
							<li><a href="">Nạp tiền phòng học</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="teacher bggray pdbottom30 pdtop30">
		<div class="container">
			{{--@include('site.module_learnclass.profile_teacher_classroom')--}}
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-12 offset-2 recharge bgwhite noPadding">
					<div class="row title">
						<div class="col-12 contentTop text-ct">
							<h2 class="f18 text-up clwhite mgbottom0 pd-10 text-b700 bgxanhdam">THÔNG TIN THANH TOÁN </h2>
						</div>
					</div>

					<div class="PayscienceTop clblack text-lt mbpdleft0 mgbottom20 pd-020">

						<div class="row contentRightPay">

						</div>

						{{--<div class="boxShadow bgwhite pd20 mgbottom20">--}}
							{{--<h2 class="f16 text-up clorang activepay clorang clhr-orang text-b">Hình thức thanh toán</h2>--}}
							{{--<div class="content mg-20">--}}
								{{--<div class="clblack">--}}
									{{--Bước 1: Nhập đầy đủ các thông tin của bạn vào mẫu trên <br>--}}
									{{--Bước 2: phonghoctructuyen.com sẽ gọi điện thoại để xác nhận đơn hàng. <br>--}}
									{{--Bước 3: Nhân viên của chúng tôi sẽ đến địa chỉ mà bạn đăng ký để nhận tiền (phí thu tiền: ~30.000) <br>--}}
									{{--Bước 4: Số tiền nộp sẽ được cộng vào tài khoản phonghoctructuyen.com của bạn. <br>--}}
									{{--Toàn bộ quy trình trên sẽ diễn ra tối đa trong vòng 24 giờ làm việc.--}}
								{{--</div>--}}
							{{--</div>--}}
						{{--</div>--}}

						<form class="bgwhite mg-30" action="{{ route('sub_payment') }}" method="post">
							<p>
								<div class="input-group">
									<label class="col-12 col-md-3 col-lg-2">Bạn muốn nạp số tiền:</label>
									<textarea name="message" class="form-control col-12 col-md-6">Bạn sẽ nạp {{ isset($_GET['payment']) ? $_GET['payment'] : ''  }} vnđ để thanh toán {{ isset($_GET['message']) ? $_GET['message'] : ''  }}.</textarea>
								</div>
							</p>

							<script>
                                $('.formatPrice').priceFormat({
                                    prefix: '',
                                    centsLimit: 0,
                                    thousandsSeparator: '.'
                                });
							</script>
							<div class="clearfix mgbottom20">
								<div class="orClass"><span>Thông tin vận chuyển</span></div>
							</div>

							@if (\Illuminate\Support\Facades\Auth::check() && \App\Entity\Payment::checkEmptyOfUser(\Illuminate\Support\Facades\Auth::id()) > 0)
								@foreach (\App\Entity\Payment::getPaymentOfUser(\Illuminate\Support\Facades\Auth::id())  as $id => $payment)
									@if ($id == 0)
									<div class="pd10 checkOut bgwhite boxShadow mgbottom20">
										<div class="checkbox small m-b-2">
											<div class="checkbox-overlay">
												<input type="radio" value="{{ $payment->payment_id }}" name="payment_old" checked/>
												<div class="checkbox-container">
													<div class="checkbox-checkmark"></div>
												</div>
												<label>Giao hàng tới</label>
											</div>
										</div>
										<div>
											Họ tên: {{ $payment->name }}<br>
											Địa chỉ: {{ $payment->address }}<br>
											Điện thoại: {{ $payment->phone }}<br>
											Email: {{ $payment->email }}
										</div>
									</div>
									@endif
								@endforeach

							<div class="clearfix mgbottom20">
								<p>
									<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
										Chỉnh sửa
									</button>
								</p>
								<div class="collapse" id="collapseExample">
									<div class="card card-body">
										<table class="table">
											<thead>
												<th></th>
												<th>Họ tên</th>
												<th>Địa chỉ</th>
												<th>Điện thoại</th>
												<th>Email</th>
											</thead>
											@if (\Illuminate\Support\Facades\Auth::check())
												@foreach (\App\Entity\Payment::getPaymentOfUser(\Illuminate\Support\Facades\Auth::id())  as $id => $payment)
													<tbody>
														<tr>
															<td>
																<div class="checkbox small m-b-2">
																	<div class="checkbox-overlay">
																		<input type="radio" value="{{ $payment->payment_id }}" name="payment_old" />
																		<div class="checkbox-container">
																			<div class="checkbox-checkmark"></div>
																		</div>
																	</div>
																</div>
															</td>
															<td>{{ $payment->name }}</td>
															<td>{{ $payment->address }}</td>
															<td>{{ $payment->phone }}</td>
															<td>{{ $payment->email }}</td>
														</tr>
													</tbody>
												@endforeach
											@endif
										</table>
										<div class="orClass"><span>hoặc</span></div>
										<div class="bgwhite clblack pd15 boxShadow">
											{!! csrf_field() !!}
											<div class="form-group row">
												<label for="staticEmail" class="col-sm-12 col-form-label text-inline"><span class="text-b700">Chú ý : </span><span class="clred pd-05">(*)</span> là những trường bắt buộc</label>
											</div>

											<div class="form-group row">
												<div class="form-group col-md-6">
													<label for="staticEmail" class=""><span class="text-b700">Tên riêng</span><span class="clred pd-05">(*)</span></label>
													<div class="">
														<input type="text" class="form-control f14" name="name" placeholder="Tên riêng" >
														<!-- <p class="f14 text-inline clred pd-5 pdleft10 mgbottom0">Vui lòng nhập tên riêng</p> -->
													</div>
												</div>

												<div class="form-group col-md-6">
													<label for="staticEmail" class=""><span class="text-b700">Hòm thư điện tử</span><span class="clred pd-05">(*)</span></label>
													<div class="">
														<input type="email" class="form-control f14"  name="email" placeholder="Email" >
														<!-- <p class="f14 text-inline clred pd-5 pdleft10 mgbottom0">Vui lòng nhập tên riêng</p> -->
													</div>
												</div>
											</div>
											<div class="form-group row">
												<div class="form-group col-md-6">
													<label for="staticEmail" class=""><span class="text-b700">Điện thoại</span><span class="clred pd-05">(*)</span></label>
													<div class="">
														<input type="number" class="form-control f14" name="phone"  placeholder="Số điện thoại" >
													</div>
												</div>
												<div class="form-group col-md-6">
													<label for="staticEmail" class=""><span class="text-b700">Địa chỉ</span><span class="clred pd-05">(*)</span></label>
													<div class="">
														<input type="text" class="form-control f14" name="address"  placeholder="Địa chỉ" >
													</div>
												</div>
												@if($errors->any())
													@foreach ($errors->all() as $error)
														<div class="alert alert-danger" role="alert">
															<strong>{{ $error }}</strong>
														</div>
													@endforeach
												@endif
											</div>


											<div class="form-group row">

												<div class="col-sm-12 pdtop10">
													<button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite ">Đặt mua</button>
												</div>

											</div>
										</div>
									</div>
								</div>

							</div>
								@else
								<div class="bgwhite clblack pd15 boxShadow">
									{!! csrf_field() !!}
									<div class="form-group row">
										<label for="staticEmail" class="col-sm-12 col-form-label text-inline"><span class="text-b700">Chú ý : </span><span class="clred pd-05">(*)</span> là những trường bắt buộc</label>
									</div>

									<div class="form-group row">
										<div class="form-group col-md-6">
											<label for="staticEmail" class=""><span class="text-b700">Tên riêng</span><span class="clred pd-05">(*)</span></label>
											<div class="">
												<input type="text" class="form-control f14" name="name" placeholder="Tên riêng" >
												<!-- <p class="f14 text-inline clred pd-5 pdleft10 mgbottom0">Vui lòng nhập tên riêng</p> -->
											</div>
										</div>

										<div class="form-group col-md-6">
											<label for="staticEmail" class=""><span class="text-b700">Hòm thư điện tử</span><span class="clred pd-05">(*)</span></label>
											<div class="">
												<input type="email" class="form-control f14"  name="email" placeholder="Email" >
												<!-- <p class="f14 text-inline clred pd-5 pdleft10 mgbottom0">Vui lòng nhập tên riêng</p> -->
											</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="form-group col-md-6">
											<label for="staticEmail" class=""><span class="text-b700">Điện thoại</span><span class="clred pd-05">(*)</span></label>
											<div class="">
												<input type="number" class="form-control f14" name="phone"  placeholder="Số điện thoại" >
											</div>
										</div>
										<div class="form-group col-md-6">
											<label for="staticEmail" class=""><span class="text-b700">Địa chỉ</span><span class="clred pd-05">(*)</span></label>
											<div class="">
												<input type="text" class="form-control f14" name="address"  placeholder="Địa chỉ" >
											</div>
										</div>
										@if($errors->any())
											@foreach ($errors->all() as $error)
												<div class="alert alert-danger" role="alert">
													<strong>{{ $error }}</strong>
												</div>
											@endforeach
										@endif
									</div>


									<div class="form-group row">

										<div class="col-sm-12 pdtop10">
											<button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite ">Đặt mua</button>
										</div>

									</div>
								</div>
							@endif
						</form>


					</div>


				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript">
        $(document).ready(function(){
            $(".button_opative").click(function(){
                $('.bgpay').css('opacity','1');
            });

            $('.titlehd').matchHeight();
            $('.list').matchHeight();
            $('.itemps').matchHeight();
        });
	</script>



@endsection
