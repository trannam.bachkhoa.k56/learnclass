@extends('site.layout.site')

@section('title', 'Khóa học đã đăng ký')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <?php
    $user = \Illuminate\Support\Facades\Auth::user();
    ?>
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1 class="mbf20">Người dùng {{ $user->name }}</h1>
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="">Người dùng {{ $user->name }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="teacher bggray pdbottom30">
        <div class="container">
			@include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-5 col-sm-12 col-12 sidebarContact">
                    @include('site.module_learnclass.menu_user')
                </div>
                <div class="col-lg-9 col-md-7 col-sm-12 col-12 ">
                    <div class="teacherRight clblack text-lt">
                        <h2 class="text-ca f26 pdbottom15">Thêm mới giáo viên</h2>
                      
						<div class="row management_Teacher ">
							<div class="col-lg-12 col-md-12"> 
                               <form class="bgwhite pd15 mgbottom30" style="border: 1px solid #ddd; box-shadow: 0 1px 1px rgba(0, 0, 0, .05);" action="{{ route('register_teacher') }}" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-12 col-form-label text-inline"><span class="text-b700">Chú ý : </span><span class="clred pd-05">(*)</span> là những trường bắt buộc</label>
                        </div>
                        <h3 class="clblack f24 pd-30 text-ct">
                            Thông tin cơ bản
                        </h3>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Họ và tên</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="name" placeholder="Tên riêng" value="{{ old('name') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Số điện thoại</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <input type="number" min="0" class="form-control f14" name="phone" placeholder="Số điện thoại" value="{{ old('phone') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Email</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <input type="email"  class="form-control f14" name="email" placeholder="Nhập vào email" value="{{ old('email') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Địa chỉ</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="address" placeholder="Địa chỉ" value="{{ old('address') }}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Facebook</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="facebook" placeholder="Facebook" value="{{ old('facebook') }}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Avatar</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <img id="blah"  src="{{ asset('khoahoc/images/avatar.png') }}" alt="">
                                <button class="btn btn-default addAvatar">TẢI LÊN AVATAR</button>
                                <input type='file' id="imgInp" name="image" onchange="readURL(this)" style="display: none"/>
                                <input type="hidden" value="" name="avatar" />
                                <script>
                                    function readURL(input) {
                                        if (input.files && input.files[0]) {
                                            var reader = new FileReader();

                                            reader.onload = function(e) {
                                                $('#blah').attr('src', e.target.result);
                                            }

                                            reader.readAsDataURL(input.files[0]);
                                        }
                                    }
                                    $('.addAvatar').click(function() {
                                        $('#imgInp').click();
                                        return false;
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Mô tả ngắn</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" rows="3">{{ old('description') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Mật khẩu</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="password" placeholder="" required>
                            </div>
                        </div>
                        <h3 class="clblack f24 pd-30 text-ct">
                            Thông tin giảng dạy
                        </h3>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Đơn vị công tác</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="where_working" placeholder="Đơn vị công tác" value="{{ old('where_working') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Lĩnh vực giảng dạy</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <select class="form-control" name="teaching_field">
                                    @foreach(\App\Entity\SubPost::showSubPost('linh-vuc-giang-day',1000) as  $id=> $field)
                                        <option value="{{ $field->title }}">{{ $field->title }}</option>
                                    @endforeach
                                </select>
                                {{--<input type="text" class="form-control f14" name="teaching_field" placeholder="Lĩnh vực giảng dạy" value="{{ old('teaching_field') }}" />--}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Trình độ học vấn</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="academic_standard" placeholder="Trình độ học vấn" value="{{ old('academic_standard') }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Hình thức giảng dạy</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="form_of_teaching" placeholder="Giáo viên, gia sư" value="{{ old('form_of_teaching') }}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Công việc hiện tại</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="current_job" placeholder="Công việc hiện tại: giáo viên, sinh viên,..." value="{{ old('current_job') }}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Môn giảng dạy</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="teaching_subject" placeholder="Môn giảng dạy chinh" value="{{ old('teaching_subject') }}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Tệp thông tin cá nhân</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <div class="input-group control-group increment" >
                                    <input type="file" name="filename[]" value="" class="form-control">
                                    <div class="input-group-btn">
                                        <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>  Thêm tệp</button>
                                    </div>
                                </div>
                                <div class="clone hide">
                                    <div class="control-group input-group" style="margin-top:10px">
                                        <input type="file" name="filename[]" value="" class="form-control">
                                        <div class="input-group-btn">
                                            <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Xóa tệp</button>
                                        </div>
                                    </div>
                                </div>

                                <script type="text/javascript">

                                    $(document).ready(function() {

                                        $(".btn-success").click(function(){
                                            var html = $(".clone").html();
                                            $(".increment").after(html);
                                        });

                                        $("body").on("click",".btn-danger",function(){
                                            $(this).parents(".control-group").remove();
                                        });

                                    });
                                </script>
                                <style>
                                    .hide {
                                        display: none;
                                    }
                                </style>
                            </div>
              <p><i>Vui lòng nhập tài liệu tiếng việt không có dấu và không chứa dấu (,).</i></p>
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 pdtop30">
                                <button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite btnloadding">Gửi duyệt</button>
                            </div>

                        </div>
                    </form>
                            </div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
