@extends('site.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <?php
    $user = \Illuminate\Support\Facades\Auth::user();
    $teacher = \App\Entity\Teacher::detailTeacher($user->id);
    ?>
    <section class="breadcrumb ds-inherit pd">
		<div class="bgbread">
			<div class="container">
				<div class="row">
					<div class="col-12 pdtop15">
						<h1>Giáo viên {{ $user->name }}</h1>
						<ul>
							<li><a href="">Trang chủ</a></li>
							<li>/</li>
							<li><a href="">Giáo viên {{ $user->name }}</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
    </section>

    <section class="teacher bggray pdbottom30">
        <div class="container">
			@include('site.module_learnclass.profile_teacher_classroom')
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-12 sidebarContact">
                    @include('site.module_learnclass.menu_user')
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-12 ">
                    <div class="teacherRight clblack text-lt pdleft10">
                        <h2 class="f26">Đăng ký thêm lớp học</h2>
                        <form class="bgwhite pd15 mg-30" style="border: 1px solid #ddd; box-shadow: 0 1px 1px rgba(0, 0, 0, .05);" action="{{ route('register_classroom') }}" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Tên lớp học</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control f14" name="name" placeholder="Tên lớp học" value="{{ old('name') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Ngày dự kiến bắt đầu học</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <input id="date" type="date" value="2018-08-08" name="started_date">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Giờ dự kiến bắt đầu học</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <input id="date" type="time" value="08:08" name="started_time">
                                <i>SA: sáng, CH: chiều</i>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Số buổi học</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <input type="number" min="0" class="form-control f14" name="number_lesson" placeholder="Số buổi học" value="{{ old('number_lesson') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Số học viên tối đa</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <input type="number" min="0" class="form-control f14" name="min_student" placeholder="Số học viên tối đa" value="{{ old('min_student') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Hình thức tuyển sinh</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <select name="recruitment" class="form-control">
                                    <option value="tuyen-dung">Tuyển sinh xong mới dạy</option>
                                    <option value="day">Dạy luôn</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Ảnh nền lớp học</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <img id="blah"  src="{{ asset('khoahoc/images/avatar.png') }}" alt="">
                                <button class="btn btn-default addAvatar">Tải ảnh lên</button>
                                <input type='file' id="imgInp" name="image" onchange="readURL(this)" style="display: none"/>
                                <input type="hidden" value="" name="avatar" />
                                <script>
                                    function readURL(input) {
                                        if (input.files && input.files[0]) {
                                            var reader = new FileReader();

                                            reader.onload = function(e) {
                                                $('#blah').attr('src', e.target.result);
                                            }

                                            reader.readAsDataURL(input.files[0]);
                                        }
                                    }
                                    $('.addAvatar').click(function() {
                                        $('#imgInp').click();
                                        return false;
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Chọn cấp học</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <select name="filter[]" class="form-control f14">
                                    @foreach(\App\Entity\FilterGroup::showFilterGroup() as $id => $filterGroup)
                                        @if ($filterGroup->group_name != 'Các Môn Học')
                                            <option  disabled> {{ $filterGroup->group_name }}</option>
                                            @foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
                                                <option value="{!! $filter->name_filter !!}">{!! $filter->name_filter !!}</option>
                                            @endforeach
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Chọn môn học</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <select name="filter[]" class="form-control f14">
                                    @foreach(\App\Entity\FilterGroup::showFilterGroup() as $id => $filterGroup)
                                        @if ($filterGroup->group_name == 'Các Môn Học')
                                            @foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
                                                <option value="{!! $filter->name_filter !!}">{!! $filter->name_filter !!}</option>
                                            @endforeach
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Mô tả lớp học</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" rows="3">{{ old('description') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Giá</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <input type="number" min="0" class="form-control f14" name="price" placeholder="Giá" value="{{ old('price') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Nội dung lớp học</span><span class="clred pd-05">(*)</span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control editor" id="contentClassroom" name="content" rows="3">{{ old('content') }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label"><span class="text-b700">Tài liệu học tập liên quan</span><span class="clred pd-05"></span></label>
                            <div class="col-sm-10">
                                <div class="input-group control-group increment" >
                                    <input type="file" name="filename[]" value="" class="form-control">
                                    <div class="input-group-btn">
                                        <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>  Thêm tệp</button>
                                    </div>
                                </div>
                                <div class="clone hide">
                                    <div class="control-group input-group" style="margin-top:10px">
                                        <input type="file" name="filename[]" value="" class="form-control">
                                        <div class="input-group-btn">
                                            <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Xóa tệp</button>
                                        </div>
                                    </div>
                                </div>

                                <script type="text/javascript">

                                    $(document).ready(function() {

                                        $(".btn-success").click(function(){
                                            var html = $(".clone").html();
                                            $(".increment").after(html);
                                        });

                                        $("body").on("click",".btn-danger",function(){
                                            $(this).parents(".control-group").remove();
                                        });

                                    });

                                </script>
                                <style>
                                    .hide {
                                        display: none;
                                    }
                                </style>
								<font color="red" class="mgtop10 text-b ds-block font16"><i>Vui lòng không nhập tài liệu có tên tiếng việt và không được chứa dấu (,).</i></font>
                            </div>
                            
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 pdtop30">
                                <button type="submit" class="ds-inline pd-7 pd-014 bgorang f16 br brcl-orang br-rs3 clhr-orang bghr-white  clwhite ">Đăng ký lớp học</button>

                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection