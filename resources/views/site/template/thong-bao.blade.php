@extends('site.layout.site')

@section('title','liên hệ' )
@section('meta_description',  '')
@section('keywords', '')

@section('content')

 <section class="breadcrumb ds-inherit pd">
         <div class="bgbread">
            <div class="container">
               <div class="row">
                  <div class="col-12 pdtop15">
                     <h1 class="mbf20">Thông báo</h1>
                     <ul>
                        <li><a href="./">Trang chủ</a></li>
                        <li>/</li>
                        <li><a href="#">Thông báo</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="categoryNew2 pd-30">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-12 col-12 pd-10 ">
              <div class="sidebarnew">
                <h2 class="f20 mgtop10 text-bnone pdtop10 pd-010 clorang mgborom20">Thông báo 
                </h2>
                <ul class="list">
                    @if(\App\Entity\Notification::showNotifyClass(0 || 1 || 2 ||3,'giao_vien'))
                	<li class="ds-block">
	                  	<a href="" title="" onclick="return seenNotification(this)" class="f16 ds-block pd-10 pd-010">Thông báo phòng học <span class="clred bgwhite f16 pd-06 ds-inline text-rt ">{{ \App\Entity\Notification::countNotifyDetail(0) }}</span> </a>
	                </li>
                    @elseif(\App\Entity\Notification::showNotifyClass(0 || 1 || 2 ||3,''))
	                <li class="ds-block">	
	                	<a href="" title="" onclick="return seenNotification(this)" class="f16 ds-block pd-10 pd-010">Thông báo khóa học <span class="clred bgwhite f16 pd-06 ds-inline text-rt ">{{ \App\Entity\Notification::countNotifyDetail(1) }}</span></a>
	                </li>
                    @endif
	                 <li class="ds-block">
	                  	<a href="" title="" onclick="return seenNotification(this)" class="f16 ds-block pd-10 pd-010">Thông báo nạp tiền <span class="clred bgwhite f16 pd-06 ds-inline text-rt ">{{ \App\Entity\Notification::countNotifyDetail(2) }}</span></a>
	                </li>
	                <li class="ds-block">
	                  	<a href="" title="" onclick="return seenNotification(this)" class="f16 ds-block pd-10 pd-010">Thông báo hệ thống <span class="clred bgwhite f16 pd-06 ds-inline text-rt ">{{ \App\Entity\Notification::countNotifyDetail(3) }}</span></a>
	                </li>
                </ul>
              </div>
            </div>
         
 
        <script>
          $(document).ready(function () {
              var element = $('.sidebarnew');
              var originalY = element.offset().top;
  			
              // Space between element and top of screen (when scrolling)
              var topMargin = 20;
              
              // Should probably be set in CSS; but here just for emphasis
              element.css('position', 'relative');
              
              $(window).on('scroll', function(event) {
                  var scrollTop = $(window).scrollTop();
                  var topContent = $('.contentRightNew2').height();
  				  var widthside = $( window ).width();
                  if (scrollTop < (topContent - 500) && widthside >= 500) {
  					
                      element.stop(false, false).animate({
                          top:
                              scrollTop < originalY ? 0 : scrollTop - originalY + topMargin
                      }, 300);
                  }
        			});
        		});
        </script>

            <div class="col-lg-9 col-md-8 col-sm-12 col-12">
                @if(\App\Entity\Notification::showNotifyClass(0,'giao_vien'))
              <div class="contentRightNew2">
                <h3 class="text-bnone f20 mg-20">Thông báo phòng học</h3>
                <div class="content">
                  <ul class="list">
                      @foreach(\App\Entity\Notification::showNotifyClass(0,'giao_vien') as $notify)
                    <li class="ds-block pd-010" @if($notify->status == 0 || $notify->status == 1) style="background: #edf2fa;" @else style="background: white;" @endif>
                        <span>{{ $notify->created_at }}</span>
                        <a href="{{ route('notificationClassroom', ['notifyId' => $notify->notify_id]) }}" title="" onclick="return readNotification(this)" class="ds-inline pdtop20 pdbottom20 pd-010">
                            <span class="ds-inline mg-015 clwhite pd-010 bggrey f13 mbmgleft0">{{ $notify->title }}</span>
                            {{ $notify->content }}
                        </a>
                    </li>
                      @endforeach
                  </ul>
                </div>
              </div>
                    @elseif(\App\Entity\Notification::showNotifyClass(1,''))
                        <div class="contentRightNew2">
                            <h3 class="text-bnone f20 mg-20">Thông báo khóa học</h3>
                            <div class="content">
                                <ul class="list">
                                    @foreach(\App\Entity\Notification::showNotifyClass(1,'') as $notify)
                                        <li class="ds-block pd-010" @if($notify->status == 0 || $notify->status == 1) style="background: #edf2fa;" @else style="background: white;" @endif>
                                            <span>{{ $notify->created_at }}</span>
                                            <a href="{{ route('notificationClassroom', ['notifyId' => $notify->notify_id]) }}" title="" onclick="return readNotification(this)" class="ds-inline pdtop20 pdbottom20 pd-010">
                                                <span class="ds-inline mg-015 clwhite pd-010 bggrey f13 mbmgleft0">{{ $notify->title }}</span>
                                           {{ $notify->content }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif

                    @if(\App\Entity\Notification::showNotifyClass(2,'giao_vien'))
                        <div class="contentRightNew2">
                            <h3 class="text-bnone f20 mg-20">Thông báo nạp tiền</h3>
                            <div class="content">
                                <ul class="list">
                                    @foreach(\App\Entity\Notification::showNotifyClass(2,'giao_vien') as $notify)
                                        <li class="ds-block pd-010" @if($notify->status == 0 || $notify->status == 1) style="background: #edf2fa;" @else style="background: white;" @endif>
                                            <span>{{ $notify->created_at }}</span>
                                            <a href="{{ route('notificationClassroom', ['notifyId' => $notify->notify_id]) }}" title="" onclick="return readNotification(this)" class="ds-inline pdtop20 pdbottom20 pd-010">
                                                <span class="ds-inline mg-015 clwhite pd-010 bggrey f13 mbmgleft0">{{ $notify->title }}</span>
                                 {{ $notify->content }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @elseif(\App\Entity\Notification::showNotifyClass(2,''))
                        <div class="contentRightNew2">
                            <h3 class="text-bnone f20 mg-20">Thông báo nạp tiền</h3>
                            <div class="content">
                                <ul class="list">
                                    @foreach(\App\Entity\Notification::showNotifyClass(2,'') as $notify)
                                        <li class="ds-block pd-010" @if($notify->status == 0 || $notify->status == 1) style="background: #edf2fa;" @else style="background: white;" @endif>
                                            <span>{{ $notify->created_at }}</span>
                                            <a href="{{ route('notificationClassroom', ['notifyId' => $notify->notify_id]) }}" title="" onclick="return readNotification(this)" class="ds-inline pdtop20 pdbottom20 pd-010">
                                                <span class="ds-inline mg-015 clwhite pd-010 bggrey f13 mbmgleft0">{{ $notify->title }}</span>
                                                {{ $notify->content }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif

                    @if(\App\Entity\Notification::showNotifyClass(3,'giao_vien'))
                        <div class="contentRightNew2">
                            <h3 class="text-bnone f20 mg-20">Thông báo hệ thống</h3>
                            <div class="content">
                                <ul class="list">
                                    @foreach(\App\Entity\Notification::showNotifyClass(3,'giao_vien') as $notify)
                                        <li class="ds-block pd-010" @if($notify->status == 0 || $notify->status == 1) style="background: #edf2fa;" @else style="background: white;" @endif>
                                            <span>{{ $notify->created_at }}</span>
                                            <a href="{{ route('notificationClassroom', ['notifyId' => $notify->notify_id]) }}" title="" onclick="return readNotification(this)" class="ds-inline pdtop20 pdbottom20 pd-010">
                                                <span class="ds-inline mg-015 clwhite pd-010 bggrey f13 mbmgleft0">{{ $notify->title }}</span>
                                           {{ $notify->content }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @elseif(\App\Entity\Notification::showNotifyClass(3,''))
                        <div class="contentRightNew2">
                            <h3 class="text-bnone f20 mg-20">Thông báo hệ thống</h3>
                            <div class="content">
                                <ul class="list">
                                    @foreach(\App\Entity\Notification::showNotifyClass(3,'') as $notify)
                                        <li class="ds-block pd-010" @if($notify->status == 0 || $notify->status == 1) style="background: #edf2fa;" @else style="background: white;" @endif>
                                            <span>{{ $notify->created_at }}</span>
                                            <a href="{{ route('notificationClassroom', ['notifyId' => $notify->notify_id]) }}" title="" onclick="return readNotification(this)" class="ds-inline pdtop20 pdbottom20 pd-010">
                                                <span class="ds-inline mg-015 clwhite pd-010 bggrey f13 mbmgleft0">{{ $notify->title }}</span>
                                                {{ $notify->content }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
            </div>
          </div>
        </div>
      </section>
@endsection

