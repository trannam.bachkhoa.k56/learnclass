@extends('site.layout.site')

@section('title','hỏi đáp' )
@section('meta_description',  'hỏi đáp về phòng học trực tuyến')
@section('keywords', '')
@section('content')
    <section class="breadcrumb ds-inherit pd">
        <div class="bgbread">
            <div class="container">
                <div class="row">
                    <div class="col-12 pdtop15">
                        <h1>Hỏi Đáp</h1>
                        <ul>
                            <li><a href="./">Trang chủ</a></li>
                            <li>/</li>
                            <li><a href="#">Hỏi Đáp</a></li>
                            @if (isset($category))
                                {{ $category->slug}}
                            @endif
                            <li>/</li>
                            <li><a href="#">{{ $post->title }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="question">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-3 col-sm-12 col-12 qsSliderbar">
                    <div class="questionLeft mgtop20">
                        <ul class="listquestLeft mbtext-ct">
                            @foreach (\App\Entity\Menu::showWithLocation('footer-second') as $Main_menu_footer2)
                                @foreach (\App\Entity\MenuElement::showMenuPageArray($Main_menu_footer2->slug) as $id=>$menu_footer2)
                                    @php
                                        $arrayslug = array();
                                        $urlslug = isset($menu_footer2['url']) ? $menu_footer2['url'] : '';
                                        $arrayslug = explode("/",$urlslug);
                                    @endphp
                                    <li class="ds-block
                                    <?php if (isset($category) && $arrayslug[2] == $category->slug) {
                                        echo 'activeQues';
                                    }
                                    ?> pd-15 pdleft20">
                                        <a href="{{ $menu_footer2['url'] }}"
                                           class="f13">{{ $menu_footer2['title_show'] }}</a></li>
                                @endforeach
                            @endforeach
                        </ul>
                    </div>
                    <script>
                        $(document).ready(function () {

                        //   $('ul.listquestLeft>li').click(function () {
                        //      if($('li').hasClass('activeQues')) {
                        //         $('li').removeClass('activeQues');
                        //         $(this).addClass('activeQues');
                        //     alert(id);
                        //      }
                        //      else
                        //      {
                        //         $(this).addClass('activeQues');
                        //      }
                        //   });
                        // });

                    </script>

                </div>


                <div class="col-lg-10 col-md-9 col-sm-12 col-12 qsSliderRight pdbottom20">
                    <div class="contentRight mgtop20 pdleft10">
                        <h2 class="pdtop10 text-bnone">Câu hỏi thường gặp</h2>
                    </div>
                    <div class="itemQuestion pd-10">
                        <div class="row">
                            <div class="col-lg-1 text-ct">
                                <p class="mgbottom0"><span class="f38 clxam"><i class="fas fa-caret-up"></i></span></p>
                                <p class="mgbottom0"><span
                                            class="f22">{{ \App\Entity\Comment::getCountComment($post->post_id) }}</span>
                                </p>
                                <p class="mgbottom0"><span class="f38 clxam"><i class="fas fa-caret-down"></i></span>
                                </p>


                                <p class="mgbottom0"><span class="f12">Bình luận</span></p>
                            </div>
                            <div class="col-lg-11">
                                <h1 class="f18 titleqs">{{ $post->title }}</h1>
                                <p class="f14">{{ $post->description }}</p>
                                <div class="contentReply">
                                    {!! $post->content !!}
                                </div>
                                <div class="mgtop20 mgbottom20">
                                    @include('general.sub_comments', ['post_id' => $post->post_id] )
                                </div>
                            </div>


                        </div>
                    </div>
                </div>


            </div>
            <script>
                $(document).ready(function () {
                    var element = $('.questionLeft');
                    var originalY = element.offset().top;

                    var topMargin = 20;

                    element.css('position', 'relative');

                    $(window).on('scroll', function (event) {
                        var scrollTop = $(window).scrollTop();
                        var topContent = $('.qsSliderRight').height();
                        var widthside = $(window).width();
                        if (scrollTop < (topContent - 500) && widthside >= 500) {

                            element.stop(false, false).animate({
                                top:
                                    scrollTop < originalY ? 0 : scrollTop - originalY + topMargin
                            }, 300);
                        }

                    });
                });
            </script>

        </div>
        </div>
    </section>


@endsection

