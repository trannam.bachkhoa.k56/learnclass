@extends('site.layout.site')

@section('title', 'Trang cá nhân giáo viên')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <?php
    $user = \Illuminate\Support\Facades\Auth::user();
    $teacher = \App\Entity\Teacher::detailTeacher($user->id);
    ?>
<!-- SLIDER -->
<section class="breadcrumb ds-inherit pd">
	<div class="bgbread">
		<div class="container">
			<div class="row">
				<div class="col-12 pdtop15">
					<h1>Giáo viên {{ $user->name }}</h1>
					<ul>
						<li><a href="">Trang chủ</a></li>
						<li>/</li>
						<li><a href="">Trang cá nhân giáo viên</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="teacher mg-50">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-12 sidebarContact">
                {{--<div class="teacherLeft text-lt pd10 bgwhite boxShadow">--}}
                    {{--<div class="content  ">--}}
                        {{--<div class="CropImg CropImgV">--}}
                            {{--<div class="thumbs">--}}
                                {{--<a href="" title="">--}}
                                    {{--<img src="{{ isset($user->image) && !empty($user->image) ? asset($user->image) : asset('images/no_teacher.png')  }}" alt="{{ $user->name }}"  class="">--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="spec pdtop20">--}}
                        {{--<h3 class="f20 clback">Kĩ năng</h3>--}}
                        {{--<p class="des f14">{{ $teacher->current_job }}</p>--}}
                        {{--<p class="des f14">{{ $teacher->form_of_teaching }}</p>--}}
                    {{--</div>--}}
                    {{--<div class="spec pdtop20">--}}
                        {{--<h3 class="f20 clback">Thời gian</h3>--}}
                        {{--<p class="des f14">Sunday - Saturday: Only Appointment</p>--}}
                        {{--<p class="des f14">Monday - Friday: 9:00 - 15:00</p>--}}
                    {{--</div>--}}
                    {{--<div class="contentdes mgleft20 mdmgleft0 mbmgbottom30 pdtop20">--}}
                        {{--<div class="icon text-lt pdbottom20">--}}
                            {{--<ul>--}}
                                {{--<li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-012 ds-block br br-orang"><i class="fab fa-facebook-f"></i></a></li>--}}
                                {{--<li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-twitter"></i></a></li>--}}
                                {{--<li class="ds-inline mgright5"><a href="" class="clwhite clhr-orang bgorang  bghr-white br-rs50 f14 pd-5 br-rscricle pd-010 ds-block br br-orang"><i class="fab fa-linkedin-in"></i></a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                @include('site.module_learnclass.profile_teacher', ['teacher' => $teacher, 'user' => $user])
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-12 ">
                <div class="teacherRight clblack text-lt pdleft10">
                    <h2 class="f26">{{ $user->name }}</h2>
                    <h4 class="f20 text-bnone">{{ $teacher->where_working }}</h4>
                    <div class="des f14 text-js">
                        <p>{{ $user->description }}</p>
                    </div>

                    <style>
                        .method
                        {
                            background: #777;
                            width: {{ $teacher-> teaching_methods }}%;
                        }
                        .skills
                        {
                            background: #3adb76;
                            width: {{ $teacher-> communication_skill }}%;
                        }
                        .exp
                        {
                            background: #ffae00;
                            width: {{ $teacher-> experience }}%;
                        }
                        .indexs
                        {
                            width: {{ $teacher-> numeral }}%;
                            background: #ec5840;
                        }
                    </style>

                    <h3 class="text-b f24">Chuyên môn</h3>
                    <label for="" class="f16 text-b pdtop20">Phương pháp dạy ấn tượng</label>
                    <div class="skill w100 ">
                        <div class="method"></div>
                    </div>
                    <label for="" class="f16 text-b pdtop20">Kĩ năng giao tiếp</label>
                    <div class="skill w100 ">
                        <div class="skills"></div>
                    </div>

                    <label for="" class="f16 text-b pdtop20">Kinh nghiệm</label>
                    <div class="skill w100 ">
                        <div class="exp"></div>
                    </div>

                    <label for="" class="f16 text-b pdtop20">Chỉ số</label>
                    <div class="skill w100 ">
                        <div class="indexs"></div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
<section class="joinclass">
    <div class="container">
        <div class="row">
            <div class="col-12 titleIndex">
                <h3>Tham gia vào khóa học</h3>
                <p>Thông tin khóa hoc</p>
                <div class="borderTitle">
                    <div class="left"></div>
                    <div class="center"><i class="fas fa-graduation-cap"></i></div>
                    <div class="right"></div>
                </div>
            </div>
        </div>
        <div class="row listtable">
            <div class="col-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col" class="f16 text-up bgorang clwhite text-ct">id</th>
                        <th scope="col" class="f16 text-up bgorang clwhite text-ct">tên khóa học</th>
                        <th scope="col" class="f16 text-up bgorang clwhite text-ct">thời lượng</th>
                        <th scope="col" class="f16 text-up bgorang clwhite text-ct">mốc thời gian</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach (\App\Entity\Classroom::getClassrooms($teacher->teacher_id) as $classroom)
                    <tr class="text-ct">
                        <td scope="row" >{{ $classroom->classroom_id }}</td>
                        <td class="text-lt">{{ $classroom->name }}</td>
                        <td>{{ (((strtotime($classroom->ended_date) - strtotime($classroom->started_date))/60)/60)/24 }} ngày</td>
                        <td>{{ $classroom->started_date }} - {{ $classroom->ended_date }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{--<div class="row listCate">--}}
                {{--<div class="col-lg-6 col-md-6 col-sm-12 col-12 titleLeft">--}}
                    {{--<p><a href="">{{ $category->title }}</a></p>--}}
                {{--</div>--}}
                {{--<div class="col-lg-6 col-md-6 col-sm-12 col-12 showRight">--}}
                    {{--<div class="showall"><a href="">Xem tất cả <i class="fas fa-plus-square"></i></a></div>--}}
                {{--</div>--}}
                {{--@if ($countProduct == 0)--}}
                    {{--<div class="col-12 col-sm-12 col-md-12">--}}
                        {{--<p style="text-align: center;">Hiện tại chưa có khóa học nào cả.</p>--}}
                    {{--</div>--}}
                {{--@endif--}}
                {{--@foreach (\App\Entity\Product::getProductByTeacher($teacher->teacher_id) as $id => $product)--}}
                    {{--<div class="col-lg-4 col-md-12 col-sm-6 col-12 mgbottom20">--}}
                        {{--<div class="item">--}}
                            {{--@include('site.module_learnclass.product_item', ['product' => $product])--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--@endforeach--}}
                {{--<div class="col-12 Pagecate">--}}
                    {{--@if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )--}}
                        {{--{{ $products->links() }}--}}
                    {{--@endif--}}
                {{--</div>--}}

            {{--</div>--}}
        </div>
    </div>
</section>
<!-- KHÁCH HÀNG NÓI VỀ CHÚNG TÔI -->
<section class="customer">
    <div class="container">
        <div class="row">
            <div class="col-12 titleIndex">
                <h3>khách hàng nói về chúng tôi</h3>
                <p>Khách hàng tin tưởng vào phòng học trực tuyến</p>
                <div class="borderTitle">
                    <div class="left"></div>
                    <div class="center"><i class="fas fa-graduation-cap"></i></div>
                    <div class="right"></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-11 col-md-12 col-sm-12">
                <div class="owl-carousel">
                    @foreach(\App\Entity\SubPost::showSubPost('khach-hang',15) as  $id=> $custom)
                    <div class="item">
                        <div class="itemcustomer">
                            <div class="img">
                                <div class="CropImg">
                                    <div class="thumbs">
                                        <a href="#" title="{{ isset($custom['ten-khach-hang']) ?$custom['ten-khach-hang'] : '' }}">
                                            <img src="{{ asset($custom['image']) ?$custom['image'] : '' }}" alt="{{ isset($custom['ten-khach-hang']) ?$custom['ten-khach-hang'] : '' }}">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <a href="" class="titletea">{{ isset($custom['ten-khach-hang']) ?$custom['ten-khach-hang'] : '' }}</a>
                                <p class="des">{{ isset($custom['ten-phu']) ?$custom['ten-phu'] : '' }}</p>
                                <p><i class="fas fa-minus"></i><span>{{ isset($custom['mo-ta']) ?$custom['mo-ta'] : '' }}</span></p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <script>
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:20,
                responsiveClass:true,
                autoplay:true,
                autoplayTimeout:7000,
                nav:true,
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:1,
                        nav:true
                    },
                    1000:{
                        items:2,
                        nav:true,
                        loop:true
                    }
                }
            });
            // $('.owl-prev').html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');
            // $('.owl-next').html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
        </script>
    </div>
</section>

{{--<section class="Starlear formcateNew mg-50">--}}
    {{--<div class="bgapactity">--}}
        {{--<div class="container">--}}
            {{--<div class="row justify-content-lg-center">--}}
                {{--<div class=" col-12 titleIndex mdpdtop30">--}}
                    {{--<h3> <span class="clwhite">Đặt chỗ ngồi của bạn</span></h3>--}}
                    {{--<p><span class="clwhite">Vui lòng nhập vào form đăng kí</span></p>--}}
                    {{--<div class="borderTitle">--}}
                        {{--<div class="left" style="background: #fff"></div>--}}
                        {{--<div class="center clwhite"><i class="fas fa-graduation-cap"></i></div>--}}
                        {{--<div class="right" style="background: #fff"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-10 col-md-12 col-sm-12 col-12 formContact">--}}
                    {{--<form action="" method="get" accept-charset="utf-8" class="">--}}
                        {{--<div class="form-group row mbpdleft10">--}}
                            {{--<div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">--}}
                                {{--<label for="" class="text-b f14 clwhite">Họ *</label>--}}
                                {{--<input type="text"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey bgwhite" id="staticEmail"  placeholder="Nhập họ ...">--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">--}}
                                {{--<label for="" class="text-b f14 clwhite">Tên *</label>--}}
                                {{--<input type="text"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey bgwhite" id="staticEmail"  placeholder="Nhập tên">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group row mbpdleft10">--}}
                            {{--<div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">--}}
                                {{--<label for="" class="text-b f14 clwhite">Website *</label>--}}
                                {{--<input type="text"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey bgwhite" id="staticEmail"  placeholder="Nhập họ ...">--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">--}}
                                {{--<label for="" class="text-b f14 clwhite">Tiêu đề *</label>--}}
                                {{--<input type="text"  class="form-control-plaintext f14 clgrey br brcl-input br-rs3 pd8 pdleft10 clgrey bgwhite" id="staticEmail"  placeholder="Nhập tên">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group row mbpdleft10">--}}
                            {{--<div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">--}}
                                {{--<label for="" class="text-b f14 clwhite">Website *</label>--}}
                                {{--<select id="inputState" class="form-control">--}}
                                    {{--<option selected>Choose...</option>--}}
                                    {{--<option>...</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-6 col-md-12 col-sm-12 col-12 pd pdright20 mdmgtop15">--}}
                                {{--<label for="" class="text-b f14 clwhite">Tiêu đề *</label>--}}
                                {{--<select id="inputState" class="form-control">--}}
                                    {{--<option selected>Choose...</option>--}}
                                    {{--<option>...</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group row mdmgtop15 mbpdleft10">--}}
                            {{--<div class="col-12 pd pdright10">--}}
                                {{--<label for="" class="text-b f14 clwhite ">Bình luận *</label>--}}
                                {{--<textarea name="" rows="8" placeholder="Vui lòng để lại bình luận" class="w100 pdleft10 br brcl-input br-rs5 clgrey"></textarea>--}}
                            {{--</div>--}}

                        {{--</div>--}}
                        {{--<div class="form-group row mbpdleft10">--}}
                            {{--<button class="pd-10 pd-020 bgnone clwhite f18 br br-white bghr-orang text-up text-b br-rs5   ">Gửi thông tin liên hệ</button>--}}

                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</section>--}}

@endsection