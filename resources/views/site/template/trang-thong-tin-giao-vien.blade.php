@extends('site.layout.site')

@section('title', 'Trang thông tin giáo viên')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <?php
        $user = \Illuminate\Support\Facades\Auth::user();
        $teacher = \App\Entity\Teacher::detailTeacher($user->id);
    ?>
    <section class="breadcrumb ds-inherit pd">
    	<div class="bgbread">
	        <div class="container">
	            <div class="row">
	                <div class="col-12 pdtop15">
	                    <h1 class="mbf20">Giáo viên {{ $user->name }}</h1>
	                    <ul>
	                        <li><a href="">Trang chủ</a></li>
	                        <li>/</li>
	                        <li><a href="">Giáo viên {{ $user->name }}</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
	    </div>
    </section>

	<section class="teacher bggray pdbottom30">
		<div class="container">
			@include('site.module_learnclass.profile_teacher_classroom')
			<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-12 col-12 sidebarContact">
					@include('site.module_learnclass.menu_user')
				</div>
				<div class="col-lg-9 col-md-8 col-sm-12 col-12">
					<div class="newCourse text-lt row mgbottom20">
						<a class="ds-inline f15 clwhite bgorang text-up text-b700 pd-10 pd-010 mgright5 posedit mdds-none hvr-shutter-out-vertical" href="/trang/them-moi-lop-hoc"><i class="fa fa-plus mgright5"></i> Thêm mới khóa học</a>
					</div>
					@if (isset($teacher->teacher_id) )
						@foreach (\App\Entity\Classroom::getClassrooms($teacher->teacher_id) as $classroom)
					<div class="row contentRgihtInfo pd20 bgwhite mgbottom15">
						<div class="col-lg-3 pd">
							<div class="img">
								<div class="CropImg CropImg80">
									<div class="thumbs">
										<a href="{{ route('lesson', ['classroomSlug' => $classroom->slug, 'classroomId' => $classroom->classroom_id ]) }}" title="">
											<img src="{{ isset($classroom->image) ? asset($classroom->image) : asset('images/noiimg.png') }}" class="">
										</a>
									</div>
								</div>
								@php
										$dateStart = $classroom->started_date.' '.$classroom->started_time;
                                        $timeStart = strtotime($dateStart);
                                        $dateEnd = $classroom->ended_date;
                                        $timeEnd = strtotime($dateEnd);
									@endphp
									@if ($classroom->recruitment == 'tuyen-dung')
										<div class="public item">Đang tuyển sinh</div>
									@elseif ($timeStart > time())
										<div class="public item">Sắp bắt đầu</div>
									@elseif ( $timeStart <= time() && $timeEnd >= time() )
										<div class="start item">Đang diễn ra</div>
									@else
										<div class="complete item">Đã hoàn thành</div>
									@endif
							</div>

						</div>
						<div class="col-lg-9 pd00">
							<div class="content contentTop text-lt mgleft25">
								<h3 class="text-ca f26">{{ $classroom->title }}</h3>
								<p class="clred f16 price"><span class="mgright15 text">{{ (!empty($classroom->price)) ? number_format($classroom->price, 0, ',', '.').' đ' : 'Miễn phí' }}</span></p>
								<ul>
									<li><a href=""><i class="fas fa-users"></i>Giới hạn học sinh: {{ $classroom->min_student }} người</a></li>
									<li><a href=""><i class="fa fa-user"></i>Số người đã đăng ký: 5</a></li>
									<li><a href=""><i class="fas fa-clock mgright5"></i>{{ ($classroom->recruitment == 'tuyen-dung') ? 'Dự kiến' : 'Bắt đầu' }}: {{ App\Ultility\Ultility::formatDateTime($classroom->started_time, $classroom->started_date) }}</a></li>
								</ul>
								<a href="{{ route('lesson', ['classroomSlug' => $classroom->slug, 'classroomId' => $classroom->classroom_id ]) }}" class="ds-inline f15 clwhite bgorang text-up text-b700 pd-5 pd-010 mgright5 posedit mdds-none hvr-shutter-out-vertical">
									<i class="far fa-edit"></i> Xem chi tiết
								</a>

								<a href="{{ route('lesson', ['classroomSlug' => $classroom->slug, 'classroomId' => $classroom->classroom_id ]) }}" class="ds-none f15 clwhite bgorang text-up text-b700 pd-5 pd-010 mgright5 mdds-inline hvr-shutter-out-vertical">
									<i class="far fa-edit"></i> Xem chi tiết
								</a>
							</div>

						</div>
						<div class="col-lg-12 pd">
							<div class="borderbottom">

							</div>
							<div class="listClassInfo text-lt">
								<div class="titleNumber f18 mgbottom20">Số buổi học: {{ $classroom->number_lesson }} buổi học <a class="text-up clwhite bgorang text-b700 pd-5 pd-010 fl-rt hvr-shutter-out-vertical"><i class="fas fa-address-book mgright5"></i> Xem danh sách buổi học</a></div>
								<div class="clear"></div>
								<ul class="ShowList">
									@foreach (\App\Entity\Lesson::getLesson($classroom->classroom_id) as $lesson)
									@php
										$dateStart = $lesson->date_at.' '.$lesson->time_start;
										$timeStart = strtotime($dateStart);
										$dateEnd = $lesson->date_at.' '.$lesson->time_end;
										$timeEnd = strtotime($dateEnd);
									@endphp
									<li class="done">
										<div class="row">
											<div class="col-md-8">
												<a href="" class="f16 ds-inline whitespace">
														@if ($timeEnd < time())
															<span class="clwhite clgrey bggrey status">
																Đã hoàn thành
															</span>
														@elseif ( $timeEnd >= time() && $timeStart <= time() )
															<span class="clwhite bgred status">
																Đang diễn ra
															</span>
														@else
															<span class="clwhite bgorang status">
																Sắp bắt đầu
															</span>
														@endif
													
													{{ $lesson->title }} 

													
												</a>
											</div>
											<div class="dateEdit f18 ds-block text-rt col-md-4">
												<span class="date">({{ \App\Ultility\Ultility::formatTime($lesson->time_start) }} {{ \App\Ultility\Ultility::formatDate($lesson->date_at) }})</span>
												<a href="{{ route('edit_lesson', ['classroomId' => $classroom->classroom_id, 'lesson_id' => $lesson->lesson_id]) }}" class="f18 ds-inline"><i class="far fa-edit"></i></a>
												<a href="{{ route('destroy_lesson', ['classroomId' => $classroom->classroom_id, 'lesson_id' => $lesson->lesson_id]) }}" class="f18 ds-inline"><i class="far fa-trash-alt"></i></a>
													
											</div>
										</div>
									</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
						@endforeach
					@endif
				</div>
				<script>
					var click = $('.listClassInfo .titleNumber');
					click.click(function(){
						if($(this).parent().find('.ShowList').is(":hidden")) {
							$(this).parent().find('.ShowList').slideDown();
						}
						else {
							$(this).parent().find('.ShowList').slideUp();
						}
					});

				</script>
			</div>
		</div>
	</section>
@endsection