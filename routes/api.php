<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['namespace'=>'Api'],function(){
    Route::post('login', 'LoginController@index');
    Route::get('infor-user', 'LoginController@getAuthUser');
    Route::get('check-user', 'LoginController@checkAuthUser');
    // Khóa Học
    Route::get('list-classroom', 'ClassroomController@index');
    Route::get('classroom/{classroomID}', 'ClassroomController@detail');

    // Thông Báo
    Route::get('notifications', 'NotificationController@index');
    Route::get('notification/{notificationId}', 'NotificationController@detail');

    Route::post('push-notification', 'NotificationController@pushNotification');

    Route::get('notification-join-classroom', 'NotificationController@joinClassroom');
    // Đăng Ký
    Route::post('register', 'LoginController@register');

    Route::get('forget-password', 'LoginController@forgetPassword');

    Route::get('/get-users', 'TestController@getUsers')->name('get_users');

    Route::post('/store-user', 'TestController@store')->name('store_user');

    Route::post('/update-user', 'TestController@update')->name('update_user');

    Route::get('/delete-user', 'TestController@delete')->name('delete_user');

    // Lấy Toàn Bộ Lớp Học
    Route::get('/all-class','ClassroomController@getAllClassroom')->name('all_class');

    // Đăng Ký Lớp Học
    Route::get('/register_course/{classroomID}','RegisterCourseController@registerCourse');
});
