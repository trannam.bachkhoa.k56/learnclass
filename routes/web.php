<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'admin','namespace'=>'Admin', 'middleware' => ['admin']],function(){
    Route::get('home', 'AdminController@home')->name('admin_home');

    Route::resource('posts', 'PostController');
    Route::get('posts-show', 'PostController@anyDatatables')->name('datatable_post');
    Route::get('posts-visiable', 'PostController@visiable')->name('visable_post');

    Route::resource('pages', 'PageController');
    Route::get('pages-show', 'PageController@anyDatatables')->name('show_page');

    Route::resource('filter', 'FilterController');

    Route::resource('comments', 'CommentController');
    Route::get('comments-show', 'CommentController@anyDatatables')->name('datatable_comment');
    Route::post('comments-random', 'CommentController@randomCommentFromForm')->name('randomCommentFromForm');
    Route::get('comments-random', 'CommentController@randomComment')->name('randomComment');

    Route::resource('contact', 'ContactController');

    Route::resource('products', 'ProductController');
    Route::get('products-show', 'ProductController@anyDatatables')->name('datatable_product');
    Route::get('export-products', 'ProductController@exportToExcel')->name('exportProducts');
    Route::post('import-products', 'ProductController@importToExcel')->name('importProducts');
    Route::post('get-product-getfly', 'ProductController@getProductGetfly')->name('getProductGetfly');

    Route::resource('templates', 'TemplateController');
    Route::resource('type-information', 'TypeInformationController');
    Route::resource('type-input', 'TypeInputController');
    Route::resource('type-sub-post', 'TypeSubPostController');
    Route::resource('categories', 'CategoryController');
    Route::resource('category-products', 'CategoryProductController');
    Route::get('get-cate-product-getfly', 'CategoryProductController@getCateProductGetfly')->name('getCateProductGetfly');

    Route::group(['prefix' => '{typePost}'], function($typePost){
        // Files
        Route::resource('sub-posts', 'SubPostController');
        Route::get('sub-posts-show', 'SubPostController@anyDatatables')->name('datatable_sub_posts');
    });
    Route::resource('information', 'InformationController', ['only' => [
        'index', 'store'
    ]]);
    Route::get('information/general', 'InformationController@generalCreate')->name('information-general');
    Route::post('information/general-store', 'InformationController@generalStore')->name('information-store');

    Route::resource('menus', 'MenuController');
    Route::resource('users', 'UserController');

    Route::resource('subcribe-email', 'SubcribeEmailController');
    Route::get('subcribe-email-anyDatabase', 'SubcribeEmailController@anyDatatables')->name('subcribe-email-data');
    Route::post('import-subcribe-email', 'SubcribeEmailController@subcribeEmail')->name('import_subcribe_email');


    Route::post('group-mail/create', 'GroupMailController@store')->name('group_mail.create');
    Route::delete('group-mail/{groupMail}', 'GroupMailController@destroy')->name('group_mail.destroy');
    Route::post('send-mail', 'SubcribeEmailController@send')->name('subcribe-email_send');

    Route::get('hinh-thuc-thanh-toan', 'OrderController@setting')->name('method_payment');
    Route::post('cap-nhat-ngan-hang', 'OrderController@updateBank')->name('bank');
    Route::delete('quan-li-ngan-hang/{orderBanks}', 'OrderController@deleteBank')->name('orderBanks.destroy');
    Route::post('cap-nhat-ma-giam-gia', 'OrderController@updateCodeSale')->name('code_sale');
    Route::delete('quan-li-giam-gia/{orderCodeSales}', 'OrderController@deleteCodeSale')->name('orderCodeSales.destroy');

    Route::post('cap-nhat-phi-ship', 'OrderController@updateShip')->name('cost_ship');
    Route::post('cap-nhat-tinh-diem', 'OrderController@updateSetting')->name('updateSetting');
    Route::delete('quan-li-phi-ship/{orderShips}', 'OrderController@deleteShip')->name('orderShips.destroy');

    Route::post('cai-dat-getfly', 'OrderController@updateSettingGetFly')->name('updateSettingGetFly');
    Route::post('cau-hinh-email', 'OrderController@updateSettingEmail')->name('updateSettingEmail');
    Route::post('kiem-tra-cau-hinh', 'SettingController@testEmail')->name('testEmail');

    Route::get('don-hang', 'OrderController@listOrder')->name('orderAdmin');
    Route::get('export-exel','OrderController@exportToExcel')->name('exportToExcel');
    Route::delete('don-hang/{order}', 'OrderController@deleteOrder')->name('orderAdmin.destroy');

    Route::post('cap-nhat-trang-thai-don-hang', 'OrderController@updateStatusOrder')->name('orderUpdateStatus');

    Route::get('thong-bao', 'ReportController@allreport')->name('report');
    Route::get('da-xem-thong-bao', 'ReportController@seenNotification')->name('seenNotification');
    Route::get('da-doc-thong-bao', 'ReportController@readNotification')->name('readNotification');
    Route::get('thong-bao-day', 'ReportController@pushNotification')->name('pushNotify');

    Route::post('upload-fanpage', 'PostFanpageController@uploadFanpage')->name('upload_fanpage');
    Route::get('comment-facebook', 'CommentFacebookController@pushComment')->name('comment_facebook');

    Route::post('update-groups', 'FanpageController@updateGroups')->name('update_groups');
    Route::post('update-face-id', 'FanpageController@updateFaceIds')->name('update_facebook_id');
    Route::get('delete-face-id', 'FanpageController@deleteFaceIds')->name('delete_facebook_id');
    Route::post('update-setting-face', 'FanpageController@updateSetting')->name('update_setting_face');
    Route::get('get-post-facebook', 'FanpageController@index')->name('get_post_facebook');


    Route::get('get-uid', 'FacebookUidController@getUid')->name('get_uid');
    Route::get('show-get-uid', 'FacebookUidController@showGetUid')->name('show_get_uid');

    Route::get('show-convert-uid', 'FacebookConvertController@showConvertFromUid')->name('show_convert_uid');
    Route::get('convert-uid', 'FacebookConvertController@convertFromUid')->name('convert_uid');
    Route::post('get-uid-from-excel', 'FacebookConvertController@getUidFromExcel')->name('get_uid_from_excel');

    Route::get('show-request-friend', 'FacebookConvertController@showRequestFriend')->name('show_request_friend');
    Route::get('request-friend', 'FacebookConvertController@requestFriend')->name('request_friend');

    Route::get('download-member', 'FacebookUidController@showMembers')->name('download-member');

    Route::resource('teacher', 'TeacherController');
    Route::get('teacher-show', 'TeacherController@anyDatatables')->name('datatable_teacher');

    Route::resource('student-appraise-teacher', 'StudentAppraiseTeacherController');

    Route::resource('student', 'StudentController');
    Route::get('student-show', 'StudentController@anyDatatables')->name('datatable_student');

    Route::resource('classroom', 'ClassroomController');
    Route::get('classroom-show', 'ClassroomController@anyDatatables')->name('datatable_classroom');
    Route::post('add-student-classroom', 'ClassroomController@addStudentClassroom')->name('add_student_classroom');
    Route::post('add-lesson-classroom', 'ClassroomController@addLessonClassroom')->name('add_lesson_classroom');
    Route::get('remove-student-classroom/{classroomId}/{userId}', 'ClassroomController@removeStudentClassroom')->name('remove_student_classroom');
    Route::get('remove-lesson-classroom/{classroomId}', 'ClassroomController@removeLessonClassroom')->name('remove_lesson_classroom');

    Route::resource('teacher-appraise-student', 'TeacherAppraiseStudentController');
    Route::get('teacher-appraise-student-show', 'TeacherAppraiseStudentController@anyDatatables')->name('datatable_teacher-appraise-student');

    Route::resource('student-appraise-teacher', 'StudentAppraiseTeacherController');
    Route::get('student-appraise-teacher-show', 'StudentAppraiseTeacherController@anyDatatables')->name('datatable_student-appraise-teacher');

    Route::resource('appraise-class', 'AppraiseClassController');
    Route::get('appraise-classroom-show', 'AppraiseClassController@anyDatatables')->name('datatable_appraise-classroom');

    // quản lý đại lý
    Route::resource('agency', 'AgencyController');
    Route::get('agency-show', 'AgencyController@anyDatatables')->name('datatable_agency');

    Route::resource('cards', 'CardController');
    Route::get('card-show', 'CardController@anyDatatables')->name('datatable_card');

    Route::resource('email-spam', 'EmailSpamController');
    Route::get('email-spam-test', 'EmailSpamController@testEmail')->name('email_spam_test');

});

//dang nhap
Route::group(['prefix'=>'admin', 'namespace'=>'Admin' ],function(){
    Route::get('/','LoginController@showLoginForm');
    Route::get('login','LoginController@showLoginForm')->name('login');
    Route::post('login','LoginController@login');
    Route::get('logout','LoginController@logout');
    Route::post('logout','LoginController@logout')->name('logout');
    //reset password
    Route::get('password/reset','LoginController@getReset');
    Route::post('password/reset','LoginController@postReset');
});

Route::group(['namespace'=>'Site'], function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/hot-deal', 'ProductCategoryController@index');
    Route::get('/danh-muc/{cate_slug}', 'CategoryController@index')->name('site_category_post');
    Route::get('/cua-hang/{cate_slug}', 'ProductCategoryController@index')->name('site_category_product');
    Route::get('/tim-kiem', 'ProductCategoryController@search')->name('search_product');
    Route::get('/tim-kiem-ajax', 'ProductCategoryController@searchAjax')->name('search_product_ajax');
    Route::get('rating', 'ProductController@Rating')->name('rating');

    Route::get('/tags', 'ProductCategoryController@tags')->name('tags_product');

    Route::get('/bo-sung/{sub_post_slug}', 'SubPostController@index');

    Route::get('/dang-ky','RegisterController@showRegistrationForm')->name('register');
    Route::post('/dang-ky','RegisterController@register');

    Route::post('/quen-mat-khau','PersonController@forgetPassword')->name('forget_password');
    Route::post('/dang-nhap','LoginController@login');
    Route::get('dang-xuat','LoginController@logout');
    Route::post('dang-xuat','LoginController@logout')->name('logoutHome');
    Route::get('callbacklogin','LoginController@callbackLogin');

    Route::get('login/google', 'LoginController@redirectToProvider')->name('google_login');
    Route::get('login/google/callback', 'LoginController@handleProviderCallback');

    Route::get('/thong-tin-ca-nhan','PersonController@index');
    Route::post('/thong-tin-ca-nhan','PersonController@store');
    Route::get('/doi-mat-khau','PersonController@resetPassword');
    Route::post('/doi-mat-khau','PersonController@storeResetPassword');
    Route::get('/don-hang-ca-nhan','PersonController@orderPerson')->name('orderPerson');
    Route::post('/nhap-ma-code','PersonController@inputCode')->name('input_code_user');

    Route::get('/lien-he','ContactController@index');
    Route::post('submit/contact','ContactController@submit')->name('sub_contact');

    Route::post('submit/payment','ContactController@payment')->name('sub_payment');


    Route::get('/binh-luan', 'CommentController@index')->name('comment');
    Route::get('/xoa-binh-luan', 'CommentController@delete')->name('delete_comment');
    Route::get('/sua-binh-luan', 'CommentController@edit')->name('edit_comment');
    Route::get('/binh-luan-moi', 'CommentController@virtural')->name('virtural_comment');

    Route::get('/chi-tiet-thong-bao/{notifyId}', 'NotificationController@notificationClassroom')->name('notificationClassroom');

    Route::get('loc-san-pham', 'ProductCategoryController@filter');

    /*===== đặt hàng  ===== */
    Route::post('/dat-hang', 'OrderController@addToCart')->name('addToCart');
    Route::get('/gio-hang', 'OrderController@order')->name('order');
    Route::get('/xoa-don-hang', 'OrderController@deleteItemCart')->name('deleteItemCart');

    Route::post('/gui-don-hang', 'OrderController@send')->name('send');

    Route::get('/phong-hoc/{classroomID}/{lessonId}', 'LearnClassController@index');
	Route::get('/man-hinh-giao-vien/{classroomID}/{lessonId}', 'LearnClassController@screen');
    Route::get('/hoc-sinh-vao-phong-hoc/{classroomID}/{lessonId}', 'LearnClassController@studentClick');
	Route::get('/demo-hoc-sinh/{classroomID}/{lessonId}', 'LearnClassController@studentDemoClick');
	
    Route::get('/giao-vien-bat-dau-day/{classroomID}/{lessonId}', 'LearnClassController@teacherOpenClass')->name('teacher_open_class');
    /*===== subcribe email   ===== */
    Route::post('subcribe-email', 'SubcribeEmailController@index')->name('subcribe_email');

    Route::post('giao-vien-gui-mail-cho-hoc-sinh', 'TeacherController@sendMailUser')->name('teacher_send_mail_user');
    //Quan ly giao vien khi dang nhap

    Route::resource('manegerteacher', 'ManergerTeacherController');
    Route::resource('manegerstudent', 'ManegerStudentController');


    // Quản lý giáo viên
    Route::get('/danh-sach-giao-vien-giang-day', 'TeacherCategoryController@index')->name('category_teacher');
    Route::get('/thong-tin-chi-tiet-giao-vien/{teacherId}', 'TeacherController@index')->name('detail_teacher');
    Route::post('/dang-ky-tro-thanh-giao-vien', 'TeacherController@register')->name('register_teacher');

//    Route::get('/danh-sach-giao-vien-giang-day', 'TeacherController@search')->name('search_teacher');
    Route::get('/tim-kiem-giao-vien-ajax', 'TeacherCategoryController@searchAjax')->name('search_teacher_ajax');

    // lop hoc
    Route::post('/dang-ky-lop-hoc', 'ClassroomController@register')->name('register_classroom');
    Route::post('/chinh-sua-lop-hoc/{classroomId}', 'ClassroomController@update')->name('update_classroom');

    Route::get('/lop-hoc/{classroomSlug}/{classroomId}', 'LessonController@index')->name('lesson');
    Route::get('/dang-ky-buoi-hoc/{classroomSlug}/{classroomId}', 'LessonController@register')->name('register_lesson');
    Route::post('/dang-ky-buoi-hoc', 'LessonController@registerSubmit')->name('register_lesson_post');
    Route::get('/chinh-sua-buoi-hoc/{classroomId}/{lessonId}', 'LessonController@editLesson')->name('edit_lesson');
    Route::post('/chinh-sua-buoi-hoc', 'LessonController@update')->name('register_lesson_get');
    Route::get('/xoa-buoi-hoc/{classroomId}/{lessonId}', 'LessonController@destroy')->name('destroy_lesson');

    Route::get('/check-trang-thai-login', 'SiteController@checkLogin')->name('check_login');
    Route::get('/kich-hoat-the', 'CardController@index')->name('unlock_card');

    Route::get('/tham-gia-lop-hoc/{classroomId}', 'ClassroomController@takePartInClassroom')->name('take_part_in_classroom');

    Route::get('sitemap.xml', 'SitemapsController@index');
    Route::get('trang/{post_slug}', 'PageController@index')->name('page');

    Route::get('{cate_slug}/{post_slug}', 'PostController@index')->name('post');
    Route::get('{post_slug}', 'ProductController@index')->name('product');
    Route::get('callback','HomeController@callback')->name('callback');

});
